import FairyGUITypes, { fairyGUIElement } from './fairyGUITypes'
export default class Utils{
    private static elementHighlightBox: Laya.Sprite[] = []
    private static PropertiesText: Laya.Text
    private static RelevantProperties = [
        'x', 'y', 'width', 'height'
    ]

    static filters = {
        byName: (a: fairygui.GObject, b: string) => {
            return a.name.toLowerCase().indexOf(b.toLowerCase()) > -1
        },
        byText: (a: fairygui.GObject, b: string) => {
            return typeof a.text == 'string' && a.text.toLowerCase().indexOf(b.toLowerCase()) > -1
        },
        byType: (a: any, b: string) => {
            let type = a.constructor.name
            
            if(!FairyGUITypes[type]){
                type = a.constructor.__className.split('.')[1]
            }
            if(!FairyGUITypes[type]){
                console.error(`Couldn't find type for node `, a, `. Impossible to filter`)
                return false
            }

            return type.toLowerCase().indexOf(b.toLowerCase()) > -1
        }
    }

    private static children: fairygui.GComponent[]

    static getCubeLines = (e: fairygui.GComponent) =>{
        const d = Utils.getAbsoluteDimensions(e)

        return [
            {//top
                fromX: d.x,
                toX: d.x + d.width,
                fromY: d.y,
                toY: d.y
            },
            {//right
                fromX: d.x + d.width,
                toX: d.x + d.width,
                fromY: d.y,
                toY: d.y + d.height
            },
            {//bottom
                fromX: d.x + d.width,
                toX: d.x,
                fromY: d.y + d.height,
                toY: d.y + d.height
            },
            {//left
                fromX: d.x,
                toX: d.x,
                fromY: d.y + d.height,
                toY: d.y
            }
        ]
    }

    static drawStats = (view: fairygui.GComponent, process?: boolean) =>{
        const children = Utils.getChildren(view)
        children.forEach(element => {
            element.text = "Whatever"
            Utils.drawBorder(element)
        })
    }

    static getAbsoluteDimensions = (element: fairygui.GComponent) =>{
        const dimensions = {
            x: element.x,
            y: element.y,
            width: element.width,
            height: element.height
        }

        let parent = element.parent

        while(parent === Object(parent)){
            dimensions.x += parent.x
            dimensions.y += parent.y

            parent = parent.parent
        }

        return dimensions
    }

    static drawBorder = (element: fairygui.GComponent) =>{
        const cubeLines = Utils.getCubeLines(element)
        const sprite = new Laya.Sprite()
        Laya.stage.addChild(sprite)

        cubeLines.forEach(coords => {
            sprite.graphics.drawLine(coords.fromX, coords.fromY, coords.toX, coords.toY, "#e02340", 2);
        })
    }

    static processElement = (element: fairygui.GComponent) =>{
        //Mr. FairyGUI decided to disable touch events for text elements during runtime by 
        //disabling their "mouseEnabled" property on the original _displayObject used by LayaAir

        element.displayObject.mouseEnabled = true 
        element.touchable = true
        element.focusable = true
        element.enabled = true
    }

    static findElement = (container: fairygui.GComponent, text: string, filter: Function, highlight?: boolean, cleanAfter?: number, showFeedback?: boolean): any[] => {
        if(typeof text != 'string'){
            console.error('Invalid name to search by. A string is expected.')
            return
        }
        if(container !== Object(container)){
            console.error('Invalid element', container)
            return
        }

        const children = Utils.getChildren(container)
        const filteredChildren = []
        children.forEach((element) => {
            if(filter(element, text)){
                filteredChildren.push(element)
                if(highlight !== false){
                    Utils.drawBoxAroundElement(element)
                }
            }
        })

        if(highlight !== false){
            let _cleanAfter = typeof cleanAfter == 'number' ? cleanAfter : 5000
            setTimeout(Utils.cleanLines, _cleanAfter)
        }


        if(!filteredChildren.length){
            if(showFeedback !== false){
                console.info(`Couldn't find element by name "${text}" in container`, container)
            }
            return []
        }
        else{
            if(showFeedback !== false){
                console.info(`%c Found ${filteredChildren.length} element(s)`, 'background: #222; color: #bada55', filteredChildren)
            }
            return filteredChildren
        }
    }

    static findElementByName = (container: fairygui.GComponent, name: string, highlight?: boolean, cleanAfter?: number, showFeedback?: boolean): any[] => {
        return Utils.findElement(container, name, Utils.filters.byName, highlight, cleanAfter, showFeedback)
    }

    static findElementByText = (container: fairygui.GComponent, text: string, highlight?: boolean, cleanAfter?: number, showFeedback?: boolean): any[] => {
        return Utils.findElement(container, text, Utils.filters.byText, highlight, cleanAfter, showFeedback)
    }

    static findElementByType = (container: fairygui.GComponent, text: string, highlight?: boolean, cleanAfter?: number, showFeedback?: boolean): any[] => {
        return Utils.findElement(container, text, Utils.filters.byType, highlight, cleanAfter, showFeedback)
    }


    static getChildren = (container: fairygui.GComponent, recursive?: boolean, process?: boolean): fairygui.GComponent[] => {
        if(container !== Object(container)){
            console.error('Invalid element', container)
            return
        }

        const children = []

        container._children.forEach(element => {
            children.push(element)
            if(process === true)
                Utils.processElement(element)
            
            if(recursive !== false && Array.isArray(element._children) && element._children.length)
                children.push(...Utils.getChildren(element, recursive, process))
        })

        return children
    }

    //TODO simplify overloads
    static getChildrenByType(element: fairygui.GComponent, type: fairyGUIElement, recursive?: boolean): fairygui.GObject[]{
        if(!(element instanceof fairygui.GComponent))
            throw new Error(`Invalid element to get children by type from. Not an instance of fairygui.GComponent`)

        const children = []
        element._children.forEach(child => {
            if(child instanceof fairygui[type])
                children.push(child)

            if(recursive === true && Array.isArray(child._children) && child._children.length)
                children.push(...Utils.getChildrenByType(child, type, recursive))
        })

        return children
    }

    static highlightInspectedEvent = (event) => {
        Utils.cleanLines()
        Utils.cleanPropertiesText()
        Utils.drawBoxAroundElement(event.target)
    }

    static removeInspectionEvent = (event) => {
        event.currentTarget.off(Laya.Event.MOUSE_OUT, Utils, Utils.removeInspectionEvent)
    }

    static addInspectionListeners = (element: fairygui.GObject, onInspect: Function) => {
        element.on(Laya.Event.MOUSE_MOVE, Utils, Utils.highlightInspectedEvent)
        element.on(Laya.Event.CLICK, Utils, onInspect)
    }

    static removeInspectionListeners = (element: fairygui.GObject, onInspect: Function) => {
        element.off(Laya.Event.MOUSE_MOVE, Utils, Utils.highlightInspectedEvent)
        element.off(Laya.Event.CLICK, Utils, onInspect)
        element.on(Laya.Event.MOUSE_OUT, Utils, e => { Utils.removeInspectionEvent(e) })
    }

    static cleanLines = () =>{
        Utils.elementHighlightBox.forEach(sprite => {
            Laya.stage.removeChild(sprite)
        })
    }

    static cleanPropertiesText = () => {
        Laya.stage.removeChild(Utils.PropertiesText)
    }

    static getRelevantProperties = (element: fairygui.GComponent) => {
        let properties: any = {}
        const dimensions = Utils.getAbsoluteDimensions(element)
        Utils.RelevantProperties.forEach(property => {
            properties[property] = element[property]
        })

        properties.absoluteX = dimensions.x
        properties.absoluteY = dimensions.y

        let formattedProperties = '{\n'
        for(const property in properties){
            formattedProperties += `\t ${property}: ${properties[property]},\n`
        }

        formattedProperties += '}'

        return {
            formatted: formattedProperties,
            properties: properties
        }
    }

    static createFormattedPropertiesBox = (element: fairygui.GComponent) => {
        const formattedProperties = Utils.getRelevantProperties(element)
        const coords = Utils.getAbsoluteDimensions(element)
        const PropertiesText = new Laya.Text() 
        PropertiesText.font = 'Consolas'
        PropertiesText.text = formattedProperties.formatted
        PropertiesText.color = '#fdf6e3'
        PropertiesText.fontSize = 14  
        PropertiesText.bgColor = '#002b36'
        PropertiesText.padding = [10, 10, 10, 10]
        PropertiesText.pos(coords.x, coords.height + coords.y)  

        if(PropertiesText.y + PropertiesText.height > Laya.stage.height){//overflow-y
            PropertiesText.y = Math.abs(coords.y - PropertiesText.height)
        }

        if(PropertiesText.x + PropertiesText.width > Laya.stage.width){//overflow-x
            PropertiesText.x = coords.x - Math.abs(coords.width - PropertiesText.width)
        }

        return PropertiesText
    }

    static drawBoxAroundElement = (element: fairygui.GComponent) => {
        const coords = Utils.getAbsoluteDimensions(element)
        const cubeLines = Utils.getCubeLines(element)
        const sprite = new Laya.Sprite()
        sprite.alpha = 0.5
        Utils.elementHighlightBox.push(sprite)
        Laya.stage.addChild(sprite)
        
        cubeLines.forEach(coords => {
            sprite.graphics.drawLine(coords.fromX, coords.fromY, coords.toX, coords.toY, "#e02340", 6)
        })

        sprite.graphics.drawRect(coords.x, coords.y, coords.width, coords.height, "#268bd2")

        return sprite
    }

    static debugElement = (element: fairygui.GComponent, event?: Laya.Event, log?: boolean) => {
        if(log !== false){
            console.info(`%c Clicked`, 'background: #222; color: #bada55', element)
        }

        //event.nativeEvent.cancelBubble = true
        //event.stopPropagation()
        Utils.cleanLines()
        Utils.cleanPropertiesText()
        Utils.drawBoxAroundElement(element)
        
        Utils.PropertiesText = Utils.createFormattedPropertiesBox(element)
        Laya.stage.addChild(Utils.PropertiesText)
    }

    static addListeners = (view: fairygui.GComponent) => {
        const children = Utils.getChildren(view, true, true)

        children.forEach(element => {
            element.on(Laya.Event.MOUSE_OVER, Utils, (event: Laya.Event) => {
                Utils.debugElement(element, event)
            })

            element.on(Laya.Event.CLICK, Utils, (event: Laya.Event) => {
                Utils.debugElement(element, event, true)
            })

            element.on(Laya.Event.MOUSE_OUT, Utils, (event: Event) => {
                //console.log('out event', event, element)
                Utils.cleanLines()
                Utils.cleanPropertiesText()
            })
        })
    }
}