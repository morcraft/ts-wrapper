const types = {
  GBasicTextField: true,
  GButton: true,
  GComboBox: true,
  GComponent: true,
  GGraph: true,
  GGroup: true,
  GImage: true,
  GList: true,
  GLabel: true,
  GLoader: true,
  GMovieClip: true,
  GObjectPool: true,
  GProgressBar: true,
  GRichTextField: true,
  GRoot: true,
  GSlider: true,
  GScrollBar: true,
  GTextField: true,
  GTextInput: true,
  GObject: true,
}

export type fairyGUIElement = keyof typeof types
export type fairyGUIMap = {
    [K in fairyGUIElement]: any
}
export type fairyGUIPartialMap = {
    [K in fairyGUIElement]?: any
}

export default types