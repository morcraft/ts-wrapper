declare module mainUI{
	interface Main_XML extends fairygui.GComponent{
		getChild(name: 'n0'): Main_XMLǃn0ǃIMAGE
		getChildAt(index: 0): Main_XMLǃn0ǃIMAGE
		getChildById(id: 'n0_cwmc'): Main_XMLǃn0ǃIMAGE
		getChild(name: 'n274'): Main_XMLǃn274ǃIMAGE
		getChildAt(index: 1): Main_XMLǃn274ǃIMAGE
		getChildById(id: 'n274_ej3n'): Main_XMLǃn274ǃIMAGE
		getChild(name: 'n274_CN2'): Main_XMLǃn274_CN2ǃIMAGE
		getChildAt(index: 2): Main_XMLǃn274_CN2ǃIMAGE
		getChildById(id: 'n292_knf5'): Main_XMLǃn274_CN2ǃIMAGE
		getChild(name: 'n274_EN'): Main_XMLǃn274_ENǃIMAGE
		getChildAt(index: 3): Main_XMLǃn274_ENǃIMAGE
		getChildById(id: 'n293_knf5'): Main_XMLǃn274_ENǃIMAGE
		getChild(name: 'n274_IN'): Main_XMLǃn274_INǃIMAGE
		getChildAt(index: 4): Main_XMLǃn274_INǃIMAGE
		getChildById(id: 'n294_knf5'): Main_XMLǃn274_INǃIMAGE
		getChild(name: 'n274_JP'): Main_XMLǃn274_JPǃIMAGE
		getChildAt(index: 5): Main_XMLǃn274_JPǃIMAGE
		getChildById(id: 'n295_knf5'): Main_XMLǃn274_JPǃIMAGE
		getChild(name: 'n274_KR'): Main_XMLǃn274_KRǃIMAGE
		getChildAt(index: 6): Main_XMLǃn274_KRǃIMAGE
		getChildById(id: 'n296_knf5'): Main_XMLǃn274_KRǃIMAGE
		getChild(name: 'n274_TH'): Main_XMLǃn274_THǃIMAGE
		getChildAt(index: 7): Main_XMLǃn274_THǃIMAGE
		getChildById(id: 'n297_knf5'): Main_XMLǃn274_THǃIMAGE
		getChild(name: 'n264_EN'): Main_XMLǃn264_ENǃIMAGE
		getChildAt(index: 8): Main_XMLǃn264_ENǃIMAGE
		getChildById(id: 'n264_u4wi_EN'): Main_XMLǃn264_ENǃIMAGE
		getChild(name: 'n264_IN'): Main_XMLǃn264_INǃIMAGE
		getChildAt(index: 9): Main_XMLǃn264_INǃIMAGE
		getChildById(id: 'n264_u4wi_IN'): Main_XMLǃn264_INǃIMAGE
		getChild(name: 'n264_JP'): Main_XMLǃn264_JPǃIMAGE
		getChildAt(index: 10): Main_XMLǃn264_JPǃIMAGE
		getChildById(id: 'n264_u4wi_JP'): Main_XMLǃn264_JPǃIMAGE
		getChild(name: 'n264_KR'): Main_XMLǃn264_KRǃIMAGE
		getChildAt(index: 11): Main_XMLǃn264_KRǃIMAGE
		getChildById(id: 'n264_u4wi_KR'): Main_XMLǃn264_KRǃIMAGE
		getChild(name: 'n264_TH'): Main_XMLǃn264_THǃIMAGE
		getChildAt(index: 12): Main_XMLǃn264_THǃIMAGE
		getChildById(id: 'n264_u4wi_TH'): Main_XMLǃn264_THǃIMAGE
		getChild(name: 'n264_VN'): Main_XMLǃn264_VNǃIMAGE
		getChildAt(index: 13): Main_XMLǃn264_VNǃIMAGE
		getChildById(id: 'n264_u4wi_VN'): Main_XMLǃn264_VNǃIMAGE
		getChild(name: 'n264_CN2'): Main_XMLǃn264_CN2ǃIMAGE
		getChildAt(index: 14): Main_XMLǃn264_CN2ǃIMAGE
		getChildById(id: 'n264_u4wi_CN2'): Main_XMLǃn264_CN2ǃIMAGE
		getChild(name: 'n264'): Main_XMLǃn264ǃIMAGE
		getChildAt(index: 15): Main_XMLǃn264ǃIMAGE
		getChildById(id: 'n264_u4wi'): Main_XMLǃn264ǃIMAGE
		getChild(name: 'n265_EN'): Main_XMLǃn265_ENǃIMAGE
		getChildAt(index: 16): Main_XMLǃn265_ENǃIMAGE
		getChildById(id: 'n265_u4wi_EN'): Main_XMLǃn265_ENǃIMAGE
		getChild(name: 'n265_IN'): Main_XMLǃn265_INǃIMAGE
		getChildAt(index: 17): Main_XMLǃn265_INǃIMAGE
		getChildById(id: 'n265_u4wi_IN'): Main_XMLǃn265_INǃIMAGE
		getChild(name: 'n265_JP'): Main_XMLǃn265_JPǃIMAGE
		getChildAt(index: 18): Main_XMLǃn265_JPǃIMAGE
		getChildById(id: 'n265_u4wi_JP'): Main_XMLǃn265_JPǃIMAGE
		getChild(name: 'n265_KR'): Main_XMLǃn265_KRǃIMAGE
		getChildAt(index: 19): Main_XMLǃn265_KRǃIMAGE
		getChildById(id: 'n265_u4wi_KR'): Main_XMLǃn265_KRǃIMAGE
		getChild(name: 'n265_TH'): Main_XMLǃn265_THǃIMAGE
		getChildAt(index: 20): Main_XMLǃn265_THǃIMAGE
		getChildById(id: 'n265_u4wi_TH'): Main_XMLǃn265_THǃIMAGE
		getChild(name: 'n265_VN'): Main_XMLǃn265_VNǃIMAGE
		getChildAt(index: 21): Main_XMLǃn265_VNǃIMAGE
		getChildById(id: 'n265_u4wi_VN'): Main_XMLǃn265_VNǃIMAGE
		getChild(name: 'n265_CN2'): Main_XMLǃn265_CN2ǃIMAGE
		getChildAt(index: 22): Main_XMLǃn265_CN2ǃIMAGE
		getChildById(id: 'n265_u4wi_CN2'): Main_XMLǃn265_CN2ǃIMAGE
		getChild(name: 'n265'): Main_XMLǃn265ǃIMAGE
		getChildAt(index: 23): Main_XMLǃn265ǃIMAGE
		getChildById(id: 'n265_u4wi'): Main_XMLǃn265ǃIMAGE
		getChild(name: 'n266_EN'): Main_XMLǃn266_ENǃIMAGE
		getChildAt(index: 24): Main_XMLǃn266_ENǃIMAGE
		getChildById(id: 'n266_u4wi_EN'): Main_XMLǃn266_ENǃIMAGE
		getChild(name: 'n266_IN'): Main_XMLǃn266_INǃIMAGE
		getChildAt(index: 25): Main_XMLǃn266_INǃIMAGE
		getChildById(id: 'n266_u4wi_IN'): Main_XMLǃn266_INǃIMAGE
		getChild(name: 'n266_JP'): Main_XMLǃn266_JPǃIMAGE
		getChildAt(index: 26): Main_XMLǃn266_JPǃIMAGE
		getChildById(id: 'n266_u4wi_JP'): Main_XMLǃn266_JPǃIMAGE
		getChild(name: 'n266_KR'): Main_XMLǃn266_KRǃIMAGE
		getChildAt(index: 27): Main_XMLǃn266_KRǃIMAGE
		getChildById(id: 'n266_u4wi_KR'): Main_XMLǃn266_KRǃIMAGE
		getChild(name: 'n266_TH'): Main_XMLǃn266_THǃIMAGE
		getChildAt(index: 28): Main_XMLǃn266_THǃIMAGE
		getChildById(id: 'n266_u4wi_TH'): Main_XMLǃn266_THǃIMAGE
		getChild(name: 'n266_VN'): Main_XMLǃn266_VNǃIMAGE
		getChildAt(index: 29): Main_XMLǃn266_VNǃIMAGE
		getChildById(id: 'n266_u4wi_VN'): Main_XMLǃn266_VNǃIMAGE
		getChild(name: 'n266_CN2'): Main_XMLǃn266_CN2ǃIMAGE
		getChildAt(index: 30): Main_XMLǃn266_CN2ǃIMAGE
		getChildById(id: 'n266_u4wi_CN2'): Main_XMLǃn266_CN2ǃIMAGE
		getChild(name: 'n266'): Main_XMLǃn266ǃIMAGE
		getChildAt(index: 31): Main_XMLǃn266ǃIMAGE
		getChildById(id: 'n266_u4wi'): Main_XMLǃn266ǃIMAGE
		getChild(name: 'n267_EN'): Main_XMLǃn267_ENǃIMAGE
		getChildAt(index: 32): Main_XMLǃn267_ENǃIMAGE
		getChildById(id: 'n267_u4wi_EN'): Main_XMLǃn267_ENǃIMAGE
		getChild(name: 'n267_IN'): Main_XMLǃn267_INǃIMAGE
		getChildAt(index: 33): Main_XMLǃn267_INǃIMAGE
		getChildById(id: 'n267_u4wi_IN'): Main_XMLǃn267_INǃIMAGE
		getChild(name: 'n267_JP'): Main_XMLǃn267_JPǃIMAGE
		getChildAt(index: 34): Main_XMLǃn267_JPǃIMAGE
		getChildById(id: 'n267_u4wi_JP'): Main_XMLǃn267_JPǃIMAGE
		getChild(name: 'n267_KR'): Main_XMLǃn267_KRǃIMAGE
		getChildAt(index: 35): Main_XMLǃn267_KRǃIMAGE
		getChildById(id: 'n267_u4wi_KR'): Main_XMLǃn267_KRǃIMAGE
		getChild(name: 'n267_TH'): Main_XMLǃn267_THǃIMAGE
		getChildAt(index: 36): Main_XMLǃn267_THǃIMAGE
		getChildById(id: 'n267_u4wi_TH'): Main_XMLǃn267_THǃIMAGE
		getChild(name: 'n267_VN'): Main_XMLǃn267_VNǃIMAGE
		getChildAt(index: 37): Main_XMLǃn267_VNǃIMAGE
		getChildById(id: 'n267_u4wi_VN'): Main_XMLǃn267_VNǃIMAGE
		getChild(name: 'n267_CN2'): Main_XMLǃn267_CN2ǃIMAGE
		getChildAt(index: 38): Main_XMLǃn267_CN2ǃIMAGE
		getChildById(id: 'n267_u4wi_CN2'): Main_XMLǃn267_CN2ǃIMAGE
		getChild(name: 'n267'): Main_XMLǃn267ǃIMAGE
		getChildAt(index: 39): Main_XMLǃn267ǃIMAGE
		getChildById(id: 'n267_u4wi'): Main_XMLǃn267ǃIMAGE
		getChild(name: 'n268_EN'): Main_XMLǃn268_ENǃIMAGE
		getChildAt(index: 40): Main_XMLǃn268_ENǃIMAGE
		getChildById(id: 'n268_u4wi_EN'): Main_XMLǃn268_ENǃIMAGE
		getChild(name: 'n268_IN'): Main_XMLǃn268_INǃIMAGE
		getChildAt(index: 41): Main_XMLǃn268_INǃIMAGE
		getChildById(id: 'n268_u4wi_IN'): Main_XMLǃn268_INǃIMAGE
		getChild(name: 'n268_JP'): Main_XMLǃn268_JPǃIMAGE
		getChildAt(index: 42): Main_XMLǃn268_JPǃIMAGE
		getChildById(id: 'n268_u4wi_JP'): Main_XMLǃn268_JPǃIMAGE
		getChild(name: 'n268_KR'): Main_XMLǃn268_KRǃIMAGE
		getChildAt(index: 43): Main_XMLǃn268_KRǃIMAGE
		getChildById(id: 'n268_u4wi_KR'): Main_XMLǃn268_KRǃIMAGE
		getChild(name: 'n268_TH'): Main_XMLǃn268_THǃIMAGE
		getChildAt(index: 44): Main_XMLǃn268_THǃIMAGE
		getChildById(id: 'n268_u4wi_TH'): Main_XMLǃn268_THǃIMAGE
		getChild(name: 'n268_VN'): Main_XMLǃn268_VNǃIMAGE
		getChildAt(index: 45): Main_XMLǃn268_VNǃIMAGE
		getChildById(id: 'n268_u4wi_VN'): Main_XMLǃn268_VNǃIMAGE
		getChild(name: 'n268_CN2'): Main_XMLǃn268_CN2ǃIMAGE
		getChildAt(index: 46): Main_XMLǃn268_CN2ǃIMAGE
		getChildById(id: 'n268_u4wi_CN2'): Main_XMLǃn268_CN2ǃIMAGE
		getChild(name: 'n268'): Main_XMLǃn268ǃIMAGE
		getChildAt(index: 47): Main_XMLǃn268ǃIMAGE
		getChildById(id: 'n268_u4wi'): Main_XMLǃn268ǃIMAGE
		getChild(name: 'n269_EN'): Main_XMLǃn269_ENǃIMAGE
		getChildAt(index: 48): Main_XMLǃn269_ENǃIMAGE
		getChildById(id: 'n269_u4wi_EN'): Main_XMLǃn269_ENǃIMAGE
		getChild(name: 'n269_IN'): Main_XMLǃn269_INǃIMAGE
		getChildAt(index: 49): Main_XMLǃn269_INǃIMAGE
		getChildById(id: 'n269_u4wi_IN'): Main_XMLǃn269_INǃIMAGE
		getChild(name: 'n269_JP'): Main_XMLǃn269_JPǃIMAGE
		getChildAt(index: 50): Main_XMLǃn269_JPǃIMAGE
		getChildById(id: 'n269_u4wi_JP'): Main_XMLǃn269_JPǃIMAGE
		getChild(name: 'n269_KR'): Main_XMLǃn269_KRǃIMAGE
		getChildAt(index: 51): Main_XMLǃn269_KRǃIMAGE
		getChildById(id: 'n269_u4wi_KR'): Main_XMLǃn269_KRǃIMAGE
		getChild(name: 'n269_TH'): Main_XMLǃn269_THǃIMAGE
		getChildAt(index: 52): Main_XMLǃn269_THǃIMAGE
		getChildById(id: 'n269_u4wi_TH'): Main_XMLǃn269_THǃIMAGE
		getChild(name: 'n269_VN'): Main_XMLǃn269_VNǃIMAGE
		getChildAt(index: 53): Main_XMLǃn269_VNǃIMAGE
		getChildById(id: 'n269_u4wi_VN'): Main_XMLǃn269_VNǃIMAGE
		getChild(name: 'n269_CN2'): Main_XMLǃn269_CN2ǃIMAGE
		getChildAt(index: 54): Main_XMLǃn269_CN2ǃIMAGE
		getChildById(id: 'n269_u4wi_CN2'): Main_XMLǃn269_CN2ǃIMAGE
		getChild(name: 'n269'): Main_XMLǃn269ǃIMAGE
		getChildAt(index: 55): Main_XMLǃn269ǃIMAGE
		getChildById(id: 'n269_u4wi'): Main_XMLǃn269ǃIMAGE
		getChild(name: 'n270_EN'): Main_XMLǃn270_ENǃIMAGE
		getChildAt(index: 56): Main_XMLǃn270_ENǃIMAGE
		getChildById(id: 'n270_u4wi_EN'): Main_XMLǃn270_ENǃIMAGE
		getChild(name: 'n270_IN'): Main_XMLǃn270_INǃIMAGE
		getChildAt(index: 57): Main_XMLǃn270_INǃIMAGE
		getChildById(id: 'n270_u4wi_IN'): Main_XMLǃn270_INǃIMAGE
		getChild(name: 'n270_JP'): Main_XMLǃn270_JPǃIMAGE
		getChildAt(index: 58): Main_XMLǃn270_JPǃIMAGE
		getChildById(id: 'n270_u4wi_JP'): Main_XMLǃn270_JPǃIMAGE
		getChild(name: 'n270_KR'): Main_XMLǃn270_KRǃIMAGE
		getChildAt(index: 59): Main_XMLǃn270_KRǃIMAGE
		getChildById(id: 'n270_u4wi_KR'): Main_XMLǃn270_KRǃIMAGE
		getChild(name: 'n270_TH'): Main_XMLǃn270_THǃIMAGE
		getChildAt(index: 60): Main_XMLǃn270_THǃIMAGE
		getChildById(id: 'n270_u4wi_TH'): Main_XMLǃn270_THǃIMAGE
		getChild(name: 'n270_VN'): Main_XMLǃn270_VNǃIMAGE
		getChildAt(index: 61): Main_XMLǃn270_VNǃIMAGE
		getChildById(id: 'n270_u4wi_VN'): Main_XMLǃn270_VNǃIMAGE
		getChild(name: 'n270_CN2'): Main_XMLǃn270_CN2ǃIMAGE
		getChildAt(index: 62): Main_XMLǃn270_CN2ǃIMAGE
		getChildById(id: 'n270_u4wi_CN2'): Main_XMLǃn270_CN2ǃIMAGE
		getChild(name: 'n270'): Main_XMLǃn270ǃIMAGE
		getChildAt(index: 63): Main_XMLǃn270ǃIMAGE
		getChildById(id: 'n270_u4wi'): Main_XMLǃn270ǃIMAGE
		getChild(name: 'n271_EN'): Main_XMLǃn271_ENǃIMAGE
		getChildAt(index: 64): Main_XMLǃn271_ENǃIMAGE
		getChildById(id: 'n271_u4wi_EN'): Main_XMLǃn271_ENǃIMAGE
		getChild(name: 'n271_IN'): Main_XMLǃn271_INǃIMAGE
		getChildAt(index: 65): Main_XMLǃn271_INǃIMAGE
		getChildById(id: 'n271_u4wi_IN'): Main_XMLǃn271_INǃIMAGE
		getChild(name: 'n271_JP'): Main_XMLǃn271_JPǃIMAGE
		getChildAt(index: 66): Main_XMLǃn271_JPǃIMAGE
		getChildById(id: 'n271_u4wi_JP'): Main_XMLǃn271_JPǃIMAGE
		getChild(name: 'n271_KR'): Main_XMLǃn271_KRǃIMAGE
		getChildAt(index: 67): Main_XMLǃn271_KRǃIMAGE
		getChildById(id: 'n271_u4wi_KR'): Main_XMLǃn271_KRǃIMAGE
		getChild(name: 'n271_TH'): Main_XMLǃn271_THǃIMAGE
		getChildAt(index: 68): Main_XMLǃn271_THǃIMAGE
		getChildById(id: 'n271_u4wi_TH'): Main_XMLǃn271_THǃIMAGE
		getChild(name: 'n271_VN'): Main_XMLǃn271_VNǃIMAGE
		getChildAt(index: 69): Main_XMLǃn271_VNǃIMAGE
		getChildById(id: 'n271_u4wi_VN'): Main_XMLǃn271_VNǃIMAGE
		getChild(name: 'n271_CN2'): Main_XMLǃn271_CN2ǃIMAGE
		getChildAt(index: 70): Main_XMLǃn271_CN2ǃIMAGE
		getChildById(id: 'n271_u4wi_CN2'): Main_XMLǃn271_CN2ǃIMAGE
		getChild(name: 'n271'): Main_XMLǃn271ǃIMAGE
		getChildAt(index: 71): Main_XMLǃn271ǃIMAGE
		getChildById(id: 'n271_u4wi'): Main_XMLǃn271ǃIMAGE
		getChild(name: 'n272_EN'): Main_XMLǃn272_ENǃIMAGE
		getChildAt(index: 72): Main_XMLǃn272_ENǃIMAGE
		getChildById(id: 'n272_u4wi_EN'): Main_XMLǃn272_ENǃIMAGE
		getChild(name: 'n272_IN'): Main_XMLǃn272_INǃIMAGE
		getChildAt(index: 73): Main_XMLǃn272_INǃIMAGE
		getChildById(id: 'n272_u4wi_IN'): Main_XMLǃn272_INǃIMAGE
		getChild(name: 'n272_JP'): Main_XMLǃn272_JPǃIMAGE
		getChildAt(index: 74): Main_XMLǃn272_JPǃIMAGE
		getChildById(id: 'n272_u4wi_JP'): Main_XMLǃn272_JPǃIMAGE
		getChild(name: 'n272_KR'): Main_XMLǃn272_KRǃIMAGE
		getChildAt(index: 75): Main_XMLǃn272_KRǃIMAGE
		getChildById(id: 'n272_u4wi_KR'): Main_XMLǃn272_KRǃIMAGE
		getChild(name: 'n272_TH'): Main_XMLǃn272_THǃIMAGE
		getChildAt(index: 76): Main_XMLǃn272_THǃIMAGE
		getChildById(id: 'n272_u4wi_TH'): Main_XMLǃn272_THǃIMAGE
		getChild(name: 'n272_VN'): Main_XMLǃn272_VNǃIMAGE
		getChildAt(index: 77): Main_XMLǃn272_VNǃIMAGE
		getChildById(id: 'n272_u4wi_VN'): Main_XMLǃn272_VNǃIMAGE
		getChild(name: 'n272_CN2'): Main_XMLǃn272_CN2ǃIMAGE
		getChildAt(index: 78): Main_XMLǃn272_CN2ǃIMAGE
		getChildById(id: 'n272_u4wi_CN2'): Main_XMLǃn272_CN2ǃIMAGE
		getChild(name: 'n272'): Main_XMLǃn272ǃIMAGE
		getChildAt(index: 79): Main_XMLǃn272ǃIMAGE
		getChildById(id: 'n272_u4wi'): Main_XMLǃn272ǃIMAGE
		getChild(name: 'chouma'): otherǁchouma_XMLǃchoumaǃCOMPONENT
		getChildAt(index: 80): otherǁchouma_XMLǃchoumaǃCOMPONENT
		getChildById(id: 'n5_da1b'): otherǁchouma_XMLǃchoumaǃCOMPONENT
		getChild(name: 'button_chexiao'): buttonsǁgongnengǁbutton_chexiao_XMLǃbutton_chexiaoǃCOMPONENT
		getChildAt(index: 81): buttonsǁgongnengǁbutton_chexiao_XMLǃbutton_chexiaoǃCOMPONENT
		getChildById(id: 'n22_da1b'): buttonsǁgongnengǁbutton_chexiao_XMLǃbutton_chexiaoǃCOMPONENT
		getChild(name: 'button_quanqing'): buttonsǁgongnengǁbutton_quanxing_XMLǃbutton_quanqingǃCOMPONENT
		getChildAt(index: 82): buttonsǁgongnengǁbutton_quanxing_XMLǃbutton_quanqingǃCOMPONENT
		getChildById(id: 'n23_da1b'): buttonsǁgongnengǁbutton_quanxing_XMLǃbutton_quanqingǃCOMPONENT
		getChild(name: 'button_chongfutouzhu'): buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃbutton_chongfutouzhuǃCOMPONENT
		getChildAt(index: 83): buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃbutton_chongfutouzhuǃCOMPONENT
		getChildById(id: 'n214_h3lk'): buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃbutton_chongfutouzhuǃCOMPONENT
		getChild(name: 'zhuangbu'): Main_XMLǃzhuangbuǃIMAGE
		getChildAt(index: 84): Main_XMLǃzhuangbuǃIMAGE
		getChildById(id: 'n207_tztn'): Main_XMLǃzhuangbuǃIMAGE
		getChild(name: 'xianbu_CN2'): Main_XMLǃxianbu_CN2ǃIMAGE
		getChildAt(index: 85): Main_XMLǃxianbu_CN2ǃIMAGE
		getChildById(id: 'n284_ej3n'): Main_XMLǃxianbu_CN2ǃIMAGE
		getChild(name: 'xianbu_EN'): Main_XMLǃxianbu_ENǃIMAGE
		getChildAt(index: 86): Main_XMLǃxianbu_ENǃIMAGE
		getChildById(id: 'n285_ej3n'): Main_XMLǃxianbu_ENǃIMAGE
		getChild(name: 'xianbu_IN'): Main_XMLǃxianbu_INǃIMAGE
		getChildAt(index: 87): Main_XMLǃxianbu_INǃIMAGE
		getChildById(id: 'n286_ej3n'): Main_XMLǃxianbu_INǃIMAGE
		getChild(name: 'xianbu_JP'): Main_XMLǃxianbu_JPǃIMAGE
		getChildAt(index: 88): Main_XMLǃxianbu_JPǃIMAGE
		getChildById(id: 'n287_ej3n'): Main_XMLǃxianbu_JPǃIMAGE
		getChild(name: 'xianbu_KR'): Main_XMLǃxianbu_KRǃIMAGE
		getChildAt(index: 89): Main_XMLǃxianbu_KRǃIMAGE
		getChildById(id: 'n288_ej3n'): Main_XMLǃxianbu_KRǃIMAGE
		getChild(name: 'xianbu_TH'): Main_XMLǃxianbu_THǃIMAGE
		getChildAt(index: 90): Main_XMLǃxianbu_THǃIMAGE
		getChildById(id: 'n289_ej3n'): Main_XMLǃxianbu_THǃIMAGE
		getChild(name: 'xianbu_VN'): Main_XMLǃxianbu_VNǃIMAGE
		getChildAt(index: 91): Main_XMLǃxianbu_VNǃIMAGE
		getChildById(id: 'n290_ej3n'): Main_XMLǃxianbu_VNǃIMAGE
		getChild(name: 'xianbu'): Main_XMLǃxianbuǃIMAGE
		getChildAt(index: 92): Main_XMLǃxianbuǃIMAGE
		getChildById(id: 'n206_tztn'): Main_XMLǃxianbuǃIMAGE
		getChild(name: 'fapai_08'): cradsǁpaidi_XMLǃfapai_08ǃCOMPONENT
		getChildAt(index: 93): cradsǁpaidi_XMLǃfapai_08ǃCOMPONENT
		getChildById(id: 'n95_lgg0'): cradsǁpaidi_XMLǃfapai_08ǃCOMPONENT
		getChild(name: 'fapai_07'): cradsǁpaidi_XMLǃfapai_07ǃCOMPONENT
		getChildAt(index: 94): cradsǁpaidi_XMLǃfapai_07ǃCOMPONENT
		getChildById(id: 'n94_lgg0'): cradsǁpaidi_XMLǃfapai_07ǃCOMPONENT
		getChild(name: 'fapai_06'): cradsǁpaidi2_XMLǃfapai_06ǃCOMPONENT
		getChildAt(index: 95): cradsǁpaidi2_XMLǃfapai_06ǃCOMPONENT
		getChildById(id: 'n93_lgg0'): cradsǁpaidi2_XMLǃfapai_06ǃCOMPONENT
		getChild(name: 'fapai_05'): cradsǁpaidi2_XMLǃfapai_05ǃCOMPONENT
		getChildAt(index: 96): cradsǁpaidi2_XMLǃfapai_05ǃCOMPONENT
		getChildById(id: 'n92_lgg0'): cradsǁpaidi2_XMLǃfapai_05ǃCOMPONENT
		getChild(name: 'fapai_04'): cradsǁpaidi4_XMLǃfapai_04ǃCOMPONENT
		getChildAt(index: 97): cradsǁpaidi4_XMLǃfapai_04ǃCOMPONENT
		getChildById(id: 'n91_lgg0'): cradsǁpaidi4_XMLǃfapai_04ǃCOMPONENT
		getChild(name: 'fapai_03'): cradsǁpaidi1_XMLǃfapai_03ǃCOMPONENT
		getChildAt(index: 98): cradsǁpaidi1_XMLǃfapai_03ǃCOMPONENT
		getChildById(id: 'n90_lgg0'): cradsǁpaidi1_XMLǃfapai_03ǃCOMPONENT
		getChild(name: 'fapai_02'): cradsǁpaidi3_XMLǃfapai_02ǃCOMPONENT
		getChildAt(index: 99): cradsǁpaidi3_XMLǃfapai_02ǃCOMPONENT
		getChildById(id: 'n88_lgg0'): cradsǁpaidi3_XMLǃfapai_02ǃCOMPONENT
		getChild(name: 'fapai_01'): cradsǁpaidi_XMLǃfapai_01ǃCOMPONENT
		getChildAt(index: 100): cradsǁpaidi_XMLǃfapai_01ǃCOMPONENT
		getChildById(id: 'n89_lgg0'): cradsǁpaidi_XMLǃfapai_01ǃCOMPONENT
		getChild(name: 'ani_xipai'): Main_XMLǃani_xipaiǃMOVIECLIP
		getChildAt(index: 101): Main_XMLǃani_xipaiǃMOVIECLIP
		getChildById(id: 'n98_f7j0'): Main_XMLǃani_xipaiǃMOVIECLIP
		getChild(name: 'text_center_CN2'): Main_XMLǃtext_center_CN2ǃTEXT
		getChildAt(index: 102): Main_XMLǃtext_center_CN2ǃTEXT
		getChildById(id: 'n96_lgg0_CN2'): Main_XMLǃtext_center_CN2ǃTEXT
		getChild(name: 'text_center_EN'): Main_XMLǃtext_center_ENǃTEXT
		getChildAt(index: 103): Main_XMLǃtext_center_ENǃTEXT
		getChildById(id: 'n96_lgg0_EN'): Main_XMLǃtext_center_ENǃTEXT
		getChild(name: 'text_center_IN'): Main_XMLǃtext_center_INǃTEXT
		getChildAt(index: 104): Main_XMLǃtext_center_INǃTEXT
		getChildById(id: 'n96_lgg0_IN'): Main_XMLǃtext_center_INǃTEXT
		getChild(name: 'text_center_JP'): Main_XMLǃtext_center_JPǃTEXT
		getChildAt(index: 105): Main_XMLǃtext_center_JPǃTEXT
		getChildById(id: 'n96_lgg0_JP'): Main_XMLǃtext_center_JPǃTEXT
		getChild(name: 'text_center_KR'): Main_XMLǃtext_center_KRǃTEXT
		getChildAt(index: 106): Main_XMLǃtext_center_KRǃTEXT
		getChildById(id: 'n96_lgg0_KR'): Main_XMLǃtext_center_KRǃTEXT
		getChild(name: 'text_center_TH'): Main_XMLǃtext_center_THǃTEXT
		getChildAt(index: 107): Main_XMLǃtext_center_THǃTEXT
		getChildById(id: 'n96_lgg0_TH'): Main_XMLǃtext_center_THǃTEXT
		getChild(name: 'text_center_VN'): Main_XMLǃtext_center_VNǃTEXT
		getChildAt(index: 108): Main_XMLǃtext_center_VNǃTEXT
		getChildById(id: 'n96_lgg0_VN'): Main_XMLǃtext_center_VNǃTEXT
		getChild(name: 'text_center'): Main_XMLǃtext_centerǃTEXT
		getChildAt(index: 109): Main_XMLǃtext_centerǃTEXT
		getChildById(id: 'n96_lgg0'): Main_XMLǃtext_centerǃTEXT
		getChild(name: 'btn_menu'): buttonsǁgongnengǁbtn_menu_XMLǃbtn_menuǃCOMPONENT
		getChildAt(index: 110): buttonsǁgongnengǁbtn_menu_XMLǃbtn_menuǃCOMPONENT
		getChildById(id: 'n254_rad3'): buttonsǁgongnengǁbtn_menu_XMLǃbtn_menuǃCOMPONENT
		getChild(name: 'text_qishu'): Main_XMLǃtext_qishuǃRICHTEXT
		getChildAt(index: 111): Main_XMLǃtext_qishuǃRICHTEXT
		getChildById(id: 'n259_cq3n'): Main_XMLǃtext_qishuǃRICHTEXT
		getChild(name: 'text_countdown'): Main_XMLǃtext_countdownǃTEXT
		getChildAt(index: 112): Main_XMLǃtext_countdownǃTEXT
		getChildById(id: 'n189_f95u'): Main_XMLǃtext_countdownǃTEXT
		getChild(name: 'ping'): pingInfo_XMLǃpingǃCOMPONENT
		getChildAt(index: 113): pingInfo_XMLǃpingǃCOMPONENT
		getChildById(id: 'n248_jtlp'): pingInfo_XMLǃpingǃCOMPONENT
		getChild(name: 't112_CN2'): Main_XMLǃt112_CN2ǃTEXT
		getChildAt(index: 114): Main_XMLǃt112_CN2ǃTEXT
		getChildById(id: 'n249_jtlp_CN2'): Main_XMLǃt112_CN2ǃTEXT
		getChild(name: 't112_EN'): Main_XMLǃt112_ENǃTEXT
		getChildAt(index: 115): Main_XMLǃt112_ENǃTEXT
		getChildById(id: 'n249_jtlp_EN'): Main_XMLǃt112_ENǃTEXT
		getChild(name: 't112_IN'): Main_XMLǃt112_INǃTEXT
		getChildAt(index: 116): Main_XMLǃt112_INǃTEXT
		getChildById(id: 'n249_jtlp_IN'): Main_XMLǃt112_INǃTEXT
		getChild(name: 't112_JP'): Main_XMLǃt112_JPǃTEXT
		getChildAt(index: 117): Main_XMLǃt112_JPǃTEXT
		getChildById(id: 'n249_jtlp_JP'): Main_XMLǃt112_JPǃTEXT
		getChild(name: 't112_KR'): Main_XMLǃt112_KRǃTEXT
		getChildAt(index: 118): Main_XMLǃt112_KRǃTEXT
		getChildById(id: 'n249_jtlp_KR'): Main_XMLǃt112_KRǃTEXT
		getChild(name: 't112_TH'): Main_XMLǃt112_THǃTEXT
		getChildAt(index: 119): Main_XMLǃt112_THǃTEXT
		getChildById(id: 'n249_jtlp_TH'): Main_XMLǃt112_THǃTEXT
		getChild(name: 't112_VN'): Main_XMLǃt112_VNǃTEXT
		getChildAt(index: 120): Main_XMLǃt112_VNǃTEXT
		getChildById(id: 'n249_jtlp_VN'): Main_XMLǃt112_VNǃTEXT
		getChild(name: 't112'): Main_XMLǃt112ǃTEXT
		getChildAt(index: 121): Main_XMLǃt112ǃTEXT
		getChildById(id: 'n249_jtlp'): Main_XMLǃt112ǃTEXT
		getChild(name: 'n113_CN2'): Main_XMLǃn113_CN2ǃTEXT
		getChildAt(index: 122): Main_XMLǃn113_CN2ǃTEXT
		getChildById(id: 'n250_jtlp_CN2'): Main_XMLǃn113_CN2ǃTEXT
		getChild(name: 'n113_EN'): Main_XMLǃn113_ENǃTEXT
		getChildAt(index: 123): Main_XMLǃn113_ENǃTEXT
		getChildById(id: 'n250_jtlp_EN'): Main_XMLǃn113_ENǃTEXT
		getChild(name: 'n113_IN'): Main_XMLǃn113_INǃTEXT
		getChildAt(index: 124): Main_XMLǃn113_INǃTEXT
		getChildById(id: 'n250_jtlp_IN'): Main_XMLǃn113_INǃTEXT
		getChild(name: 'n113_JP'): Main_XMLǃn113_JPǃTEXT
		getChildAt(index: 125): Main_XMLǃn113_JPǃTEXT
		getChildById(id: 'n250_jtlp_JP'): Main_XMLǃn113_JPǃTEXT
		getChild(name: 'n113_KR'): Main_XMLǃn113_KRǃTEXT
		getChildAt(index: 126): Main_XMLǃn113_KRǃTEXT
		getChildById(id: 'n250_jtlp_KR'): Main_XMLǃn113_KRǃTEXT
		getChild(name: 'n113_TH'): Main_XMLǃn113_THǃTEXT
		getChildAt(index: 127): Main_XMLǃn113_THǃTEXT
		getChildById(id: 'n250_jtlp_TH'): Main_XMLǃn113_THǃTEXT
		getChild(name: 'n113_VN'): Main_XMLǃn113_VNǃTEXT
		getChildAt(index: 128): Main_XMLǃn113_VNǃTEXT
		getChildById(id: 'n250_jtlp_VN'): Main_XMLǃn113_VNǃTEXT
		getChild(name: 'n113'): Main_XMLǃn113ǃTEXT
		getChildAt(index: 129): Main_XMLǃn113ǃTEXT
		getChildById(id: 'n250_jtlp'): Main_XMLǃn113ǃTEXT
		getChild(name: 'text_renshu'): Main_XMLǃtext_renshuǃTEXT
		getChildAt(index: 130): Main_XMLǃtext_renshuǃTEXT
		getChildById(id: 'n187_e1vg'): Main_XMLǃtext_renshuǃTEXT
		getChild(name: 'button_querentouzhu'): buttonsǁgongnengǁbutton_touzhu_XMLǃbutton_querentouzhuǃCOMPONENT
		getChildAt(index: 131): buttonsǁgongnengǁbutton_touzhu_XMLǃbutton_querentouzhuǃCOMPONENT
		getChildById(id: 'n24_da1b'): buttonsǁgongnengǁbutton_touzhu_XMLǃbutton_querentouzhuǃCOMPONENT
		getChild(name: 'xinxi_da'): xiazhuxinxiǁxinxi_moban1_XMLǃxinxi_daǃCOMPONENT
		getChildAt(index: 132): xiazhuxinxiǁxinxi_moban1_XMLǃxinxi_daǃCOMPONENT
		getChildById(id: 'n45_x8xm'): xiazhuxinxiǁxinxi_moban1_XMLǃxinxi_daǃCOMPONENT
		getChild(name: 'xinxi_renyiduizi'): xiazhuxinxiǁxinxi_moban1_XMLǃxinxi_renyiduiziǃCOMPONENT
		getChildAt(index: 133): xiazhuxinxiǁxinxi_moban1_XMLǃxinxi_renyiduiziǃCOMPONENT
		getChildById(id: 'n115_o1w7'): xiazhuxinxiǁxinxi_moban1_XMLǃxinxi_renyiduiziǃCOMPONENT
		getChild(name: 'xinxi_xiandui'): xiazhuxinxiǁxinxi_moban1_XMLǃxinxi_xianduiǃCOMPONENT
		getChildAt(index: 134): xiazhuxinxiǁxinxi_moban1_XMLǃxinxi_xianduiǃCOMPONENT
		getChildById(id: 'n116_o1w7'): xiazhuxinxiǁxinxi_moban1_XMLǃxinxi_xianduiǃCOMPONENT
		getChild(name: 'xinxi_xiao'): xiazhuxinxiǁxinxi_moban1_XMLǃxinxi_xiaoǃCOMPONENT
		getChildAt(index: 135): xiazhuxinxiǁxinxi_moban1_XMLǃxinxi_xiaoǃCOMPONENT
		getChildById(id: 'n117_o1w7'): xiazhuxinxiǁxinxi_moban1_XMLǃxinxi_xiaoǃCOMPONENT
		getChild(name: 'xinxi_zhuangdui'): xiazhuxinxiǁxinxi_moban1_XMLǃxinxi_zhuangduiǃCOMPONENT
		getChildAt(index: 136): xiazhuxinxiǁxinxi_moban1_XMLǃxinxi_zhuangduiǃCOMPONENT
		getChildById(id: 'n118_o1w7'): xiazhuxinxiǁxinxi_moban1_XMLǃxinxi_zhuangduiǃCOMPONENT
		getChild(name: 'xinxi_wanmeiduizi'): xiazhuxinxiǁxinxi_moban1_XMLǃxinxi_wanmeiduiziǃCOMPONENT
		getChildAt(index: 137): xiazhuxinxiǁxinxi_moban1_XMLǃxinxi_wanmeiduiziǃCOMPONENT
		getChildById(id: 'n119_o1w7'): xiazhuxinxiǁxinxi_moban1_XMLǃxinxi_wanmeiduiziǃCOMPONENT
		getChild(name: 'xinxi_xian'): xiazhuxinxiǁxinxi_moban2_XMLǃxinxi_xianǃCOMPONENT
		getChildAt(index: 138): xiazhuxinxiǁxinxi_moban2_XMLǃxinxi_xianǃCOMPONENT
		getChildById(id: 'n123_o1w7'): xiazhuxinxiǁxinxi_moban2_XMLǃxinxi_xianǃCOMPONENT
		getChild(name: 'xinxi_he'): xiazhuxinxiǁxinxi_moban2_XMLǃxinxi_heǃCOMPONENT
		getChildAt(index: 139): xiazhuxinxiǁxinxi_moban2_XMLǃxinxi_heǃCOMPONENT
		getChildById(id: 'n124_o1w7'): xiazhuxinxiǁxinxi_moban2_XMLǃxinxi_heǃCOMPONENT
		getChild(name: 'xinxi_zhuang'): xiazhuxinxiǁxinxi_moban2_XMLǃxinxi_zhuangǃCOMPONENT
		getChildAt(index: 140): xiazhuxinxiǁxinxi_moban2_XMLǃxinxi_zhuangǃCOMPONENT
		getChildById(id: 'n125_o1w7'): xiazhuxinxiǁxinxi_moban2_XMLǃxinxi_zhuangǃCOMPONENT
		getChild(name: 'btn_help_renyiduizi'): buttonsǁgongnengǁbutton_wenhao_XMLǃbtn_help_renyiduiziǃCOMPONENT
		getChildAt(index: 141): buttonsǁgongnengǁbutton_wenhao_XMLǃbtn_help_renyiduiziǃCOMPONENT
		getChildById(id: 'n224_h3lk'): buttonsǁgongnengǁbutton_wenhao_XMLǃbtn_help_renyiduiziǃCOMPONENT
		getChild(name: 'btn_help_xiandui'): buttonsǁgongnengǁbutton_wenhao_XMLǃbtn_help_xianduiǃCOMPONENT
		getChildAt(index: 142): buttonsǁgongnengǁbutton_wenhao_XMLǃbtn_help_xianduiǃCOMPONENT
		getChildById(id: 'n223_h3lk'): buttonsǁgongnengǁbutton_wenhao_XMLǃbtn_help_xianduiǃCOMPONENT
		getChild(name: 'btn_help_da'): buttonsǁgongnengǁbutton_wenhao_XMLǃbtn_help_daǃCOMPONENT
		getChildAt(index: 143): buttonsǁgongnengǁbutton_wenhao_XMLǃbtn_help_daǃCOMPONENT
		getChildById(id: 'n217_h3lk'): buttonsǁgongnengǁbutton_wenhao_XMLǃbtn_help_daǃCOMPONENT
		getChild(name: 'btn_help_xiao'): buttonsǁgongnengǁbutton_wenhao_XMLǃbtn_help_xiaoǃCOMPONENT
		getChildAt(index: 144): buttonsǁgongnengǁbutton_wenhao_XMLǃbtn_help_xiaoǃCOMPONENT
		getChildById(id: 'n218_h3lk'): buttonsǁgongnengǁbutton_wenhao_XMLǃbtn_help_xiaoǃCOMPONENT
		getChild(name: 'btn_help_zhuangdui'): buttonsǁgongnengǁbutton_wenhao_XMLǃbtn_help_zhuangduiǃCOMPONENT
		getChildAt(index: 145): buttonsǁgongnengǁbutton_wenhao_XMLǃbtn_help_zhuangduiǃCOMPONENT
		getChildById(id: 'n222_h3lk'): buttonsǁgongnengǁbutton_wenhao_XMLǃbtn_help_zhuangduiǃCOMPONENT
		getChild(name: 'btn_help_wanmeiduizi'): buttonsǁgongnengǁbutton_wenhao_XMLǃbtn_help_wanmeiduiziǃCOMPONENT
		getChildAt(index: 146): buttonsǁgongnengǁbutton_wenhao_XMLǃbtn_help_wanmeiduiziǃCOMPONENT
		getChildById(id: 'n225_h3lk'): buttonsǁgongnengǁbutton_wenhao_XMLǃbtn_help_wanmeiduiziǃCOMPONENT
		getChild(name: 'table_xian'): otherǁtable_chouma_XMLǃtable_xianǃCOMPONENT
		getChildAt(index: 147): otherǁtable_chouma_XMLǃtable_xianǃCOMPONENT
		getChildById(id: 'n164_s6qw'): otherǁtable_chouma_XMLǃtable_xianǃCOMPONENT
		getChild(name: 'table_he'): otherǁtable_chouma_XMLǃtable_heǃCOMPONENT
		getChildAt(index: 148): otherǁtable_chouma_XMLǃtable_heǃCOMPONENT
		getChildById(id: 'n165_s6qw'): otherǁtable_chouma_XMLǃtable_heǃCOMPONENT
		getChild(name: 'table_zhuang'): otherǁtable_chouma_XMLǃtable_zhuangǃCOMPONENT
		getChildAt(index: 149): otherǁtable_chouma_XMLǃtable_zhuangǃCOMPONENT
		getChildById(id: 'n166_s6qw'): otherǁtable_chouma_XMLǃtable_zhuangǃCOMPONENT
		getChild(name: 'table_renyiduizi'): otherǁtable_chouma2_XMLǃtable_renyiduiziǃCOMPONENT
		getChildAt(index: 150): otherǁtable_chouma2_XMLǃtable_renyiduiziǃCOMPONENT
		getChildById(id: 'n190_uiyu'): otherǁtable_chouma2_XMLǃtable_renyiduiziǃCOMPONENT
		getChild(name: 'table_xiandui'): otherǁtable_chouma2_XMLǃtable_xianduiǃCOMPONENT
		getChildAt(index: 151): otherǁtable_chouma2_XMLǃtable_xianduiǃCOMPONENT
		getChildById(id: 'n191_uiyu'): otherǁtable_chouma2_XMLǃtable_xianduiǃCOMPONENT
		getChild(name: 'table_da'): otherǁtable_chouma2_XMLǃtable_daǃCOMPONENT
		getChildAt(index: 152): otherǁtable_chouma2_XMLǃtable_daǃCOMPONENT
		getChildById(id: 'n192_uiyu'): otherǁtable_chouma2_XMLǃtable_daǃCOMPONENT
		getChild(name: 'table_xiao'): otherǁtable_chouma2_XMLǃtable_xiaoǃCOMPONENT
		getChildAt(index: 153): otherǁtable_chouma2_XMLǃtable_xiaoǃCOMPONENT
		getChildById(id: 'n193_uiyu'): otherǁtable_chouma2_XMLǃtable_xiaoǃCOMPONENT
		getChild(name: 'table_zhuangdui'): otherǁtable_chouma2_XMLǃtable_zhuangduiǃCOMPONENT
		getChildAt(index: 154): otherǁtable_chouma2_XMLǃtable_zhuangduiǃCOMPONENT
		getChildById(id: 'n194_uiyu'): otherǁtable_chouma2_XMLǃtable_zhuangduiǃCOMPONENT
		getChild(name: 'table_wanmeiduizi'): otherǁtable_chouma2_XMLǃtable_wanmeiduiziǃCOMPONENT
		getChildAt(index: 155): otherǁtable_chouma2_XMLǃtable_wanmeiduiziǃCOMPONENT
		getChildById(id: 'n195_uiyu'): otherǁtable_chouma2_XMLǃtable_wanmeiduiziǃCOMPONENT
		getChild(name: 'playerchouma_xian_1'): otherǁchouma_common_XMLǃplayerchouma_xian_1ǃCOMPONENT
		getChildAt(index: 156): otherǁchouma_common_XMLǃplayerchouma_xian_1ǃCOMPONENT
		getChildById(id: 'n155_s6qw'): otherǁchouma_common_XMLǃplayerchouma_xian_1ǃCOMPONENT
		getChild(name: 'playerchouma_xian_2'): otherǁchouma_common_XMLǃplayerchouma_xian_2ǃCOMPONENT
		getChildAt(index: 157): otherǁchouma_common_XMLǃplayerchouma_xian_2ǃCOMPONENT
		getChildById(id: 'n167_s6qw'): otherǁchouma_common_XMLǃplayerchouma_xian_2ǃCOMPONENT
		getChild(name: 'playerchouma_xian_3'): otherǁchouma_common_XMLǃplayerchouma_xian_3ǃCOMPONENT
		getChildAt(index: 158): otherǁchouma_common_XMLǃplayerchouma_xian_3ǃCOMPONENT
		getChildById(id: 'n168_s6qw'): otherǁchouma_common_XMLǃplayerchouma_xian_3ǃCOMPONENT
		getChild(name: 'playerchouma_xian_4'): otherǁchouma_common_XMLǃplayerchouma_xian_4ǃCOMPONENT
		getChildAt(index: 159): otherǁchouma_common_XMLǃplayerchouma_xian_4ǃCOMPONENT
		getChildById(id: 'n169_s6qw'): otherǁchouma_common_XMLǃplayerchouma_xian_4ǃCOMPONENT
		getChild(name: 'playerchouma_xian_5'): otherǁchouma_common_XMLǃplayerchouma_xian_5ǃCOMPONENT
		getChildAt(index: 160): otherǁchouma_common_XMLǃplayerchouma_xian_5ǃCOMPONENT
		getChildById(id: 'n170_s6qw'): otherǁchouma_common_XMLǃplayerchouma_xian_5ǃCOMPONENT
		getChild(name: 'playerchouma_xian_6'): otherǁchouma_common_XMLǃplayerchouma_xian_6ǃCOMPONENT
		getChildAt(index: 161): otherǁchouma_common_XMLǃplayerchouma_xian_6ǃCOMPONENT
		getChildById(id: 'n171_s6qw'): otherǁchouma_common_XMLǃplayerchouma_xian_6ǃCOMPONENT
		getChild(name: 'playerchouma_da_1'): otherǁchouma_common_XMLǃplayerchouma_da_1ǃCOMPONENT
		getChildAt(index: 162): otherǁchouma_common_XMLǃplayerchouma_da_1ǃCOMPONENT
		getChildById(id: 'n180_s6qw'): otherǁchouma_common_XMLǃplayerchouma_da_1ǃCOMPONENT
		getChild(name: 'playerchouma_da_2'): otherǁchouma_common_XMLǃplayerchouma_da_2ǃCOMPONENT
		getChildAt(index: 163): otherǁchouma_common_XMLǃplayerchouma_da_2ǃCOMPONENT
		getChildById(id: 'n181_s6qw'): otherǁchouma_common_XMLǃplayerchouma_da_2ǃCOMPONENT
		getChild(name: 'playerchouma_da_3'): otherǁchouma_common_XMLǃplayerchouma_da_3ǃCOMPONENT
		getChildAt(index: 164): otherǁchouma_common_XMLǃplayerchouma_da_3ǃCOMPONENT
		getChildById(id: 'n182_s6qw'): otherǁchouma_common_XMLǃplayerchouma_da_3ǃCOMPONENT
		getChild(name: 'playerchouma_da_4'): otherǁchouma_common_XMLǃplayerchouma_da_4ǃCOMPONENT
		getChildAt(index: 165): otherǁchouma_common_XMLǃplayerchouma_da_4ǃCOMPONENT
		getChildById(id: 'n183_s6qw'): otherǁchouma_common_XMLǃplayerchouma_da_4ǃCOMPONENT
		getChild(name: 'playerchouma_da_5'): otherǁchouma_common_XMLǃplayerchouma_da_5ǃCOMPONENT
		getChildAt(index: 166): otherǁchouma_common_XMLǃplayerchouma_da_5ǃCOMPONENT
		getChildById(id: 'n184_s6qw'): otherǁchouma_common_XMLǃplayerchouma_da_5ǃCOMPONENT
		getChild(name: 'playerchouma_da_6'): otherǁchouma_common_XMLǃplayerchouma_da_6ǃCOMPONENT
		getChildAt(index: 167): otherǁchouma_common_XMLǃplayerchouma_da_6ǃCOMPONENT
		getChildById(id: 'n185_s6qw'): otherǁchouma_common_XMLǃplayerchouma_da_6ǃCOMPONENT
		getChild(name: 'playerchouma_he_1'): otherǁchouma_common_XMLǃplayerchouma_he_1ǃCOMPONENT
		getChildAt(index: 168): otherǁchouma_common_XMLǃplayerchouma_he_1ǃCOMPONENT
		getChildById(id: 'n173_s6qw'): otherǁchouma_common_XMLǃplayerchouma_he_1ǃCOMPONENT
		getChild(name: 'playerchouma_he_2'): otherǁchouma_common_XMLǃplayerchouma_he_2ǃCOMPONENT
		getChildAt(index: 169): otherǁchouma_common_XMLǃplayerchouma_he_2ǃCOMPONENT
		getChildById(id: 'n174_s6qw'): otherǁchouma_common_XMLǃplayerchouma_he_2ǃCOMPONENT
		getChild(name: 'playerchouma_he_3'): otherǁchouma_common_XMLǃplayerchouma_he_3ǃCOMPONENT
		getChildAt(index: 170): otherǁchouma_common_XMLǃplayerchouma_he_3ǃCOMPONENT
		getChildById(id: 'n175_s6qw'): otherǁchouma_common_XMLǃplayerchouma_he_3ǃCOMPONENT
		getChild(name: 'playerchouma_he_4'): otherǁchouma_common_XMLǃplayerchouma_he_4ǃCOMPONENT
		getChildAt(index: 171): otherǁchouma_common_XMLǃplayerchouma_he_4ǃCOMPONENT
		getChildById(id: 'n176_s6qw'): otherǁchouma_common_XMLǃplayerchouma_he_4ǃCOMPONENT
		getChild(name: 'playerchouma_he_5'): otherǁchouma_common_XMLǃplayerchouma_he_5ǃCOMPONENT
		getChildAt(index: 172): otherǁchouma_common_XMLǃplayerchouma_he_5ǃCOMPONENT
		getChildById(id: 'n177_s6qw'): otherǁchouma_common_XMLǃplayerchouma_he_5ǃCOMPONENT
		getChild(name: 'playerchouma_he_6'): otherǁchouma_common_XMLǃplayerchouma_he_6ǃCOMPONENT
		getChildAt(index: 173): otherǁchouma_common_XMLǃplayerchouma_he_6ǃCOMPONENT
		getChildById(id: 'n178_s6qw'): otherǁchouma_common_XMLǃplayerchouma_he_6ǃCOMPONENT
		getChild(name: 'playerchouma_xiao_1'): otherǁchouma_common_XMLǃplayerchouma_xiao_1ǃCOMPONENT
		getChildAt(index: 174): otherǁchouma_common_XMLǃplayerchouma_xiao_1ǃCOMPONENT
		getChildById(id: 'n180_s6qw'): otherǁchouma_common_XMLǃplayerchouma_xiao_1ǃCOMPONENT
		getChild(name: 'playerchouma_xiao_2'): otherǁchouma_common_XMLǃplayerchouma_xiao_2ǃCOMPONENT
		getChildAt(index: 175): otherǁchouma_common_XMLǃplayerchouma_xiao_2ǃCOMPONENT
		getChildById(id: 'n181_s6qw'): otherǁchouma_common_XMLǃplayerchouma_xiao_2ǃCOMPONENT
		getChild(name: 'playerchouma_xiao_3'): otherǁchouma_common_XMLǃplayerchouma_xiao_3ǃCOMPONENT
		getChildAt(index: 176): otherǁchouma_common_XMLǃplayerchouma_xiao_3ǃCOMPONENT
		getChildById(id: 'n182_s6qw'): otherǁchouma_common_XMLǃplayerchouma_xiao_3ǃCOMPONENT
		getChild(name: 'playerchouma_xiao_4'): otherǁchouma_common_XMLǃplayerchouma_xiao_4ǃCOMPONENT
		getChildAt(index: 177): otherǁchouma_common_XMLǃplayerchouma_xiao_4ǃCOMPONENT
		getChildById(id: 'n183_s6qw'): otherǁchouma_common_XMLǃplayerchouma_xiao_4ǃCOMPONENT
		getChild(name: 'playerchouma_xiao_5'): otherǁchouma_common_XMLǃplayerchouma_xiao_5ǃCOMPONENT
		getChildAt(index: 178): otherǁchouma_common_XMLǃplayerchouma_xiao_5ǃCOMPONENT
		getChildById(id: 'n184_s6qw'): otherǁchouma_common_XMLǃplayerchouma_xiao_5ǃCOMPONENT
		getChild(name: 'playerchouma_xiao_6'): otherǁchouma_common_XMLǃplayerchouma_xiao_6ǃCOMPONENT
		getChildAt(index: 179): otherǁchouma_common_XMLǃplayerchouma_xiao_6ǃCOMPONENT
		getChildById(id: 'n185_s6qw'): otherǁchouma_common_XMLǃplayerchouma_xiao_6ǃCOMPONENT
		getChild(name: 'playerchouma_wanmeiduizi_1'): otherǁchouma_common_XMLǃplayerchouma_wanmeiduizi_1ǃCOMPONENT
		getChildAt(index: 180): otherǁchouma_common_XMLǃplayerchouma_wanmeiduizi_1ǃCOMPONENT
		getChildById(id: 'n180_s6qw'): otherǁchouma_common_XMLǃplayerchouma_wanmeiduizi_1ǃCOMPONENT
		getChild(name: 'playerchouma_wanmeiduizi_2'): otherǁchouma_common_XMLǃplayerchouma_wanmeiduizi_2ǃCOMPONENT
		getChildAt(index: 181): otherǁchouma_common_XMLǃplayerchouma_wanmeiduizi_2ǃCOMPONENT
		getChildById(id: 'n181_s6qw'): otherǁchouma_common_XMLǃplayerchouma_wanmeiduizi_2ǃCOMPONENT
		getChild(name: 'playerchouma_wanmeiduizi_3'): otherǁchouma_common_XMLǃplayerchouma_wanmeiduizi_3ǃCOMPONENT
		getChildAt(index: 182): otherǁchouma_common_XMLǃplayerchouma_wanmeiduizi_3ǃCOMPONENT
		getChildById(id: 'n182_s6qw'): otherǁchouma_common_XMLǃplayerchouma_wanmeiduizi_3ǃCOMPONENT
		getChild(name: 'playerchouma_wanmeiduizi_4'): otherǁchouma_common_XMLǃplayerchouma_wanmeiduizi_4ǃCOMPONENT
		getChildAt(index: 183): otherǁchouma_common_XMLǃplayerchouma_wanmeiduizi_4ǃCOMPONENT
		getChildById(id: 'n183_s6qw'): otherǁchouma_common_XMLǃplayerchouma_wanmeiduizi_4ǃCOMPONENT
		getChild(name: 'playerchouma_wanmeiduizi_5'): otherǁchouma_common_XMLǃplayerchouma_wanmeiduizi_5ǃCOMPONENT
		getChildAt(index: 184): otherǁchouma_common_XMLǃplayerchouma_wanmeiduizi_5ǃCOMPONENT
		getChildById(id: 'n184_s6qw'): otherǁchouma_common_XMLǃplayerchouma_wanmeiduizi_5ǃCOMPONENT
		getChild(name: 'playerchouma_wanmeiduizi_6'): otherǁchouma_common_XMLǃplayerchouma_wanmeiduizi_6ǃCOMPONENT
		getChildAt(index: 185): otherǁchouma_common_XMLǃplayerchouma_wanmeiduizi_6ǃCOMPONENT
		getChildById(id: 'n185_s6qw'): otherǁchouma_common_XMLǃplayerchouma_wanmeiduizi_6ǃCOMPONENT
		getChild(name: 'playerchouma_renyiduizi_1'): otherǁchouma_common_XMLǃplayerchouma_renyiduizi_1ǃCOMPONENT
		getChildAt(index: 186): otherǁchouma_common_XMLǃplayerchouma_renyiduizi_1ǃCOMPONENT
		getChildById(id: 'n180_s6qw'): otherǁchouma_common_XMLǃplayerchouma_renyiduizi_1ǃCOMPONENT
		getChild(name: 'playerchouma_renyiduizi_2'): otherǁchouma_common_XMLǃplayerchouma_renyiduizi_2ǃCOMPONENT
		getChildAt(index: 187): otherǁchouma_common_XMLǃplayerchouma_renyiduizi_2ǃCOMPONENT
		getChildById(id: 'n181_s6qw'): otherǁchouma_common_XMLǃplayerchouma_renyiduizi_2ǃCOMPONENT
		getChild(name: 'playerchouma_renyiduizi_3'): otherǁchouma_common_XMLǃplayerchouma_renyiduizi_3ǃCOMPONENT
		getChildAt(index: 188): otherǁchouma_common_XMLǃplayerchouma_renyiduizi_3ǃCOMPONENT
		getChildById(id: 'n182_s6qw'): otherǁchouma_common_XMLǃplayerchouma_renyiduizi_3ǃCOMPONENT
		getChild(name: 'playerchouma_renyiduizi_4'): otherǁchouma_common_XMLǃplayerchouma_renyiduizi_4ǃCOMPONENT
		getChildAt(index: 189): otherǁchouma_common_XMLǃplayerchouma_renyiduizi_4ǃCOMPONENT
		getChildById(id: 'n183_s6qw'): otherǁchouma_common_XMLǃplayerchouma_renyiduizi_4ǃCOMPONENT
		getChild(name: 'playerchouma_renyiduizi_5'): otherǁchouma_common_XMLǃplayerchouma_renyiduizi_5ǃCOMPONENT
		getChildAt(index: 190): otherǁchouma_common_XMLǃplayerchouma_renyiduizi_5ǃCOMPONENT
		getChildById(id: 'n184_s6qw'): otherǁchouma_common_XMLǃplayerchouma_renyiduizi_5ǃCOMPONENT
		getChild(name: 'playerchouma_renyiduizi_6'): otherǁchouma_common_XMLǃplayerchouma_renyiduizi_6ǃCOMPONENT
		getChildAt(index: 191): otherǁchouma_common_XMLǃplayerchouma_renyiduizi_6ǃCOMPONENT
		getChildById(id: 'n185_s6qw'): otherǁchouma_common_XMLǃplayerchouma_renyiduizi_6ǃCOMPONENT
		getChild(name: 'playerchouma_zhuangdui_1'): otherǁchouma_common_XMLǃplayerchouma_zhuangdui_1ǃCOMPONENT
		getChildAt(index: 192): otherǁchouma_common_XMLǃplayerchouma_zhuangdui_1ǃCOMPONENT
		getChildById(id: 'n180_s6qw'): otherǁchouma_common_XMLǃplayerchouma_zhuangdui_1ǃCOMPONENT
		getChild(name: 'playerchouma_zhuangdui_2'): otherǁchouma_common_XMLǃplayerchouma_zhuangdui_2ǃCOMPONENT
		getChildAt(index: 193): otherǁchouma_common_XMLǃplayerchouma_zhuangdui_2ǃCOMPONENT
		getChildById(id: 'n181_s6qw'): otherǁchouma_common_XMLǃplayerchouma_zhuangdui_2ǃCOMPONENT
		getChild(name: 'playerchouma_zhuangdui_3'): otherǁchouma_common_XMLǃplayerchouma_zhuangdui_3ǃCOMPONENT
		getChildAt(index: 194): otherǁchouma_common_XMLǃplayerchouma_zhuangdui_3ǃCOMPONENT
		getChildById(id: 'n182_s6qw'): otherǁchouma_common_XMLǃplayerchouma_zhuangdui_3ǃCOMPONENT
		getChild(name: 'playerchouma_zhuangdui_4'): otherǁchouma_common_XMLǃplayerchouma_zhuangdui_4ǃCOMPONENT
		getChildAt(index: 195): otherǁchouma_common_XMLǃplayerchouma_zhuangdui_4ǃCOMPONENT
		getChildById(id: 'n183_s6qw'): otherǁchouma_common_XMLǃplayerchouma_zhuangdui_4ǃCOMPONENT
		getChild(name: 'playerchouma_zhuangdui_5'): otherǁchouma_common_XMLǃplayerchouma_zhuangdui_5ǃCOMPONENT
		getChildAt(index: 196): otherǁchouma_common_XMLǃplayerchouma_zhuangdui_5ǃCOMPONENT
		getChildById(id: 'n184_s6qw'): otherǁchouma_common_XMLǃplayerchouma_zhuangdui_5ǃCOMPONENT
		getChild(name: 'playerchouma_zhuangdui_6'): otherǁchouma_common_XMLǃplayerchouma_zhuangdui_6ǃCOMPONENT
		getChildAt(index: 197): otherǁchouma_common_XMLǃplayerchouma_zhuangdui_6ǃCOMPONENT
		getChildById(id: 'n185_s6qw'): otherǁchouma_common_XMLǃplayerchouma_zhuangdui_6ǃCOMPONENT
		getChild(name: 'playerchouma_xiandui_1'): otherǁchouma_common_XMLǃplayerchouma_xiandui_1ǃCOMPONENT
		getChildAt(index: 198): otherǁchouma_common_XMLǃplayerchouma_xiandui_1ǃCOMPONENT
		getChildById(id: 'n180_s6qw'): otherǁchouma_common_XMLǃplayerchouma_xiandui_1ǃCOMPONENT
		getChild(name: 'playerchouma_xiandui_2'): otherǁchouma_common_XMLǃplayerchouma_xiandui_2ǃCOMPONENT
		getChildAt(index: 199): otherǁchouma_common_XMLǃplayerchouma_xiandui_2ǃCOMPONENT
		getChildById(id: 'n181_s6qw'): otherǁchouma_common_XMLǃplayerchouma_xiandui_2ǃCOMPONENT
		getChild(name: 'playerchouma_xiandui_3'): otherǁchouma_common_XMLǃplayerchouma_xiandui_3ǃCOMPONENT
		getChildAt(index: 200): otherǁchouma_common_XMLǃplayerchouma_xiandui_3ǃCOMPONENT
		getChildById(id: 'n182_s6qw'): otherǁchouma_common_XMLǃplayerchouma_xiandui_3ǃCOMPONENT
		getChild(name: 'playerchouma_xiandui_4'): otherǁchouma_common_XMLǃplayerchouma_xiandui_4ǃCOMPONENT
		getChildAt(index: 201): otherǁchouma_common_XMLǃplayerchouma_xiandui_4ǃCOMPONENT
		getChildById(id: 'n183_s6qw'): otherǁchouma_common_XMLǃplayerchouma_xiandui_4ǃCOMPONENT
		getChild(name: 'playerchouma_xiandui_5'): otherǁchouma_common_XMLǃplayerchouma_xiandui_5ǃCOMPONENT
		getChildAt(index: 202): otherǁchouma_common_XMLǃplayerchouma_xiandui_5ǃCOMPONENT
		getChildById(id: 'n184_s6qw'): otherǁchouma_common_XMLǃplayerchouma_xiandui_5ǃCOMPONENT
		getChild(name: 'playerchouma_xiandui_6'): otherǁchouma_common_XMLǃplayerchouma_xiandui_6ǃCOMPONENT
		getChildAt(index: 203): otherǁchouma_common_XMLǃplayerchouma_xiandui_6ǃCOMPONENT
		getChildById(id: 'n185_s6qw'): otherǁchouma_common_XMLǃplayerchouma_xiandui_6ǃCOMPONENT
		getChild(name: 'playerchouma_zhuang_1'): otherǁchouma_common_XMLǃplayerchouma_zhuang_1ǃCOMPONENT
		getChildAt(index: 204): otherǁchouma_common_XMLǃplayerchouma_zhuang_1ǃCOMPONENT
		getChildById(id: 'n180_s6qw'): otherǁchouma_common_XMLǃplayerchouma_zhuang_1ǃCOMPONENT
		getChild(name: 'playerchouma_zhuang_2'): otherǁchouma_common_XMLǃplayerchouma_zhuang_2ǃCOMPONENT
		getChildAt(index: 205): otherǁchouma_common_XMLǃplayerchouma_zhuang_2ǃCOMPONENT
		getChildById(id: 'n181_s6qw'): otherǁchouma_common_XMLǃplayerchouma_zhuang_2ǃCOMPONENT
		getChild(name: 'playerchouma_zhuang_3'): otherǁchouma_common_XMLǃplayerchouma_zhuang_3ǃCOMPONENT
		getChildAt(index: 206): otherǁchouma_common_XMLǃplayerchouma_zhuang_3ǃCOMPONENT
		getChildById(id: 'n182_s6qw'): otherǁchouma_common_XMLǃplayerchouma_zhuang_3ǃCOMPONENT
		getChild(name: 'playerchouma_zhuang_4'): otherǁchouma_common_XMLǃplayerchouma_zhuang_4ǃCOMPONENT
		getChildAt(index: 207): otherǁchouma_common_XMLǃplayerchouma_zhuang_4ǃCOMPONENT
		getChildById(id: 'n183_s6qw'): otherǁchouma_common_XMLǃplayerchouma_zhuang_4ǃCOMPONENT
		getChild(name: 'playerchouma_zhuang_5'): otherǁchouma_common_XMLǃplayerchouma_zhuang_5ǃCOMPONENT
		getChildAt(index: 208): otherǁchouma_common_XMLǃplayerchouma_zhuang_5ǃCOMPONENT
		getChildById(id: 'n184_s6qw'): otherǁchouma_common_XMLǃplayerchouma_zhuang_5ǃCOMPONENT
		getChild(name: 'playerchouma_zhuang_6'): otherǁchouma_common_XMLǃplayerchouma_zhuang_6ǃCOMPONENT
		getChildAt(index: 209): otherǁchouma_common_XMLǃplayerchouma_zhuang_6ǃCOMPONENT
		getChildById(id: 'n185_s6qw'): otherǁchouma_common_XMLǃplayerchouma_zhuang_6ǃCOMPONENT
		getChild(name: 'mask_xian'): otherǁzhongjiang_mask5_XMLǃmask_xianǃCOMPONENT
		getChildAt(index: 210): otherǁzhongjiang_mask5_XMLǃmask_xianǃCOMPONENT
		getChildById(id: 'n105_mxij'): otherǁzhongjiang_mask5_XMLǃmask_xianǃCOMPONENT
		getChild(name: 'mask_zhuang'): otherǁzhongjiang_mask4_XMLǃmask_zhuangǃCOMPONENT
		getChildAt(index: 211): otherǁzhongjiang_mask4_XMLǃmask_zhuangǃCOMPONENT
		getChildById(id: 'n107_o1w7'): otherǁzhongjiang_mask4_XMLǃmask_zhuangǃCOMPONENT
		getChild(name: 'mask_he'): otherǁzhongjiang_mask2_XMLǃmask_heǃCOMPONENT
		getChildAt(index: 212): otherǁzhongjiang_mask2_XMLǃmask_heǃCOMPONENT
		getChildById(id: 'n108_o1w7'): otherǁzhongjiang_mask2_XMLǃmask_heǃCOMPONENT
		getChild(name: 'mask_renyiduizi'): otherǁzhongjiang_mask2_XMLǃmask_renyiduiziǃCOMPONENT
		getChildAt(index: 213): otherǁzhongjiang_mask2_XMLǃmask_renyiduiziǃCOMPONENT
		getChildById(id: 'n109_o1w7'): otherǁzhongjiang_mask2_XMLǃmask_renyiduiziǃCOMPONENT
		getChild(name: 'mask_xiandui'): otherǁzhongjiang_mask2_XMLǃmask_xianduiǃCOMPONENT
		getChildAt(index: 214): otherǁzhongjiang_mask2_XMLǃmask_xianduiǃCOMPONENT
		getChildById(id: 'n110_o1w7'): otherǁzhongjiang_mask2_XMLǃmask_xianduiǃCOMPONENT
		getChild(name: 'mask_wanmeiduizi'): otherǁzhongjiang_mask2_XMLǃmask_wanmeiduiziǃCOMPONENT
		getChildAt(index: 215): otherǁzhongjiang_mask2_XMLǃmask_wanmeiduiziǃCOMPONENT
		getChildById(id: 'n111_o1w7'): otherǁzhongjiang_mask2_XMLǃmask_wanmeiduiziǃCOMPONENT
		getChild(name: 'mask_zhuangdui'): otherǁzhongjiang_mask2_XMLǃmask_zhuangduiǃCOMPONENT
		getChildAt(index: 216): otherǁzhongjiang_mask2_XMLǃmask_zhuangduiǃCOMPONENT
		getChildById(id: 'n112_o1w7'): otherǁzhongjiang_mask2_XMLǃmask_zhuangduiǃCOMPONENT
		getChild(name: 'mask_xiao'): otherǁzhongjiang_mask2_XMLǃmask_xiaoǃCOMPONENT
		getChildAt(index: 217): otherǁzhongjiang_mask2_XMLǃmask_xiaoǃCOMPONENT
		getChildById(id: 'n113_o1w7'): otherǁzhongjiang_mask2_XMLǃmask_xiaoǃCOMPONENT
		getChild(name: 'mask_da'): otherǁzhongjiang_mask2_XMLǃmask_daǃCOMPONENT
		getChildAt(index: 218): otherǁzhongjiang_mask2_XMLǃmask_daǃCOMPONENT
		getChildById(id: 'n114_o1w7'): otherǁzhongjiang_mask2_XMLǃmask_daǃCOMPONENT
		getChild(name: 'help_1'): otherǁhelpǁhelp_renyiduizi_XMLǃhelp_1ǃCOMPONENT
		getChildAt(index: 219): otherǁhelpǁhelp_renyiduizi_XMLǃhelp_1ǃCOMPONENT
		getChildById(id: 'n228_ww6c'): otherǁhelpǁhelp_renyiduizi_XMLǃhelp_1ǃCOMPONENT
		getChild(name: 'help_2'): otherǁhelpǁhelp_xiandui_XMLǃhelp_2ǃCOMPONENT
		getChildAt(index: 220): otherǁhelpǁhelp_xiandui_XMLǃhelp_2ǃCOMPONENT
		getChildById(id: 'n231_ww6c'): otherǁhelpǁhelp_xiandui_XMLǃhelp_2ǃCOMPONENT
		getChild(name: 'help_3'): otherǁhelpǁhelp_da_XMLǃhelp_3ǃCOMPONENT
		getChildAt(index: 221): otherǁhelpǁhelp_da_XMLǃhelp_3ǃCOMPONENT
		getChildById(id: 'n229_ww6c'): otherǁhelpǁhelp_da_XMLǃhelp_3ǃCOMPONENT
		getChild(name: 'help_4'): otherǁhelpǁhelp_xiao_XMLǃhelp_4ǃCOMPONENT
		getChildAt(index: 222): otherǁhelpǁhelp_xiao_XMLǃhelp_4ǃCOMPONENT
		getChildById(id: 'n232_ww6c'): otherǁhelpǁhelp_xiao_XMLǃhelp_4ǃCOMPONENT
		getChild(name: 'help_5'): otherǁhelpǁhelp_zhuangdui_XMLǃhelp_5ǃCOMPONENT
		getChildAt(index: 223): otherǁhelpǁhelp_zhuangdui_XMLǃhelp_5ǃCOMPONENT
		getChildById(id: 'n233_ww6c'): otherǁhelpǁhelp_zhuangdui_XMLǃhelp_5ǃCOMPONENT
		getChild(name: 'help_6'): otherǁhelpǁhelp_wanmeiduizi_XMLǃhelp_6ǃCOMPONENT
		getChildAt(index: 224): otherǁhelpǁhelp_wanmeiduizi_XMLǃhelp_6ǃCOMPONENT
		getChildById(id: 'n230_ww6c'): otherǁhelpǁhelp_wanmeiduizi_XMLǃhelp_6ǃCOMPONENT
		getChild(name: 'button_zidongtouzhu'): buttonsǁgongnengǁbutton_zidongtouzhu_XMLǃbutton_zidongtouzhuǃCOMPONENT
		getChildAt(index: 225): buttonsǁgongnengǁbutton_zidongtouzhu_XMLǃbutton_zidongtouzhuǃCOMPONENT
		getChildById(id: 'n215_h3lk'): buttonsǁgongnengǁbutton_zidongtouzhu_XMLǃbutton_zidongtouzhuǃCOMPONENT
		getChild(name: 'button_return'): buttonsǁgongnengǁbutton_return_XMLǃbutton_returnǃCOMPONENT
		getChildAt(index: 226): buttonsǁgongnengǁbutton_return_XMLǃbutton_returnǃCOMPONENT
		getChildById(id: 'n237_l9o9'): buttonsǁgongnengǁbutton_return_XMLǃbutton_returnǃCOMPONENT
		getChild(name: 'reward_text_ani'): otherǁreward_text_ani_XMLǃreward_text_aniǃCOMPONENT
		getChildAt(index: 227): otherǁreward_text_ani_XMLǃreward_text_aniǃCOMPONENT
		getChildById(id: 'n236_ww6c'): otherǁreward_text_ani_XMLǃreward_text_aniǃCOMPONENT
		getChild(name: 'button_daxiaolu'): buttonsǁluzhiǁdaxiaolu_XMLǃbutton_daxiaoluǃCOMPONENT
		getChildAt(index: 228): buttonsǁluzhiǁdaxiaolu_XMLǃbutton_daxiaoluǃCOMPONENT
		getChildById(id: 'n245_jprs'): buttonsǁluzhiǁdaxiaolu_XMLǃbutton_daxiaoluǃCOMPONENT
		getChild(name: 'button_zhugulu'): buttonsǁluzhiǁzhugulu_XMLǃbutton_zhuguluǃCOMPONENT
		getChildAt(index: 229): buttonsǁluzhiǁzhugulu_XMLǃbutton_zhuguluǃCOMPONENT
		getChildById(id: 'n246_jprs'): buttonsǁluzhiǁzhugulu_XMLǃbutton_zhuguluǃCOMPONENT
		getChild(name: 'daojishi_CN2'): Main_XMLǃdaojishi_CN2ǃTEXT
		getChildAt(index: 230): Main_XMLǃdaojishi_CN2ǃTEXT
		getChildById(id: 'n253_xls7_CN2'): Main_XMLǃdaojishi_CN2ǃTEXT
		getChild(name: 'daojishi_EN'): Main_XMLǃdaojishi_ENǃTEXT
		getChildAt(index: 231): Main_XMLǃdaojishi_ENǃTEXT
		getChildById(id: 'n253_xls7_EN'): Main_XMLǃdaojishi_ENǃTEXT
		getChild(name: 'daojishi_IN'): Main_XMLǃdaojishi_INǃTEXT
		getChildAt(index: 232): Main_XMLǃdaojishi_INǃTEXT
		getChildById(id: 'n253_xls7_IN'): Main_XMLǃdaojishi_INǃTEXT
		getChild(name: 'daojishi_JP'): Main_XMLǃdaojishi_JPǃTEXT
		getChildAt(index: 233): Main_XMLǃdaojishi_JPǃTEXT
		getChildById(id: 'n253_xls7_JP'): Main_XMLǃdaojishi_JPǃTEXT
		getChild(name: 'daojishi_KR'): Main_XMLǃdaojishi_KRǃTEXT
		getChildAt(index: 234): Main_XMLǃdaojishi_KRǃTEXT
		getChildById(id: 'n253_xls7_KR'): Main_XMLǃdaojishi_KRǃTEXT
		getChild(name: 'daojishi_TH'): Main_XMLǃdaojishi_THǃTEXT
		getChildAt(index: 235): Main_XMLǃdaojishi_THǃTEXT
		getChildById(id: 'n253_xls7_TH'): Main_XMLǃdaojishi_THǃTEXT
		getChild(name: 'daojishi_VN'): Main_XMLǃdaojishi_VNǃTEXT
		getChildAt(index: 236): Main_XMLǃdaojishi_VNǃTEXT
		getChildById(id: 'n253_xls7_VN'): Main_XMLǃdaojishi_VNǃTEXT
		getChild(name: 'daojishi'): Main_XMLǃdaojishiǃTEXT
		getChildAt(index: 237): Main_XMLǃdaojishiǃTEXT
		getChildById(id: 'n253_xls7'): Main_XMLǃdaojishiǃTEXT
		getChild(name: 'text_xiandianshu'): Main_XMLǃtext_xiandianshuǃTEXT
		getChildAt(index: 238): Main_XMLǃtext_xiandianshuǃTEXT
		getChildById(id: 'n242_jprs'): Main_XMLǃtext_xiandianshuǃTEXT
		getChild(name: 'text_zhuangdianshu'): Main_XMLǃtext_zhuangdianshuǃTEXT
		getChildAt(index: 239): Main_XMLǃtext_zhuangdianshuǃTEXT
		getChildById(id: 'n243_jprs'): Main_XMLǃtext_zhuangdianshuǃTEXT
		getChild(name: 'danmu'): otherǁdanmu_XMLǃdanmuǃCOMPONENT
		getChildAt(index: 240): otherǁdanmu_XMLǃdanmuǃCOMPONENT
		getChildById(id: 'n239_j4zf'): otherǁdanmu_XMLǃdanmuǃCOMPONENT
		getChild(name: 'liaotianshi'): liaotianshi_XMLǃliaotianshiǃCOMPONENT
		getChildAt(index: 241): liaotianshi_XMLǃliaotianshiǃCOMPONENT
		getChildById(id: 'n102_q4bw'): liaotianshi_XMLǃliaotianshiǃCOMPONENT
		getChild(name: 'btn_gain'): buttonsǁbtn_gain_XMLǃbtn_gainǃCOMPONENT
		getChildAt(index: 242): buttonsǁbtn_gain_XMLǃbtn_gainǃCOMPONENT
		getChildById(id: 'n251_jtlp'): buttonsǁbtn_gain_XMLǃbtn_gainǃCOMPONENT
		getChild(name: 'redirectGame'): settingsǁredirectBox_XMLǃredirectGameǃCOMPONENT
		getChildAt(index: 243): settingsǁredirectBox_XMLǃredirectGameǃCOMPONENT
		getChildById(id: 'n255_rddw'): settingsǁredirectBox_XMLǃredirectGameǃCOMPONENT
		getChild(name: 'menu'): menu_XMLǃmenuǃCOMPONENT
		getChildAt(index: 244): menu_XMLǃmenuǃCOMPONENT
		getChildById(id: 'n99_edb2'): menu_XMLǃmenuǃCOMPONENT
		getChild(name: 'luzhitu'): newluzhitu_XMLǃluzhituǃCOMPONENT
		getChildAt(index: 245): newluzhitu_XMLǃluzhituǃCOMPONENT
		getChildById(id: 'n227_awmy'): newluzhitu_XMLǃluzhituǃCOMPONENT
		getChild(name: 'info_zhupan'): otherǁinfo_zhupan_XMLǃinfo_zhupanǃCOMPONENT
		getChildAt(index: 246): otherǁinfo_zhupan_XMLǃinfo_zhupanǃCOMPONENT
		getChildById(id: 'n237_b7kc'): otherǁinfo_zhupan_XMLǃinfo_zhupanǃCOMPONENT
		getChild(name: 'settings'): settings_XMLǃsettingsǃCOMPONENT
		getChildAt(index: 247): settings_XMLǃsettingsǃCOMPONENT
		getChildById(id: 'n211_jwy6'): settings_XMLǃsettingsǃCOMPONENT
		getChild(name: 'help'): help_XMLǃhelpǃCOMPONENT
		getChildAt(index: 248): help_XMLǃhelpǃCOMPONENT
		getChildById(id: 'n208_pfou'): help_XMLǃhelpǃCOMPONENT
		getChild(name: 'leaderboard'): otherǁLeaderboard_XMLǃleaderboardǃCOMPONENT
		getChildAt(index: 249): otherǁLeaderboard_XMLǃleaderboardǃCOMPONENT
		getChildById(id: 'n227_ww6c'): otherǁLeaderboard_XMLǃleaderboardǃCOMPONENT
		getChild(name: 'n247'): buttonsǁautobetDialog_XMLǃn247ǃCOMPONENT
		getChildAt(index: 250): buttonsǁautobetDialog_XMLǃn247ǃCOMPONENT
		getChildById(id: 'n247_pwo9'): buttonsǁautobetDialog_XMLǃn247ǃCOMPONENT
		getChild(name: 'yingqian_tips'): yingqian_tip_XMLǃyingqian_tipsǃCOMPONENT
		getChildAt(index: 251): yingqian_tip_XMLǃyingqian_tipsǃCOMPONENT
		getChildById(id: 'n189_k6ob'): yingqian_tip_XMLǃyingqian_tipsǃCOMPONENT
		getChild(name: 'kaishixiazhu'): kaishixiazhu_XMLǃkaishixiazhuǃCOMPONENT
		getChildAt(index: 252): kaishixiazhu_XMLǃkaishixiazhuǃCOMPONENT
		getChildById(id: 'n80_x8xm'): kaishixiazhu_XMLǃkaishixiazhuǃCOMPONENT
		getChild(name: 'zuihoutouzhu_confirm'): otherǁzuihoutouzhu_confirm_XMLǃzuihoutouzhu_confirmǃCOMPONENT
		getChildAt(index: 253): otherǁzuihoutouzhu_confirm_XMLǃzuihoutouzhu_confirmǃCOMPONENT
		getChildById(id: 'n104_fjuj'): otherǁzuihoutouzhu_confirm_XMLǃzuihoutouzhu_confirmǃCOMPONENT
		getChild(name: 'message_box'): MessageBox_XMLǃmessage_boxǃCOMPONENT
		getChildAt(index: 254): MessageBox_XMLǃmessage_boxǃCOMPONENT
		getChildById(id: 'n188_e1vg'): MessageBox_XMLǃmessage_boxǃCOMPONENT
		getChild(name: 'reconnect_box'): MessageBox_XMLǃreconnect_boxǃCOMPONENT
		getChildAt(index: 255): MessageBox_XMLǃreconnect_boxǃCOMPONENT
		getChildById(id: 'n213_vhvs'): MessageBox_XMLǃreconnect_boxǃCOMPONENT
		getChild(name: 'leave_confirm'): message_confirm_XMLǃleave_confirmǃCOMPONENT
		getChildAt(index: 256): message_confirm_XMLǃleave_confirmǃCOMPONENT
		getChildById(id: 'n238_l9o9'): message_confirm_XMLǃleave_confirmǃCOMPONENT
		getChild(name: 'n273'): Main_XMLǃn273ǃIMAGE
		getChildAt(index: 257): Main_XMLǃn273ǃIMAGE
		getChildById(id: 'n273_ej3n'): Main_XMLǃn273ǃIMAGE
		getChild(name: 'n273_CN2'): Main_XMLǃn273_CN2ǃIMAGE
		getChildAt(index: 258): Main_XMLǃn273_CN2ǃIMAGE
		getChildById(id: 'n300_knf5'): Main_XMLǃn273_CN2ǃIMAGE
		getChild(name: 'n273_EN'): Main_XMLǃn273_ENǃIMAGE
		getChildAt(index: 259): Main_XMLǃn273_ENǃIMAGE
		getChildById(id: 'n301_knf5'): Main_XMLǃn273_ENǃIMAGE
		getChild(name: 'n273_IN'): Main_XMLǃn273_INǃIMAGE
		getChildAt(index: 260): Main_XMLǃn273_INǃIMAGE
		getChildById(id: 'n302_knf5'): Main_XMLǃn273_INǃIMAGE
		getChild(name: 'n273_JP'): Main_XMLǃn273_JPǃIMAGE
		getChildAt(index: 261): Main_XMLǃn273_JPǃIMAGE
		getChildById(id: 'n303_knf5'): Main_XMLǃn273_JPǃIMAGE
		getChild(name: 'n273_KR'): Main_XMLǃn273_KRǃIMAGE
		getChildAt(index: 262): Main_XMLǃn273_KRǃIMAGE
		getChildById(id: 'n304_knf5'): Main_XMLǃn273_KRǃIMAGE
		getChild(name: 'n273_TH'): Main_XMLǃn273_THǃIMAGE
		getChildAt(index: 263): Main_XMLǃn273_THǃIMAGE
		getChildById(id: 'n305_knf5'): Main_XMLǃn273_THǃIMAGE
		getChild(name: 'n273_VN'): Main_XMLǃn273_VNǃIMAGE
		getChildAt(index: 264): Main_XMLǃn273_VNǃIMAGE
		getChildById(id: 'n306_knf5'): Main_XMLǃn273_VNǃIMAGE
		_children: [
			Main_XMLǃn0ǃIMAGE,
			Main_XMLǃn274ǃIMAGE,
			Main_XMLǃn274_CN2ǃIMAGE,
			Main_XMLǃn274_ENǃIMAGE,
			Main_XMLǃn274_INǃIMAGE,
			Main_XMLǃn274_JPǃIMAGE,
			Main_XMLǃn274_KRǃIMAGE,
			Main_XMLǃn274_THǃIMAGE,
			Main_XMLǃn264_ENǃIMAGE,
			Main_XMLǃn264_INǃIMAGE,
			Main_XMLǃn264_JPǃIMAGE,
			Main_XMLǃn264_KRǃIMAGE,
			Main_XMLǃn264_THǃIMAGE,
			Main_XMLǃn264_VNǃIMAGE,
			Main_XMLǃn264_CN2ǃIMAGE,
			Main_XMLǃn264ǃIMAGE,
			Main_XMLǃn265_ENǃIMAGE,
			Main_XMLǃn265_INǃIMAGE,
			Main_XMLǃn265_JPǃIMAGE,
			Main_XMLǃn265_KRǃIMAGE,
			Main_XMLǃn265_THǃIMAGE,
			Main_XMLǃn265_VNǃIMAGE,
			Main_XMLǃn265_CN2ǃIMAGE,
			Main_XMLǃn265ǃIMAGE,
			Main_XMLǃn266_ENǃIMAGE,
			Main_XMLǃn266_INǃIMAGE,
			Main_XMLǃn266_JPǃIMAGE,
			Main_XMLǃn266_KRǃIMAGE,
			Main_XMLǃn266_THǃIMAGE,
			Main_XMLǃn266_VNǃIMAGE,
			Main_XMLǃn266_CN2ǃIMAGE,
			Main_XMLǃn266ǃIMAGE,
			Main_XMLǃn267_ENǃIMAGE,
			Main_XMLǃn267_INǃIMAGE,
			Main_XMLǃn267_JPǃIMAGE,
			Main_XMLǃn267_KRǃIMAGE,
			Main_XMLǃn267_THǃIMAGE,
			Main_XMLǃn267_VNǃIMAGE,
			Main_XMLǃn267_CN2ǃIMAGE,
			Main_XMLǃn267ǃIMAGE,
			Main_XMLǃn268_ENǃIMAGE,
			Main_XMLǃn268_INǃIMAGE,
			Main_XMLǃn268_JPǃIMAGE,
			Main_XMLǃn268_KRǃIMAGE,
			Main_XMLǃn268_THǃIMAGE,
			Main_XMLǃn268_VNǃIMAGE,
			Main_XMLǃn268_CN2ǃIMAGE,
			Main_XMLǃn268ǃIMAGE,
			Main_XMLǃn269_ENǃIMAGE,
			Main_XMLǃn269_INǃIMAGE,
			Main_XMLǃn269_JPǃIMAGE,
			Main_XMLǃn269_KRǃIMAGE,
			Main_XMLǃn269_THǃIMAGE,
			Main_XMLǃn269_VNǃIMAGE,
			Main_XMLǃn269_CN2ǃIMAGE,
			Main_XMLǃn269ǃIMAGE,
			Main_XMLǃn270_ENǃIMAGE,
			Main_XMLǃn270_INǃIMAGE,
			Main_XMLǃn270_JPǃIMAGE,
			Main_XMLǃn270_KRǃIMAGE,
			Main_XMLǃn270_THǃIMAGE,
			Main_XMLǃn270_VNǃIMAGE,
			Main_XMLǃn270_CN2ǃIMAGE,
			Main_XMLǃn270ǃIMAGE,
			Main_XMLǃn271_ENǃIMAGE,
			Main_XMLǃn271_INǃIMAGE,
			Main_XMLǃn271_JPǃIMAGE,
			Main_XMLǃn271_KRǃIMAGE,
			Main_XMLǃn271_THǃIMAGE,
			Main_XMLǃn271_VNǃIMAGE,
			Main_XMLǃn271_CN2ǃIMAGE,
			Main_XMLǃn271ǃIMAGE,
			Main_XMLǃn272_ENǃIMAGE,
			Main_XMLǃn272_INǃIMAGE,
			Main_XMLǃn272_JPǃIMAGE,
			Main_XMLǃn272_KRǃIMAGE,
			Main_XMLǃn272_THǃIMAGE,
			Main_XMLǃn272_VNǃIMAGE,
			Main_XMLǃn272_CN2ǃIMAGE,
			Main_XMLǃn272ǃIMAGE,
			otherǁchouma_XMLǃchoumaǃCOMPONENT,
			buttonsǁgongnengǁbutton_chexiao_XMLǃbutton_chexiaoǃCOMPONENT,
			buttonsǁgongnengǁbutton_quanxing_XMLǃbutton_quanqingǃCOMPONENT,
			buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃbutton_chongfutouzhuǃCOMPONENT,
			Main_XMLǃzhuangbuǃIMAGE,
			Main_XMLǃxianbu_CN2ǃIMAGE,
			Main_XMLǃxianbu_ENǃIMAGE,
			Main_XMLǃxianbu_INǃIMAGE,
			Main_XMLǃxianbu_JPǃIMAGE,
			Main_XMLǃxianbu_KRǃIMAGE,
			Main_XMLǃxianbu_THǃIMAGE,
			Main_XMLǃxianbu_VNǃIMAGE,
			Main_XMLǃxianbuǃIMAGE,
			cradsǁpaidi_XMLǃfapai_08ǃCOMPONENT,
			cradsǁpaidi_XMLǃfapai_07ǃCOMPONENT,
			cradsǁpaidi2_XMLǃfapai_06ǃCOMPONENT,
			cradsǁpaidi2_XMLǃfapai_05ǃCOMPONENT,
			cradsǁpaidi4_XMLǃfapai_04ǃCOMPONENT,
			cradsǁpaidi1_XMLǃfapai_03ǃCOMPONENT,
			cradsǁpaidi3_XMLǃfapai_02ǃCOMPONENT,
			cradsǁpaidi_XMLǃfapai_01ǃCOMPONENT,
			Main_XMLǃani_xipaiǃMOVIECLIP,
			Main_XMLǃtext_center_CN2ǃTEXT,
			Main_XMLǃtext_center_ENǃTEXT,
			Main_XMLǃtext_center_INǃTEXT,
			Main_XMLǃtext_center_JPǃTEXT,
			Main_XMLǃtext_center_KRǃTEXT,
			Main_XMLǃtext_center_THǃTEXT,
			Main_XMLǃtext_center_VNǃTEXT,
			Main_XMLǃtext_centerǃTEXT,
			buttonsǁgongnengǁbtn_menu_XMLǃbtn_menuǃCOMPONENT,
			Main_XMLǃtext_qishuǃRICHTEXT,
			Main_XMLǃtext_countdownǃTEXT,
			pingInfo_XMLǃpingǃCOMPONENT,
			Main_XMLǃt112_CN2ǃTEXT,
			Main_XMLǃt112_ENǃTEXT,
			Main_XMLǃt112_INǃTEXT,
			Main_XMLǃt112_JPǃTEXT,
			Main_XMLǃt112_KRǃTEXT,
			Main_XMLǃt112_THǃTEXT,
			Main_XMLǃt112_VNǃTEXT,
			Main_XMLǃt112ǃTEXT,
			Main_XMLǃn113_CN2ǃTEXT,
			Main_XMLǃn113_ENǃTEXT,
			Main_XMLǃn113_INǃTEXT,
			Main_XMLǃn113_JPǃTEXT,
			Main_XMLǃn113_KRǃTEXT,
			Main_XMLǃn113_THǃTEXT,
			Main_XMLǃn113_VNǃTEXT,
			Main_XMLǃn113ǃTEXT,
			Main_XMLǃtext_renshuǃTEXT,
			buttonsǁgongnengǁbutton_touzhu_XMLǃbutton_querentouzhuǃCOMPONENT,
			xiazhuxinxiǁxinxi_moban1_XMLǃxinxi_daǃCOMPONENT,
			xiazhuxinxiǁxinxi_moban1_XMLǃxinxi_renyiduiziǃCOMPONENT,
			xiazhuxinxiǁxinxi_moban1_XMLǃxinxi_xianduiǃCOMPONENT,
			xiazhuxinxiǁxinxi_moban1_XMLǃxinxi_xiaoǃCOMPONENT,
			xiazhuxinxiǁxinxi_moban1_XMLǃxinxi_zhuangduiǃCOMPONENT,
			xiazhuxinxiǁxinxi_moban1_XMLǃxinxi_wanmeiduiziǃCOMPONENT,
			xiazhuxinxiǁxinxi_moban2_XMLǃxinxi_xianǃCOMPONENT,
			xiazhuxinxiǁxinxi_moban2_XMLǃxinxi_heǃCOMPONENT,
			xiazhuxinxiǁxinxi_moban2_XMLǃxinxi_zhuangǃCOMPONENT,
			buttonsǁgongnengǁbutton_wenhao_XMLǃbtn_help_renyiduiziǃCOMPONENT,
			buttonsǁgongnengǁbutton_wenhao_XMLǃbtn_help_xianduiǃCOMPONENT,
			buttonsǁgongnengǁbutton_wenhao_XMLǃbtn_help_daǃCOMPONENT,
			buttonsǁgongnengǁbutton_wenhao_XMLǃbtn_help_xiaoǃCOMPONENT,
			buttonsǁgongnengǁbutton_wenhao_XMLǃbtn_help_zhuangduiǃCOMPONENT,
			buttonsǁgongnengǁbutton_wenhao_XMLǃbtn_help_wanmeiduiziǃCOMPONENT,
			otherǁtable_chouma_XMLǃtable_xianǃCOMPONENT,
			otherǁtable_chouma_XMLǃtable_heǃCOMPONENT,
			otherǁtable_chouma_XMLǃtable_zhuangǃCOMPONENT,
			otherǁtable_chouma2_XMLǃtable_renyiduiziǃCOMPONENT,
			otherǁtable_chouma2_XMLǃtable_xianduiǃCOMPONENT,
			otherǁtable_chouma2_XMLǃtable_daǃCOMPONENT,
			otherǁtable_chouma2_XMLǃtable_xiaoǃCOMPONENT,
			otherǁtable_chouma2_XMLǃtable_zhuangduiǃCOMPONENT,
			otherǁtable_chouma2_XMLǃtable_wanmeiduiziǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_xian_1ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_xian_2ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_xian_3ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_xian_4ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_xian_5ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_xian_6ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_da_1ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_da_2ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_da_3ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_da_4ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_da_5ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_da_6ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_he_1ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_he_2ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_he_3ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_he_4ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_he_5ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_he_6ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_xiao_1ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_xiao_2ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_xiao_3ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_xiao_4ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_xiao_5ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_xiao_6ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_wanmeiduizi_1ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_wanmeiduizi_2ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_wanmeiduizi_3ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_wanmeiduizi_4ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_wanmeiduizi_5ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_wanmeiduizi_6ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_renyiduizi_1ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_renyiduizi_2ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_renyiduizi_3ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_renyiduizi_4ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_renyiduizi_5ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_renyiduizi_6ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_zhuangdui_1ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_zhuangdui_2ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_zhuangdui_3ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_zhuangdui_4ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_zhuangdui_5ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_zhuangdui_6ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_xiandui_1ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_xiandui_2ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_xiandui_3ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_xiandui_4ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_xiandui_5ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_xiandui_6ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_zhuang_1ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_zhuang_2ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_zhuang_3ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_zhuang_4ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_zhuang_5ǃCOMPONENT,
			otherǁchouma_common_XMLǃplayerchouma_zhuang_6ǃCOMPONENT,
			otherǁzhongjiang_mask5_XMLǃmask_xianǃCOMPONENT,
			otherǁzhongjiang_mask4_XMLǃmask_zhuangǃCOMPONENT,
			otherǁzhongjiang_mask2_XMLǃmask_heǃCOMPONENT,
			otherǁzhongjiang_mask2_XMLǃmask_renyiduiziǃCOMPONENT,
			otherǁzhongjiang_mask2_XMLǃmask_xianduiǃCOMPONENT,
			otherǁzhongjiang_mask2_XMLǃmask_wanmeiduiziǃCOMPONENT,
			otherǁzhongjiang_mask2_XMLǃmask_zhuangduiǃCOMPONENT,
			otherǁzhongjiang_mask2_XMLǃmask_xiaoǃCOMPONENT,
			otherǁzhongjiang_mask2_XMLǃmask_daǃCOMPONENT,
			otherǁhelpǁhelp_renyiduizi_XMLǃhelp_1ǃCOMPONENT,
			otherǁhelpǁhelp_xiandui_XMLǃhelp_2ǃCOMPONENT,
			otherǁhelpǁhelp_da_XMLǃhelp_3ǃCOMPONENT,
			otherǁhelpǁhelp_xiao_XMLǃhelp_4ǃCOMPONENT,
			otherǁhelpǁhelp_zhuangdui_XMLǃhelp_5ǃCOMPONENT,
			otherǁhelpǁhelp_wanmeiduizi_XMLǃhelp_6ǃCOMPONENT,
			buttonsǁgongnengǁbutton_zidongtouzhu_XMLǃbutton_zidongtouzhuǃCOMPONENT,
			buttonsǁgongnengǁbutton_return_XMLǃbutton_returnǃCOMPONENT,
			otherǁreward_text_ani_XMLǃreward_text_aniǃCOMPONENT,
			buttonsǁluzhiǁdaxiaolu_XMLǃbutton_daxiaoluǃCOMPONENT,
			buttonsǁluzhiǁzhugulu_XMLǃbutton_zhuguluǃCOMPONENT,
			Main_XMLǃdaojishi_CN2ǃTEXT,
			Main_XMLǃdaojishi_ENǃTEXT,
			Main_XMLǃdaojishi_INǃTEXT,
			Main_XMLǃdaojishi_JPǃTEXT,
			Main_XMLǃdaojishi_KRǃTEXT,
			Main_XMLǃdaojishi_THǃTEXT,
			Main_XMLǃdaojishi_VNǃTEXT,
			Main_XMLǃdaojishiǃTEXT,
			Main_XMLǃtext_xiandianshuǃTEXT,
			Main_XMLǃtext_zhuangdianshuǃTEXT,
			otherǁdanmu_XMLǃdanmuǃCOMPONENT,
			liaotianshi_XMLǃliaotianshiǃCOMPONENT,
			buttonsǁbtn_gain_XMLǃbtn_gainǃCOMPONENT,
			settingsǁredirectBox_XMLǃredirectGameǃCOMPONENT,
			menu_XMLǃmenuǃCOMPONENT,
			newluzhitu_XMLǃluzhituǃCOMPONENT,
			otherǁinfo_zhupan_XMLǃinfo_zhupanǃCOMPONENT,
			settings_XMLǃsettingsǃCOMPONENT,
			help_XMLǃhelpǃCOMPONENT,
			otherǁLeaderboard_XMLǃleaderboardǃCOMPONENT,
			buttonsǁautobetDialog_XMLǃn247ǃCOMPONENT,
			yingqian_tip_XMLǃyingqian_tipsǃCOMPONENT,
			kaishixiazhu_XMLǃkaishixiazhuǃCOMPONENT,
			otherǁzuihoutouzhu_confirm_XMLǃzuihoutouzhu_confirmǃCOMPONENT,
			MessageBox_XMLǃmessage_boxǃCOMPONENT,
			MessageBox_XMLǃreconnect_boxǃCOMPONENT,
			message_confirm_XMLǃleave_confirmǃCOMPONENT,
			Main_XMLǃn273ǃIMAGE,
			Main_XMLǃn273_CN2ǃIMAGE,
			Main_XMLǃn273_ENǃIMAGE,
			Main_XMLǃn273_INǃIMAGE,
			Main_XMLǃn273_JPǃIMAGE,
			Main_XMLǃn273_KRǃIMAGE,
			Main_XMLǃn273_THǃIMAGE,
			Main_XMLǃn273_VNǃIMAGE
		]
		getTransition(name: 't0'): Main_XMLǃt0ǃTRANSITION
		getTransitionAt(index: 0): Main_XMLǃt0ǃTRANSITION
		getTransition(name: 't1'): Main_XMLǃt1ǃTRANSITION
		getTransitionAt(index: 1): Main_XMLǃt1ǃTRANSITION
		getTransition(name: 't3'): Main_XMLǃt3ǃTRANSITION
		getTransitionAt(index: 2): Main_XMLǃt3ǃTRANSITION
		getTransition(name: 't4'): Main_XMLǃt4ǃTRANSITION
		getTransitionAt(index: 3): Main_XMLǃt4ǃTRANSITION
		_transitions: [
			Main_XMLǃt0ǃTRANSITION,
			Main_XMLǃt1ǃTRANSITION,
			Main_XMLǃt3ǃTRANSITION,
			Main_XMLǃt4ǃTRANSITION
		]
		getController(name: '__language'): Main_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): Main_XMLǃ__languageǃCONTROLLER
		getController(name: 'controller_fapai'): Main_XMLǃcontroller_fapaiǃCONTROLLER
		getControllerAt(index: 1): Main_XMLǃcontroller_fapaiǃCONTROLLER
		getController(name: 'luzhitu'): Main_XMLǃluzhituǃCONTROLLER
		getControllerAt(index: 2): Main_XMLǃluzhituǃCONTROLLER
		getController(name: 'zuihoutouzhu'): Main_XMLǃzuihoutouzhuǃCONTROLLER
		getControllerAt(index: 3): Main_XMLǃzuihoutouzhuǃCONTROLLER
		getController(name: 'playercm_xian'): Main_XMLǃplayercm_xianǃCONTROLLER
		getControllerAt(index: 4): Main_XMLǃplayercm_xianǃCONTROLLER
		getController(name: 'playercm_he'): Main_XMLǃplayercm_heǃCONTROLLER
		getControllerAt(index: 5): Main_XMLǃplayercm_heǃCONTROLLER
		getController(name: 'playercm_zhuang'): Main_XMLǃplayercm_zhuangǃCONTROLLER
		getControllerAt(index: 6): Main_XMLǃplayercm_zhuangǃCONTROLLER
		getController(name: 'playercm_xiandui'): Main_XMLǃplayercm_xianduiǃCONTROLLER
		getControllerAt(index: 7): Main_XMLǃplayercm_xianduiǃCONTROLLER
		getController(name: 'playercm_zhuangdui'): Main_XMLǃplayercm_zhuangduiǃCONTROLLER
		getControllerAt(index: 8): Main_XMLǃplayercm_zhuangduiǃCONTROLLER
		getController(name: 'playercm_renyiduizi'): Main_XMLǃplayercm_renyiduiziǃCONTROLLER
		getControllerAt(index: 9): Main_XMLǃplayercm_renyiduiziǃCONTROLLER
		getController(name: 'playercm_wanmeiduizi'): Main_XMLǃplayercm_wanmeiduiziǃCONTROLLER
		getControllerAt(index: 10): Main_XMLǃplayercm_wanmeiduiziǃCONTROLLER
		getController(name: 'playercm_da'): Main_XMLǃplayercm_daǃCONTROLLER
		getControllerAt(index: 11): Main_XMLǃplayercm_daǃCONTROLLER
		getController(name: 'playercm_xiao'): Main_XMLǃplayercm_xiaoǃCONTROLLER
		getControllerAt(index: 12): Main_XMLǃplayercm_xiaoǃCONTROLLER
		getController(name: 'lb_ctrl'): Main_XMLǃlb_ctrlǃCONTROLLER
		getControllerAt(index: 13): Main_XMLǃlb_ctrlǃCONTROLLER
		getController(name: 'help_ctrl'): Main_XMLǃhelp_ctrlǃCONTROLLER
		getControllerAt(index: 14): Main_XMLǃhelp_ctrlǃCONTROLLER
		getController(name: 'danmu_ctrl'): Main_XMLǃdanmu_ctrlǃCONTROLLER
		getControllerAt(index: 15): Main_XMLǃdanmu_ctrlǃCONTROLLER
		getController(name: 'luzhi_ctrl'): Main_XMLǃluzhi_ctrlǃCONTROLLER
		getControllerAt(index: 16): Main_XMLǃluzhi_ctrlǃCONTROLLER
		getController(name: 'autobet_ctrl'): Main_XMLǃautobet_ctrlǃCONTROLLER
		getControllerAt(index: 17): Main_XMLǃautobet_ctrlǃCONTROLLER
		getController(name: 'gain_ctrl'): Main_XMLǃgain_ctrlǃCONTROLLER
		getControllerAt(index: 18): Main_XMLǃgain_ctrlǃCONTROLLER
		getController(name: 'menu_ctrl'): Main_XMLǃmenu_ctrlǃCONTROLLER
		getControllerAt(index: 19): Main_XMLǃmenu_ctrlǃCONTROLLER
		_controllers: [
			Main_XMLǃ__languageǃCONTROLLER,
			Main_XMLǃcontroller_fapaiǃCONTROLLER,
			Main_XMLǃluzhituǃCONTROLLER,
			Main_XMLǃzuihoutouzhuǃCONTROLLER,
			Main_XMLǃplayercm_xianǃCONTROLLER,
			Main_XMLǃplayercm_heǃCONTROLLER,
			Main_XMLǃplayercm_zhuangǃCONTROLLER,
			Main_XMLǃplayercm_xianduiǃCONTROLLER,
			Main_XMLǃplayercm_zhuangduiǃCONTROLLER,
			Main_XMLǃplayercm_renyiduiziǃCONTROLLER,
			Main_XMLǃplayercm_wanmeiduiziǃCONTROLLER,
			Main_XMLǃplayercm_daǃCONTROLLER,
			Main_XMLǃplayercm_xiaoǃCONTROLLER,
			Main_XMLǃlb_ctrlǃCONTROLLER,
			Main_XMLǃhelp_ctrlǃCONTROLLER,
			Main_XMLǃdanmu_ctrlǃCONTROLLER,
			Main_XMLǃluzhi_ctrlǃCONTROLLER,
			Main_XMLǃautobet_ctrlǃCONTROLLER,
			Main_XMLǃgain_ctrlǃCONTROLLER,
			Main_XMLǃmenu_ctrlǃCONTROLLER
		]
	}
	interface Main_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: Main_XML
	}
	interface Main_XMLǃcontroller_fapaiǃCONTROLLER extends fairygui.Controller{
		_parent: Main_XML
	}
	interface Main_XMLǃluzhituǃCONTROLLER extends fairygui.Controller{
		_parent: Main_XML
	}
	interface Main_XMLǃzuihoutouzhuǃCONTROLLER extends fairygui.Controller{
		_parent: Main_XML
	}
	interface Main_XMLǃplayercm_xianǃCONTROLLER extends fairygui.Controller{
		_parent: Main_XML
	}
	interface Main_XMLǃplayercm_heǃCONTROLLER extends fairygui.Controller{
		_parent: Main_XML
	}
	interface Main_XMLǃplayercm_zhuangǃCONTROLLER extends fairygui.Controller{
		_parent: Main_XML
	}
	interface Main_XMLǃplayercm_xianduiǃCONTROLLER extends fairygui.Controller{
		_parent: Main_XML
	}
	interface Main_XMLǃplayercm_zhuangduiǃCONTROLLER extends fairygui.Controller{
		_parent: Main_XML
	}
	interface Main_XMLǃplayercm_renyiduiziǃCONTROLLER extends fairygui.Controller{
		_parent: Main_XML
	}
	interface Main_XMLǃplayercm_wanmeiduiziǃCONTROLLER extends fairygui.Controller{
		_parent: Main_XML
	}
	interface Main_XMLǃplayercm_daǃCONTROLLER extends fairygui.Controller{
		_parent: Main_XML
	}
	interface Main_XMLǃplayercm_xiaoǃCONTROLLER extends fairygui.Controller{
		_parent: Main_XML
	}
	interface Main_XMLǃlb_ctrlǃCONTROLLER extends fairygui.Controller{
		_parent: Main_XML
	}
	interface Main_XMLǃhelp_ctrlǃCONTROLLER extends fairygui.Controller{
		_parent: Main_XML
	}
	interface Main_XMLǃdanmu_ctrlǃCONTROLLER extends fairygui.Controller{
		_parent: Main_XML
	}
	interface Main_XMLǃluzhi_ctrlǃCONTROLLER extends fairygui.Controller{
		_parent: Main_XML
	}
	interface Main_XMLǃautobet_ctrlǃCONTROLLER extends fairygui.Controller{
		_parent: Main_XML
	}
	interface Main_XMLǃgain_ctrlǃCONTROLLER extends fairygui.Controller{
		_parent: Main_XML
	}
	interface Main_XMLǃmenu_ctrlǃCONTROLLER extends fairygui.Controller{
		_parent: Main_XML
	}
	interface Main_XMLǃn0ǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn274ǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn274_CN2ǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn274_ENǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn274_INǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn274_JPǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn274_KRǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn274_THǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn264_ENǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn264_INǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn264_JPǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn264_KRǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn264_THǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn264_VNǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn264_CN2ǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn264ǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn265_ENǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn265_INǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn265_JPǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn265_KRǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn265_THǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn265_VNǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn265_CN2ǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn265ǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn266_ENǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn266_INǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn266_JPǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn266_KRǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn266_THǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn266_VNǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn266_CN2ǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn266ǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn267_ENǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn267_INǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn267_JPǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn267_KRǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn267_THǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn267_VNǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn267_CN2ǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn267ǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn268_ENǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn268_INǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn268_JPǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn268_KRǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn268_THǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn268_VNǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn268_CN2ǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn268ǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn269_ENǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn269_INǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn269_JPǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn269_KRǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn269_THǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn269_VNǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn269_CN2ǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn269ǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn270_ENǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn270_INǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn270_JPǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn270_KRǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn270_THǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn270_VNǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn270_CN2ǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn270ǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn271_ENǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn271_INǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn271_JPǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn271_KRǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn271_THǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn271_VNǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn271_CN2ǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn271ǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn272_ENǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn272_INǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn272_JPǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn272_KRǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn272_THǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn272_VNǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn272_CN2ǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn272ǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface otherǁchouma_XML extends fairygui.GComponent{
		getChild(name: 'chouma_0'): buttonsǁchoumaǁbtn_chouma_XMLǃchouma_0ǃCOMPONENT
		getChildAt(index: 0): buttonsǁchoumaǁbtn_chouma_XMLǃchouma_0ǃCOMPONENT
		getChildById(id: 'n5_da1b'): buttonsǁchoumaǁbtn_chouma_XMLǃchouma_0ǃCOMPONENT
		getChild(name: 'chouma_1'): buttonsǁchoumaǁbtn_chouma_XMLǃchouma_1ǃCOMPONENT
		getChildAt(index: 1): buttonsǁchoumaǁbtn_chouma_XMLǃchouma_1ǃCOMPONENT
		getChildById(id: 'n22_edb2'): buttonsǁchoumaǁbtn_chouma_XMLǃchouma_1ǃCOMPONENT
		getChild(name: 'chouma_2'): buttonsǁchoumaǁbtn_chouma_XMLǃchouma_2ǃCOMPONENT
		getChildAt(index: 2): buttonsǁchoumaǁbtn_chouma_XMLǃchouma_2ǃCOMPONENT
		getChildById(id: 'n4_da1b'): buttonsǁchoumaǁbtn_chouma_XMLǃchouma_2ǃCOMPONENT
		getChild(name: 'chouma_3'): buttonsǁchoumaǁbtn_chouma_XMLǃchouma_3ǃCOMPONENT
		getChildAt(index: 3): buttonsǁchoumaǁbtn_chouma_XMLǃchouma_3ǃCOMPONENT
		getChildById(id: 'n3_da1b'): buttonsǁchoumaǁbtn_chouma_XMLǃchouma_3ǃCOMPONENT
		_children: [
			buttonsǁchoumaǁbtn_chouma_XMLǃchouma_0ǃCOMPONENT,
			buttonsǁchoumaǁbtn_chouma_XMLǃchouma_1ǃCOMPONENT,
			buttonsǁchoumaǁbtn_chouma_XMLǃchouma_2ǃCOMPONENT,
			buttonsǁchoumaǁbtn_chouma_XMLǃchouma_3ǃCOMPONENT
		]
		getController(name: '__language'): otherǁchouma_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): otherǁchouma_XMLǃ__languageǃCONTROLLER
		getController(name: 'c1'): otherǁchouma_XMLǃc1ǃCONTROLLER
		getControllerAt(index: 1): otherǁchouma_XMLǃc1ǃCONTROLLER
		_controllers: [
			otherǁchouma_XMLǃ__languageǃCONTROLLER,
			otherǁchouma_XMLǃc1ǃCONTROLLER
		]
	}
	interface otherǁchouma_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: otherǁchouma_XML
	}
	interface otherǁchouma_XMLǃc1ǃCONTROLLER extends fairygui.Controller{
		_parent: otherǁchouma_XML
	}
	interface buttonsǁchoumaǁbtn_chouma_XML extends fairygui.GButton{
		getChild(name: 'n1'): buttonsǁchoumaǁbtn_chouma_XMLǃn1ǃLOADER
		getChildAt(index: 0): buttonsǁchoumaǁbtn_chouma_XMLǃn1ǃLOADER
		getChildById(id: 'n3_6485'): buttonsǁchoumaǁbtn_chouma_XMLǃn1ǃLOADER
		getChild(name: 'n2'): buttonsǁchoumaǁbtn_chouma_XMLǃn2ǃLOADER
		getChildAt(index: 1): buttonsǁchoumaǁbtn_chouma_XMLǃn2ǃLOADER
		getChildById(id: 'n4_6485'): buttonsǁchoumaǁbtn_chouma_XMLǃn2ǃLOADER
		_children: [
			buttonsǁchoumaǁbtn_chouma_XMLǃn1ǃLOADER,
			buttonsǁchoumaǁbtn_chouma_XMLǃn2ǃLOADER
		]
		getController(name: '__language'): buttonsǁchoumaǁbtn_chouma_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁchoumaǁbtn_chouma_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁchoumaǁbtn_chouma_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁchoumaǁbtn_chouma_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁchoumaǁbtn_chouma_XMLǃ__languageǃCONTROLLER,
			buttonsǁchoumaǁbtn_chouma_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁchoumaǁbtn_chouma_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁchoumaǁbtn_chouma_XML
	}
	interface buttonsǁchoumaǁbtn_chouma_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁchoumaǁbtn_chouma_XML
	}
	interface buttonsǁchoumaǁbtn_chouma_XMLǃn1ǃLOADER extends fairygui.GLoader{
		parent: buttonsǁchoumaǁbtn_chouma_XML
	}
	interface buttonsǁchoumaǁbtn_chouma_XMLǃn2ǃLOADER extends fairygui.GLoader{
		parent: buttonsǁchoumaǁbtn_chouma_XML
	}
	interface buttonsǁchoumaǁbtn_chouma_XMLǃchouma_0ǃCOMPONENT extends buttonsǁchoumaǁbtn_chouma_XML{
		parent: otherǁchouma_XML
	}
	interface buttonsǁchoumaǁbtn_chouma_XMLǃchouma_1ǃCOMPONENT extends buttonsǁchoumaǁbtn_chouma_XML{
		parent: otherǁchouma_XML
	}
	interface buttonsǁchoumaǁbtn_chouma_XMLǃchouma_2ǃCOMPONENT extends buttonsǁchoumaǁbtn_chouma_XML{
		parent: otherǁchouma_XML
	}
	interface buttonsǁchoumaǁbtn_chouma_XMLǃchouma_3ǃCOMPONENT extends buttonsǁchoumaǁbtn_chouma_XML{
		parent: otherǁchouma_XML
	}
	interface otherǁchouma_XMLǃchoumaǃCOMPONENT extends otherǁchouma_XML{
		parent: Main_XML
	}
	interface buttonsǁgongnengǁbutton_chexiao_XML extends fairygui.GButton{
		getChild(name: 'n1'): buttonsǁgongnengǁbutton_chexiao_XMLǃn1ǃIMAGE
		getChildAt(index: 0): buttonsǁgongnengǁbutton_chexiao_XMLǃn1ǃIMAGE
		getChildById(id: 'n1'): buttonsǁgongnengǁbutton_chexiao_XMLǃn1ǃIMAGE
		getChild(name: 'n2_CN2'): buttonsǁgongnengǁbutton_chexiao_XMLǃn2_CN2ǃTEXT
		getChildAt(index: 1): buttonsǁgongnengǁbutton_chexiao_XMLǃn2_CN2ǃTEXT
		getChildById(id: 'n2_fo6w_CN2'): buttonsǁgongnengǁbutton_chexiao_XMLǃn2_CN2ǃTEXT
		getChild(name: 'n2_EN'): buttonsǁgongnengǁbutton_chexiao_XMLǃn2_ENǃTEXT
		getChildAt(index: 2): buttonsǁgongnengǁbutton_chexiao_XMLǃn2_ENǃTEXT
		getChildById(id: 'n2_fo6w_EN'): buttonsǁgongnengǁbutton_chexiao_XMLǃn2_ENǃTEXT
		getChild(name: 'n2_IN'): buttonsǁgongnengǁbutton_chexiao_XMLǃn2_INǃTEXT
		getChildAt(index: 3): buttonsǁgongnengǁbutton_chexiao_XMLǃn2_INǃTEXT
		getChildById(id: 'n2_fo6w_IN'): buttonsǁgongnengǁbutton_chexiao_XMLǃn2_INǃTEXT
		getChild(name: 'n2_JP'): buttonsǁgongnengǁbutton_chexiao_XMLǃn2_JPǃTEXT
		getChildAt(index: 4): buttonsǁgongnengǁbutton_chexiao_XMLǃn2_JPǃTEXT
		getChildById(id: 'n2_fo6w_JP'): buttonsǁgongnengǁbutton_chexiao_XMLǃn2_JPǃTEXT
		getChild(name: 'n2_KR'): buttonsǁgongnengǁbutton_chexiao_XMLǃn2_KRǃTEXT
		getChildAt(index: 5): buttonsǁgongnengǁbutton_chexiao_XMLǃn2_KRǃTEXT
		getChildById(id: 'n2_fo6w_KR'): buttonsǁgongnengǁbutton_chexiao_XMLǃn2_KRǃTEXT
		getChild(name: 'n2_TH'): buttonsǁgongnengǁbutton_chexiao_XMLǃn2_THǃTEXT
		getChildAt(index: 6): buttonsǁgongnengǁbutton_chexiao_XMLǃn2_THǃTEXT
		getChildById(id: 'n2_fo6w_TH'): buttonsǁgongnengǁbutton_chexiao_XMLǃn2_THǃTEXT
		getChild(name: 'n2_VN'): buttonsǁgongnengǁbutton_chexiao_XMLǃn2_VNǃTEXT
		getChildAt(index: 7): buttonsǁgongnengǁbutton_chexiao_XMLǃn2_VNǃTEXT
		getChildById(id: 'n2_fo6w_VN'): buttonsǁgongnengǁbutton_chexiao_XMLǃn2_VNǃTEXT
		getChild(name: 'n2'): buttonsǁgongnengǁbutton_chexiao_XMLǃn2ǃTEXT
		getChildAt(index: 8): buttonsǁgongnengǁbutton_chexiao_XMLǃn2ǃTEXT
		getChildById(id: 'n2_fo6w'): buttonsǁgongnengǁbutton_chexiao_XMLǃn2ǃTEXT
		_children: [
			buttonsǁgongnengǁbutton_chexiao_XMLǃn1ǃIMAGE,
			buttonsǁgongnengǁbutton_chexiao_XMLǃn2_CN2ǃTEXT,
			buttonsǁgongnengǁbutton_chexiao_XMLǃn2_ENǃTEXT,
			buttonsǁgongnengǁbutton_chexiao_XMLǃn2_INǃTEXT,
			buttonsǁgongnengǁbutton_chexiao_XMLǃn2_JPǃTEXT,
			buttonsǁgongnengǁbutton_chexiao_XMLǃn2_KRǃTEXT,
			buttonsǁgongnengǁbutton_chexiao_XMLǃn2_THǃTEXT,
			buttonsǁgongnengǁbutton_chexiao_XMLǃn2_VNǃTEXT,
			buttonsǁgongnengǁbutton_chexiao_XMLǃn2ǃTEXT
		]
		getController(name: '__language'): buttonsǁgongnengǁbutton_chexiao_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁgongnengǁbutton_chexiao_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁgongnengǁbutton_chexiao_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁgongnengǁbutton_chexiao_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁgongnengǁbutton_chexiao_XMLǃ__languageǃCONTROLLER,
			buttonsǁgongnengǁbutton_chexiao_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁgongnengǁbutton_chexiao_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁgongnengǁbutton_chexiao_XML
	}
	interface buttonsǁgongnengǁbutton_chexiao_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁgongnengǁbutton_chexiao_XML
	}
	interface buttonsǁgongnengǁbutton_chexiao_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁgongnengǁbutton_chexiao_XML
	}
	interface buttonsǁgongnengǁbutton_chexiao_XMLǃn2_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁgongnengǁbutton_chexiao_XML
	}
	interface buttonsǁgongnengǁbutton_chexiao_XMLǃn2_ENǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁgongnengǁbutton_chexiao_XML
	}
	interface buttonsǁgongnengǁbutton_chexiao_XMLǃn2_INǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁgongnengǁbutton_chexiao_XML
	}
	interface buttonsǁgongnengǁbutton_chexiao_XMLǃn2_JPǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁgongnengǁbutton_chexiao_XML
	}
	interface buttonsǁgongnengǁbutton_chexiao_XMLǃn2_KRǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁgongnengǁbutton_chexiao_XML
	}
	interface buttonsǁgongnengǁbutton_chexiao_XMLǃn2_THǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁgongnengǁbutton_chexiao_XML
	}
	interface buttonsǁgongnengǁbutton_chexiao_XMLǃn2_VNǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁgongnengǁbutton_chexiao_XML
	}
	interface buttonsǁgongnengǁbutton_chexiao_XMLǃn2ǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁgongnengǁbutton_chexiao_XML
	}
	interface buttonsǁgongnengǁbutton_chexiao_XMLǃbutton_chexiaoǃCOMPONENT extends buttonsǁgongnengǁbutton_chexiao_XML{
		parent: Main_XML
	}
	interface buttonsǁgongnengǁbutton_quanxing_XML extends fairygui.GButton{
		getChild(name: 'n1'): buttonsǁgongnengǁbutton_quanxing_XMLǃn1ǃIMAGE
		getChildAt(index: 0): buttonsǁgongnengǁbutton_quanxing_XMLǃn1ǃIMAGE
		getChildById(id: 'n1'): buttonsǁgongnengǁbutton_quanxing_XMLǃn1ǃIMAGE
		getChild(name: 'n2_CN2'): buttonsǁgongnengǁbutton_quanxing_XMLǃn2_CN2ǃTEXT
		getChildAt(index: 1): buttonsǁgongnengǁbutton_quanxing_XMLǃn2_CN2ǃTEXT
		getChildById(id: 'n2_fo6w_CN2'): buttonsǁgongnengǁbutton_quanxing_XMLǃn2_CN2ǃTEXT
		getChild(name: 'n2_EN'): buttonsǁgongnengǁbutton_quanxing_XMLǃn2_ENǃTEXT
		getChildAt(index: 2): buttonsǁgongnengǁbutton_quanxing_XMLǃn2_ENǃTEXT
		getChildById(id: 'n2_fo6w_EN'): buttonsǁgongnengǁbutton_quanxing_XMLǃn2_ENǃTEXT
		getChild(name: 'n2_IN'): buttonsǁgongnengǁbutton_quanxing_XMLǃn2_INǃTEXT
		getChildAt(index: 3): buttonsǁgongnengǁbutton_quanxing_XMLǃn2_INǃTEXT
		getChildById(id: 'n2_fo6w_IN'): buttonsǁgongnengǁbutton_quanxing_XMLǃn2_INǃTEXT
		getChild(name: 'n2_JP'): buttonsǁgongnengǁbutton_quanxing_XMLǃn2_JPǃTEXT
		getChildAt(index: 4): buttonsǁgongnengǁbutton_quanxing_XMLǃn2_JPǃTEXT
		getChildById(id: 'n2_fo6w_JP'): buttonsǁgongnengǁbutton_quanxing_XMLǃn2_JPǃTEXT
		getChild(name: 'n2_KR'): buttonsǁgongnengǁbutton_quanxing_XMLǃn2_KRǃTEXT
		getChildAt(index: 5): buttonsǁgongnengǁbutton_quanxing_XMLǃn2_KRǃTEXT
		getChildById(id: 'n2_fo6w_KR'): buttonsǁgongnengǁbutton_quanxing_XMLǃn2_KRǃTEXT
		getChild(name: 'n2_TH'): buttonsǁgongnengǁbutton_quanxing_XMLǃn2_THǃTEXT
		getChildAt(index: 6): buttonsǁgongnengǁbutton_quanxing_XMLǃn2_THǃTEXT
		getChildById(id: 'n2_fo6w_TH'): buttonsǁgongnengǁbutton_quanxing_XMLǃn2_THǃTEXT
		getChild(name: 'n2_VN'): buttonsǁgongnengǁbutton_quanxing_XMLǃn2_VNǃTEXT
		getChildAt(index: 7): buttonsǁgongnengǁbutton_quanxing_XMLǃn2_VNǃTEXT
		getChildById(id: 'n2_fo6w_VN'): buttonsǁgongnengǁbutton_quanxing_XMLǃn2_VNǃTEXT
		getChild(name: 'n2'): buttonsǁgongnengǁbutton_quanxing_XMLǃn2ǃTEXT
		getChildAt(index: 8): buttonsǁgongnengǁbutton_quanxing_XMLǃn2ǃTEXT
		getChildById(id: 'n2_fo6w'): buttonsǁgongnengǁbutton_quanxing_XMLǃn2ǃTEXT
		_children: [
			buttonsǁgongnengǁbutton_quanxing_XMLǃn1ǃIMAGE,
			buttonsǁgongnengǁbutton_quanxing_XMLǃn2_CN2ǃTEXT,
			buttonsǁgongnengǁbutton_quanxing_XMLǃn2_ENǃTEXT,
			buttonsǁgongnengǁbutton_quanxing_XMLǃn2_INǃTEXT,
			buttonsǁgongnengǁbutton_quanxing_XMLǃn2_JPǃTEXT,
			buttonsǁgongnengǁbutton_quanxing_XMLǃn2_KRǃTEXT,
			buttonsǁgongnengǁbutton_quanxing_XMLǃn2_THǃTEXT,
			buttonsǁgongnengǁbutton_quanxing_XMLǃn2_VNǃTEXT,
			buttonsǁgongnengǁbutton_quanxing_XMLǃn2ǃTEXT
		]
		getController(name: '__language'): buttonsǁgongnengǁbutton_quanxing_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁgongnengǁbutton_quanxing_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁgongnengǁbutton_quanxing_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁgongnengǁbutton_quanxing_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁgongnengǁbutton_quanxing_XMLǃ__languageǃCONTROLLER,
			buttonsǁgongnengǁbutton_quanxing_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁgongnengǁbutton_quanxing_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁgongnengǁbutton_quanxing_XML
	}
	interface buttonsǁgongnengǁbutton_quanxing_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁgongnengǁbutton_quanxing_XML
	}
	interface buttonsǁgongnengǁbutton_quanxing_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁgongnengǁbutton_quanxing_XML
	}
	interface buttonsǁgongnengǁbutton_quanxing_XMLǃn2_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁgongnengǁbutton_quanxing_XML
	}
	interface buttonsǁgongnengǁbutton_quanxing_XMLǃn2_ENǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁgongnengǁbutton_quanxing_XML
	}
	interface buttonsǁgongnengǁbutton_quanxing_XMLǃn2_INǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁgongnengǁbutton_quanxing_XML
	}
	interface buttonsǁgongnengǁbutton_quanxing_XMLǃn2_JPǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁgongnengǁbutton_quanxing_XML
	}
	interface buttonsǁgongnengǁbutton_quanxing_XMLǃn2_KRǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁgongnengǁbutton_quanxing_XML
	}
	interface buttonsǁgongnengǁbutton_quanxing_XMLǃn2_THǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁgongnengǁbutton_quanxing_XML
	}
	interface buttonsǁgongnengǁbutton_quanxing_XMLǃn2_VNǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁgongnengǁbutton_quanxing_XML
	}
	interface buttonsǁgongnengǁbutton_quanxing_XMLǃn2ǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁgongnengǁbutton_quanxing_XML
	}
	interface buttonsǁgongnengǁbutton_quanxing_XMLǃbutton_quanqingǃCOMPONENT extends buttonsǁgongnengǁbutton_quanxing_XML{
		parent: Main_XML
	}
	interface buttonsǁgongnengǁbutton_chongfutouzhu_XML extends fairygui.GButton{
		getChild(name: 'n1'): buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn1ǃIMAGE
		getChildAt(index: 0): buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn1ǃIMAGE
		getChildById(id: 'n1'): buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn1ǃIMAGE
		getChild(name: 'n2_CN2'): buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2_CN2ǃTEXT
		getChildAt(index: 1): buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2_CN2ǃTEXT
		getChildById(id: 'n2_fo6w_CN2'): buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2_CN2ǃTEXT
		getChild(name: 'n2_EN'): buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2_ENǃTEXT
		getChildAt(index: 2): buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2_ENǃTEXT
		getChildById(id: 'n2_fo6w_EN'): buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2_ENǃTEXT
		getChild(name: 'n2_IN'): buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2_INǃTEXT
		getChildAt(index: 3): buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2_INǃTEXT
		getChildById(id: 'n2_fo6w_IN'): buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2_INǃTEXT
		getChild(name: 'n2_JP'): buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2_JPǃTEXT
		getChildAt(index: 4): buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2_JPǃTEXT
		getChildById(id: 'n2_fo6w_JP'): buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2_JPǃTEXT
		getChild(name: 'n2_KR'): buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2_KRǃTEXT
		getChildAt(index: 5): buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2_KRǃTEXT
		getChildById(id: 'n2_fo6w_KR'): buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2_KRǃTEXT
		getChild(name: 'n2_TH'): buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2_THǃTEXT
		getChildAt(index: 6): buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2_THǃTEXT
		getChildById(id: 'n2_fo6w_TH'): buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2_THǃTEXT
		getChild(name: 'n2_VN'): buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2_VNǃTEXT
		getChildAt(index: 7): buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2_VNǃTEXT
		getChildById(id: 'n2_fo6w_VN'): buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2_VNǃTEXT
		getChild(name: 'n2'): buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2ǃTEXT
		getChildAt(index: 8): buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2ǃTEXT
		getChildById(id: 'n2_fo6w'): buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2ǃTEXT
		_children: [
			buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn1ǃIMAGE,
			buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2_CN2ǃTEXT,
			buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2_ENǃTEXT,
			buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2_INǃTEXT,
			buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2_JPǃTEXT,
			buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2_KRǃTEXT,
			buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2_THǃTEXT,
			buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2_VNǃTEXT,
			buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2ǃTEXT
		]
		getController(name: '__language'): buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃ__languageǃCONTROLLER,
			buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁgongnengǁbutton_chongfutouzhu_XML
	}
	interface buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁgongnengǁbutton_chongfutouzhu_XML
	}
	interface buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁgongnengǁbutton_chongfutouzhu_XML
	}
	interface buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁgongnengǁbutton_chongfutouzhu_XML
	}
	interface buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2_ENǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁgongnengǁbutton_chongfutouzhu_XML
	}
	interface buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2_INǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁgongnengǁbutton_chongfutouzhu_XML
	}
	interface buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2_JPǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁgongnengǁbutton_chongfutouzhu_XML
	}
	interface buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2_KRǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁgongnengǁbutton_chongfutouzhu_XML
	}
	interface buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2_THǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁgongnengǁbutton_chongfutouzhu_XML
	}
	interface buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2_VNǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁgongnengǁbutton_chongfutouzhu_XML
	}
	interface buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃn2ǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁgongnengǁbutton_chongfutouzhu_XML
	}
	interface buttonsǁgongnengǁbutton_chongfutouzhu_XMLǃbutton_chongfutouzhuǃCOMPONENT extends buttonsǁgongnengǁbutton_chongfutouzhu_XML{
		parent: Main_XML
	}
	interface Main_XMLǃzhuangbuǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃxianbu_CN2ǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃxianbu_ENǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃxianbu_INǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃxianbu_JPǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃxianbu_KRǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃxianbu_THǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃxianbu_VNǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃxianbuǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface cradsǁpaidi_XML extends fairygui.GComponent{
		getChild(name: 'n2'): cradsǁpaidi_XMLǃn2ǃIMAGE
		getChildAt(index: 0): cradsǁpaidi_XMLǃn2ǃIMAGE
		getChildById(id: 'n2_f7j0'): cradsǁpaidi_XMLǃn2ǃIMAGE
		getChild(name: 'n3'): cradsǁpaidi_XMLǃn3ǃIMAGE
		getChildAt(index: 1): cradsǁpaidi_XMLǃn3ǃIMAGE
		getChildById(id: 'n3_edb2'): cradsǁpaidi_XMLǃn3ǃIMAGE
		getChild(name: 'n4'): cradsǁpaidi_XMLǃn4ǃIMAGE
		getChildAt(index: 2): cradsǁpaidi_XMLǃn4ǃIMAGE
		getChildById(id: 'n4_edb2'): cradsǁpaidi_XMLǃn4ǃIMAGE
		getChild(name: 'n5'): cradsǁpaidi_XMLǃn5ǃIMAGE
		getChildAt(index: 3): cradsǁpaidi_XMLǃn5ǃIMAGE
		getChildById(id: 'n5_edb2'): cradsǁpaidi_XMLǃn5ǃIMAGE
		getChild(name: 'n6'): cradsǁpaidi_XMLǃn6ǃIMAGE
		getChildAt(index: 4): cradsǁpaidi_XMLǃn6ǃIMAGE
		getChildById(id: 'n6_edb2'): cradsǁpaidi_XMLǃn6ǃIMAGE
		getChild(name: 'n7'): cradsǁpaidi_XMLǃn7ǃIMAGE
		getChildAt(index: 5): cradsǁpaidi_XMLǃn7ǃIMAGE
		getChildById(id: 'n7_edb2'): cradsǁpaidi_XMLǃn7ǃIMAGE
		getChild(name: 'n8'): cradsǁpaidi_XMLǃn8ǃIMAGE
		getChildAt(index: 6): cradsǁpaidi_XMLǃn8ǃIMAGE
		getChildById(id: 'n8_edb2'): cradsǁpaidi_XMLǃn8ǃIMAGE
		getChild(name: 'n9'): cradsǁpaidi_XMLǃn9ǃIMAGE
		getChildAt(index: 7): cradsǁpaidi_XMLǃn9ǃIMAGE
		getChildById(id: 'n9_edb2'): cradsǁpaidi_XMLǃn9ǃIMAGE
		getChild(name: 'n10'): cradsǁpaidi_XMLǃn10ǃIMAGE
		getChildAt(index: 8): cradsǁpaidi_XMLǃn10ǃIMAGE
		getChildById(id: 'n10_edb2'): cradsǁpaidi_XMLǃn10ǃIMAGE
		getChild(name: 'n11'): cradsǁpaidi_XMLǃn11ǃIMAGE
		getChildAt(index: 9): cradsǁpaidi_XMLǃn11ǃIMAGE
		getChildById(id: 'n11_edb2'): cradsǁpaidi_XMLǃn11ǃIMAGE
		getChild(name: 'n12'): cradsǁpaidi_XMLǃn12ǃIMAGE
		getChildAt(index: 10): cradsǁpaidi_XMLǃn12ǃIMAGE
		getChildById(id: 'n12_edb2'): cradsǁpaidi_XMLǃn12ǃIMAGE
		getChild(name: 'n13'): cradsǁpaidi_XMLǃn13ǃIMAGE
		getChildAt(index: 11): cradsǁpaidi_XMLǃn13ǃIMAGE
		getChildById(id: 'n13_edb2'): cradsǁpaidi_XMLǃn13ǃIMAGE
		getChild(name: 'n14'): cradsǁpaidi_XMLǃn14ǃIMAGE
		getChildAt(index: 12): cradsǁpaidi_XMLǃn14ǃIMAGE
		getChildById(id: 'n14_edb2'): cradsǁpaidi_XMLǃn14ǃIMAGE
		getChild(name: 'n15'): cradsǁpaidi_XMLǃn15ǃIMAGE
		getChildAt(index: 13): cradsǁpaidi_XMLǃn15ǃIMAGE
		getChildById(id: 'n15_edb2'): cradsǁpaidi_XMLǃn15ǃIMAGE
		getChild(name: 'n16'): cradsǁpaidi_XMLǃn16ǃIMAGE
		getChildAt(index: 14): cradsǁpaidi_XMLǃn16ǃIMAGE
		getChildById(id: 'n16_edb2'): cradsǁpaidi_XMLǃn16ǃIMAGE
		getChild(name: 'n17'): cradsǁpaidi_XMLǃn17ǃIMAGE
		getChildAt(index: 15): cradsǁpaidi_XMLǃn17ǃIMAGE
		getChildById(id: 'n17_edb2'): cradsǁpaidi_XMLǃn17ǃIMAGE
		getChild(name: 'n18'): cradsǁpaidi_XMLǃn18ǃIMAGE
		getChildAt(index: 16): cradsǁpaidi_XMLǃn18ǃIMAGE
		getChildById(id: 'n18_edb2'): cradsǁpaidi_XMLǃn18ǃIMAGE
		getChild(name: 'n19'): cradsǁpaidi_XMLǃn19ǃIMAGE
		getChildAt(index: 17): cradsǁpaidi_XMLǃn19ǃIMAGE
		getChildById(id: 'n19_edb2'): cradsǁpaidi_XMLǃn19ǃIMAGE
		getChild(name: 'n20'): cradsǁpaidi_XMLǃn20ǃIMAGE
		getChildAt(index: 18): cradsǁpaidi_XMLǃn20ǃIMAGE
		getChildById(id: 'n20_edb2'): cradsǁpaidi_XMLǃn20ǃIMAGE
		getChild(name: 'n21'): cradsǁpaidi_XMLǃn21ǃIMAGE
		getChildAt(index: 19): cradsǁpaidi_XMLǃn21ǃIMAGE
		getChildById(id: 'n21_edb2'): cradsǁpaidi_XMLǃn21ǃIMAGE
		getChild(name: 'n22'): cradsǁpaidi_XMLǃn22ǃIMAGE
		getChildAt(index: 20): cradsǁpaidi_XMLǃn22ǃIMAGE
		getChildById(id: 'n22_edb2'): cradsǁpaidi_XMLǃn22ǃIMAGE
		getChild(name: 'n23'): cradsǁpaidi_XMLǃn23ǃIMAGE
		getChildAt(index: 21): cradsǁpaidi_XMLǃn23ǃIMAGE
		getChildById(id: 'n23_edb2'): cradsǁpaidi_XMLǃn23ǃIMAGE
		getChild(name: 'n24'): cradsǁpaidi_XMLǃn24ǃIMAGE
		getChildAt(index: 22): cradsǁpaidi_XMLǃn24ǃIMAGE
		getChildById(id: 'n24_edb2'): cradsǁpaidi_XMLǃn24ǃIMAGE
		getChild(name: 'n25'): cradsǁpaidi_XMLǃn25ǃIMAGE
		getChildAt(index: 23): cradsǁpaidi_XMLǃn25ǃIMAGE
		getChildById(id: 'n25_edb2'): cradsǁpaidi_XMLǃn25ǃIMAGE
		getChild(name: 'n26'): cradsǁpaidi_XMLǃn26ǃIMAGE
		getChildAt(index: 24): cradsǁpaidi_XMLǃn26ǃIMAGE
		getChildById(id: 'n26_edb2'): cradsǁpaidi_XMLǃn26ǃIMAGE
		getChild(name: 'n27'): cradsǁpaidi_XMLǃn27ǃIMAGE
		getChildAt(index: 25): cradsǁpaidi_XMLǃn27ǃIMAGE
		getChildById(id: 'n27_edb2'): cradsǁpaidi_XMLǃn27ǃIMAGE
		getChild(name: 'n28'): cradsǁpaidi_XMLǃn28ǃIMAGE
		getChildAt(index: 26): cradsǁpaidi_XMLǃn28ǃIMAGE
		getChildById(id: 'n28_edb2'): cradsǁpaidi_XMLǃn28ǃIMAGE
		getChild(name: 'n29'): cradsǁpaidi_XMLǃn29ǃIMAGE
		getChildAt(index: 27): cradsǁpaidi_XMLǃn29ǃIMAGE
		getChildById(id: 'n29_edb2'): cradsǁpaidi_XMLǃn29ǃIMAGE
		getChild(name: 'n30'): cradsǁpaidi_XMLǃn30ǃIMAGE
		getChildAt(index: 28): cradsǁpaidi_XMLǃn30ǃIMAGE
		getChildById(id: 'n30_edb2'): cradsǁpaidi_XMLǃn30ǃIMAGE
		getChild(name: 'n31'): cradsǁpaidi_XMLǃn31ǃIMAGE
		getChildAt(index: 29): cradsǁpaidi_XMLǃn31ǃIMAGE
		getChildById(id: 'n31_edb2'): cradsǁpaidi_XMLǃn31ǃIMAGE
		getChild(name: 'n32'): cradsǁpaidi_XMLǃn32ǃIMAGE
		getChildAt(index: 30): cradsǁpaidi_XMLǃn32ǃIMAGE
		getChildById(id: 'n32_edb2'): cradsǁpaidi_XMLǃn32ǃIMAGE
		getChild(name: 'n33'): cradsǁpaidi_XMLǃn33ǃIMAGE
		getChildAt(index: 31): cradsǁpaidi_XMLǃn33ǃIMAGE
		getChildById(id: 'n33_edb2'): cradsǁpaidi_XMLǃn33ǃIMAGE
		getChild(name: 'n34'): cradsǁpaidi_XMLǃn34ǃIMAGE
		getChildAt(index: 32): cradsǁpaidi_XMLǃn34ǃIMAGE
		getChildById(id: 'n34_edb2'): cradsǁpaidi_XMLǃn34ǃIMAGE
		getChild(name: 'n35'): cradsǁpaidi_XMLǃn35ǃIMAGE
		getChildAt(index: 33): cradsǁpaidi_XMLǃn35ǃIMAGE
		getChildById(id: 'n35_edb2'): cradsǁpaidi_XMLǃn35ǃIMAGE
		getChild(name: 'n36'): cradsǁpaidi_XMLǃn36ǃIMAGE
		getChildAt(index: 34): cradsǁpaidi_XMLǃn36ǃIMAGE
		getChildById(id: 'n36_edb2'): cradsǁpaidi_XMLǃn36ǃIMAGE
		getChild(name: 'n37'): cradsǁpaidi_XMLǃn37ǃIMAGE
		getChildAt(index: 35): cradsǁpaidi_XMLǃn37ǃIMAGE
		getChildById(id: 'n37_edb2'): cradsǁpaidi_XMLǃn37ǃIMAGE
		getChild(name: 'n38'): cradsǁpaidi_XMLǃn38ǃIMAGE
		getChildAt(index: 36): cradsǁpaidi_XMLǃn38ǃIMAGE
		getChildById(id: 'n38_edb2'): cradsǁpaidi_XMLǃn38ǃIMAGE
		getChild(name: 'n39'): cradsǁpaidi_XMLǃn39ǃIMAGE
		getChildAt(index: 37): cradsǁpaidi_XMLǃn39ǃIMAGE
		getChildById(id: 'n39_edb2'): cradsǁpaidi_XMLǃn39ǃIMAGE
		getChild(name: 'n40'): cradsǁpaidi_XMLǃn40ǃIMAGE
		getChildAt(index: 38): cradsǁpaidi_XMLǃn40ǃIMAGE
		getChildById(id: 'n40_edb2'): cradsǁpaidi_XMLǃn40ǃIMAGE
		getChild(name: 'n41'): cradsǁpaidi_XMLǃn41ǃIMAGE
		getChildAt(index: 39): cradsǁpaidi_XMLǃn41ǃIMAGE
		getChildById(id: 'n41_edb2'): cradsǁpaidi_XMLǃn41ǃIMAGE
		getChild(name: 'n42'): cradsǁpaidi_XMLǃn42ǃIMAGE
		getChildAt(index: 40): cradsǁpaidi_XMLǃn42ǃIMAGE
		getChildById(id: 'n42_edb2'): cradsǁpaidi_XMLǃn42ǃIMAGE
		getChild(name: 'n43'): cradsǁpaidi_XMLǃn43ǃIMAGE
		getChildAt(index: 41): cradsǁpaidi_XMLǃn43ǃIMAGE
		getChildById(id: 'n43_edb2'): cradsǁpaidi_XMLǃn43ǃIMAGE
		getChild(name: 'n44'): cradsǁpaidi_XMLǃn44ǃIMAGE
		getChildAt(index: 42): cradsǁpaidi_XMLǃn44ǃIMAGE
		getChildById(id: 'n44_edb2'): cradsǁpaidi_XMLǃn44ǃIMAGE
		getChild(name: 'n45'): cradsǁpaidi_XMLǃn45ǃIMAGE
		getChildAt(index: 43): cradsǁpaidi_XMLǃn45ǃIMAGE
		getChildById(id: 'n45_edb2'): cradsǁpaidi_XMLǃn45ǃIMAGE
		getChild(name: 'n46'): cradsǁpaidi_XMLǃn46ǃIMAGE
		getChildAt(index: 44): cradsǁpaidi_XMLǃn46ǃIMAGE
		getChildById(id: 'n46_edb2'): cradsǁpaidi_XMLǃn46ǃIMAGE
		getChild(name: 'n47'): cradsǁpaidi_XMLǃn47ǃIMAGE
		getChildAt(index: 45): cradsǁpaidi_XMLǃn47ǃIMAGE
		getChildById(id: 'n47_edb2'): cradsǁpaidi_XMLǃn47ǃIMAGE
		getChild(name: 'n48'): cradsǁpaidi_XMLǃn48ǃIMAGE
		getChildAt(index: 46): cradsǁpaidi_XMLǃn48ǃIMAGE
		getChildById(id: 'n48_edb2'): cradsǁpaidi_XMLǃn48ǃIMAGE
		getChild(name: 'n49'): cradsǁpaidi_XMLǃn49ǃIMAGE
		getChildAt(index: 47): cradsǁpaidi_XMLǃn49ǃIMAGE
		getChildById(id: 'n49_edb2'): cradsǁpaidi_XMLǃn49ǃIMAGE
		getChild(name: 'n50'): cradsǁpaidi_XMLǃn50ǃIMAGE
		getChildAt(index: 48): cradsǁpaidi_XMLǃn50ǃIMAGE
		getChildById(id: 'n50_edb2'): cradsǁpaidi_XMLǃn50ǃIMAGE
		getChild(name: 'n51'): cradsǁpaidi_XMLǃn51ǃIMAGE
		getChildAt(index: 49): cradsǁpaidi_XMLǃn51ǃIMAGE
		getChildById(id: 'n51_edb2'): cradsǁpaidi_XMLǃn51ǃIMAGE
		getChild(name: 'n52'): cradsǁpaidi_XMLǃn52ǃIMAGE
		getChildAt(index: 50): cradsǁpaidi_XMLǃn52ǃIMAGE
		getChildById(id: 'n52_edb2'): cradsǁpaidi_XMLǃn52ǃIMAGE
		getChild(name: 'n53'): cradsǁpaidi_XMLǃn53ǃIMAGE
		getChildAt(index: 51): cradsǁpaidi_XMLǃn53ǃIMAGE
		getChildById(id: 'n53_edb2'): cradsǁpaidi_XMLǃn53ǃIMAGE
		getChild(name: 'n54'): cradsǁpaidi_XMLǃn54ǃIMAGE
		getChildAt(index: 52): cradsǁpaidi_XMLǃn54ǃIMAGE
		getChildById(id: 'n54_edb2'): cradsǁpaidi_XMLǃn54ǃIMAGE
		getChild(name: 'mask'): cradsǁpaidi_XMLǃmaskǃGRAPH
		getChildAt(index: 53): cradsǁpaidi_XMLǃmaskǃGRAPH
		getChildById(id: 'n56_ajve'): cradsǁpaidi_XMLǃmaskǃGRAPH
		getChild(name: 'face'): cradsǁcaohua_1_pngǃfaceǃLOADER
		getChildAt(index: 54): cradsǁcaohua_1_pngǃfaceǃLOADER
		getChildById(id: 'n55_ajve'): cradsǁcaohua_1_pngǃfaceǃLOADER
		getChild(name: 'finger'): cradsǁpaidi_XMLǃfingerǃIMAGE
		getChildAt(index: 55): cradsǁpaidi_XMLǃfingerǃIMAGE
		getChildById(id: 'n58_hmse'): cradsǁpaidi_XMLǃfingerǃIMAGE
		getChild(name: 'face1'): cradsǁhongxin_1_pngǃface1ǃLOADER
		getChildAt(index: 56): cradsǁhongxin_1_pngǃface1ǃLOADER
		getChildById(id: 'n60_sjy9'): cradsǁhongxin_1_pngǃface1ǃLOADER
		getChild(name: 'n61'): cradsǁpaidi_XMLǃn61ǃIMAGE
		getChildAt(index: 57): cradsǁpaidi_XMLǃn61ǃIMAGE
		getChildById(id: 'n61_sjy9'): cradsǁpaidi_XMLǃn61ǃIMAGE
		getChild(name: 'finger1'): cradsǁpaidi_XMLǃfinger1ǃIMAGE
		getChildAt(index: 58): cradsǁpaidi_XMLǃfinger1ǃIMAGE
		getChildById(id: 'n62_sjy9'): cradsǁpaidi_XMLǃfinger1ǃIMAGE
		_children: [
			cradsǁpaidi_XMLǃn2ǃIMAGE,
			cradsǁpaidi_XMLǃn3ǃIMAGE,
			cradsǁpaidi_XMLǃn4ǃIMAGE,
			cradsǁpaidi_XMLǃn5ǃIMAGE,
			cradsǁpaidi_XMLǃn6ǃIMAGE,
			cradsǁpaidi_XMLǃn7ǃIMAGE,
			cradsǁpaidi_XMLǃn8ǃIMAGE,
			cradsǁpaidi_XMLǃn9ǃIMAGE,
			cradsǁpaidi_XMLǃn10ǃIMAGE,
			cradsǁpaidi_XMLǃn11ǃIMAGE,
			cradsǁpaidi_XMLǃn12ǃIMAGE,
			cradsǁpaidi_XMLǃn13ǃIMAGE,
			cradsǁpaidi_XMLǃn14ǃIMAGE,
			cradsǁpaidi_XMLǃn15ǃIMAGE,
			cradsǁpaidi_XMLǃn16ǃIMAGE,
			cradsǁpaidi_XMLǃn17ǃIMAGE,
			cradsǁpaidi_XMLǃn18ǃIMAGE,
			cradsǁpaidi_XMLǃn19ǃIMAGE,
			cradsǁpaidi_XMLǃn20ǃIMAGE,
			cradsǁpaidi_XMLǃn21ǃIMAGE,
			cradsǁpaidi_XMLǃn22ǃIMAGE,
			cradsǁpaidi_XMLǃn23ǃIMAGE,
			cradsǁpaidi_XMLǃn24ǃIMAGE,
			cradsǁpaidi_XMLǃn25ǃIMAGE,
			cradsǁpaidi_XMLǃn26ǃIMAGE,
			cradsǁpaidi_XMLǃn27ǃIMAGE,
			cradsǁpaidi_XMLǃn28ǃIMAGE,
			cradsǁpaidi_XMLǃn29ǃIMAGE,
			cradsǁpaidi_XMLǃn30ǃIMAGE,
			cradsǁpaidi_XMLǃn31ǃIMAGE,
			cradsǁpaidi_XMLǃn32ǃIMAGE,
			cradsǁpaidi_XMLǃn33ǃIMAGE,
			cradsǁpaidi_XMLǃn34ǃIMAGE,
			cradsǁpaidi_XMLǃn35ǃIMAGE,
			cradsǁpaidi_XMLǃn36ǃIMAGE,
			cradsǁpaidi_XMLǃn37ǃIMAGE,
			cradsǁpaidi_XMLǃn38ǃIMAGE,
			cradsǁpaidi_XMLǃn39ǃIMAGE,
			cradsǁpaidi_XMLǃn40ǃIMAGE,
			cradsǁpaidi_XMLǃn41ǃIMAGE,
			cradsǁpaidi_XMLǃn42ǃIMAGE,
			cradsǁpaidi_XMLǃn43ǃIMAGE,
			cradsǁpaidi_XMLǃn44ǃIMAGE,
			cradsǁpaidi_XMLǃn45ǃIMAGE,
			cradsǁpaidi_XMLǃn46ǃIMAGE,
			cradsǁpaidi_XMLǃn47ǃIMAGE,
			cradsǁpaidi_XMLǃn48ǃIMAGE,
			cradsǁpaidi_XMLǃn49ǃIMAGE,
			cradsǁpaidi_XMLǃn50ǃIMAGE,
			cradsǁpaidi_XMLǃn51ǃIMAGE,
			cradsǁpaidi_XMLǃn52ǃIMAGE,
			cradsǁpaidi_XMLǃn53ǃIMAGE,
			cradsǁpaidi_XMLǃn54ǃIMAGE,
			cradsǁpaidi_XMLǃmaskǃGRAPH,
			cradsǁcaohua_1_pngǃfaceǃLOADER,
			cradsǁpaidi_XMLǃfingerǃIMAGE,
			cradsǁhongxin_1_pngǃface1ǃLOADER,
			cradsǁpaidi_XMLǃn61ǃIMAGE,
			cradsǁpaidi_XMLǃfinger1ǃIMAGE
		]
		getTransition(name: 't1'): cradsǁpaidi_XMLǃt1ǃTRANSITION
		getTransitionAt(index: 0): cradsǁpaidi_XMLǃt1ǃTRANSITION
		getTransition(name: 't2'): cradsǁpaidi_XMLǃt2ǃTRANSITION
		getTransitionAt(index: 1): cradsǁpaidi_XMLǃt2ǃTRANSITION
		getTransition(name: 't4'): cradsǁpaidi_XMLǃt4ǃTRANSITION
		getTransitionAt(index: 2): cradsǁpaidi_XMLǃt4ǃTRANSITION
		_transitions: [
			cradsǁpaidi_XMLǃt1ǃTRANSITION,
			cradsǁpaidi_XMLǃt2ǃTRANSITION,
			cradsǁpaidi_XMLǃt4ǃTRANSITION
		]
		getController(name: '__language'): cradsǁpaidi_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): cradsǁpaidi_XMLǃ__languageǃCONTROLLER
		getController(name: 'c1'): cradsǁpaidi_XMLǃc1ǃCONTROLLER
		getControllerAt(index: 1): cradsǁpaidi_XMLǃc1ǃCONTROLLER
		_controllers: [
			cradsǁpaidi_XMLǃ__languageǃCONTROLLER,
			cradsǁpaidi_XMLǃc1ǃCONTROLLER
		]
	}
	interface cradsǁpaidi_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃc1ǃCONTROLLER extends fairygui.Controller{
		_parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn2ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn3ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn4ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn5ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn6ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn7ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn8ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn9ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn10ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn11ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn12ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn13ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn14ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn15ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn16ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn17ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn18ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn19ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn20ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn21ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn22ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn23ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn24ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn25ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn26ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn27ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn28ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn29ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn30ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn31ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn32ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn33ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn34ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn35ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn36ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn37ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn38ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn39ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn40ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn41ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn42ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn43ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn44ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn45ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn46ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn47ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn48ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn49ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn50ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn51ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn52ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn53ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn54ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃmaskǃGRAPH extends fairygui.GGraph{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁcaohua_1_pngǃfaceǃLOADER extends fairygui.GLoader{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃfingerǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁhongxin_1_pngǃface1ǃLOADER extends fairygui.GLoader{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃn61ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃfinger1ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃt1ǃTRANSITION extends fairygui.Transition{
		_owner: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃt2ǃTRANSITION extends fairygui.Transition{
		_owner: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃt4ǃTRANSITION extends fairygui.Transition{
		_owner: cradsǁpaidi_XML
	}
	interface cradsǁpaidi_XMLǃfapai_08ǃCOMPONENT extends cradsǁpaidi_XML{
		parent: Main_XML
	}
	interface cradsǁpaidi_XMLǃfapai_07ǃCOMPONENT extends cradsǁpaidi_XML{
		parent: Main_XML
	}
	interface cradsǁpaidi2_XML extends fairygui.GComponent{
		getChild(name: 'n2'): cradsǁpaidi2_XMLǃn2ǃIMAGE
		getChildAt(index: 0): cradsǁpaidi2_XMLǃn2ǃIMAGE
		getChildById(id: 'n2_f7j0'): cradsǁpaidi2_XMLǃn2ǃIMAGE
		getChild(name: 'n3'): cradsǁpaidi2_XMLǃn3ǃIMAGE
		getChildAt(index: 1): cradsǁpaidi2_XMLǃn3ǃIMAGE
		getChildById(id: 'n3_edb2'): cradsǁpaidi2_XMLǃn3ǃIMAGE
		getChild(name: 'n4'): cradsǁpaidi2_XMLǃn4ǃIMAGE
		getChildAt(index: 2): cradsǁpaidi2_XMLǃn4ǃIMAGE
		getChildById(id: 'n4_edb2'): cradsǁpaidi2_XMLǃn4ǃIMAGE
		getChild(name: 'n5'): cradsǁpaidi2_XMLǃn5ǃIMAGE
		getChildAt(index: 3): cradsǁpaidi2_XMLǃn5ǃIMAGE
		getChildById(id: 'n5_edb2'): cradsǁpaidi2_XMLǃn5ǃIMAGE
		getChild(name: 'n6'): cradsǁpaidi2_XMLǃn6ǃIMAGE
		getChildAt(index: 4): cradsǁpaidi2_XMLǃn6ǃIMAGE
		getChildById(id: 'n6_edb2'): cradsǁpaidi2_XMLǃn6ǃIMAGE
		getChild(name: 'n7'): cradsǁpaidi2_XMLǃn7ǃIMAGE
		getChildAt(index: 5): cradsǁpaidi2_XMLǃn7ǃIMAGE
		getChildById(id: 'n7_edb2'): cradsǁpaidi2_XMLǃn7ǃIMAGE
		getChild(name: 'n8'): cradsǁpaidi2_XMLǃn8ǃIMAGE
		getChildAt(index: 6): cradsǁpaidi2_XMLǃn8ǃIMAGE
		getChildById(id: 'n8_edb2'): cradsǁpaidi2_XMLǃn8ǃIMAGE
		getChild(name: 'n9'): cradsǁpaidi2_XMLǃn9ǃIMAGE
		getChildAt(index: 7): cradsǁpaidi2_XMLǃn9ǃIMAGE
		getChildById(id: 'n9_edb2'): cradsǁpaidi2_XMLǃn9ǃIMAGE
		getChild(name: 'n10'): cradsǁpaidi2_XMLǃn10ǃIMAGE
		getChildAt(index: 8): cradsǁpaidi2_XMLǃn10ǃIMAGE
		getChildById(id: 'n10_edb2'): cradsǁpaidi2_XMLǃn10ǃIMAGE
		getChild(name: 'n11'): cradsǁpaidi2_XMLǃn11ǃIMAGE
		getChildAt(index: 9): cradsǁpaidi2_XMLǃn11ǃIMAGE
		getChildById(id: 'n11_edb2'): cradsǁpaidi2_XMLǃn11ǃIMAGE
		getChild(name: 'n12'): cradsǁpaidi2_XMLǃn12ǃIMAGE
		getChildAt(index: 10): cradsǁpaidi2_XMLǃn12ǃIMAGE
		getChildById(id: 'n12_edb2'): cradsǁpaidi2_XMLǃn12ǃIMAGE
		getChild(name: 'n13'): cradsǁpaidi2_XMLǃn13ǃIMAGE
		getChildAt(index: 11): cradsǁpaidi2_XMLǃn13ǃIMAGE
		getChildById(id: 'n13_edb2'): cradsǁpaidi2_XMLǃn13ǃIMAGE
		getChild(name: 'n14'): cradsǁpaidi2_XMLǃn14ǃIMAGE
		getChildAt(index: 12): cradsǁpaidi2_XMLǃn14ǃIMAGE
		getChildById(id: 'n14_edb2'): cradsǁpaidi2_XMLǃn14ǃIMAGE
		getChild(name: 'n15'): cradsǁpaidi2_XMLǃn15ǃIMAGE
		getChildAt(index: 13): cradsǁpaidi2_XMLǃn15ǃIMAGE
		getChildById(id: 'n15_edb2'): cradsǁpaidi2_XMLǃn15ǃIMAGE
		getChild(name: 'n16'): cradsǁpaidi2_XMLǃn16ǃIMAGE
		getChildAt(index: 14): cradsǁpaidi2_XMLǃn16ǃIMAGE
		getChildById(id: 'n16_edb2'): cradsǁpaidi2_XMLǃn16ǃIMAGE
		getChild(name: 'n17'): cradsǁpaidi2_XMLǃn17ǃIMAGE
		getChildAt(index: 15): cradsǁpaidi2_XMLǃn17ǃIMAGE
		getChildById(id: 'n17_edb2'): cradsǁpaidi2_XMLǃn17ǃIMAGE
		getChild(name: 'n18'): cradsǁpaidi2_XMLǃn18ǃIMAGE
		getChildAt(index: 16): cradsǁpaidi2_XMLǃn18ǃIMAGE
		getChildById(id: 'n18_edb2'): cradsǁpaidi2_XMLǃn18ǃIMAGE
		getChild(name: 'n19'): cradsǁpaidi2_XMLǃn19ǃIMAGE
		getChildAt(index: 17): cradsǁpaidi2_XMLǃn19ǃIMAGE
		getChildById(id: 'n19_edb2'): cradsǁpaidi2_XMLǃn19ǃIMAGE
		getChild(name: 'n20'): cradsǁpaidi2_XMLǃn20ǃIMAGE
		getChildAt(index: 18): cradsǁpaidi2_XMLǃn20ǃIMAGE
		getChildById(id: 'n20_edb2'): cradsǁpaidi2_XMLǃn20ǃIMAGE
		getChild(name: 'n21'): cradsǁpaidi2_XMLǃn21ǃIMAGE
		getChildAt(index: 19): cradsǁpaidi2_XMLǃn21ǃIMAGE
		getChildById(id: 'n21_edb2'): cradsǁpaidi2_XMLǃn21ǃIMAGE
		getChild(name: 'n22'): cradsǁpaidi2_XMLǃn22ǃIMAGE
		getChildAt(index: 20): cradsǁpaidi2_XMLǃn22ǃIMAGE
		getChildById(id: 'n22_edb2'): cradsǁpaidi2_XMLǃn22ǃIMAGE
		getChild(name: 'n23'): cradsǁpaidi2_XMLǃn23ǃIMAGE
		getChildAt(index: 21): cradsǁpaidi2_XMLǃn23ǃIMAGE
		getChildById(id: 'n23_edb2'): cradsǁpaidi2_XMLǃn23ǃIMAGE
		getChild(name: 'n24'): cradsǁpaidi2_XMLǃn24ǃIMAGE
		getChildAt(index: 22): cradsǁpaidi2_XMLǃn24ǃIMAGE
		getChildById(id: 'n24_edb2'): cradsǁpaidi2_XMLǃn24ǃIMAGE
		getChild(name: 'n25'): cradsǁpaidi2_XMLǃn25ǃIMAGE
		getChildAt(index: 23): cradsǁpaidi2_XMLǃn25ǃIMAGE
		getChildById(id: 'n25_edb2'): cradsǁpaidi2_XMLǃn25ǃIMAGE
		getChild(name: 'n26'): cradsǁpaidi2_XMLǃn26ǃIMAGE
		getChildAt(index: 24): cradsǁpaidi2_XMLǃn26ǃIMAGE
		getChildById(id: 'n26_edb2'): cradsǁpaidi2_XMLǃn26ǃIMAGE
		getChild(name: 'n27'): cradsǁpaidi2_XMLǃn27ǃIMAGE
		getChildAt(index: 25): cradsǁpaidi2_XMLǃn27ǃIMAGE
		getChildById(id: 'n27_edb2'): cradsǁpaidi2_XMLǃn27ǃIMAGE
		getChild(name: 'n28'): cradsǁpaidi2_XMLǃn28ǃIMAGE
		getChildAt(index: 26): cradsǁpaidi2_XMLǃn28ǃIMAGE
		getChildById(id: 'n28_edb2'): cradsǁpaidi2_XMLǃn28ǃIMAGE
		getChild(name: 'n29'): cradsǁpaidi2_XMLǃn29ǃIMAGE
		getChildAt(index: 27): cradsǁpaidi2_XMLǃn29ǃIMAGE
		getChildById(id: 'n29_edb2'): cradsǁpaidi2_XMLǃn29ǃIMAGE
		getChild(name: 'n30'): cradsǁpaidi2_XMLǃn30ǃIMAGE
		getChildAt(index: 28): cradsǁpaidi2_XMLǃn30ǃIMAGE
		getChildById(id: 'n30_edb2'): cradsǁpaidi2_XMLǃn30ǃIMAGE
		getChild(name: 'n31'): cradsǁpaidi2_XMLǃn31ǃIMAGE
		getChildAt(index: 29): cradsǁpaidi2_XMLǃn31ǃIMAGE
		getChildById(id: 'n31_edb2'): cradsǁpaidi2_XMLǃn31ǃIMAGE
		getChild(name: 'n32'): cradsǁpaidi2_XMLǃn32ǃIMAGE
		getChildAt(index: 30): cradsǁpaidi2_XMLǃn32ǃIMAGE
		getChildById(id: 'n32_edb2'): cradsǁpaidi2_XMLǃn32ǃIMAGE
		getChild(name: 'n33'): cradsǁpaidi2_XMLǃn33ǃIMAGE
		getChildAt(index: 31): cradsǁpaidi2_XMLǃn33ǃIMAGE
		getChildById(id: 'n33_edb2'): cradsǁpaidi2_XMLǃn33ǃIMAGE
		getChild(name: 'n34'): cradsǁpaidi2_XMLǃn34ǃIMAGE
		getChildAt(index: 32): cradsǁpaidi2_XMLǃn34ǃIMAGE
		getChildById(id: 'n34_edb2'): cradsǁpaidi2_XMLǃn34ǃIMAGE
		getChild(name: 'n35'): cradsǁpaidi2_XMLǃn35ǃIMAGE
		getChildAt(index: 33): cradsǁpaidi2_XMLǃn35ǃIMAGE
		getChildById(id: 'n35_edb2'): cradsǁpaidi2_XMLǃn35ǃIMAGE
		getChild(name: 'n36'): cradsǁpaidi2_XMLǃn36ǃIMAGE
		getChildAt(index: 34): cradsǁpaidi2_XMLǃn36ǃIMAGE
		getChildById(id: 'n36_edb2'): cradsǁpaidi2_XMLǃn36ǃIMAGE
		getChild(name: 'n37'): cradsǁpaidi2_XMLǃn37ǃIMAGE
		getChildAt(index: 35): cradsǁpaidi2_XMLǃn37ǃIMAGE
		getChildById(id: 'n37_edb2'): cradsǁpaidi2_XMLǃn37ǃIMAGE
		getChild(name: 'n38'): cradsǁpaidi2_XMLǃn38ǃIMAGE
		getChildAt(index: 36): cradsǁpaidi2_XMLǃn38ǃIMAGE
		getChildById(id: 'n38_edb2'): cradsǁpaidi2_XMLǃn38ǃIMAGE
		getChild(name: 'n39'): cradsǁpaidi2_XMLǃn39ǃIMAGE
		getChildAt(index: 37): cradsǁpaidi2_XMLǃn39ǃIMAGE
		getChildById(id: 'n39_edb2'): cradsǁpaidi2_XMLǃn39ǃIMAGE
		getChild(name: 'n40'): cradsǁpaidi2_XMLǃn40ǃIMAGE
		getChildAt(index: 38): cradsǁpaidi2_XMLǃn40ǃIMAGE
		getChildById(id: 'n40_edb2'): cradsǁpaidi2_XMLǃn40ǃIMAGE
		getChild(name: 'n41'): cradsǁpaidi2_XMLǃn41ǃIMAGE
		getChildAt(index: 39): cradsǁpaidi2_XMLǃn41ǃIMAGE
		getChildById(id: 'n41_edb2'): cradsǁpaidi2_XMLǃn41ǃIMAGE
		getChild(name: 'n42'): cradsǁpaidi2_XMLǃn42ǃIMAGE
		getChildAt(index: 40): cradsǁpaidi2_XMLǃn42ǃIMAGE
		getChildById(id: 'n42_edb2'): cradsǁpaidi2_XMLǃn42ǃIMAGE
		getChild(name: 'n43'): cradsǁpaidi2_XMLǃn43ǃIMAGE
		getChildAt(index: 41): cradsǁpaidi2_XMLǃn43ǃIMAGE
		getChildById(id: 'n43_edb2'): cradsǁpaidi2_XMLǃn43ǃIMAGE
		getChild(name: 'n44'): cradsǁpaidi2_XMLǃn44ǃIMAGE
		getChildAt(index: 42): cradsǁpaidi2_XMLǃn44ǃIMAGE
		getChildById(id: 'n44_edb2'): cradsǁpaidi2_XMLǃn44ǃIMAGE
		getChild(name: 'n45'): cradsǁpaidi2_XMLǃn45ǃIMAGE
		getChildAt(index: 43): cradsǁpaidi2_XMLǃn45ǃIMAGE
		getChildById(id: 'n45_edb2'): cradsǁpaidi2_XMLǃn45ǃIMAGE
		getChild(name: 'n46'): cradsǁpaidi2_XMLǃn46ǃIMAGE
		getChildAt(index: 44): cradsǁpaidi2_XMLǃn46ǃIMAGE
		getChildById(id: 'n46_edb2'): cradsǁpaidi2_XMLǃn46ǃIMAGE
		getChild(name: 'n47'): cradsǁpaidi2_XMLǃn47ǃIMAGE
		getChildAt(index: 45): cradsǁpaidi2_XMLǃn47ǃIMAGE
		getChildById(id: 'n47_edb2'): cradsǁpaidi2_XMLǃn47ǃIMAGE
		getChild(name: 'n48'): cradsǁpaidi2_XMLǃn48ǃIMAGE
		getChildAt(index: 46): cradsǁpaidi2_XMLǃn48ǃIMAGE
		getChildById(id: 'n48_edb2'): cradsǁpaidi2_XMLǃn48ǃIMAGE
		getChild(name: 'n49'): cradsǁpaidi2_XMLǃn49ǃIMAGE
		getChildAt(index: 47): cradsǁpaidi2_XMLǃn49ǃIMAGE
		getChildById(id: 'n49_edb2'): cradsǁpaidi2_XMLǃn49ǃIMAGE
		getChild(name: 'n50'): cradsǁpaidi2_XMLǃn50ǃIMAGE
		getChildAt(index: 48): cradsǁpaidi2_XMLǃn50ǃIMAGE
		getChildById(id: 'n50_edb2'): cradsǁpaidi2_XMLǃn50ǃIMAGE
		getChild(name: 'n51'): cradsǁpaidi2_XMLǃn51ǃIMAGE
		getChildAt(index: 49): cradsǁpaidi2_XMLǃn51ǃIMAGE
		getChildById(id: 'n51_edb2'): cradsǁpaidi2_XMLǃn51ǃIMAGE
		getChild(name: 'n52'): cradsǁpaidi2_XMLǃn52ǃIMAGE
		getChildAt(index: 50): cradsǁpaidi2_XMLǃn52ǃIMAGE
		getChildById(id: 'n52_edb2'): cradsǁpaidi2_XMLǃn52ǃIMAGE
		getChild(name: 'n53'): cradsǁpaidi2_XMLǃn53ǃIMAGE
		getChildAt(index: 51): cradsǁpaidi2_XMLǃn53ǃIMAGE
		getChildById(id: 'n53_edb2'): cradsǁpaidi2_XMLǃn53ǃIMAGE
		getChild(name: 'n54'): cradsǁpaidi2_XMLǃn54ǃIMAGE
		getChildAt(index: 52): cradsǁpaidi2_XMLǃn54ǃIMAGE
		getChildById(id: 'n54_edb2'): cradsǁpaidi2_XMLǃn54ǃIMAGE
		getChild(name: 'mask'): cradsǁpaidi2_XMLǃmaskǃGRAPH
		getChildAt(index: 53): cradsǁpaidi2_XMLǃmaskǃGRAPH
		getChildById(id: 'n56_ajve'): cradsǁpaidi2_XMLǃmaskǃGRAPH
		getChild(name: 'finger'): cradsǁpaidi2_XMLǃfingerǃIMAGE
		getChildAt(index: 54): cradsǁpaidi2_XMLǃfingerǃIMAGE
		getChildById(id: 'n58_hmse'): cradsǁpaidi2_XMLǃfingerǃIMAGE
		getChild(name: 'n61'): cradsǁpaidi2_XMLǃn61ǃIMAGE
		getChildAt(index: 55): cradsǁpaidi2_XMLǃn61ǃIMAGE
		getChildById(id: 'n61_sjy9'): cradsǁpaidi2_XMLǃn61ǃIMAGE
		getChild(name: 'finger1'): cradsǁpaidi2_XMLǃfinger1ǃIMAGE
		getChildAt(index: 56): cradsǁpaidi2_XMLǃfinger1ǃIMAGE
		getChildById(id: 'n62_sjy9'): cradsǁpaidi2_XMLǃfinger1ǃIMAGE
		_children: [
			cradsǁpaidi2_XMLǃn2ǃIMAGE,
			cradsǁpaidi2_XMLǃn3ǃIMAGE,
			cradsǁpaidi2_XMLǃn4ǃIMAGE,
			cradsǁpaidi2_XMLǃn5ǃIMAGE,
			cradsǁpaidi2_XMLǃn6ǃIMAGE,
			cradsǁpaidi2_XMLǃn7ǃIMAGE,
			cradsǁpaidi2_XMLǃn8ǃIMAGE,
			cradsǁpaidi2_XMLǃn9ǃIMAGE,
			cradsǁpaidi2_XMLǃn10ǃIMAGE,
			cradsǁpaidi2_XMLǃn11ǃIMAGE,
			cradsǁpaidi2_XMLǃn12ǃIMAGE,
			cradsǁpaidi2_XMLǃn13ǃIMAGE,
			cradsǁpaidi2_XMLǃn14ǃIMAGE,
			cradsǁpaidi2_XMLǃn15ǃIMAGE,
			cradsǁpaidi2_XMLǃn16ǃIMAGE,
			cradsǁpaidi2_XMLǃn17ǃIMAGE,
			cradsǁpaidi2_XMLǃn18ǃIMAGE,
			cradsǁpaidi2_XMLǃn19ǃIMAGE,
			cradsǁpaidi2_XMLǃn20ǃIMAGE,
			cradsǁpaidi2_XMLǃn21ǃIMAGE,
			cradsǁpaidi2_XMLǃn22ǃIMAGE,
			cradsǁpaidi2_XMLǃn23ǃIMAGE,
			cradsǁpaidi2_XMLǃn24ǃIMAGE,
			cradsǁpaidi2_XMLǃn25ǃIMAGE,
			cradsǁpaidi2_XMLǃn26ǃIMAGE,
			cradsǁpaidi2_XMLǃn27ǃIMAGE,
			cradsǁpaidi2_XMLǃn28ǃIMAGE,
			cradsǁpaidi2_XMLǃn29ǃIMAGE,
			cradsǁpaidi2_XMLǃn30ǃIMAGE,
			cradsǁpaidi2_XMLǃn31ǃIMAGE,
			cradsǁpaidi2_XMLǃn32ǃIMAGE,
			cradsǁpaidi2_XMLǃn33ǃIMAGE,
			cradsǁpaidi2_XMLǃn34ǃIMAGE,
			cradsǁpaidi2_XMLǃn35ǃIMAGE,
			cradsǁpaidi2_XMLǃn36ǃIMAGE,
			cradsǁpaidi2_XMLǃn37ǃIMAGE,
			cradsǁpaidi2_XMLǃn38ǃIMAGE,
			cradsǁpaidi2_XMLǃn39ǃIMAGE,
			cradsǁpaidi2_XMLǃn40ǃIMAGE,
			cradsǁpaidi2_XMLǃn41ǃIMAGE,
			cradsǁpaidi2_XMLǃn42ǃIMAGE,
			cradsǁpaidi2_XMLǃn43ǃIMAGE,
			cradsǁpaidi2_XMLǃn44ǃIMAGE,
			cradsǁpaidi2_XMLǃn45ǃIMAGE,
			cradsǁpaidi2_XMLǃn46ǃIMAGE,
			cradsǁpaidi2_XMLǃn47ǃIMAGE,
			cradsǁpaidi2_XMLǃn48ǃIMAGE,
			cradsǁpaidi2_XMLǃn49ǃIMAGE,
			cradsǁpaidi2_XMLǃn50ǃIMAGE,
			cradsǁpaidi2_XMLǃn51ǃIMAGE,
			cradsǁpaidi2_XMLǃn52ǃIMAGE,
			cradsǁpaidi2_XMLǃn53ǃIMAGE,
			cradsǁpaidi2_XMLǃn54ǃIMAGE,
			cradsǁpaidi2_XMLǃmaskǃGRAPH,
			cradsǁpaidi2_XMLǃfingerǃIMAGE,
			cradsǁpaidi2_XMLǃn61ǃIMAGE,
			cradsǁpaidi2_XMLǃfinger1ǃIMAGE
		]
		getTransition(name: 't1'): cradsǁpaidi2_XMLǃt1ǃTRANSITION
		getTransitionAt(index: 0): cradsǁpaidi2_XMLǃt1ǃTRANSITION
		getTransition(name: 't2'): cradsǁpaidi2_XMLǃt2ǃTRANSITION
		getTransitionAt(index: 1): cradsǁpaidi2_XMLǃt2ǃTRANSITION
		getTransition(name: 't4'): cradsǁpaidi2_XMLǃt4ǃTRANSITION
		getTransitionAt(index: 2): cradsǁpaidi2_XMLǃt4ǃTRANSITION
		_transitions: [
			cradsǁpaidi2_XMLǃt1ǃTRANSITION,
			cradsǁpaidi2_XMLǃt2ǃTRANSITION,
			cradsǁpaidi2_XMLǃt4ǃTRANSITION
		]
		getController(name: '__language'): cradsǁpaidi2_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): cradsǁpaidi2_XMLǃ__languageǃCONTROLLER
		getController(name: 'c1'): cradsǁpaidi2_XMLǃc1ǃCONTROLLER
		getControllerAt(index: 1): cradsǁpaidi2_XMLǃc1ǃCONTROLLER
		_controllers: [
			cradsǁpaidi2_XMLǃ__languageǃCONTROLLER,
			cradsǁpaidi2_XMLǃc1ǃCONTROLLER
		]
	}
	interface cradsǁpaidi2_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃc1ǃCONTROLLER extends fairygui.Controller{
		_parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn2ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn3ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn4ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn5ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn6ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn7ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn8ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn9ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn10ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn11ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn12ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn13ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn14ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn15ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn16ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn17ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn18ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn19ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn20ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn21ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn22ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn23ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn24ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn25ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn26ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn27ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn28ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn29ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn30ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn31ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn32ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn33ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn34ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn35ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn36ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn37ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn38ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn39ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn40ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn41ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn42ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn43ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn44ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn45ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn46ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn47ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn48ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn49ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn50ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn51ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn52ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn53ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn54ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃmaskǃGRAPH extends fairygui.GGraph{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃfingerǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃn61ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃfinger1ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃt1ǃTRANSITION extends fairygui.Transition{
		_owner: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃt2ǃTRANSITION extends fairygui.Transition{
		_owner: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃt4ǃTRANSITION extends fairygui.Transition{
		_owner: cradsǁpaidi2_XML
	}
	interface cradsǁpaidi2_XMLǃfapai_06ǃCOMPONENT extends cradsǁpaidi2_XML{
		parent: Main_XML
	}
	interface cradsǁpaidi2_XMLǃfapai_05ǃCOMPONENT extends cradsǁpaidi2_XML{
		parent: Main_XML
	}
	interface cradsǁpaidi4_XML extends fairygui.GComponent{
		getChild(name: 'n2'): cradsǁpaidi4_XMLǃn2ǃIMAGE
		getChildAt(index: 0): cradsǁpaidi4_XMLǃn2ǃIMAGE
		getChildById(id: 'n2_f7j0'): cradsǁpaidi4_XMLǃn2ǃIMAGE
		getChild(name: 'n3'): cradsǁpaidi4_XMLǃn3ǃIMAGE
		getChildAt(index: 1): cradsǁpaidi4_XMLǃn3ǃIMAGE
		getChildById(id: 'n3_edb2'): cradsǁpaidi4_XMLǃn3ǃIMAGE
		getChild(name: 'n4'): cradsǁpaidi4_XMLǃn4ǃIMAGE
		getChildAt(index: 2): cradsǁpaidi4_XMLǃn4ǃIMAGE
		getChildById(id: 'n4_edb2'): cradsǁpaidi4_XMLǃn4ǃIMAGE
		getChild(name: 'n5'): cradsǁpaidi4_XMLǃn5ǃIMAGE
		getChildAt(index: 3): cradsǁpaidi4_XMLǃn5ǃIMAGE
		getChildById(id: 'n5_edb2'): cradsǁpaidi4_XMLǃn5ǃIMAGE
		getChild(name: 'n6'): cradsǁpaidi4_XMLǃn6ǃIMAGE
		getChildAt(index: 4): cradsǁpaidi4_XMLǃn6ǃIMAGE
		getChildById(id: 'n6_edb2'): cradsǁpaidi4_XMLǃn6ǃIMAGE
		getChild(name: 'n7'): cradsǁpaidi4_XMLǃn7ǃIMAGE
		getChildAt(index: 5): cradsǁpaidi4_XMLǃn7ǃIMAGE
		getChildById(id: 'n7_edb2'): cradsǁpaidi4_XMLǃn7ǃIMAGE
		getChild(name: 'n8'): cradsǁpaidi4_XMLǃn8ǃIMAGE
		getChildAt(index: 6): cradsǁpaidi4_XMLǃn8ǃIMAGE
		getChildById(id: 'n8_edb2'): cradsǁpaidi4_XMLǃn8ǃIMAGE
		getChild(name: 'n9'): cradsǁpaidi4_XMLǃn9ǃIMAGE
		getChildAt(index: 7): cradsǁpaidi4_XMLǃn9ǃIMAGE
		getChildById(id: 'n9_edb2'): cradsǁpaidi4_XMLǃn9ǃIMAGE
		getChild(name: 'n10'): cradsǁpaidi4_XMLǃn10ǃIMAGE
		getChildAt(index: 8): cradsǁpaidi4_XMLǃn10ǃIMAGE
		getChildById(id: 'n10_edb2'): cradsǁpaidi4_XMLǃn10ǃIMAGE
		getChild(name: 'n11'): cradsǁpaidi4_XMLǃn11ǃIMAGE
		getChildAt(index: 9): cradsǁpaidi4_XMLǃn11ǃIMAGE
		getChildById(id: 'n11_edb2'): cradsǁpaidi4_XMLǃn11ǃIMAGE
		getChild(name: 'n12'): cradsǁpaidi4_XMLǃn12ǃIMAGE
		getChildAt(index: 10): cradsǁpaidi4_XMLǃn12ǃIMAGE
		getChildById(id: 'n12_edb2'): cradsǁpaidi4_XMLǃn12ǃIMAGE
		getChild(name: 'n13'): cradsǁpaidi4_XMLǃn13ǃIMAGE
		getChildAt(index: 11): cradsǁpaidi4_XMLǃn13ǃIMAGE
		getChildById(id: 'n13_edb2'): cradsǁpaidi4_XMLǃn13ǃIMAGE
		getChild(name: 'n14'): cradsǁpaidi4_XMLǃn14ǃIMAGE
		getChildAt(index: 12): cradsǁpaidi4_XMLǃn14ǃIMAGE
		getChildById(id: 'n14_edb2'): cradsǁpaidi4_XMLǃn14ǃIMAGE
		getChild(name: 'n15'): cradsǁpaidi4_XMLǃn15ǃIMAGE
		getChildAt(index: 13): cradsǁpaidi4_XMLǃn15ǃIMAGE
		getChildById(id: 'n15_edb2'): cradsǁpaidi4_XMLǃn15ǃIMAGE
		getChild(name: 'n16'): cradsǁpaidi4_XMLǃn16ǃIMAGE
		getChildAt(index: 14): cradsǁpaidi4_XMLǃn16ǃIMAGE
		getChildById(id: 'n16_edb2'): cradsǁpaidi4_XMLǃn16ǃIMAGE
		getChild(name: 'n17'): cradsǁpaidi4_XMLǃn17ǃIMAGE
		getChildAt(index: 15): cradsǁpaidi4_XMLǃn17ǃIMAGE
		getChildById(id: 'n17_edb2'): cradsǁpaidi4_XMLǃn17ǃIMAGE
		getChild(name: 'n18'): cradsǁpaidi4_XMLǃn18ǃIMAGE
		getChildAt(index: 16): cradsǁpaidi4_XMLǃn18ǃIMAGE
		getChildById(id: 'n18_edb2'): cradsǁpaidi4_XMLǃn18ǃIMAGE
		getChild(name: 'n19'): cradsǁpaidi4_XMLǃn19ǃIMAGE
		getChildAt(index: 17): cradsǁpaidi4_XMLǃn19ǃIMAGE
		getChildById(id: 'n19_edb2'): cradsǁpaidi4_XMLǃn19ǃIMAGE
		getChild(name: 'n20'): cradsǁpaidi4_XMLǃn20ǃIMAGE
		getChildAt(index: 18): cradsǁpaidi4_XMLǃn20ǃIMAGE
		getChildById(id: 'n20_edb2'): cradsǁpaidi4_XMLǃn20ǃIMAGE
		getChild(name: 'n21'): cradsǁpaidi4_XMLǃn21ǃIMAGE
		getChildAt(index: 19): cradsǁpaidi4_XMLǃn21ǃIMAGE
		getChildById(id: 'n21_edb2'): cradsǁpaidi4_XMLǃn21ǃIMAGE
		getChild(name: 'n22'): cradsǁpaidi4_XMLǃn22ǃIMAGE
		getChildAt(index: 20): cradsǁpaidi4_XMLǃn22ǃIMAGE
		getChildById(id: 'n22_edb2'): cradsǁpaidi4_XMLǃn22ǃIMAGE
		getChild(name: 'n23'): cradsǁpaidi4_XMLǃn23ǃIMAGE
		getChildAt(index: 21): cradsǁpaidi4_XMLǃn23ǃIMAGE
		getChildById(id: 'n23_edb2'): cradsǁpaidi4_XMLǃn23ǃIMAGE
		getChild(name: 'n24'): cradsǁpaidi4_XMLǃn24ǃIMAGE
		getChildAt(index: 22): cradsǁpaidi4_XMLǃn24ǃIMAGE
		getChildById(id: 'n24_edb2'): cradsǁpaidi4_XMLǃn24ǃIMAGE
		getChild(name: 'n25'): cradsǁpaidi4_XMLǃn25ǃIMAGE
		getChildAt(index: 23): cradsǁpaidi4_XMLǃn25ǃIMAGE
		getChildById(id: 'n25_edb2'): cradsǁpaidi4_XMLǃn25ǃIMAGE
		getChild(name: 'n26'): cradsǁpaidi4_XMLǃn26ǃIMAGE
		getChildAt(index: 24): cradsǁpaidi4_XMLǃn26ǃIMAGE
		getChildById(id: 'n26_edb2'): cradsǁpaidi4_XMLǃn26ǃIMAGE
		getChild(name: 'n27'): cradsǁpaidi4_XMLǃn27ǃIMAGE
		getChildAt(index: 25): cradsǁpaidi4_XMLǃn27ǃIMAGE
		getChildById(id: 'n27_edb2'): cradsǁpaidi4_XMLǃn27ǃIMAGE
		getChild(name: 'n28'): cradsǁpaidi4_XMLǃn28ǃIMAGE
		getChildAt(index: 26): cradsǁpaidi4_XMLǃn28ǃIMAGE
		getChildById(id: 'n28_edb2'): cradsǁpaidi4_XMLǃn28ǃIMAGE
		getChild(name: 'n29'): cradsǁpaidi4_XMLǃn29ǃIMAGE
		getChildAt(index: 27): cradsǁpaidi4_XMLǃn29ǃIMAGE
		getChildById(id: 'n29_edb2'): cradsǁpaidi4_XMLǃn29ǃIMAGE
		getChild(name: 'n30'): cradsǁpaidi4_XMLǃn30ǃIMAGE
		getChildAt(index: 28): cradsǁpaidi4_XMLǃn30ǃIMAGE
		getChildById(id: 'n30_edb2'): cradsǁpaidi4_XMLǃn30ǃIMAGE
		getChild(name: 'n31'): cradsǁpaidi4_XMLǃn31ǃIMAGE
		getChildAt(index: 29): cradsǁpaidi4_XMLǃn31ǃIMAGE
		getChildById(id: 'n31_edb2'): cradsǁpaidi4_XMLǃn31ǃIMAGE
		getChild(name: 'n32'): cradsǁpaidi4_XMLǃn32ǃIMAGE
		getChildAt(index: 30): cradsǁpaidi4_XMLǃn32ǃIMAGE
		getChildById(id: 'n32_edb2'): cradsǁpaidi4_XMLǃn32ǃIMAGE
		getChild(name: 'n33'): cradsǁpaidi4_XMLǃn33ǃIMAGE
		getChildAt(index: 31): cradsǁpaidi4_XMLǃn33ǃIMAGE
		getChildById(id: 'n33_edb2'): cradsǁpaidi4_XMLǃn33ǃIMAGE
		getChild(name: 'n34'): cradsǁpaidi4_XMLǃn34ǃIMAGE
		getChildAt(index: 32): cradsǁpaidi4_XMLǃn34ǃIMAGE
		getChildById(id: 'n34_edb2'): cradsǁpaidi4_XMLǃn34ǃIMAGE
		getChild(name: 'n35'): cradsǁpaidi4_XMLǃn35ǃIMAGE
		getChildAt(index: 33): cradsǁpaidi4_XMLǃn35ǃIMAGE
		getChildById(id: 'n35_edb2'): cradsǁpaidi4_XMLǃn35ǃIMAGE
		getChild(name: 'n36'): cradsǁpaidi4_XMLǃn36ǃIMAGE
		getChildAt(index: 34): cradsǁpaidi4_XMLǃn36ǃIMAGE
		getChildById(id: 'n36_edb2'): cradsǁpaidi4_XMLǃn36ǃIMAGE
		getChild(name: 'n37'): cradsǁpaidi4_XMLǃn37ǃIMAGE
		getChildAt(index: 35): cradsǁpaidi4_XMLǃn37ǃIMAGE
		getChildById(id: 'n37_edb2'): cradsǁpaidi4_XMLǃn37ǃIMAGE
		getChild(name: 'n38'): cradsǁpaidi4_XMLǃn38ǃIMAGE
		getChildAt(index: 36): cradsǁpaidi4_XMLǃn38ǃIMAGE
		getChildById(id: 'n38_edb2'): cradsǁpaidi4_XMLǃn38ǃIMAGE
		getChild(name: 'n39'): cradsǁpaidi4_XMLǃn39ǃIMAGE
		getChildAt(index: 37): cradsǁpaidi4_XMLǃn39ǃIMAGE
		getChildById(id: 'n39_edb2'): cradsǁpaidi4_XMLǃn39ǃIMAGE
		getChild(name: 'n40'): cradsǁpaidi4_XMLǃn40ǃIMAGE
		getChildAt(index: 38): cradsǁpaidi4_XMLǃn40ǃIMAGE
		getChildById(id: 'n40_edb2'): cradsǁpaidi4_XMLǃn40ǃIMAGE
		getChild(name: 'n41'): cradsǁpaidi4_XMLǃn41ǃIMAGE
		getChildAt(index: 39): cradsǁpaidi4_XMLǃn41ǃIMAGE
		getChildById(id: 'n41_edb2'): cradsǁpaidi4_XMLǃn41ǃIMAGE
		getChild(name: 'n42'): cradsǁpaidi4_XMLǃn42ǃIMAGE
		getChildAt(index: 40): cradsǁpaidi4_XMLǃn42ǃIMAGE
		getChildById(id: 'n42_edb2'): cradsǁpaidi4_XMLǃn42ǃIMAGE
		getChild(name: 'n43'): cradsǁpaidi4_XMLǃn43ǃIMAGE
		getChildAt(index: 41): cradsǁpaidi4_XMLǃn43ǃIMAGE
		getChildById(id: 'n43_edb2'): cradsǁpaidi4_XMLǃn43ǃIMAGE
		getChild(name: 'n44'): cradsǁpaidi4_XMLǃn44ǃIMAGE
		getChildAt(index: 42): cradsǁpaidi4_XMLǃn44ǃIMAGE
		getChildById(id: 'n44_edb2'): cradsǁpaidi4_XMLǃn44ǃIMAGE
		getChild(name: 'n45'): cradsǁpaidi4_XMLǃn45ǃIMAGE
		getChildAt(index: 43): cradsǁpaidi4_XMLǃn45ǃIMAGE
		getChildById(id: 'n45_edb2'): cradsǁpaidi4_XMLǃn45ǃIMAGE
		getChild(name: 'n46'): cradsǁpaidi4_XMLǃn46ǃIMAGE
		getChildAt(index: 44): cradsǁpaidi4_XMLǃn46ǃIMAGE
		getChildById(id: 'n46_edb2'): cradsǁpaidi4_XMLǃn46ǃIMAGE
		getChild(name: 'n47'): cradsǁpaidi4_XMLǃn47ǃIMAGE
		getChildAt(index: 45): cradsǁpaidi4_XMLǃn47ǃIMAGE
		getChildById(id: 'n47_edb2'): cradsǁpaidi4_XMLǃn47ǃIMAGE
		getChild(name: 'n48'): cradsǁpaidi4_XMLǃn48ǃIMAGE
		getChildAt(index: 46): cradsǁpaidi4_XMLǃn48ǃIMAGE
		getChildById(id: 'n48_edb2'): cradsǁpaidi4_XMLǃn48ǃIMAGE
		getChild(name: 'n49'): cradsǁpaidi4_XMLǃn49ǃIMAGE
		getChildAt(index: 47): cradsǁpaidi4_XMLǃn49ǃIMAGE
		getChildById(id: 'n49_edb2'): cradsǁpaidi4_XMLǃn49ǃIMAGE
		getChild(name: 'n50'): cradsǁpaidi4_XMLǃn50ǃIMAGE
		getChildAt(index: 48): cradsǁpaidi4_XMLǃn50ǃIMAGE
		getChildById(id: 'n50_edb2'): cradsǁpaidi4_XMLǃn50ǃIMAGE
		getChild(name: 'n51'): cradsǁpaidi4_XMLǃn51ǃIMAGE
		getChildAt(index: 49): cradsǁpaidi4_XMLǃn51ǃIMAGE
		getChildById(id: 'n51_edb2'): cradsǁpaidi4_XMLǃn51ǃIMAGE
		getChild(name: 'n52'): cradsǁpaidi4_XMLǃn52ǃIMAGE
		getChildAt(index: 50): cradsǁpaidi4_XMLǃn52ǃIMAGE
		getChildById(id: 'n52_edb2'): cradsǁpaidi4_XMLǃn52ǃIMAGE
		getChild(name: 'n53'): cradsǁpaidi4_XMLǃn53ǃIMAGE
		getChildAt(index: 51): cradsǁpaidi4_XMLǃn53ǃIMAGE
		getChildById(id: 'n53_edb2'): cradsǁpaidi4_XMLǃn53ǃIMAGE
		getChild(name: 'n54'): cradsǁpaidi4_XMLǃn54ǃIMAGE
		getChildAt(index: 52): cradsǁpaidi4_XMLǃn54ǃIMAGE
		getChildById(id: 'n54_edb2'): cradsǁpaidi4_XMLǃn54ǃIMAGE
		getChild(name: 'mask'): cradsǁpaidi4_XMLǃmaskǃGRAPH
		getChildAt(index: 53): cradsǁpaidi4_XMLǃmaskǃGRAPH
		getChildById(id: 'n56_ajve'): cradsǁpaidi4_XMLǃmaskǃGRAPH
		getChild(name: 'finger'): cradsǁpaidi4_XMLǃfingerǃIMAGE
		getChildAt(index: 54): cradsǁpaidi4_XMLǃfingerǃIMAGE
		getChildById(id: 'n58_hmse'): cradsǁpaidi4_XMLǃfingerǃIMAGE
		getChild(name: 'n61'): cradsǁpaidi4_XMLǃn61ǃIMAGE
		getChildAt(index: 55): cradsǁpaidi4_XMLǃn61ǃIMAGE
		getChildById(id: 'n61_sjy9'): cradsǁpaidi4_XMLǃn61ǃIMAGE
		getChild(name: 'finger1'): cradsǁpaidi4_XMLǃfinger1ǃIMAGE
		getChildAt(index: 56): cradsǁpaidi4_XMLǃfinger1ǃIMAGE
		getChildById(id: 'n62_sjy9'): cradsǁpaidi4_XMLǃfinger1ǃIMAGE
		_children: [
			cradsǁpaidi4_XMLǃn2ǃIMAGE,
			cradsǁpaidi4_XMLǃn3ǃIMAGE,
			cradsǁpaidi4_XMLǃn4ǃIMAGE,
			cradsǁpaidi4_XMLǃn5ǃIMAGE,
			cradsǁpaidi4_XMLǃn6ǃIMAGE,
			cradsǁpaidi4_XMLǃn7ǃIMAGE,
			cradsǁpaidi4_XMLǃn8ǃIMAGE,
			cradsǁpaidi4_XMLǃn9ǃIMAGE,
			cradsǁpaidi4_XMLǃn10ǃIMAGE,
			cradsǁpaidi4_XMLǃn11ǃIMAGE,
			cradsǁpaidi4_XMLǃn12ǃIMAGE,
			cradsǁpaidi4_XMLǃn13ǃIMAGE,
			cradsǁpaidi4_XMLǃn14ǃIMAGE,
			cradsǁpaidi4_XMLǃn15ǃIMAGE,
			cradsǁpaidi4_XMLǃn16ǃIMAGE,
			cradsǁpaidi4_XMLǃn17ǃIMAGE,
			cradsǁpaidi4_XMLǃn18ǃIMAGE,
			cradsǁpaidi4_XMLǃn19ǃIMAGE,
			cradsǁpaidi4_XMLǃn20ǃIMAGE,
			cradsǁpaidi4_XMLǃn21ǃIMAGE,
			cradsǁpaidi4_XMLǃn22ǃIMAGE,
			cradsǁpaidi4_XMLǃn23ǃIMAGE,
			cradsǁpaidi4_XMLǃn24ǃIMAGE,
			cradsǁpaidi4_XMLǃn25ǃIMAGE,
			cradsǁpaidi4_XMLǃn26ǃIMAGE,
			cradsǁpaidi4_XMLǃn27ǃIMAGE,
			cradsǁpaidi4_XMLǃn28ǃIMAGE,
			cradsǁpaidi4_XMLǃn29ǃIMAGE,
			cradsǁpaidi4_XMLǃn30ǃIMAGE,
			cradsǁpaidi4_XMLǃn31ǃIMAGE,
			cradsǁpaidi4_XMLǃn32ǃIMAGE,
			cradsǁpaidi4_XMLǃn33ǃIMAGE,
			cradsǁpaidi4_XMLǃn34ǃIMAGE,
			cradsǁpaidi4_XMLǃn35ǃIMAGE,
			cradsǁpaidi4_XMLǃn36ǃIMAGE,
			cradsǁpaidi4_XMLǃn37ǃIMAGE,
			cradsǁpaidi4_XMLǃn38ǃIMAGE,
			cradsǁpaidi4_XMLǃn39ǃIMAGE,
			cradsǁpaidi4_XMLǃn40ǃIMAGE,
			cradsǁpaidi4_XMLǃn41ǃIMAGE,
			cradsǁpaidi4_XMLǃn42ǃIMAGE,
			cradsǁpaidi4_XMLǃn43ǃIMAGE,
			cradsǁpaidi4_XMLǃn44ǃIMAGE,
			cradsǁpaidi4_XMLǃn45ǃIMAGE,
			cradsǁpaidi4_XMLǃn46ǃIMAGE,
			cradsǁpaidi4_XMLǃn47ǃIMAGE,
			cradsǁpaidi4_XMLǃn48ǃIMAGE,
			cradsǁpaidi4_XMLǃn49ǃIMAGE,
			cradsǁpaidi4_XMLǃn50ǃIMAGE,
			cradsǁpaidi4_XMLǃn51ǃIMAGE,
			cradsǁpaidi4_XMLǃn52ǃIMAGE,
			cradsǁpaidi4_XMLǃn53ǃIMAGE,
			cradsǁpaidi4_XMLǃn54ǃIMAGE,
			cradsǁpaidi4_XMLǃmaskǃGRAPH,
			cradsǁpaidi4_XMLǃfingerǃIMAGE,
			cradsǁpaidi4_XMLǃn61ǃIMAGE,
			cradsǁpaidi4_XMLǃfinger1ǃIMAGE
		]
		getTransition(name: 't1'): cradsǁpaidi4_XMLǃt1ǃTRANSITION
		getTransitionAt(index: 0): cradsǁpaidi4_XMLǃt1ǃTRANSITION
		getTransition(name: 't2'): cradsǁpaidi4_XMLǃt2ǃTRANSITION
		getTransitionAt(index: 1): cradsǁpaidi4_XMLǃt2ǃTRANSITION
		getTransition(name: 't4'): cradsǁpaidi4_XMLǃt4ǃTRANSITION
		getTransitionAt(index: 2): cradsǁpaidi4_XMLǃt4ǃTRANSITION
		_transitions: [
			cradsǁpaidi4_XMLǃt1ǃTRANSITION,
			cradsǁpaidi4_XMLǃt2ǃTRANSITION,
			cradsǁpaidi4_XMLǃt4ǃTRANSITION
		]
		getController(name: '__language'): cradsǁpaidi4_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): cradsǁpaidi4_XMLǃ__languageǃCONTROLLER
		getController(name: 'c1'): cradsǁpaidi4_XMLǃc1ǃCONTROLLER
		getControllerAt(index: 1): cradsǁpaidi4_XMLǃc1ǃCONTROLLER
		_controllers: [
			cradsǁpaidi4_XMLǃ__languageǃCONTROLLER,
			cradsǁpaidi4_XMLǃc1ǃCONTROLLER
		]
	}
	interface cradsǁpaidi4_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃc1ǃCONTROLLER extends fairygui.Controller{
		_parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn2ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn3ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn4ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn5ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn6ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn7ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn8ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn9ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn10ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn11ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn12ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn13ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn14ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn15ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn16ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn17ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn18ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn19ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn20ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn21ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn22ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn23ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn24ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn25ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn26ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn27ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn28ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn29ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn30ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn31ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn32ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn33ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn34ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn35ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn36ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn37ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn38ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn39ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn40ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn41ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn42ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn43ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn44ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn45ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn46ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn47ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn48ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn49ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn50ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn51ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn52ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn53ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn54ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃmaskǃGRAPH extends fairygui.GGraph{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃfingerǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃn61ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃfinger1ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃt1ǃTRANSITION extends fairygui.Transition{
		_owner: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃt2ǃTRANSITION extends fairygui.Transition{
		_owner: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃt4ǃTRANSITION extends fairygui.Transition{
		_owner: cradsǁpaidi4_XML
	}
	interface cradsǁpaidi4_XMLǃfapai_04ǃCOMPONENT extends cradsǁpaidi4_XML{
		parent: Main_XML
	}
	interface cradsǁpaidi1_XML extends fairygui.GComponent{
		getChild(name: 'n2'): cradsǁpaidi1_XMLǃn2ǃIMAGE
		getChildAt(index: 0): cradsǁpaidi1_XMLǃn2ǃIMAGE
		getChildById(id: 'n2_f7j0'): cradsǁpaidi1_XMLǃn2ǃIMAGE
		getChild(name: 'n3'): cradsǁpaidi1_XMLǃn3ǃIMAGE
		getChildAt(index: 1): cradsǁpaidi1_XMLǃn3ǃIMAGE
		getChildById(id: 'n3_edb2'): cradsǁpaidi1_XMLǃn3ǃIMAGE
		getChild(name: 'n4'): cradsǁpaidi1_XMLǃn4ǃIMAGE
		getChildAt(index: 2): cradsǁpaidi1_XMLǃn4ǃIMAGE
		getChildById(id: 'n4_edb2'): cradsǁpaidi1_XMLǃn4ǃIMAGE
		getChild(name: 'n5'): cradsǁpaidi1_XMLǃn5ǃIMAGE
		getChildAt(index: 3): cradsǁpaidi1_XMLǃn5ǃIMAGE
		getChildById(id: 'n5_edb2'): cradsǁpaidi1_XMLǃn5ǃIMAGE
		getChild(name: 'n6'): cradsǁpaidi1_XMLǃn6ǃIMAGE
		getChildAt(index: 4): cradsǁpaidi1_XMLǃn6ǃIMAGE
		getChildById(id: 'n6_edb2'): cradsǁpaidi1_XMLǃn6ǃIMAGE
		getChild(name: 'n7'): cradsǁpaidi1_XMLǃn7ǃIMAGE
		getChildAt(index: 5): cradsǁpaidi1_XMLǃn7ǃIMAGE
		getChildById(id: 'n7_edb2'): cradsǁpaidi1_XMLǃn7ǃIMAGE
		getChild(name: 'n8'): cradsǁpaidi1_XMLǃn8ǃIMAGE
		getChildAt(index: 6): cradsǁpaidi1_XMLǃn8ǃIMAGE
		getChildById(id: 'n8_edb2'): cradsǁpaidi1_XMLǃn8ǃIMAGE
		getChild(name: 'n9'): cradsǁpaidi1_XMLǃn9ǃIMAGE
		getChildAt(index: 7): cradsǁpaidi1_XMLǃn9ǃIMAGE
		getChildById(id: 'n9_edb2'): cradsǁpaidi1_XMLǃn9ǃIMAGE
		getChild(name: 'n10'): cradsǁpaidi1_XMLǃn10ǃIMAGE
		getChildAt(index: 8): cradsǁpaidi1_XMLǃn10ǃIMAGE
		getChildById(id: 'n10_edb2'): cradsǁpaidi1_XMLǃn10ǃIMAGE
		getChild(name: 'n11'): cradsǁpaidi1_XMLǃn11ǃIMAGE
		getChildAt(index: 9): cradsǁpaidi1_XMLǃn11ǃIMAGE
		getChildById(id: 'n11_edb2'): cradsǁpaidi1_XMLǃn11ǃIMAGE
		getChild(name: 'n12'): cradsǁpaidi1_XMLǃn12ǃIMAGE
		getChildAt(index: 10): cradsǁpaidi1_XMLǃn12ǃIMAGE
		getChildById(id: 'n12_edb2'): cradsǁpaidi1_XMLǃn12ǃIMAGE
		getChild(name: 'n13'): cradsǁpaidi1_XMLǃn13ǃIMAGE
		getChildAt(index: 11): cradsǁpaidi1_XMLǃn13ǃIMAGE
		getChildById(id: 'n13_edb2'): cradsǁpaidi1_XMLǃn13ǃIMAGE
		getChild(name: 'n14'): cradsǁpaidi1_XMLǃn14ǃIMAGE
		getChildAt(index: 12): cradsǁpaidi1_XMLǃn14ǃIMAGE
		getChildById(id: 'n14_edb2'): cradsǁpaidi1_XMLǃn14ǃIMAGE
		getChild(name: 'n15'): cradsǁpaidi1_XMLǃn15ǃIMAGE
		getChildAt(index: 13): cradsǁpaidi1_XMLǃn15ǃIMAGE
		getChildById(id: 'n15_edb2'): cradsǁpaidi1_XMLǃn15ǃIMAGE
		getChild(name: 'n16'): cradsǁpaidi1_XMLǃn16ǃIMAGE
		getChildAt(index: 14): cradsǁpaidi1_XMLǃn16ǃIMAGE
		getChildById(id: 'n16_edb2'): cradsǁpaidi1_XMLǃn16ǃIMAGE
		getChild(name: 'n17'): cradsǁpaidi1_XMLǃn17ǃIMAGE
		getChildAt(index: 15): cradsǁpaidi1_XMLǃn17ǃIMAGE
		getChildById(id: 'n17_edb2'): cradsǁpaidi1_XMLǃn17ǃIMAGE
		getChild(name: 'n18'): cradsǁpaidi1_XMLǃn18ǃIMAGE
		getChildAt(index: 16): cradsǁpaidi1_XMLǃn18ǃIMAGE
		getChildById(id: 'n18_edb2'): cradsǁpaidi1_XMLǃn18ǃIMAGE
		getChild(name: 'n19'): cradsǁpaidi1_XMLǃn19ǃIMAGE
		getChildAt(index: 17): cradsǁpaidi1_XMLǃn19ǃIMAGE
		getChildById(id: 'n19_edb2'): cradsǁpaidi1_XMLǃn19ǃIMAGE
		getChild(name: 'n20'): cradsǁpaidi1_XMLǃn20ǃIMAGE
		getChildAt(index: 18): cradsǁpaidi1_XMLǃn20ǃIMAGE
		getChildById(id: 'n20_edb2'): cradsǁpaidi1_XMLǃn20ǃIMAGE
		getChild(name: 'n21'): cradsǁpaidi1_XMLǃn21ǃIMAGE
		getChildAt(index: 19): cradsǁpaidi1_XMLǃn21ǃIMAGE
		getChildById(id: 'n21_edb2'): cradsǁpaidi1_XMLǃn21ǃIMAGE
		getChild(name: 'n22'): cradsǁpaidi1_XMLǃn22ǃIMAGE
		getChildAt(index: 20): cradsǁpaidi1_XMLǃn22ǃIMAGE
		getChildById(id: 'n22_edb2'): cradsǁpaidi1_XMLǃn22ǃIMAGE
		getChild(name: 'n23'): cradsǁpaidi1_XMLǃn23ǃIMAGE
		getChildAt(index: 21): cradsǁpaidi1_XMLǃn23ǃIMAGE
		getChildById(id: 'n23_edb2'): cradsǁpaidi1_XMLǃn23ǃIMAGE
		getChild(name: 'n24'): cradsǁpaidi1_XMLǃn24ǃIMAGE
		getChildAt(index: 22): cradsǁpaidi1_XMLǃn24ǃIMAGE
		getChildById(id: 'n24_edb2'): cradsǁpaidi1_XMLǃn24ǃIMAGE
		getChild(name: 'n25'): cradsǁpaidi1_XMLǃn25ǃIMAGE
		getChildAt(index: 23): cradsǁpaidi1_XMLǃn25ǃIMAGE
		getChildById(id: 'n25_edb2'): cradsǁpaidi1_XMLǃn25ǃIMAGE
		getChild(name: 'n26'): cradsǁpaidi1_XMLǃn26ǃIMAGE
		getChildAt(index: 24): cradsǁpaidi1_XMLǃn26ǃIMAGE
		getChildById(id: 'n26_edb2'): cradsǁpaidi1_XMLǃn26ǃIMAGE
		getChild(name: 'n27'): cradsǁpaidi1_XMLǃn27ǃIMAGE
		getChildAt(index: 25): cradsǁpaidi1_XMLǃn27ǃIMAGE
		getChildById(id: 'n27_edb2'): cradsǁpaidi1_XMLǃn27ǃIMAGE
		getChild(name: 'n28'): cradsǁpaidi1_XMLǃn28ǃIMAGE
		getChildAt(index: 26): cradsǁpaidi1_XMLǃn28ǃIMAGE
		getChildById(id: 'n28_edb2'): cradsǁpaidi1_XMLǃn28ǃIMAGE
		getChild(name: 'n29'): cradsǁpaidi1_XMLǃn29ǃIMAGE
		getChildAt(index: 27): cradsǁpaidi1_XMLǃn29ǃIMAGE
		getChildById(id: 'n29_edb2'): cradsǁpaidi1_XMLǃn29ǃIMAGE
		getChild(name: 'n30'): cradsǁpaidi1_XMLǃn30ǃIMAGE
		getChildAt(index: 28): cradsǁpaidi1_XMLǃn30ǃIMAGE
		getChildById(id: 'n30_edb2'): cradsǁpaidi1_XMLǃn30ǃIMAGE
		getChild(name: 'n31'): cradsǁpaidi1_XMLǃn31ǃIMAGE
		getChildAt(index: 29): cradsǁpaidi1_XMLǃn31ǃIMAGE
		getChildById(id: 'n31_edb2'): cradsǁpaidi1_XMLǃn31ǃIMAGE
		getChild(name: 'n32'): cradsǁpaidi1_XMLǃn32ǃIMAGE
		getChildAt(index: 30): cradsǁpaidi1_XMLǃn32ǃIMAGE
		getChildById(id: 'n32_edb2'): cradsǁpaidi1_XMLǃn32ǃIMAGE
		getChild(name: 'n33'): cradsǁpaidi1_XMLǃn33ǃIMAGE
		getChildAt(index: 31): cradsǁpaidi1_XMLǃn33ǃIMAGE
		getChildById(id: 'n33_edb2'): cradsǁpaidi1_XMLǃn33ǃIMAGE
		getChild(name: 'n34'): cradsǁpaidi1_XMLǃn34ǃIMAGE
		getChildAt(index: 32): cradsǁpaidi1_XMLǃn34ǃIMAGE
		getChildById(id: 'n34_edb2'): cradsǁpaidi1_XMLǃn34ǃIMAGE
		getChild(name: 'n35'): cradsǁpaidi1_XMLǃn35ǃIMAGE
		getChildAt(index: 33): cradsǁpaidi1_XMLǃn35ǃIMAGE
		getChildById(id: 'n35_edb2'): cradsǁpaidi1_XMLǃn35ǃIMAGE
		getChild(name: 'n36'): cradsǁpaidi1_XMLǃn36ǃIMAGE
		getChildAt(index: 34): cradsǁpaidi1_XMLǃn36ǃIMAGE
		getChildById(id: 'n36_edb2'): cradsǁpaidi1_XMLǃn36ǃIMAGE
		getChild(name: 'n37'): cradsǁpaidi1_XMLǃn37ǃIMAGE
		getChildAt(index: 35): cradsǁpaidi1_XMLǃn37ǃIMAGE
		getChildById(id: 'n37_edb2'): cradsǁpaidi1_XMLǃn37ǃIMAGE
		getChild(name: 'n38'): cradsǁpaidi1_XMLǃn38ǃIMAGE
		getChildAt(index: 36): cradsǁpaidi1_XMLǃn38ǃIMAGE
		getChildById(id: 'n38_edb2'): cradsǁpaidi1_XMLǃn38ǃIMAGE
		getChild(name: 'n39'): cradsǁpaidi1_XMLǃn39ǃIMAGE
		getChildAt(index: 37): cradsǁpaidi1_XMLǃn39ǃIMAGE
		getChildById(id: 'n39_edb2'): cradsǁpaidi1_XMLǃn39ǃIMAGE
		getChild(name: 'n40'): cradsǁpaidi1_XMLǃn40ǃIMAGE
		getChildAt(index: 38): cradsǁpaidi1_XMLǃn40ǃIMAGE
		getChildById(id: 'n40_edb2'): cradsǁpaidi1_XMLǃn40ǃIMAGE
		getChild(name: 'n41'): cradsǁpaidi1_XMLǃn41ǃIMAGE
		getChildAt(index: 39): cradsǁpaidi1_XMLǃn41ǃIMAGE
		getChildById(id: 'n41_edb2'): cradsǁpaidi1_XMLǃn41ǃIMAGE
		getChild(name: 'n42'): cradsǁpaidi1_XMLǃn42ǃIMAGE
		getChildAt(index: 40): cradsǁpaidi1_XMLǃn42ǃIMAGE
		getChildById(id: 'n42_edb2'): cradsǁpaidi1_XMLǃn42ǃIMAGE
		getChild(name: 'n43'): cradsǁpaidi1_XMLǃn43ǃIMAGE
		getChildAt(index: 41): cradsǁpaidi1_XMLǃn43ǃIMAGE
		getChildById(id: 'n43_edb2'): cradsǁpaidi1_XMLǃn43ǃIMAGE
		getChild(name: 'n44'): cradsǁpaidi1_XMLǃn44ǃIMAGE
		getChildAt(index: 42): cradsǁpaidi1_XMLǃn44ǃIMAGE
		getChildById(id: 'n44_edb2'): cradsǁpaidi1_XMLǃn44ǃIMAGE
		getChild(name: 'n45'): cradsǁpaidi1_XMLǃn45ǃIMAGE
		getChildAt(index: 43): cradsǁpaidi1_XMLǃn45ǃIMAGE
		getChildById(id: 'n45_edb2'): cradsǁpaidi1_XMLǃn45ǃIMAGE
		getChild(name: 'n46'): cradsǁpaidi1_XMLǃn46ǃIMAGE
		getChildAt(index: 44): cradsǁpaidi1_XMLǃn46ǃIMAGE
		getChildById(id: 'n46_edb2'): cradsǁpaidi1_XMLǃn46ǃIMAGE
		getChild(name: 'n47'): cradsǁpaidi1_XMLǃn47ǃIMAGE
		getChildAt(index: 45): cradsǁpaidi1_XMLǃn47ǃIMAGE
		getChildById(id: 'n47_edb2'): cradsǁpaidi1_XMLǃn47ǃIMAGE
		getChild(name: 'n48'): cradsǁpaidi1_XMLǃn48ǃIMAGE
		getChildAt(index: 46): cradsǁpaidi1_XMLǃn48ǃIMAGE
		getChildById(id: 'n48_edb2'): cradsǁpaidi1_XMLǃn48ǃIMAGE
		getChild(name: 'n49'): cradsǁpaidi1_XMLǃn49ǃIMAGE
		getChildAt(index: 47): cradsǁpaidi1_XMLǃn49ǃIMAGE
		getChildById(id: 'n49_edb2'): cradsǁpaidi1_XMLǃn49ǃIMAGE
		getChild(name: 'n50'): cradsǁpaidi1_XMLǃn50ǃIMAGE
		getChildAt(index: 48): cradsǁpaidi1_XMLǃn50ǃIMAGE
		getChildById(id: 'n50_edb2'): cradsǁpaidi1_XMLǃn50ǃIMAGE
		getChild(name: 'n51'): cradsǁpaidi1_XMLǃn51ǃIMAGE
		getChildAt(index: 49): cradsǁpaidi1_XMLǃn51ǃIMAGE
		getChildById(id: 'n51_edb2'): cradsǁpaidi1_XMLǃn51ǃIMAGE
		getChild(name: 'n52'): cradsǁpaidi1_XMLǃn52ǃIMAGE
		getChildAt(index: 50): cradsǁpaidi1_XMLǃn52ǃIMAGE
		getChildById(id: 'n52_edb2'): cradsǁpaidi1_XMLǃn52ǃIMAGE
		getChild(name: 'n53'): cradsǁpaidi1_XMLǃn53ǃIMAGE
		getChildAt(index: 51): cradsǁpaidi1_XMLǃn53ǃIMAGE
		getChildById(id: 'n53_edb2'): cradsǁpaidi1_XMLǃn53ǃIMAGE
		getChild(name: 'n54'): cradsǁpaidi1_XMLǃn54ǃIMAGE
		getChildAt(index: 52): cradsǁpaidi1_XMLǃn54ǃIMAGE
		getChildById(id: 'n54_edb2'): cradsǁpaidi1_XMLǃn54ǃIMAGE
		getChild(name: 'mask'): cradsǁpaidi1_XMLǃmaskǃGRAPH
		getChildAt(index: 53): cradsǁpaidi1_XMLǃmaskǃGRAPH
		getChildById(id: 'n56_ajve'): cradsǁpaidi1_XMLǃmaskǃGRAPH
		getChild(name: 'finger'): cradsǁpaidi1_XMLǃfingerǃIMAGE
		getChildAt(index: 54): cradsǁpaidi1_XMLǃfingerǃIMAGE
		getChildById(id: 'n58_hmse'): cradsǁpaidi1_XMLǃfingerǃIMAGE
		getChild(name: 'n61'): cradsǁpaidi1_XMLǃn61ǃIMAGE
		getChildAt(index: 55): cradsǁpaidi1_XMLǃn61ǃIMAGE
		getChildById(id: 'n61_sjy9'): cradsǁpaidi1_XMLǃn61ǃIMAGE
		getChild(name: 'finger1'): cradsǁpaidi1_XMLǃfinger1ǃIMAGE
		getChildAt(index: 56): cradsǁpaidi1_XMLǃfinger1ǃIMAGE
		getChildById(id: 'n62_sjy9'): cradsǁpaidi1_XMLǃfinger1ǃIMAGE
		_children: [
			cradsǁpaidi1_XMLǃn2ǃIMAGE,
			cradsǁpaidi1_XMLǃn3ǃIMAGE,
			cradsǁpaidi1_XMLǃn4ǃIMAGE,
			cradsǁpaidi1_XMLǃn5ǃIMAGE,
			cradsǁpaidi1_XMLǃn6ǃIMAGE,
			cradsǁpaidi1_XMLǃn7ǃIMAGE,
			cradsǁpaidi1_XMLǃn8ǃIMAGE,
			cradsǁpaidi1_XMLǃn9ǃIMAGE,
			cradsǁpaidi1_XMLǃn10ǃIMAGE,
			cradsǁpaidi1_XMLǃn11ǃIMAGE,
			cradsǁpaidi1_XMLǃn12ǃIMAGE,
			cradsǁpaidi1_XMLǃn13ǃIMAGE,
			cradsǁpaidi1_XMLǃn14ǃIMAGE,
			cradsǁpaidi1_XMLǃn15ǃIMAGE,
			cradsǁpaidi1_XMLǃn16ǃIMAGE,
			cradsǁpaidi1_XMLǃn17ǃIMAGE,
			cradsǁpaidi1_XMLǃn18ǃIMAGE,
			cradsǁpaidi1_XMLǃn19ǃIMAGE,
			cradsǁpaidi1_XMLǃn20ǃIMAGE,
			cradsǁpaidi1_XMLǃn21ǃIMAGE,
			cradsǁpaidi1_XMLǃn22ǃIMAGE,
			cradsǁpaidi1_XMLǃn23ǃIMAGE,
			cradsǁpaidi1_XMLǃn24ǃIMAGE,
			cradsǁpaidi1_XMLǃn25ǃIMAGE,
			cradsǁpaidi1_XMLǃn26ǃIMAGE,
			cradsǁpaidi1_XMLǃn27ǃIMAGE,
			cradsǁpaidi1_XMLǃn28ǃIMAGE,
			cradsǁpaidi1_XMLǃn29ǃIMAGE,
			cradsǁpaidi1_XMLǃn30ǃIMAGE,
			cradsǁpaidi1_XMLǃn31ǃIMAGE,
			cradsǁpaidi1_XMLǃn32ǃIMAGE,
			cradsǁpaidi1_XMLǃn33ǃIMAGE,
			cradsǁpaidi1_XMLǃn34ǃIMAGE,
			cradsǁpaidi1_XMLǃn35ǃIMAGE,
			cradsǁpaidi1_XMLǃn36ǃIMAGE,
			cradsǁpaidi1_XMLǃn37ǃIMAGE,
			cradsǁpaidi1_XMLǃn38ǃIMAGE,
			cradsǁpaidi1_XMLǃn39ǃIMAGE,
			cradsǁpaidi1_XMLǃn40ǃIMAGE,
			cradsǁpaidi1_XMLǃn41ǃIMAGE,
			cradsǁpaidi1_XMLǃn42ǃIMAGE,
			cradsǁpaidi1_XMLǃn43ǃIMAGE,
			cradsǁpaidi1_XMLǃn44ǃIMAGE,
			cradsǁpaidi1_XMLǃn45ǃIMAGE,
			cradsǁpaidi1_XMLǃn46ǃIMAGE,
			cradsǁpaidi1_XMLǃn47ǃIMAGE,
			cradsǁpaidi1_XMLǃn48ǃIMAGE,
			cradsǁpaidi1_XMLǃn49ǃIMAGE,
			cradsǁpaidi1_XMLǃn50ǃIMAGE,
			cradsǁpaidi1_XMLǃn51ǃIMAGE,
			cradsǁpaidi1_XMLǃn52ǃIMAGE,
			cradsǁpaidi1_XMLǃn53ǃIMAGE,
			cradsǁpaidi1_XMLǃn54ǃIMAGE,
			cradsǁpaidi1_XMLǃmaskǃGRAPH,
			cradsǁpaidi1_XMLǃfingerǃIMAGE,
			cradsǁpaidi1_XMLǃn61ǃIMAGE,
			cradsǁpaidi1_XMLǃfinger1ǃIMAGE
		]
		getTransition(name: 't1'): cradsǁpaidi1_XMLǃt1ǃTRANSITION
		getTransitionAt(index: 0): cradsǁpaidi1_XMLǃt1ǃTRANSITION
		getTransition(name: 't2'): cradsǁpaidi1_XMLǃt2ǃTRANSITION
		getTransitionAt(index: 1): cradsǁpaidi1_XMLǃt2ǃTRANSITION
		getTransition(name: 't4'): cradsǁpaidi1_XMLǃt4ǃTRANSITION
		getTransitionAt(index: 2): cradsǁpaidi1_XMLǃt4ǃTRANSITION
		_transitions: [
			cradsǁpaidi1_XMLǃt1ǃTRANSITION,
			cradsǁpaidi1_XMLǃt2ǃTRANSITION,
			cradsǁpaidi1_XMLǃt4ǃTRANSITION
		]
		getController(name: '__language'): cradsǁpaidi1_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): cradsǁpaidi1_XMLǃ__languageǃCONTROLLER
		getController(name: 'c1'): cradsǁpaidi1_XMLǃc1ǃCONTROLLER
		getControllerAt(index: 1): cradsǁpaidi1_XMLǃc1ǃCONTROLLER
		_controllers: [
			cradsǁpaidi1_XMLǃ__languageǃCONTROLLER,
			cradsǁpaidi1_XMLǃc1ǃCONTROLLER
		]
	}
	interface cradsǁpaidi1_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃc1ǃCONTROLLER extends fairygui.Controller{
		_parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn2ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn3ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn4ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn5ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn6ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn7ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn8ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn9ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn10ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn11ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn12ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn13ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn14ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn15ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn16ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn17ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn18ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn19ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn20ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn21ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn22ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn23ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn24ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn25ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn26ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn27ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn28ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn29ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn30ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn31ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn32ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn33ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn34ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn35ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn36ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn37ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn38ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn39ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn40ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn41ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn42ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn43ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn44ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn45ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn46ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn47ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn48ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn49ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn50ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn51ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn52ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn53ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn54ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃmaskǃGRAPH extends fairygui.GGraph{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃfingerǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃn61ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃfinger1ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃt1ǃTRANSITION extends fairygui.Transition{
		_owner: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃt2ǃTRANSITION extends fairygui.Transition{
		_owner: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃt4ǃTRANSITION extends fairygui.Transition{
		_owner: cradsǁpaidi1_XML
	}
	interface cradsǁpaidi1_XMLǃfapai_03ǃCOMPONENT extends cradsǁpaidi1_XML{
		parent: Main_XML
	}
	interface cradsǁpaidi3_XML extends fairygui.GComponent{
		getChild(name: 'n2'): cradsǁpaidi3_XMLǃn2ǃIMAGE
		getChildAt(index: 0): cradsǁpaidi3_XMLǃn2ǃIMAGE
		getChildById(id: 'n2_f7j0'): cradsǁpaidi3_XMLǃn2ǃIMAGE
		getChild(name: 'n3'): cradsǁpaidi3_XMLǃn3ǃIMAGE
		getChildAt(index: 1): cradsǁpaidi3_XMLǃn3ǃIMAGE
		getChildById(id: 'n3_edb2'): cradsǁpaidi3_XMLǃn3ǃIMAGE
		getChild(name: 'n4'): cradsǁpaidi3_XMLǃn4ǃIMAGE
		getChildAt(index: 2): cradsǁpaidi3_XMLǃn4ǃIMAGE
		getChildById(id: 'n4_edb2'): cradsǁpaidi3_XMLǃn4ǃIMAGE
		getChild(name: 'n5'): cradsǁpaidi3_XMLǃn5ǃIMAGE
		getChildAt(index: 3): cradsǁpaidi3_XMLǃn5ǃIMAGE
		getChildById(id: 'n5_edb2'): cradsǁpaidi3_XMLǃn5ǃIMAGE
		getChild(name: 'n6'): cradsǁpaidi3_XMLǃn6ǃIMAGE
		getChildAt(index: 4): cradsǁpaidi3_XMLǃn6ǃIMAGE
		getChildById(id: 'n6_edb2'): cradsǁpaidi3_XMLǃn6ǃIMAGE
		getChild(name: 'n7'): cradsǁpaidi3_XMLǃn7ǃIMAGE
		getChildAt(index: 5): cradsǁpaidi3_XMLǃn7ǃIMAGE
		getChildById(id: 'n7_edb2'): cradsǁpaidi3_XMLǃn7ǃIMAGE
		getChild(name: 'n8'): cradsǁpaidi3_XMLǃn8ǃIMAGE
		getChildAt(index: 6): cradsǁpaidi3_XMLǃn8ǃIMAGE
		getChildById(id: 'n8_edb2'): cradsǁpaidi3_XMLǃn8ǃIMAGE
		getChild(name: 'n9'): cradsǁpaidi3_XMLǃn9ǃIMAGE
		getChildAt(index: 7): cradsǁpaidi3_XMLǃn9ǃIMAGE
		getChildById(id: 'n9_edb2'): cradsǁpaidi3_XMLǃn9ǃIMAGE
		getChild(name: 'n10'): cradsǁpaidi3_XMLǃn10ǃIMAGE
		getChildAt(index: 8): cradsǁpaidi3_XMLǃn10ǃIMAGE
		getChildById(id: 'n10_edb2'): cradsǁpaidi3_XMLǃn10ǃIMAGE
		getChild(name: 'n11'): cradsǁpaidi3_XMLǃn11ǃIMAGE
		getChildAt(index: 9): cradsǁpaidi3_XMLǃn11ǃIMAGE
		getChildById(id: 'n11_edb2'): cradsǁpaidi3_XMLǃn11ǃIMAGE
		getChild(name: 'n12'): cradsǁpaidi3_XMLǃn12ǃIMAGE
		getChildAt(index: 10): cradsǁpaidi3_XMLǃn12ǃIMAGE
		getChildById(id: 'n12_edb2'): cradsǁpaidi3_XMLǃn12ǃIMAGE
		getChild(name: 'n13'): cradsǁpaidi3_XMLǃn13ǃIMAGE
		getChildAt(index: 11): cradsǁpaidi3_XMLǃn13ǃIMAGE
		getChildById(id: 'n13_edb2'): cradsǁpaidi3_XMLǃn13ǃIMAGE
		getChild(name: 'n14'): cradsǁpaidi3_XMLǃn14ǃIMAGE
		getChildAt(index: 12): cradsǁpaidi3_XMLǃn14ǃIMAGE
		getChildById(id: 'n14_edb2'): cradsǁpaidi3_XMLǃn14ǃIMAGE
		getChild(name: 'n15'): cradsǁpaidi3_XMLǃn15ǃIMAGE
		getChildAt(index: 13): cradsǁpaidi3_XMLǃn15ǃIMAGE
		getChildById(id: 'n15_edb2'): cradsǁpaidi3_XMLǃn15ǃIMAGE
		getChild(name: 'n16'): cradsǁpaidi3_XMLǃn16ǃIMAGE
		getChildAt(index: 14): cradsǁpaidi3_XMLǃn16ǃIMAGE
		getChildById(id: 'n16_edb2'): cradsǁpaidi3_XMLǃn16ǃIMAGE
		getChild(name: 'n17'): cradsǁpaidi3_XMLǃn17ǃIMAGE
		getChildAt(index: 15): cradsǁpaidi3_XMLǃn17ǃIMAGE
		getChildById(id: 'n17_edb2'): cradsǁpaidi3_XMLǃn17ǃIMAGE
		getChild(name: 'n18'): cradsǁpaidi3_XMLǃn18ǃIMAGE
		getChildAt(index: 16): cradsǁpaidi3_XMLǃn18ǃIMAGE
		getChildById(id: 'n18_edb2'): cradsǁpaidi3_XMLǃn18ǃIMAGE
		getChild(name: 'n19'): cradsǁpaidi3_XMLǃn19ǃIMAGE
		getChildAt(index: 17): cradsǁpaidi3_XMLǃn19ǃIMAGE
		getChildById(id: 'n19_edb2'): cradsǁpaidi3_XMLǃn19ǃIMAGE
		getChild(name: 'n20'): cradsǁpaidi3_XMLǃn20ǃIMAGE
		getChildAt(index: 18): cradsǁpaidi3_XMLǃn20ǃIMAGE
		getChildById(id: 'n20_edb2'): cradsǁpaidi3_XMLǃn20ǃIMAGE
		getChild(name: 'n21'): cradsǁpaidi3_XMLǃn21ǃIMAGE
		getChildAt(index: 19): cradsǁpaidi3_XMLǃn21ǃIMAGE
		getChildById(id: 'n21_edb2'): cradsǁpaidi3_XMLǃn21ǃIMAGE
		getChild(name: 'n22'): cradsǁpaidi3_XMLǃn22ǃIMAGE
		getChildAt(index: 20): cradsǁpaidi3_XMLǃn22ǃIMAGE
		getChildById(id: 'n22_edb2'): cradsǁpaidi3_XMLǃn22ǃIMAGE
		getChild(name: 'n23'): cradsǁpaidi3_XMLǃn23ǃIMAGE
		getChildAt(index: 21): cradsǁpaidi3_XMLǃn23ǃIMAGE
		getChildById(id: 'n23_edb2'): cradsǁpaidi3_XMLǃn23ǃIMAGE
		getChild(name: 'n24'): cradsǁpaidi3_XMLǃn24ǃIMAGE
		getChildAt(index: 22): cradsǁpaidi3_XMLǃn24ǃIMAGE
		getChildById(id: 'n24_edb2'): cradsǁpaidi3_XMLǃn24ǃIMAGE
		getChild(name: 'n25'): cradsǁpaidi3_XMLǃn25ǃIMAGE
		getChildAt(index: 23): cradsǁpaidi3_XMLǃn25ǃIMAGE
		getChildById(id: 'n25_edb2'): cradsǁpaidi3_XMLǃn25ǃIMAGE
		getChild(name: 'n26'): cradsǁpaidi3_XMLǃn26ǃIMAGE
		getChildAt(index: 24): cradsǁpaidi3_XMLǃn26ǃIMAGE
		getChildById(id: 'n26_edb2'): cradsǁpaidi3_XMLǃn26ǃIMAGE
		getChild(name: 'n27'): cradsǁpaidi3_XMLǃn27ǃIMAGE
		getChildAt(index: 25): cradsǁpaidi3_XMLǃn27ǃIMAGE
		getChildById(id: 'n27_edb2'): cradsǁpaidi3_XMLǃn27ǃIMAGE
		getChild(name: 'n28'): cradsǁpaidi3_XMLǃn28ǃIMAGE
		getChildAt(index: 26): cradsǁpaidi3_XMLǃn28ǃIMAGE
		getChildById(id: 'n28_edb2'): cradsǁpaidi3_XMLǃn28ǃIMAGE
		getChild(name: 'n29'): cradsǁpaidi3_XMLǃn29ǃIMAGE
		getChildAt(index: 27): cradsǁpaidi3_XMLǃn29ǃIMAGE
		getChildById(id: 'n29_edb2'): cradsǁpaidi3_XMLǃn29ǃIMAGE
		getChild(name: 'n30'): cradsǁpaidi3_XMLǃn30ǃIMAGE
		getChildAt(index: 28): cradsǁpaidi3_XMLǃn30ǃIMAGE
		getChildById(id: 'n30_edb2'): cradsǁpaidi3_XMLǃn30ǃIMAGE
		getChild(name: 'n31'): cradsǁpaidi3_XMLǃn31ǃIMAGE
		getChildAt(index: 29): cradsǁpaidi3_XMLǃn31ǃIMAGE
		getChildById(id: 'n31_edb2'): cradsǁpaidi3_XMLǃn31ǃIMAGE
		getChild(name: 'n32'): cradsǁpaidi3_XMLǃn32ǃIMAGE
		getChildAt(index: 30): cradsǁpaidi3_XMLǃn32ǃIMAGE
		getChildById(id: 'n32_edb2'): cradsǁpaidi3_XMLǃn32ǃIMAGE
		getChild(name: 'n33'): cradsǁpaidi3_XMLǃn33ǃIMAGE
		getChildAt(index: 31): cradsǁpaidi3_XMLǃn33ǃIMAGE
		getChildById(id: 'n33_edb2'): cradsǁpaidi3_XMLǃn33ǃIMAGE
		getChild(name: 'n34'): cradsǁpaidi3_XMLǃn34ǃIMAGE
		getChildAt(index: 32): cradsǁpaidi3_XMLǃn34ǃIMAGE
		getChildById(id: 'n34_edb2'): cradsǁpaidi3_XMLǃn34ǃIMAGE
		getChild(name: 'n35'): cradsǁpaidi3_XMLǃn35ǃIMAGE
		getChildAt(index: 33): cradsǁpaidi3_XMLǃn35ǃIMAGE
		getChildById(id: 'n35_edb2'): cradsǁpaidi3_XMLǃn35ǃIMAGE
		getChild(name: 'n36'): cradsǁpaidi3_XMLǃn36ǃIMAGE
		getChildAt(index: 34): cradsǁpaidi3_XMLǃn36ǃIMAGE
		getChildById(id: 'n36_edb2'): cradsǁpaidi3_XMLǃn36ǃIMAGE
		getChild(name: 'n37'): cradsǁpaidi3_XMLǃn37ǃIMAGE
		getChildAt(index: 35): cradsǁpaidi3_XMLǃn37ǃIMAGE
		getChildById(id: 'n37_edb2'): cradsǁpaidi3_XMLǃn37ǃIMAGE
		getChild(name: 'n38'): cradsǁpaidi3_XMLǃn38ǃIMAGE
		getChildAt(index: 36): cradsǁpaidi3_XMLǃn38ǃIMAGE
		getChildById(id: 'n38_edb2'): cradsǁpaidi3_XMLǃn38ǃIMAGE
		getChild(name: 'n39'): cradsǁpaidi3_XMLǃn39ǃIMAGE
		getChildAt(index: 37): cradsǁpaidi3_XMLǃn39ǃIMAGE
		getChildById(id: 'n39_edb2'): cradsǁpaidi3_XMLǃn39ǃIMAGE
		getChild(name: 'n40'): cradsǁpaidi3_XMLǃn40ǃIMAGE
		getChildAt(index: 38): cradsǁpaidi3_XMLǃn40ǃIMAGE
		getChildById(id: 'n40_edb2'): cradsǁpaidi3_XMLǃn40ǃIMAGE
		getChild(name: 'n41'): cradsǁpaidi3_XMLǃn41ǃIMAGE
		getChildAt(index: 39): cradsǁpaidi3_XMLǃn41ǃIMAGE
		getChildById(id: 'n41_edb2'): cradsǁpaidi3_XMLǃn41ǃIMAGE
		getChild(name: 'n42'): cradsǁpaidi3_XMLǃn42ǃIMAGE
		getChildAt(index: 40): cradsǁpaidi3_XMLǃn42ǃIMAGE
		getChildById(id: 'n42_edb2'): cradsǁpaidi3_XMLǃn42ǃIMAGE
		getChild(name: 'n43'): cradsǁpaidi3_XMLǃn43ǃIMAGE
		getChildAt(index: 41): cradsǁpaidi3_XMLǃn43ǃIMAGE
		getChildById(id: 'n43_edb2'): cradsǁpaidi3_XMLǃn43ǃIMAGE
		getChild(name: 'n44'): cradsǁpaidi3_XMLǃn44ǃIMAGE
		getChildAt(index: 42): cradsǁpaidi3_XMLǃn44ǃIMAGE
		getChildById(id: 'n44_edb2'): cradsǁpaidi3_XMLǃn44ǃIMAGE
		getChild(name: 'n45'): cradsǁpaidi3_XMLǃn45ǃIMAGE
		getChildAt(index: 43): cradsǁpaidi3_XMLǃn45ǃIMAGE
		getChildById(id: 'n45_edb2'): cradsǁpaidi3_XMLǃn45ǃIMAGE
		getChild(name: 'n46'): cradsǁpaidi3_XMLǃn46ǃIMAGE
		getChildAt(index: 44): cradsǁpaidi3_XMLǃn46ǃIMAGE
		getChildById(id: 'n46_edb2'): cradsǁpaidi3_XMLǃn46ǃIMAGE
		getChild(name: 'n47'): cradsǁpaidi3_XMLǃn47ǃIMAGE
		getChildAt(index: 45): cradsǁpaidi3_XMLǃn47ǃIMAGE
		getChildById(id: 'n47_edb2'): cradsǁpaidi3_XMLǃn47ǃIMAGE
		getChild(name: 'n48'): cradsǁpaidi3_XMLǃn48ǃIMAGE
		getChildAt(index: 46): cradsǁpaidi3_XMLǃn48ǃIMAGE
		getChildById(id: 'n48_edb2'): cradsǁpaidi3_XMLǃn48ǃIMAGE
		getChild(name: 'n49'): cradsǁpaidi3_XMLǃn49ǃIMAGE
		getChildAt(index: 47): cradsǁpaidi3_XMLǃn49ǃIMAGE
		getChildById(id: 'n49_edb2'): cradsǁpaidi3_XMLǃn49ǃIMAGE
		getChild(name: 'n50'): cradsǁpaidi3_XMLǃn50ǃIMAGE
		getChildAt(index: 48): cradsǁpaidi3_XMLǃn50ǃIMAGE
		getChildById(id: 'n50_edb2'): cradsǁpaidi3_XMLǃn50ǃIMAGE
		getChild(name: 'n51'): cradsǁpaidi3_XMLǃn51ǃIMAGE
		getChildAt(index: 49): cradsǁpaidi3_XMLǃn51ǃIMAGE
		getChildById(id: 'n51_edb2'): cradsǁpaidi3_XMLǃn51ǃIMAGE
		getChild(name: 'n52'): cradsǁpaidi3_XMLǃn52ǃIMAGE
		getChildAt(index: 50): cradsǁpaidi3_XMLǃn52ǃIMAGE
		getChildById(id: 'n52_edb2'): cradsǁpaidi3_XMLǃn52ǃIMAGE
		getChild(name: 'n53'): cradsǁpaidi3_XMLǃn53ǃIMAGE
		getChildAt(index: 51): cradsǁpaidi3_XMLǃn53ǃIMAGE
		getChildById(id: 'n53_edb2'): cradsǁpaidi3_XMLǃn53ǃIMAGE
		getChild(name: 'n54'): cradsǁpaidi3_XMLǃn54ǃIMAGE
		getChildAt(index: 52): cradsǁpaidi3_XMLǃn54ǃIMAGE
		getChildById(id: 'n54_edb2'): cradsǁpaidi3_XMLǃn54ǃIMAGE
		getChild(name: 'mask'): cradsǁpaidi3_XMLǃmaskǃGRAPH
		getChildAt(index: 53): cradsǁpaidi3_XMLǃmaskǃGRAPH
		getChildById(id: 'n56_ajve'): cradsǁpaidi3_XMLǃmaskǃGRAPH
		getChild(name: 'finger'): cradsǁpaidi3_XMLǃfingerǃIMAGE
		getChildAt(index: 54): cradsǁpaidi3_XMLǃfingerǃIMAGE
		getChildById(id: 'n58_hmse'): cradsǁpaidi3_XMLǃfingerǃIMAGE
		getChild(name: 'n61'): cradsǁpaidi3_XMLǃn61ǃIMAGE
		getChildAt(index: 55): cradsǁpaidi3_XMLǃn61ǃIMAGE
		getChildById(id: 'n61_sjy9'): cradsǁpaidi3_XMLǃn61ǃIMAGE
		getChild(name: 'finger1'): cradsǁpaidi3_XMLǃfinger1ǃIMAGE
		getChildAt(index: 56): cradsǁpaidi3_XMLǃfinger1ǃIMAGE
		getChildById(id: 'n62_sjy9'): cradsǁpaidi3_XMLǃfinger1ǃIMAGE
		_children: [
			cradsǁpaidi3_XMLǃn2ǃIMAGE,
			cradsǁpaidi3_XMLǃn3ǃIMAGE,
			cradsǁpaidi3_XMLǃn4ǃIMAGE,
			cradsǁpaidi3_XMLǃn5ǃIMAGE,
			cradsǁpaidi3_XMLǃn6ǃIMAGE,
			cradsǁpaidi3_XMLǃn7ǃIMAGE,
			cradsǁpaidi3_XMLǃn8ǃIMAGE,
			cradsǁpaidi3_XMLǃn9ǃIMAGE,
			cradsǁpaidi3_XMLǃn10ǃIMAGE,
			cradsǁpaidi3_XMLǃn11ǃIMAGE,
			cradsǁpaidi3_XMLǃn12ǃIMAGE,
			cradsǁpaidi3_XMLǃn13ǃIMAGE,
			cradsǁpaidi3_XMLǃn14ǃIMAGE,
			cradsǁpaidi3_XMLǃn15ǃIMAGE,
			cradsǁpaidi3_XMLǃn16ǃIMAGE,
			cradsǁpaidi3_XMLǃn17ǃIMAGE,
			cradsǁpaidi3_XMLǃn18ǃIMAGE,
			cradsǁpaidi3_XMLǃn19ǃIMAGE,
			cradsǁpaidi3_XMLǃn20ǃIMAGE,
			cradsǁpaidi3_XMLǃn21ǃIMAGE,
			cradsǁpaidi3_XMLǃn22ǃIMAGE,
			cradsǁpaidi3_XMLǃn23ǃIMAGE,
			cradsǁpaidi3_XMLǃn24ǃIMAGE,
			cradsǁpaidi3_XMLǃn25ǃIMAGE,
			cradsǁpaidi3_XMLǃn26ǃIMAGE,
			cradsǁpaidi3_XMLǃn27ǃIMAGE,
			cradsǁpaidi3_XMLǃn28ǃIMAGE,
			cradsǁpaidi3_XMLǃn29ǃIMAGE,
			cradsǁpaidi3_XMLǃn30ǃIMAGE,
			cradsǁpaidi3_XMLǃn31ǃIMAGE,
			cradsǁpaidi3_XMLǃn32ǃIMAGE,
			cradsǁpaidi3_XMLǃn33ǃIMAGE,
			cradsǁpaidi3_XMLǃn34ǃIMAGE,
			cradsǁpaidi3_XMLǃn35ǃIMAGE,
			cradsǁpaidi3_XMLǃn36ǃIMAGE,
			cradsǁpaidi3_XMLǃn37ǃIMAGE,
			cradsǁpaidi3_XMLǃn38ǃIMAGE,
			cradsǁpaidi3_XMLǃn39ǃIMAGE,
			cradsǁpaidi3_XMLǃn40ǃIMAGE,
			cradsǁpaidi3_XMLǃn41ǃIMAGE,
			cradsǁpaidi3_XMLǃn42ǃIMAGE,
			cradsǁpaidi3_XMLǃn43ǃIMAGE,
			cradsǁpaidi3_XMLǃn44ǃIMAGE,
			cradsǁpaidi3_XMLǃn45ǃIMAGE,
			cradsǁpaidi3_XMLǃn46ǃIMAGE,
			cradsǁpaidi3_XMLǃn47ǃIMAGE,
			cradsǁpaidi3_XMLǃn48ǃIMAGE,
			cradsǁpaidi3_XMLǃn49ǃIMAGE,
			cradsǁpaidi3_XMLǃn50ǃIMAGE,
			cradsǁpaidi3_XMLǃn51ǃIMAGE,
			cradsǁpaidi3_XMLǃn52ǃIMAGE,
			cradsǁpaidi3_XMLǃn53ǃIMAGE,
			cradsǁpaidi3_XMLǃn54ǃIMAGE,
			cradsǁpaidi3_XMLǃmaskǃGRAPH,
			cradsǁpaidi3_XMLǃfingerǃIMAGE,
			cradsǁpaidi3_XMLǃn61ǃIMAGE,
			cradsǁpaidi3_XMLǃfinger1ǃIMAGE
		]
		getTransition(name: 't1'): cradsǁpaidi3_XMLǃt1ǃTRANSITION
		getTransitionAt(index: 0): cradsǁpaidi3_XMLǃt1ǃTRANSITION
		getTransition(name: 't2'): cradsǁpaidi3_XMLǃt2ǃTRANSITION
		getTransitionAt(index: 1): cradsǁpaidi3_XMLǃt2ǃTRANSITION
		getTransition(name: 't4'): cradsǁpaidi3_XMLǃt4ǃTRANSITION
		getTransitionAt(index: 2): cradsǁpaidi3_XMLǃt4ǃTRANSITION
		_transitions: [
			cradsǁpaidi3_XMLǃt1ǃTRANSITION,
			cradsǁpaidi3_XMLǃt2ǃTRANSITION,
			cradsǁpaidi3_XMLǃt4ǃTRANSITION
		]
		getController(name: '__language'): cradsǁpaidi3_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): cradsǁpaidi3_XMLǃ__languageǃCONTROLLER
		getController(name: 'c1'): cradsǁpaidi3_XMLǃc1ǃCONTROLLER
		getControllerAt(index: 1): cradsǁpaidi3_XMLǃc1ǃCONTROLLER
		_controllers: [
			cradsǁpaidi3_XMLǃ__languageǃCONTROLLER,
			cradsǁpaidi3_XMLǃc1ǃCONTROLLER
		]
	}
	interface cradsǁpaidi3_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃc1ǃCONTROLLER extends fairygui.Controller{
		_parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn2ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn3ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn4ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn5ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn6ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn7ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn8ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn9ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn10ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn11ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn12ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn13ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn14ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn15ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn16ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn17ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn18ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn19ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn20ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn21ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn22ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn23ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn24ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn25ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn26ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn27ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn28ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn29ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn30ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn31ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn32ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn33ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn34ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn35ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn36ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn37ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn38ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn39ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn40ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn41ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn42ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn43ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn44ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn45ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn46ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn47ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn48ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn49ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn50ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn51ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn52ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn53ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn54ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃmaskǃGRAPH extends fairygui.GGraph{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃfingerǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃn61ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃfinger1ǃIMAGE extends fairygui.GImage{
		parent: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃt1ǃTRANSITION extends fairygui.Transition{
		_owner: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃt2ǃTRANSITION extends fairygui.Transition{
		_owner: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃt4ǃTRANSITION extends fairygui.Transition{
		_owner: cradsǁpaidi3_XML
	}
	interface cradsǁpaidi3_XMLǃfapai_02ǃCOMPONENT extends cradsǁpaidi3_XML{
		parent: Main_XML
	}
	interface cradsǁpaidi_XMLǃfapai_01ǃCOMPONENT extends cradsǁpaidi_XML{
		parent: Main_XML
	}
	interface Main_XMLǃani_xipaiǃMOVIECLIP extends fairygui.GMovieClip{
		parent: Main_XML
	}
	interface Main_XMLǃtext_center_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: Main_XML
	}
	interface Main_XMLǃtext_center_ENǃTEXT extends fairygui.GBasicTextField{
		parent: Main_XML
	}
	interface Main_XMLǃtext_center_INǃTEXT extends fairygui.GBasicTextField{
		parent: Main_XML
	}
	interface Main_XMLǃtext_center_JPǃTEXT extends fairygui.GBasicTextField{
		parent: Main_XML
	}
	interface Main_XMLǃtext_center_KRǃTEXT extends fairygui.GBasicTextField{
		parent: Main_XML
	}
	interface Main_XMLǃtext_center_THǃTEXT extends fairygui.GBasicTextField{
		parent: Main_XML
	}
	interface Main_XMLǃtext_center_VNǃTEXT extends fairygui.GBasicTextField{
		parent: Main_XML
	}
	interface Main_XMLǃtext_centerǃTEXT extends fairygui.GBasicTextField{
		parent: Main_XML
	}
	interface buttonsǁgongnengǁbtn_menu_XML extends fairygui.GButton{
		getChild(name: 'n0'): buttonsǁgongnengǁbtn_menu_XMLǃn0ǃIMAGE
		getChildAt(index: 0): buttonsǁgongnengǁbtn_menu_XMLǃn0ǃIMAGE
		getChildById(id: 'n0_rad3'): buttonsǁgongnengǁbtn_menu_XMLǃn0ǃIMAGE
		_children: [
			buttonsǁgongnengǁbtn_menu_XMLǃn0ǃIMAGE
		]
		getController(name: '__language'): buttonsǁgongnengǁbtn_menu_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁgongnengǁbtn_menu_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁgongnengǁbtn_menu_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁgongnengǁbtn_menu_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁgongnengǁbtn_menu_XMLǃ__languageǃCONTROLLER,
			buttonsǁgongnengǁbtn_menu_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁgongnengǁbtn_menu_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁgongnengǁbtn_menu_XML
	}
	interface buttonsǁgongnengǁbtn_menu_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁgongnengǁbtn_menu_XML
	}
	interface buttonsǁgongnengǁbtn_menu_XMLǃn0ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁgongnengǁbtn_menu_XML
	}
	interface buttonsǁgongnengǁbtn_menu_XMLǃbtn_menuǃCOMPONENT extends buttonsǁgongnengǁbtn_menu_XML{
		parent: Main_XML
	}
	interface Main_XMLǃtext_qishuǃRICHTEXT extends fairygui.GRichTextField{
		parent: Main_XML
	}
	interface Main_XMLǃtext_countdownǃTEXT extends fairygui.GBasicTextField{
		parent: Main_XML
	}
	interface pingInfo_XML extends fairygui.GComponent{
		getChild(name: 'ping'): pingInfo_XMLǃpingǃRICHTEXT
		getChildAt(index: 0): pingInfo_XMLǃpingǃRICHTEXT
		getChildById(id: 'n3_bgg4'): pingInfo_XMLǃpingǃRICHTEXT
		getChild(name: 'n0'): pingInfo_XMLǃn0ǃIMAGE
		getChildAt(index: 1): pingInfo_XMLǃn0ǃIMAGE
		getChildById(id: 'n0_9liv'): pingInfo_XMLǃn0ǃIMAGE
		getChild(name: 'green'): ping_XMLǃgreenǃCOMPONENT
		getChildAt(index: 2): ping_XMLǃgreenǃCOMPONENT
		getChildById(id: 'n7_9liv'): ping_XMLǃgreenǃCOMPONENT
		_children: [
			pingInfo_XMLǃpingǃRICHTEXT,
			pingInfo_XMLǃn0ǃIMAGE,
			ping_XMLǃgreenǃCOMPONENT
		]
		getController(name: '__language'): pingInfo_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): pingInfo_XMLǃ__languageǃCONTROLLER
		_controllers: [
			pingInfo_XMLǃ__languageǃCONTROLLER
		]
	}
	interface pingInfo_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: pingInfo_XML
	}
	interface pingInfo_XMLǃpingǃRICHTEXT extends fairygui.GRichTextField{
		parent: pingInfo_XML
	}
	interface pingInfo_XMLǃn0ǃIMAGE extends fairygui.GImage{
		parent: pingInfo_XML
	}
	interface ping_XML extends fairygui.GComponent{
		getChild(name: 'n0'): ping_XMLǃn0ǃGRAPH
		getChildAt(index: 0): ping_XMLǃn0ǃGRAPH
		getChildById(id: 'n0_9liv'): ping_XMLǃn0ǃGRAPH
		getChild(name: 'n1'): ping_XMLǃn1ǃIMAGE
		getChildAt(index: 1): ping_XMLǃn1ǃIMAGE
		getChildById(id: 'n1_9liv'): ping_XMLǃn1ǃIMAGE
		_children: [
			ping_XMLǃn0ǃGRAPH,
			ping_XMLǃn1ǃIMAGE
		]
		getController(name: '__language'): ping_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): ping_XMLǃ__languageǃCONTROLLER
		getController(name: 'c1'): ping_XMLǃc1ǃCONTROLLER
		getControllerAt(index: 1): ping_XMLǃc1ǃCONTROLLER
		_controllers: [
			ping_XMLǃ__languageǃCONTROLLER,
			ping_XMLǃc1ǃCONTROLLER
		]
	}
	interface ping_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: ping_XML
	}
	interface ping_XMLǃc1ǃCONTROLLER extends fairygui.Controller{
		_parent: ping_XML
	}
	interface ping_XMLǃn0ǃGRAPH extends fairygui.GGraph{
		parent: ping_XML
	}
	interface ping_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: ping_XML
	}
	interface ping_XMLǃgreenǃCOMPONENT extends ping_XML{
		parent: pingInfo_XML
	}
	interface pingInfo_XMLǃpingǃCOMPONENT extends pingInfo_XML{
		parent: Main_XML
	}
	interface Main_XMLǃt112_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: Main_XML
	}
	interface Main_XMLǃt112_ENǃTEXT extends fairygui.GBasicTextField{
		parent: Main_XML
	}
	interface Main_XMLǃt112_INǃTEXT extends fairygui.GBasicTextField{
		parent: Main_XML
	}
	interface Main_XMLǃt112_JPǃTEXT extends fairygui.GBasicTextField{
		parent: Main_XML
	}
	interface Main_XMLǃt112_KRǃTEXT extends fairygui.GBasicTextField{
		parent: Main_XML
	}
	interface Main_XMLǃt112_THǃTEXT extends fairygui.GBasicTextField{
		parent: Main_XML
	}
	interface Main_XMLǃt112_VNǃTEXT extends fairygui.GBasicTextField{
		parent: Main_XML
	}
	interface Main_XMLǃt112ǃTEXT extends fairygui.GBasicTextField{
		parent: Main_XML
	}
	interface Main_XMLǃn113_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: Main_XML
	}
	interface Main_XMLǃn113_ENǃTEXT extends fairygui.GBasicTextField{
		parent: Main_XML
	}
	interface Main_XMLǃn113_INǃTEXT extends fairygui.GBasicTextField{
		parent: Main_XML
	}
	interface Main_XMLǃn113_JPǃTEXT extends fairygui.GBasicTextField{
		parent: Main_XML
	}
	interface Main_XMLǃn113_KRǃTEXT extends fairygui.GBasicTextField{
		parent: Main_XML
	}
	interface Main_XMLǃn113_THǃTEXT extends fairygui.GBasicTextField{
		parent: Main_XML
	}
	interface Main_XMLǃn113_VNǃTEXT extends fairygui.GBasicTextField{
		parent: Main_XML
	}
	interface Main_XMLǃn113ǃTEXT extends fairygui.GBasicTextField{
		parent: Main_XML
	}
	interface Main_XMLǃtext_renshuǃTEXT extends fairygui.GBasicTextField{
		parent: Main_XML
	}
	interface buttonsǁgongnengǁbutton_touzhu_XML extends fairygui.GButton{
		getChild(name: 'n1'): buttonsǁgongnengǁbutton_touzhu_XMLǃn1ǃIMAGE
		getChildAt(index: 0): buttonsǁgongnengǁbutton_touzhu_XMLǃn1ǃIMAGE
		getChildById(id: 'n1'): buttonsǁgongnengǁbutton_touzhu_XMLǃn1ǃIMAGE
		getChild(name: 'n5_CN2'): buttonsǁgongnengǁbutton_touzhu_XMLǃn5_CN2ǃTEXT
		getChildAt(index: 1): buttonsǁgongnengǁbutton_touzhu_XMLǃn5_CN2ǃTEXT
		getChildById(id: 'n5_fo6w_CN2'): buttonsǁgongnengǁbutton_touzhu_XMLǃn5_CN2ǃTEXT
		getChild(name: 'n5_EN'): buttonsǁgongnengǁbutton_touzhu_XMLǃn5_ENǃTEXT
		getChildAt(index: 2): buttonsǁgongnengǁbutton_touzhu_XMLǃn5_ENǃTEXT
		getChildById(id: 'n5_fo6w_EN'): buttonsǁgongnengǁbutton_touzhu_XMLǃn5_ENǃTEXT
		getChild(name: 'n5_IN'): buttonsǁgongnengǁbutton_touzhu_XMLǃn5_INǃTEXT
		getChildAt(index: 3): buttonsǁgongnengǁbutton_touzhu_XMLǃn5_INǃTEXT
		getChildById(id: 'n5_fo6w_IN'): buttonsǁgongnengǁbutton_touzhu_XMLǃn5_INǃTEXT
		getChild(name: 'n5_JP'): buttonsǁgongnengǁbutton_touzhu_XMLǃn5_JPǃTEXT
		getChildAt(index: 4): buttonsǁgongnengǁbutton_touzhu_XMLǃn5_JPǃTEXT
		getChildById(id: 'n5_fo6w_JP'): buttonsǁgongnengǁbutton_touzhu_XMLǃn5_JPǃTEXT
		getChild(name: 'n5_KR'): buttonsǁgongnengǁbutton_touzhu_XMLǃn5_KRǃTEXT
		getChildAt(index: 5): buttonsǁgongnengǁbutton_touzhu_XMLǃn5_KRǃTEXT
		getChildById(id: 'n5_fo6w_KR'): buttonsǁgongnengǁbutton_touzhu_XMLǃn5_KRǃTEXT
		getChild(name: 'n5_TH'): buttonsǁgongnengǁbutton_touzhu_XMLǃn5_THǃTEXT
		getChildAt(index: 6): buttonsǁgongnengǁbutton_touzhu_XMLǃn5_THǃTEXT
		getChildById(id: 'n5_fo6w_TH'): buttonsǁgongnengǁbutton_touzhu_XMLǃn5_THǃTEXT
		getChild(name: 'n5_VN'): buttonsǁgongnengǁbutton_touzhu_XMLǃn5_VNǃTEXT
		getChildAt(index: 7): buttonsǁgongnengǁbutton_touzhu_XMLǃn5_VNǃTEXT
		getChildById(id: 'n5_fo6w_VN'): buttonsǁgongnengǁbutton_touzhu_XMLǃn5_VNǃTEXT
		getChild(name: 'n5'): buttonsǁgongnengǁbutton_touzhu_XMLǃn5ǃTEXT
		getChildAt(index: 8): buttonsǁgongnengǁbutton_touzhu_XMLǃn5ǃTEXT
		getChildById(id: 'n5_fo6w'): buttonsǁgongnengǁbutton_touzhu_XMLǃn5ǃTEXT
		getChild(name: 'countdown'): buttonsǁgongnengǁbutton_touzhu_XMLǃcountdownǃTEXT
		getChildAt(index: 9): buttonsǁgongnengǁbutton_touzhu_XMLǃcountdownǃTEXT
		getChildById(id: 'n4_zis0'): buttonsǁgongnengǁbutton_touzhu_XMLǃcountdownǃTEXT
		_children: [
			buttonsǁgongnengǁbutton_touzhu_XMLǃn1ǃIMAGE,
			buttonsǁgongnengǁbutton_touzhu_XMLǃn5_CN2ǃTEXT,
			buttonsǁgongnengǁbutton_touzhu_XMLǃn5_ENǃTEXT,
			buttonsǁgongnengǁbutton_touzhu_XMLǃn5_INǃTEXT,
			buttonsǁgongnengǁbutton_touzhu_XMLǃn5_JPǃTEXT,
			buttonsǁgongnengǁbutton_touzhu_XMLǃn5_KRǃTEXT,
			buttonsǁgongnengǁbutton_touzhu_XMLǃn5_THǃTEXT,
			buttonsǁgongnengǁbutton_touzhu_XMLǃn5_VNǃTEXT,
			buttonsǁgongnengǁbutton_touzhu_XMLǃn5ǃTEXT,
			buttonsǁgongnengǁbutton_touzhu_XMLǃcountdownǃTEXT
		]
		getController(name: '__language'): buttonsǁgongnengǁbutton_touzhu_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁgongnengǁbutton_touzhu_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁgongnengǁbutton_touzhu_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁgongnengǁbutton_touzhu_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁgongnengǁbutton_touzhu_XMLǃ__languageǃCONTROLLER,
			buttonsǁgongnengǁbutton_touzhu_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁgongnengǁbutton_touzhu_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁgongnengǁbutton_touzhu_XML
	}
	interface buttonsǁgongnengǁbutton_touzhu_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁgongnengǁbutton_touzhu_XML
	}
	interface buttonsǁgongnengǁbutton_touzhu_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁgongnengǁbutton_touzhu_XML
	}
	interface buttonsǁgongnengǁbutton_touzhu_XMLǃn5_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁgongnengǁbutton_touzhu_XML
	}
	interface buttonsǁgongnengǁbutton_touzhu_XMLǃn5_ENǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁgongnengǁbutton_touzhu_XML
	}
	interface buttonsǁgongnengǁbutton_touzhu_XMLǃn5_INǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁgongnengǁbutton_touzhu_XML
	}
	interface buttonsǁgongnengǁbutton_touzhu_XMLǃn5_JPǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁgongnengǁbutton_touzhu_XML
	}
	interface buttonsǁgongnengǁbutton_touzhu_XMLǃn5_KRǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁgongnengǁbutton_touzhu_XML
	}
	interface buttonsǁgongnengǁbutton_touzhu_XMLǃn5_THǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁgongnengǁbutton_touzhu_XML
	}
	interface buttonsǁgongnengǁbutton_touzhu_XMLǃn5_VNǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁgongnengǁbutton_touzhu_XML
	}
	interface buttonsǁgongnengǁbutton_touzhu_XMLǃn5ǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁgongnengǁbutton_touzhu_XML
	}
	interface buttonsǁgongnengǁbutton_touzhu_XMLǃcountdownǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁgongnengǁbutton_touzhu_XML
	}
	interface buttonsǁgongnengǁbutton_touzhu_XMLǃbutton_querentouzhuǃCOMPONENT extends buttonsǁgongnengǁbutton_touzhu_XML{
		parent: Main_XML
	}
	interface xiazhuxinxiǁxinxi_moban1_XML extends fairygui.GComponent{
		getChild(name: 'n45'): xiazhuxinxiǁxinxi_moban1_XMLǃn45ǃGRAPH
		getChildAt(index: 0): xiazhuxinxiǁxinxi_moban1_XMLǃn45ǃGRAPH
		getChildById(id: 'n45_x8xm'): xiazhuxinxiǁxinxi_moban1_XMLǃn45ǃGRAPH
		getChild(name: 'odds'): xiazhuxinxiǁxinxi_moban1_XMLǃoddsǃTEXT
		getChildAt(index: 1): xiazhuxinxiǁxinxi_moban1_XMLǃoddsǃTEXT
		getChildById(id: 'n55_x8xm'): xiazhuxinxiǁxinxi_moban1_XMLǃoddsǃTEXT
		getChild(name: 'totalBet'): xiazhuxinxiǁxinxi_moban1_XMLǃtotalBetǃTEXT
		getChildAt(index: 2): xiazhuxinxiǁxinxi_moban1_XMLǃtotalBetǃTEXT
		getChildById(id: 'n59_x8xm'): xiazhuxinxiǁxinxi_moban1_XMLǃtotalBetǃTEXT
		getChild(name: 'myBet'): xiazhuxinxiǁxinxi_moban1_XMLǃmyBetǃTEXT
		getChildAt(index: 3): xiazhuxinxiǁxinxi_moban1_XMLǃmyBetǃTEXT
		getChildById(id: 'n60_x8xm'): xiazhuxinxiǁxinxi_moban1_XMLǃmyBetǃTEXT
		getChild(name: 'maskt0'): xiazhuxinxiǁxinxi_moban1_XMLǃmaskt0ǃIMAGE
		getChildAt(index: 4): xiazhuxinxiǁxinxi_moban1_XMLǃmaskt0ǃIMAGE
		getChildById(id: 'n61_ka1v'): xiazhuxinxiǁxinxi_moban1_XMLǃmaskt0ǃIMAGE
		getChild(name: 'maskt1'): xiazhuxinxiǁxinxi_moban1_XMLǃmaskt1ǃIMAGE
		getChildAt(index: 5): xiazhuxinxiǁxinxi_moban1_XMLǃmaskt1ǃIMAGE
		getChildById(id: 'n62_ka1v'): xiazhuxinxiǁxinxi_moban1_XMLǃmaskt1ǃIMAGE
		getChild(name: 'myPreBet'): xiazhuxinxiǁxinxi_moban1_XMLǃmyPreBetǃTEXT
		getChildAt(index: 6): xiazhuxinxiǁxinxi_moban1_XMLǃmyPreBetǃTEXT
		getChildById(id: 'n63_ww6c'): xiazhuxinxiǁxinxi_moban1_XMLǃmyPreBetǃTEXT
		_children: [
			xiazhuxinxiǁxinxi_moban1_XMLǃn45ǃGRAPH,
			xiazhuxinxiǁxinxi_moban1_XMLǃoddsǃTEXT,
			xiazhuxinxiǁxinxi_moban1_XMLǃtotalBetǃTEXT,
			xiazhuxinxiǁxinxi_moban1_XMLǃmyBetǃTEXT,
			xiazhuxinxiǁxinxi_moban1_XMLǃmaskt0ǃIMAGE,
			xiazhuxinxiǁxinxi_moban1_XMLǃmaskt1ǃIMAGE,
			xiazhuxinxiǁxinxi_moban1_XMLǃmyPreBetǃTEXT
		]
		getTransition(name: 't0'): xiazhuxinxiǁxinxi_moban1_XMLǃt0ǃTRANSITION
		getTransitionAt(index: 0): xiazhuxinxiǁxinxi_moban1_XMLǃt0ǃTRANSITION
		getTransition(name: 't1'): xiazhuxinxiǁxinxi_moban1_XMLǃt1ǃTRANSITION
		getTransitionAt(index: 1): xiazhuxinxiǁxinxi_moban1_XMLǃt1ǃTRANSITION
		_transitions: [
			xiazhuxinxiǁxinxi_moban1_XMLǃt0ǃTRANSITION,
			xiazhuxinxiǁxinxi_moban1_XMLǃt1ǃTRANSITION
		]
		getController(name: '__language'): xiazhuxinxiǁxinxi_moban1_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): xiazhuxinxiǁxinxi_moban1_XMLǃ__languageǃCONTROLLER
		_controllers: [
			xiazhuxinxiǁxinxi_moban1_XMLǃ__languageǃCONTROLLER
		]
	}
	interface xiazhuxinxiǁxinxi_moban1_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: xiazhuxinxiǁxinxi_moban1_XML
	}
	interface xiazhuxinxiǁxinxi_moban1_XMLǃn45ǃGRAPH extends fairygui.GGraph{
		parent: xiazhuxinxiǁxinxi_moban1_XML
	}
	interface xiazhuxinxiǁxinxi_moban1_XMLǃoddsǃTEXT extends fairygui.GBasicTextField{
		parent: xiazhuxinxiǁxinxi_moban1_XML
	}
	interface xiazhuxinxiǁxinxi_moban1_XMLǃtotalBetǃTEXT extends fairygui.GBasicTextField{
		parent: xiazhuxinxiǁxinxi_moban1_XML
	}
	interface xiazhuxinxiǁxinxi_moban1_XMLǃmyBetǃTEXT extends fairygui.GBasicTextField{
		parent: xiazhuxinxiǁxinxi_moban1_XML
	}
	interface xiazhuxinxiǁxinxi_moban1_XMLǃmaskt0ǃIMAGE extends fairygui.GImage{
		parent: xiazhuxinxiǁxinxi_moban1_XML
	}
	interface xiazhuxinxiǁxinxi_moban1_XMLǃmaskt1ǃIMAGE extends fairygui.GImage{
		parent: xiazhuxinxiǁxinxi_moban1_XML
	}
	interface xiazhuxinxiǁxinxi_moban1_XMLǃmyPreBetǃTEXT extends fairygui.GBasicTextField{
		parent: xiazhuxinxiǁxinxi_moban1_XML
	}
	interface xiazhuxinxiǁxinxi_moban1_XMLǃt0ǃTRANSITION extends fairygui.Transition{
		_owner: xiazhuxinxiǁxinxi_moban1_XML
	}
	interface xiazhuxinxiǁxinxi_moban1_XMLǃt1ǃTRANSITION extends fairygui.Transition{
		_owner: xiazhuxinxiǁxinxi_moban1_XML
	}
	interface xiazhuxinxiǁxinxi_moban1_XMLǃxinxi_daǃCOMPONENT extends xiazhuxinxiǁxinxi_moban1_XML{
		parent: Main_XML
	}
	interface xiazhuxinxiǁxinxi_moban1_XMLǃxinxi_renyiduiziǃCOMPONENT extends xiazhuxinxiǁxinxi_moban1_XML{
		parent: Main_XML
	}
	interface xiazhuxinxiǁxinxi_moban1_XMLǃxinxi_xianduiǃCOMPONENT extends xiazhuxinxiǁxinxi_moban1_XML{
		parent: Main_XML
	}
	interface xiazhuxinxiǁxinxi_moban1_XMLǃxinxi_xiaoǃCOMPONENT extends xiazhuxinxiǁxinxi_moban1_XML{
		parent: Main_XML
	}
	interface xiazhuxinxiǁxinxi_moban1_XMLǃxinxi_zhuangduiǃCOMPONENT extends xiazhuxinxiǁxinxi_moban1_XML{
		parent: Main_XML
	}
	interface xiazhuxinxiǁxinxi_moban1_XMLǃxinxi_wanmeiduiziǃCOMPONENT extends xiazhuxinxiǁxinxi_moban1_XML{
		parent: Main_XML
	}
	interface xiazhuxinxiǁxinxi_moban2_XML extends fairygui.GComponent{
		getChild(name: 'n45'): xiazhuxinxiǁxinxi_moban2_XMLǃn45ǃGRAPH
		getChildAt(index: 0): xiazhuxinxiǁxinxi_moban2_XMLǃn45ǃGRAPH
		getChildById(id: 'n45_x8xm'): xiazhuxinxiǁxinxi_moban2_XMLǃn45ǃGRAPH
		getChild(name: 'odds'): xiazhuxinxiǁxinxi_moban2_XMLǃoddsǃTEXT
		getChildAt(index: 1): xiazhuxinxiǁxinxi_moban2_XMLǃoddsǃTEXT
		getChildById(id: 'n55_x8xm'): xiazhuxinxiǁxinxi_moban2_XMLǃoddsǃTEXT
		getChild(name: 'totalBet'): xiazhuxinxiǁxinxi_moban2_XMLǃtotalBetǃTEXT
		getChildAt(index: 2): xiazhuxinxiǁxinxi_moban2_XMLǃtotalBetǃTEXT
		getChildById(id: 'n59_x8xm'): xiazhuxinxiǁxinxi_moban2_XMLǃtotalBetǃTEXT
		getChild(name: 'myBet'): xiazhuxinxiǁxinxi_moban2_XMLǃmyBetǃTEXT
		getChildAt(index: 3): xiazhuxinxiǁxinxi_moban2_XMLǃmyBetǃTEXT
		getChildById(id: 'n60_x8xm'): xiazhuxinxiǁxinxi_moban2_XMLǃmyBetǃTEXT
		getChild(name: 'maskt0'): xiazhuxinxiǁxinxi_moban2_XMLǃmaskt0ǃIMAGE
		getChildAt(index: 4): xiazhuxinxiǁxinxi_moban2_XMLǃmaskt0ǃIMAGE
		getChildById(id: 'n61_ka1v'): xiazhuxinxiǁxinxi_moban2_XMLǃmaskt0ǃIMAGE
		getChild(name: 'maskt1'): xiazhuxinxiǁxinxi_moban2_XMLǃmaskt1ǃIMAGE
		getChildAt(index: 5): xiazhuxinxiǁxinxi_moban2_XMLǃmaskt1ǃIMAGE
		getChildById(id: 'n62_ka1v'): xiazhuxinxiǁxinxi_moban2_XMLǃmaskt1ǃIMAGE
		getChild(name: 'myPreBet'): xiazhuxinxiǁxinxi_moban2_XMLǃmyPreBetǃTEXT
		getChildAt(index: 6): xiazhuxinxiǁxinxi_moban2_XMLǃmyPreBetǃTEXT
		getChildById(id: 'n63_ww6c'): xiazhuxinxiǁxinxi_moban2_XMLǃmyPreBetǃTEXT
		_children: [
			xiazhuxinxiǁxinxi_moban2_XMLǃn45ǃGRAPH,
			xiazhuxinxiǁxinxi_moban2_XMLǃoddsǃTEXT,
			xiazhuxinxiǁxinxi_moban2_XMLǃtotalBetǃTEXT,
			xiazhuxinxiǁxinxi_moban2_XMLǃmyBetǃTEXT,
			xiazhuxinxiǁxinxi_moban2_XMLǃmaskt0ǃIMAGE,
			xiazhuxinxiǁxinxi_moban2_XMLǃmaskt1ǃIMAGE,
			xiazhuxinxiǁxinxi_moban2_XMLǃmyPreBetǃTEXT
		]
		getTransition(name: 't0'): xiazhuxinxiǁxinxi_moban2_XMLǃt0ǃTRANSITION
		getTransitionAt(index: 0): xiazhuxinxiǁxinxi_moban2_XMLǃt0ǃTRANSITION
		getTransition(name: 't1'): xiazhuxinxiǁxinxi_moban2_XMLǃt1ǃTRANSITION
		getTransitionAt(index: 1): xiazhuxinxiǁxinxi_moban2_XMLǃt1ǃTRANSITION
		_transitions: [
			xiazhuxinxiǁxinxi_moban2_XMLǃt0ǃTRANSITION,
			xiazhuxinxiǁxinxi_moban2_XMLǃt1ǃTRANSITION
		]
		getController(name: '__language'): xiazhuxinxiǁxinxi_moban2_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): xiazhuxinxiǁxinxi_moban2_XMLǃ__languageǃCONTROLLER
		_controllers: [
			xiazhuxinxiǁxinxi_moban2_XMLǃ__languageǃCONTROLLER
		]
	}
	interface xiazhuxinxiǁxinxi_moban2_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: xiazhuxinxiǁxinxi_moban2_XML
	}
	interface xiazhuxinxiǁxinxi_moban2_XMLǃn45ǃGRAPH extends fairygui.GGraph{
		parent: xiazhuxinxiǁxinxi_moban2_XML
	}
	interface xiazhuxinxiǁxinxi_moban2_XMLǃoddsǃTEXT extends fairygui.GBasicTextField{
		parent: xiazhuxinxiǁxinxi_moban2_XML
	}
	interface xiazhuxinxiǁxinxi_moban2_XMLǃtotalBetǃTEXT extends fairygui.GBasicTextField{
		parent: xiazhuxinxiǁxinxi_moban2_XML
	}
	interface xiazhuxinxiǁxinxi_moban2_XMLǃmyBetǃTEXT extends fairygui.GBasicTextField{
		parent: xiazhuxinxiǁxinxi_moban2_XML
	}
	interface xiazhuxinxiǁxinxi_moban2_XMLǃmaskt0ǃIMAGE extends fairygui.GImage{
		parent: xiazhuxinxiǁxinxi_moban2_XML
	}
	interface xiazhuxinxiǁxinxi_moban2_XMLǃmaskt1ǃIMAGE extends fairygui.GImage{
		parent: xiazhuxinxiǁxinxi_moban2_XML
	}
	interface xiazhuxinxiǁxinxi_moban2_XMLǃmyPreBetǃTEXT extends fairygui.GBasicTextField{
		parent: xiazhuxinxiǁxinxi_moban2_XML
	}
	interface xiazhuxinxiǁxinxi_moban2_XMLǃt0ǃTRANSITION extends fairygui.Transition{
		_owner: xiazhuxinxiǁxinxi_moban2_XML
	}
	interface xiazhuxinxiǁxinxi_moban2_XMLǃt1ǃTRANSITION extends fairygui.Transition{
		_owner: xiazhuxinxiǁxinxi_moban2_XML
	}
	interface xiazhuxinxiǁxinxi_moban2_XMLǃxinxi_xianǃCOMPONENT extends xiazhuxinxiǁxinxi_moban2_XML{
		parent: Main_XML
	}
	interface xiazhuxinxiǁxinxi_moban2_XMLǃxinxi_heǃCOMPONENT extends xiazhuxinxiǁxinxi_moban2_XML{
		parent: Main_XML
	}
	interface xiazhuxinxiǁxinxi_moban2_XMLǃxinxi_zhuangǃCOMPONENT extends xiazhuxinxiǁxinxi_moban2_XML{
		parent: Main_XML
	}
	interface buttonsǁgongnengǁbutton_wenhao_XML extends fairygui.GButton{
		getChild(name: 'n1'): buttonsǁgongnengǁbutton_wenhao_XMLǃn1ǃIMAGE
		getChildAt(index: 0): buttonsǁgongnengǁbutton_wenhao_XMLǃn1ǃIMAGE
		getChildById(id: 'n1'): buttonsǁgongnengǁbutton_wenhao_XMLǃn1ǃIMAGE
		_children: [
			buttonsǁgongnengǁbutton_wenhao_XMLǃn1ǃIMAGE
		]
		getController(name: '__language'): buttonsǁgongnengǁbutton_wenhao_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁgongnengǁbutton_wenhao_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁgongnengǁbutton_wenhao_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁgongnengǁbutton_wenhao_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁgongnengǁbutton_wenhao_XMLǃ__languageǃCONTROLLER,
			buttonsǁgongnengǁbutton_wenhao_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁgongnengǁbutton_wenhao_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁgongnengǁbutton_wenhao_XML
	}
	interface buttonsǁgongnengǁbutton_wenhao_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁgongnengǁbutton_wenhao_XML
	}
	interface buttonsǁgongnengǁbutton_wenhao_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁgongnengǁbutton_wenhao_XML
	}
	interface buttonsǁgongnengǁbutton_wenhao_XMLǃbtn_help_renyiduiziǃCOMPONENT extends buttonsǁgongnengǁbutton_wenhao_XML{
		parent: Main_XML
	}
	interface buttonsǁgongnengǁbutton_wenhao_XMLǃbtn_help_xianduiǃCOMPONENT extends buttonsǁgongnengǁbutton_wenhao_XML{
		parent: Main_XML
	}
	interface buttonsǁgongnengǁbutton_wenhao_XMLǃbtn_help_daǃCOMPONENT extends buttonsǁgongnengǁbutton_wenhao_XML{
		parent: Main_XML
	}
	interface buttonsǁgongnengǁbutton_wenhao_XMLǃbtn_help_xiaoǃCOMPONENT extends buttonsǁgongnengǁbutton_wenhao_XML{
		parent: Main_XML
	}
	interface buttonsǁgongnengǁbutton_wenhao_XMLǃbtn_help_zhuangduiǃCOMPONENT extends buttonsǁgongnengǁbutton_wenhao_XML{
		parent: Main_XML
	}
	interface buttonsǁgongnengǁbutton_wenhao_XMLǃbtn_help_wanmeiduiziǃCOMPONENT extends buttonsǁgongnengǁbutton_wenhao_XML{
		parent: Main_XML
	}
	interface otherǁtable_chouma_XML extends fairygui.GComponent{
		getChild(name: 'chouma_5'): otherǁchouma_common_XMLǃchouma_5ǃCOMPONENT
		getChildAt(index: 0): otherǁchouma_common_XMLǃchouma_5ǃCOMPONENT
		getChildById(id: 'n0_s6qw'): otherǁchouma_common_XMLǃchouma_5ǃCOMPONENT
		getChild(name: 'chouma_4'): otherǁchouma_common_XMLǃchouma_4ǃCOMPONENT
		getChildAt(index: 1): otherǁchouma_common_XMLǃchouma_4ǃCOMPONENT
		getChildById(id: 'n1_s6qw'): otherǁchouma_common_XMLǃchouma_4ǃCOMPONENT
		getChild(name: 'chouma_6'): otherǁchouma_common_XMLǃchouma_6ǃCOMPONENT
		getChildAt(index: 2): otherǁchouma_common_XMLǃchouma_6ǃCOMPONENT
		getChildById(id: 'n2_s6qw'): otherǁchouma_common_XMLǃchouma_6ǃCOMPONENT
		getChild(name: 'chouma_2'): otherǁchouma_common_XMLǃchouma_2ǃCOMPONENT
		getChildAt(index: 3): otherǁchouma_common_XMLǃchouma_2ǃCOMPONENT
		getChildById(id: 'n3_s6qw'): otherǁchouma_common_XMLǃchouma_2ǃCOMPONENT
		getChild(name: 'chouma_1'): otherǁchouma_common_XMLǃchouma_1ǃCOMPONENT
		getChildAt(index: 4): otherǁchouma_common_XMLǃchouma_1ǃCOMPONENT
		getChildById(id: 'n4_s6qw'): otherǁchouma_common_XMLǃchouma_1ǃCOMPONENT
		getChild(name: 'chouma_3'): otherǁchouma_common_XMLǃchouma_3ǃCOMPONENT
		getChildAt(index: 5): otherǁchouma_common_XMLǃchouma_3ǃCOMPONENT
		getChildById(id: 'n5_s6qw'): otherǁchouma_common_XMLǃchouma_3ǃCOMPONENT
		_children: [
			otherǁchouma_common_XMLǃchouma_5ǃCOMPONENT,
			otherǁchouma_common_XMLǃchouma_4ǃCOMPONENT,
			otherǁchouma_common_XMLǃchouma_6ǃCOMPONENT,
			otherǁchouma_common_XMLǃchouma_2ǃCOMPONENT,
			otherǁchouma_common_XMLǃchouma_1ǃCOMPONENT,
			otherǁchouma_common_XMLǃchouma_3ǃCOMPONENT
		]
		getController(name: '__language'): otherǁtable_chouma_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): otherǁtable_chouma_XMLǃ__languageǃCONTROLLER
		_controllers: [
			otherǁtable_chouma_XMLǃ__languageǃCONTROLLER
		]
	}
	interface otherǁtable_chouma_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: otherǁtable_chouma_XML
	}
	interface otherǁchouma_common_XML extends fairygui.GComponent{
		getChild(name: 'n0'): otherǁchouma_common_XMLǃn0ǃIMAGE
		getChildAt(index: 0): otherǁchouma_common_XMLǃn0ǃIMAGE
		getChildById(id: 'n0_s6qw'): otherǁchouma_common_XMLǃn0ǃIMAGE
		getChild(name: 'n1'): otherǁchouma_common_XMLǃn1ǃIMAGE
		getChildAt(index: 1): otherǁchouma_common_XMLǃn1ǃIMAGE
		getChildById(id: 'n1_s6qw'): otherǁchouma_common_XMLǃn1ǃIMAGE
		getChild(name: 'n2'): otherǁchouma_common_XMLǃn2ǃIMAGE
		getChildAt(index: 2): otherǁchouma_common_XMLǃn2ǃIMAGE
		getChildById(id: 'n2_s6qw'): otherǁchouma_common_XMLǃn2ǃIMAGE
		getChild(name: 'n3'): otherǁchouma_common_XMLǃn3ǃIMAGE
		getChildAt(index: 3): otherǁchouma_common_XMLǃn3ǃIMAGE
		getChildById(id: 'n3_s6qw'): otherǁchouma_common_XMLǃn3ǃIMAGE
		getChild(name: 'n4'): otherǁchouma_common_XMLǃn4ǃIMAGE
		getChildAt(index: 4): otherǁchouma_common_XMLǃn4ǃIMAGE
		getChildById(id: 'n4_s6qw'): otherǁchouma_common_XMLǃn4ǃIMAGE
		getChild(name: 'n5'): otherǁchouma_common_XMLǃn5ǃIMAGE
		getChildAt(index: 5): otherǁchouma_common_XMLǃn5ǃIMAGE
		getChildById(id: 'n5_s6qw'): otherǁchouma_common_XMLǃn5ǃIMAGE
		getChild(name: 'n6'): otherǁchouma_common_XMLǃn6ǃIMAGE
		getChildAt(index: 6): otherǁchouma_common_XMLǃn6ǃIMAGE
		getChildById(id: 'n6_s6qw'): otherǁchouma_common_XMLǃn6ǃIMAGE
		_children: [
			otherǁchouma_common_XMLǃn0ǃIMAGE,
			otherǁchouma_common_XMLǃn1ǃIMAGE,
			otherǁchouma_common_XMLǃn2ǃIMAGE,
			otherǁchouma_common_XMLǃn3ǃIMAGE,
			otherǁchouma_common_XMLǃn4ǃIMAGE,
			otherǁchouma_common_XMLǃn5ǃIMAGE,
			otherǁchouma_common_XMLǃn6ǃIMAGE
		]
		getController(name: '__language'): otherǁchouma_common_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): otherǁchouma_common_XMLǃ__languageǃCONTROLLER
		getController(name: 'c1'): otherǁchouma_common_XMLǃc1ǃCONTROLLER
		getControllerAt(index: 1): otherǁchouma_common_XMLǃc1ǃCONTROLLER
		_controllers: [
			otherǁchouma_common_XMLǃ__languageǃCONTROLLER,
			otherǁchouma_common_XMLǃc1ǃCONTROLLER
		]
	}
	interface otherǁchouma_common_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: otherǁchouma_common_XML
	}
	interface otherǁchouma_common_XMLǃc1ǃCONTROLLER extends fairygui.Controller{
		_parent: otherǁchouma_common_XML
	}
	interface otherǁchouma_common_XMLǃn0ǃIMAGE extends fairygui.GImage{
		parent: otherǁchouma_common_XML
	}
	interface otherǁchouma_common_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: otherǁchouma_common_XML
	}
	interface otherǁchouma_common_XMLǃn2ǃIMAGE extends fairygui.GImage{
		parent: otherǁchouma_common_XML
	}
	interface otherǁchouma_common_XMLǃn3ǃIMAGE extends fairygui.GImage{
		parent: otherǁchouma_common_XML
	}
	interface otherǁchouma_common_XMLǃn4ǃIMAGE extends fairygui.GImage{
		parent: otherǁchouma_common_XML
	}
	interface otherǁchouma_common_XMLǃn5ǃIMAGE extends fairygui.GImage{
		parent: otherǁchouma_common_XML
	}
	interface otherǁchouma_common_XMLǃn6ǃIMAGE extends fairygui.GImage{
		parent: otherǁchouma_common_XML
	}
	interface otherǁchouma_common_XMLǃchouma_5ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: otherǁtable_chouma_XML
	}
	interface otherǁchouma_common_XMLǃchouma_4ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: otherǁtable_chouma_XML
	}
	interface otherǁchouma_common_XMLǃchouma_6ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: otherǁtable_chouma_XML
	}
	interface otherǁchouma_common_XMLǃchouma_2ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: otherǁtable_chouma_XML
	}
	interface otherǁchouma_common_XMLǃchouma_1ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: otherǁtable_chouma_XML
	}
	interface otherǁchouma_common_XMLǃchouma_3ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: otherǁtable_chouma_XML
	}
	interface otherǁtable_chouma_XMLǃtable_xianǃCOMPONENT extends otherǁtable_chouma_XML{
		parent: Main_XML
	}
	interface otherǁtable_chouma_XMLǃtable_heǃCOMPONENT extends otherǁtable_chouma_XML{
		parent: Main_XML
	}
	interface otherǁtable_chouma_XMLǃtable_zhuangǃCOMPONENT extends otherǁtable_chouma_XML{
		parent: Main_XML
	}
	interface otherǁtable_chouma2_XML extends fairygui.GComponent{
		getChild(name: 'chouma_3'): otherǁtable_chouma2_XMLǃchouma_3ǃCOMPONENT
		getChildAt(index: 0): otherǁtable_chouma2_XMLǃchouma_3ǃCOMPONENT
		getChildById(id: 'n2_s6qw'): otherǁtable_chouma2_XMLǃchouma_3ǃCOMPONENT
		getChild(name: 'chouma_2'): otherǁtable_chouma2_XMLǃchouma_2ǃCOMPONENT
		getChildAt(index: 1): otherǁtable_chouma2_XMLǃchouma_2ǃCOMPONENT
		getChildById(id: 'n1_s6qw'): otherǁtable_chouma2_XMLǃchouma_2ǃCOMPONENT
		getChild(name: 'chouma_1'): otherǁtable_chouma2_XMLǃchouma_1ǃCOMPONENT
		getChildAt(index: 2): otherǁtable_chouma2_XMLǃchouma_1ǃCOMPONENT
		getChildById(id: 'n0_s6qw'): otherǁtable_chouma2_XMLǃchouma_1ǃCOMPONENT
		_children: [
			otherǁtable_chouma2_XMLǃchouma_3ǃCOMPONENT,
			otherǁtable_chouma2_XMLǃchouma_2ǃCOMPONENT,
			otherǁtable_chouma2_XMLǃchouma_1ǃCOMPONENT
		]
		getController(name: '__language'): otherǁtable_chouma2_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): otherǁtable_chouma2_XMLǃ__languageǃCONTROLLER
		_controllers: [
			otherǁtable_chouma2_XMLǃ__languageǃCONTROLLER
		]
	}
	interface otherǁtable_chouma2_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: otherǁtable_chouma2_XML
	}
	interface otherǁtable_chouma2_XMLǃchouma_3ǃCOMPONENT extends fairygui.GComponent{
		parent: otherǁtable_chouma2_XML
	}
	interface otherǁtable_chouma2_XMLǃchouma_2ǃCOMPONENT extends fairygui.GComponent{
		parent: otherǁtable_chouma2_XML
	}
	interface otherǁtable_chouma2_XMLǃchouma_1ǃCOMPONENT extends fairygui.GComponent{
		parent: otherǁtable_chouma2_XML
	}
	interface otherǁtable_chouma2_XMLǃtable_renyiduiziǃCOMPONENT extends otherǁtable_chouma2_XML{
		parent: Main_XML
	}
	interface otherǁtable_chouma2_XMLǃtable_xianduiǃCOMPONENT extends otherǁtable_chouma2_XML{
		parent: Main_XML
	}
	interface otherǁtable_chouma2_XMLǃtable_daǃCOMPONENT extends otherǁtable_chouma2_XML{
		parent: Main_XML
	}
	interface otherǁtable_chouma2_XMLǃtable_xiaoǃCOMPONENT extends otherǁtable_chouma2_XML{
		parent: Main_XML
	}
	interface otherǁtable_chouma2_XMLǃtable_zhuangduiǃCOMPONENT extends otherǁtable_chouma2_XML{
		parent: Main_XML
	}
	interface otherǁtable_chouma2_XMLǃtable_wanmeiduiziǃCOMPONENT extends otherǁtable_chouma2_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_xian_1ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_xian_2ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_xian_3ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_xian_4ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_xian_5ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_xian_6ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_da_1ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_da_2ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_da_3ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_da_4ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_da_5ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_da_6ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_he_1ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_he_2ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_he_3ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_he_4ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_he_5ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_he_6ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_xiao_1ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_xiao_2ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_xiao_3ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_xiao_4ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_xiao_5ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_xiao_6ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_wanmeiduizi_1ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_wanmeiduizi_2ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_wanmeiduizi_3ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_wanmeiduizi_4ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_wanmeiduizi_5ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_wanmeiduizi_6ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_renyiduizi_1ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_renyiduizi_2ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_renyiduizi_3ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_renyiduizi_4ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_renyiduizi_5ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_renyiduizi_6ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_zhuangdui_1ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_zhuangdui_2ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_zhuangdui_3ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_zhuangdui_4ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_zhuangdui_5ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_zhuangdui_6ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_xiandui_1ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_xiandui_2ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_xiandui_3ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_xiandui_4ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_xiandui_5ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_xiandui_6ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_zhuang_1ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_zhuang_2ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_zhuang_3ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_zhuang_4ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_zhuang_5ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁchouma_common_XMLǃplayerchouma_zhuang_6ǃCOMPONENT extends otherǁchouma_common_XML{
		parent: Main_XML
	}
	interface otherǁzhongjiang_mask5_XML extends fairygui.GComponent{
		getChild(name: 'n0'): otherǁzhongjiang_mask5_XMLǃn0ǃIMAGE
		getChildAt(index: 0): otherǁzhongjiang_mask5_XMLǃn0ǃIMAGE
		getChildById(id: 'n0_mxij'): otherǁzhongjiang_mask5_XMLǃn0ǃIMAGE
		getChild(name: 'tf_amount'): otherǁzhongjiang_mask5_XMLǃtf_amountǃTEXT
		getChildAt(index: 1): otherǁzhongjiang_mask5_XMLǃtf_amountǃTEXT
		getChildById(id: 'n2_mxij'): otherǁzhongjiang_mask5_XMLǃtf_amountǃTEXT
		_children: [
			otherǁzhongjiang_mask5_XMLǃn0ǃIMAGE,
			otherǁzhongjiang_mask5_XMLǃtf_amountǃTEXT
		]
		getTransition(name: 't0'): otherǁzhongjiang_mask5_XMLǃt0ǃTRANSITION
		getTransitionAt(index: 0): otherǁzhongjiang_mask5_XMLǃt0ǃTRANSITION
		_transitions: [
			otherǁzhongjiang_mask5_XMLǃt0ǃTRANSITION
		]
		getController(name: '__language'): otherǁzhongjiang_mask5_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): otherǁzhongjiang_mask5_XMLǃ__languageǃCONTROLLER
		_controllers: [
			otherǁzhongjiang_mask5_XMLǃ__languageǃCONTROLLER
		]
	}
	interface otherǁzhongjiang_mask5_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: otherǁzhongjiang_mask5_XML
	}
	interface otherǁzhongjiang_mask5_XMLǃn0ǃIMAGE extends fairygui.GImage{
		parent: otherǁzhongjiang_mask5_XML
	}
	interface otherǁzhongjiang_mask5_XMLǃtf_amountǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁzhongjiang_mask5_XML
	}
	interface otherǁzhongjiang_mask5_XMLǃt0ǃTRANSITION extends fairygui.Transition{
		_owner: otherǁzhongjiang_mask5_XML
	}
	interface otherǁzhongjiang_mask5_XMLǃmask_xianǃCOMPONENT extends otherǁzhongjiang_mask5_XML{
		parent: Main_XML
	}
	interface otherǁzhongjiang_mask4_XML extends fairygui.GComponent{
		getChild(name: 'n0'): otherǁzhongjiang_mask4_XMLǃn0ǃIMAGE
		getChildAt(index: 0): otherǁzhongjiang_mask4_XMLǃn0ǃIMAGE
		getChildById(id: 'n0_mxij'): otherǁzhongjiang_mask4_XMLǃn0ǃIMAGE
		getChild(name: 'tf_amount'): otherǁzhongjiang_mask4_XMLǃtf_amountǃTEXT
		getChildAt(index: 1): otherǁzhongjiang_mask4_XMLǃtf_amountǃTEXT
		getChildById(id: 'n2_mxij'): otherǁzhongjiang_mask4_XMLǃtf_amountǃTEXT
		_children: [
			otherǁzhongjiang_mask4_XMLǃn0ǃIMAGE,
			otherǁzhongjiang_mask4_XMLǃtf_amountǃTEXT
		]
		getTransition(name: 't0'): otherǁzhongjiang_mask4_XMLǃt0ǃTRANSITION
		getTransitionAt(index: 0): otherǁzhongjiang_mask4_XMLǃt0ǃTRANSITION
		_transitions: [
			otherǁzhongjiang_mask4_XMLǃt0ǃTRANSITION
		]
		getController(name: '__language'): otherǁzhongjiang_mask4_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): otherǁzhongjiang_mask4_XMLǃ__languageǃCONTROLLER
		_controllers: [
			otherǁzhongjiang_mask4_XMLǃ__languageǃCONTROLLER
		]
	}
	interface otherǁzhongjiang_mask4_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: otherǁzhongjiang_mask4_XML
	}
	interface otherǁzhongjiang_mask4_XMLǃn0ǃIMAGE extends fairygui.GImage{
		parent: otherǁzhongjiang_mask4_XML
	}
	interface otherǁzhongjiang_mask4_XMLǃtf_amountǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁzhongjiang_mask4_XML
	}
	interface otherǁzhongjiang_mask4_XMLǃt0ǃTRANSITION extends fairygui.Transition{
		_owner: otherǁzhongjiang_mask4_XML
	}
	interface otherǁzhongjiang_mask4_XMLǃmask_zhuangǃCOMPONENT extends otherǁzhongjiang_mask4_XML{
		parent: Main_XML
	}
	interface otherǁzhongjiang_mask2_XML extends fairygui.GComponent{
		getChild(name: 'n0'): otherǁzhongjiang_mask2_XMLǃn0ǃIMAGE
		getChildAt(index: 0): otherǁzhongjiang_mask2_XMLǃn0ǃIMAGE
		getChildById(id: 'n0_mxij'): otherǁzhongjiang_mask2_XMLǃn0ǃIMAGE
		getChild(name: 'tf_amount'): otherǁzhongjiang_mask2_XMLǃtf_amountǃTEXT
		getChildAt(index: 1): otherǁzhongjiang_mask2_XMLǃtf_amountǃTEXT
		getChildById(id: 'n2_mxij'): otherǁzhongjiang_mask2_XMLǃtf_amountǃTEXT
		_children: [
			otherǁzhongjiang_mask2_XMLǃn0ǃIMAGE,
			otherǁzhongjiang_mask2_XMLǃtf_amountǃTEXT
		]
		getTransition(name: 't0'): otherǁzhongjiang_mask2_XMLǃt0ǃTRANSITION
		getTransitionAt(index: 0): otherǁzhongjiang_mask2_XMLǃt0ǃTRANSITION
		_transitions: [
			otherǁzhongjiang_mask2_XMLǃt0ǃTRANSITION
		]
		getController(name: '__language'): otherǁzhongjiang_mask2_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): otherǁzhongjiang_mask2_XMLǃ__languageǃCONTROLLER
		_controllers: [
			otherǁzhongjiang_mask2_XMLǃ__languageǃCONTROLLER
		]
	}
	interface otherǁzhongjiang_mask2_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: otherǁzhongjiang_mask2_XML
	}
	interface otherǁzhongjiang_mask2_XMLǃn0ǃIMAGE extends fairygui.GImage{
		parent: otherǁzhongjiang_mask2_XML
	}
	interface otherǁzhongjiang_mask2_XMLǃtf_amountǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁzhongjiang_mask2_XML
	}
	interface otherǁzhongjiang_mask2_XMLǃt0ǃTRANSITION extends fairygui.Transition{
		_owner: otherǁzhongjiang_mask2_XML
	}
	interface otherǁzhongjiang_mask2_XMLǃmask_heǃCOMPONENT extends otherǁzhongjiang_mask2_XML{
		parent: Main_XML
	}
	interface otherǁzhongjiang_mask2_XMLǃmask_renyiduiziǃCOMPONENT extends otherǁzhongjiang_mask2_XML{
		parent: Main_XML
	}
	interface otherǁzhongjiang_mask2_XMLǃmask_xianduiǃCOMPONENT extends otherǁzhongjiang_mask2_XML{
		parent: Main_XML
	}
	interface otherǁzhongjiang_mask2_XMLǃmask_wanmeiduiziǃCOMPONENT extends otherǁzhongjiang_mask2_XML{
		parent: Main_XML
	}
	interface otherǁzhongjiang_mask2_XMLǃmask_zhuangduiǃCOMPONENT extends otherǁzhongjiang_mask2_XML{
		parent: Main_XML
	}
	interface otherǁzhongjiang_mask2_XMLǃmask_xiaoǃCOMPONENT extends otherǁzhongjiang_mask2_XML{
		parent: Main_XML
	}
	interface otherǁzhongjiang_mask2_XMLǃmask_daǃCOMPONENT extends otherǁzhongjiang_mask2_XML{
		parent: Main_XML
	}
	interface otherǁhelpǁhelp_renyiduizi_XML extends fairygui.GComponent{
		getChild(name: 'n0'): otherǁhelpǁhelp_renyiduizi_XMLǃn0ǃIMAGE
		getChildAt(index: 0): otherǁhelpǁhelp_renyiduizi_XMLǃn0ǃIMAGE
		getChildById(id: 'n0_ww6c'): otherǁhelpǁhelp_renyiduizi_XMLǃn0ǃIMAGE
		getChild(name: 'n1_CN2'): otherǁhelpǁhelp_renyiduizi_XMLǃn1_CN2ǃTEXT
		getChildAt(index: 1): otherǁhelpǁhelp_renyiduizi_XMLǃn1_CN2ǃTEXT
		getChildById(id: 'n1_ww6c_CN2'): otherǁhelpǁhelp_renyiduizi_XMLǃn1_CN2ǃTEXT
		getChild(name: 'n1_EN'): otherǁhelpǁhelp_renyiduizi_XMLǃn1_ENǃTEXT
		getChildAt(index: 2): otherǁhelpǁhelp_renyiduizi_XMLǃn1_ENǃTEXT
		getChildById(id: 'n1_ww6c_EN'): otherǁhelpǁhelp_renyiduizi_XMLǃn1_ENǃTEXT
		getChild(name: 'n1_IN'): otherǁhelpǁhelp_renyiduizi_XMLǃn1_INǃTEXT
		getChildAt(index: 3): otherǁhelpǁhelp_renyiduizi_XMLǃn1_INǃTEXT
		getChildById(id: 'n1_ww6c_IN'): otherǁhelpǁhelp_renyiduizi_XMLǃn1_INǃTEXT
		getChild(name: 'n1_JP'): otherǁhelpǁhelp_renyiduizi_XMLǃn1_JPǃTEXT
		getChildAt(index: 4): otherǁhelpǁhelp_renyiduizi_XMLǃn1_JPǃTEXT
		getChildById(id: 'n1_ww6c_JP'): otherǁhelpǁhelp_renyiduizi_XMLǃn1_JPǃTEXT
		getChild(name: 'n1_KR'): otherǁhelpǁhelp_renyiduizi_XMLǃn1_KRǃTEXT
		getChildAt(index: 5): otherǁhelpǁhelp_renyiduizi_XMLǃn1_KRǃTEXT
		getChildById(id: 'n1_ww6c_KR'): otherǁhelpǁhelp_renyiduizi_XMLǃn1_KRǃTEXT
		getChild(name: 'n1_TH'): otherǁhelpǁhelp_renyiduizi_XMLǃn1_THǃTEXT
		getChildAt(index: 6): otherǁhelpǁhelp_renyiduizi_XMLǃn1_THǃTEXT
		getChildById(id: 'n1_ww6c_TH'): otherǁhelpǁhelp_renyiduizi_XMLǃn1_THǃTEXT
		getChild(name: 'n1_VN'): otherǁhelpǁhelp_renyiduizi_XMLǃn1_VNǃTEXT
		getChildAt(index: 7): otherǁhelpǁhelp_renyiduizi_XMLǃn1_VNǃTEXT
		getChildById(id: 'n1_ww6c_VN'): otherǁhelpǁhelp_renyiduizi_XMLǃn1_VNǃTEXT
		getChild(name: 'n1'): otherǁhelpǁhelp_renyiduizi_XMLǃn1ǃTEXT
		getChildAt(index: 8): otherǁhelpǁhelp_renyiduizi_XMLǃn1ǃTEXT
		getChildById(id: 'n1_ww6c'): otherǁhelpǁhelp_renyiduizi_XMLǃn1ǃTEXT
		_children: [
			otherǁhelpǁhelp_renyiduizi_XMLǃn0ǃIMAGE,
			otherǁhelpǁhelp_renyiduizi_XMLǃn1_CN2ǃTEXT,
			otherǁhelpǁhelp_renyiduizi_XMLǃn1_ENǃTEXT,
			otherǁhelpǁhelp_renyiduizi_XMLǃn1_INǃTEXT,
			otherǁhelpǁhelp_renyiduizi_XMLǃn1_JPǃTEXT,
			otherǁhelpǁhelp_renyiduizi_XMLǃn1_KRǃTEXT,
			otherǁhelpǁhelp_renyiduizi_XMLǃn1_THǃTEXT,
			otherǁhelpǁhelp_renyiduizi_XMLǃn1_VNǃTEXT,
			otherǁhelpǁhelp_renyiduizi_XMLǃn1ǃTEXT
		]
		getController(name: '__language'): otherǁhelpǁhelp_renyiduizi_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): otherǁhelpǁhelp_renyiduizi_XMLǃ__languageǃCONTROLLER
		_controllers: [
			otherǁhelpǁhelp_renyiduizi_XMLǃ__languageǃCONTROLLER
		]
	}
	interface otherǁhelpǁhelp_renyiduizi_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: otherǁhelpǁhelp_renyiduizi_XML
	}
	interface otherǁhelpǁhelp_renyiduizi_XMLǃn0ǃIMAGE extends fairygui.GImage{
		parent: otherǁhelpǁhelp_renyiduizi_XML
	}
	interface otherǁhelpǁhelp_renyiduizi_XMLǃn1_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_renyiduizi_XML
	}
	interface otherǁhelpǁhelp_renyiduizi_XMLǃn1_ENǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_renyiduizi_XML
	}
	interface otherǁhelpǁhelp_renyiduizi_XMLǃn1_INǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_renyiduizi_XML
	}
	interface otherǁhelpǁhelp_renyiduizi_XMLǃn1_JPǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_renyiduizi_XML
	}
	interface otherǁhelpǁhelp_renyiduizi_XMLǃn1_KRǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_renyiduizi_XML
	}
	interface otherǁhelpǁhelp_renyiduizi_XMLǃn1_THǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_renyiduizi_XML
	}
	interface otherǁhelpǁhelp_renyiduizi_XMLǃn1_VNǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_renyiduizi_XML
	}
	interface otherǁhelpǁhelp_renyiduizi_XMLǃn1ǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_renyiduizi_XML
	}
	interface otherǁhelpǁhelp_renyiduizi_XMLǃhelp_1ǃCOMPONENT extends otherǁhelpǁhelp_renyiduizi_XML{
		parent: Main_XML
	}
	interface otherǁhelpǁhelp_xiandui_XML extends fairygui.GComponent{
		getChild(name: 'n0'): otherǁhelpǁhelp_xiandui_XMLǃn0ǃIMAGE
		getChildAt(index: 0): otherǁhelpǁhelp_xiandui_XMLǃn0ǃIMAGE
		getChildById(id: 'n0_ww6c'): otherǁhelpǁhelp_xiandui_XMLǃn0ǃIMAGE
		getChild(name: 'n1_CN2'): otherǁhelpǁhelp_xiandui_XMLǃn1_CN2ǃTEXT
		getChildAt(index: 1): otherǁhelpǁhelp_xiandui_XMLǃn1_CN2ǃTEXT
		getChildById(id: 'n1_ww6c_CN2'): otherǁhelpǁhelp_xiandui_XMLǃn1_CN2ǃTEXT
		getChild(name: 'n1_EN'): otherǁhelpǁhelp_xiandui_XMLǃn1_ENǃTEXT
		getChildAt(index: 2): otherǁhelpǁhelp_xiandui_XMLǃn1_ENǃTEXT
		getChildById(id: 'n1_ww6c_EN'): otherǁhelpǁhelp_xiandui_XMLǃn1_ENǃTEXT
		getChild(name: 'n1_IN'): otherǁhelpǁhelp_xiandui_XMLǃn1_INǃTEXT
		getChildAt(index: 3): otherǁhelpǁhelp_xiandui_XMLǃn1_INǃTEXT
		getChildById(id: 'n1_ww6c_IN'): otherǁhelpǁhelp_xiandui_XMLǃn1_INǃTEXT
		getChild(name: 'n1_JP'): otherǁhelpǁhelp_xiandui_XMLǃn1_JPǃTEXT
		getChildAt(index: 4): otherǁhelpǁhelp_xiandui_XMLǃn1_JPǃTEXT
		getChildById(id: 'n1_ww6c_JP'): otherǁhelpǁhelp_xiandui_XMLǃn1_JPǃTEXT
		getChild(name: 'n1_KR'): otherǁhelpǁhelp_xiandui_XMLǃn1_KRǃTEXT
		getChildAt(index: 5): otherǁhelpǁhelp_xiandui_XMLǃn1_KRǃTEXT
		getChildById(id: 'n1_ww6c_KR'): otherǁhelpǁhelp_xiandui_XMLǃn1_KRǃTEXT
		getChild(name: 'n1_TH'): otherǁhelpǁhelp_xiandui_XMLǃn1_THǃTEXT
		getChildAt(index: 6): otherǁhelpǁhelp_xiandui_XMLǃn1_THǃTEXT
		getChildById(id: 'n1_ww6c_TH'): otherǁhelpǁhelp_xiandui_XMLǃn1_THǃTEXT
		getChild(name: 'n1_VN'): otherǁhelpǁhelp_xiandui_XMLǃn1_VNǃTEXT
		getChildAt(index: 7): otherǁhelpǁhelp_xiandui_XMLǃn1_VNǃTEXT
		getChildById(id: 'n1_ww6c_VN'): otherǁhelpǁhelp_xiandui_XMLǃn1_VNǃTEXT
		getChild(name: 'n1'): otherǁhelpǁhelp_xiandui_XMLǃn1ǃTEXT
		getChildAt(index: 8): otherǁhelpǁhelp_xiandui_XMLǃn1ǃTEXT
		getChildById(id: 'n1_ww6c'): otherǁhelpǁhelp_xiandui_XMLǃn1ǃTEXT
		_children: [
			otherǁhelpǁhelp_xiandui_XMLǃn0ǃIMAGE,
			otherǁhelpǁhelp_xiandui_XMLǃn1_CN2ǃTEXT,
			otherǁhelpǁhelp_xiandui_XMLǃn1_ENǃTEXT,
			otherǁhelpǁhelp_xiandui_XMLǃn1_INǃTEXT,
			otherǁhelpǁhelp_xiandui_XMLǃn1_JPǃTEXT,
			otherǁhelpǁhelp_xiandui_XMLǃn1_KRǃTEXT,
			otherǁhelpǁhelp_xiandui_XMLǃn1_THǃTEXT,
			otherǁhelpǁhelp_xiandui_XMLǃn1_VNǃTEXT,
			otherǁhelpǁhelp_xiandui_XMLǃn1ǃTEXT
		]
		getController(name: '__language'): otherǁhelpǁhelp_xiandui_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): otherǁhelpǁhelp_xiandui_XMLǃ__languageǃCONTROLLER
		_controllers: [
			otherǁhelpǁhelp_xiandui_XMLǃ__languageǃCONTROLLER
		]
	}
	interface otherǁhelpǁhelp_xiandui_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: otherǁhelpǁhelp_xiandui_XML
	}
	interface otherǁhelpǁhelp_xiandui_XMLǃn0ǃIMAGE extends fairygui.GImage{
		parent: otherǁhelpǁhelp_xiandui_XML
	}
	interface otherǁhelpǁhelp_xiandui_XMLǃn1_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_xiandui_XML
	}
	interface otherǁhelpǁhelp_xiandui_XMLǃn1_ENǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_xiandui_XML
	}
	interface otherǁhelpǁhelp_xiandui_XMLǃn1_INǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_xiandui_XML
	}
	interface otherǁhelpǁhelp_xiandui_XMLǃn1_JPǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_xiandui_XML
	}
	interface otherǁhelpǁhelp_xiandui_XMLǃn1_KRǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_xiandui_XML
	}
	interface otherǁhelpǁhelp_xiandui_XMLǃn1_THǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_xiandui_XML
	}
	interface otherǁhelpǁhelp_xiandui_XMLǃn1_VNǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_xiandui_XML
	}
	interface otherǁhelpǁhelp_xiandui_XMLǃn1ǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_xiandui_XML
	}
	interface otherǁhelpǁhelp_xiandui_XMLǃhelp_2ǃCOMPONENT extends otherǁhelpǁhelp_xiandui_XML{
		parent: Main_XML
	}
	interface otherǁhelpǁhelp_da_XML extends fairygui.GComponent{
		getChild(name: 'n0'): otherǁhelpǁhelp_da_XMLǃn0ǃIMAGE
		getChildAt(index: 0): otherǁhelpǁhelp_da_XMLǃn0ǃIMAGE
		getChildById(id: 'n0_ww6c'): otherǁhelpǁhelp_da_XMLǃn0ǃIMAGE
		getChild(name: 'n1_CN2'): otherǁhelpǁhelp_da_XMLǃn1_CN2ǃTEXT
		getChildAt(index: 1): otherǁhelpǁhelp_da_XMLǃn1_CN2ǃTEXT
		getChildById(id: 'n1_ww6c_CN2'): otherǁhelpǁhelp_da_XMLǃn1_CN2ǃTEXT
		getChild(name: 'n1_EN'): otherǁhelpǁhelp_da_XMLǃn1_ENǃTEXT
		getChildAt(index: 2): otherǁhelpǁhelp_da_XMLǃn1_ENǃTEXT
		getChildById(id: 'n1_ww6c_EN'): otherǁhelpǁhelp_da_XMLǃn1_ENǃTEXT
		getChild(name: 'n1_IN'): otherǁhelpǁhelp_da_XMLǃn1_INǃTEXT
		getChildAt(index: 3): otherǁhelpǁhelp_da_XMLǃn1_INǃTEXT
		getChildById(id: 'n1_ww6c_IN'): otherǁhelpǁhelp_da_XMLǃn1_INǃTEXT
		getChild(name: 'n1_JP'): otherǁhelpǁhelp_da_XMLǃn1_JPǃTEXT
		getChildAt(index: 4): otherǁhelpǁhelp_da_XMLǃn1_JPǃTEXT
		getChildById(id: 'n1_ww6c_JP'): otherǁhelpǁhelp_da_XMLǃn1_JPǃTEXT
		getChild(name: 'n1_KR'): otherǁhelpǁhelp_da_XMLǃn1_KRǃTEXT
		getChildAt(index: 5): otherǁhelpǁhelp_da_XMLǃn1_KRǃTEXT
		getChildById(id: 'n1_ww6c_KR'): otherǁhelpǁhelp_da_XMLǃn1_KRǃTEXT
		getChild(name: 'n1_TH'): otherǁhelpǁhelp_da_XMLǃn1_THǃTEXT
		getChildAt(index: 6): otherǁhelpǁhelp_da_XMLǃn1_THǃTEXT
		getChildById(id: 'n1_ww6c_TH'): otherǁhelpǁhelp_da_XMLǃn1_THǃTEXT
		getChild(name: 'n1_VN'): otherǁhelpǁhelp_da_XMLǃn1_VNǃTEXT
		getChildAt(index: 7): otherǁhelpǁhelp_da_XMLǃn1_VNǃTEXT
		getChildById(id: 'n1_ww6c_VN'): otherǁhelpǁhelp_da_XMLǃn1_VNǃTEXT
		getChild(name: 'n1'): otherǁhelpǁhelp_da_XMLǃn1ǃTEXT
		getChildAt(index: 8): otherǁhelpǁhelp_da_XMLǃn1ǃTEXT
		getChildById(id: 'n1_ww6c'): otherǁhelpǁhelp_da_XMLǃn1ǃTEXT
		_children: [
			otherǁhelpǁhelp_da_XMLǃn0ǃIMAGE,
			otherǁhelpǁhelp_da_XMLǃn1_CN2ǃTEXT,
			otherǁhelpǁhelp_da_XMLǃn1_ENǃTEXT,
			otherǁhelpǁhelp_da_XMLǃn1_INǃTEXT,
			otherǁhelpǁhelp_da_XMLǃn1_JPǃTEXT,
			otherǁhelpǁhelp_da_XMLǃn1_KRǃTEXT,
			otherǁhelpǁhelp_da_XMLǃn1_THǃTEXT,
			otherǁhelpǁhelp_da_XMLǃn1_VNǃTEXT,
			otherǁhelpǁhelp_da_XMLǃn1ǃTEXT
		]
		getController(name: '__language'): otherǁhelpǁhelp_da_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): otherǁhelpǁhelp_da_XMLǃ__languageǃCONTROLLER
		_controllers: [
			otherǁhelpǁhelp_da_XMLǃ__languageǃCONTROLLER
		]
	}
	interface otherǁhelpǁhelp_da_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: otherǁhelpǁhelp_da_XML
	}
	interface otherǁhelpǁhelp_da_XMLǃn0ǃIMAGE extends fairygui.GImage{
		parent: otherǁhelpǁhelp_da_XML
	}
	interface otherǁhelpǁhelp_da_XMLǃn1_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_da_XML
	}
	interface otherǁhelpǁhelp_da_XMLǃn1_ENǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_da_XML
	}
	interface otherǁhelpǁhelp_da_XMLǃn1_INǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_da_XML
	}
	interface otherǁhelpǁhelp_da_XMLǃn1_JPǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_da_XML
	}
	interface otherǁhelpǁhelp_da_XMLǃn1_KRǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_da_XML
	}
	interface otherǁhelpǁhelp_da_XMLǃn1_THǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_da_XML
	}
	interface otherǁhelpǁhelp_da_XMLǃn1_VNǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_da_XML
	}
	interface otherǁhelpǁhelp_da_XMLǃn1ǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_da_XML
	}
	interface otherǁhelpǁhelp_da_XMLǃhelp_3ǃCOMPONENT extends otherǁhelpǁhelp_da_XML{
		parent: Main_XML
	}
	interface otherǁhelpǁhelp_xiao_XML extends fairygui.GComponent{
		getChild(name: 'n0'): otherǁhelpǁhelp_xiao_XMLǃn0ǃIMAGE
		getChildAt(index: 0): otherǁhelpǁhelp_xiao_XMLǃn0ǃIMAGE
		getChildById(id: 'n0_ww6c'): otherǁhelpǁhelp_xiao_XMLǃn0ǃIMAGE
		getChild(name: 'n1_CN2'): otherǁhelpǁhelp_xiao_XMLǃn1_CN2ǃTEXT
		getChildAt(index: 1): otherǁhelpǁhelp_xiao_XMLǃn1_CN2ǃTEXT
		getChildById(id: 'n1_ww6c_CN2'): otherǁhelpǁhelp_xiao_XMLǃn1_CN2ǃTEXT
		getChild(name: 'n1_EN'): otherǁhelpǁhelp_xiao_XMLǃn1_ENǃTEXT
		getChildAt(index: 2): otherǁhelpǁhelp_xiao_XMLǃn1_ENǃTEXT
		getChildById(id: 'n1_ww6c_EN'): otherǁhelpǁhelp_xiao_XMLǃn1_ENǃTEXT
		getChild(name: 'n1_IN'): otherǁhelpǁhelp_xiao_XMLǃn1_INǃTEXT
		getChildAt(index: 3): otherǁhelpǁhelp_xiao_XMLǃn1_INǃTEXT
		getChildById(id: 'n1_ww6c_IN'): otherǁhelpǁhelp_xiao_XMLǃn1_INǃTEXT
		getChild(name: 'n1_JP'): otherǁhelpǁhelp_xiao_XMLǃn1_JPǃTEXT
		getChildAt(index: 4): otherǁhelpǁhelp_xiao_XMLǃn1_JPǃTEXT
		getChildById(id: 'n1_ww6c_JP'): otherǁhelpǁhelp_xiao_XMLǃn1_JPǃTEXT
		getChild(name: 'n1_KR'): otherǁhelpǁhelp_xiao_XMLǃn1_KRǃTEXT
		getChildAt(index: 5): otherǁhelpǁhelp_xiao_XMLǃn1_KRǃTEXT
		getChildById(id: 'n1_ww6c_KR'): otherǁhelpǁhelp_xiao_XMLǃn1_KRǃTEXT
		getChild(name: 'n1_TH'): otherǁhelpǁhelp_xiao_XMLǃn1_THǃTEXT
		getChildAt(index: 6): otherǁhelpǁhelp_xiao_XMLǃn1_THǃTEXT
		getChildById(id: 'n1_ww6c_TH'): otherǁhelpǁhelp_xiao_XMLǃn1_THǃTEXT
		getChild(name: 'n1_VN'): otherǁhelpǁhelp_xiao_XMLǃn1_VNǃTEXT
		getChildAt(index: 7): otherǁhelpǁhelp_xiao_XMLǃn1_VNǃTEXT
		getChildById(id: 'n1_ww6c_VN'): otherǁhelpǁhelp_xiao_XMLǃn1_VNǃTEXT
		getChild(name: 'n1'): otherǁhelpǁhelp_xiao_XMLǃn1ǃTEXT
		getChildAt(index: 8): otherǁhelpǁhelp_xiao_XMLǃn1ǃTEXT
		getChildById(id: 'n1_ww6c'): otherǁhelpǁhelp_xiao_XMLǃn1ǃTEXT
		_children: [
			otherǁhelpǁhelp_xiao_XMLǃn0ǃIMAGE,
			otherǁhelpǁhelp_xiao_XMLǃn1_CN2ǃTEXT,
			otherǁhelpǁhelp_xiao_XMLǃn1_ENǃTEXT,
			otherǁhelpǁhelp_xiao_XMLǃn1_INǃTEXT,
			otherǁhelpǁhelp_xiao_XMLǃn1_JPǃTEXT,
			otherǁhelpǁhelp_xiao_XMLǃn1_KRǃTEXT,
			otherǁhelpǁhelp_xiao_XMLǃn1_THǃTEXT,
			otherǁhelpǁhelp_xiao_XMLǃn1_VNǃTEXT,
			otherǁhelpǁhelp_xiao_XMLǃn1ǃTEXT
		]
		getController(name: '__language'): otherǁhelpǁhelp_xiao_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): otherǁhelpǁhelp_xiao_XMLǃ__languageǃCONTROLLER
		_controllers: [
			otherǁhelpǁhelp_xiao_XMLǃ__languageǃCONTROLLER
		]
	}
	interface otherǁhelpǁhelp_xiao_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: otherǁhelpǁhelp_xiao_XML
	}
	interface otherǁhelpǁhelp_xiao_XMLǃn0ǃIMAGE extends fairygui.GImage{
		parent: otherǁhelpǁhelp_xiao_XML
	}
	interface otherǁhelpǁhelp_xiao_XMLǃn1_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_xiao_XML
	}
	interface otherǁhelpǁhelp_xiao_XMLǃn1_ENǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_xiao_XML
	}
	interface otherǁhelpǁhelp_xiao_XMLǃn1_INǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_xiao_XML
	}
	interface otherǁhelpǁhelp_xiao_XMLǃn1_JPǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_xiao_XML
	}
	interface otherǁhelpǁhelp_xiao_XMLǃn1_KRǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_xiao_XML
	}
	interface otherǁhelpǁhelp_xiao_XMLǃn1_THǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_xiao_XML
	}
	interface otherǁhelpǁhelp_xiao_XMLǃn1_VNǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_xiao_XML
	}
	interface otherǁhelpǁhelp_xiao_XMLǃn1ǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_xiao_XML
	}
	interface otherǁhelpǁhelp_xiao_XMLǃhelp_4ǃCOMPONENT extends otherǁhelpǁhelp_xiao_XML{
		parent: Main_XML
	}
	interface otherǁhelpǁhelp_zhuangdui_XML extends fairygui.GComponent{
		getChild(name: 'n0'): otherǁhelpǁhelp_zhuangdui_XMLǃn0ǃIMAGE
		getChildAt(index: 0): otherǁhelpǁhelp_zhuangdui_XMLǃn0ǃIMAGE
		getChildById(id: 'n0_ww6c'): otherǁhelpǁhelp_zhuangdui_XMLǃn0ǃIMAGE
		getChild(name: 'n1_CN2'): otherǁhelpǁhelp_zhuangdui_XMLǃn1_CN2ǃTEXT
		getChildAt(index: 1): otherǁhelpǁhelp_zhuangdui_XMLǃn1_CN2ǃTEXT
		getChildById(id: 'n1_ww6c_CN2'): otherǁhelpǁhelp_zhuangdui_XMLǃn1_CN2ǃTEXT
		getChild(name: 'n1_EN'): otherǁhelpǁhelp_zhuangdui_XMLǃn1_ENǃTEXT
		getChildAt(index: 2): otherǁhelpǁhelp_zhuangdui_XMLǃn1_ENǃTEXT
		getChildById(id: 'n1_ww6c_EN'): otherǁhelpǁhelp_zhuangdui_XMLǃn1_ENǃTEXT
		getChild(name: 'n1_IN'): otherǁhelpǁhelp_zhuangdui_XMLǃn1_INǃTEXT
		getChildAt(index: 3): otherǁhelpǁhelp_zhuangdui_XMLǃn1_INǃTEXT
		getChildById(id: 'n1_ww6c_IN'): otherǁhelpǁhelp_zhuangdui_XMLǃn1_INǃTEXT
		getChild(name: 'n1_JP'): otherǁhelpǁhelp_zhuangdui_XMLǃn1_JPǃTEXT
		getChildAt(index: 4): otherǁhelpǁhelp_zhuangdui_XMLǃn1_JPǃTEXT
		getChildById(id: 'n1_ww6c_JP'): otherǁhelpǁhelp_zhuangdui_XMLǃn1_JPǃTEXT
		getChild(name: 'n1_KR'): otherǁhelpǁhelp_zhuangdui_XMLǃn1_KRǃTEXT
		getChildAt(index: 5): otherǁhelpǁhelp_zhuangdui_XMLǃn1_KRǃTEXT
		getChildById(id: 'n1_ww6c_KR'): otherǁhelpǁhelp_zhuangdui_XMLǃn1_KRǃTEXT
		getChild(name: 'n1_TH'): otherǁhelpǁhelp_zhuangdui_XMLǃn1_THǃTEXT
		getChildAt(index: 6): otherǁhelpǁhelp_zhuangdui_XMLǃn1_THǃTEXT
		getChildById(id: 'n1_ww6c_TH'): otherǁhelpǁhelp_zhuangdui_XMLǃn1_THǃTEXT
		getChild(name: 'n1_VN'): otherǁhelpǁhelp_zhuangdui_XMLǃn1_VNǃTEXT
		getChildAt(index: 7): otherǁhelpǁhelp_zhuangdui_XMLǃn1_VNǃTEXT
		getChildById(id: 'n1_ww6c_VN'): otherǁhelpǁhelp_zhuangdui_XMLǃn1_VNǃTEXT
		getChild(name: 'n1'): otherǁhelpǁhelp_zhuangdui_XMLǃn1ǃTEXT
		getChildAt(index: 8): otherǁhelpǁhelp_zhuangdui_XMLǃn1ǃTEXT
		getChildById(id: 'n1_ww6c'): otherǁhelpǁhelp_zhuangdui_XMLǃn1ǃTEXT
		_children: [
			otherǁhelpǁhelp_zhuangdui_XMLǃn0ǃIMAGE,
			otherǁhelpǁhelp_zhuangdui_XMLǃn1_CN2ǃTEXT,
			otherǁhelpǁhelp_zhuangdui_XMLǃn1_ENǃTEXT,
			otherǁhelpǁhelp_zhuangdui_XMLǃn1_INǃTEXT,
			otherǁhelpǁhelp_zhuangdui_XMLǃn1_JPǃTEXT,
			otherǁhelpǁhelp_zhuangdui_XMLǃn1_KRǃTEXT,
			otherǁhelpǁhelp_zhuangdui_XMLǃn1_THǃTEXT,
			otherǁhelpǁhelp_zhuangdui_XMLǃn1_VNǃTEXT,
			otherǁhelpǁhelp_zhuangdui_XMLǃn1ǃTEXT
		]
		getController(name: '__language'): otherǁhelpǁhelp_zhuangdui_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): otherǁhelpǁhelp_zhuangdui_XMLǃ__languageǃCONTROLLER
		_controllers: [
			otherǁhelpǁhelp_zhuangdui_XMLǃ__languageǃCONTROLLER
		]
	}
	interface otherǁhelpǁhelp_zhuangdui_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: otherǁhelpǁhelp_zhuangdui_XML
	}
	interface otherǁhelpǁhelp_zhuangdui_XMLǃn0ǃIMAGE extends fairygui.GImage{
		parent: otherǁhelpǁhelp_zhuangdui_XML
	}
	interface otherǁhelpǁhelp_zhuangdui_XMLǃn1_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_zhuangdui_XML
	}
	interface otherǁhelpǁhelp_zhuangdui_XMLǃn1_ENǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_zhuangdui_XML
	}
	interface otherǁhelpǁhelp_zhuangdui_XMLǃn1_INǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_zhuangdui_XML
	}
	interface otherǁhelpǁhelp_zhuangdui_XMLǃn1_JPǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_zhuangdui_XML
	}
	interface otherǁhelpǁhelp_zhuangdui_XMLǃn1_KRǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_zhuangdui_XML
	}
	interface otherǁhelpǁhelp_zhuangdui_XMLǃn1_THǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_zhuangdui_XML
	}
	interface otherǁhelpǁhelp_zhuangdui_XMLǃn1_VNǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_zhuangdui_XML
	}
	interface otherǁhelpǁhelp_zhuangdui_XMLǃn1ǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_zhuangdui_XML
	}
	interface otherǁhelpǁhelp_zhuangdui_XMLǃhelp_5ǃCOMPONENT extends otherǁhelpǁhelp_zhuangdui_XML{
		parent: Main_XML
	}
	interface otherǁhelpǁhelp_wanmeiduizi_XML extends fairygui.GComponent{
		getChild(name: 'n0'): otherǁhelpǁhelp_wanmeiduizi_XMLǃn0ǃIMAGE
		getChildAt(index: 0): otherǁhelpǁhelp_wanmeiduizi_XMLǃn0ǃIMAGE
		getChildById(id: 'n0_ww6c'): otherǁhelpǁhelp_wanmeiduizi_XMLǃn0ǃIMAGE
		getChild(name: 'n1_CN2'): otherǁhelpǁhelp_wanmeiduizi_XMLǃn1_CN2ǃTEXT
		getChildAt(index: 1): otherǁhelpǁhelp_wanmeiduizi_XMLǃn1_CN2ǃTEXT
		getChildById(id: 'n1_ww6c_CN2'): otherǁhelpǁhelp_wanmeiduizi_XMLǃn1_CN2ǃTEXT
		getChild(name: 'n1_EN'): otherǁhelpǁhelp_wanmeiduizi_XMLǃn1_ENǃTEXT
		getChildAt(index: 2): otherǁhelpǁhelp_wanmeiduizi_XMLǃn1_ENǃTEXT
		getChildById(id: 'n1_ww6c_EN'): otherǁhelpǁhelp_wanmeiduizi_XMLǃn1_ENǃTEXT
		getChild(name: 'n1_IN'): otherǁhelpǁhelp_wanmeiduizi_XMLǃn1_INǃTEXT
		getChildAt(index: 3): otherǁhelpǁhelp_wanmeiduizi_XMLǃn1_INǃTEXT
		getChildById(id: 'n1_ww6c_IN'): otherǁhelpǁhelp_wanmeiduizi_XMLǃn1_INǃTEXT
		getChild(name: 'n1_JP'): otherǁhelpǁhelp_wanmeiduizi_XMLǃn1_JPǃTEXT
		getChildAt(index: 4): otherǁhelpǁhelp_wanmeiduizi_XMLǃn1_JPǃTEXT
		getChildById(id: 'n1_ww6c_JP'): otherǁhelpǁhelp_wanmeiduizi_XMLǃn1_JPǃTEXT
		getChild(name: 'n1_KR'): otherǁhelpǁhelp_wanmeiduizi_XMLǃn1_KRǃTEXT
		getChildAt(index: 5): otherǁhelpǁhelp_wanmeiduizi_XMLǃn1_KRǃTEXT
		getChildById(id: 'n1_ww6c_KR'): otherǁhelpǁhelp_wanmeiduizi_XMLǃn1_KRǃTEXT
		getChild(name: 'n1_TH'): otherǁhelpǁhelp_wanmeiduizi_XMLǃn1_THǃTEXT
		getChildAt(index: 6): otherǁhelpǁhelp_wanmeiduizi_XMLǃn1_THǃTEXT
		getChildById(id: 'n1_ww6c_TH'): otherǁhelpǁhelp_wanmeiduizi_XMLǃn1_THǃTEXT
		getChild(name: 'n1_VN'): otherǁhelpǁhelp_wanmeiduizi_XMLǃn1_VNǃTEXT
		getChildAt(index: 7): otherǁhelpǁhelp_wanmeiduizi_XMLǃn1_VNǃTEXT
		getChildById(id: 'n1_ww6c_VN'): otherǁhelpǁhelp_wanmeiduizi_XMLǃn1_VNǃTEXT
		getChild(name: 'n1'): otherǁhelpǁhelp_wanmeiduizi_XMLǃn1ǃTEXT
		getChildAt(index: 8): otherǁhelpǁhelp_wanmeiduizi_XMLǃn1ǃTEXT
		getChildById(id: 'n1_ww6c'): otherǁhelpǁhelp_wanmeiduizi_XMLǃn1ǃTEXT
		_children: [
			otherǁhelpǁhelp_wanmeiduizi_XMLǃn0ǃIMAGE,
			otherǁhelpǁhelp_wanmeiduizi_XMLǃn1_CN2ǃTEXT,
			otherǁhelpǁhelp_wanmeiduizi_XMLǃn1_ENǃTEXT,
			otherǁhelpǁhelp_wanmeiduizi_XMLǃn1_INǃTEXT,
			otherǁhelpǁhelp_wanmeiduizi_XMLǃn1_JPǃTEXT,
			otherǁhelpǁhelp_wanmeiduizi_XMLǃn1_KRǃTEXT,
			otherǁhelpǁhelp_wanmeiduizi_XMLǃn1_THǃTEXT,
			otherǁhelpǁhelp_wanmeiduizi_XMLǃn1_VNǃTEXT,
			otherǁhelpǁhelp_wanmeiduizi_XMLǃn1ǃTEXT
		]
		getController(name: '__language'): otherǁhelpǁhelp_wanmeiduizi_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): otherǁhelpǁhelp_wanmeiduizi_XMLǃ__languageǃCONTROLLER
		_controllers: [
			otherǁhelpǁhelp_wanmeiduizi_XMLǃ__languageǃCONTROLLER
		]
	}
	interface otherǁhelpǁhelp_wanmeiduizi_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: otherǁhelpǁhelp_wanmeiduizi_XML
	}
	interface otherǁhelpǁhelp_wanmeiduizi_XMLǃn0ǃIMAGE extends fairygui.GImage{
		parent: otherǁhelpǁhelp_wanmeiduizi_XML
	}
	interface otherǁhelpǁhelp_wanmeiduizi_XMLǃn1_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_wanmeiduizi_XML
	}
	interface otherǁhelpǁhelp_wanmeiduizi_XMLǃn1_ENǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_wanmeiduizi_XML
	}
	interface otherǁhelpǁhelp_wanmeiduizi_XMLǃn1_INǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_wanmeiduizi_XML
	}
	interface otherǁhelpǁhelp_wanmeiduizi_XMLǃn1_JPǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_wanmeiduizi_XML
	}
	interface otherǁhelpǁhelp_wanmeiduizi_XMLǃn1_KRǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_wanmeiduizi_XML
	}
	interface otherǁhelpǁhelp_wanmeiduizi_XMLǃn1_THǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_wanmeiduizi_XML
	}
	interface otherǁhelpǁhelp_wanmeiduizi_XMLǃn1_VNǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_wanmeiduizi_XML
	}
	interface otherǁhelpǁhelp_wanmeiduizi_XMLǃn1ǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁhelpǁhelp_wanmeiduizi_XML
	}
	interface otherǁhelpǁhelp_wanmeiduizi_XMLǃhelp_6ǃCOMPONENT extends otherǁhelpǁhelp_wanmeiduizi_XML{
		parent: Main_XML
	}
	interface buttonsǁgongnengǁbutton_zidongtouzhu_XML extends fairygui.GButton{
		getChild(name: 'n1'): buttonsǁgongnengǁbutton_zidongtouzhu_XMLǃn1ǃIMAGE
		getChildAt(index: 0): buttonsǁgongnengǁbutton_zidongtouzhu_XMLǃn1ǃIMAGE
		getChildById(id: 'n2'): buttonsǁgongnengǁbutton_zidongtouzhu_XMLǃn1ǃIMAGE
		getChild(name: 'n2'): buttonsǁgongnengǁbutton_zidongtouzhu_XMLǃn2ǃIMAGE
		getChildAt(index: 1): buttonsǁgongnengǁbutton_zidongtouzhu_XMLǃn2ǃIMAGE
		getChildById(id: 'n1'): buttonsǁgongnengǁbutton_zidongtouzhu_XMLǃn2ǃIMAGE
		_children: [
			buttonsǁgongnengǁbutton_zidongtouzhu_XMLǃn1ǃIMAGE,
			buttonsǁgongnengǁbutton_zidongtouzhu_XMLǃn2ǃIMAGE
		]
		getController(name: '__language'): buttonsǁgongnengǁbutton_zidongtouzhu_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁgongnengǁbutton_zidongtouzhu_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁgongnengǁbutton_zidongtouzhu_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁgongnengǁbutton_zidongtouzhu_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁgongnengǁbutton_zidongtouzhu_XMLǃ__languageǃCONTROLLER,
			buttonsǁgongnengǁbutton_zidongtouzhu_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁgongnengǁbutton_zidongtouzhu_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁgongnengǁbutton_zidongtouzhu_XML
	}
	interface buttonsǁgongnengǁbutton_zidongtouzhu_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁgongnengǁbutton_zidongtouzhu_XML
	}
	interface buttonsǁgongnengǁbutton_zidongtouzhu_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁgongnengǁbutton_zidongtouzhu_XML
	}
	interface buttonsǁgongnengǁbutton_zidongtouzhu_XMLǃn2ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁgongnengǁbutton_zidongtouzhu_XML
	}
	interface buttonsǁgongnengǁbutton_zidongtouzhu_XMLǃbutton_zidongtouzhuǃCOMPONENT extends buttonsǁgongnengǁbutton_zidongtouzhu_XML{
		parent: Main_XML
	}
	interface buttonsǁgongnengǁbutton_return_XML extends fairygui.GButton{
		getChild(name: 'n1'): buttonsǁgongnengǁbutton_return_XMLǃn1ǃIMAGE
		getChildAt(index: 0): buttonsǁgongnengǁbutton_return_XMLǃn1ǃIMAGE
		getChildById(id: 'n1'): buttonsǁgongnengǁbutton_return_XMLǃn1ǃIMAGE
		_children: [
			buttonsǁgongnengǁbutton_return_XMLǃn1ǃIMAGE
		]
		getController(name: '__language'): buttonsǁgongnengǁbutton_return_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁgongnengǁbutton_return_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁgongnengǁbutton_return_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁgongnengǁbutton_return_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁgongnengǁbutton_return_XMLǃ__languageǃCONTROLLER,
			buttonsǁgongnengǁbutton_return_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁgongnengǁbutton_return_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁgongnengǁbutton_return_XML
	}
	interface buttonsǁgongnengǁbutton_return_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁgongnengǁbutton_return_XML
	}
	interface buttonsǁgongnengǁbutton_return_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁgongnengǁbutton_return_XML
	}
	interface buttonsǁgongnengǁbutton_return_XMLǃbutton_returnǃCOMPONENT extends buttonsǁgongnengǁbutton_return_XML{
		parent: Main_XML
	}
	interface otherǁreward_text_ani_XML extends fairygui.GComponent{
		getChild(name: 'tf_reward'): otherǁreward_text_ani_XMLǃtf_rewardǃTEXT
		getChildAt(index: 0): otherǁreward_text_ani_XMLǃtf_rewardǃTEXT
		getChildById(id: 'n5_s6qw'): otherǁreward_text_ani_XMLǃtf_rewardǃTEXT
		_children: [
			otherǁreward_text_ani_XMLǃtf_rewardǃTEXT
		]
		getTransition(name: 't0'): otherǁreward_text_ani_XMLǃt0ǃTRANSITION
		getTransitionAt(index: 0): otherǁreward_text_ani_XMLǃt0ǃTRANSITION
		_transitions: [
			otherǁreward_text_ani_XMLǃt0ǃTRANSITION
		]
		getController(name: '__language'): otherǁreward_text_ani_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): otherǁreward_text_ani_XMLǃ__languageǃCONTROLLER
		_controllers: [
			otherǁreward_text_ani_XMLǃ__languageǃCONTROLLER
		]
	}
	interface otherǁreward_text_ani_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: otherǁreward_text_ani_XML
	}
	interface otherǁreward_text_ani_XMLǃtf_rewardǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁreward_text_ani_XML
	}
	interface otherǁreward_text_ani_XMLǃt0ǃTRANSITION extends fairygui.Transition{
		_owner: otherǁreward_text_ani_XML
	}
	interface otherǁreward_text_ani_XMLǃreward_text_aniǃCOMPONENT extends otherǁreward_text_ani_XML{
		parent: Main_XML
	}
	interface buttonsǁluzhiǁdaxiaolu_XML extends fairygui.GButton{
		getChild(name: 'n3'): buttonsǁluzhiǁdaxiaolu_XMLǃn3ǃIMAGE
		getChildAt(index: 0): buttonsǁluzhiǁdaxiaolu_XMLǃn3ǃIMAGE
		getChildById(id: 'n3_xls7'): buttonsǁluzhiǁdaxiaolu_XMLǃn3ǃIMAGE
		getChild(name: 'n4'): buttonsǁluzhiǁdaxiaolu_XMLǃn4ǃIMAGE
		getChildAt(index: 1): buttonsǁluzhiǁdaxiaolu_XMLǃn4ǃIMAGE
		getChildById(id: 'n4_xls7'): buttonsǁluzhiǁdaxiaolu_XMLǃn4ǃIMAGE
		getChild(name: 'n5_CN2'): buttonsǁluzhiǁdaxiaolu_XMLǃn5_CN2ǃTEXT
		getChildAt(index: 2): buttonsǁluzhiǁdaxiaolu_XMLǃn5_CN2ǃTEXT
		getChildById(id: 'n5_xls7_CN2'): buttonsǁluzhiǁdaxiaolu_XMLǃn5_CN2ǃTEXT
		getChild(name: 'n5_EN'): buttonsǁluzhiǁdaxiaolu_XMLǃn5_ENǃTEXT
		getChildAt(index: 3): buttonsǁluzhiǁdaxiaolu_XMLǃn5_ENǃTEXT
		getChildById(id: 'n5_xls7_EN'): buttonsǁluzhiǁdaxiaolu_XMLǃn5_ENǃTEXT
		getChild(name: 'n5_IN'): buttonsǁluzhiǁdaxiaolu_XMLǃn5_INǃTEXT
		getChildAt(index: 4): buttonsǁluzhiǁdaxiaolu_XMLǃn5_INǃTEXT
		getChildById(id: 'n5_xls7_IN'): buttonsǁluzhiǁdaxiaolu_XMLǃn5_INǃTEXT
		getChild(name: 'n5_JP'): buttonsǁluzhiǁdaxiaolu_XMLǃn5_JPǃTEXT
		getChildAt(index: 5): buttonsǁluzhiǁdaxiaolu_XMLǃn5_JPǃTEXT
		getChildById(id: 'n5_xls7_JP'): buttonsǁluzhiǁdaxiaolu_XMLǃn5_JPǃTEXT
		getChild(name: 'n5_KR'): buttonsǁluzhiǁdaxiaolu_XMLǃn5_KRǃTEXT
		getChildAt(index: 6): buttonsǁluzhiǁdaxiaolu_XMLǃn5_KRǃTEXT
		getChildById(id: 'n5_xls7_KR'): buttonsǁluzhiǁdaxiaolu_XMLǃn5_KRǃTEXT
		getChild(name: 'n5_TH'): buttonsǁluzhiǁdaxiaolu_XMLǃn5_THǃTEXT
		getChildAt(index: 7): buttonsǁluzhiǁdaxiaolu_XMLǃn5_THǃTEXT
		getChildById(id: 'n5_xls7_TH'): buttonsǁluzhiǁdaxiaolu_XMLǃn5_THǃTEXT
		getChild(name: 'n5_VN'): buttonsǁluzhiǁdaxiaolu_XMLǃn5_VNǃTEXT
		getChildAt(index: 8): buttonsǁluzhiǁdaxiaolu_XMLǃn5_VNǃTEXT
		getChildById(id: 'n5_xls7_VN'): buttonsǁluzhiǁdaxiaolu_XMLǃn5_VNǃTEXT
		getChild(name: 'n5'): buttonsǁluzhiǁdaxiaolu_XMLǃn5ǃTEXT
		getChildAt(index: 9): buttonsǁluzhiǁdaxiaolu_XMLǃn5ǃTEXT
		getChildById(id: 'n5_xls7'): buttonsǁluzhiǁdaxiaolu_XMLǃn5ǃTEXT
		_children: [
			buttonsǁluzhiǁdaxiaolu_XMLǃn3ǃIMAGE,
			buttonsǁluzhiǁdaxiaolu_XMLǃn4ǃIMAGE,
			buttonsǁluzhiǁdaxiaolu_XMLǃn5_CN2ǃTEXT,
			buttonsǁluzhiǁdaxiaolu_XMLǃn5_ENǃTEXT,
			buttonsǁluzhiǁdaxiaolu_XMLǃn5_INǃTEXT,
			buttonsǁluzhiǁdaxiaolu_XMLǃn5_JPǃTEXT,
			buttonsǁluzhiǁdaxiaolu_XMLǃn5_KRǃTEXT,
			buttonsǁluzhiǁdaxiaolu_XMLǃn5_THǃTEXT,
			buttonsǁluzhiǁdaxiaolu_XMLǃn5_VNǃTEXT,
			buttonsǁluzhiǁdaxiaolu_XMLǃn5ǃTEXT
		]
		getController(name: '__language'): buttonsǁluzhiǁdaxiaolu_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁluzhiǁdaxiaolu_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁluzhiǁdaxiaolu_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁluzhiǁdaxiaolu_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁluzhiǁdaxiaolu_XMLǃ__languageǃCONTROLLER,
			buttonsǁluzhiǁdaxiaolu_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁluzhiǁdaxiaolu_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁluzhiǁdaxiaolu_XML
	}
	interface buttonsǁluzhiǁdaxiaolu_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁluzhiǁdaxiaolu_XML
	}
	interface buttonsǁluzhiǁdaxiaolu_XMLǃn3ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁluzhiǁdaxiaolu_XML
	}
	interface buttonsǁluzhiǁdaxiaolu_XMLǃn4ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁluzhiǁdaxiaolu_XML
	}
	interface buttonsǁluzhiǁdaxiaolu_XMLǃn5_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁluzhiǁdaxiaolu_XML
	}
	interface buttonsǁluzhiǁdaxiaolu_XMLǃn5_ENǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁluzhiǁdaxiaolu_XML
	}
	interface buttonsǁluzhiǁdaxiaolu_XMLǃn5_INǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁluzhiǁdaxiaolu_XML
	}
	interface buttonsǁluzhiǁdaxiaolu_XMLǃn5_JPǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁluzhiǁdaxiaolu_XML
	}
	interface buttonsǁluzhiǁdaxiaolu_XMLǃn5_KRǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁluzhiǁdaxiaolu_XML
	}
	interface buttonsǁluzhiǁdaxiaolu_XMLǃn5_THǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁluzhiǁdaxiaolu_XML
	}
	interface buttonsǁluzhiǁdaxiaolu_XMLǃn5_VNǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁluzhiǁdaxiaolu_XML
	}
	interface buttonsǁluzhiǁdaxiaolu_XMLǃn5ǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁluzhiǁdaxiaolu_XML
	}
	interface buttonsǁluzhiǁdaxiaolu_XMLǃbutton_daxiaoluǃCOMPONENT extends buttonsǁluzhiǁdaxiaolu_XML{
		parent: Main_XML
	}
	interface buttonsǁluzhiǁzhugulu_XML extends fairygui.GButton{
		getChild(name: 'n3'): buttonsǁluzhiǁzhugulu_XMLǃn3ǃIMAGE
		getChildAt(index: 0): buttonsǁluzhiǁzhugulu_XMLǃn3ǃIMAGE
		getChildById(id: 'n3_xls7'): buttonsǁluzhiǁzhugulu_XMLǃn3ǃIMAGE
		getChild(name: 'n4'): buttonsǁluzhiǁzhugulu_XMLǃn4ǃIMAGE
		getChildAt(index: 1): buttonsǁluzhiǁzhugulu_XMLǃn4ǃIMAGE
		getChildById(id: 'n4_xls7'): buttonsǁluzhiǁzhugulu_XMLǃn4ǃIMAGE
		getChild(name: 'n5_CN2'): buttonsǁluzhiǁzhugulu_XMLǃn5_CN2ǃTEXT
		getChildAt(index: 2): buttonsǁluzhiǁzhugulu_XMLǃn5_CN2ǃTEXT
		getChildById(id: 'n5_xls7_CN2'): buttonsǁluzhiǁzhugulu_XMLǃn5_CN2ǃTEXT
		getChild(name: 'n5_EN'): buttonsǁluzhiǁzhugulu_XMLǃn5_ENǃTEXT
		getChildAt(index: 3): buttonsǁluzhiǁzhugulu_XMLǃn5_ENǃTEXT
		getChildById(id: 'n5_xls7_EN'): buttonsǁluzhiǁzhugulu_XMLǃn5_ENǃTEXT
		getChild(name: 'n5_IN'): buttonsǁluzhiǁzhugulu_XMLǃn5_INǃTEXT
		getChildAt(index: 4): buttonsǁluzhiǁzhugulu_XMLǃn5_INǃTEXT
		getChildById(id: 'n5_xls7_IN'): buttonsǁluzhiǁzhugulu_XMLǃn5_INǃTEXT
		getChild(name: 'n5_JP'): buttonsǁluzhiǁzhugulu_XMLǃn5_JPǃTEXT
		getChildAt(index: 5): buttonsǁluzhiǁzhugulu_XMLǃn5_JPǃTEXT
		getChildById(id: 'n5_xls7_JP'): buttonsǁluzhiǁzhugulu_XMLǃn5_JPǃTEXT
		getChild(name: 'n5_KR'): buttonsǁluzhiǁzhugulu_XMLǃn5_KRǃTEXT
		getChildAt(index: 6): buttonsǁluzhiǁzhugulu_XMLǃn5_KRǃTEXT
		getChildById(id: 'n5_xls7_KR'): buttonsǁluzhiǁzhugulu_XMLǃn5_KRǃTEXT
		getChild(name: 'n5_TH'): buttonsǁluzhiǁzhugulu_XMLǃn5_THǃTEXT
		getChildAt(index: 7): buttonsǁluzhiǁzhugulu_XMLǃn5_THǃTEXT
		getChildById(id: 'n5_xls7_TH'): buttonsǁluzhiǁzhugulu_XMLǃn5_THǃTEXT
		getChild(name: 'n5_VN'): buttonsǁluzhiǁzhugulu_XMLǃn5_VNǃTEXT
		getChildAt(index: 8): buttonsǁluzhiǁzhugulu_XMLǃn5_VNǃTEXT
		getChildById(id: 'n5_xls7_VN'): buttonsǁluzhiǁzhugulu_XMLǃn5_VNǃTEXT
		getChild(name: 'n5'): buttonsǁluzhiǁzhugulu_XMLǃn5ǃTEXT
		getChildAt(index: 9): buttonsǁluzhiǁzhugulu_XMLǃn5ǃTEXT
		getChildById(id: 'n5_xls7'): buttonsǁluzhiǁzhugulu_XMLǃn5ǃTEXT
		_children: [
			buttonsǁluzhiǁzhugulu_XMLǃn3ǃIMAGE,
			buttonsǁluzhiǁzhugulu_XMLǃn4ǃIMAGE,
			buttonsǁluzhiǁzhugulu_XMLǃn5_CN2ǃTEXT,
			buttonsǁluzhiǁzhugulu_XMLǃn5_ENǃTEXT,
			buttonsǁluzhiǁzhugulu_XMLǃn5_INǃTEXT,
			buttonsǁluzhiǁzhugulu_XMLǃn5_JPǃTEXT,
			buttonsǁluzhiǁzhugulu_XMLǃn5_KRǃTEXT,
			buttonsǁluzhiǁzhugulu_XMLǃn5_THǃTEXT,
			buttonsǁluzhiǁzhugulu_XMLǃn5_VNǃTEXT,
			buttonsǁluzhiǁzhugulu_XMLǃn5ǃTEXT
		]
		getController(name: '__language'): buttonsǁluzhiǁzhugulu_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁluzhiǁzhugulu_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁluzhiǁzhugulu_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁluzhiǁzhugulu_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁluzhiǁzhugulu_XMLǃ__languageǃCONTROLLER,
			buttonsǁluzhiǁzhugulu_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁluzhiǁzhugulu_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁluzhiǁzhugulu_XML
	}
	interface buttonsǁluzhiǁzhugulu_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁluzhiǁzhugulu_XML
	}
	interface buttonsǁluzhiǁzhugulu_XMLǃn3ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁluzhiǁzhugulu_XML
	}
	interface buttonsǁluzhiǁzhugulu_XMLǃn4ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁluzhiǁzhugulu_XML
	}
	interface buttonsǁluzhiǁzhugulu_XMLǃn5_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁluzhiǁzhugulu_XML
	}
	interface buttonsǁluzhiǁzhugulu_XMLǃn5_ENǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁluzhiǁzhugulu_XML
	}
	interface buttonsǁluzhiǁzhugulu_XMLǃn5_INǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁluzhiǁzhugulu_XML
	}
	interface buttonsǁluzhiǁzhugulu_XMLǃn5_JPǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁluzhiǁzhugulu_XML
	}
	interface buttonsǁluzhiǁzhugulu_XMLǃn5_KRǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁluzhiǁzhugulu_XML
	}
	interface buttonsǁluzhiǁzhugulu_XMLǃn5_THǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁluzhiǁzhugulu_XML
	}
	interface buttonsǁluzhiǁzhugulu_XMLǃn5_VNǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁluzhiǁzhugulu_XML
	}
	interface buttonsǁluzhiǁzhugulu_XMLǃn5ǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁluzhiǁzhugulu_XML
	}
	interface buttonsǁluzhiǁzhugulu_XMLǃbutton_zhuguluǃCOMPONENT extends buttonsǁluzhiǁzhugulu_XML{
		parent: Main_XML
	}
	interface Main_XMLǃdaojishi_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: Main_XML
	}
	interface Main_XMLǃdaojishi_ENǃTEXT extends fairygui.GBasicTextField{
		parent: Main_XML
	}
	interface Main_XMLǃdaojishi_INǃTEXT extends fairygui.GBasicTextField{
		parent: Main_XML
	}
	interface Main_XMLǃdaojishi_JPǃTEXT extends fairygui.GBasicTextField{
		parent: Main_XML
	}
	interface Main_XMLǃdaojishi_KRǃTEXT extends fairygui.GBasicTextField{
		parent: Main_XML
	}
	interface Main_XMLǃdaojishi_THǃTEXT extends fairygui.GBasicTextField{
		parent: Main_XML
	}
	interface Main_XMLǃdaojishi_VNǃTEXT extends fairygui.GBasicTextField{
		parent: Main_XML
	}
	interface Main_XMLǃdaojishiǃTEXT extends fairygui.GBasicTextField{
		parent: Main_XML
	}
	interface Main_XMLǃtext_xiandianshuǃTEXT extends fairygui.GBasicTextField{
		parent: Main_XML
	}
	interface Main_XMLǃtext_zhuangdianshuǃTEXT extends fairygui.GBasicTextField{
		parent: Main_XML
	}
	interface otherǁdanmu_XML extends fairygui.GComponent{
		getChild(name: 'n0'): otherǁdanmu_XMLǃn0ǃIMAGE
		getChildAt(index: 0): otherǁdanmu_XMLǃn0ǃIMAGE
		getChildById(id: 'n0_j4zf'): otherǁdanmu_XMLǃn0ǃIMAGE
		getChild(name: 'text_00'): otherǁdanmu_item_XMLǃtext_00ǃCOMPONENT
		getChildAt(index: 1): otherǁdanmu_item_XMLǃtext_00ǃCOMPONENT
		getChildById(id: 'n1_eiw7'): otherǁdanmu_item_XMLǃtext_00ǃCOMPONENT
		getChild(name: 'text_01'): otherǁdanmu_item_XMLǃtext_01ǃCOMPONENT
		getChildAt(index: 2): otherǁdanmu_item_XMLǃtext_01ǃCOMPONENT
		getChildById(id: 'n2_eiw7'): otherǁdanmu_item_XMLǃtext_01ǃCOMPONENT
		getChild(name: 'text_02'): otherǁdanmu_item_XMLǃtext_02ǃCOMPONENT
		getChildAt(index: 3): otherǁdanmu_item_XMLǃtext_02ǃCOMPONENT
		getChildById(id: 'n3_eiw7'): otherǁdanmu_item_XMLǃtext_02ǃCOMPONENT
		_children: [
			otherǁdanmu_XMLǃn0ǃIMAGE,
			otherǁdanmu_item_XMLǃtext_00ǃCOMPONENT,
			otherǁdanmu_item_XMLǃtext_01ǃCOMPONENT,
			otherǁdanmu_item_XMLǃtext_02ǃCOMPONENT
		]
		getController(name: '__language'): otherǁdanmu_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): otherǁdanmu_XMLǃ__languageǃCONTROLLER
		_controllers: [
			otherǁdanmu_XMLǃ__languageǃCONTROLLER
		]
	}
	interface otherǁdanmu_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: otherǁdanmu_XML
	}
	interface otherǁdanmu_XMLǃn0ǃIMAGE extends fairygui.GImage{
		parent: otherǁdanmu_XML
	}
	interface otherǁdanmu_item_XML extends fairygui.GComponent{
		getController(name: '__language'): otherǁdanmu_item_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): otherǁdanmu_item_XMLǃ__languageǃCONTROLLER
		_controllers: [
			otherǁdanmu_item_XMLǃ__languageǃCONTROLLER
		]
	}
	interface otherǁdanmu_item_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: otherǁdanmu_item_XML
	}
	interface otherǁdanmu_item_XMLǃtext_00ǃCOMPONENT extends otherǁdanmu_item_XML{
		parent: otherǁdanmu_XML
	}
	interface otherǁdanmu_item_XMLǃtext_01ǃCOMPONENT extends otherǁdanmu_item_XML{
		parent: otherǁdanmu_XML
	}
	interface otherǁdanmu_item_XMLǃtext_02ǃCOMPONENT extends otherǁdanmu_item_XML{
		parent: otherǁdanmu_XML
	}
	interface otherǁdanmu_XMLǃdanmuǃCOMPONENT extends otherǁdanmu_XML{
		parent: Main_XML
	}
	interface liaotianshi_XML extends fairygui.GComponent{
		getChild(name: 'n126'): liaotianshi_XMLǃn126ǃIMAGE
		getChildAt(index: 0): liaotianshi_XMLǃn126ǃIMAGE
		getChildById(id: 'n126_fxoc'): liaotianshi_XMLǃn126ǃIMAGE
		getChild(name: 'button_hide'): buttonsǁbtn_hide_XMLǃbutton_hideǃCOMPONENT
		getChildAt(index: 1): buttonsǁbtn_hide_XMLǃbutton_hideǃCOMPONENT
		getChildById(id: 'n123_xls7'): buttonsǁbtn_hide_XMLǃbutton_hideǃCOMPONENT
		getChild(name: 'button_fenxiang'): buttonsǁbutton_danmu_XMLǃbutton_fenxiangǃCOMPONENT
		getChildAt(index: 2): buttonsǁbutton_danmu_XMLǃbutton_fenxiangǃCOMPONENT
		getChildById(id: 'n125_xls7'): buttonsǁbutton_danmu_XMLǃbutton_fenxiangǃCOMPONENT
		getChild(name: 'content'): liaotianneirong_XMLǃcontentǃCOMPONENT
		getChildAt(index: 3): liaotianneirong_XMLǃcontentǃCOMPONENT
		getChildById(id: 'n110_q4bw'): liaotianneirong_XMLǃcontentǃCOMPONENT
		getChild(name: 'button_fasong'): buttonsǁliaotianshiǁbutton_fasong_XMLǃbutton_fasongǃCOMPONENT
		getChildAt(index: 4): buttonsǁliaotianshiǁbutton_fasong_XMLǃbutton_fasongǃCOMPONENT
		getChildById(id: 'n111_q4bw'): buttonsǁliaotianshiǁbutton_fasong_XMLǃbutton_fasongǃCOMPONENT
		getChild(name: 'input'): liaotianshi_XMLǃinputǃTEXT
		getChildAt(index: 5): liaotianshi_XMLǃinputǃTEXT
		getChildById(id: 'n113_q4bw'): liaotianshi_XMLǃinputǃTEXT
		getChild(name: 'phrase'): phrase_XMLǃphraseǃCOMPONENT
		getChildAt(index: 6): phrase_XMLǃphraseǃCOMPONENT
		getChildById(id: 'n124_xls7'): phrase_XMLǃphraseǃCOMPONENT
		getChild(name: 'zhanghuyue'): liaotianshi_XMLǃzhanghuyueǃTEXT
		getChildAt(index: 7): liaotianshi_XMLǃzhanghuyueǃTEXT
		getChildById(id: 'n114_q4bw'): liaotianshi_XMLǃzhanghuyueǃTEXT
		getChild(name: 'benjuxiazhu'): liaotianshi_XMLǃbenjuxiazhuǃTEXT
		getChildAt(index: 8): liaotianshi_XMLǃbenjuxiazhuǃTEXT
		getChildById(id: 'n115_q4bw'): liaotianshi_XMLǃbenjuxiazhuǃTEXT
		getChild(name: 'zongyingkui'): liaotianshi_XMLǃzongyingkuiǃTEXT
		getChildAt(index: 9): liaotianshi_XMLǃzongyingkuiǃTEXT
		getChildById(id: 'n116_h3lk'): liaotianshi_XMLǃzongyingkuiǃTEXT
		getChild(name: 'n118_CN2'): liaotianshi_XMLǃn118_CN2ǃTEXT
		getChildAt(index: 10): liaotianshi_XMLǃn118_CN2ǃTEXT
		getChildById(id: 'n118_xls7_CN2'): liaotianshi_XMLǃn118_CN2ǃTEXT
		getChild(name: 'n118_EN'): liaotianshi_XMLǃn118_ENǃTEXT
		getChildAt(index: 11): liaotianshi_XMLǃn118_ENǃTEXT
		getChildById(id: 'n118_xls7_EN'): liaotianshi_XMLǃn118_ENǃTEXT
		getChild(name: 'n118_IN'): liaotianshi_XMLǃn118_INǃTEXT
		getChildAt(index: 12): liaotianshi_XMLǃn118_INǃTEXT
		getChildById(id: 'n118_xls7_IN'): liaotianshi_XMLǃn118_INǃTEXT
		getChild(name: 'n118_JP'): liaotianshi_XMLǃn118_JPǃTEXT
		getChildAt(index: 13): liaotianshi_XMLǃn118_JPǃTEXT
		getChildById(id: 'n118_xls7_JP'): liaotianshi_XMLǃn118_JPǃTEXT
		getChild(name: 'n118_KR'): liaotianshi_XMLǃn118_KRǃTEXT
		getChildAt(index: 14): liaotianshi_XMLǃn118_KRǃTEXT
		getChildById(id: 'n118_xls7_KR'): liaotianshi_XMLǃn118_KRǃTEXT
		getChild(name: 'n118_TH'): liaotianshi_XMLǃn118_THǃTEXT
		getChildAt(index: 15): liaotianshi_XMLǃn118_THǃTEXT
		getChildById(id: 'n118_xls7_TH'): liaotianshi_XMLǃn118_THǃTEXT
		getChild(name: 'n118_VN'): liaotianshi_XMLǃn118_VNǃTEXT
		getChildAt(index: 16): liaotianshi_XMLǃn118_VNǃTEXT
		getChildById(id: 'n118_xls7_VN'): liaotianshi_XMLǃn118_VNǃTEXT
		getChild(name: 'n118'): liaotianshi_XMLǃn118ǃTEXT
		getChildAt(index: 17): liaotianshi_XMLǃn118ǃTEXT
		getChildById(id: 'n118_xls7'): liaotianshi_XMLǃn118ǃTEXT
		getChild(name: 'button_quanbu'): buttonsǁliaotianshiǁbutton_quanbu_XMLǃbutton_quanbuǃCOMPONENT
		getChildAt(index: 18): buttonsǁliaotianshiǁbutton_quanbu_XMLǃbutton_quanbuǃCOMPONENT
		getChildById(id: 'n104_q4bw'): buttonsǁliaotianshiǁbutton_quanbu_XMLǃbutton_quanbuǃCOMPONENT
		getChild(name: 'button_wodetouzhu'): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃbutton_wodetouzhuǃCOMPONENT
		getChildAt(index: 19): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃbutton_wodetouzhuǃCOMPONENT
		getChildById(id: 'n105_q4bw'): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃbutton_wodetouzhuǃCOMPONENT
		getChild(name: 'button_zhongjiangjilu'): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃbutton_zhongjiangjiluǃCOMPONENT
		getChildAt(index: 20): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃbutton_zhongjiangjiluǃCOMPONENT
		getChildById(id: 'n106_q4bw'): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃbutton_zhongjiangjiluǃCOMPONENT
		getChild(name: 'n119_CN2'): liaotianshi_XMLǃn119_CN2ǃTEXT
		getChildAt(index: 21): liaotianshi_XMLǃn119_CN2ǃTEXT
		getChildById(id: 'n119_xls7_CN2'): liaotianshi_XMLǃn119_CN2ǃTEXT
		getChild(name: 'n119_EN'): liaotianshi_XMLǃn119_ENǃTEXT
		getChildAt(index: 22): liaotianshi_XMLǃn119_ENǃTEXT
		getChildById(id: 'n119_xls7_EN'): liaotianshi_XMLǃn119_ENǃTEXT
		getChild(name: 'n119_IN'): liaotianshi_XMLǃn119_INǃTEXT
		getChildAt(index: 23): liaotianshi_XMLǃn119_INǃTEXT
		getChildById(id: 'n119_xls7_IN'): liaotianshi_XMLǃn119_INǃTEXT
		getChild(name: 'n119_JP'): liaotianshi_XMLǃn119_JPǃTEXT
		getChildAt(index: 24): liaotianshi_XMLǃn119_JPǃTEXT
		getChildById(id: 'n119_xls7_JP'): liaotianshi_XMLǃn119_JPǃTEXT
		getChild(name: 'n119_KR'): liaotianshi_XMLǃn119_KRǃTEXT
		getChildAt(index: 25): liaotianshi_XMLǃn119_KRǃTEXT
		getChildById(id: 'n119_xls7_KR'): liaotianshi_XMLǃn119_KRǃTEXT
		getChild(name: 'n119_TH'): liaotianshi_XMLǃn119_THǃTEXT
		getChildAt(index: 26): liaotianshi_XMLǃn119_THǃTEXT
		getChildById(id: 'n119_xls7_TH'): liaotianshi_XMLǃn119_THǃTEXT
		getChild(name: 'n119_VN'): liaotianshi_XMLǃn119_VNǃTEXT
		getChildAt(index: 27): liaotianshi_XMLǃn119_VNǃTEXT
		getChildById(id: 'n119_xls7_VN'): liaotianshi_XMLǃn119_VNǃTEXT
		getChild(name: 'n119'): liaotianshi_XMLǃn119ǃTEXT
		getChildAt(index: 28): liaotianshi_XMLǃn119ǃTEXT
		getChildById(id: 'n119_xls7'): liaotianshi_XMLǃn119ǃTEXT
		getChild(name: 'n120_CN2'): liaotianshi_XMLǃn120_CN2ǃTEXT
		getChildAt(index: 29): liaotianshi_XMLǃn120_CN2ǃTEXT
		getChildById(id: 'n120_xls7_CN2'): liaotianshi_XMLǃn120_CN2ǃTEXT
		getChild(name: 'n120_EN'): liaotianshi_XMLǃn120_ENǃTEXT
		getChildAt(index: 30): liaotianshi_XMLǃn120_ENǃTEXT
		getChildById(id: 'n120_xls7_EN'): liaotianshi_XMLǃn120_ENǃTEXT
		getChild(name: 'n120_IN'): liaotianshi_XMLǃn120_INǃTEXT
		getChildAt(index: 31): liaotianshi_XMLǃn120_INǃTEXT
		getChildById(id: 'n120_xls7_IN'): liaotianshi_XMLǃn120_INǃTEXT
		getChild(name: 'n120_JP'): liaotianshi_XMLǃn120_JPǃTEXT
		getChildAt(index: 32): liaotianshi_XMLǃn120_JPǃTEXT
		getChildById(id: 'n120_xls7_JP'): liaotianshi_XMLǃn120_JPǃTEXT
		getChild(name: 'n120_KR'): liaotianshi_XMLǃn120_KRǃTEXT
		getChildAt(index: 33): liaotianshi_XMLǃn120_KRǃTEXT
		getChildById(id: 'n120_xls7_KR'): liaotianshi_XMLǃn120_KRǃTEXT
		getChild(name: 'n120_TH'): liaotianshi_XMLǃn120_THǃTEXT
		getChildAt(index: 34): liaotianshi_XMLǃn120_THǃTEXT
		getChildById(id: 'n120_xls7_TH'): liaotianshi_XMLǃn120_THǃTEXT
		getChild(name: 'n120_VN'): liaotianshi_XMLǃn120_VNǃTEXT
		getChildAt(index: 35): liaotianshi_XMLǃn120_VNǃTEXT
		getChildById(id: 'n120_xls7_VN'): liaotianshi_XMLǃn120_VNǃTEXT
		getChild(name: 'n120'): liaotianshi_XMLǃn120ǃTEXT
		getChildAt(index: 36): liaotianshi_XMLǃn120ǃTEXT
		getChildById(id: 'n120_xls7'): liaotianshi_XMLǃn120ǃTEXT
		getChild(name: 'n121_CN2'): liaotianshi_XMLǃn121_CN2ǃTEXT
		getChildAt(index: 37): liaotianshi_XMLǃn121_CN2ǃTEXT
		getChildById(id: 'n121_xls7_CN2'): liaotianshi_XMLǃn121_CN2ǃTEXT
		getChild(name: 'n121_EN'): liaotianshi_XMLǃn121_ENǃTEXT
		getChildAt(index: 38): liaotianshi_XMLǃn121_ENǃTEXT
		getChildById(id: 'n121_xls7_EN'): liaotianshi_XMLǃn121_ENǃTEXT
		getChild(name: 'n121_IN'): liaotianshi_XMLǃn121_INǃTEXT
		getChildAt(index: 39): liaotianshi_XMLǃn121_INǃTEXT
		getChildById(id: 'n121_xls7_IN'): liaotianshi_XMLǃn121_INǃTEXT
		getChild(name: 'n121_JP'): liaotianshi_XMLǃn121_JPǃTEXT
		getChildAt(index: 40): liaotianshi_XMLǃn121_JPǃTEXT
		getChildById(id: 'n121_xls7_JP'): liaotianshi_XMLǃn121_JPǃTEXT
		getChild(name: 'n121_KR'): liaotianshi_XMLǃn121_KRǃTEXT
		getChildAt(index: 41): liaotianshi_XMLǃn121_KRǃTEXT
		getChildById(id: 'n121_xls7_KR'): liaotianshi_XMLǃn121_KRǃTEXT
		getChild(name: 'n121_TH'): liaotianshi_XMLǃn121_THǃTEXT
		getChildAt(index: 42): liaotianshi_XMLǃn121_THǃTEXT
		getChildById(id: 'n121_xls7_TH'): liaotianshi_XMLǃn121_THǃTEXT
		getChild(name: 'n121_VN'): liaotianshi_XMLǃn121_VNǃTEXT
		getChildAt(index: 43): liaotianshi_XMLǃn121_VNǃTEXT
		getChildById(id: 'n121_xls7_VN'): liaotianshi_XMLǃn121_VNǃTEXT
		getChild(name: 'n121'): liaotianshi_XMLǃn121ǃTEXT
		getChildAt(index: 44): liaotianshi_XMLǃn121ǃTEXT
		getChildById(id: 'n121_xls7'): liaotianshi_XMLǃn121ǃTEXT
		_children: [
			liaotianshi_XMLǃn126ǃIMAGE,
			buttonsǁbtn_hide_XMLǃbutton_hideǃCOMPONENT,
			buttonsǁbutton_danmu_XMLǃbutton_fenxiangǃCOMPONENT,
			liaotianneirong_XMLǃcontentǃCOMPONENT,
			buttonsǁliaotianshiǁbutton_fasong_XMLǃbutton_fasongǃCOMPONENT,
			liaotianshi_XMLǃinputǃTEXT,
			phrase_XMLǃphraseǃCOMPONENT,
			liaotianshi_XMLǃzhanghuyueǃTEXT,
			liaotianshi_XMLǃbenjuxiazhuǃTEXT,
			liaotianshi_XMLǃzongyingkuiǃTEXT,
			liaotianshi_XMLǃn118_CN2ǃTEXT,
			liaotianshi_XMLǃn118_ENǃTEXT,
			liaotianshi_XMLǃn118_INǃTEXT,
			liaotianshi_XMLǃn118_JPǃTEXT,
			liaotianshi_XMLǃn118_KRǃTEXT,
			liaotianshi_XMLǃn118_THǃTEXT,
			liaotianshi_XMLǃn118_VNǃTEXT,
			liaotianshi_XMLǃn118ǃTEXT,
			buttonsǁliaotianshiǁbutton_quanbu_XMLǃbutton_quanbuǃCOMPONENT,
			buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃbutton_wodetouzhuǃCOMPONENT,
			buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃbutton_zhongjiangjiluǃCOMPONENT,
			liaotianshi_XMLǃn119_CN2ǃTEXT,
			liaotianshi_XMLǃn119_ENǃTEXT,
			liaotianshi_XMLǃn119_INǃTEXT,
			liaotianshi_XMLǃn119_JPǃTEXT,
			liaotianshi_XMLǃn119_KRǃTEXT,
			liaotianshi_XMLǃn119_THǃTEXT,
			liaotianshi_XMLǃn119_VNǃTEXT,
			liaotianshi_XMLǃn119ǃTEXT,
			liaotianshi_XMLǃn120_CN2ǃTEXT,
			liaotianshi_XMLǃn120_ENǃTEXT,
			liaotianshi_XMLǃn120_INǃTEXT,
			liaotianshi_XMLǃn120_JPǃTEXT,
			liaotianshi_XMLǃn120_KRǃTEXT,
			liaotianshi_XMLǃn120_THǃTEXT,
			liaotianshi_XMLǃn120_VNǃTEXT,
			liaotianshi_XMLǃn120ǃTEXT,
			liaotianshi_XMLǃn121_CN2ǃTEXT,
			liaotianshi_XMLǃn121_ENǃTEXT,
			liaotianshi_XMLǃn121_INǃTEXT,
			liaotianshi_XMLǃn121_JPǃTEXT,
			liaotianshi_XMLǃn121_KRǃTEXT,
			liaotianshi_XMLǃn121_THǃTEXT,
			liaotianshi_XMLǃn121_VNǃTEXT,
			liaotianshi_XMLǃn121ǃTEXT
		]
		getController(name: '__language'): liaotianshi_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): liaotianshi_XMLǃ__languageǃCONTROLLER
		getController(name: 'controller_showhide'): liaotianshi_XMLǃcontroller_showhideǃCONTROLLER
		getControllerAt(index: 1): liaotianshi_XMLǃcontroller_showhideǃCONTROLLER
		getController(name: 'controller_content'): liaotianshi_XMLǃcontroller_contentǃCONTROLLER
		getControllerAt(index: 2): liaotianshi_XMLǃcontroller_contentǃCONTROLLER
		_controllers: [
			liaotianshi_XMLǃ__languageǃCONTROLLER,
			liaotianshi_XMLǃcontroller_showhideǃCONTROLLER,
			liaotianshi_XMLǃcontroller_contentǃCONTROLLER
		]
	}
	interface liaotianshi_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃcontroller_showhideǃCONTROLLER extends fairygui.Controller{
		_parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃcontroller_contentǃCONTROLLER extends fairygui.Controller{
		_parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃn126ǃIMAGE extends fairygui.GImage{
		parent: liaotianshi_XML
	}
	interface buttonsǁbtn_hide_XML extends fairygui.GButton{
		getChild(name: 'n0'): buttonsǁbtn_hide_XMLǃn0ǃGRAPH
		getChildAt(index: 0): buttonsǁbtn_hide_XMLǃn0ǃGRAPH
		getChildById(id: 'n0_h0rg'): buttonsǁbtn_hide_XMLǃn0ǃGRAPH
		_children: [
			buttonsǁbtn_hide_XMLǃn0ǃGRAPH
		]
		getController(name: '__language'): buttonsǁbtn_hide_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁbtn_hide_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁbtn_hide_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁbtn_hide_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁbtn_hide_XMLǃ__languageǃCONTROLLER,
			buttonsǁbtn_hide_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁbtn_hide_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁbtn_hide_XML
	}
	interface buttonsǁbtn_hide_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁbtn_hide_XML
	}
	interface buttonsǁbtn_hide_XMLǃn0ǃGRAPH extends fairygui.GGraph{
		parent: buttonsǁbtn_hide_XML
	}
	interface buttonsǁbtn_hide_XMLǃbutton_hideǃCOMPONENT extends buttonsǁbtn_hide_XML{
		parent: liaotianshi_XML
	}
	interface buttonsǁbutton_danmu_XML extends fairygui.GButton{
		getChild(name: 'n1'): buttonsǁbutton_danmu_XMLǃn1ǃIMAGE
		getChildAt(index: 0): buttonsǁbutton_danmu_XMLǃn1ǃIMAGE
		getChildById(id: 'n1'): buttonsǁbutton_danmu_XMLǃn1ǃIMAGE
		getChild(name: 'n3'): buttonsǁbutton_danmu_XMLǃn3ǃIMAGE
		getChildAt(index: 1): buttonsǁbutton_danmu_XMLǃn3ǃIMAGE
		getChildById(id: 'n3_j4zf'): buttonsǁbutton_danmu_XMLǃn3ǃIMAGE
		getChild(name: 'n4'): buttonsǁbutton_danmu_XMLǃn4ǃTEXT
		getChildAt(index: 2): buttonsǁbutton_danmu_XMLǃn4ǃTEXT
		getChildById(id: 'n4_ymr3'): buttonsǁbutton_danmu_XMLǃn4ǃTEXT
		getChild(name: 'n5'): buttonsǁbutton_danmu_XMLǃn5ǃTEXT
		getChildAt(index: 3): buttonsǁbutton_danmu_XMLǃn5ǃTEXT
		getChildById(id: 'n5_ymr3'): buttonsǁbutton_danmu_XMLǃn5ǃTEXT
		_children: [
			buttonsǁbutton_danmu_XMLǃn1ǃIMAGE,
			buttonsǁbutton_danmu_XMLǃn3ǃIMAGE,
			buttonsǁbutton_danmu_XMLǃn4ǃTEXT,
			buttonsǁbutton_danmu_XMLǃn5ǃTEXT
		]
		getController(name: '__language'): buttonsǁbutton_danmu_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁbutton_danmu_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁbutton_danmu_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁbutton_danmu_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁbutton_danmu_XMLǃ__languageǃCONTROLLER,
			buttonsǁbutton_danmu_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁbutton_danmu_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁbutton_danmu_XML
	}
	interface buttonsǁbutton_danmu_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁbutton_danmu_XML
	}
	interface buttonsǁbutton_danmu_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁbutton_danmu_XML
	}
	interface buttonsǁbutton_danmu_XMLǃn3ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁbutton_danmu_XML
	}
	interface buttonsǁbutton_danmu_XMLǃn4ǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁbutton_danmu_XML
	}
	interface buttonsǁbutton_danmu_XMLǃn5ǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁbutton_danmu_XML
	}
	interface buttonsǁbutton_danmu_XMLǃbutton_fenxiangǃCOMPONENT extends buttonsǁbutton_danmu_XML{
		parent: liaotianshi_XML
	}
	interface liaotianneirong_XML extends fairygui.GComponent{
		getChild(name: 'text'): liaotianneirong_XMLǃtextǃRICHTEXT
		getChildAt(index: 0): liaotianneirong_XMLǃtextǃRICHTEXT
		getChildById(id: 'n111_q4bw'): liaotianneirong_XMLǃtextǃRICHTEXT
		_children: [
			liaotianneirong_XMLǃtextǃRICHTEXT
		]
		getController(name: '__language'): liaotianneirong_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): liaotianneirong_XMLǃ__languageǃCONTROLLER
		_controllers: [
			liaotianneirong_XMLǃ__languageǃCONTROLLER
		]
	}
	interface liaotianneirong_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: liaotianneirong_XML
	}
	interface liaotianneirong_XMLǃtextǃRICHTEXT extends fairygui.GRichTextField{
		parent: liaotianneirong_XML
	}
	interface liaotianneirong_XMLǃcontentǃCOMPONENT extends liaotianneirong_XML{
		parent: liaotianshi_XML
	}
	interface buttonsǁliaotianshiǁbutton_fasong_XML extends fairygui.GButton{
		getChild(name: 'n1'): buttonsǁliaotianshiǁbutton_fasong_XMLǃn1ǃIMAGE
		getChildAt(index: 0): buttonsǁliaotianshiǁbutton_fasong_XMLǃn1ǃIMAGE
		getChildById(id: 'n1'): buttonsǁliaotianshiǁbutton_fasong_XMLǃn1ǃIMAGE
		getChild(name: 'n2_CN2'): buttonsǁliaotianshiǁbutton_fasong_XMLǃn2_CN2ǃTEXT
		getChildAt(index: 1): buttonsǁliaotianshiǁbutton_fasong_XMLǃn2_CN2ǃTEXT
		getChildById(id: 'n2_puwk_CN2'): buttonsǁliaotianshiǁbutton_fasong_XMLǃn2_CN2ǃTEXT
		getChild(name: 'n2_EN'): buttonsǁliaotianshiǁbutton_fasong_XMLǃn2_ENǃTEXT
		getChildAt(index: 2): buttonsǁliaotianshiǁbutton_fasong_XMLǃn2_ENǃTEXT
		getChildById(id: 'n2_puwk_EN'): buttonsǁliaotianshiǁbutton_fasong_XMLǃn2_ENǃTEXT
		getChild(name: 'n2_IN'): buttonsǁliaotianshiǁbutton_fasong_XMLǃn2_INǃTEXT
		getChildAt(index: 3): buttonsǁliaotianshiǁbutton_fasong_XMLǃn2_INǃTEXT
		getChildById(id: 'n2_puwk_IN'): buttonsǁliaotianshiǁbutton_fasong_XMLǃn2_INǃTEXT
		getChild(name: 'n2_JP'): buttonsǁliaotianshiǁbutton_fasong_XMLǃn2_JPǃTEXT
		getChildAt(index: 4): buttonsǁliaotianshiǁbutton_fasong_XMLǃn2_JPǃTEXT
		getChildById(id: 'n2_puwk_JP'): buttonsǁliaotianshiǁbutton_fasong_XMLǃn2_JPǃTEXT
		getChild(name: 'n2_KR'): buttonsǁliaotianshiǁbutton_fasong_XMLǃn2_KRǃTEXT
		getChildAt(index: 5): buttonsǁliaotianshiǁbutton_fasong_XMLǃn2_KRǃTEXT
		getChildById(id: 'n2_puwk_KR'): buttonsǁliaotianshiǁbutton_fasong_XMLǃn2_KRǃTEXT
		getChild(name: 'n2_TH'): buttonsǁliaotianshiǁbutton_fasong_XMLǃn2_THǃTEXT
		getChildAt(index: 6): buttonsǁliaotianshiǁbutton_fasong_XMLǃn2_THǃTEXT
		getChildById(id: 'n2_puwk_TH'): buttonsǁliaotianshiǁbutton_fasong_XMLǃn2_THǃTEXT
		getChild(name: 'n2_VN'): buttonsǁliaotianshiǁbutton_fasong_XMLǃn2_VNǃTEXT
		getChildAt(index: 7): buttonsǁliaotianshiǁbutton_fasong_XMLǃn2_VNǃTEXT
		getChildById(id: 'n2_puwk_VN'): buttonsǁliaotianshiǁbutton_fasong_XMLǃn2_VNǃTEXT
		getChild(name: 'n2'): buttonsǁliaotianshiǁbutton_fasong_XMLǃn2ǃTEXT
		getChildAt(index: 8): buttonsǁliaotianshiǁbutton_fasong_XMLǃn2ǃTEXT
		getChildById(id: 'n2_puwk'): buttonsǁliaotianshiǁbutton_fasong_XMLǃn2ǃTEXT
		_children: [
			buttonsǁliaotianshiǁbutton_fasong_XMLǃn1ǃIMAGE,
			buttonsǁliaotianshiǁbutton_fasong_XMLǃn2_CN2ǃTEXT,
			buttonsǁliaotianshiǁbutton_fasong_XMLǃn2_ENǃTEXT,
			buttonsǁliaotianshiǁbutton_fasong_XMLǃn2_INǃTEXT,
			buttonsǁliaotianshiǁbutton_fasong_XMLǃn2_JPǃTEXT,
			buttonsǁliaotianshiǁbutton_fasong_XMLǃn2_KRǃTEXT,
			buttonsǁliaotianshiǁbutton_fasong_XMLǃn2_THǃTEXT,
			buttonsǁliaotianshiǁbutton_fasong_XMLǃn2_VNǃTEXT,
			buttonsǁliaotianshiǁbutton_fasong_XMLǃn2ǃTEXT
		]
		getController(name: '__language'): buttonsǁliaotianshiǁbutton_fasong_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁliaotianshiǁbutton_fasong_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁliaotianshiǁbutton_fasong_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁliaotianshiǁbutton_fasong_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁliaotianshiǁbutton_fasong_XMLǃ__languageǃCONTROLLER,
			buttonsǁliaotianshiǁbutton_fasong_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁliaotianshiǁbutton_fasong_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁliaotianshiǁbutton_fasong_XML
	}
	interface buttonsǁliaotianshiǁbutton_fasong_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁliaotianshiǁbutton_fasong_XML
	}
	interface buttonsǁliaotianshiǁbutton_fasong_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁliaotianshiǁbutton_fasong_XML
	}
	interface buttonsǁliaotianshiǁbutton_fasong_XMLǃn2_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁliaotianshiǁbutton_fasong_XML
	}
	interface buttonsǁliaotianshiǁbutton_fasong_XMLǃn2_ENǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁliaotianshiǁbutton_fasong_XML
	}
	interface buttonsǁliaotianshiǁbutton_fasong_XMLǃn2_INǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁliaotianshiǁbutton_fasong_XML
	}
	interface buttonsǁliaotianshiǁbutton_fasong_XMLǃn2_JPǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁliaotianshiǁbutton_fasong_XML
	}
	interface buttonsǁliaotianshiǁbutton_fasong_XMLǃn2_KRǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁliaotianshiǁbutton_fasong_XML
	}
	interface buttonsǁliaotianshiǁbutton_fasong_XMLǃn2_THǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁliaotianshiǁbutton_fasong_XML
	}
	interface buttonsǁliaotianshiǁbutton_fasong_XMLǃn2_VNǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁliaotianshiǁbutton_fasong_XML
	}
	interface buttonsǁliaotianshiǁbutton_fasong_XMLǃn2ǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁliaotianshiǁbutton_fasong_XML
	}
	interface buttonsǁliaotianshiǁbutton_fasong_XMLǃbutton_fasongǃCOMPONENT extends buttonsǁliaotianshiǁbutton_fasong_XML{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃinputǃTEXT extends fairygui.GBasicTextField{
		parent: liaotianshi_XML
	}
	interface phrase_XML extends fairygui.GComponent{
		getChild(name: 'n0'): phrase_XMLǃn0ǃIMAGE
		getChildAt(index: 0): phrase_XMLǃn0ǃIMAGE
		getChildById(id: 'n0_h0rg'): phrase_XMLǃn0ǃIMAGE
		getChild(name: 'list'): phrase_XMLǃlistǃLIST
		getChildAt(index: 1): phrase_XMLǃlistǃLIST
		getChildById(id: 'n1_h0rg'): phrase_XMLǃlistǃLIST
		_children: [
			phrase_XMLǃn0ǃIMAGE,
			phrase_XMLǃlistǃLIST
		]
		getController(name: '__language'): phrase_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): phrase_XMLǃ__languageǃCONTROLLER
		_controllers: [
			phrase_XMLǃ__languageǃCONTROLLER
		]
	}
	interface phrase_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: phrase_XML
	}
	interface phrase_XMLǃn0ǃIMAGE extends fairygui.GImage{
		parent: phrase_XML
	}
	interface phrase_XMLǃlistǃLIST extends fairygui.GList{
		parent: phrase_XML
	}
	interface phrase_XMLǃphraseǃCOMPONENT extends phrase_XML{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃzhanghuyueǃTEXT extends fairygui.GBasicTextField{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃbenjuxiazhuǃTEXT extends fairygui.GBasicTextField{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃzongyingkuiǃTEXT extends fairygui.GBasicTextField{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃn118_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃn118_ENǃTEXT extends fairygui.GBasicTextField{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃn118_INǃTEXT extends fairygui.GBasicTextField{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃn118_JPǃTEXT extends fairygui.GBasicTextField{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃn118_KRǃTEXT extends fairygui.GBasicTextField{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃn118_THǃTEXT extends fairygui.GBasicTextField{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃn118_VNǃTEXT extends fairygui.GBasicTextField{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃn118ǃTEXT extends fairygui.GBasicTextField{
		parent: liaotianshi_XML
	}
	interface buttonsǁliaotianshiǁbutton_quanbu_XML extends fairygui.GButton{
		getChild(name: 'n1'): buttonsǁliaotianshiǁbutton_quanbu_XMLǃn1ǃIMAGE
		getChildAt(index: 0): buttonsǁliaotianshiǁbutton_quanbu_XMLǃn1ǃIMAGE
		getChildById(id: 'n1'): buttonsǁliaotianshiǁbutton_quanbu_XMLǃn1ǃIMAGE
		getChild(name: 'n2'): buttonsǁliaotianshiǁbutton_quanbu_XMLǃn2ǃIMAGE
		getChildAt(index: 1): buttonsǁliaotianshiǁbutton_quanbu_XMLǃn2ǃIMAGE
		getChildById(id: 'n2'): buttonsǁliaotianshiǁbutton_quanbu_XMLǃn2ǃIMAGE
		getChild(name: 'n3_CN2'): buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3_CN2ǃTEXT
		getChildAt(index: 2): buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3_CN2ǃTEXT
		getChildById(id: 'n3_xls7_CN2'): buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3_CN2ǃTEXT
		getChild(name: 'n3_EN'): buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3_ENǃTEXT
		getChildAt(index: 3): buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3_ENǃTEXT
		getChildById(id: 'n3_xls7_EN'): buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3_ENǃTEXT
		getChild(name: 'n3_IN'): buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3_INǃTEXT
		getChildAt(index: 4): buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3_INǃTEXT
		getChildById(id: 'n3_xls7_IN'): buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3_INǃTEXT
		getChild(name: 'n3_JP'): buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3_JPǃTEXT
		getChildAt(index: 5): buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3_JPǃTEXT
		getChildById(id: 'n3_xls7_JP'): buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3_JPǃTEXT
		getChild(name: 'n3_KR'): buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3_KRǃTEXT
		getChildAt(index: 6): buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3_KRǃTEXT
		getChildById(id: 'n3_xls7_KR'): buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3_KRǃTEXT
		getChild(name: 'n3_TH'): buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3_THǃTEXT
		getChildAt(index: 7): buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3_THǃTEXT
		getChildById(id: 'n3_xls7_TH'): buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3_THǃTEXT
		getChild(name: 'n3_VN'): buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3_VNǃTEXT
		getChildAt(index: 8): buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3_VNǃTEXT
		getChildById(id: 'n3_xls7_VN'): buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3_VNǃTEXT
		getChild(name: 'n3'): buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3ǃTEXT
		getChildAt(index: 9): buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3ǃTEXT
		getChildById(id: 'n3_xls7'): buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3ǃTEXT
		_children: [
			buttonsǁliaotianshiǁbutton_quanbu_XMLǃn1ǃIMAGE,
			buttonsǁliaotianshiǁbutton_quanbu_XMLǃn2ǃIMAGE,
			buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3_CN2ǃTEXT,
			buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3_ENǃTEXT,
			buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3_INǃTEXT,
			buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3_JPǃTEXT,
			buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3_KRǃTEXT,
			buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3_THǃTEXT,
			buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3_VNǃTEXT,
			buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3ǃTEXT
		]
		getController(name: '__language'): buttonsǁliaotianshiǁbutton_quanbu_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁliaotianshiǁbutton_quanbu_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁliaotianshiǁbutton_quanbu_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁliaotianshiǁbutton_quanbu_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁliaotianshiǁbutton_quanbu_XMLǃ__languageǃCONTROLLER,
			buttonsǁliaotianshiǁbutton_quanbu_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁliaotianshiǁbutton_quanbu_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁliaotianshiǁbutton_quanbu_XML
	}
	interface buttonsǁliaotianshiǁbutton_quanbu_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁliaotianshiǁbutton_quanbu_XML
	}
	interface buttonsǁliaotianshiǁbutton_quanbu_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁliaotianshiǁbutton_quanbu_XML
	}
	interface buttonsǁliaotianshiǁbutton_quanbu_XMLǃn2ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁliaotianshiǁbutton_quanbu_XML
	}
	interface buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁliaotianshiǁbutton_quanbu_XML
	}
	interface buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3_ENǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁliaotianshiǁbutton_quanbu_XML
	}
	interface buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3_INǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁliaotianshiǁbutton_quanbu_XML
	}
	interface buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3_JPǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁliaotianshiǁbutton_quanbu_XML
	}
	interface buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3_KRǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁliaotianshiǁbutton_quanbu_XML
	}
	interface buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3_THǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁliaotianshiǁbutton_quanbu_XML
	}
	interface buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3_VNǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁliaotianshiǁbutton_quanbu_XML
	}
	interface buttonsǁliaotianshiǁbutton_quanbu_XMLǃn3ǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁliaotianshiǁbutton_quanbu_XML
	}
	interface buttonsǁliaotianshiǁbutton_quanbu_XMLǃbutton_quanbuǃCOMPONENT extends buttonsǁliaotianshiǁbutton_quanbu_XML{
		parent: liaotianshi_XML
	}
	interface buttonsǁliaotianshiǁbutton_wodetouzhu_XML extends fairygui.GButton{
		getChild(name: 'n3'): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn3ǃIMAGE
		getChildAt(index: 0): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn3ǃIMAGE
		getChildById(id: 'n3_xls7'): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn3ǃIMAGE
		getChild(name: 'n4'): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn4ǃIMAGE
		getChildAt(index: 1): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn4ǃIMAGE
		getChildById(id: 'n4_xls7'): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn4ǃIMAGE
		getChild(name: 'n5_CN2'): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5_CN2ǃTEXT
		getChildAt(index: 2): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5_CN2ǃTEXT
		getChildById(id: 'n5_xls7_CN2'): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5_CN2ǃTEXT
		getChild(name: 'n5_EN'): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5_ENǃTEXT
		getChildAt(index: 3): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5_ENǃTEXT
		getChildById(id: 'n5_xls7_EN'): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5_ENǃTEXT
		getChild(name: 'n5_IN'): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5_INǃTEXT
		getChildAt(index: 4): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5_INǃTEXT
		getChildById(id: 'n5_xls7_IN'): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5_INǃTEXT
		getChild(name: 'n5_JP'): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5_JPǃTEXT
		getChildAt(index: 5): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5_JPǃTEXT
		getChildById(id: 'n5_xls7_JP'): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5_JPǃTEXT
		getChild(name: 'n5_KR'): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5_KRǃTEXT
		getChildAt(index: 6): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5_KRǃTEXT
		getChildById(id: 'n5_xls7_KR'): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5_KRǃTEXT
		getChild(name: 'n5_TH'): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5_THǃTEXT
		getChildAt(index: 7): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5_THǃTEXT
		getChildById(id: 'n5_xls7_TH'): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5_THǃTEXT
		getChild(name: 'n5_VN'): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5_VNǃTEXT
		getChildAt(index: 8): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5_VNǃTEXT
		getChildById(id: 'n5_xls7_VN'): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5_VNǃTEXT
		getChild(name: 'n5'): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5ǃTEXT
		getChildAt(index: 9): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5ǃTEXT
		getChildById(id: 'n5_xls7'): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5ǃTEXT
		_children: [
			buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn3ǃIMAGE,
			buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn4ǃIMAGE,
			buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5_CN2ǃTEXT,
			buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5_ENǃTEXT,
			buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5_INǃTEXT,
			buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5_JPǃTEXT,
			buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5_KRǃTEXT,
			buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5_THǃTEXT,
			buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5_VNǃTEXT,
			buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5ǃTEXT
		]
		getController(name: '__language'): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃ__languageǃCONTROLLER,
			buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁliaotianshiǁbutton_wodetouzhu_XML
	}
	interface buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁliaotianshiǁbutton_wodetouzhu_XML
	}
	interface buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn3ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁliaotianshiǁbutton_wodetouzhu_XML
	}
	interface buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn4ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁliaotianshiǁbutton_wodetouzhu_XML
	}
	interface buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁliaotianshiǁbutton_wodetouzhu_XML
	}
	interface buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5_ENǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁliaotianshiǁbutton_wodetouzhu_XML
	}
	interface buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5_INǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁliaotianshiǁbutton_wodetouzhu_XML
	}
	interface buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5_JPǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁliaotianshiǁbutton_wodetouzhu_XML
	}
	interface buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5_KRǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁliaotianshiǁbutton_wodetouzhu_XML
	}
	interface buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5_THǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁliaotianshiǁbutton_wodetouzhu_XML
	}
	interface buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5_VNǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁliaotianshiǁbutton_wodetouzhu_XML
	}
	interface buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃn5ǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁliaotianshiǁbutton_wodetouzhu_XML
	}
	interface buttonsǁliaotianshiǁbutton_wodetouzhu_XMLǃbutton_wodetouzhuǃCOMPONENT extends buttonsǁliaotianshiǁbutton_wodetouzhu_XML{
		parent: liaotianshi_XML
	}
	interface buttonsǁliaotianshiǁbutton_zhongjiangjilu_XML extends fairygui.GButton{
		getChild(name: 'n3'): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn3ǃIMAGE
		getChildAt(index: 0): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn3ǃIMAGE
		getChildById(id: 'n3_xls7'): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn3ǃIMAGE
		getChild(name: 'n4'): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn4ǃIMAGE
		getChildAt(index: 1): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn4ǃIMAGE
		getChildById(id: 'n4_xls7'): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn4ǃIMAGE
		getChild(name: 'n5_CN2'): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5_CN2ǃTEXT
		getChildAt(index: 2): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5_CN2ǃTEXT
		getChildById(id: 'n5_xls7_CN2'): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5_CN2ǃTEXT
		getChild(name: 'n5_EN'): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5_ENǃTEXT
		getChildAt(index: 3): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5_ENǃTEXT
		getChildById(id: 'n5_xls7_EN'): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5_ENǃTEXT
		getChild(name: 'n5_IN'): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5_INǃTEXT
		getChildAt(index: 4): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5_INǃTEXT
		getChildById(id: 'n5_xls7_IN'): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5_INǃTEXT
		getChild(name: 'n5_JP'): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5_JPǃTEXT
		getChildAt(index: 5): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5_JPǃTEXT
		getChildById(id: 'n5_xls7_JP'): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5_JPǃTEXT
		getChild(name: 'n5_KR'): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5_KRǃTEXT
		getChildAt(index: 6): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5_KRǃTEXT
		getChildById(id: 'n5_xls7_KR'): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5_KRǃTEXT
		getChild(name: 'n5_TH'): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5_THǃTEXT
		getChildAt(index: 7): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5_THǃTEXT
		getChildById(id: 'n5_xls7_TH'): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5_THǃTEXT
		getChild(name: 'n5_VN'): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5_VNǃTEXT
		getChildAt(index: 8): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5_VNǃTEXT
		getChildById(id: 'n5_xls7_VN'): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5_VNǃTEXT
		getChild(name: 'n5'): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5ǃTEXT
		getChildAt(index: 9): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5ǃTEXT
		getChildById(id: 'n5_xls7'): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5ǃTEXT
		_children: [
			buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn3ǃIMAGE,
			buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn4ǃIMAGE,
			buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5_CN2ǃTEXT,
			buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5_ENǃTEXT,
			buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5_INǃTEXT,
			buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5_JPǃTEXT,
			buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5_KRǃTEXT,
			buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5_THǃTEXT,
			buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5_VNǃTEXT,
			buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5ǃTEXT
		]
		getController(name: '__language'): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃ__languageǃCONTROLLER,
			buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁliaotianshiǁbutton_zhongjiangjilu_XML
	}
	interface buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁliaotianshiǁbutton_zhongjiangjilu_XML
	}
	interface buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn3ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁliaotianshiǁbutton_zhongjiangjilu_XML
	}
	interface buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn4ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁliaotianshiǁbutton_zhongjiangjilu_XML
	}
	interface buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁliaotianshiǁbutton_zhongjiangjilu_XML
	}
	interface buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5_ENǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁliaotianshiǁbutton_zhongjiangjilu_XML
	}
	interface buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5_INǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁliaotianshiǁbutton_zhongjiangjilu_XML
	}
	interface buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5_JPǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁliaotianshiǁbutton_zhongjiangjilu_XML
	}
	interface buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5_KRǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁliaotianshiǁbutton_zhongjiangjilu_XML
	}
	interface buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5_THǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁliaotianshiǁbutton_zhongjiangjilu_XML
	}
	interface buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5_VNǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁliaotianshiǁbutton_zhongjiangjilu_XML
	}
	interface buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃn5ǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁliaotianshiǁbutton_zhongjiangjilu_XML
	}
	interface buttonsǁliaotianshiǁbutton_zhongjiangjilu_XMLǃbutton_zhongjiangjiluǃCOMPONENT extends buttonsǁliaotianshiǁbutton_zhongjiangjilu_XML{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃn119_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃn119_ENǃTEXT extends fairygui.GBasicTextField{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃn119_INǃTEXT extends fairygui.GBasicTextField{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃn119_JPǃTEXT extends fairygui.GBasicTextField{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃn119_KRǃTEXT extends fairygui.GBasicTextField{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃn119_THǃTEXT extends fairygui.GBasicTextField{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃn119_VNǃTEXT extends fairygui.GBasicTextField{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃn119ǃTEXT extends fairygui.GBasicTextField{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃn120_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃn120_ENǃTEXT extends fairygui.GBasicTextField{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃn120_INǃTEXT extends fairygui.GBasicTextField{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃn120_JPǃTEXT extends fairygui.GBasicTextField{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃn120_KRǃTEXT extends fairygui.GBasicTextField{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃn120_THǃTEXT extends fairygui.GBasicTextField{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃn120_VNǃTEXT extends fairygui.GBasicTextField{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃn120ǃTEXT extends fairygui.GBasicTextField{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃn121_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃn121_ENǃTEXT extends fairygui.GBasicTextField{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃn121_INǃTEXT extends fairygui.GBasicTextField{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃn121_JPǃTEXT extends fairygui.GBasicTextField{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃn121_KRǃTEXT extends fairygui.GBasicTextField{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃn121_THǃTEXT extends fairygui.GBasicTextField{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃn121_VNǃTEXT extends fairygui.GBasicTextField{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃn121ǃTEXT extends fairygui.GBasicTextField{
		parent: liaotianshi_XML
	}
	interface liaotianshi_XMLǃliaotianshiǃCOMPONENT extends liaotianshi_XML{
		parent: Main_XML
	}
	interface buttonsǁbtn_gain_XML extends fairygui.GButton{
		getChild(name: 'n0'): buttonsǁbtn_gain_XMLǃn0ǃIMAGE
		getChildAt(index: 0): buttonsǁbtn_gain_XMLǃn0ǃIMAGE
		getChildById(id: 'n0_jtlp'): buttonsǁbtn_gain_XMLǃn0ǃIMAGE
		_children: [
			buttonsǁbtn_gain_XMLǃn0ǃIMAGE
		]
		getController(name: '__language'): buttonsǁbtn_gain_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁbtn_gain_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁbtn_gain_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁbtn_gain_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁbtn_gain_XMLǃ__languageǃCONTROLLER,
			buttonsǁbtn_gain_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁbtn_gain_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁbtn_gain_XML
	}
	interface buttonsǁbtn_gain_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁbtn_gain_XML
	}
	interface buttonsǁbtn_gain_XMLǃn0ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁbtn_gain_XML
	}
	interface buttonsǁbtn_gain_XMLǃbtn_gainǃCOMPONENT extends buttonsǁbtn_gain_XML{
		parent: Main_XML
	}
	interface settingsǁredirectBox_XML extends fairygui.GComboBox{
		getChild(name: 'n0'): settingsǁredirectBox_XMLǃn0ǃIMAGE
		getChildAt(index: 0): settingsǁredirectBox_XMLǃn0ǃIMAGE
		getChildById(id: 'n0_ujhe'): settingsǁredirectBox_XMLǃn0ǃIMAGE
		getChild(name: 'title'): settingsǁredirectBox_XMLǃtitleǃTEXT
		getChildAt(index: 1): settingsǁredirectBox_XMLǃtitleǃTEXT
		getChildById(id: 'n1_ujhe'): settingsǁredirectBox_XMLǃtitleǃTEXT
		_children: [
			settingsǁredirectBox_XMLǃn0ǃIMAGE,
			settingsǁredirectBox_XMLǃtitleǃTEXT
		]
		getController(name: '__language'): settingsǁredirectBox_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): settingsǁredirectBox_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): settingsǁredirectBox_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): settingsǁredirectBox_XMLǃbuttonǃCONTROLLER
		_controllers: [
			settingsǁredirectBox_XMLǃ__languageǃCONTROLLER,
			settingsǁredirectBox_XMLǃbuttonǃCONTROLLER
		]
	}
	interface settingsǁredirectBox_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: settingsǁredirectBox_XML
	}
	interface settingsǁredirectBox_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: settingsǁredirectBox_XML
	}
	interface settingsǁredirectBox_XMLǃn0ǃIMAGE extends fairygui.GImage{
		parent: settingsǁredirectBox_XML
	}
	interface settingsǁredirectBox_XMLǃtitleǃTEXT extends fairygui.GBasicTextField{
		parent: settingsǁredirectBox_XML
	}
	interface settingsǁredirectBox_XMLǃredirectGameǃCOMPONENT extends settingsǁredirectBox_XML{
		parent: Main_XML
	}
	interface menu_XML extends fairygui.GComponent{
		getChild(name: 'n213'): menu_XMLǃn213ǃIMAGE
		getChildAt(index: 0): menu_XMLǃn213ǃIMAGE
		getChildById(id: 'n213_rad3'): menu_XMLǃn213ǃIMAGE
		getChild(name: 'button_help'): buttonsǁgongnengǁbutton_help_XMLǃbutton_helpǃCOMPONENT
		getChildAt(index: 1): buttonsǁgongnengǁbutton_help_XMLǃbutton_helpǃCOMPONENT
		getChildById(id: 'n99_edb2'): buttonsǁgongnengǁbutton_help_XMLǃbutton_helpǃCOMPONENT
		getChild(name: 'button_settings'): buttonsǁgongnengǁbutton_settings_XMLǃbutton_settingsǃCOMPONENT
		getChildAt(index: 2): buttonsǁgongnengǁbutton_settings_XMLǃbutton_settingsǃCOMPONENT
		getChildById(id: 'n212_jwy6'): buttonsǁgongnengǁbutton_settings_XMLǃbutton_settingsǃCOMPONENT
		getChild(name: 'button_paihangbang'): buttonsǁgongnengǁbutton_paihangbang_XMLǃbutton_paihangbangǃCOMPONENT
		getChildAt(index: 3): buttonsǁgongnengǁbutton_paihangbang_XMLǃbutton_paihangbangǃCOMPONENT
		getChildById(id: 'n100_edb2'): buttonsǁgongnengǁbutton_paihangbang_XMLǃbutton_paihangbangǃCOMPONENT
		_children: [
			menu_XMLǃn213ǃIMAGE,
			buttonsǁgongnengǁbutton_help_XMLǃbutton_helpǃCOMPONENT,
			buttonsǁgongnengǁbutton_settings_XMLǃbutton_settingsǃCOMPONENT,
			buttonsǁgongnengǁbutton_paihangbang_XMLǃbutton_paihangbangǃCOMPONENT
		]
		getController(name: '__language'): menu_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): menu_XMLǃ__languageǃCONTROLLER
		_controllers: [
			menu_XMLǃ__languageǃCONTROLLER
		]
	}
	interface menu_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: menu_XML
	}
	interface menu_XMLǃn213ǃIMAGE extends fairygui.GImage{
		parent: menu_XML
	}
	interface buttonsǁgongnengǁbutton_help_XML extends fairygui.GButton{
		getChild(name: 'n1'): buttonsǁgongnengǁbutton_help_XMLǃn1ǃIMAGE
		getChildAt(index: 0): buttonsǁgongnengǁbutton_help_XMLǃn1ǃIMAGE
		getChildById(id: 'n1'): buttonsǁgongnengǁbutton_help_XMLǃn1ǃIMAGE
		_children: [
			buttonsǁgongnengǁbutton_help_XMLǃn1ǃIMAGE
		]
		getController(name: '__language'): buttonsǁgongnengǁbutton_help_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁgongnengǁbutton_help_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁgongnengǁbutton_help_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁgongnengǁbutton_help_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁgongnengǁbutton_help_XMLǃ__languageǃCONTROLLER,
			buttonsǁgongnengǁbutton_help_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁgongnengǁbutton_help_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁgongnengǁbutton_help_XML
	}
	interface buttonsǁgongnengǁbutton_help_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁgongnengǁbutton_help_XML
	}
	interface buttonsǁgongnengǁbutton_help_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁgongnengǁbutton_help_XML
	}
	interface buttonsǁgongnengǁbutton_help_XMLǃbutton_helpǃCOMPONENT extends buttonsǁgongnengǁbutton_help_XML{
		parent: menu_XML
	}
	interface buttonsǁgongnengǁbutton_settings_XML extends fairygui.GButton{
		getChild(name: 'n1'): buttonsǁgongnengǁbutton_settings_XMLǃn1ǃIMAGE
		getChildAt(index: 0): buttonsǁgongnengǁbutton_settings_XMLǃn1ǃIMAGE
		getChildById(id: 'n1'): buttonsǁgongnengǁbutton_settings_XMLǃn1ǃIMAGE
		_children: [
			buttonsǁgongnengǁbutton_settings_XMLǃn1ǃIMAGE
		]
		getController(name: '__language'): buttonsǁgongnengǁbutton_settings_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁgongnengǁbutton_settings_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁgongnengǁbutton_settings_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁgongnengǁbutton_settings_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁgongnengǁbutton_settings_XMLǃ__languageǃCONTROLLER,
			buttonsǁgongnengǁbutton_settings_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁgongnengǁbutton_settings_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁgongnengǁbutton_settings_XML
	}
	interface buttonsǁgongnengǁbutton_settings_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁgongnengǁbutton_settings_XML
	}
	interface buttonsǁgongnengǁbutton_settings_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁgongnengǁbutton_settings_XML
	}
	interface buttonsǁgongnengǁbutton_settings_XMLǃbutton_settingsǃCOMPONENT extends buttonsǁgongnengǁbutton_settings_XML{
		parent: menu_XML
	}
	interface buttonsǁgongnengǁbutton_paihangbang_XML extends fairygui.GButton{
		getChild(name: 'n1'): buttonsǁgongnengǁbutton_paihangbang_XMLǃn1ǃIMAGE
		getChildAt(index: 0): buttonsǁgongnengǁbutton_paihangbang_XMLǃn1ǃIMAGE
		getChildById(id: 'n1'): buttonsǁgongnengǁbutton_paihangbang_XMLǃn1ǃIMAGE
		_children: [
			buttonsǁgongnengǁbutton_paihangbang_XMLǃn1ǃIMAGE
		]
		getController(name: '__language'): buttonsǁgongnengǁbutton_paihangbang_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁgongnengǁbutton_paihangbang_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁgongnengǁbutton_paihangbang_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁgongnengǁbutton_paihangbang_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁgongnengǁbutton_paihangbang_XMLǃ__languageǃCONTROLLER,
			buttonsǁgongnengǁbutton_paihangbang_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁgongnengǁbutton_paihangbang_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁgongnengǁbutton_paihangbang_XML
	}
	interface buttonsǁgongnengǁbutton_paihangbang_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁgongnengǁbutton_paihangbang_XML
	}
	interface buttonsǁgongnengǁbutton_paihangbang_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁgongnengǁbutton_paihangbang_XML
	}
	interface buttonsǁgongnengǁbutton_paihangbang_XMLǃbutton_paihangbangǃCOMPONENT extends buttonsǁgongnengǁbutton_paihangbang_XML{
		parent: menu_XML
	}
	interface menu_XMLǃmenuǃCOMPONENT extends menu_XML{
		parent: Main_XML
	}
	interface newluzhitu_XML extends fairygui.GComponent{
		getChild(name: 'zhupan0'): newluzhitu_XMLǃzhupan0ǃLOADER
		getChildAt(index: 0): newluzhitu_XMLǃzhupan0ǃLOADER
		getChildById(id: 'n3_awmy'): newluzhitu_XMLǃzhupan0ǃLOADER
		getChild(name: 'zhupan1'): newluzhitu_XMLǃzhupan1ǃLOADER
		getChildAt(index: 1): newluzhitu_XMLǃzhupan1ǃLOADER
		getChildById(id: 'n4_awmy'): newluzhitu_XMLǃzhupan1ǃLOADER
		getChild(name: 'zhupan2'): newluzhitu_XMLǃzhupan2ǃLOADER
		getChildAt(index: 2): newluzhitu_XMLǃzhupan2ǃLOADER
		getChildById(id: 'n5_awmy'): newluzhitu_XMLǃzhupan2ǃLOADER
		getChild(name: 'zhupan3'): newluzhitu_XMLǃzhupan3ǃLOADER
		getChildAt(index: 3): newluzhitu_XMLǃzhupan3ǃLOADER
		getChildById(id: 'n6_awmy'): newluzhitu_XMLǃzhupan3ǃLOADER
		getChild(name: 'zhupan4'): newluzhitu_XMLǃzhupan4ǃLOADER
		getChildAt(index: 4): newluzhitu_XMLǃzhupan4ǃLOADER
		getChildById(id: 'n7_awmy'): newluzhitu_XMLǃzhupan4ǃLOADER
		getChild(name: 'zhupan5'): newluzhitu_XMLǃzhupan5ǃLOADER
		getChildAt(index: 5): newluzhitu_XMLǃzhupan5ǃLOADER
		getChildById(id: 'n8_awmy'): newluzhitu_XMLǃzhupan5ǃLOADER
		getChild(name: 'zhupan6'): newluzhitu_XMLǃzhupan6ǃLOADER
		getChildAt(index: 6): newluzhitu_XMLǃzhupan6ǃLOADER
		getChildById(id: 'n9_awmy'): newluzhitu_XMLǃzhupan6ǃLOADER
		getChild(name: 'zhupan7'): newluzhitu_XMLǃzhupan7ǃLOADER
		getChildAt(index: 7): newluzhitu_XMLǃzhupan7ǃLOADER
		getChildById(id: 'n10_awmy'): newluzhitu_XMLǃzhupan7ǃLOADER
		getChild(name: 'zhupan8'): newluzhitu_XMLǃzhupan8ǃLOADER
		getChildAt(index: 8): newluzhitu_XMLǃzhupan8ǃLOADER
		getChildById(id: 'n11_awmy'): newluzhitu_XMLǃzhupan8ǃLOADER
		getChild(name: 'zhupan9'): newluzhitu_XMLǃzhupan9ǃLOADER
		getChildAt(index: 9): newluzhitu_XMLǃzhupan9ǃLOADER
		getChildById(id: 'n12_awmy'): newluzhitu_XMLǃzhupan9ǃLOADER
		getChild(name: 'zhupan10'): newluzhitu_XMLǃzhupan10ǃLOADER
		getChildAt(index: 10): newluzhitu_XMLǃzhupan10ǃLOADER
		getChildById(id: 'n13_awmy'): newluzhitu_XMLǃzhupan10ǃLOADER
		getChild(name: 'zhupan11'): newluzhitu_XMLǃzhupan11ǃLOADER
		getChildAt(index: 11): newluzhitu_XMLǃzhupan11ǃLOADER
		getChildById(id: 'n14_awmy'): newluzhitu_XMLǃzhupan11ǃLOADER
		getChild(name: 'zhupan12'): newluzhitu_XMLǃzhupan12ǃLOADER
		getChildAt(index: 12): newluzhitu_XMLǃzhupan12ǃLOADER
		getChildById(id: 'n15_awmy'): newluzhitu_XMLǃzhupan12ǃLOADER
		getChild(name: 'zhupan13'): newluzhitu_XMLǃzhupan13ǃLOADER
		getChildAt(index: 13): newluzhitu_XMLǃzhupan13ǃLOADER
		getChildById(id: 'n16_awmy'): newluzhitu_XMLǃzhupan13ǃLOADER
		getChild(name: 'zhupan14'): newluzhitu_XMLǃzhupan14ǃLOADER
		getChildAt(index: 14): newluzhitu_XMLǃzhupan14ǃLOADER
		getChildById(id: 'n17_awmy'): newluzhitu_XMLǃzhupan14ǃLOADER
		getChild(name: 'zhupan15'): newluzhitu_XMLǃzhupan15ǃLOADER
		getChildAt(index: 15): newluzhitu_XMLǃzhupan15ǃLOADER
		getChildById(id: 'n18_awmy'): newluzhitu_XMLǃzhupan15ǃLOADER
		getChild(name: 'zhupan16'): newluzhitu_XMLǃzhupan16ǃLOADER
		getChildAt(index: 16): newluzhitu_XMLǃzhupan16ǃLOADER
		getChildById(id: 'n19_awmy'): newluzhitu_XMLǃzhupan16ǃLOADER
		getChild(name: 'zhupan17'): newluzhitu_XMLǃzhupan17ǃLOADER
		getChildAt(index: 17): newluzhitu_XMLǃzhupan17ǃLOADER
		getChildById(id: 'n20_awmy'): newluzhitu_XMLǃzhupan17ǃLOADER
		getChild(name: 'zhupan18'): newluzhitu_XMLǃzhupan18ǃLOADER
		getChildAt(index: 18): newluzhitu_XMLǃzhupan18ǃLOADER
		getChildById(id: 'n21_awmy'): newluzhitu_XMLǃzhupan18ǃLOADER
		getChild(name: 'zhupan19'): newluzhitu_XMLǃzhupan19ǃLOADER
		getChildAt(index: 19): newluzhitu_XMLǃzhupan19ǃLOADER
		getChildById(id: 'n22_awmy'): newluzhitu_XMLǃzhupan19ǃLOADER
		getChild(name: 'zhupan20'): newluzhitu_XMLǃzhupan20ǃLOADER
		getChildAt(index: 20): newluzhitu_XMLǃzhupan20ǃLOADER
		getChildById(id: 'n23_awmy'): newluzhitu_XMLǃzhupan20ǃLOADER
		getChild(name: 'zhupan21'): newluzhitu_XMLǃzhupan21ǃLOADER
		getChildAt(index: 21): newluzhitu_XMLǃzhupan21ǃLOADER
		getChildById(id: 'n24_awmy'): newluzhitu_XMLǃzhupan21ǃLOADER
		getChild(name: 'zhupan22'): newluzhitu_XMLǃzhupan22ǃLOADER
		getChildAt(index: 22): newluzhitu_XMLǃzhupan22ǃLOADER
		getChildById(id: 'n25_awmy'): newluzhitu_XMLǃzhupan22ǃLOADER
		getChild(name: 'zhupan23'): newluzhitu_XMLǃzhupan23ǃLOADER
		getChildAt(index: 23): newluzhitu_XMLǃzhupan23ǃLOADER
		getChildById(id: 'n26_awmy'): newluzhitu_XMLǃzhupan23ǃLOADER
		getChild(name: 'zhupan24'): newluzhitu_XMLǃzhupan24ǃLOADER
		getChildAt(index: 24): newluzhitu_XMLǃzhupan24ǃLOADER
		getChildById(id: 'n27_awmy'): newluzhitu_XMLǃzhupan24ǃLOADER
		getChild(name: 'zhupan25'): newluzhitu_XMLǃzhupan25ǃLOADER
		getChildAt(index: 25): newluzhitu_XMLǃzhupan25ǃLOADER
		getChildById(id: 'n28_awmy'): newluzhitu_XMLǃzhupan25ǃLOADER
		getChild(name: 'zhupan26'): newluzhitu_XMLǃzhupan26ǃLOADER
		getChildAt(index: 26): newluzhitu_XMLǃzhupan26ǃLOADER
		getChildById(id: 'n29_awmy'): newluzhitu_XMLǃzhupan26ǃLOADER
		getChild(name: 'zhupan27'): newluzhitu_XMLǃzhupan27ǃLOADER
		getChildAt(index: 27): newluzhitu_XMLǃzhupan27ǃLOADER
		getChildById(id: 'n30_awmy'): newluzhitu_XMLǃzhupan27ǃLOADER
		getChild(name: 'zhupan28'): newluzhitu_XMLǃzhupan28ǃLOADER
		getChildAt(index: 28): newluzhitu_XMLǃzhupan28ǃLOADER
		getChildById(id: 'n31_awmy'): newluzhitu_XMLǃzhupan28ǃLOADER
		getChild(name: 'zhupan29'): newluzhitu_XMLǃzhupan29ǃLOADER
		getChildAt(index: 29): newluzhitu_XMLǃzhupan29ǃLOADER
		getChildById(id: 'n32_awmy'): newluzhitu_XMLǃzhupan29ǃLOADER
		getChild(name: 'zhupan30'): newluzhitu_XMLǃzhupan30ǃLOADER
		getChildAt(index: 30): newluzhitu_XMLǃzhupan30ǃLOADER
		getChildById(id: 'n33_awmy'): newluzhitu_XMLǃzhupan30ǃLOADER
		getChild(name: 'zhupan31'): newluzhitu_XMLǃzhupan31ǃLOADER
		getChildAt(index: 31): newluzhitu_XMLǃzhupan31ǃLOADER
		getChildById(id: 'n34_awmy'): newluzhitu_XMLǃzhupan31ǃLOADER
		getChild(name: 'zhupan32'): newluzhitu_XMLǃzhupan32ǃLOADER
		getChildAt(index: 32): newluzhitu_XMLǃzhupan32ǃLOADER
		getChildById(id: 'n35_awmy'): newluzhitu_XMLǃzhupan32ǃLOADER
		getChild(name: 'zhupan33'): newluzhitu_XMLǃzhupan33ǃLOADER
		getChildAt(index: 33): newluzhitu_XMLǃzhupan33ǃLOADER
		getChildById(id: 'n36_awmy'): newluzhitu_XMLǃzhupan33ǃLOADER
		getChild(name: 'zhupan34'): newluzhitu_XMLǃzhupan34ǃLOADER
		getChildAt(index: 34): newluzhitu_XMLǃzhupan34ǃLOADER
		getChildById(id: 'n37_awmy'): newluzhitu_XMLǃzhupan34ǃLOADER
		getChild(name: 'zhupan35'): newluzhitu_XMLǃzhupan35ǃLOADER
		getChildAt(index: 35): newluzhitu_XMLǃzhupan35ǃLOADER
		getChildById(id: 'n38_awmy'): newluzhitu_XMLǃzhupan35ǃLOADER
		getChild(name: 'zhupan36'): newluzhitu_XMLǃzhupan36ǃLOADER
		getChildAt(index: 36): newluzhitu_XMLǃzhupan36ǃLOADER
		getChildById(id: 'n39_awmy'): newluzhitu_XMLǃzhupan36ǃLOADER
		getChild(name: 'zhupan37'): newluzhitu_XMLǃzhupan37ǃLOADER
		getChildAt(index: 37): newluzhitu_XMLǃzhupan37ǃLOADER
		getChildById(id: 'n40_awmy'): newluzhitu_XMLǃzhupan37ǃLOADER
		getChild(name: 'zhupan38'): newluzhitu_XMLǃzhupan38ǃLOADER
		getChildAt(index: 38): newluzhitu_XMLǃzhupan38ǃLOADER
		getChildById(id: 'n41_awmy'): newluzhitu_XMLǃzhupan38ǃLOADER
		getChild(name: 'zhupan39'): newluzhitu_XMLǃzhupan39ǃLOADER
		getChildAt(index: 39): newluzhitu_XMLǃzhupan39ǃLOADER
		getChildById(id: 'n42_awmy'): newluzhitu_XMLǃzhupan39ǃLOADER
		getChild(name: 'zhupan40'): newluzhitu_XMLǃzhupan40ǃLOADER
		getChildAt(index: 40): newluzhitu_XMLǃzhupan40ǃLOADER
		getChildById(id: 'n43_awmy'): newluzhitu_XMLǃzhupan40ǃLOADER
		getChild(name: 'zhupan41'): newluzhitu_XMLǃzhupan41ǃLOADER
		getChildAt(index: 41): newluzhitu_XMLǃzhupan41ǃLOADER
		getChildById(id: 'n44_awmy'): newluzhitu_XMLǃzhupan41ǃLOADER
		getChild(name: 'zhupan42'): newluzhitu_XMLǃzhupan42ǃLOADER
		getChildAt(index: 42): newluzhitu_XMLǃzhupan42ǃLOADER
		getChildById(id: 'n45_awmy'): newluzhitu_XMLǃzhupan42ǃLOADER
		getChild(name: 'zhupan43'): newluzhitu_XMLǃzhupan43ǃLOADER
		getChildAt(index: 43): newluzhitu_XMLǃzhupan43ǃLOADER
		getChildById(id: 'n46_awmy'): newluzhitu_XMLǃzhupan43ǃLOADER
		getChild(name: 'zhupan44'): newluzhitu_XMLǃzhupan44ǃLOADER
		getChildAt(index: 44): newluzhitu_XMLǃzhupan44ǃLOADER
		getChildById(id: 'n47_awmy'): newluzhitu_XMLǃzhupan44ǃLOADER
		getChild(name: 'zhupan45'): newluzhitu_XMLǃzhupan45ǃLOADER
		getChildAt(index: 45): newluzhitu_XMLǃzhupan45ǃLOADER
		getChildById(id: 'n48_awmy'): newluzhitu_XMLǃzhupan45ǃLOADER
		getChild(name: 'zhupan46'): newluzhitu_XMLǃzhupan46ǃLOADER
		getChildAt(index: 46): newluzhitu_XMLǃzhupan46ǃLOADER
		getChildById(id: 'n49_awmy'): newluzhitu_XMLǃzhupan46ǃLOADER
		getChild(name: 'zhupan47'): newluzhitu_XMLǃzhupan47ǃLOADER
		getChildAt(index: 47): newluzhitu_XMLǃzhupan47ǃLOADER
		getChildById(id: 'n50_awmy'): newluzhitu_XMLǃzhupan47ǃLOADER
		getChild(name: 'zhupan48'): newluzhitu_XMLǃzhupan48ǃLOADER
		getChildAt(index: 48): newluzhitu_XMLǃzhupan48ǃLOADER
		getChildById(id: 'n51_awmy'): newluzhitu_XMLǃzhupan48ǃLOADER
		getChild(name: 'zhupan49'): newluzhitu_XMLǃzhupan49ǃLOADER
		getChildAt(index: 49): newluzhitu_XMLǃzhupan49ǃLOADER
		getChildById(id: 'n52_awmy'): newluzhitu_XMLǃzhupan49ǃLOADER
		getChild(name: 'zhupan50'): newluzhitu_XMLǃzhupan50ǃLOADER
		getChildAt(index: 50): newluzhitu_XMLǃzhupan50ǃLOADER
		getChildById(id: 'n53_awmy'): newluzhitu_XMLǃzhupan50ǃLOADER
		getChild(name: 'zhupan51'): newluzhitu_XMLǃzhupan51ǃLOADER
		getChildAt(index: 51): newluzhitu_XMLǃzhupan51ǃLOADER
		getChildById(id: 'n54_awmy'): newluzhitu_XMLǃzhupan51ǃLOADER
		getChild(name: 'zhupan52'): newluzhitu_XMLǃzhupan52ǃLOADER
		getChildAt(index: 52): newluzhitu_XMLǃzhupan52ǃLOADER
		getChildById(id: 'n55_awmy'): newluzhitu_XMLǃzhupan52ǃLOADER
		getChild(name: 'zhupan53'): newluzhitu_XMLǃzhupan53ǃLOADER
		getChildAt(index: 53): newluzhitu_XMLǃzhupan53ǃLOADER
		getChildById(id: 'n56_awmy'): newluzhitu_XMLǃzhupan53ǃLOADER
		getChild(name: 'zhupan54'): newluzhitu_XMLǃzhupan54ǃLOADER
		getChildAt(index: 54): newluzhitu_XMLǃzhupan54ǃLOADER
		getChildById(id: 'n57_awmy'): newluzhitu_XMLǃzhupan54ǃLOADER
		getChild(name: 'zhupan55'): newluzhitu_XMLǃzhupan55ǃLOADER
		getChildAt(index: 55): newluzhitu_XMLǃzhupan55ǃLOADER
		getChildById(id: 'n58_awmy'): newluzhitu_XMLǃzhupan55ǃLOADER
		getChild(name: 'zhupan56'): newluzhitu_XMLǃzhupan56ǃLOADER
		getChildAt(index: 56): newluzhitu_XMLǃzhupan56ǃLOADER
		getChildById(id: 'n59_awmy'): newluzhitu_XMLǃzhupan56ǃLOADER
		getChild(name: 'zhupan57'): newluzhitu_XMLǃzhupan57ǃLOADER
		getChildAt(index: 57): newluzhitu_XMLǃzhupan57ǃLOADER
		getChildById(id: 'n60_awmy'): newluzhitu_XMLǃzhupan57ǃLOADER
		getChild(name: 'zhupan58'): newluzhitu_XMLǃzhupan58ǃLOADER
		getChildAt(index: 58): newluzhitu_XMLǃzhupan58ǃLOADER
		getChildById(id: 'n61_awmy'): newluzhitu_XMLǃzhupan58ǃLOADER
		getChild(name: 'zhupan59'): newluzhitu_XMLǃzhupan59ǃLOADER
		getChildAt(index: 59): newluzhitu_XMLǃzhupan59ǃLOADER
		getChildById(id: 'n62_awmy'): newluzhitu_XMLǃzhupan59ǃLOADER
		getChild(name: 'num_0'): componentǁDaXiaoinfo_XMLǃnum_0ǃCOMPONENT
		getChildAt(index: 60): componentǁDaXiaoinfo_XMLǃnum_0ǃCOMPONENT
		getChildById(id: 'n607_wl2k'): componentǁDaXiaoinfo_XMLǃnum_0ǃCOMPONENT
		getChild(name: 'num_1'): componentǁDaXiaoinfo_XMLǃnum_1ǃCOMPONENT
		getChildAt(index: 61): componentǁDaXiaoinfo_XMLǃnum_1ǃCOMPONENT
		getChildById(id: 'n608_wl2k'): componentǁDaXiaoinfo_XMLǃnum_1ǃCOMPONENT
		getChild(name: 'num_2'): componentǁDaXiaoinfo_XMLǃnum_2ǃCOMPONENT
		getChildAt(index: 62): componentǁDaXiaoinfo_XMLǃnum_2ǃCOMPONENT
		getChildById(id: 'n609_wl2k'): componentǁDaXiaoinfo_XMLǃnum_2ǃCOMPONENT
		getChild(name: 'num_3'): componentǁDaXiaoinfo_XMLǃnum_3ǃCOMPONENT
		getChildAt(index: 63): componentǁDaXiaoinfo_XMLǃnum_3ǃCOMPONENT
		getChildById(id: 'n610_wl2k'): componentǁDaXiaoinfo_XMLǃnum_3ǃCOMPONENT
		getChild(name: 'num_4'): componentǁDaXiaoinfo_XMLǃnum_4ǃCOMPONENT
		getChildAt(index: 64): componentǁDaXiaoinfo_XMLǃnum_4ǃCOMPONENT
		getChildById(id: 'n611_wl2k'): componentǁDaXiaoinfo_XMLǃnum_4ǃCOMPONENT
		getChild(name: 'num_5'): componentǁDaXiaoinfo_XMLǃnum_5ǃCOMPONENT
		getChildAt(index: 65): componentǁDaXiaoinfo_XMLǃnum_5ǃCOMPONENT
		getChildById(id: 'n612_wl2k'): componentǁDaXiaoinfo_XMLǃnum_5ǃCOMPONENT
		getChild(name: 'num_6'): componentǁDaXiaoinfo_XMLǃnum_6ǃCOMPONENT
		getChildAt(index: 66): componentǁDaXiaoinfo_XMLǃnum_6ǃCOMPONENT
		getChildById(id: 'n613_wl2k'): componentǁDaXiaoinfo_XMLǃnum_6ǃCOMPONENT
		getChild(name: 'num_7'): componentǁDaXiaoinfo_XMLǃnum_7ǃCOMPONENT
		getChildAt(index: 67): componentǁDaXiaoinfo_XMLǃnum_7ǃCOMPONENT
		getChildById(id: 'n614_wl2k'): componentǁDaXiaoinfo_XMLǃnum_7ǃCOMPONENT
		getChild(name: 'num_8'): componentǁDaXiaoinfo_XMLǃnum_8ǃCOMPONENT
		getChildAt(index: 68): componentǁDaXiaoinfo_XMLǃnum_8ǃCOMPONENT
		getChildById(id: 'n615_wl2k'): componentǁDaXiaoinfo_XMLǃnum_8ǃCOMPONENT
		getChild(name: 'num_9'): componentǁDaXiaoinfo_XMLǃnum_9ǃCOMPONENT
		getChildAt(index: 69): componentǁDaXiaoinfo_XMLǃnum_9ǃCOMPONENT
		getChildById(id: 'n616_wl2k'): componentǁDaXiaoinfo_XMLǃnum_9ǃCOMPONENT
		getChild(name: 'dalu0'): daluComponent_XMLǃdalu0ǃCOMPONENT
		getChildAt(index: 70): daluComponent_XMLǃdalu0ǃCOMPONENT
		getChildById(id: 'n64_awmy'): daluComponent_XMLǃdalu0ǃCOMPONENT
		getChild(name: 'dalu1'): daluComponent_XMLǃdalu1ǃCOMPONENT
		getChildAt(index: 71): daluComponent_XMLǃdalu1ǃCOMPONENT
		getChildById(id: 'n65_awmy'): daluComponent_XMLǃdalu1ǃCOMPONENT
		getChild(name: 'dalu2'): daluComponent_XMLǃdalu2ǃCOMPONENT
		getChildAt(index: 72): daluComponent_XMLǃdalu2ǃCOMPONENT
		getChildById(id: 'n66_awmy'): daluComponent_XMLǃdalu2ǃCOMPONENT
		getChild(name: 'dalu3'): daluComponent_XMLǃdalu3ǃCOMPONENT
		getChildAt(index: 73): daluComponent_XMLǃdalu3ǃCOMPONENT
		getChildById(id: 'n67_awmy'): daluComponent_XMLǃdalu3ǃCOMPONENT
		getChild(name: 'dalu4'): daluComponent_XMLǃdalu4ǃCOMPONENT
		getChildAt(index: 74): daluComponent_XMLǃdalu4ǃCOMPONENT
		getChildById(id: 'n68_awmy'): daluComponent_XMLǃdalu4ǃCOMPONENT
		getChild(name: 'dalu5'): daluComponent_XMLǃdalu5ǃCOMPONENT
		getChildAt(index: 75): daluComponent_XMLǃdalu5ǃCOMPONENT
		getChildById(id: 'n69_awmy'): daluComponent_XMLǃdalu5ǃCOMPONENT
		getChild(name: 'dalu6'): daluComponent_XMLǃdalu6ǃCOMPONENT
		getChildAt(index: 76): daluComponent_XMLǃdalu6ǃCOMPONENT
		getChildById(id: 'n70_awmy'): daluComponent_XMLǃdalu6ǃCOMPONENT
		getChild(name: 'dalu7'): daluComponent_XMLǃdalu7ǃCOMPONENT
		getChildAt(index: 77): daluComponent_XMLǃdalu7ǃCOMPONENT
		getChildById(id: 'n71_awmy'): daluComponent_XMLǃdalu7ǃCOMPONENT
		getChild(name: 'dalu8'): daluComponent_XMLǃdalu8ǃCOMPONENT
		getChildAt(index: 78): daluComponent_XMLǃdalu8ǃCOMPONENT
		getChildById(id: 'n72_awmy'): daluComponent_XMLǃdalu8ǃCOMPONENT
		getChild(name: 'dalu9'): daluComponent_XMLǃdalu9ǃCOMPONENT
		getChildAt(index: 79): daluComponent_XMLǃdalu9ǃCOMPONENT
		getChildById(id: 'n73_awmy'): daluComponent_XMLǃdalu9ǃCOMPONENT
		getChild(name: 'dalu10'): daluComponent_XMLǃdalu10ǃCOMPONENT
		getChildAt(index: 80): daluComponent_XMLǃdalu10ǃCOMPONENT
		getChildById(id: 'n74_awmy'): daluComponent_XMLǃdalu10ǃCOMPONENT
		getChild(name: 'dalu11'): daluComponent_XMLǃdalu11ǃCOMPONENT
		getChildAt(index: 81): daluComponent_XMLǃdalu11ǃCOMPONENT
		getChildById(id: 'n75_awmy'): daluComponent_XMLǃdalu11ǃCOMPONENT
		getChild(name: 'dalu12'): daluComponent_XMLǃdalu12ǃCOMPONENT
		getChildAt(index: 82): daluComponent_XMLǃdalu12ǃCOMPONENT
		getChildById(id: 'n76_awmy'): daluComponent_XMLǃdalu12ǃCOMPONENT
		getChild(name: 'dalu13'): daluComponent_XMLǃdalu13ǃCOMPONENT
		getChildAt(index: 83): daluComponent_XMLǃdalu13ǃCOMPONENT
		getChildById(id: 'n77_awmy'): daluComponent_XMLǃdalu13ǃCOMPONENT
		getChild(name: 'dalu14'): daluComponent_XMLǃdalu14ǃCOMPONENT
		getChildAt(index: 84): daluComponent_XMLǃdalu14ǃCOMPONENT
		getChildById(id: 'n78_awmy'): daluComponent_XMLǃdalu14ǃCOMPONENT
		getChild(name: 'dalu15'): daluComponent_XMLǃdalu15ǃCOMPONENT
		getChildAt(index: 85): daluComponent_XMLǃdalu15ǃCOMPONENT
		getChildById(id: 'n79_awmy'): daluComponent_XMLǃdalu15ǃCOMPONENT
		getChild(name: 'dalu16'): daluComponent_XMLǃdalu16ǃCOMPONENT
		getChildAt(index: 86): daluComponent_XMLǃdalu16ǃCOMPONENT
		getChildById(id: 'n80_awmy'): daluComponent_XMLǃdalu16ǃCOMPONENT
		getChild(name: 'dalu17'): daluComponent_XMLǃdalu17ǃCOMPONENT
		getChildAt(index: 87): daluComponent_XMLǃdalu17ǃCOMPONENT
		getChildById(id: 'n81_awmy'): daluComponent_XMLǃdalu17ǃCOMPONENT
		getChild(name: 'dalu18'): daluComponent_XMLǃdalu18ǃCOMPONENT
		getChildAt(index: 88): daluComponent_XMLǃdalu18ǃCOMPONENT
		getChildById(id: 'n82_awmy'): daluComponent_XMLǃdalu18ǃCOMPONENT
		getChild(name: 'dalu19'): daluComponent_XMLǃdalu19ǃCOMPONENT
		getChildAt(index: 89): daluComponent_XMLǃdalu19ǃCOMPONENT
		getChildById(id: 'n83_awmy'): daluComponent_XMLǃdalu19ǃCOMPONENT
		getChild(name: 'dalu20'): daluComponent_XMLǃdalu20ǃCOMPONENT
		getChildAt(index: 90): daluComponent_XMLǃdalu20ǃCOMPONENT
		getChildById(id: 'n84_awmy'): daluComponent_XMLǃdalu20ǃCOMPONENT
		getChild(name: 'dalu21'): daluComponent_XMLǃdalu21ǃCOMPONENT
		getChildAt(index: 91): daluComponent_XMLǃdalu21ǃCOMPONENT
		getChildById(id: 'n85_awmy'): daluComponent_XMLǃdalu21ǃCOMPONENT
		getChild(name: 'dalu22'): daluComponent_XMLǃdalu22ǃCOMPONENT
		getChildAt(index: 92): daluComponent_XMLǃdalu22ǃCOMPONENT
		getChildById(id: 'n86_awmy'): daluComponent_XMLǃdalu22ǃCOMPONENT
		getChild(name: 'dalu23'): daluComponent_XMLǃdalu23ǃCOMPONENT
		getChildAt(index: 93): daluComponent_XMLǃdalu23ǃCOMPONENT
		getChildById(id: 'n87_awmy'): daluComponent_XMLǃdalu23ǃCOMPONENT
		getChild(name: 'dalu24'): daluComponent_XMLǃdalu24ǃCOMPONENT
		getChildAt(index: 94): daluComponent_XMLǃdalu24ǃCOMPONENT
		getChildById(id: 'n88_awmy'): daluComponent_XMLǃdalu24ǃCOMPONENT
		getChild(name: 'dalu25'): daluComponent_XMLǃdalu25ǃCOMPONENT
		getChildAt(index: 95): daluComponent_XMLǃdalu25ǃCOMPONENT
		getChildById(id: 'n89_awmy'): daluComponent_XMLǃdalu25ǃCOMPONENT
		getChild(name: 'dalu26'): daluComponent_XMLǃdalu26ǃCOMPONENT
		getChildAt(index: 96): daluComponent_XMLǃdalu26ǃCOMPONENT
		getChildById(id: 'n90_awmy'): daluComponent_XMLǃdalu26ǃCOMPONENT
		getChild(name: 'dalu27'): daluComponent_XMLǃdalu27ǃCOMPONENT
		getChildAt(index: 97): daluComponent_XMLǃdalu27ǃCOMPONENT
		getChildById(id: 'n91_awmy'): daluComponent_XMLǃdalu27ǃCOMPONENT
		getChild(name: 'dalu28'): daluComponent_XMLǃdalu28ǃCOMPONENT
		getChildAt(index: 98): daluComponent_XMLǃdalu28ǃCOMPONENT
		getChildById(id: 'n92_awmy'): daluComponent_XMLǃdalu28ǃCOMPONENT
		getChild(name: 'dalu29'): daluComponent_XMLǃdalu29ǃCOMPONENT
		getChildAt(index: 99): daluComponent_XMLǃdalu29ǃCOMPONENT
		getChildById(id: 'n93_awmy'): daluComponent_XMLǃdalu29ǃCOMPONENT
		getChild(name: 'dalu30'): daluComponent_XMLǃdalu30ǃCOMPONENT
		getChildAt(index: 100): daluComponent_XMLǃdalu30ǃCOMPONENT
		getChildById(id: 'n94_awmy'): daluComponent_XMLǃdalu30ǃCOMPONENT
		getChild(name: 'dalu31'): daluComponent_XMLǃdalu31ǃCOMPONENT
		getChildAt(index: 101): daluComponent_XMLǃdalu31ǃCOMPONENT
		getChildById(id: 'n95_awmy'): daluComponent_XMLǃdalu31ǃCOMPONENT
		getChild(name: 'dalu32'): daluComponent_XMLǃdalu32ǃCOMPONENT
		getChildAt(index: 102): daluComponent_XMLǃdalu32ǃCOMPONENT
		getChildById(id: 'n96_awmy'): daluComponent_XMLǃdalu32ǃCOMPONENT
		getChild(name: 'dalu33'): daluComponent_XMLǃdalu33ǃCOMPONENT
		getChildAt(index: 103): daluComponent_XMLǃdalu33ǃCOMPONENT
		getChildById(id: 'n97_awmy'): daluComponent_XMLǃdalu33ǃCOMPONENT
		getChild(name: 'dalu34'): daluComponent_XMLǃdalu34ǃCOMPONENT
		getChildAt(index: 104): daluComponent_XMLǃdalu34ǃCOMPONENT
		getChildById(id: 'n98_awmy'): daluComponent_XMLǃdalu34ǃCOMPONENT
		getChild(name: 'dalu35'): daluComponent_XMLǃdalu35ǃCOMPONENT
		getChildAt(index: 105): daluComponent_XMLǃdalu35ǃCOMPONENT
		getChildById(id: 'n99_awmy'): daluComponent_XMLǃdalu35ǃCOMPONENT
		getChild(name: 'dalu36'): daluComponent_XMLǃdalu36ǃCOMPONENT
		getChildAt(index: 106): daluComponent_XMLǃdalu36ǃCOMPONENT
		getChildById(id: 'n100_awmy'): daluComponent_XMLǃdalu36ǃCOMPONENT
		getChild(name: 'dalu37'): daluComponent_XMLǃdalu37ǃCOMPONENT
		getChildAt(index: 107): daluComponent_XMLǃdalu37ǃCOMPONENT
		getChildById(id: 'n101_awmy'): daluComponent_XMLǃdalu37ǃCOMPONENT
		getChild(name: 'dalu38'): daluComponent_XMLǃdalu38ǃCOMPONENT
		getChildAt(index: 108): daluComponent_XMLǃdalu38ǃCOMPONENT
		getChildById(id: 'n102_awmy'): daluComponent_XMLǃdalu38ǃCOMPONENT
		getChild(name: 'dalu39'): daluComponent_XMLǃdalu39ǃCOMPONENT
		getChildAt(index: 109): daluComponent_XMLǃdalu39ǃCOMPONENT
		getChildById(id: 'n103_awmy'): daluComponent_XMLǃdalu39ǃCOMPONENT
		getChild(name: 'dalu40'): daluComponent_XMLǃdalu40ǃCOMPONENT
		getChildAt(index: 110): daluComponent_XMLǃdalu40ǃCOMPONENT
		getChildById(id: 'n104_awmy'): daluComponent_XMLǃdalu40ǃCOMPONENT
		getChild(name: 'dalu41'): daluComponent_XMLǃdalu41ǃCOMPONENT
		getChildAt(index: 111): daluComponent_XMLǃdalu41ǃCOMPONENT
		getChildById(id: 'n105_awmy'): daluComponent_XMLǃdalu41ǃCOMPONENT
		getChild(name: 'dalu42'): daluComponent_XMLǃdalu42ǃCOMPONENT
		getChildAt(index: 112): daluComponent_XMLǃdalu42ǃCOMPONENT
		getChildById(id: 'n106_awmy'): daluComponent_XMLǃdalu42ǃCOMPONENT
		getChild(name: 'dalu43'): daluComponent_XMLǃdalu43ǃCOMPONENT
		getChildAt(index: 113): daluComponent_XMLǃdalu43ǃCOMPONENT
		getChildById(id: 'n107_awmy'): daluComponent_XMLǃdalu43ǃCOMPONENT
		getChild(name: 'dalu44'): daluComponent_XMLǃdalu44ǃCOMPONENT
		getChildAt(index: 114): daluComponent_XMLǃdalu44ǃCOMPONENT
		getChildById(id: 'n108_awmy'): daluComponent_XMLǃdalu44ǃCOMPONENT
		getChild(name: 'dalu45'): daluComponent_XMLǃdalu45ǃCOMPONENT
		getChildAt(index: 115): daluComponent_XMLǃdalu45ǃCOMPONENT
		getChildById(id: 'n109_awmy'): daluComponent_XMLǃdalu45ǃCOMPONENT
		getChild(name: 'dalu46'): daluComponent_XMLǃdalu46ǃCOMPONENT
		getChildAt(index: 116): daluComponent_XMLǃdalu46ǃCOMPONENT
		getChildById(id: 'n110_awmy'): daluComponent_XMLǃdalu46ǃCOMPONENT
		getChild(name: 'dalu47'): daluComponent_XMLǃdalu47ǃCOMPONENT
		getChildAt(index: 117): daluComponent_XMLǃdalu47ǃCOMPONENT
		getChildById(id: 'n111_awmy'): daluComponent_XMLǃdalu47ǃCOMPONENT
		getChild(name: 'dalu48'): daluComponent_XMLǃdalu48ǃCOMPONENT
		getChildAt(index: 118): daluComponent_XMLǃdalu48ǃCOMPONENT
		getChildById(id: 'n112_awmy'): daluComponent_XMLǃdalu48ǃCOMPONENT
		getChild(name: 'dalu49'): daluComponent_XMLǃdalu49ǃCOMPONENT
		getChildAt(index: 119): daluComponent_XMLǃdalu49ǃCOMPONENT
		getChildById(id: 'n113_awmy'): daluComponent_XMLǃdalu49ǃCOMPONENT
		getChild(name: 'dalu50'): daluComponent_XMLǃdalu50ǃCOMPONENT
		getChildAt(index: 120): daluComponent_XMLǃdalu50ǃCOMPONENT
		getChildById(id: 'n114_awmy'): daluComponent_XMLǃdalu50ǃCOMPONENT
		getChild(name: 'dalu51'): daluComponent_XMLǃdalu51ǃCOMPONENT
		getChildAt(index: 121): daluComponent_XMLǃdalu51ǃCOMPONENT
		getChildById(id: 'n115_awmy'): daluComponent_XMLǃdalu51ǃCOMPONENT
		getChild(name: 'dalu52'): daluComponent_XMLǃdalu52ǃCOMPONENT
		getChildAt(index: 122): daluComponent_XMLǃdalu52ǃCOMPONENT
		getChildById(id: 'n116_awmy'): daluComponent_XMLǃdalu52ǃCOMPONENT
		getChild(name: 'dalu53'): daluComponent_XMLǃdalu53ǃCOMPONENT
		getChildAt(index: 123): daluComponent_XMLǃdalu53ǃCOMPONENT
		getChildById(id: 'n117_awmy'): daluComponent_XMLǃdalu53ǃCOMPONENT
		getChild(name: 'dalu54'): daluComponent_XMLǃdalu54ǃCOMPONENT
		getChildAt(index: 124): daluComponent_XMLǃdalu54ǃCOMPONENT
		getChildById(id: 'n118_awmy'): daluComponent_XMLǃdalu54ǃCOMPONENT
		getChild(name: 'dalu55'): daluComponent_XMLǃdalu55ǃCOMPONENT
		getChildAt(index: 125): daluComponent_XMLǃdalu55ǃCOMPONENT
		getChildById(id: 'n119_awmy'): daluComponent_XMLǃdalu55ǃCOMPONENT
		getChild(name: 'dalu56'): daluComponent_XMLǃdalu56ǃCOMPONENT
		getChildAt(index: 126): daluComponent_XMLǃdalu56ǃCOMPONENT
		getChildById(id: 'n120_awmy'): daluComponent_XMLǃdalu56ǃCOMPONENT
		getChild(name: 'dalu57'): daluComponent_XMLǃdalu57ǃCOMPONENT
		getChildAt(index: 127): daluComponent_XMLǃdalu57ǃCOMPONENT
		getChildById(id: 'n121_awmy'): daluComponent_XMLǃdalu57ǃCOMPONENT
		getChild(name: 'dalu58'): daluComponent_XMLǃdalu58ǃCOMPONENT
		getChildAt(index: 128): daluComponent_XMLǃdalu58ǃCOMPONENT
		getChildById(id: 'n122_awmy'): daluComponent_XMLǃdalu58ǃCOMPONENT
		getChild(name: 'dalu59'): daluComponent_XMLǃdalu59ǃCOMPONENT
		getChildAt(index: 129): daluComponent_XMLǃdalu59ǃCOMPONENT
		getChildById(id: 'n123_awmy'): daluComponent_XMLǃdalu59ǃCOMPONENT
		getChild(name: 'dalu60'): daluComponent_XMLǃdalu60ǃCOMPONENT
		getChildAt(index: 130): daluComponent_XMLǃdalu60ǃCOMPONENT
		getChildById(id: 'n124_awmy'): daluComponent_XMLǃdalu60ǃCOMPONENT
		getChild(name: 'dalu61'): daluComponent_XMLǃdalu61ǃCOMPONENT
		getChildAt(index: 131): daluComponent_XMLǃdalu61ǃCOMPONENT
		getChildById(id: 'n125_awmy'): daluComponent_XMLǃdalu61ǃCOMPONENT
		getChild(name: 'dalu62'): daluComponent_XMLǃdalu62ǃCOMPONENT
		getChildAt(index: 132): daluComponent_XMLǃdalu62ǃCOMPONENT
		getChildById(id: 'n126_awmy'): daluComponent_XMLǃdalu62ǃCOMPONENT
		getChild(name: 'dalu63'): daluComponent_XMLǃdalu63ǃCOMPONENT
		getChildAt(index: 133): daluComponent_XMLǃdalu63ǃCOMPONENT
		getChildById(id: 'n127_awmy'): daluComponent_XMLǃdalu63ǃCOMPONENT
		getChild(name: 'dalu64'): daluComponent_XMLǃdalu64ǃCOMPONENT
		getChildAt(index: 134): daluComponent_XMLǃdalu64ǃCOMPONENT
		getChildById(id: 'n128_awmy'): daluComponent_XMLǃdalu64ǃCOMPONENT
		getChild(name: 'dalu65'): daluComponent_XMLǃdalu65ǃCOMPONENT
		getChildAt(index: 135): daluComponent_XMLǃdalu65ǃCOMPONENT
		getChildById(id: 'n129_awmy'): daluComponent_XMLǃdalu65ǃCOMPONENT
		getChild(name: 'dalu66'): daluComponent_XMLǃdalu66ǃCOMPONENT
		getChildAt(index: 136): daluComponent_XMLǃdalu66ǃCOMPONENT
		getChildById(id: 'n130_awmy'): daluComponent_XMLǃdalu66ǃCOMPONENT
		getChild(name: 'dalu67'): daluComponent_XMLǃdalu67ǃCOMPONENT
		getChildAt(index: 137): daluComponent_XMLǃdalu67ǃCOMPONENT
		getChildById(id: 'n131_awmy'): daluComponent_XMLǃdalu67ǃCOMPONENT
		getChild(name: 'dalu68'): daluComponent_XMLǃdalu68ǃCOMPONENT
		getChildAt(index: 138): daluComponent_XMLǃdalu68ǃCOMPONENT
		getChildById(id: 'n132_awmy'): daluComponent_XMLǃdalu68ǃCOMPONENT
		getChild(name: 'dalu69'): daluComponent_XMLǃdalu69ǃCOMPONENT
		getChildAt(index: 139): daluComponent_XMLǃdalu69ǃCOMPONENT
		getChildById(id: 'n133_awmy'): daluComponent_XMLǃdalu69ǃCOMPONENT
		getChild(name: 'dalu70'): daluComponent_XMLǃdalu70ǃCOMPONENT
		getChildAt(index: 140): daluComponent_XMLǃdalu70ǃCOMPONENT
		getChildById(id: 'n134_awmy'): daluComponent_XMLǃdalu70ǃCOMPONENT
		getChild(name: 'dalu71'): daluComponent_XMLǃdalu71ǃCOMPONENT
		getChildAt(index: 141): daluComponent_XMLǃdalu71ǃCOMPONENT
		getChildById(id: 'n135_awmy'): daluComponent_XMLǃdalu71ǃCOMPONENT
		getChild(name: 'dalu72'): daluComponent_XMLǃdalu72ǃCOMPONENT
		getChildAt(index: 142): daluComponent_XMLǃdalu72ǃCOMPONENT
		getChildById(id: 'n136_awmy'): daluComponent_XMLǃdalu72ǃCOMPONENT
		getChild(name: 'dalu73'): daluComponent_XMLǃdalu73ǃCOMPONENT
		getChildAt(index: 143): daluComponent_XMLǃdalu73ǃCOMPONENT
		getChildById(id: 'n137_awmy'): daluComponent_XMLǃdalu73ǃCOMPONENT
		getChild(name: 'dalu74'): daluComponent_XMLǃdalu74ǃCOMPONENT
		getChildAt(index: 144): daluComponent_XMLǃdalu74ǃCOMPONENT
		getChildById(id: 'n138_awmy'): daluComponent_XMLǃdalu74ǃCOMPONENT
		getChild(name: 'dalu75'): daluComponent_XMLǃdalu75ǃCOMPONENT
		getChildAt(index: 145): daluComponent_XMLǃdalu75ǃCOMPONENT
		getChildById(id: 'n139_awmy'): daluComponent_XMLǃdalu75ǃCOMPONENT
		getChild(name: 'dalu76'): daluComponent_XMLǃdalu76ǃCOMPONENT
		getChildAt(index: 146): daluComponent_XMLǃdalu76ǃCOMPONENT
		getChildById(id: 'n140_awmy'): daluComponent_XMLǃdalu76ǃCOMPONENT
		getChild(name: 'dalu77'): daluComponent_XMLǃdalu77ǃCOMPONENT
		getChildAt(index: 147): daluComponent_XMLǃdalu77ǃCOMPONENT
		getChildById(id: 'n141_awmy'): daluComponent_XMLǃdalu77ǃCOMPONENT
		getChild(name: 'dalu78'): daluComponent_XMLǃdalu78ǃCOMPONENT
		getChildAt(index: 148): daluComponent_XMLǃdalu78ǃCOMPONENT
		getChildById(id: 'n142_awmy'): daluComponent_XMLǃdalu78ǃCOMPONENT
		getChild(name: 'dalu79'): daluComponent_XMLǃdalu79ǃCOMPONENT
		getChildAt(index: 149): daluComponent_XMLǃdalu79ǃCOMPONENT
		getChildById(id: 'n143_awmy'): daluComponent_XMLǃdalu79ǃCOMPONENT
		getChild(name: 'dalu80'): daluComponent_XMLǃdalu80ǃCOMPONENT
		getChildAt(index: 150): daluComponent_XMLǃdalu80ǃCOMPONENT
		getChildById(id: 'n144_awmy'): daluComponent_XMLǃdalu80ǃCOMPONENT
		getChild(name: 'dalu81'): daluComponent_XMLǃdalu81ǃCOMPONENT
		getChildAt(index: 151): daluComponent_XMLǃdalu81ǃCOMPONENT
		getChildById(id: 'n145_awmy'): daluComponent_XMLǃdalu81ǃCOMPONENT
		getChild(name: 'dalu82'): daluComponent_XMLǃdalu82ǃCOMPONENT
		getChildAt(index: 152): daluComponent_XMLǃdalu82ǃCOMPONENT
		getChildById(id: 'n146_awmy'): daluComponent_XMLǃdalu82ǃCOMPONENT
		getChild(name: 'dalu83'): daluComponent_XMLǃdalu83ǃCOMPONENT
		getChildAt(index: 153): daluComponent_XMLǃdalu83ǃCOMPONENT
		getChildById(id: 'n147_awmy'): daluComponent_XMLǃdalu83ǃCOMPONENT
		getChild(name: 'dalu84'): daluComponent_XMLǃdalu84ǃCOMPONENT
		getChildAt(index: 154): daluComponent_XMLǃdalu84ǃCOMPONENT
		getChildById(id: 'n148_awmy'): daluComponent_XMLǃdalu84ǃCOMPONENT
		getChild(name: 'dalu85'): daluComponent_XMLǃdalu85ǃCOMPONENT
		getChildAt(index: 155): daluComponent_XMLǃdalu85ǃCOMPONENT
		getChildById(id: 'n149_awmy'): daluComponent_XMLǃdalu85ǃCOMPONENT
		getChild(name: 'dalu86'): daluComponent_XMLǃdalu86ǃCOMPONENT
		getChildAt(index: 156): daluComponent_XMLǃdalu86ǃCOMPONENT
		getChildById(id: 'n150_awmy'): daluComponent_XMLǃdalu86ǃCOMPONENT
		getChild(name: 'dalu87'): daluComponent_XMLǃdalu87ǃCOMPONENT
		getChildAt(index: 157): daluComponent_XMLǃdalu87ǃCOMPONENT
		getChildById(id: 'n151_awmy'): daluComponent_XMLǃdalu87ǃCOMPONENT
		getChild(name: 'dalu88'): daluComponent_XMLǃdalu88ǃCOMPONENT
		getChildAt(index: 158): daluComponent_XMLǃdalu88ǃCOMPONENT
		getChildById(id: 'n152_awmy'): daluComponent_XMLǃdalu88ǃCOMPONENT
		getChild(name: 'dalu89'): daluComponent_XMLǃdalu89ǃCOMPONENT
		getChildAt(index: 159): daluComponent_XMLǃdalu89ǃCOMPONENT
		getChildById(id: 'n153_awmy'): daluComponent_XMLǃdalu89ǃCOMPONENT
		getChild(name: 'dalu90'): daluComponent_XMLǃdalu90ǃCOMPONENT
		getChildAt(index: 160): daluComponent_XMLǃdalu90ǃCOMPONENT
		getChildById(id: 'n154_awmy'): daluComponent_XMLǃdalu90ǃCOMPONENT
		getChild(name: 'dalu91'): daluComponent_XMLǃdalu91ǃCOMPONENT
		getChildAt(index: 161): daluComponent_XMLǃdalu91ǃCOMPONENT
		getChildById(id: 'n155_awmy'): daluComponent_XMLǃdalu91ǃCOMPONENT
		getChild(name: 'dalu92'): daluComponent_XMLǃdalu92ǃCOMPONENT
		getChildAt(index: 162): daluComponent_XMLǃdalu92ǃCOMPONENT
		getChildById(id: 'n156_awmy'): daluComponent_XMLǃdalu92ǃCOMPONENT
		getChild(name: 'dalu93'): daluComponent_XMLǃdalu93ǃCOMPONENT
		getChildAt(index: 163): daluComponent_XMLǃdalu93ǃCOMPONENT
		getChildById(id: 'n157_awmy'): daluComponent_XMLǃdalu93ǃCOMPONENT
		getChild(name: 'dalu94'): daluComponent_XMLǃdalu94ǃCOMPONENT
		getChildAt(index: 164): daluComponent_XMLǃdalu94ǃCOMPONENT
		getChildById(id: 'n158_awmy'): daluComponent_XMLǃdalu94ǃCOMPONENT
		getChild(name: 'dalu95'): daluComponent_XMLǃdalu95ǃCOMPONENT
		getChildAt(index: 165): daluComponent_XMLǃdalu95ǃCOMPONENT
		getChildById(id: 'n159_awmy'): daluComponent_XMLǃdalu95ǃCOMPONENT
		getChild(name: 'dalu96'): daluComponent_XMLǃdalu96ǃCOMPONENT
		getChildAt(index: 166): daluComponent_XMLǃdalu96ǃCOMPONENT
		getChildById(id: 'n160_awmy'): daluComponent_XMLǃdalu96ǃCOMPONENT
		getChild(name: 'dalu97'): daluComponent_XMLǃdalu97ǃCOMPONENT
		getChildAt(index: 167): daluComponent_XMLǃdalu97ǃCOMPONENT
		getChildById(id: 'n161_awmy'): daluComponent_XMLǃdalu97ǃCOMPONENT
		getChild(name: 'dalu98'): daluComponent_XMLǃdalu98ǃCOMPONENT
		getChildAt(index: 168): daluComponent_XMLǃdalu98ǃCOMPONENT
		getChildById(id: 'n162_awmy'): daluComponent_XMLǃdalu98ǃCOMPONENT
		getChild(name: 'dalu99'): daluComponent_XMLǃdalu99ǃCOMPONENT
		getChildAt(index: 169): daluComponent_XMLǃdalu99ǃCOMPONENT
		getChildById(id: 'n163_awmy'): daluComponent_XMLǃdalu99ǃCOMPONENT
		getChild(name: 'dalu100'): daluComponent_XMLǃdalu100ǃCOMPONENT
		getChildAt(index: 170): daluComponent_XMLǃdalu100ǃCOMPONENT
		getChildById(id: 'n164_awmy'): daluComponent_XMLǃdalu100ǃCOMPONENT
		getChild(name: 'dalu101'): daluComponent_XMLǃdalu101ǃCOMPONENT
		getChildAt(index: 171): daluComponent_XMLǃdalu101ǃCOMPONENT
		getChildById(id: 'n165_awmy'): daluComponent_XMLǃdalu101ǃCOMPONENT
		getChild(name: 'dalu102'): daluComponent_XMLǃdalu102ǃCOMPONENT
		getChildAt(index: 172): daluComponent_XMLǃdalu102ǃCOMPONENT
		getChildById(id: 'n166_awmy'): daluComponent_XMLǃdalu102ǃCOMPONENT
		getChild(name: 'dalu103'): daluComponent_XMLǃdalu103ǃCOMPONENT
		getChildAt(index: 173): daluComponent_XMLǃdalu103ǃCOMPONENT
		getChildById(id: 'n167_awmy'): daluComponent_XMLǃdalu103ǃCOMPONENT
		getChild(name: 'dalu104'): daluComponent_XMLǃdalu104ǃCOMPONENT
		getChildAt(index: 174): daluComponent_XMLǃdalu104ǃCOMPONENT
		getChildById(id: 'n168_awmy'): daluComponent_XMLǃdalu104ǃCOMPONENT
		getChild(name: 'dalu105'): daluComponent_XMLǃdalu105ǃCOMPONENT
		getChildAt(index: 175): daluComponent_XMLǃdalu105ǃCOMPONENT
		getChildById(id: 'n169_awmy'): daluComponent_XMLǃdalu105ǃCOMPONENT
		getChild(name: 'dalu106'): daluComponent_XMLǃdalu106ǃCOMPONENT
		getChildAt(index: 176): daluComponent_XMLǃdalu106ǃCOMPONENT
		getChildById(id: 'n170_awmy'): daluComponent_XMLǃdalu106ǃCOMPONENT
		getChild(name: 'dalu107'): daluComponent_XMLǃdalu107ǃCOMPONENT
		getChildAt(index: 177): daluComponent_XMLǃdalu107ǃCOMPONENT
		getChildById(id: 'n171_awmy'): daluComponent_XMLǃdalu107ǃCOMPONENT
		getChild(name: 'dalu108'): daluComponent_XMLǃdalu108ǃCOMPONENT
		getChildAt(index: 178): daluComponent_XMLǃdalu108ǃCOMPONENT
		getChildById(id: 'n172_awmy'): daluComponent_XMLǃdalu108ǃCOMPONENT
		getChild(name: 'dalu109'): daluComponent_XMLǃdalu109ǃCOMPONENT
		getChildAt(index: 179): daluComponent_XMLǃdalu109ǃCOMPONENT
		getChildById(id: 'n173_awmy'): daluComponent_XMLǃdalu109ǃCOMPONENT
		getChild(name: 'dalu110'): daluComponent_XMLǃdalu110ǃCOMPONENT
		getChildAt(index: 180): daluComponent_XMLǃdalu110ǃCOMPONENT
		getChildById(id: 'n174_awmy'): daluComponent_XMLǃdalu110ǃCOMPONENT
		getChild(name: 'dalu111'): daluComponent_XMLǃdalu111ǃCOMPONENT
		getChildAt(index: 181): daluComponent_XMLǃdalu111ǃCOMPONENT
		getChildById(id: 'n175_awmy'): daluComponent_XMLǃdalu111ǃCOMPONENT
		getChild(name: 'dalu112'): daluComponent_XMLǃdalu112ǃCOMPONENT
		getChildAt(index: 182): daluComponent_XMLǃdalu112ǃCOMPONENT
		getChildById(id: 'n176_awmy'): daluComponent_XMLǃdalu112ǃCOMPONENT
		getChild(name: 'dalu113'): daluComponent_XMLǃdalu113ǃCOMPONENT
		getChildAt(index: 183): daluComponent_XMLǃdalu113ǃCOMPONENT
		getChildById(id: 'n177_awmy'): daluComponent_XMLǃdalu113ǃCOMPONENT
		getChild(name: 'dayanlu0'): newluzhitu_XMLǃdayanlu0ǃLOADER
		getChildAt(index: 184): newluzhitu_XMLǃdayanlu0ǃLOADER
		getChildById(id: 'n179_awmy'): newluzhitu_XMLǃdayanlu0ǃLOADER
		getChild(name: 'dayanlu1'): newluzhitu_XMLǃdayanlu1ǃLOADER
		getChildAt(index: 185): newluzhitu_XMLǃdayanlu1ǃLOADER
		getChildById(id: 'n180_awmy'): newluzhitu_XMLǃdayanlu1ǃLOADER
		getChild(name: 'dayanlu2'): newluzhitu_XMLǃdayanlu2ǃLOADER
		getChildAt(index: 186): newluzhitu_XMLǃdayanlu2ǃLOADER
		getChildById(id: 'n181_awmy'): newluzhitu_XMLǃdayanlu2ǃLOADER
		getChild(name: 'dayanlu3'): newluzhitu_XMLǃdayanlu3ǃLOADER
		getChildAt(index: 187): newluzhitu_XMLǃdayanlu3ǃLOADER
		getChildById(id: 'n182_awmy'): newluzhitu_XMLǃdayanlu3ǃLOADER
		getChild(name: 'dayanlu4'): newluzhitu_XMLǃdayanlu4ǃLOADER
		getChildAt(index: 188): newluzhitu_XMLǃdayanlu4ǃLOADER
		getChildById(id: 'n183_awmy'): newluzhitu_XMLǃdayanlu4ǃLOADER
		getChild(name: 'dayanlu5'): newluzhitu_XMLǃdayanlu5ǃLOADER
		getChildAt(index: 189): newluzhitu_XMLǃdayanlu5ǃLOADER
		getChildById(id: 'n184_awmy'): newluzhitu_XMLǃdayanlu5ǃLOADER
		getChild(name: 'dayanlu6'): newluzhitu_XMLǃdayanlu6ǃLOADER
		getChildAt(index: 190): newluzhitu_XMLǃdayanlu6ǃLOADER
		getChildById(id: 'n185_awmy'): newluzhitu_XMLǃdayanlu6ǃLOADER
		getChild(name: 'dayanlu7'): newluzhitu_XMLǃdayanlu7ǃLOADER
		getChildAt(index: 191): newluzhitu_XMLǃdayanlu7ǃLOADER
		getChildById(id: 'n186_awmy'): newluzhitu_XMLǃdayanlu7ǃLOADER
		getChild(name: 'dayanlu8'): newluzhitu_XMLǃdayanlu8ǃLOADER
		getChildAt(index: 192): newluzhitu_XMLǃdayanlu8ǃLOADER
		getChildById(id: 'n187_awmy'): newluzhitu_XMLǃdayanlu8ǃLOADER
		getChild(name: 'dayanlu9'): newluzhitu_XMLǃdayanlu9ǃLOADER
		getChildAt(index: 193): newluzhitu_XMLǃdayanlu9ǃLOADER
		getChildById(id: 'n188_awmy'): newluzhitu_XMLǃdayanlu9ǃLOADER
		getChild(name: 'dayanlu10'): newluzhitu_XMLǃdayanlu10ǃLOADER
		getChildAt(index: 194): newluzhitu_XMLǃdayanlu10ǃLOADER
		getChildById(id: 'n189_awmy'): newluzhitu_XMLǃdayanlu10ǃLOADER
		getChild(name: 'dayanlu11'): newluzhitu_XMLǃdayanlu11ǃLOADER
		getChildAt(index: 195): newluzhitu_XMLǃdayanlu11ǃLOADER
		getChildById(id: 'n190_awmy'): newluzhitu_XMLǃdayanlu11ǃLOADER
		getChild(name: 'dayanlu12'): newluzhitu_XMLǃdayanlu12ǃLOADER
		getChildAt(index: 196): newluzhitu_XMLǃdayanlu12ǃLOADER
		getChildById(id: 'n191_awmy'): newluzhitu_XMLǃdayanlu12ǃLOADER
		getChild(name: 'dayanlu13'): newluzhitu_XMLǃdayanlu13ǃLOADER
		getChildAt(index: 197): newluzhitu_XMLǃdayanlu13ǃLOADER
		getChildById(id: 'n192_awmy'): newluzhitu_XMLǃdayanlu13ǃLOADER
		getChild(name: 'dayanlu14'): newluzhitu_XMLǃdayanlu14ǃLOADER
		getChildAt(index: 198): newluzhitu_XMLǃdayanlu14ǃLOADER
		getChildById(id: 'n193_awmy'): newluzhitu_XMLǃdayanlu14ǃLOADER
		getChild(name: 'dayanlu15'): newluzhitu_XMLǃdayanlu15ǃLOADER
		getChildAt(index: 199): newluzhitu_XMLǃdayanlu15ǃLOADER
		getChildById(id: 'n194_awmy'): newluzhitu_XMLǃdayanlu15ǃLOADER
		getChild(name: 'dayanlu16'): newluzhitu_XMLǃdayanlu16ǃLOADER
		getChildAt(index: 200): newluzhitu_XMLǃdayanlu16ǃLOADER
		getChildById(id: 'n195_awmy'): newluzhitu_XMLǃdayanlu16ǃLOADER
		getChild(name: 'dayanlu17'): newluzhitu_XMLǃdayanlu17ǃLOADER
		getChildAt(index: 201): newluzhitu_XMLǃdayanlu17ǃLOADER
		getChildById(id: 'n196_awmy'): newluzhitu_XMLǃdayanlu17ǃLOADER
		getChild(name: 'dayanlu18'): newluzhitu_XMLǃdayanlu18ǃLOADER
		getChildAt(index: 202): newluzhitu_XMLǃdayanlu18ǃLOADER
		getChildById(id: 'n197_awmy'): newluzhitu_XMLǃdayanlu18ǃLOADER
		getChild(name: 'dayanlu19'): newluzhitu_XMLǃdayanlu19ǃLOADER
		getChildAt(index: 203): newluzhitu_XMLǃdayanlu19ǃLOADER
		getChildById(id: 'n198_awmy'): newluzhitu_XMLǃdayanlu19ǃLOADER
		getChild(name: 'dayanlu20'): newluzhitu_XMLǃdayanlu20ǃLOADER
		getChildAt(index: 204): newluzhitu_XMLǃdayanlu20ǃLOADER
		getChildById(id: 'n199_awmy'): newluzhitu_XMLǃdayanlu20ǃLOADER
		getChild(name: 'dayanlu21'): newluzhitu_XMLǃdayanlu21ǃLOADER
		getChildAt(index: 205): newluzhitu_XMLǃdayanlu21ǃLOADER
		getChildById(id: 'n200_awmy'): newluzhitu_XMLǃdayanlu21ǃLOADER
		getChild(name: 'dayanlu22'): newluzhitu_XMLǃdayanlu22ǃLOADER
		getChildAt(index: 206): newluzhitu_XMLǃdayanlu22ǃLOADER
		getChildById(id: 'n201_awmy'): newluzhitu_XMLǃdayanlu22ǃLOADER
		getChild(name: 'dayanlu23'): newluzhitu_XMLǃdayanlu23ǃLOADER
		getChildAt(index: 207): newluzhitu_XMLǃdayanlu23ǃLOADER
		getChildById(id: 'n202_awmy'): newluzhitu_XMLǃdayanlu23ǃLOADER
		getChild(name: 'dayanlu24'): newluzhitu_XMLǃdayanlu24ǃLOADER
		getChildAt(index: 208): newluzhitu_XMLǃdayanlu24ǃLOADER
		getChildById(id: 'n203_awmy'): newluzhitu_XMLǃdayanlu24ǃLOADER
		getChild(name: 'dayanlu25'): newluzhitu_XMLǃdayanlu25ǃLOADER
		getChildAt(index: 209): newluzhitu_XMLǃdayanlu25ǃLOADER
		getChildById(id: 'n204_awmy'): newluzhitu_XMLǃdayanlu25ǃLOADER
		getChild(name: 'dayanlu26'): newluzhitu_XMLǃdayanlu26ǃLOADER
		getChildAt(index: 210): newluzhitu_XMLǃdayanlu26ǃLOADER
		getChildById(id: 'n205_awmy'): newluzhitu_XMLǃdayanlu26ǃLOADER
		getChild(name: 'dayanlu27'): newluzhitu_XMLǃdayanlu27ǃLOADER
		getChildAt(index: 211): newluzhitu_XMLǃdayanlu27ǃLOADER
		getChildById(id: 'n206_awmy'): newluzhitu_XMLǃdayanlu27ǃLOADER
		getChild(name: 'dayanlu28'): newluzhitu_XMLǃdayanlu28ǃLOADER
		getChildAt(index: 212): newluzhitu_XMLǃdayanlu28ǃLOADER
		getChildById(id: 'n207_awmy'): newluzhitu_XMLǃdayanlu28ǃLOADER
		getChild(name: 'dayanlu29'): newluzhitu_XMLǃdayanlu29ǃLOADER
		getChildAt(index: 213): newluzhitu_XMLǃdayanlu29ǃLOADER
		getChildById(id: 'n208_awmy'): newluzhitu_XMLǃdayanlu29ǃLOADER
		getChild(name: 'dayanlu30'): newluzhitu_XMLǃdayanlu30ǃLOADER
		getChildAt(index: 214): newluzhitu_XMLǃdayanlu30ǃLOADER
		getChildById(id: 'n209_awmy'): newluzhitu_XMLǃdayanlu30ǃLOADER
		getChild(name: 'dayanlu31'): newluzhitu_XMLǃdayanlu31ǃLOADER
		getChildAt(index: 215): newluzhitu_XMLǃdayanlu31ǃLOADER
		getChildById(id: 'n210_awmy'): newluzhitu_XMLǃdayanlu31ǃLOADER
		getChild(name: 'dayanlu32'): newluzhitu_XMLǃdayanlu32ǃLOADER
		getChildAt(index: 216): newluzhitu_XMLǃdayanlu32ǃLOADER
		getChildById(id: 'n211_awmy'): newluzhitu_XMLǃdayanlu32ǃLOADER
		getChild(name: 'dayanlu33'): newluzhitu_XMLǃdayanlu33ǃLOADER
		getChildAt(index: 217): newluzhitu_XMLǃdayanlu33ǃLOADER
		getChildById(id: 'n212_awmy'): newluzhitu_XMLǃdayanlu33ǃLOADER
		getChild(name: 'dayanlu34'): newluzhitu_XMLǃdayanlu34ǃLOADER
		getChildAt(index: 218): newluzhitu_XMLǃdayanlu34ǃLOADER
		getChildById(id: 'n213_awmy'): newluzhitu_XMLǃdayanlu34ǃLOADER
		getChild(name: 'dayanlu35'): newluzhitu_XMLǃdayanlu35ǃLOADER
		getChildAt(index: 219): newluzhitu_XMLǃdayanlu35ǃLOADER
		getChildById(id: 'n214_awmy'): newluzhitu_XMLǃdayanlu35ǃLOADER
		getChild(name: 'dayanlu36'): newluzhitu_XMLǃdayanlu36ǃLOADER
		getChildAt(index: 220): newluzhitu_XMLǃdayanlu36ǃLOADER
		getChildById(id: 'n215_awmy'): newluzhitu_XMLǃdayanlu36ǃLOADER
		getChild(name: 'dayanlu37'): newluzhitu_XMLǃdayanlu37ǃLOADER
		getChildAt(index: 221): newluzhitu_XMLǃdayanlu37ǃLOADER
		getChildById(id: 'n216_awmy'): newluzhitu_XMLǃdayanlu37ǃLOADER
		getChild(name: 'dayanlu38'): newluzhitu_XMLǃdayanlu38ǃLOADER
		getChildAt(index: 222): newluzhitu_XMLǃdayanlu38ǃLOADER
		getChildById(id: 'n217_awmy'): newluzhitu_XMLǃdayanlu38ǃLOADER
		getChild(name: 'dayanlu39'): newluzhitu_XMLǃdayanlu39ǃLOADER
		getChildAt(index: 223): newluzhitu_XMLǃdayanlu39ǃLOADER
		getChildById(id: 'n218_awmy'): newluzhitu_XMLǃdayanlu39ǃLOADER
		getChild(name: 'dayanlu40'): newluzhitu_XMLǃdayanlu40ǃLOADER
		getChildAt(index: 224): newluzhitu_XMLǃdayanlu40ǃLOADER
		getChildById(id: 'n219_awmy'): newluzhitu_XMLǃdayanlu40ǃLOADER
		getChild(name: 'dayanlu41'): newluzhitu_XMLǃdayanlu41ǃLOADER
		getChildAt(index: 225): newluzhitu_XMLǃdayanlu41ǃLOADER
		getChildById(id: 'n220_awmy'): newluzhitu_XMLǃdayanlu41ǃLOADER
		getChild(name: 'dayanlu42'): newluzhitu_XMLǃdayanlu42ǃLOADER
		getChildAt(index: 226): newluzhitu_XMLǃdayanlu42ǃLOADER
		getChildById(id: 'n221_awmy'): newluzhitu_XMLǃdayanlu42ǃLOADER
		getChild(name: 'dayanlu43'): newluzhitu_XMLǃdayanlu43ǃLOADER
		getChildAt(index: 227): newluzhitu_XMLǃdayanlu43ǃLOADER
		getChildById(id: 'n222_awmy'): newluzhitu_XMLǃdayanlu43ǃLOADER
		getChild(name: 'dayanlu44'): newluzhitu_XMLǃdayanlu44ǃLOADER
		getChildAt(index: 228): newluzhitu_XMLǃdayanlu44ǃLOADER
		getChildById(id: 'n223_awmy'): newluzhitu_XMLǃdayanlu44ǃLOADER
		getChild(name: 'dayanlu45'): newluzhitu_XMLǃdayanlu45ǃLOADER
		getChildAt(index: 229): newluzhitu_XMLǃdayanlu45ǃLOADER
		getChildById(id: 'n224_awmy'): newluzhitu_XMLǃdayanlu45ǃLOADER
		getChild(name: 'dayanlu46'): newluzhitu_XMLǃdayanlu46ǃLOADER
		getChildAt(index: 230): newluzhitu_XMLǃdayanlu46ǃLOADER
		getChildById(id: 'n225_awmy'): newluzhitu_XMLǃdayanlu46ǃLOADER
		getChild(name: 'dayanlu47'): newluzhitu_XMLǃdayanlu47ǃLOADER
		getChildAt(index: 231): newluzhitu_XMLǃdayanlu47ǃLOADER
		getChildById(id: 'n226_awmy'): newluzhitu_XMLǃdayanlu47ǃLOADER
		getChild(name: 'dayanlu48'): newluzhitu_XMLǃdayanlu48ǃLOADER
		getChildAt(index: 232): newluzhitu_XMLǃdayanlu48ǃLOADER
		getChildById(id: 'n227_awmy'): newluzhitu_XMLǃdayanlu48ǃLOADER
		getChild(name: 'dayanlu49'): newluzhitu_XMLǃdayanlu49ǃLOADER
		getChildAt(index: 233): newluzhitu_XMLǃdayanlu49ǃLOADER
		getChildById(id: 'n228_awmy'): newluzhitu_XMLǃdayanlu49ǃLOADER
		getChild(name: 'dayanlu50'): newluzhitu_XMLǃdayanlu50ǃLOADER
		getChildAt(index: 234): newluzhitu_XMLǃdayanlu50ǃLOADER
		getChildById(id: 'n229_awmy'): newluzhitu_XMLǃdayanlu50ǃLOADER
		getChild(name: 'dayanlu51'): newluzhitu_XMLǃdayanlu51ǃLOADER
		getChildAt(index: 235): newluzhitu_XMLǃdayanlu51ǃLOADER
		getChildById(id: 'n230_awmy'): newluzhitu_XMLǃdayanlu51ǃLOADER
		getChild(name: 'dayanlu52'): newluzhitu_XMLǃdayanlu52ǃLOADER
		getChildAt(index: 236): newluzhitu_XMLǃdayanlu52ǃLOADER
		getChildById(id: 'n231_awmy'): newluzhitu_XMLǃdayanlu52ǃLOADER
		getChild(name: 'dayanlu53'): newluzhitu_XMLǃdayanlu53ǃLOADER
		getChildAt(index: 237): newluzhitu_XMLǃdayanlu53ǃLOADER
		getChildById(id: 'n232_awmy'): newluzhitu_XMLǃdayanlu53ǃLOADER
		getChild(name: 'dayanlu54'): newluzhitu_XMLǃdayanlu54ǃLOADER
		getChildAt(index: 238): newluzhitu_XMLǃdayanlu54ǃLOADER
		getChildById(id: 'n233_awmy'): newluzhitu_XMLǃdayanlu54ǃLOADER
		getChild(name: 'dayanlu55'): newluzhitu_XMLǃdayanlu55ǃLOADER
		getChildAt(index: 239): newluzhitu_XMLǃdayanlu55ǃLOADER
		getChildById(id: 'n234_awmy'): newluzhitu_XMLǃdayanlu55ǃLOADER
		getChild(name: 'dayanlu56'): newluzhitu_XMLǃdayanlu56ǃLOADER
		getChildAt(index: 240): newluzhitu_XMLǃdayanlu56ǃLOADER
		getChildById(id: 'n235_awmy'): newluzhitu_XMLǃdayanlu56ǃLOADER
		getChild(name: 'dayanlu57'): newluzhitu_XMLǃdayanlu57ǃLOADER
		getChildAt(index: 241): newluzhitu_XMLǃdayanlu57ǃLOADER
		getChildById(id: 'n236_awmy'): newluzhitu_XMLǃdayanlu57ǃLOADER
		getChild(name: 'dayanlu58'): newluzhitu_XMLǃdayanlu58ǃLOADER
		getChildAt(index: 242): newluzhitu_XMLǃdayanlu58ǃLOADER
		getChildById(id: 'n237_awmy'): newluzhitu_XMLǃdayanlu58ǃLOADER
		getChild(name: 'dayanlu59'): newluzhitu_XMLǃdayanlu59ǃLOADER
		getChildAt(index: 243): newluzhitu_XMLǃdayanlu59ǃLOADER
		getChildById(id: 'n238_awmy'): newluzhitu_XMLǃdayanlu59ǃLOADER
		getChild(name: 'dayanlu60'): newluzhitu_XMLǃdayanlu60ǃLOADER
		getChildAt(index: 244): newluzhitu_XMLǃdayanlu60ǃLOADER
		getChildById(id: 'n239_awmy'): newluzhitu_XMLǃdayanlu60ǃLOADER
		getChild(name: 'dayanlu61'): newluzhitu_XMLǃdayanlu61ǃLOADER
		getChildAt(index: 245): newluzhitu_XMLǃdayanlu61ǃLOADER
		getChildById(id: 'n240_awmy'): newluzhitu_XMLǃdayanlu61ǃLOADER
		getChild(name: 'dayanlu62'): newluzhitu_XMLǃdayanlu62ǃLOADER
		getChildAt(index: 246): newluzhitu_XMLǃdayanlu62ǃLOADER
		getChildById(id: 'n241_awmy'): newluzhitu_XMLǃdayanlu62ǃLOADER
		getChild(name: 'dayanlu63'): newluzhitu_XMLǃdayanlu63ǃLOADER
		getChildAt(index: 247): newluzhitu_XMLǃdayanlu63ǃLOADER
		getChildById(id: 'n242_awmy'): newluzhitu_XMLǃdayanlu63ǃLOADER
		getChild(name: 'dayanlu64'): newluzhitu_XMLǃdayanlu64ǃLOADER
		getChildAt(index: 248): newluzhitu_XMLǃdayanlu64ǃLOADER
		getChildById(id: 'n243_awmy'): newluzhitu_XMLǃdayanlu64ǃLOADER
		getChild(name: 'dayanlu65'): newluzhitu_XMLǃdayanlu65ǃLOADER
		getChildAt(index: 249): newluzhitu_XMLǃdayanlu65ǃLOADER
		getChildById(id: 'n244_awmy'): newluzhitu_XMLǃdayanlu65ǃLOADER
		getChild(name: 'dayanlu66'): newluzhitu_XMLǃdayanlu66ǃLOADER
		getChildAt(index: 250): newluzhitu_XMLǃdayanlu66ǃLOADER
		getChildById(id: 'n245_awmy'): newluzhitu_XMLǃdayanlu66ǃLOADER
		getChild(name: 'dayanlu67'): newluzhitu_XMLǃdayanlu67ǃLOADER
		getChildAt(index: 251): newluzhitu_XMLǃdayanlu67ǃLOADER
		getChildById(id: 'n246_awmy'): newluzhitu_XMLǃdayanlu67ǃLOADER
		getChild(name: 'dayanlu68'): newluzhitu_XMLǃdayanlu68ǃLOADER
		getChildAt(index: 252): newluzhitu_XMLǃdayanlu68ǃLOADER
		getChildById(id: 'n247_awmy'): newluzhitu_XMLǃdayanlu68ǃLOADER
		getChild(name: 'dayanlu69'): newluzhitu_XMLǃdayanlu69ǃLOADER
		getChildAt(index: 253): newluzhitu_XMLǃdayanlu69ǃLOADER
		getChildById(id: 'n248_awmy'): newluzhitu_XMLǃdayanlu69ǃLOADER
		getChild(name: 'dayanlu70'): newluzhitu_XMLǃdayanlu70ǃLOADER
		getChildAt(index: 254): newluzhitu_XMLǃdayanlu70ǃLOADER
		getChildById(id: 'n249_awmy'): newluzhitu_XMLǃdayanlu70ǃLOADER
		getChild(name: 'dayanlu71'): newluzhitu_XMLǃdayanlu71ǃLOADER
		getChildAt(index: 255): newluzhitu_XMLǃdayanlu71ǃLOADER
		getChildById(id: 'n250_awmy'): newluzhitu_XMLǃdayanlu71ǃLOADER
		getChild(name: 'dayanlu72'): newluzhitu_XMLǃdayanlu72ǃLOADER
		getChildAt(index: 256): newluzhitu_XMLǃdayanlu72ǃLOADER
		getChildById(id: 'n251_awmy'): newluzhitu_XMLǃdayanlu72ǃLOADER
		getChild(name: 'dayanlu73'): newluzhitu_XMLǃdayanlu73ǃLOADER
		getChildAt(index: 257): newluzhitu_XMLǃdayanlu73ǃLOADER
		getChildById(id: 'n252_awmy'): newluzhitu_XMLǃdayanlu73ǃLOADER
		getChild(name: 'dayanlu74'): newluzhitu_XMLǃdayanlu74ǃLOADER
		getChildAt(index: 258): newluzhitu_XMLǃdayanlu74ǃLOADER
		getChildById(id: 'n253_awmy'): newluzhitu_XMLǃdayanlu74ǃLOADER
		getChild(name: 'dayanlu75'): newluzhitu_XMLǃdayanlu75ǃLOADER
		getChildAt(index: 259): newluzhitu_XMLǃdayanlu75ǃLOADER
		getChildById(id: 'n254_awmy'): newluzhitu_XMLǃdayanlu75ǃLOADER
		getChild(name: 'dayanlu76'): newluzhitu_XMLǃdayanlu76ǃLOADER
		getChildAt(index: 260): newluzhitu_XMLǃdayanlu76ǃLOADER
		getChildById(id: 'n255_awmy'): newluzhitu_XMLǃdayanlu76ǃLOADER
		getChild(name: 'dayanlu77'): newluzhitu_XMLǃdayanlu77ǃLOADER
		getChildAt(index: 261): newluzhitu_XMLǃdayanlu77ǃLOADER
		getChildById(id: 'n256_awmy'): newluzhitu_XMLǃdayanlu77ǃLOADER
		getChild(name: 'dayanlu78'): newluzhitu_XMLǃdayanlu78ǃLOADER
		getChildAt(index: 262): newluzhitu_XMLǃdayanlu78ǃLOADER
		getChildById(id: 'n257_awmy'): newluzhitu_XMLǃdayanlu78ǃLOADER
		getChild(name: 'dayanlu79'): newluzhitu_XMLǃdayanlu79ǃLOADER
		getChildAt(index: 263): newluzhitu_XMLǃdayanlu79ǃLOADER
		getChildById(id: 'n258_awmy'): newluzhitu_XMLǃdayanlu79ǃLOADER
		getChild(name: 'dayanlu80'): newluzhitu_XMLǃdayanlu80ǃLOADER
		getChildAt(index: 264): newluzhitu_XMLǃdayanlu80ǃLOADER
		getChildById(id: 'n259_awmy'): newluzhitu_XMLǃdayanlu80ǃLOADER
		getChild(name: 'dayanlu81'): newluzhitu_XMLǃdayanlu81ǃLOADER
		getChildAt(index: 265): newluzhitu_XMLǃdayanlu81ǃLOADER
		getChildById(id: 'n260_awmy'): newluzhitu_XMLǃdayanlu81ǃLOADER
		getChild(name: 'dayanlu82'): newluzhitu_XMLǃdayanlu82ǃLOADER
		getChildAt(index: 266): newluzhitu_XMLǃdayanlu82ǃLOADER
		getChildById(id: 'n261_awmy'): newluzhitu_XMLǃdayanlu82ǃLOADER
		getChild(name: 'dayanlu83'): newluzhitu_XMLǃdayanlu83ǃLOADER
		getChildAt(index: 267): newluzhitu_XMLǃdayanlu83ǃLOADER
		getChildById(id: 'n262_awmy'): newluzhitu_XMLǃdayanlu83ǃLOADER
		getChild(name: 'dayanlu84'): newluzhitu_XMLǃdayanlu84ǃLOADER
		getChildAt(index: 268): newluzhitu_XMLǃdayanlu84ǃLOADER
		getChildById(id: 'n263_awmy'): newluzhitu_XMLǃdayanlu84ǃLOADER
		getChild(name: 'dayanlu85'): newluzhitu_XMLǃdayanlu85ǃLOADER
		getChildAt(index: 269): newluzhitu_XMLǃdayanlu85ǃLOADER
		getChildById(id: 'n264_awmy'): newluzhitu_XMLǃdayanlu85ǃLOADER
		getChild(name: 'dayanlu86'): newluzhitu_XMLǃdayanlu86ǃLOADER
		getChildAt(index: 270): newluzhitu_XMLǃdayanlu86ǃLOADER
		getChildById(id: 'n265_awmy'): newluzhitu_XMLǃdayanlu86ǃLOADER
		getChild(name: 'dayanlu87'): newluzhitu_XMLǃdayanlu87ǃLOADER
		getChildAt(index: 271): newluzhitu_XMLǃdayanlu87ǃLOADER
		getChildById(id: 'n266_awmy'): newluzhitu_XMLǃdayanlu87ǃLOADER
		getChild(name: 'dayanlu88'): newluzhitu_XMLǃdayanlu88ǃLOADER
		getChildAt(index: 272): newluzhitu_XMLǃdayanlu88ǃLOADER
		getChildById(id: 'n267_awmy'): newluzhitu_XMLǃdayanlu88ǃLOADER
		getChild(name: 'dayanlu89'): newluzhitu_XMLǃdayanlu89ǃLOADER
		getChildAt(index: 273): newluzhitu_XMLǃdayanlu89ǃLOADER
		getChildById(id: 'n268_awmy'): newluzhitu_XMLǃdayanlu89ǃLOADER
		getChild(name: 'dayanlu90'): newluzhitu_XMLǃdayanlu90ǃLOADER
		getChildAt(index: 274): newluzhitu_XMLǃdayanlu90ǃLOADER
		getChildById(id: 'n269_awmy'): newluzhitu_XMLǃdayanlu90ǃLOADER
		getChild(name: 'dayanlu91'): newluzhitu_XMLǃdayanlu91ǃLOADER
		getChildAt(index: 275): newluzhitu_XMLǃdayanlu91ǃLOADER
		getChildById(id: 'n270_awmy'): newluzhitu_XMLǃdayanlu91ǃLOADER
		getChild(name: 'dayanlu92'): newluzhitu_XMLǃdayanlu92ǃLOADER
		getChildAt(index: 276): newluzhitu_XMLǃdayanlu92ǃLOADER
		getChildById(id: 'n271_awmy'): newluzhitu_XMLǃdayanlu92ǃLOADER
		getChild(name: 'dayanlu93'): newluzhitu_XMLǃdayanlu93ǃLOADER
		getChildAt(index: 277): newluzhitu_XMLǃdayanlu93ǃLOADER
		getChildById(id: 'n272_awmy'): newluzhitu_XMLǃdayanlu93ǃLOADER
		getChild(name: 'dayanlu94'): newluzhitu_XMLǃdayanlu94ǃLOADER
		getChildAt(index: 278): newluzhitu_XMLǃdayanlu94ǃLOADER
		getChildById(id: 'n273_awmy'): newluzhitu_XMLǃdayanlu94ǃLOADER
		getChild(name: 'dayanlu95'): newluzhitu_XMLǃdayanlu95ǃLOADER
		getChildAt(index: 279): newluzhitu_XMLǃdayanlu95ǃLOADER
		getChildById(id: 'n274_awmy'): newluzhitu_XMLǃdayanlu95ǃLOADER
		getChild(name: 'dayanlu96'): newluzhitu_XMLǃdayanlu96ǃLOADER
		getChildAt(index: 280): newluzhitu_XMLǃdayanlu96ǃLOADER
		getChildById(id: 'n275_awmy'): newluzhitu_XMLǃdayanlu96ǃLOADER
		getChild(name: 'dayanlu97'): newluzhitu_XMLǃdayanlu97ǃLOADER
		getChildAt(index: 281): newluzhitu_XMLǃdayanlu97ǃLOADER
		getChildById(id: 'n276_awmy'): newluzhitu_XMLǃdayanlu97ǃLOADER
		getChild(name: 'dayanlu98'): newluzhitu_XMLǃdayanlu98ǃLOADER
		getChildAt(index: 282): newluzhitu_XMLǃdayanlu98ǃLOADER
		getChildById(id: 'n277_awmy'): newluzhitu_XMLǃdayanlu98ǃLOADER
		getChild(name: 'dayanlu99'): newluzhitu_XMLǃdayanlu99ǃLOADER
		getChildAt(index: 283): newluzhitu_XMLǃdayanlu99ǃLOADER
		getChildById(id: 'n278_awmy'): newluzhitu_XMLǃdayanlu99ǃLOADER
		getChild(name: 'dayanlu100'): newluzhitu_XMLǃdayanlu100ǃLOADER
		getChildAt(index: 284): newluzhitu_XMLǃdayanlu100ǃLOADER
		getChildById(id: 'n279_awmy'): newluzhitu_XMLǃdayanlu100ǃLOADER
		getChild(name: 'dayanlu101'): newluzhitu_XMLǃdayanlu101ǃLOADER
		getChildAt(index: 285): newluzhitu_XMLǃdayanlu101ǃLOADER
		getChildById(id: 'n280_awmy'): newluzhitu_XMLǃdayanlu101ǃLOADER
		getChild(name: 'dayanlu102'): newluzhitu_XMLǃdayanlu102ǃLOADER
		getChildAt(index: 286): newluzhitu_XMLǃdayanlu102ǃLOADER
		getChildById(id: 'n281_awmy'): newluzhitu_XMLǃdayanlu102ǃLOADER
		getChild(name: 'dayanlu103'): newluzhitu_XMLǃdayanlu103ǃLOADER
		getChildAt(index: 287): newluzhitu_XMLǃdayanlu103ǃLOADER
		getChildById(id: 'n282_awmy'): newluzhitu_XMLǃdayanlu103ǃLOADER
		getChild(name: 'dayanlu104'): newluzhitu_XMLǃdayanlu104ǃLOADER
		getChildAt(index: 288): newluzhitu_XMLǃdayanlu104ǃLOADER
		getChildById(id: 'n283_awmy'): newluzhitu_XMLǃdayanlu104ǃLOADER
		getChild(name: 'dayanlu105'): newluzhitu_XMLǃdayanlu105ǃLOADER
		getChildAt(index: 289): newluzhitu_XMLǃdayanlu105ǃLOADER
		getChildById(id: 'n284_awmy'): newluzhitu_XMLǃdayanlu105ǃLOADER
		getChild(name: 'dayanlu106'): newluzhitu_XMLǃdayanlu106ǃLOADER
		getChildAt(index: 290): newluzhitu_XMLǃdayanlu106ǃLOADER
		getChildById(id: 'n285_awmy'): newluzhitu_XMLǃdayanlu106ǃLOADER
		getChild(name: 'dayanlu107'): newluzhitu_XMLǃdayanlu107ǃLOADER
		getChildAt(index: 291): newluzhitu_XMLǃdayanlu107ǃLOADER
		getChildById(id: 'n286_awmy'): newluzhitu_XMLǃdayanlu107ǃLOADER
		getChild(name: 'dayanlu108'): newluzhitu_XMLǃdayanlu108ǃLOADER
		getChildAt(index: 292): newluzhitu_XMLǃdayanlu108ǃLOADER
		getChildById(id: 'n287_awmy'): newluzhitu_XMLǃdayanlu108ǃLOADER
		getChild(name: 'dayanlu109'): newluzhitu_XMLǃdayanlu109ǃLOADER
		getChildAt(index: 293): newluzhitu_XMLǃdayanlu109ǃLOADER
		getChildById(id: 'n288_awmy'): newluzhitu_XMLǃdayanlu109ǃLOADER
		getChild(name: 'dayanlu110'): newluzhitu_XMLǃdayanlu110ǃLOADER
		getChildAt(index: 294): newluzhitu_XMLǃdayanlu110ǃLOADER
		getChildById(id: 'n289_awmy'): newluzhitu_XMLǃdayanlu110ǃLOADER
		getChild(name: 'dayanlu111'): newluzhitu_XMLǃdayanlu111ǃLOADER
		getChildAt(index: 295): newluzhitu_XMLǃdayanlu111ǃLOADER
		getChildById(id: 'n290_awmy'): newluzhitu_XMLǃdayanlu111ǃLOADER
		getChild(name: 'dayanlu112'): newluzhitu_XMLǃdayanlu112ǃLOADER
		getChildAt(index: 296): newluzhitu_XMLǃdayanlu112ǃLOADER
		getChildById(id: 'n291_awmy'): newluzhitu_XMLǃdayanlu112ǃLOADER
		getChild(name: 'dayanlu113'): newluzhitu_XMLǃdayanlu113ǃLOADER
		getChildAt(index: 297): newluzhitu_XMLǃdayanlu113ǃLOADER
		getChildById(id: 'n292_awmy'): newluzhitu_XMLǃdayanlu113ǃLOADER
		getChild(name: 'dayanlu114'): newluzhitu_XMLǃdayanlu114ǃLOADER
		getChildAt(index: 298): newluzhitu_XMLǃdayanlu114ǃLOADER
		getChildById(id: 'n293_awmy'): newluzhitu_XMLǃdayanlu114ǃLOADER
		getChild(name: 'dayanlu115'): newluzhitu_XMLǃdayanlu115ǃLOADER
		getChildAt(index: 299): newluzhitu_XMLǃdayanlu115ǃLOADER
		getChildById(id: 'n294_awmy'): newluzhitu_XMLǃdayanlu115ǃLOADER
		getChild(name: 'dayanlu116'): newluzhitu_XMLǃdayanlu116ǃLOADER
		getChildAt(index: 300): newluzhitu_XMLǃdayanlu116ǃLOADER
		getChildById(id: 'n295_awmy'): newluzhitu_XMLǃdayanlu116ǃLOADER
		getChild(name: 'dayanlu117'): newluzhitu_XMLǃdayanlu117ǃLOADER
		getChildAt(index: 301): newluzhitu_XMLǃdayanlu117ǃLOADER
		getChildById(id: 'n296_awmy'): newluzhitu_XMLǃdayanlu117ǃLOADER
		getChild(name: 'dayanlu118'): newluzhitu_XMLǃdayanlu118ǃLOADER
		getChildAt(index: 302): newluzhitu_XMLǃdayanlu118ǃLOADER
		getChildById(id: 'n297_awmy'): newluzhitu_XMLǃdayanlu118ǃLOADER
		getChild(name: 'dayanlu119'): newluzhitu_XMLǃdayanlu119ǃLOADER
		getChildAt(index: 303): newluzhitu_XMLǃdayanlu119ǃLOADER
		getChildById(id: 'n298_awmy'): newluzhitu_XMLǃdayanlu119ǃLOADER
		getChild(name: 'dayanlu120'): newluzhitu_XMLǃdayanlu120ǃLOADER
		getChildAt(index: 304): newluzhitu_XMLǃdayanlu120ǃLOADER
		getChildById(id: 'n299_awmy'): newluzhitu_XMLǃdayanlu120ǃLOADER
		getChild(name: 'dayanlu121'): newluzhitu_XMLǃdayanlu121ǃLOADER
		getChildAt(index: 305): newluzhitu_XMLǃdayanlu121ǃLOADER
		getChildById(id: 'n300_awmy'): newluzhitu_XMLǃdayanlu121ǃLOADER
		getChild(name: 'dayanlu122'): newluzhitu_XMLǃdayanlu122ǃLOADER
		getChildAt(index: 306): newluzhitu_XMLǃdayanlu122ǃLOADER
		getChildById(id: 'n301_awmy'): newluzhitu_XMLǃdayanlu122ǃLOADER
		getChild(name: 'dayanlu123'): newluzhitu_XMLǃdayanlu123ǃLOADER
		getChildAt(index: 307): newluzhitu_XMLǃdayanlu123ǃLOADER
		getChildById(id: 'n302_awmy'): newluzhitu_XMLǃdayanlu123ǃLOADER
		getChild(name: 'dayanlu124'): newluzhitu_XMLǃdayanlu124ǃLOADER
		getChildAt(index: 308): newluzhitu_XMLǃdayanlu124ǃLOADER
		getChildById(id: 'n303_awmy'): newluzhitu_XMLǃdayanlu124ǃLOADER
		getChild(name: 'dayanlu125'): newluzhitu_XMLǃdayanlu125ǃLOADER
		getChildAt(index: 309): newluzhitu_XMLǃdayanlu125ǃLOADER
		getChildById(id: 'n304_awmy'): newluzhitu_XMLǃdayanlu125ǃLOADER
		getChild(name: 'dayanlu126'): newluzhitu_XMLǃdayanlu126ǃLOADER
		getChildAt(index: 310): newluzhitu_XMLǃdayanlu126ǃLOADER
		getChildById(id: 'n305_awmy'): newluzhitu_XMLǃdayanlu126ǃLOADER
		getChild(name: 'dayanlu127'): newluzhitu_XMLǃdayanlu127ǃLOADER
		getChildAt(index: 311): newluzhitu_XMLǃdayanlu127ǃLOADER
		getChildById(id: 'n306_awmy'): newluzhitu_XMLǃdayanlu127ǃLOADER
		getChild(name: 'dayanlu128'): newluzhitu_XMLǃdayanlu128ǃLOADER
		getChildAt(index: 312): newluzhitu_XMLǃdayanlu128ǃLOADER
		getChildById(id: 'n307_awmy'): newluzhitu_XMLǃdayanlu128ǃLOADER
		getChild(name: 'dayanlu129'): newluzhitu_XMLǃdayanlu129ǃLOADER
		getChildAt(index: 313): newluzhitu_XMLǃdayanlu129ǃLOADER
		getChildById(id: 'n308_awmy'): newluzhitu_XMLǃdayanlu129ǃLOADER
		getChild(name: 'dayanlu130'): newluzhitu_XMLǃdayanlu130ǃLOADER
		getChildAt(index: 314): newluzhitu_XMLǃdayanlu130ǃLOADER
		getChildById(id: 'n309_awmy'): newluzhitu_XMLǃdayanlu130ǃLOADER
		getChild(name: 'dayanlu131'): newluzhitu_XMLǃdayanlu131ǃLOADER
		getChildAt(index: 315): newluzhitu_XMLǃdayanlu131ǃLOADER
		getChildById(id: 'n310_awmy'): newluzhitu_XMLǃdayanlu131ǃLOADER
		getChild(name: 'dayanlu132'): newluzhitu_XMLǃdayanlu132ǃLOADER
		getChildAt(index: 316): newluzhitu_XMLǃdayanlu132ǃLOADER
		getChildById(id: 'n311_awmy'): newluzhitu_XMLǃdayanlu132ǃLOADER
		getChild(name: 'dayanlu133'): newluzhitu_XMLǃdayanlu133ǃLOADER
		getChildAt(index: 317): newluzhitu_XMLǃdayanlu133ǃLOADER
		getChildById(id: 'n312_awmy'): newluzhitu_XMLǃdayanlu133ǃLOADER
		getChild(name: 'dayanlu134'): newluzhitu_XMLǃdayanlu134ǃLOADER
		getChildAt(index: 318): newluzhitu_XMLǃdayanlu134ǃLOADER
		getChildById(id: 'n313_awmy'): newluzhitu_XMLǃdayanlu134ǃLOADER
		getChild(name: 'dayanlu135'): newluzhitu_XMLǃdayanlu135ǃLOADER
		getChildAt(index: 319): newluzhitu_XMLǃdayanlu135ǃLOADER
		getChildById(id: 'n314_awmy'): newluzhitu_XMLǃdayanlu135ǃLOADER
		getChild(name: 'dayanlu136'): newluzhitu_XMLǃdayanlu136ǃLOADER
		getChildAt(index: 320): newluzhitu_XMLǃdayanlu136ǃLOADER
		getChildById(id: 'n315_awmy'): newluzhitu_XMLǃdayanlu136ǃLOADER
		getChild(name: 'dayanlu137'): newluzhitu_XMLǃdayanlu137ǃLOADER
		getChildAt(index: 321): newluzhitu_XMLǃdayanlu137ǃLOADER
		getChildById(id: 'n316_awmy'): newluzhitu_XMLǃdayanlu137ǃLOADER
		getChild(name: 'dayanlu138'): newluzhitu_XMLǃdayanlu138ǃLOADER
		getChildAt(index: 322): newluzhitu_XMLǃdayanlu138ǃLOADER
		getChildById(id: 'n317_awmy'): newluzhitu_XMLǃdayanlu138ǃLOADER
		getChild(name: 'dayanlu139'): newluzhitu_XMLǃdayanlu139ǃLOADER
		getChildAt(index: 323): newluzhitu_XMLǃdayanlu139ǃLOADER
		getChildById(id: 'n318_awmy'): newluzhitu_XMLǃdayanlu139ǃLOADER
		getChild(name: 'dayanlu140'): newluzhitu_XMLǃdayanlu140ǃLOADER
		getChildAt(index: 324): newluzhitu_XMLǃdayanlu140ǃLOADER
		getChildById(id: 'n319_awmy'): newluzhitu_XMLǃdayanlu140ǃLOADER
		getChild(name: 'dayanlu141'): newluzhitu_XMLǃdayanlu141ǃLOADER
		getChildAt(index: 325): newluzhitu_XMLǃdayanlu141ǃLOADER
		getChildById(id: 'n320_awmy'): newluzhitu_XMLǃdayanlu141ǃLOADER
		getChild(name: 'dayanlu142'): newluzhitu_XMLǃdayanlu142ǃLOADER
		getChildAt(index: 326): newluzhitu_XMLǃdayanlu142ǃLOADER
		getChildById(id: 'n321_awmy'): newluzhitu_XMLǃdayanlu142ǃLOADER
		getChild(name: 'dayanlu143'): newluzhitu_XMLǃdayanlu143ǃLOADER
		getChildAt(index: 327): newluzhitu_XMLǃdayanlu143ǃLOADER
		getChildById(id: 'n322_awmy'): newluzhitu_XMLǃdayanlu143ǃLOADER
		getChild(name: 'dayanlu144'): newluzhitu_XMLǃdayanlu144ǃLOADER
		getChildAt(index: 328): newluzhitu_XMLǃdayanlu144ǃLOADER
		getChildById(id: 'n323_awmy'): newluzhitu_XMLǃdayanlu144ǃLOADER
		getChild(name: 'dayanlu145'): newluzhitu_XMLǃdayanlu145ǃLOADER
		getChildAt(index: 329): newluzhitu_XMLǃdayanlu145ǃLOADER
		getChildById(id: 'n324_awmy'): newluzhitu_XMLǃdayanlu145ǃLOADER
		getChild(name: 'dayanlu146'): newluzhitu_XMLǃdayanlu146ǃLOADER
		getChildAt(index: 330): newluzhitu_XMLǃdayanlu146ǃLOADER
		getChildById(id: 'n325_awmy'): newluzhitu_XMLǃdayanlu146ǃLOADER
		getChild(name: 'dayanlu147'): newluzhitu_XMLǃdayanlu147ǃLOADER
		getChildAt(index: 331): newluzhitu_XMLǃdayanlu147ǃLOADER
		getChildById(id: 'n326_awmy'): newluzhitu_XMLǃdayanlu147ǃLOADER
		getChild(name: 'dayanlu148'): newluzhitu_XMLǃdayanlu148ǃLOADER
		getChildAt(index: 332): newluzhitu_XMLǃdayanlu148ǃLOADER
		getChildById(id: 'n327_awmy'): newluzhitu_XMLǃdayanlu148ǃLOADER
		getChild(name: 'dayanlu149'): newluzhitu_XMLǃdayanlu149ǃLOADER
		getChildAt(index: 333): newluzhitu_XMLǃdayanlu149ǃLOADER
		getChildById(id: 'n328_awmy'): newluzhitu_XMLǃdayanlu149ǃLOADER
		getChild(name: 'dayanlu150'): newluzhitu_XMLǃdayanlu150ǃLOADER
		getChildAt(index: 334): newluzhitu_XMLǃdayanlu150ǃLOADER
		getChildById(id: 'n329_awmy'): newluzhitu_XMLǃdayanlu150ǃLOADER
		getChild(name: 'dayanlu151'): newluzhitu_XMLǃdayanlu151ǃLOADER
		getChildAt(index: 335): newluzhitu_XMLǃdayanlu151ǃLOADER
		getChildById(id: 'n330_awmy'): newluzhitu_XMLǃdayanlu151ǃLOADER
		getChild(name: 'dayanlu152'): newluzhitu_XMLǃdayanlu152ǃLOADER
		getChildAt(index: 336): newluzhitu_XMLǃdayanlu152ǃLOADER
		getChildById(id: 'n331_awmy'): newluzhitu_XMLǃdayanlu152ǃLOADER
		getChild(name: 'dayanlu153'): newluzhitu_XMLǃdayanlu153ǃLOADER
		getChildAt(index: 337): newluzhitu_XMLǃdayanlu153ǃLOADER
		getChildById(id: 'n332_awmy'): newluzhitu_XMLǃdayanlu153ǃLOADER
		getChild(name: 'dayanlu154'): newluzhitu_XMLǃdayanlu154ǃLOADER
		getChildAt(index: 338): newluzhitu_XMLǃdayanlu154ǃLOADER
		getChildById(id: 'n333_awmy'): newluzhitu_XMLǃdayanlu154ǃLOADER
		getChild(name: 'dayanlu155'): newluzhitu_XMLǃdayanlu155ǃLOADER
		getChildAt(index: 339): newluzhitu_XMLǃdayanlu155ǃLOADER
		getChildById(id: 'n334_awmy'): newluzhitu_XMLǃdayanlu155ǃLOADER
		getChild(name: 'dayanlu156'): newluzhitu_XMLǃdayanlu156ǃLOADER
		getChildAt(index: 340): newluzhitu_XMLǃdayanlu156ǃLOADER
		getChildById(id: 'n335_awmy'): newluzhitu_XMLǃdayanlu156ǃLOADER
		getChild(name: 'dayanlu157'): newluzhitu_XMLǃdayanlu157ǃLOADER
		getChildAt(index: 341): newluzhitu_XMLǃdayanlu157ǃLOADER
		getChildById(id: 'n336_awmy'): newluzhitu_XMLǃdayanlu157ǃLOADER
		getChild(name: 'dayanlu158'): newluzhitu_XMLǃdayanlu158ǃLOADER
		getChildAt(index: 342): newluzhitu_XMLǃdayanlu158ǃLOADER
		getChildById(id: 'n337_awmy'): newluzhitu_XMLǃdayanlu158ǃLOADER
		getChild(name: 'dayanlu159'): newluzhitu_XMLǃdayanlu159ǃLOADER
		getChildAt(index: 343): newluzhitu_XMLǃdayanlu159ǃLOADER
		getChildById(id: 'n338_awmy'): newluzhitu_XMLǃdayanlu159ǃLOADER
		getChild(name: 'dayanlu160'): newluzhitu_XMLǃdayanlu160ǃLOADER
		getChildAt(index: 344): newluzhitu_XMLǃdayanlu160ǃLOADER
		getChildById(id: 'n339_awmy'): newluzhitu_XMLǃdayanlu160ǃLOADER
		getChild(name: 'dayanlu161'): newluzhitu_XMLǃdayanlu161ǃLOADER
		getChildAt(index: 345): newluzhitu_XMLǃdayanlu161ǃLOADER
		getChildById(id: 'n340_awmy'): newluzhitu_XMLǃdayanlu161ǃLOADER
		getChild(name: 'dayanlu162'): newluzhitu_XMLǃdayanlu162ǃLOADER
		getChildAt(index: 346): newluzhitu_XMLǃdayanlu162ǃLOADER
		getChildById(id: 'n341_awmy'): newluzhitu_XMLǃdayanlu162ǃLOADER
		getChild(name: 'dayanlu163'): newluzhitu_XMLǃdayanlu163ǃLOADER
		getChildAt(index: 347): newluzhitu_XMLǃdayanlu163ǃLOADER
		getChildById(id: 'n342_awmy'): newluzhitu_XMLǃdayanlu163ǃLOADER
		getChild(name: 'dayanlu164'): newluzhitu_XMLǃdayanlu164ǃLOADER
		getChildAt(index: 348): newluzhitu_XMLǃdayanlu164ǃLOADER
		getChildById(id: 'n343_awmy'): newluzhitu_XMLǃdayanlu164ǃLOADER
		getChild(name: 'dayanlu165'): newluzhitu_XMLǃdayanlu165ǃLOADER
		getChildAt(index: 349): newluzhitu_XMLǃdayanlu165ǃLOADER
		getChildById(id: 'n344_awmy'): newluzhitu_XMLǃdayanlu165ǃLOADER
		getChild(name: 'dayanlu166'): newluzhitu_XMLǃdayanlu166ǃLOADER
		getChildAt(index: 350): newluzhitu_XMLǃdayanlu166ǃLOADER
		getChildById(id: 'n345_awmy'): newluzhitu_XMLǃdayanlu166ǃLOADER
		getChild(name: 'dayanlu167'): newluzhitu_XMLǃdayanlu167ǃLOADER
		getChildAt(index: 351): newluzhitu_XMLǃdayanlu167ǃLOADER
		getChildById(id: 'n346_awmy'): newluzhitu_XMLǃdayanlu167ǃLOADER
		getChild(name: 'dayanlu168'): newluzhitu_XMLǃdayanlu168ǃLOADER
		getChildAt(index: 352): newluzhitu_XMLǃdayanlu168ǃLOADER
		getChildById(id: 'n347_awmy'): newluzhitu_XMLǃdayanlu168ǃLOADER
		getChild(name: 'dayanlu169'): newluzhitu_XMLǃdayanlu169ǃLOADER
		getChildAt(index: 353): newluzhitu_XMLǃdayanlu169ǃLOADER
		getChildById(id: 'n348_awmy'): newluzhitu_XMLǃdayanlu169ǃLOADER
		getChild(name: 'dayanlu170'): newluzhitu_XMLǃdayanlu170ǃLOADER
		getChildAt(index: 354): newluzhitu_XMLǃdayanlu170ǃLOADER
		getChildById(id: 'n349_awmy'): newluzhitu_XMLǃdayanlu170ǃLOADER
		getChild(name: 'dayanlu171'): newluzhitu_XMLǃdayanlu171ǃLOADER
		getChildAt(index: 355): newluzhitu_XMLǃdayanlu171ǃLOADER
		getChildById(id: 'n350_awmy'): newluzhitu_XMLǃdayanlu171ǃLOADER
		getChild(name: 'dayanlu172'): newluzhitu_XMLǃdayanlu172ǃLOADER
		getChildAt(index: 356): newluzhitu_XMLǃdayanlu172ǃLOADER
		getChildById(id: 'n351_awmy'): newluzhitu_XMLǃdayanlu172ǃLOADER
		getChild(name: 'dayanlu173'): newluzhitu_XMLǃdayanlu173ǃLOADER
		getChildAt(index: 357): newluzhitu_XMLǃdayanlu173ǃLOADER
		getChildById(id: 'n352_awmy'): newluzhitu_XMLǃdayanlu173ǃLOADER
		getChild(name: 'dayanlu174'): newluzhitu_XMLǃdayanlu174ǃLOADER
		getChildAt(index: 358): newluzhitu_XMLǃdayanlu174ǃLOADER
		getChildById(id: 'n353_awmy'): newluzhitu_XMLǃdayanlu174ǃLOADER
		getChild(name: 'dayanlu175'): newluzhitu_XMLǃdayanlu175ǃLOADER
		getChildAt(index: 359): newluzhitu_XMLǃdayanlu175ǃLOADER
		getChildById(id: 'n354_awmy'): newluzhitu_XMLǃdayanlu175ǃLOADER
		getChild(name: 'dayanlu176'): newluzhitu_XMLǃdayanlu176ǃLOADER
		getChildAt(index: 360): newluzhitu_XMLǃdayanlu176ǃLOADER
		getChildById(id: 'n355_awmy'): newluzhitu_XMLǃdayanlu176ǃLOADER
		getChild(name: 'dayanlu177'): newluzhitu_XMLǃdayanlu177ǃLOADER
		getChildAt(index: 361): newluzhitu_XMLǃdayanlu177ǃLOADER
		getChildById(id: 'n356_awmy'): newluzhitu_XMLǃdayanlu177ǃLOADER
		getChild(name: 'dayanlu178'): newluzhitu_XMLǃdayanlu178ǃLOADER
		getChildAt(index: 362): newluzhitu_XMLǃdayanlu178ǃLOADER
		getChildById(id: 'n357_awmy'): newluzhitu_XMLǃdayanlu178ǃLOADER
		getChild(name: 'dayanlu179'): newluzhitu_XMLǃdayanlu179ǃLOADER
		getChildAt(index: 363): newluzhitu_XMLǃdayanlu179ǃLOADER
		getChildById(id: 'n358_awmy'): newluzhitu_XMLǃdayanlu179ǃLOADER
		getChild(name: 'dayanlu180'): newluzhitu_XMLǃdayanlu180ǃLOADER
		getChildAt(index: 364): newluzhitu_XMLǃdayanlu180ǃLOADER
		getChildById(id: 'n359_awmy'): newluzhitu_XMLǃdayanlu180ǃLOADER
		getChild(name: 'dayanlu181'): newluzhitu_XMLǃdayanlu181ǃLOADER
		getChildAt(index: 365): newluzhitu_XMLǃdayanlu181ǃLOADER
		getChildById(id: 'n360_awmy'): newluzhitu_XMLǃdayanlu181ǃLOADER
		getChild(name: 'dayanlu182'): newluzhitu_XMLǃdayanlu182ǃLOADER
		getChildAt(index: 366): newluzhitu_XMLǃdayanlu182ǃLOADER
		getChildById(id: 'n361_awmy'): newluzhitu_XMLǃdayanlu182ǃLOADER
		getChild(name: 'dayanlu183'): newluzhitu_XMLǃdayanlu183ǃLOADER
		getChildAt(index: 367): newluzhitu_XMLǃdayanlu183ǃLOADER
		getChildById(id: 'n362_awmy'): newluzhitu_XMLǃdayanlu183ǃLOADER
		getChild(name: 'dayanlu184'): newluzhitu_XMLǃdayanlu184ǃLOADER
		getChildAt(index: 368): newluzhitu_XMLǃdayanlu184ǃLOADER
		getChildById(id: 'n363_awmy'): newluzhitu_XMLǃdayanlu184ǃLOADER
		getChild(name: 'dayanlu185'): newluzhitu_XMLǃdayanlu185ǃLOADER
		getChildAt(index: 369): newluzhitu_XMLǃdayanlu185ǃLOADER
		getChildById(id: 'n364_awmy'): newluzhitu_XMLǃdayanlu185ǃLOADER
		getChild(name: 'dayanlu186'): newluzhitu_XMLǃdayanlu186ǃLOADER
		getChildAt(index: 370): newluzhitu_XMLǃdayanlu186ǃLOADER
		getChildById(id: 'n365_awmy'): newluzhitu_XMLǃdayanlu186ǃLOADER
		getChild(name: 'dayanlu187'): newluzhitu_XMLǃdayanlu187ǃLOADER
		getChildAt(index: 371): newluzhitu_XMLǃdayanlu187ǃLOADER
		getChildById(id: 'n366_awmy'): newluzhitu_XMLǃdayanlu187ǃLOADER
		getChild(name: 'dayanlu188'): newluzhitu_XMLǃdayanlu188ǃLOADER
		getChildAt(index: 372): newluzhitu_XMLǃdayanlu188ǃLOADER
		getChildById(id: 'n367_awmy'): newluzhitu_XMLǃdayanlu188ǃLOADER
		getChild(name: 'dayanlu189'): newluzhitu_XMLǃdayanlu189ǃLOADER
		getChildAt(index: 373): newluzhitu_XMLǃdayanlu189ǃLOADER
		getChildById(id: 'n368_awmy'): newluzhitu_XMLǃdayanlu189ǃLOADER
		getChild(name: 'dayanlu190'): newluzhitu_XMLǃdayanlu190ǃLOADER
		getChildAt(index: 374): newluzhitu_XMLǃdayanlu190ǃLOADER
		getChildById(id: 'n369_awmy'): newluzhitu_XMLǃdayanlu190ǃLOADER
		getChild(name: 'dayanlu191'): newluzhitu_XMLǃdayanlu191ǃLOADER
		getChildAt(index: 375): newluzhitu_XMLǃdayanlu191ǃLOADER
		getChildById(id: 'n370_awmy'): newluzhitu_XMLǃdayanlu191ǃLOADER
		getChild(name: 'dayanlu192'): newluzhitu_XMLǃdayanlu192ǃLOADER
		getChildAt(index: 376): newluzhitu_XMLǃdayanlu192ǃLOADER
		getChildById(id: 'n371_awmy'): newluzhitu_XMLǃdayanlu192ǃLOADER
		getChild(name: 'dayanlu193'): newluzhitu_XMLǃdayanlu193ǃLOADER
		getChildAt(index: 377): newluzhitu_XMLǃdayanlu193ǃLOADER
		getChildById(id: 'n372_awmy'): newluzhitu_XMLǃdayanlu193ǃLOADER
		getChild(name: 'dayanlu194'): newluzhitu_XMLǃdayanlu194ǃLOADER
		getChildAt(index: 378): newluzhitu_XMLǃdayanlu194ǃLOADER
		getChildById(id: 'n373_awmy'): newluzhitu_XMLǃdayanlu194ǃLOADER
		getChild(name: 'dayanlu195'): newluzhitu_XMLǃdayanlu195ǃLOADER
		getChildAt(index: 379): newluzhitu_XMLǃdayanlu195ǃLOADER
		getChildById(id: 'n374_awmy'): newluzhitu_XMLǃdayanlu195ǃLOADER
		getChild(name: 'dayanlu196'): newluzhitu_XMLǃdayanlu196ǃLOADER
		getChildAt(index: 380): newluzhitu_XMLǃdayanlu196ǃLOADER
		getChildById(id: 'n375_awmy'): newluzhitu_XMLǃdayanlu196ǃLOADER
		getChild(name: 'dayanlu197'): newluzhitu_XMLǃdayanlu197ǃLOADER
		getChildAt(index: 381): newluzhitu_XMLǃdayanlu197ǃLOADER
		getChildById(id: 'n376_awmy'): newluzhitu_XMLǃdayanlu197ǃLOADER
		getChild(name: 'xiaolu0'): newluzhitu_XMLǃxiaolu0ǃLOADER
		getChildAt(index: 382): newluzhitu_XMLǃxiaolu0ǃLOADER
		getChildById(id: 'n384_awmy'): newluzhitu_XMLǃxiaolu0ǃLOADER
		getChild(name: 'xiaolu1'): newluzhitu_XMLǃxiaolu1ǃLOADER
		getChildAt(index: 383): newluzhitu_XMLǃxiaolu1ǃLOADER
		getChildById(id: 'n385_awmy'): newluzhitu_XMLǃxiaolu1ǃLOADER
		getChild(name: 'xiaolu2'): newluzhitu_XMLǃxiaolu2ǃLOADER
		getChildAt(index: 384): newluzhitu_XMLǃxiaolu2ǃLOADER
		getChildById(id: 'n386_awmy'): newluzhitu_XMLǃxiaolu2ǃLOADER
		getChild(name: 'xiaolu3'): newluzhitu_XMLǃxiaolu3ǃLOADER
		getChildAt(index: 385): newluzhitu_XMLǃxiaolu3ǃLOADER
		getChildById(id: 'n387_awmy'): newluzhitu_XMLǃxiaolu3ǃLOADER
		getChild(name: 'xiaolu4'): newluzhitu_XMLǃxiaolu4ǃLOADER
		getChildAt(index: 386): newluzhitu_XMLǃxiaolu4ǃLOADER
		getChildById(id: 'n388_awmy'): newluzhitu_XMLǃxiaolu4ǃLOADER
		getChild(name: 'xiaolu5'): newluzhitu_XMLǃxiaolu5ǃLOADER
		getChildAt(index: 387): newluzhitu_XMLǃxiaolu5ǃLOADER
		getChildById(id: 'n389_awmy'): newluzhitu_XMLǃxiaolu5ǃLOADER
		getChild(name: 'xiaolu6'): newluzhitu_XMLǃxiaolu6ǃLOADER
		getChildAt(index: 388): newluzhitu_XMLǃxiaolu6ǃLOADER
		getChildById(id: 'n390_awmy'): newluzhitu_XMLǃxiaolu6ǃLOADER
		getChild(name: 'xiaolu7'): newluzhitu_XMLǃxiaolu7ǃLOADER
		getChildAt(index: 389): newluzhitu_XMLǃxiaolu7ǃLOADER
		getChildById(id: 'n391_awmy'): newluzhitu_XMLǃxiaolu7ǃLOADER
		getChild(name: 'xiaolu8'): newluzhitu_XMLǃxiaolu8ǃLOADER
		getChildAt(index: 390): newluzhitu_XMLǃxiaolu8ǃLOADER
		getChildById(id: 'n392_awmy'): newluzhitu_XMLǃxiaolu8ǃLOADER
		getChild(name: 'xiaolu9'): newluzhitu_XMLǃxiaolu9ǃLOADER
		getChildAt(index: 391): newluzhitu_XMLǃxiaolu9ǃLOADER
		getChildById(id: 'n393_awmy'): newluzhitu_XMLǃxiaolu9ǃLOADER
		getChild(name: 'xiaolu10'): newluzhitu_XMLǃxiaolu10ǃLOADER
		getChildAt(index: 392): newluzhitu_XMLǃxiaolu10ǃLOADER
		getChildById(id: 'n394_awmy'): newluzhitu_XMLǃxiaolu10ǃLOADER
		getChild(name: 'xiaolu11'): newluzhitu_XMLǃxiaolu11ǃLOADER
		getChildAt(index: 393): newluzhitu_XMLǃxiaolu11ǃLOADER
		getChildById(id: 'n395_awmy'): newluzhitu_XMLǃxiaolu11ǃLOADER
		getChild(name: 'xiaolu12'): newluzhitu_XMLǃxiaolu12ǃLOADER
		getChildAt(index: 394): newluzhitu_XMLǃxiaolu12ǃLOADER
		getChildById(id: 'n396_awmy'): newluzhitu_XMLǃxiaolu12ǃLOADER
		getChild(name: 'xiaolu13'): newluzhitu_XMLǃxiaolu13ǃLOADER
		getChildAt(index: 395): newluzhitu_XMLǃxiaolu13ǃLOADER
		getChildById(id: 'n397_awmy'): newluzhitu_XMLǃxiaolu13ǃLOADER
		getChild(name: 'xiaolu14'): newluzhitu_XMLǃxiaolu14ǃLOADER
		getChildAt(index: 396): newluzhitu_XMLǃxiaolu14ǃLOADER
		getChildById(id: 'n398_awmy'): newluzhitu_XMLǃxiaolu14ǃLOADER
		getChild(name: 'xiaolu15'): newluzhitu_XMLǃxiaolu15ǃLOADER
		getChildAt(index: 397): newluzhitu_XMLǃxiaolu15ǃLOADER
		getChildById(id: 'n399_awmy'): newluzhitu_XMLǃxiaolu15ǃLOADER
		getChild(name: 'xiaolu16'): newluzhitu_XMLǃxiaolu16ǃLOADER
		getChildAt(index: 398): newluzhitu_XMLǃxiaolu16ǃLOADER
		getChildById(id: 'n400_awmy'): newluzhitu_XMLǃxiaolu16ǃLOADER
		getChild(name: 'xiaolu17'): newluzhitu_XMLǃxiaolu17ǃLOADER
		getChildAt(index: 399): newluzhitu_XMLǃxiaolu17ǃLOADER
		getChildById(id: 'n401_awmy'): newluzhitu_XMLǃxiaolu17ǃLOADER
		getChild(name: 'xiaolu18'): newluzhitu_XMLǃxiaolu18ǃLOADER
		getChildAt(index: 400): newluzhitu_XMLǃxiaolu18ǃLOADER
		getChildById(id: 'n402_awmy'): newluzhitu_XMLǃxiaolu18ǃLOADER
		getChild(name: 'xiaolu19'): newluzhitu_XMLǃxiaolu19ǃLOADER
		getChildAt(index: 401): newluzhitu_XMLǃxiaolu19ǃLOADER
		getChildById(id: 'n403_awmy'): newluzhitu_XMLǃxiaolu19ǃLOADER
		getChild(name: 'xiaolu20'): newluzhitu_XMLǃxiaolu20ǃLOADER
		getChildAt(index: 402): newluzhitu_XMLǃxiaolu20ǃLOADER
		getChildById(id: 'n404_awmy'): newluzhitu_XMLǃxiaolu20ǃLOADER
		getChild(name: 'xiaolu21'): newluzhitu_XMLǃxiaolu21ǃLOADER
		getChildAt(index: 403): newluzhitu_XMLǃxiaolu21ǃLOADER
		getChildById(id: 'n405_awmy'): newluzhitu_XMLǃxiaolu21ǃLOADER
		getChild(name: 'xiaolu22'): newluzhitu_XMLǃxiaolu22ǃLOADER
		getChildAt(index: 404): newluzhitu_XMLǃxiaolu22ǃLOADER
		getChildById(id: 'n406_awmy'): newluzhitu_XMLǃxiaolu22ǃLOADER
		getChild(name: 'xiaolu23'): newluzhitu_XMLǃxiaolu23ǃLOADER
		getChildAt(index: 405): newluzhitu_XMLǃxiaolu23ǃLOADER
		getChildById(id: 'n407_awmy'): newluzhitu_XMLǃxiaolu23ǃLOADER
		getChild(name: 'xiaolu24'): newluzhitu_XMLǃxiaolu24ǃLOADER
		getChildAt(index: 406): newluzhitu_XMLǃxiaolu24ǃLOADER
		getChildById(id: 'n408_awmy'): newluzhitu_XMLǃxiaolu24ǃLOADER
		getChild(name: 'xiaolu25'): newluzhitu_XMLǃxiaolu25ǃLOADER
		getChildAt(index: 407): newluzhitu_XMLǃxiaolu25ǃLOADER
		getChildById(id: 'n409_awmy'): newluzhitu_XMLǃxiaolu25ǃLOADER
		getChild(name: 'xiaolu26'): newluzhitu_XMLǃxiaolu26ǃLOADER
		getChildAt(index: 408): newluzhitu_XMLǃxiaolu26ǃLOADER
		getChildById(id: 'n410_awmy'): newluzhitu_XMLǃxiaolu26ǃLOADER
		getChild(name: 'xiaolu27'): newluzhitu_XMLǃxiaolu27ǃLOADER
		getChildAt(index: 409): newluzhitu_XMLǃxiaolu27ǃLOADER
		getChildById(id: 'n411_awmy'): newluzhitu_XMLǃxiaolu27ǃLOADER
		getChild(name: 'xiaolu28'): newluzhitu_XMLǃxiaolu28ǃLOADER
		getChildAt(index: 410): newluzhitu_XMLǃxiaolu28ǃLOADER
		getChildById(id: 'n412_awmy'): newluzhitu_XMLǃxiaolu28ǃLOADER
		getChild(name: 'xiaolu29'): newluzhitu_XMLǃxiaolu29ǃLOADER
		getChildAt(index: 411): newluzhitu_XMLǃxiaolu29ǃLOADER
		getChildById(id: 'n413_awmy'): newluzhitu_XMLǃxiaolu29ǃLOADER
		getChild(name: 'xiaolu30'): newluzhitu_XMLǃxiaolu30ǃLOADER
		getChildAt(index: 412): newluzhitu_XMLǃxiaolu30ǃLOADER
		getChildById(id: 'n414_awmy'): newluzhitu_XMLǃxiaolu30ǃLOADER
		getChild(name: 'xiaolu31'): newluzhitu_XMLǃxiaolu31ǃLOADER
		getChildAt(index: 413): newluzhitu_XMLǃxiaolu31ǃLOADER
		getChildById(id: 'n415_awmy'): newluzhitu_XMLǃxiaolu31ǃLOADER
		getChild(name: 'xiaolu32'): newluzhitu_XMLǃxiaolu32ǃLOADER
		getChildAt(index: 414): newluzhitu_XMLǃxiaolu32ǃLOADER
		getChildById(id: 'n416_awmy'): newluzhitu_XMLǃxiaolu32ǃLOADER
		getChild(name: 'xiaolu33'): newluzhitu_XMLǃxiaolu33ǃLOADER
		getChildAt(index: 415): newluzhitu_XMLǃxiaolu33ǃLOADER
		getChildById(id: 'n417_awmy'): newluzhitu_XMLǃxiaolu33ǃLOADER
		getChild(name: 'xiaolu34'): newluzhitu_XMLǃxiaolu34ǃLOADER
		getChildAt(index: 416): newluzhitu_XMLǃxiaolu34ǃLOADER
		getChildById(id: 'n418_awmy'): newluzhitu_XMLǃxiaolu34ǃLOADER
		getChild(name: 'xiaolu35'): newluzhitu_XMLǃxiaolu35ǃLOADER
		getChildAt(index: 417): newluzhitu_XMLǃxiaolu35ǃLOADER
		getChildById(id: 'n419_awmy'): newluzhitu_XMLǃxiaolu35ǃLOADER
		getChild(name: 'xiaolu36'): newluzhitu_XMLǃxiaolu36ǃLOADER
		getChildAt(index: 418): newluzhitu_XMLǃxiaolu36ǃLOADER
		getChildById(id: 'n420_awmy'): newluzhitu_XMLǃxiaolu36ǃLOADER
		getChild(name: 'xiaolu37'): newluzhitu_XMLǃxiaolu37ǃLOADER
		getChildAt(index: 419): newluzhitu_XMLǃxiaolu37ǃLOADER
		getChildById(id: 'n421_awmy'): newluzhitu_XMLǃxiaolu37ǃLOADER
		getChild(name: 'xiaolu38'): newluzhitu_XMLǃxiaolu38ǃLOADER
		getChildAt(index: 420): newluzhitu_XMLǃxiaolu38ǃLOADER
		getChildById(id: 'n422_awmy'): newluzhitu_XMLǃxiaolu38ǃLOADER
		getChild(name: 'xiaolu39'): newluzhitu_XMLǃxiaolu39ǃLOADER
		getChildAt(index: 421): newluzhitu_XMLǃxiaolu39ǃLOADER
		getChildById(id: 'n423_awmy'): newluzhitu_XMLǃxiaolu39ǃLOADER
		getChild(name: 'xiaolu40'): newluzhitu_XMLǃxiaolu40ǃLOADER
		getChildAt(index: 422): newluzhitu_XMLǃxiaolu40ǃLOADER
		getChildById(id: 'n424_awmy'): newluzhitu_XMLǃxiaolu40ǃLOADER
		getChild(name: 'xiaolu41'): newluzhitu_XMLǃxiaolu41ǃLOADER
		getChildAt(index: 423): newluzhitu_XMLǃxiaolu41ǃLOADER
		getChildById(id: 'n425_awmy'): newluzhitu_XMLǃxiaolu41ǃLOADER
		getChild(name: 'xiaolu42'): newluzhitu_XMLǃxiaolu42ǃLOADER
		getChildAt(index: 424): newluzhitu_XMLǃxiaolu42ǃLOADER
		getChildById(id: 'n426_awmy'): newluzhitu_XMLǃxiaolu42ǃLOADER
		getChild(name: 'xiaolu43'): newluzhitu_XMLǃxiaolu43ǃLOADER
		getChildAt(index: 425): newluzhitu_XMLǃxiaolu43ǃLOADER
		getChildById(id: 'n427_awmy'): newluzhitu_XMLǃxiaolu43ǃLOADER
		getChild(name: 'xiaolu44'): newluzhitu_XMLǃxiaolu44ǃLOADER
		getChildAt(index: 426): newluzhitu_XMLǃxiaolu44ǃLOADER
		getChildById(id: 'n428_awmy'): newluzhitu_XMLǃxiaolu44ǃLOADER
		getChild(name: 'xiaolu45'): newluzhitu_XMLǃxiaolu45ǃLOADER
		getChildAt(index: 427): newluzhitu_XMLǃxiaolu45ǃLOADER
		getChildById(id: 'n429_awmy'): newluzhitu_XMLǃxiaolu45ǃLOADER
		getChild(name: 'xiaolu46'): newluzhitu_XMLǃxiaolu46ǃLOADER
		getChildAt(index: 428): newluzhitu_XMLǃxiaolu46ǃLOADER
		getChildById(id: 'n430_awmy'): newluzhitu_XMLǃxiaolu46ǃLOADER
		getChild(name: 'xiaolu47'): newluzhitu_XMLǃxiaolu47ǃLOADER
		getChildAt(index: 429): newluzhitu_XMLǃxiaolu47ǃLOADER
		getChildById(id: 'n431_awmy'): newluzhitu_XMLǃxiaolu47ǃLOADER
		getChild(name: 'xiaolu48'): newluzhitu_XMLǃxiaolu48ǃLOADER
		getChildAt(index: 430): newluzhitu_XMLǃxiaolu48ǃLOADER
		getChildById(id: 'n432_awmy'): newluzhitu_XMLǃxiaolu48ǃLOADER
		getChild(name: 'xiaolu49'): newluzhitu_XMLǃxiaolu49ǃLOADER
		getChildAt(index: 431): newluzhitu_XMLǃxiaolu49ǃLOADER
		getChildById(id: 'n433_awmy'): newluzhitu_XMLǃxiaolu49ǃLOADER
		getChild(name: 'xiaolu50'): newluzhitu_XMLǃxiaolu50ǃLOADER
		getChildAt(index: 432): newluzhitu_XMLǃxiaolu50ǃLOADER
		getChildById(id: 'n434_awmy'): newluzhitu_XMLǃxiaolu50ǃLOADER
		getChild(name: 'xiaolu51'): newluzhitu_XMLǃxiaolu51ǃLOADER
		getChildAt(index: 433): newluzhitu_XMLǃxiaolu51ǃLOADER
		getChildById(id: 'n435_awmy'): newluzhitu_XMLǃxiaolu51ǃLOADER
		getChild(name: 'xiaolu52'): newluzhitu_XMLǃxiaolu52ǃLOADER
		getChildAt(index: 434): newluzhitu_XMLǃxiaolu52ǃLOADER
		getChildById(id: 'n436_awmy'): newluzhitu_XMLǃxiaolu52ǃLOADER
		getChild(name: 'xiaolu53'): newluzhitu_XMLǃxiaolu53ǃLOADER
		getChildAt(index: 435): newluzhitu_XMLǃxiaolu53ǃLOADER
		getChildById(id: 'n437_awmy'): newluzhitu_XMLǃxiaolu53ǃLOADER
		getChild(name: 'xiaolu54'): newluzhitu_XMLǃxiaolu54ǃLOADER
		getChildAt(index: 436): newluzhitu_XMLǃxiaolu54ǃLOADER
		getChildById(id: 'n438_awmy'): newluzhitu_XMLǃxiaolu54ǃLOADER
		getChild(name: 'xiaolu55'): newluzhitu_XMLǃxiaolu55ǃLOADER
		getChildAt(index: 437): newluzhitu_XMLǃxiaolu55ǃLOADER
		getChildById(id: 'n439_awmy'): newluzhitu_XMLǃxiaolu55ǃLOADER
		getChild(name: 'xiaolu56'): newluzhitu_XMLǃxiaolu56ǃLOADER
		getChildAt(index: 438): newluzhitu_XMLǃxiaolu56ǃLOADER
		getChildById(id: 'n440_awmy'): newluzhitu_XMLǃxiaolu56ǃLOADER
		getChild(name: 'xiaolu57'): newluzhitu_XMLǃxiaolu57ǃLOADER
		getChildAt(index: 439): newluzhitu_XMLǃxiaolu57ǃLOADER
		getChildById(id: 'n441_awmy'): newluzhitu_XMLǃxiaolu57ǃLOADER
		getChild(name: 'xiaolu58'): newluzhitu_XMLǃxiaolu58ǃLOADER
		getChildAt(index: 440): newluzhitu_XMLǃxiaolu58ǃLOADER
		getChildById(id: 'n442_awmy'): newluzhitu_XMLǃxiaolu58ǃLOADER
		getChild(name: 'xiaolu59'): newluzhitu_XMLǃxiaolu59ǃLOADER
		getChildAt(index: 441): newluzhitu_XMLǃxiaolu59ǃLOADER
		getChildById(id: 'n443_awmy'): newluzhitu_XMLǃxiaolu59ǃLOADER
		getChild(name: 'xiaolu60'): newluzhitu_XMLǃxiaolu60ǃLOADER
		getChildAt(index: 442): newluzhitu_XMLǃxiaolu60ǃLOADER
		getChildById(id: 'n444_awmy'): newluzhitu_XMLǃxiaolu60ǃLOADER
		getChild(name: 'xiaolu61'): newluzhitu_XMLǃxiaolu61ǃLOADER
		getChildAt(index: 443): newluzhitu_XMLǃxiaolu61ǃLOADER
		getChildById(id: 'n445_awmy'): newluzhitu_XMLǃxiaolu61ǃLOADER
		getChild(name: 'xiaolu62'): newluzhitu_XMLǃxiaolu62ǃLOADER
		getChildAt(index: 444): newluzhitu_XMLǃxiaolu62ǃLOADER
		getChildById(id: 'n446_awmy'): newluzhitu_XMLǃxiaolu62ǃLOADER
		getChild(name: 'xiaolu63'): newluzhitu_XMLǃxiaolu63ǃLOADER
		getChildAt(index: 445): newluzhitu_XMLǃxiaolu63ǃLOADER
		getChildById(id: 'n447_awmy'): newluzhitu_XMLǃxiaolu63ǃLOADER
		getChild(name: 'xiaolu64'): newluzhitu_XMLǃxiaolu64ǃLOADER
		getChildAt(index: 446): newluzhitu_XMLǃxiaolu64ǃLOADER
		getChildById(id: 'n448_awmy'): newluzhitu_XMLǃxiaolu64ǃLOADER
		getChild(name: 'xiaolu65'): newluzhitu_XMLǃxiaolu65ǃLOADER
		getChildAt(index: 447): newluzhitu_XMLǃxiaolu65ǃLOADER
		getChildById(id: 'n449_awmy'): newluzhitu_XMLǃxiaolu65ǃLOADER
		getChild(name: 'xiaolu66'): newluzhitu_XMLǃxiaolu66ǃLOADER
		getChildAt(index: 448): newluzhitu_XMLǃxiaolu66ǃLOADER
		getChildById(id: 'n450_awmy'): newluzhitu_XMLǃxiaolu66ǃLOADER
		getChild(name: 'xiaolu67'): newluzhitu_XMLǃxiaolu67ǃLOADER
		getChildAt(index: 449): newluzhitu_XMLǃxiaolu67ǃLOADER
		getChildById(id: 'n451_awmy'): newluzhitu_XMLǃxiaolu67ǃLOADER
		getChild(name: 'xiaolu68'): newluzhitu_XMLǃxiaolu68ǃLOADER
		getChildAt(index: 450): newluzhitu_XMLǃxiaolu68ǃLOADER
		getChildById(id: 'n452_awmy'): newluzhitu_XMLǃxiaolu68ǃLOADER
		getChild(name: 'xiaolu69'): newluzhitu_XMLǃxiaolu69ǃLOADER
		getChildAt(index: 451): newluzhitu_XMLǃxiaolu69ǃLOADER
		getChildById(id: 'n453_awmy'): newluzhitu_XMLǃxiaolu69ǃLOADER
		getChild(name: 'xiaolu70'): newluzhitu_XMLǃxiaolu70ǃLOADER
		getChildAt(index: 452): newluzhitu_XMLǃxiaolu70ǃLOADER
		getChildById(id: 'n454_awmy'): newluzhitu_XMLǃxiaolu70ǃLOADER
		getChild(name: 'xiaolu71'): newluzhitu_XMLǃxiaolu71ǃLOADER
		getChildAt(index: 453): newluzhitu_XMLǃxiaolu71ǃLOADER
		getChildById(id: 'n455_awmy'): newluzhitu_XMLǃxiaolu71ǃLOADER
		getChild(name: 'xiaolu72'): newluzhitu_XMLǃxiaolu72ǃLOADER
		getChildAt(index: 454): newluzhitu_XMLǃxiaolu72ǃLOADER
		getChildById(id: 'n456_awmy'): newluzhitu_XMLǃxiaolu72ǃLOADER
		getChild(name: 'xiaolu73'): newluzhitu_XMLǃxiaolu73ǃLOADER
		getChildAt(index: 455): newluzhitu_XMLǃxiaolu73ǃLOADER
		getChildById(id: 'n457_awmy'): newluzhitu_XMLǃxiaolu73ǃLOADER
		getChild(name: 'xiaolu74'): newluzhitu_XMLǃxiaolu74ǃLOADER
		getChildAt(index: 456): newluzhitu_XMLǃxiaolu74ǃLOADER
		getChildById(id: 'n458_awmy'): newluzhitu_XMLǃxiaolu74ǃLOADER
		getChild(name: 'xiaolu75'): newluzhitu_XMLǃxiaolu75ǃLOADER
		getChildAt(index: 457): newluzhitu_XMLǃxiaolu75ǃLOADER
		getChildById(id: 'n459_awmy'): newluzhitu_XMLǃxiaolu75ǃLOADER
		getChild(name: 'xiaolu76'): newluzhitu_XMLǃxiaolu76ǃLOADER
		getChildAt(index: 458): newluzhitu_XMLǃxiaolu76ǃLOADER
		getChildById(id: 'n460_awmy'): newluzhitu_XMLǃxiaolu76ǃLOADER
		getChild(name: 'xiaolu77'): newluzhitu_XMLǃxiaolu77ǃLOADER
		getChildAt(index: 459): newluzhitu_XMLǃxiaolu77ǃLOADER
		getChildById(id: 'n461_awmy'): newluzhitu_XMLǃxiaolu77ǃLOADER
		getChild(name: 'xiaolu78'): newluzhitu_XMLǃxiaolu78ǃLOADER
		getChildAt(index: 460): newluzhitu_XMLǃxiaolu78ǃLOADER
		getChildById(id: 'n462_awmy'): newluzhitu_XMLǃxiaolu78ǃLOADER
		getChild(name: 'xiaolu79'): newluzhitu_XMLǃxiaolu79ǃLOADER
		getChildAt(index: 461): newluzhitu_XMLǃxiaolu79ǃLOADER
		getChildById(id: 'n463_awmy'): newluzhitu_XMLǃxiaolu79ǃLOADER
		getChild(name: 'xiaolu80'): newluzhitu_XMLǃxiaolu80ǃLOADER
		getChildAt(index: 462): newluzhitu_XMLǃxiaolu80ǃLOADER
		getChildById(id: 'n464_awmy'): newluzhitu_XMLǃxiaolu80ǃLOADER
		getChild(name: 'xiaolu81'): newluzhitu_XMLǃxiaolu81ǃLOADER
		getChildAt(index: 463): newluzhitu_XMLǃxiaolu81ǃLOADER
		getChildById(id: 'n465_awmy'): newluzhitu_XMLǃxiaolu81ǃLOADER
		getChild(name: 'xiaolu82'): newluzhitu_XMLǃxiaolu82ǃLOADER
		getChildAt(index: 464): newluzhitu_XMLǃxiaolu82ǃLOADER
		getChildById(id: 'n466_awmy'): newluzhitu_XMLǃxiaolu82ǃLOADER
		getChild(name: 'xiaolu83'): newluzhitu_XMLǃxiaolu83ǃLOADER
		getChildAt(index: 465): newluzhitu_XMLǃxiaolu83ǃLOADER
		getChildById(id: 'n467_awmy'): newluzhitu_XMLǃxiaolu83ǃLOADER
		getChild(name: 'xiaolu84'): newluzhitu_XMLǃxiaolu84ǃLOADER
		getChildAt(index: 466): newluzhitu_XMLǃxiaolu84ǃLOADER
		getChildById(id: 'n468_awmy'): newluzhitu_XMLǃxiaolu84ǃLOADER
		getChild(name: 'xiaolu85'): newluzhitu_XMLǃxiaolu85ǃLOADER
		getChildAt(index: 467): newluzhitu_XMLǃxiaolu85ǃLOADER
		getChildById(id: 'n469_awmy'): newluzhitu_XMLǃxiaolu85ǃLOADER
		getChild(name: 'xiaolu86'): newluzhitu_XMLǃxiaolu86ǃLOADER
		getChildAt(index: 468): newluzhitu_XMLǃxiaolu86ǃLOADER
		getChildById(id: 'n470_awmy'): newluzhitu_XMLǃxiaolu86ǃLOADER
		getChild(name: 'xiaolu87'): newluzhitu_XMLǃxiaolu87ǃLOADER
		getChildAt(index: 469): newluzhitu_XMLǃxiaolu87ǃLOADER
		getChildById(id: 'n471_awmy'): newluzhitu_XMLǃxiaolu87ǃLOADER
		getChild(name: 'xiaolu88'): newluzhitu_XMLǃxiaolu88ǃLOADER
		getChildAt(index: 470): newluzhitu_XMLǃxiaolu88ǃLOADER
		getChildById(id: 'n472_awmy'): newluzhitu_XMLǃxiaolu88ǃLOADER
		getChild(name: 'xiaolu89'): newluzhitu_XMLǃxiaolu89ǃLOADER
		getChildAt(index: 471): newluzhitu_XMLǃxiaolu89ǃLOADER
		getChildById(id: 'n473_awmy'): newluzhitu_XMLǃxiaolu89ǃLOADER
		getChild(name: 'xiaolu90'): newluzhitu_XMLǃxiaolu90ǃLOADER
		getChildAt(index: 472): newluzhitu_XMLǃxiaolu90ǃLOADER
		getChildById(id: 'n474_awmy'): newluzhitu_XMLǃxiaolu90ǃLOADER
		getChild(name: 'xiaolu91'): newluzhitu_XMLǃxiaolu91ǃLOADER
		getChildAt(index: 473): newluzhitu_XMLǃxiaolu91ǃLOADER
		getChildById(id: 'n475_awmy'): newluzhitu_XMLǃxiaolu91ǃLOADER
		getChild(name: 'xiaolu92'): newluzhitu_XMLǃxiaolu92ǃLOADER
		getChildAt(index: 474): newluzhitu_XMLǃxiaolu92ǃLOADER
		getChildById(id: 'n476_awmy'): newluzhitu_XMLǃxiaolu92ǃLOADER
		getChild(name: 'xiaolu93'): newluzhitu_XMLǃxiaolu93ǃLOADER
		getChildAt(index: 475): newluzhitu_XMLǃxiaolu93ǃLOADER
		getChildById(id: 'n477_awmy'): newluzhitu_XMLǃxiaolu93ǃLOADER
		getChild(name: 'xiaolu94'): newluzhitu_XMLǃxiaolu94ǃLOADER
		getChildAt(index: 476): newluzhitu_XMLǃxiaolu94ǃLOADER
		getChildById(id: 'n478_awmy'): newluzhitu_XMLǃxiaolu94ǃLOADER
		getChild(name: 'xiaolu95'): newluzhitu_XMLǃxiaolu95ǃLOADER
		getChildAt(index: 477): newluzhitu_XMLǃxiaolu95ǃLOADER
		getChildById(id: 'n479_awmy'): newluzhitu_XMLǃxiaolu95ǃLOADER
		getChild(name: 'xiaoqianglu0'): newluzhitu_XMLǃxiaoqianglu0ǃLOADER
		getChildAt(index: 478): newluzhitu_XMLǃxiaoqianglu0ǃLOADER
		getChildById(id: 'n480_awmy'): newluzhitu_XMLǃxiaoqianglu0ǃLOADER
		getChild(name: 'xiaoqianglu1'): newluzhitu_XMLǃxiaoqianglu1ǃLOADER
		getChildAt(index: 479): newluzhitu_XMLǃxiaoqianglu1ǃLOADER
		getChildById(id: 'n481_awmy'): newluzhitu_XMLǃxiaoqianglu1ǃLOADER
		getChild(name: 'xiaoqianglu2'): newluzhitu_XMLǃxiaoqianglu2ǃLOADER
		getChildAt(index: 480): newluzhitu_XMLǃxiaoqianglu2ǃLOADER
		getChildById(id: 'n482_awmy'): newluzhitu_XMLǃxiaoqianglu2ǃLOADER
		getChild(name: 'xiaoqianglu3'): newluzhitu_XMLǃxiaoqianglu3ǃLOADER
		getChildAt(index: 481): newluzhitu_XMLǃxiaoqianglu3ǃLOADER
		getChildById(id: 'n483_awmy'): newluzhitu_XMLǃxiaoqianglu3ǃLOADER
		getChild(name: 'xiaoqianglu4'): newluzhitu_XMLǃxiaoqianglu4ǃLOADER
		getChildAt(index: 482): newluzhitu_XMLǃxiaoqianglu4ǃLOADER
		getChildById(id: 'n484_awmy'): newluzhitu_XMLǃxiaoqianglu4ǃLOADER
		getChild(name: 'xiaoqianglu5'): newluzhitu_XMLǃxiaoqianglu5ǃLOADER
		getChildAt(index: 483): newluzhitu_XMLǃxiaoqianglu5ǃLOADER
		getChildById(id: 'n485_awmy'): newluzhitu_XMLǃxiaoqianglu5ǃLOADER
		getChild(name: 'xiaoqianglu6'): newluzhitu_XMLǃxiaoqianglu6ǃLOADER
		getChildAt(index: 484): newluzhitu_XMLǃxiaoqianglu6ǃLOADER
		getChildById(id: 'n486_awmy'): newluzhitu_XMLǃxiaoqianglu6ǃLOADER
		getChild(name: 'xiaoqianglu7'): newluzhitu_XMLǃxiaoqianglu7ǃLOADER
		getChildAt(index: 485): newluzhitu_XMLǃxiaoqianglu7ǃLOADER
		getChildById(id: 'n487_awmy'): newluzhitu_XMLǃxiaoqianglu7ǃLOADER
		getChild(name: 'xiaoqianglu8'): newluzhitu_XMLǃxiaoqianglu8ǃLOADER
		getChildAt(index: 486): newluzhitu_XMLǃxiaoqianglu8ǃLOADER
		getChildById(id: 'n488_awmy'): newluzhitu_XMLǃxiaoqianglu8ǃLOADER
		getChild(name: 'xiaoqianglu9'): newluzhitu_XMLǃxiaoqianglu9ǃLOADER
		getChildAt(index: 487): newluzhitu_XMLǃxiaoqianglu9ǃLOADER
		getChildById(id: 'n489_awmy'): newluzhitu_XMLǃxiaoqianglu9ǃLOADER
		getChild(name: 'xiaoqianglu10'): newluzhitu_XMLǃxiaoqianglu10ǃLOADER
		getChildAt(index: 488): newluzhitu_XMLǃxiaoqianglu10ǃLOADER
		getChildById(id: 'n490_awmy'): newluzhitu_XMLǃxiaoqianglu10ǃLOADER
		getChild(name: 'xiaoqianglu11'): newluzhitu_XMLǃxiaoqianglu11ǃLOADER
		getChildAt(index: 489): newluzhitu_XMLǃxiaoqianglu11ǃLOADER
		getChildById(id: 'n491_awmy'): newluzhitu_XMLǃxiaoqianglu11ǃLOADER
		getChild(name: 'xiaoqianglu12'): newluzhitu_XMLǃxiaoqianglu12ǃLOADER
		getChildAt(index: 490): newluzhitu_XMLǃxiaoqianglu12ǃLOADER
		getChildById(id: 'n492_awmy'): newluzhitu_XMLǃxiaoqianglu12ǃLOADER
		getChild(name: 'xiaoqianglu13'): newluzhitu_XMLǃxiaoqianglu13ǃLOADER
		getChildAt(index: 491): newluzhitu_XMLǃxiaoqianglu13ǃLOADER
		getChildById(id: 'n493_awmy'): newluzhitu_XMLǃxiaoqianglu13ǃLOADER
		getChild(name: 'xiaoqianglu14'): newluzhitu_XMLǃxiaoqianglu14ǃLOADER
		getChildAt(index: 492): newluzhitu_XMLǃxiaoqianglu14ǃLOADER
		getChildById(id: 'n494_awmy'): newluzhitu_XMLǃxiaoqianglu14ǃLOADER
		getChild(name: 'xiaoqianglu15'): newluzhitu_XMLǃxiaoqianglu15ǃLOADER
		getChildAt(index: 493): newluzhitu_XMLǃxiaoqianglu15ǃLOADER
		getChildById(id: 'n495_awmy'): newluzhitu_XMLǃxiaoqianglu15ǃLOADER
		getChild(name: 'xiaoqianglu16'): newluzhitu_XMLǃxiaoqianglu16ǃLOADER
		getChildAt(index: 494): newluzhitu_XMLǃxiaoqianglu16ǃLOADER
		getChildById(id: 'n496_awmy'): newluzhitu_XMLǃxiaoqianglu16ǃLOADER
		getChild(name: 'xiaoqianglu17'): newluzhitu_XMLǃxiaoqianglu17ǃLOADER
		getChildAt(index: 495): newluzhitu_XMLǃxiaoqianglu17ǃLOADER
		getChildById(id: 'n497_awmy'): newluzhitu_XMLǃxiaoqianglu17ǃLOADER
		getChild(name: 'xiaoqianglu18'): newluzhitu_XMLǃxiaoqianglu18ǃLOADER
		getChildAt(index: 496): newluzhitu_XMLǃxiaoqianglu18ǃLOADER
		getChildById(id: 'n498_awmy'): newluzhitu_XMLǃxiaoqianglu18ǃLOADER
		getChild(name: 'xiaoqianglu19'): newluzhitu_XMLǃxiaoqianglu19ǃLOADER
		getChildAt(index: 497): newluzhitu_XMLǃxiaoqianglu19ǃLOADER
		getChildById(id: 'n499_awmy'): newluzhitu_XMLǃxiaoqianglu19ǃLOADER
		getChild(name: 'xiaoqianglu20'): newluzhitu_XMLǃxiaoqianglu20ǃLOADER
		getChildAt(index: 498): newluzhitu_XMLǃxiaoqianglu20ǃLOADER
		getChildById(id: 'n500_awmy'): newluzhitu_XMLǃxiaoqianglu20ǃLOADER
		getChild(name: 'xiaoqianglu21'): newluzhitu_XMLǃxiaoqianglu21ǃLOADER
		getChildAt(index: 499): newluzhitu_XMLǃxiaoqianglu21ǃLOADER
		getChildById(id: 'n501_awmy'): newluzhitu_XMLǃxiaoqianglu21ǃLOADER
		getChild(name: 'xiaoqianglu22'): newluzhitu_XMLǃxiaoqianglu22ǃLOADER
		getChildAt(index: 500): newluzhitu_XMLǃxiaoqianglu22ǃLOADER
		getChildById(id: 'n502_awmy'): newluzhitu_XMLǃxiaoqianglu22ǃLOADER
		getChild(name: 'xiaoqianglu23'): newluzhitu_XMLǃxiaoqianglu23ǃLOADER
		getChildAt(index: 501): newluzhitu_XMLǃxiaoqianglu23ǃLOADER
		getChildById(id: 'n503_awmy'): newluzhitu_XMLǃxiaoqianglu23ǃLOADER
		getChild(name: 'xiaoqianglu24'): newluzhitu_XMLǃxiaoqianglu24ǃLOADER
		getChildAt(index: 502): newluzhitu_XMLǃxiaoqianglu24ǃLOADER
		getChildById(id: 'n504_awmy'): newluzhitu_XMLǃxiaoqianglu24ǃLOADER
		getChild(name: 'xiaoqianglu25'): newluzhitu_XMLǃxiaoqianglu25ǃLOADER
		getChildAt(index: 503): newluzhitu_XMLǃxiaoqianglu25ǃLOADER
		getChildById(id: 'n505_awmy'): newluzhitu_XMLǃxiaoqianglu25ǃLOADER
		getChild(name: 'xiaoqianglu26'): newluzhitu_XMLǃxiaoqianglu26ǃLOADER
		getChildAt(index: 504): newluzhitu_XMLǃxiaoqianglu26ǃLOADER
		getChildById(id: 'n506_awmy'): newluzhitu_XMLǃxiaoqianglu26ǃLOADER
		getChild(name: 'xiaoqianglu27'): newluzhitu_XMLǃxiaoqianglu27ǃLOADER
		getChildAt(index: 505): newluzhitu_XMLǃxiaoqianglu27ǃLOADER
		getChildById(id: 'n507_awmy'): newluzhitu_XMLǃxiaoqianglu27ǃLOADER
		getChild(name: 'xiaoqianglu28'): newluzhitu_XMLǃxiaoqianglu28ǃLOADER
		getChildAt(index: 506): newluzhitu_XMLǃxiaoqianglu28ǃLOADER
		getChildById(id: 'n508_awmy'): newluzhitu_XMLǃxiaoqianglu28ǃLOADER
		getChild(name: 'xiaoqianglu29'): newluzhitu_XMLǃxiaoqianglu29ǃLOADER
		getChildAt(index: 507): newluzhitu_XMLǃxiaoqianglu29ǃLOADER
		getChildById(id: 'n509_awmy'): newluzhitu_XMLǃxiaoqianglu29ǃLOADER
		getChild(name: 'xiaoqianglu30'): newluzhitu_XMLǃxiaoqianglu30ǃLOADER
		getChildAt(index: 508): newluzhitu_XMLǃxiaoqianglu30ǃLOADER
		getChildById(id: 'n510_awmy'): newluzhitu_XMLǃxiaoqianglu30ǃLOADER
		getChild(name: 'xiaoqianglu31'): newluzhitu_XMLǃxiaoqianglu31ǃLOADER
		getChildAt(index: 509): newluzhitu_XMLǃxiaoqianglu31ǃLOADER
		getChildById(id: 'n511_awmy'): newluzhitu_XMLǃxiaoqianglu31ǃLOADER
		getChild(name: 'xiaoqianglu32'): newluzhitu_XMLǃxiaoqianglu32ǃLOADER
		getChildAt(index: 510): newluzhitu_XMLǃxiaoqianglu32ǃLOADER
		getChildById(id: 'n512_awmy'): newluzhitu_XMLǃxiaoqianglu32ǃLOADER
		getChild(name: 'xiaoqianglu33'): newluzhitu_XMLǃxiaoqianglu33ǃLOADER
		getChildAt(index: 511): newluzhitu_XMLǃxiaoqianglu33ǃLOADER
		getChildById(id: 'n513_awmy'): newluzhitu_XMLǃxiaoqianglu33ǃLOADER
		getChild(name: 'xiaoqianglu34'): newluzhitu_XMLǃxiaoqianglu34ǃLOADER
		getChildAt(index: 512): newluzhitu_XMLǃxiaoqianglu34ǃLOADER
		getChildById(id: 'n514_awmy'): newluzhitu_XMLǃxiaoqianglu34ǃLOADER
		getChild(name: 'xiaoqianglu35'): newluzhitu_XMLǃxiaoqianglu35ǃLOADER
		getChildAt(index: 513): newluzhitu_XMLǃxiaoqianglu35ǃLOADER
		getChildById(id: 'n515_awmy'): newluzhitu_XMLǃxiaoqianglu35ǃLOADER
		getChild(name: 'xiaoqianglu36'): newluzhitu_XMLǃxiaoqianglu36ǃLOADER
		getChildAt(index: 514): newluzhitu_XMLǃxiaoqianglu36ǃLOADER
		getChildById(id: 'n516_awmy'): newluzhitu_XMLǃxiaoqianglu36ǃLOADER
		getChild(name: 'xiaoqianglu37'): newluzhitu_XMLǃxiaoqianglu37ǃLOADER
		getChildAt(index: 515): newluzhitu_XMLǃxiaoqianglu37ǃLOADER
		getChildById(id: 'n517_awmy'): newluzhitu_XMLǃxiaoqianglu37ǃLOADER
		getChild(name: 'xiaoqianglu38'): newluzhitu_XMLǃxiaoqianglu38ǃLOADER
		getChildAt(index: 516): newluzhitu_XMLǃxiaoqianglu38ǃLOADER
		getChildById(id: 'n518_awmy'): newluzhitu_XMLǃxiaoqianglu38ǃLOADER
		getChild(name: 'xiaoqianglu39'): newluzhitu_XMLǃxiaoqianglu39ǃLOADER
		getChildAt(index: 517): newluzhitu_XMLǃxiaoqianglu39ǃLOADER
		getChildById(id: 'n519_awmy'): newluzhitu_XMLǃxiaoqianglu39ǃLOADER
		getChild(name: 'xiaoqianglu40'): newluzhitu_XMLǃxiaoqianglu40ǃLOADER
		getChildAt(index: 518): newluzhitu_XMLǃxiaoqianglu40ǃLOADER
		getChildById(id: 'n520_awmy'): newluzhitu_XMLǃxiaoqianglu40ǃLOADER
		getChild(name: 'xiaoqianglu41'): newluzhitu_XMLǃxiaoqianglu41ǃLOADER
		getChildAt(index: 519): newluzhitu_XMLǃxiaoqianglu41ǃLOADER
		getChildById(id: 'n521_awmy'): newluzhitu_XMLǃxiaoqianglu41ǃLOADER
		getChild(name: 'xiaoqianglu42'): newluzhitu_XMLǃxiaoqianglu42ǃLOADER
		getChildAt(index: 520): newluzhitu_XMLǃxiaoqianglu42ǃLOADER
		getChildById(id: 'n522_awmy'): newluzhitu_XMLǃxiaoqianglu42ǃLOADER
		getChild(name: 'xiaoqianglu43'): newluzhitu_XMLǃxiaoqianglu43ǃLOADER
		getChildAt(index: 521): newluzhitu_XMLǃxiaoqianglu43ǃLOADER
		getChildById(id: 'n523_awmy'): newluzhitu_XMLǃxiaoqianglu43ǃLOADER
		getChild(name: 'xiaoqianglu44'): newluzhitu_XMLǃxiaoqianglu44ǃLOADER
		getChildAt(index: 522): newluzhitu_XMLǃxiaoqianglu44ǃLOADER
		getChildById(id: 'n524_awmy'): newluzhitu_XMLǃxiaoqianglu44ǃLOADER
		getChild(name: 'xiaoqianglu45'): newluzhitu_XMLǃxiaoqianglu45ǃLOADER
		getChildAt(index: 523): newluzhitu_XMLǃxiaoqianglu45ǃLOADER
		getChildById(id: 'n525_awmy'): newluzhitu_XMLǃxiaoqianglu45ǃLOADER
		getChild(name: 'xiaoqianglu46'): newluzhitu_XMLǃxiaoqianglu46ǃLOADER
		getChildAt(index: 524): newluzhitu_XMLǃxiaoqianglu46ǃLOADER
		getChildById(id: 'n526_awmy'): newluzhitu_XMLǃxiaoqianglu46ǃLOADER
		getChild(name: 'xiaoqianglu47'): newluzhitu_XMLǃxiaoqianglu47ǃLOADER
		getChildAt(index: 525): newluzhitu_XMLǃxiaoqianglu47ǃLOADER
		getChildById(id: 'n527_awmy'): newluzhitu_XMLǃxiaoqianglu47ǃLOADER
		getChild(name: 'xiaoqianglu48'): newluzhitu_XMLǃxiaoqianglu48ǃLOADER
		getChildAt(index: 526): newluzhitu_XMLǃxiaoqianglu48ǃLOADER
		getChildById(id: 'n528_awmy'): newluzhitu_XMLǃxiaoqianglu48ǃLOADER
		getChild(name: 'xiaoqianglu49'): newluzhitu_XMLǃxiaoqianglu49ǃLOADER
		getChildAt(index: 527): newluzhitu_XMLǃxiaoqianglu49ǃLOADER
		getChildById(id: 'n529_awmy'): newluzhitu_XMLǃxiaoqianglu49ǃLOADER
		getChild(name: 'xiaoqianglu50'): newluzhitu_XMLǃxiaoqianglu50ǃLOADER
		getChildAt(index: 528): newluzhitu_XMLǃxiaoqianglu50ǃLOADER
		getChildById(id: 'n530_awmy'): newluzhitu_XMLǃxiaoqianglu50ǃLOADER
		getChild(name: 'xiaoqianglu51'): newluzhitu_XMLǃxiaoqianglu51ǃLOADER
		getChildAt(index: 529): newluzhitu_XMLǃxiaoqianglu51ǃLOADER
		getChildById(id: 'n531_awmy'): newluzhitu_XMLǃxiaoqianglu51ǃLOADER
		getChild(name: 'xiaoqianglu52'): newluzhitu_XMLǃxiaoqianglu52ǃLOADER
		getChildAt(index: 530): newluzhitu_XMLǃxiaoqianglu52ǃLOADER
		getChildById(id: 'n532_awmy'): newluzhitu_XMLǃxiaoqianglu52ǃLOADER
		getChild(name: 'xiaoqianglu53'): newluzhitu_XMLǃxiaoqianglu53ǃLOADER
		getChildAt(index: 531): newluzhitu_XMLǃxiaoqianglu53ǃLOADER
		getChildById(id: 'n533_awmy'): newluzhitu_XMLǃxiaoqianglu53ǃLOADER
		getChild(name: 'xiaoqianglu54'): newluzhitu_XMLǃxiaoqianglu54ǃLOADER
		getChildAt(index: 532): newluzhitu_XMLǃxiaoqianglu54ǃLOADER
		getChildById(id: 'n534_awmy'): newluzhitu_XMLǃxiaoqianglu54ǃLOADER
		getChild(name: 'xiaoqianglu55'): newluzhitu_XMLǃxiaoqianglu55ǃLOADER
		getChildAt(index: 533): newluzhitu_XMLǃxiaoqianglu55ǃLOADER
		getChildById(id: 'n535_awmy'): newluzhitu_XMLǃxiaoqianglu55ǃLOADER
		getChild(name: 'xiaoqianglu56'): newluzhitu_XMLǃxiaoqianglu56ǃLOADER
		getChildAt(index: 534): newluzhitu_XMLǃxiaoqianglu56ǃLOADER
		getChildById(id: 'n536_awmy'): newluzhitu_XMLǃxiaoqianglu56ǃLOADER
		getChild(name: 'xiaoqianglu57'): newluzhitu_XMLǃxiaoqianglu57ǃLOADER
		getChildAt(index: 535): newluzhitu_XMLǃxiaoqianglu57ǃLOADER
		getChildById(id: 'n537_awmy'): newluzhitu_XMLǃxiaoqianglu57ǃLOADER
		getChild(name: 'xiaoqianglu58'): newluzhitu_XMLǃxiaoqianglu58ǃLOADER
		getChildAt(index: 536): newluzhitu_XMLǃxiaoqianglu58ǃLOADER
		getChildById(id: 'n538_awmy'): newluzhitu_XMLǃxiaoqianglu58ǃLOADER
		getChild(name: 'xiaoqianglu59'): newluzhitu_XMLǃxiaoqianglu59ǃLOADER
		getChildAt(index: 537): newluzhitu_XMLǃxiaoqianglu59ǃLOADER
		getChildById(id: 'n539_awmy'): newluzhitu_XMLǃxiaoqianglu59ǃLOADER
		getChild(name: 'xiaoqianglu60'): newluzhitu_XMLǃxiaoqianglu60ǃLOADER
		getChildAt(index: 538): newluzhitu_XMLǃxiaoqianglu60ǃLOADER
		getChildById(id: 'n540_awmy'): newluzhitu_XMLǃxiaoqianglu60ǃLOADER
		getChild(name: 'xiaoqianglu61'): newluzhitu_XMLǃxiaoqianglu61ǃLOADER
		getChildAt(index: 539): newluzhitu_XMLǃxiaoqianglu61ǃLOADER
		getChildById(id: 'n541_awmy'): newluzhitu_XMLǃxiaoqianglu61ǃLOADER
		getChild(name: 'xiaoqianglu62'): newluzhitu_XMLǃxiaoqianglu62ǃLOADER
		getChildAt(index: 540): newluzhitu_XMLǃxiaoqianglu62ǃLOADER
		getChildById(id: 'n542_awmy'): newluzhitu_XMLǃxiaoqianglu62ǃLOADER
		getChild(name: 'xiaoqianglu63'): newluzhitu_XMLǃxiaoqianglu63ǃLOADER
		getChildAt(index: 541): newluzhitu_XMLǃxiaoqianglu63ǃLOADER
		getChildById(id: 'n543_awmy'): newluzhitu_XMLǃxiaoqianglu63ǃLOADER
		getChild(name: 'xiaoqianglu64'): newluzhitu_XMLǃxiaoqianglu64ǃLOADER
		getChildAt(index: 542): newluzhitu_XMLǃxiaoqianglu64ǃLOADER
		getChildById(id: 'n544_awmy'): newluzhitu_XMLǃxiaoqianglu64ǃLOADER
		getChild(name: 'xiaoqianglu65'): newluzhitu_XMLǃxiaoqianglu65ǃLOADER
		getChildAt(index: 543): newluzhitu_XMLǃxiaoqianglu65ǃLOADER
		getChildById(id: 'n545_awmy'): newluzhitu_XMLǃxiaoqianglu65ǃLOADER
		getChild(name: 'xiaoqianglu66'): newluzhitu_XMLǃxiaoqianglu66ǃLOADER
		getChildAt(index: 544): newluzhitu_XMLǃxiaoqianglu66ǃLOADER
		getChildById(id: 'n546_awmy'): newluzhitu_XMLǃxiaoqianglu66ǃLOADER
		getChild(name: 'xiaoqianglu67'): newluzhitu_XMLǃxiaoqianglu67ǃLOADER
		getChildAt(index: 545): newluzhitu_XMLǃxiaoqianglu67ǃLOADER
		getChildById(id: 'n547_awmy'): newluzhitu_XMLǃxiaoqianglu67ǃLOADER
		getChild(name: 'xiaoqianglu68'): newluzhitu_XMLǃxiaoqianglu68ǃLOADER
		getChildAt(index: 546): newluzhitu_XMLǃxiaoqianglu68ǃLOADER
		getChildById(id: 'n548_awmy'): newluzhitu_XMLǃxiaoqianglu68ǃLOADER
		getChild(name: 'xiaoqianglu69'): newluzhitu_XMLǃxiaoqianglu69ǃLOADER
		getChildAt(index: 547): newluzhitu_XMLǃxiaoqianglu69ǃLOADER
		getChildById(id: 'n549_awmy'): newluzhitu_XMLǃxiaoqianglu69ǃLOADER
		getChild(name: 'xiaoqianglu70'): newluzhitu_XMLǃxiaoqianglu70ǃLOADER
		getChildAt(index: 548): newluzhitu_XMLǃxiaoqianglu70ǃLOADER
		getChildById(id: 'n550_awmy'): newluzhitu_XMLǃxiaoqianglu70ǃLOADER
		getChild(name: 'xiaoqianglu71'): newluzhitu_XMLǃxiaoqianglu71ǃLOADER
		getChildAt(index: 549): newluzhitu_XMLǃxiaoqianglu71ǃLOADER
		getChildById(id: 'n551_awmy'): newluzhitu_XMLǃxiaoqianglu71ǃLOADER
		getChild(name: 'xiaoqianglu72'): newluzhitu_XMLǃxiaoqianglu72ǃLOADER
		getChildAt(index: 550): newluzhitu_XMLǃxiaoqianglu72ǃLOADER
		getChildById(id: 'n552_awmy'): newluzhitu_XMLǃxiaoqianglu72ǃLOADER
		getChild(name: 'xiaoqianglu73'): newluzhitu_XMLǃxiaoqianglu73ǃLOADER
		getChildAt(index: 551): newluzhitu_XMLǃxiaoqianglu73ǃLOADER
		getChildById(id: 'n553_awmy'): newluzhitu_XMLǃxiaoqianglu73ǃLOADER
		getChild(name: 'xiaoqianglu74'): newluzhitu_XMLǃxiaoqianglu74ǃLOADER
		getChildAt(index: 552): newluzhitu_XMLǃxiaoqianglu74ǃLOADER
		getChildById(id: 'n554_awmy'): newluzhitu_XMLǃxiaoqianglu74ǃLOADER
		getChild(name: 'xiaoqianglu75'): newluzhitu_XMLǃxiaoqianglu75ǃLOADER
		getChildAt(index: 553): newluzhitu_XMLǃxiaoqianglu75ǃLOADER
		getChildById(id: 'n555_awmy'): newluzhitu_XMLǃxiaoqianglu75ǃLOADER
		getChild(name: 'xiaoqianglu76'): newluzhitu_XMLǃxiaoqianglu76ǃLOADER
		getChildAt(index: 554): newluzhitu_XMLǃxiaoqianglu76ǃLOADER
		getChildById(id: 'n556_awmy'): newluzhitu_XMLǃxiaoqianglu76ǃLOADER
		getChild(name: 'xiaoqianglu77'): newluzhitu_XMLǃxiaoqianglu77ǃLOADER
		getChildAt(index: 555): newluzhitu_XMLǃxiaoqianglu77ǃLOADER
		getChildById(id: 'n557_awmy'): newluzhitu_XMLǃxiaoqianglu77ǃLOADER
		getChild(name: 'xiaoqianglu78'): newluzhitu_XMLǃxiaoqianglu78ǃLOADER
		getChildAt(index: 556): newluzhitu_XMLǃxiaoqianglu78ǃLOADER
		getChildById(id: 'n558_awmy'): newluzhitu_XMLǃxiaoqianglu78ǃLOADER
		getChild(name: 'xiaoqianglu79'): newluzhitu_XMLǃxiaoqianglu79ǃLOADER
		getChildAt(index: 557): newluzhitu_XMLǃxiaoqianglu79ǃLOADER
		getChildById(id: 'n559_awmy'): newluzhitu_XMLǃxiaoqianglu79ǃLOADER
		getChild(name: 'xiaoqianglu80'): newluzhitu_XMLǃxiaoqianglu80ǃLOADER
		getChildAt(index: 558): newluzhitu_XMLǃxiaoqianglu80ǃLOADER
		getChildById(id: 'n560_awmy'): newluzhitu_XMLǃxiaoqianglu80ǃLOADER
		getChild(name: 'xiaoqianglu81'): newluzhitu_XMLǃxiaoqianglu81ǃLOADER
		getChildAt(index: 559): newluzhitu_XMLǃxiaoqianglu81ǃLOADER
		getChildById(id: 'n561_awmy'): newluzhitu_XMLǃxiaoqianglu81ǃLOADER
		getChild(name: 'xiaoqianglu82'): newluzhitu_XMLǃxiaoqianglu82ǃLOADER
		getChildAt(index: 560): newluzhitu_XMLǃxiaoqianglu82ǃLOADER
		getChildById(id: 'n562_awmy'): newluzhitu_XMLǃxiaoqianglu82ǃLOADER
		getChild(name: 'xiaoqianglu83'): newluzhitu_XMLǃxiaoqianglu83ǃLOADER
		getChildAt(index: 561): newluzhitu_XMLǃxiaoqianglu83ǃLOADER
		getChildById(id: 'n563_awmy'): newluzhitu_XMLǃxiaoqianglu83ǃLOADER
		getChild(name: 'xiaoqianglu84'): newluzhitu_XMLǃxiaoqianglu84ǃLOADER
		getChildAt(index: 562): newluzhitu_XMLǃxiaoqianglu84ǃLOADER
		getChildById(id: 'n564_awmy'): newluzhitu_XMLǃxiaoqianglu84ǃLOADER
		getChild(name: 'xiaoqianglu85'): newluzhitu_XMLǃxiaoqianglu85ǃLOADER
		getChildAt(index: 563): newluzhitu_XMLǃxiaoqianglu85ǃLOADER
		getChildById(id: 'n565_awmy'): newluzhitu_XMLǃxiaoqianglu85ǃLOADER
		getChild(name: 'xiaoqianglu86'): newluzhitu_XMLǃxiaoqianglu86ǃLOADER
		getChildAt(index: 564): newluzhitu_XMLǃxiaoqianglu86ǃLOADER
		getChildById(id: 'n566_awmy'): newluzhitu_XMLǃxiaoqianglu86ǃLOADER
		getChild(name: 'xiaoqianglu87'): newluzhitu_XMLǃxiaoqianglu87ǃLOADER
		getChildAt(index: 565): newluzhitu_XMLǃxiaoqianglu87ǃLOADER
		getChildById(id: 'n567_awmy'): newluzhitu_XMLǃxiaoqianglu87ǃLOADER
		getChild(name: 'xiaoqianglu88'): newluzhitu_XMLǃxiaoqianglu88ǃLOADER
		getChildAt(index: 566): newluzhitu_XMLǃxiaoqianglu88ǃLOADER
		getChildById(id: 'n568_awmy'): newluzhitu_XMLǃxiaoqianglu88ǃLOADER
		getChild(name: 'xiaoqianglu89'): newluzhitu_XMLǃxiaoqianglu89ǃLOADER
		getChildAt(index: 567): newluzhitu_XMLǃxiaoqianglu89ǃLOADER
		getChildById(id: 'n569_awmy'): newluzhitu_XMLǃxiaoqianglu89ǃLOADER
		getChild(name: 'xiaoqianglu90'): newluzhitu_XMLǃxiaoqianglu90ǃLOADER
		getChildAt(index: 568): newluzhitu_XMLǃxiaoqianglu90ǃLOADER
		getChildById(id: 'n570_awmy'): newluzhitu_XMLǃxiaoqianglu90ǃLOADER
		getChild(name: 'xiaoqianglu91'): newluzhitu_XMLǃxiaoqianglu91ǃLOADER
		getChildAt(index: 569): newluzhitu_XMLǃxiaoqianglu91ǃLOADER
		getChildById(id: 'n571_awmy'): newluzhitu_XMLǃxiaoqianglu91ǃLOADER
		getChild(name: 'xiaoqianglu92'): newluzhitu_XMLǃxiaoqianglu92ǃLOADER
		getChildAt(index: 570): newluzhitu_XMLǃxiaoqianglu92ǃLOADER
		getChildById(id: 'n572_awmy'): newluzhitu_XMLǃxiaoqianglu92ǃLOADER
		getChild(name: 'xiaoqianglu93'): newluzhitu_XMLǃxiaoqianglu93ǃLOADER
		getChildAt(index: 571): newluzhitu_XMLǃxiaoqianglu93ǃLOADER
		getChildById(id: 'n573_awmy'): newluzhitu_XMLǃxiaoqianglu93ǃLOADER
		getChild(name: 'xiaoqianglu94'): newluzhitu_XMLǃxiaoqianglu94ǃLOADER
		getChildAt(index: 572): newluzhitu_XMLǃxiaoqianglu94ǃLOADER
		getChildById(id: 'n574_awmy'): newluzhitu_XMLǃxiaoqianglu94ǃLOADER
		getChild(name: 'xiaoqianglu95'): newluzhitu_XMLǃxiaoqianglu95ǃLOADER
		getChildAt(index: 573): newluzhitu_XMLǃxiaoqianglu95ǃLOADER
		getChildById(id: 'n575_awmy'): newluzhitu_XMLǃxiaoqianglu95ǃLOADER
		getChild(name: 'xiaoqianglu96'): newluzhitu_XMLǃxiaoqianglu96ǃLOADER
		getChildAt(index: 574): newluzhitu_XMLǃxiaoqianglu96ǃLOADER
		getChildById(id: 'n576_awmy'): newluzhitu_XMLǃxiaoqianglu96ǃLOADER
		getChild(name: 'xiaoqianglu97'): newluzhitu_XMLǃxiaoqianglu97ǃLOADER
		getChildAt(index: 575): newluzhitu_XMLǃxiaoqianglu97ǃLOADER
		getChildById(id: 'n577_awmy'): newluzhitu_XMLǃxiaoqianglu97ǃLOADER
		getChild(name: 'xiaoqianglu98'): newluzhitu_XMLǃxiaoqianglu98ǃLOADER
		getChildAt(index: 576): newluzhitu_XMLǃxiaoqianglu98ǃLOADER
		getChildById(id: 'n578_awmy'): newluzhitu_XMLǃxiaoqianglu98ǃLOADER
		getChild(name: 'xiaoqianglu99'): newluzhitu_XMLǃxiaoqianglu99ǃLOADER
		getChildAt(index: 577): newluzhitu_XMLǃxiaoqianglu99ǃLOADER
		getChildById(id: 'n579_awmy'): newluzhitu_XMLǃxiaoqianglu99ǃLOADER
		getChild(name: 'xiaoqianglu100'): newluzhitu_XMLǃxiaoqianglu100ǃLOADER
		getChildAt(index: 578): newluzhitu_XMLǃxiaoqianglu100ǃLOADER
		getChildById(id: 'n580_awmy'): newluzhitu_XMLǃxiaoqianglu100ǃLOADER
		getChild(name: 'xiaoqianglu101'): newluzhitu_XMLǃxiaoqianglu101ǃLOADER
		getChildAt(index: 579): newluzhitu_XMLǃxiaoqianglu101ǃLOADER
		getChildById(id: 'n581_awmy'): newluzhitu_XMLǃxiaoqianglu101ǃLOADER
		getChild(name: 'zhuang'): newluzhitu_XMLǃzhuangǃTEXT
		getChildAt(index: 580): newluzhitu_XMLǃzhuangǃTEXT
		getChildById(id: 'n584_awmy'): newluzhitu_XMLǃzhuangǃTEXT
		getChild(name: 'xian'): newluzhitu_XMLǃxianǃTEXT
		getChildAt(index: 581): newluzhitu_XMLǃxianǃTEXT
		getChildById(id: 'n585_awmy'): newluzhitu_XMLǃxianǃTEXT
		getChild(name: 'he'): newluzhitu_XMLǃheǃTEXT
		getChildAt(index: 582): newluzhitu_XMLǃheǃTEXT
		getChildById(id: 'n586_awmy'): newluzhitu_XMLǃheǃTEXT
		getChild(name: 'zhuangdui'): newluzhitu_XMLǃzhuangduiǃTEXT
		getChildAt(index: 583): newluzhitu_XMLǃzhuangduiǃTEXT
		getChildById(id: 'n587_awmy'): newluzhitu_XMLǃzhuangduiǃTEXT
		getChild(name: 'xiandui'): newluzhitu_XMLǃxianduiǃTEXT
		getChildAt(index: 584): newluzhitu_XMLǃxianduiǃTEXT
		getChildById(id: 'n588_awmy'): newluzhitu_XMLǃxianduiǃTEXT
		getChild(name: 'total'): newluzhitu_XMLǃtotalǃTEXT
		getChildAt(index: 585): newluzhitu_XMLǃtotalǃTEXT
		getChildById(id: 'n589_awmy'): newluzhitu_XMLǃtotalǃTEXT
		getChild(name: 'zwl0'): newluzhitu_XMLǃzwl0ǃLOADER
		getChildAt(index: 586): newluzhitu_XMLǃzwl0ǃLOADER
		getChildById(id: 'n590_hiph'): newluzhitu_XMLǃzwl0ǃLOADER
		getChild(name: 'zwl1'): newluzhitu_XMLǃzwl1ǃLOADER
		getChildAt(index: 587): newluzhitu_XMLǃzwl1ǃLOADER
		getChildById(id: 'n591_hiph'): newluzhitu_XMLǃzwl1ǃLOADER
		getChild(name: 'zwl2'): newluzhitu_XMLǃzwl2ǃLOADER
		getChildAt(index: 588): newluzhitu_XMLǃzwl2ǃLOADER
		getChildById(id: 'n592_hiph'): newluzhitu_XMLǃzwl2ǃLOADER
		getChild(name: 'xwl0'): newluzhitu_XMLǃxwl0ǃLOADER
		getChildAt(index: 589): newluzhitu_XMLǃxwl0ǃLOADER
		getChildById(id: 'n593_hiph'): newluzhitu_XMLǃxwl0ǃLOADER
		getChild(name: 'xwl1'): newluzhitu_XMLǃxwl1ǃLOADER
		getChildAt(index: 590): newluzhitu_XMLǃxwl1ǃLOADER
		getChildById(id: 'n594_hiph'): newluzhitu_XMLǃxwl1ǃLOADER
		getChild(name: 'xwl2'): newluzhitu_XMLǃxwl2ǃLOADER
		getChildAt(index: 591): newluzhitu_XMLǃxwl2ǃLOADER
		getChildById(id: 'n595_hiph'): newluzhitu_XMLǃxwl2ǃLOADER
		getChild(name: 'n598_CN2'): newluzhitu_XMLǃn598_CN2ǃTEXT
		getChildAt(index: 592): newluzhitu_XMLǃn598_CN2ǃTEXT
		getChildById(id: 'n598_j57m_CN2'): newluzhitu_XMLǃn598_CN2ǃTEXT
		getChild(name: 'n598_EN'): newluzhitu_XMLǃn598_ENǃTEXT
		getChildAt(index: 593): newluzhitu_XMLǃn598_ENǃTEXT
		getChildById(id: 'n598_j57m_EN'): newluzhitu_XMLǃn598_ENǃTEXT
		getChild(name: 'n598_IN'): newluzhitu_XMLǃn598_INǃTEXT
		getChildAt(index: 594): newluzhitu_XMLǃn598_INǃTEXT
		getChildById(id: 'n598_j57m_IN'): newluzhitu_XMLǃn598_INǃTEXT
		getChild(name: 'n598_JP'): newluzhitu_XMLǃn598_JPǃTEXT
		getChildAt(index: 595): newluzhitu_XMLǃn598_JPǃTEXT
		getChildById(id: 'n598_j57m_JP'): newluzhitu_XMLǃn598_JPǃTEXT
		getChild(name: 'n598_KR'): newluzhitu_XMLǃn598_KRǃTEXT
		getChildAt(index: 596): newluzhitu_XMLǃn598_KRǃTEXT
		getChildById(id: 'n598_j57m_KR'): newluzhitu_XMLǃn598_KRǃTEXT
		getChild(name: 'n598_TH'): newluzhitu_XMLǃn598_THǃTEXT
		getChildAt(index: 597): newluzhitu_XMLǃn598_THǃTEXT
		getChildById(id: 'n598_j57m_TH'): newluzhitu_XMLǃn598_THǃTEXT
		getChild(name: 'n598_VN'): newluzhitu_XMLǃn598_VNǃTEXT
		getChildAt(index: 598): newluzhitu_XMLǃn598_VNǃTEXT
		getChildById(id: 'n598_j57m_VN'): newluzhitu_XMLǃn598_VNǃTEXT
		getChild(name: 'n598'): newluzhitu_XMLǃn598ǃTEXT
		getChildAt(index: 599): newluzhitu_XMLǃn598ǃTEXT
		getChildById(id: 'n598_j57m'): newluzhitu_XMLǃn598ǃTEXT
		getChild(name: 'n599_CN2'): newluzhitu_XMLǃn599_CN2ǃTEXT
		getChildAt(index: 600): newluzhitu_XMLǃn599_CN2ǃTEXT
		getChildById(id: 'n599_j57m_CN2'): newluzhitu_XMLǃn599_CN2ǃTEXT
		getChild(name: 'n599_EN'): newluzhitu_XMLǃn599_ENǃTEXT
		getChildAt(index: 601): newluzhitu_XMLǃn599_ENǃTEXT
		getChildById(id: 'n599_j57m_EN'): newluzhitu_XMLǃn599_ENǃTEXT
		getChild(name: 'n599_IN'): newluzhitu_XMLǃn599_INǃTEXT
		getChildAt(index: 602): newluzhitu_XMLǃn599_INǃTEXT
		getChildById(id: 'n599_j57m_IN'): newluzhitu_XMLǃn599_INǃTEXT
		getChild(name: 'n599_JP'): newluzhitu_XMLǃn599_JPǃTEXT
		getChildAt(index: 603): newluzhitu_XMLǃn599_JPǃTEXT
		getChildById(id: 'n599_j57m_JP'): newluzhitu_XMLǃn599_JPǃTEXT
		getChild(name: 'n599_KR'): newluzhitu_XMLǃn599_KRǃTEXT
		getChildAt(index: 604): newluzhitu_XMLǃn599_KRǃTEXT
		getChildById(id: 'n599_j57m_KR'): newluzhitu_XMLǃn599_KRǃTEXT
		getChild(name: 'n599_TH'): newluzhitu_XMLǃn599_THǃTEXT
		getChildAt(index: 605): newluzhitu_XMLǃn599_THǃTEXT
		getChildById(id: 'n599_j57m_TH'): newluzhitu_XMLǃn599_THǃTEXT
		getChild(name: 'n599_VN'): newluzhitu_XMLǃn599_VNǃTEXT
		getChildAt(index: 606): newluzhitu_XMLǃn599_VNǃTEXT
		getChildById(id: 'n599_j57m_VN'): newluzhitu_XMLǃn599_VNǃTEXT
		getChild(name: 'n599'): newluzhitu_XMLǃn599ǃTEXT
		getChildAt(index: 607): newluzhitu_XMLǃn599ǃTEXT
		getChildById(id: 'n599_j57m'): newluzhitu_XMLǃn599ǃTEXT
		getChild(name: 'n600_CN2'): newluzhitu_XMLǃn600_CN2ǃTEXT
		getChildAt(index: 608): newluzhitu_XMLǃn600_CN2ǃTEXT
		getChildById(id: 'n600_j57m_CN2'): newluzhitu_XMLǃn600_CN2ǃTEXT
		getChild(name: 'n600_EN'): newluzhitu_XMLǃn600_ENǃTEXT
		getChildAt(index: 609): newluzhitu_XMLǃn600_ENǃTEXT
		getChildById(id: 'n600_j57m_EN'): newluzhitu_XMLǃn600_ENǃTEXT
		getChild(name: 'n600_IN'): newluzhitu_XMLǃn600_INǃTEXT
		getChildAt(index: 610): newluzhitu_XMLǃn600_INǃTEXT
		getChildById(id: 'n600_j57m_IN'): newluzhitu_XMLǃn600_INǃTEXT
		getChild(name: 'n600_JP'): newluzhitu_XMLǃn600_JPǃTEXT
		getChildAt(index: 611): newluzhitu_XMLǃn600_JPǃTEXT
		getChildById(id: 'n600_j57m_JP'): newluzhitu_XMLǃn600_JPǃTEXT
		getChild(name: 'n600_KR'): newluzhitu_XMLǃn600_KRǃTEXT
		getChildAt(index: 612): newluzhitu_XMLǃn600_KRǃTEXT
		getChildById(id: 'n600_j57m_KR'): newluzhitu_XMLǃn600_KRǃTEXT
		getChild(name: 'n600_TH'): newluzhitu_XMLǃn600_THǃTEXT
		getChildAt(index: 613): newluzhitu_XMLǃn600_THǃTEXT
		getChildById(id: 'n600_j57m_TH'): newluzhitu_XMLǃn600_THǃTEXT
		getChild(name: 'n600_VN'): newluzhitu_XMLǃn600_VNǃTEXT
		getChildAt(index: 614): newluzhitu_XMLǃn600_VNǃTEXT
		getChildById(id: 'n600_j57m_VN'): newluzhitu_XMLǃn600_VNǃTEXT
		getChild(name: 'n600'): newluzhitu_XMLǃn600ǃTEXT
		getChildAt(index: 615): newluzhitu_XMLǃn600ǃTEXT
		getChildById(id: 'n600_j57m'): newluzhitu_XMLǃn600ǃTEXT
		getChild(name: 'n601_CN2'): newluzhitu_XMLǃn601_CN2ǃTEXT
		getChildAt(index: 616): newluzhitu_XMLǃn601_CN2ǃTEXT
		getChildById(id: 'n601_j57m_CN2'): newluzhitu_XMLǃn601_CN2ǃTEXT
		getChild(name: 'n601_EN'): newluzhitu_XMLǃn601_ENǃTEXT
		getChildAt(index: 617): newluzhitu_XMLǃn601_ENǃTEXT
		getChildById(id: 'n601_j57m_EN'): newluzhitu_XMLǃn601_ENǃTEXT
		getChild(name: 'n601_IN'): newluzhitu_XMLǃn601_INǃTEXT
		getChildAt(index: 618): newluzhitu_XMLǃn601_INǃTEXT
		getChildById(id: 'n601_j57m_IN'): newluzhitu_XMLǃn601_INǃTEXT
		getChild(name: 'n601_JP'): newluzhitu_XMLǃn601_JPǃTEXT
		getChildAt(index: 619): newluzhitu_XMLǃn601_JPǃTEXT
		getChildById(id: 'n601_j57m_JP'): newluzhitu_XMLǃn601_JPǃTEXT
		getChild(name: 'n601_KR'): newluzhitu_XMLǃn601_KRǃTEXT
		getChildAt(index: 620): newluzhitu_XMLǃn601_KRǃTEXT
		getChildById(id: 'n601_j57m_KR'): newluzhitu_XMLǃn601_KRǃTEXT
		getChild(name: 'n601_TH'): newluzhitu_XMLǃn601_THǃTEXT
		getChildAt(index: 621): newluzhitu_XMLǃn601_THǃTEXT
		getChildById(id: 'n601_j57m_TH'): newluzhitu_XMLǃn601_THǃTEXT
		getChild(name: 'n601_VN'): newluzhitu_XMLǃn601_VNǃTEXT
		getChildAt(index: 622): newluzhitu_XMLǃn601_VNǃTEXT
		getChildById(id: 'n601_j57m_VN'): newluzhitu_XMLǃn601_VNǃTEXT
		getChild(name: 'n601'): newluzhitu_XMLǃn601ǃTEXT
		getChildAt(index: 623): newluzhitu_XMLǃn601ǃTEXT
		getChildById(id: 'n601_j57m'): newluzhitu_XMLǃn601ǃTEXT
		getChild(name: 'n602_CN2'): newluzhitu_XMLǃn602_CN2ǃTEXT
		getChildAt(index: 624): newluzhitu_XMLǃn602_CN2ǃTEXT
		getChildById(id: 'n602_j57m_CN2'): newluzhitu_XMLǃn602_CN2ǃTEXT
		getChild(name: 'n602_EN'): newluzhitu_XMLǃn602_ENǃTEXT
		getChildAt(index: 625): newluzhitu_XMLǃn602_ENǃTEXT
		getChildById(id: 'n602_j57m_EN'): newluzhitu_XMLǃn602_ENǃTEXT
		getChild(name: 'n602_IN'): newluzhitu_XMLǃn602_INǃTEXT
		getChildAt(index: 626): newluzhitu_XMLǃn602_INǃTEXT
		getChildById(id: 'n602_j57m_IN'): newluzhitu_XMLǃn602_INǃTEXT
		getChild(name: 'n602_JP'): newluzhitu_XMLǃn602_JPǃTEXT
		getChildAt(index: 627): newluzhitu_XMLǃn602_JPǃTEXT
		getChildById(id: 'n602_j57m_JP'): newluzhitu_XMLǃn602_JPǃTEXT
		getChild(name: 'n602_KR'): newluzhitu_XMLǃn602_KRǃTEXT
		getChildAt(index: 628): newluzhitu_XMLǃn602_KRǃTEXT
		getChildById(id: 'n602_j57m_KR'): newluzhitu_XMLǃn602_KRǃTEXT
		getChild(name: 'n602_TH'): newluzhitu_XMLǃn602_THǃTEXT
		getChildAt(index: 629): newluzhitu_XMLǃn602_THǃTEXT
		getChildById(id: 'n602_j57m_TH'): newluzhitu_XMLǃn602_THǃTEXT
		getChild(name: 'n602_VN'): newluzhitu_XMLǃn602_VNǃTEXT
		getChildAt(index: 630): newluzhitu_XMLǃn602_VNǃTEXT
		getChildById(id: 'n602_j57m_VN'): newluzhitu_XMLǃn602_VNǃTEXT
		getChild(name: 'n602'): newluzhitu_XMLǃn602ǃTEXT
		getChildAt(index: 631): newluzhitu_XMLǃn602ǃTEXT
		getChildById(id: 'n602_j57m'): newluzhitu_XMLǃn602ǃTEXT
		getChild(name: 'n603_CN2'): newluzhitu_XMLǃn603_CN2ǃTEXT
		getChildAt(index: 632): newluzhitu_XMLǃn603_CN2ǃTEXT
		getChildById(id: 'n603_j57m_CN2'): newluzhitu_XMLǃn603_CN2ǃTEXT
		getChild(name: 'n603_EN'): newluzhitu_XMLǃn603_ENǃTEXT
		getChildAt(index: 633): newluzhitu_XMLǃn603_ENǃTEXT
		getChildById(id: 'n603_j57m_EN'): newluzhitu_XMLǃn603_ENǃTEXT
		getChild(name: 'n603_IN'): newluzhitu_XMLǃn603_INǃTEXT
		getChildAt(index: 634): newluzhitu_XMLǃn603_INǃTEXT
		getChildById(id: 'n603_j57m_IN'): newluzhitu_XMLǃn603_INǃTEXT
		getChild(name: 'n603_JP'): newluzhitu_XMLǃn603_JPǃTEXT
		getChildAt(index: 635): newluzhitu_XMLǃn603_JPǃTEXT
		getChildById(id: 'n603_j57m_JP'): newluzhitu_XMLǃn603_JPǃTEXT
		getChild(name: 'n603_KR'): newluzhitu_XMLǃn603_KRǃTEXT
		getChildAt(index: 636): newluzhitu_XMLǃn603_KRǃTEXT
		getChildById(id: 'n603_j57m_KR'): newluzhitu_XMLǃn603_KRǃTEXT
		getChild(name: 'n603_TH'): newluzhitu_XMLǃn603_THǃTEXT
		getChildAt(index: 637): newluzhitu_XMLǃn603_THǃTEXT
		getChildById(id: 'n603_j57m_TH'): newluzhitu_XMLǃn603_THǃTEXT
		getChild(name: 'n603_VN'): newluzhitu_XMLǃn603_VNǃTEXT
		getChildAt(index: 638): newluzhitu_XMLǃn603_VNǃTEXT
		getChildById(id: 'n603_j57m_VN'): newluzhitu_XMLǃn603_VNǃTEXT
		getChild(name: 'n603'): newluzhitu_XMLǃn603ǃTEXT
		getChildAt(index: 639): newluzhitu_XMLǃn603ǃTEXT
		getChildById(id: 'n603_j57m'): newluzhitu_XMLǃn603ǃTEXT
		getChild(name: 'n605_CN2'): newluzhitu_XMLǃn605_CN2ǃTEXT
		getChildAt(index: 640): newluzhitu_XMLǃn605_CN2ǃTEXT
		getChildById(id: 'n605_rddw_CN2'): newluzhitu_XMLǃn605_CN2ǃTEXT
		getChild(name: 'n605_EN'): newluzhitu_XMLǃn605_ENǃTEXT
		getChildAt(index: 641): newluzhitu_XMLǃn605_ENǃTEXT
		getChildById(id: 'n605_rddw_EN'): newluzhitu_XMLǃn605_ENǃTEXT
		getChild(name: 'n605_IN'): newluzhitu_XMLǃn605_INǃTEXT
		getChildAt(index: 642): newluzhitu_XMLǃn605_INǃTEXT
		getChildById(id: 'n605_rddw_IN'): newluzhitu_XMLǃn605_INǃTEXT
		getChild(name: 'n605_JP'): newluzhitu_XMLǃn605_JPǃTEXT
		getChildAt(index: 643): newluzhitu_XMLǃn605_JPǃTEXT
		getChildById(id: 'n605_rddw_JP'): newluzhitu_XMLǃn605_JPǃTEXT
		getChild(name: 'n605_KR'): newluzhitu_XMLǃn605_KRǃTEXT
		getChildAt(index: 644): newluzhitu_XMLǃn605_KRǃTEXT
		getChildById(id: 'n605_rddw_KR'): newluzhitu_XMLǃn605_KRǃTEXT
		getChild(name: 'n605_TH'): newluzhitu_XMLǃn605_THǃTEXT
		getChildAt(index: 645): newluzhitu_XMLǃn605_THǃTEXT
		getChildById(id: 'n605_rddw_TH'): newluzhitu_XMLǃn605_THǃTEXT
		getChild(name: 'n605_VN'): newluzhitu_XMLǃn605_VNǃTEXT
		getChildAt(index: 646): newluzhitu_XMLǃn605_VNǃTEXT
		getChildById(id: 'n605_rddw_VN'): newluzhitu_XMLǃn605_VNǃTEXT
		getChild(name: 'n605'): newluzhitu_XMLǃn605ǃTEXT
		getChildAt(index: 647): newluzhitu_XMLǃn605ǃTEXT
		getChildById(id: 'n605_rddw'): newluzhitu_XMLǃn605ǃTEXT
		getChild(name: 'n606_CN2'): newluzhitu_XMLǃn606_CN2ǃTEXT
		getChildAt(index: 648): newluzhitu_XMLǃn606_CN2ǃTEXT
		getChildById(id: 'n606_rddw_CN2'): newluzhitu_XMLǃn606_CN2ǃTEXT
		getChild(name: 'n606_EN'): newluzhitu_XMLǃn606_ENǃTEXT
		getChildAt(index: 649): newluzhitu_XMLǃn606_ENǃTEXT
		getChildById(id: 'n606_rddw_EN'): newluzhitu_XMLǃn606_ENǃTEXT
		getChild(name: 'n606_IN'): newluzhitu_XMLǃn606_INǃTEXT
		getChildAt(index: 650): newluzhitu_XMLǃn606_INǃTEXT
		getChildById(id: 'n606_rddw_IN'): newluzhitu_XMLǃn606_INǃTEXT
		getChild(name: 'n606_JP'): newluzhitu_XMLǃn606_JPǃTEXT
		getChildAt(index: 651): newluzhitu_XMLǃn606_JPǃTEXT
		getChildById(id: 'n606_rddw_JP'): newluzhitu_XMLǃn606_JPǃTEXT
		getChild(name: 'n606_KR'): newluzhitu_XMLǃn606_KRǃTEXT
		getChildAt(index: 652): newluzhitu_XMLǃn606_KRǃTEXT
		getChildById(id: 'n606_rddw_KR'): newluzhitu_XMLǃn606_KRǃTEXT
		getChild(name: 'n606_TH'): newluzhitu_XMLǃn606_THǃTEXT
		getChildAt(index: 653): newluzhitu_XMLǃn606_THǃTEXT
		getChildById(id: 'n606_rddw_TH'): newluzhitu_XMLǃn606_THǃTEXT
		getChild(name: 'n606_VN'): newluzhitu_XMLǃn606_VNǃTEXT
		getChildAt(index: 654): newluzhitu_XMLǃn606_VNǃTEXT
		getChildById(id: 'n606_rddw_VN'): newluzhitu_XMLǃn606_VNǃTEXT
		getChild(name: 'n606'): newluzhitu_XMLǃn606ǃTEXT
		getChildAt(index: 655): newluzhitu_XMLǃn606ǃTEXT
		getChildById(id: 'n606_rddw'): newluzhitu_XMLǃn606ǃTEXT
		_children: [
			newluzhitu_XMLǃzhupan0ǃLOADER,
			newluzhitu_XMLǃzhupan1ǃLOADER,
			newluzhitu_XMLǃzhupan2ǃLOADER,
			newluzhitu_XMLǃzhupan3ǃLOADER,
			newluzhitu_XMLǃzhupan4ǃLOADER,
			newluzhitu_XMLǃzhupan5ǃLOADER,
			newluzhitu_XMLǃzhupan6ǃLOADER,
			newluzhitu_XMLǃzhupan7ǃLOADER,
			newluzhitu_XMLǃzhupan8ǃLOADER,
			newluzhitu_XMLǃzhupan9ǃLOADER,
			newluzhitu_XMLǃzhupan10ǃLOADER,
			newluzhitu_XMLǃzhupan11ǃLOADER,
			newluzhitu_XMLǃzhupan12ǃLOADER,
			newluzhitu_XMLǃzhupan13ǃLOADER,
			newluzhitu_XMLǃzhupan14ǃLOADER,
			newluzhitu_XMLǃzhupan15ǃLOADER,
			newluzhitu_XMLǃzhupan16ǃLOADER,
			newluzhitu_XMLǃzhupan17ǃLOADER,
			newluzhitu_XMLǃzhupan18ǃLOADER,
			newluzhitu_XMLǃzhupan19ǃLOADER,
			newluzhitu_XMLǃzhupan20ǃLOADER,
			newluzhitu_XMLǃzhupan21ǃLOADER,
			newluzhitu_XMLǃzhupan22ǃLOADER,
			newluzhitu_XMLǃzhupan23ǃLOADER,
			newluzhitu_XMLǃzhupan24ǃLOADER,
			newluzhitu_XMLǃzhupan25ǃLOADER,
			newluzhitu_XMLǃzhupan26ǃLOADER,
			newluzhitu_XMLǃzhupan27ǃLOADER,
			newluzhitu_XMLǃzhupan28ǃLOADER,
			newluzhitu_XMLǃzhupan29ǃLOADER,
			newluzhitu_XMLǃzhupan30ǃLOADER,
			newluzhitu_XMLǃzhupan31ǃLOADER,
			newluzhitu_XMLǃzhupan32ǃLOADER,
			newluzhitu_XMLǃzhupan33ǃLOADER,
			newluzhitu_XMLǃzhupan34ǃLOADER,
			newluzhitu_XMLǃzhupan35ǃLOADER,
			newluzhitu_XMLǃzhupan36ǃLOADER,
			newluzhitu_XMLǃzhupan37ǃLOADER,
			newluzhitu_XMLǃzhupan38ǃLOADER,
			newluzhitu_XMLǃzhupan39ǃLOADER,
			newluzhitu_XMLǃzhupan40ǃLOADER,
			newluzhitu_XMLǃzhupan41ǃLOADER,
			newluzhitu_XMLǃzhupan42ǃLOADER,
			newluzhitu_XMLǃzhupan43ǃLOADER,
			newluzhitu_XMLǃzhupan44ǃLOADER,
			newluzhitu_XMLǃzhupan45ǃLOADER,
			newluzhitu_XMLǃzhupan46ǃLOADER,
			newluzhitu_XMLǃzhupan47ǃLOADER,
			newluzhitu_XMLǃzhupan48ǃLOADER,
			newluzhitu_XMLǃzhupan49ǃLOADER,
			newluzhitu_XMLǃzhupan50ǃLOADER,
			newluzhitu_XMLǃzhupan51ǃLOADER,
			newluzhitu_XMLǃzhupan52ǃLOADER,
			newluzhitu_XMLǃzhupan53ǃLOADER,
			newluzhitu_XMLǃzhupan54ǃLOADER,
			newluzhitu_XMLǃzhupan55ǃLOADER,
			newluzhitu_XMLǃzhupan56ǃLOADER,
			newluzhitu_XMLǃzhupan57ǃLOADER,
			newluzhitu_XMLǃzhupan58ǃLOADER,
			newluzhitu_XMLǃzhupan59ǃLOADER,
			componentǁDaXiaoinfo_XMLǃnum_0ǃCOMPONENT,
			componentǁDaXiaoinfo_XMLǃnum_1ǃCOMPONENT,
			componentǁDaXiaoinfo_XMLǃnum_2ǃCOMPONENT,
			componentǁDaXiaoinfo_XMLǃnum_3ǃCOMPONENT,
			componentǁDaXiaoinfo_XMLǃnum_4ǃCOMPONENT,
			componentǁDaXiaoinfo_XMLǃnum_5ǃCOMPONENT,
			componentǁDaXiaoinfo_XMLǃnum_6ǃCOMPONENT,
			componentǁDaXiaoinfo_XMLǃnum_7ǃCOMPONENT,
			componentǁDaXiaoinfo_XMLǃnum_8ǃCOMPONENT,
			componentǁDaXiaoinfo_XMLǃnum_9ǃCOMPONENT,
			daluComponent_XMLǃdalu0ǃCOMPONENT,
			daluComponent_XMLǃdalu1ǃCOMPONENT,
			daluComponent_XMLǃdalu2ǃCOMPONENT,
			daluComponent_XMLǃdalu3ǃCOMPONENT,
			daluComponent_XMLǃdalu4ǃCOMPONENT,
			daluComponent_XMLǃdalu5ǃCOMPONENT,
			daluComponent_XMLǃdalu6ǃCOMPONENT,
			daluComponent_XMLǃdalu7ǃCOMPONENT,
			daluComponent_XMLǃdalu8ǃCOMPONENT,
			daluComponent_XMLǃdalu9ǃCOMPONENT,
			daluComponent_XMLǃdalu10ǃCOMPONENT,
			daluComponent_XMLǃdalu11ǃCOMPONENT,
			daluComponent_XMLǃdalu12ǃCOMPONENT,
			daluComponent_XMLǃdalu13ǃCOMPONENT,
			daluComponent_XMLǃdalu14ǃCOMPONENT,
			daluComponent_XMLǃdalu15ǃCOMPONENT,
			daluComponent_XMLǃdalu16ǃCOMPONENT,
			daluComponent_XMLǃdalu17ǃCOMPONENT,
			daluComponent_XMLǃdalu18ǃCOMPONENT,
			daluComponent_XMLǃdalu19ǃCOMPONENT,
			daluComponent_XMLǃdalu20ǃCOMPONENT,
			daluComponent_XMLǃdalu21ǃCOMPONENT,
			daluComponent_XMLǃdalu22ǃCOMPONENT,
			daluComponent_XMLǃdalu23ǃCOMPONENT,
			daluComponent_XMLǃdalu24ǃCOMPONENT,
			daluComponent_XMLǃdalu25ǃCOMPONENT,
			daluComponent_XMLǃdalu26ǃCOMPONENT,
			daluComponent_XMLǃdalu27ǃCOMPONENT,
			daluComponent_XMLǃdalu28ǃCOMPONENT,
			daluComponent_XMLǃdalu29ǃCOMPONENT,
			daluComponent_XMLǃdalu30ǃCOMPONENT,
			daluComponent_XMLǃdalu31ǃCOMPONENT,
			daluComponent_XMLǃdalu32ǃCOMPONENT,
			daluComponent_XMLǃdalu33ǃCOMPONENT,
			daluComponent_XMLǃdalu34ǃCOMPONENT,
			daluComponent_XMLǃdalu35ǃCOMPONENT,
			daluComponent_XMLǃdalu36ǃCOMPONENT,
			daluComponent_XMLǃdalu37ǃCOMPONENT,
			daluComponent_XMLǃdalu38ǃCOMPONENT,
			daluComponent_XMLǃdalu39ǃCOMPONENT,
			daluComponent_XMLǃdalu40ǃCOMPONENT,
			daluComponent_XMLǃdalu41ǃCOMPONENT,
			daluComponent_XMLǃdalu42ǃCOMPONENT,
			daluComponent_XMLǃdalu43ǃCOMPONENT,
			daluComponent_XMLǃdalu44ǃCOMPONENT,
			daluComponent_XMLǃdalu45ǃCOMPONENT,
			daluComponent_XMLǃdalu46ǃCOMPONENT,
			daluComponent_XMLǃdalu47ǃCOMPONENT,
			daluComponent_XMLǃdalu48ǃCOMPONENT,
			daluComponent_XMLǃdalu49ǃCOMPONENT,
			daluComponent_XMLǃdalu50ǃCOMPONENT,
			daluComponent_XMLǃdalu51ǃCOMPONENT,
			daluComponent_XMLǃdalu52ǃCOMPONENT,
			daluComponent_XMLǃdalu53ǃCOMPONENT,
			daluComponent_XMLǃdalu54ǃCOMPONENT,
			daluComponent_XMLǃdalu55ǃCOMPONENT,
			daluComponent_XMLǃdalu56ǃCOMPONENT,
			daluComponent_XMLǃdalu57ǃCOMPONENT,
			daluComponent_XMLǃdalu58ǃCOMPONENT,
			daluComponent_XMLǃdalu59ǃCOMPONENT,
			daluComponent_XMLǃdalu60ǃCOMPONENT,
			daluComponent_XMLǃdalu61ǃCOMPONENT,
			daluComponent_XMLǃdalu62ǃCOMPONENT,
			daluComponent_XMLǃdalu63ǃCOMPONENT,
			daluComponent_XMLǃdalu64ǃCOMPONENT,
			daluComponent_XMLǃdalu65ǃCOMPONENT,
			daluComponent_XMLǃdalu66ǃCOMPONENT,
			daluComponent_XMLǃdalu67ǃCOMPONENT,
			daluComponent_XMLǃdalu68ǃCOMPONENT,
			daluComponent_XMLǃdalu69ǃCOMPONENT,
			daluComponent_XMLǃdalu70ǃCOMPONENT,
			daluComponent_XMLǃdalu71ǃCOMPONENT,
			daluComponent_XMLǃdalu72ǃCOMPONENT,
			daluComponent_XMLǃdalu73ǃCOMPONENT,
			daluComponent_XMLǃdalu74ǃCOMPONENT,
			daluComponent_XMLǃdalu75ǃCOMPONENT,
			daluComponent_XMLǃdalu76ǃCOMPONENT,
			daluComponent_XMLǃdalu77ǃCOMPONENT,
			daluComponent_XMLǃdalu78ǃCOMPONENT,
			daluComponent_XMLǃdalu79ǃCOMPONENT,
			daluComponent_XMLǃdalu80ǃCOMPONENT,
			daluComponent_XMLǃdalu81ǃCOMPONENT,
			daluComponent_XMLǃdalu82ǃCOMPONENT,
			daluComponent_XMLǃdalu83ǃCOMPONENT,
			daluComponent_XMLǃdalu84ǃCOMPONENT,
			daluComponent_XMLǃdalu85ǃCOMPONENT,
			daluComponent_XMLǃdalu86ǃCOMPONENT,
			daluComponent_XMLǃdalu87ǃCOMPONENT,
			daluComponent_XMLǃdalu88ǃCOMPONENT,
			daluComponent_XMLǃdalu89ǃCOMPONENT,
			daluComponent_XMLǃdalu90ǃCOMPONENT,
			daluComponent_XMLǃdalu91ǃCOMPONENT,
			daluComponent_XMLǃdalu92ǃCOMPONENT,
			daluComponent_XMLǃdalu93ǃCOMPONENT,
			daluComponent_XMLǃdalu94ǃCOMPONENT,
			daluComponent_XMLǃdalu95ǃCOMPONENT,
			daluComponent_XMLǃdalu96ǃCOMPONENT,
			daluComponent_XMLǃdalu97ǃCOMPONENT,
			daluComponent_XMLǃdalu98ǃCOMPONENT,
			daluComponent_XMLǃdalu99ǃCOMPONENT,
			daluComponent_XMLǃdalu100ǃCOMPONENT,
			daluComponent_XMLǃdalu101ǃCOMPONENT,
			daluComponent_XMLǃdalu102ǃCOMPONENT,
			daluComponent_XMLǃdalu103ǃCOMPONENT,
			daluComponent_XMLǃdalu104ǃCOMPONENT,
			daluComponent_XMLǃdalu105ǃCOMPONENT,
			daluComponent_XMLǃdalu106ǃCOMPONENT,
			daluComponent_XMLǃdalu107ǃCOMPONENT,
			daluComponent_XMLǃdalu108ǃCOMPONENT,
			daluComponent_XMLǃdalu109ǃCOMPONENT,
			daluComponent_XMLǃdalu110ǃCOMPONENT,
			daluComponent_XMLǃdalu111ǃCOMPONENT,
			daluComponent_XMLǃdalu112ǃCOMPONENT,
			daluComponent_XMLǃdalu113ǃCOMPONENT,
			newluzhitu_XMLǃdayanlu0ǃLOADER,
			newluzhitu_XMLǃdayanlu1ǃLOADER,
			newluzhitu_XMLǃdayanlu2ǃLOADER,
			newluzhitu_XMLǃdayanlu3ǃLOADER,
			newluzhitu_XMLǃdayanlu4ǃLOADER,
			newluzhitu_XMLǃdayanlu5ǃLOADER,
			newluzhitu_XMLǃdayanlu6ǃLOADER,
			newluzhitu_XMLǃdayanlu7ǃLOADER,
			newluzhitu_XMLǃdayanlu8ǃLOADER,
			newluzhitu_XMLǃdayanlu9ǃLOADER,
			newluzhitu_XMLǃdayanlu10ǃLOADER,
			newluzhitu_XMLǃdayanlu11ǃLOADER,
			newluzhitu_XMLǃdayanlu12ǃLOADER,
			newluzhitu_XMLǃdayanlu13ǃLOADER,
			newluzhitu_XMLǃdayanlu14ǃLOADER,
			newluzhitu_XMLǃdayanlu15ǃLOADER,
			newluzhitu_XMLǃdayanlu16ǃLOADER,
			newluzhitu_XMLǃdayanlu17ǃLOADER,
			newluzhitu_XMLǃdayanlu18ǃLOADER,
			newluzhitu_XMLǃdayanlu19ǃLOADER,
			newluzhitu_XMLǃdayanlu20ǃLOADER,
			newluzhitu_XMLǃdayanlu21ǃLOADER,
			newluzhitu_XMLǃdayanlu22ǃLOADER,
			newluzhitu_XMLǃdayanlu23ǃLOADER,
			newluzhitu_XMLǃdayanlu24ǃLOADER,
			newluzhitu_XMLǃdayanlu25ǃLOADER,
			newluzhitu_XMLǃdayanlu26ǃLOADER,
			newluzhitu_XMLǃdayanlu27ǃLOADER,
			newluzhitu_XMLǃdayanlu28ǃLOADER,
			newluzhitu_XMLǃdayanlu29ǃLOADER,
			newluzhitu_XMLǃdayanlu30ǃLOADER,
			newluzhitu_XMLǃdayanlu31ǃLOADER,
			newluzhitu_XMLǃdayanlu32ǃLOADER,
			newluzhitu_XMLǃdayanlu33ǃLOADER,
			newluzhitu_XMLǃdayanlu34ǃLOADER,
			newluzhitu_XMLǃdayanlu35ǃLOADER,
			newluzhitu_XMLǃdayanlu36ǃLOADER,
			newluzhitu_XMLǃdayanlu37ǃLOADER,
			newluzhitu_XMLǃdayanlu38ǃLOADER,
			newluzhitu_XMLǃdayanlu39ǃLOADER,
			newluzhitu_XMLǃdayanlu40ǃLOADER,
			newluzhitu_XMLǃdayanlu41ǃLOADER,
			newluzhitu_XMLǃdayanlu42ǃLOADER,
			newluzhitu_XMLǃdayanlu43ǃLOADER,
			newluzhitu_XMLǃdayanlu44ǃLOADER,
			newluzhitu_XMLǃdayanlu45ǃLOADER,
			newluzhitu_XMLǃdayanlu46ǃLOADER,
			newluzhitu_XMLǃdayanlu47ǃLOADER,
			newluzhitu_XMLǃdayanlu48ǃLOADER,
			newluzhitu_XMLǃdayanlu49ǃLOADER,
			newluzhitu_XMLǃdayanlu50ǃLOADER,
			newluzhitu_XMLǃdayanlu51ǃLOADER,
			newluzhitu_XMLǃdayanlu52ǃLOADER,
			newluzhitu_XMLǃdayanlu53ǃLOADER,
			newluzhitu_XMLǃdayanlu54ǃLOADER,
			newluzhitu_XMLǃdayanlu55ǃLOADER,
			newluzhitu_XMLǃdayanlu56ǃLOADER,
			newluzhitu_XMLǃdayanlu57ǃLOADER,
			newluzhitu_XMLǃdayanlu58ǃLOADER,
			newluzhitu_XMLǃdayanlu59ǃLOADER,
			newluzhitu_XMLǃdayanlu60ǃLOADER,
			newluzhitu_XMLǃdayanlu61ǃLOADER,
			newluzhitu_XMLǃdayanlu62ǃLOADER,
			newluzhitu_XMLǃdayanlu63ǃLOADER,
			newluzhitu_XMLǃdayanlu64ǃLOADER,
			newluzhitu_XMLǃdayanlu65ǃLOADER,
			newluzhitu_XMLǃdayanlu66ǃLOADER,
			newluzhitu_XMLǃdayanlu67ǃLOADER,
			newluzhitu_XMLǃdayanlu68ǃLOADER,
			newluzhitu_XMLǃdayanlu69ǃLOADER,
			newluzhitu_XMLǃdayanlu70ǃLOADER,
			newluzhitu_XMLǃdayanlu71ǃLOADER,
			newluzhitu_XMLǃdayanlu72ǃLOADER,
			newluzhitu_XMLǃdayanlu73ǃLOADER,
			newluzhitu_XMLǃdayanlu74ǃLOADER,
			newluzhitu_XMLǃdayanlu75ǃLOADER,
			newluzhitu_XMLǃdayanlu76ǃLOADER,
			newluzhitu_XMLǃdayanlu77ǃLOADER,
			newluzhitu_XMLǃdayanlu78ǃLOADER,
			newluzhitu_XMLǃdayanlu79ǃLOADER,
			newluzhitu_XMLǃdayanlu80ǃLOADER,
			newluzhitu_XMLǃdayanlu81ǃLOADER,
			newluzhitu_XMLǃdayanlu82ǃLOADER,
			newluzhitu_XMLǃdayanlu83ǃLOADER,
			newluzhitu_XMLǃdayanlu84ǃLOADER,
			newluzhitu_XMLǃdayanlu85ǃLOADER,
			newluzhitu_XMLǃdayanlu86ǃLOADER,
			newluzhitu_XMLǃdayanlu87ǃLOADER,
			newluzhitu_XMLǃdayanlu88ǃLOADER,
			newluzhitu_XMLǃdayanlu89ǃLOADER,
			newluzhitu_XMLǃdayanlu90ǃLOADER,
			newluzhitu_XMLǃdayanlu91ǃLOADER,
			newluzhitu_XMLǃdayanlu92ǃLOADER,
			newluzhitu_XMLǃdayanlu93ǃLOADER,
			newluzhitu_XMLǃdayanlu94ǃLOADER,
			newluzhitu_XMLǃdayanlu95ǃLOADER,
			newluzhitu_XMLǃdayanlu96ǃLOADER,
			newluzhitu_XMLǃdayanlu97ǃLOADER,
			newluzhitu_XMLǃdayanlu98ǃLOADER,
			newluzhitu_XMLǃdayanlu99ǃLOADER,
			newluzhitu_XMLǃdayanlu100ǃLOADER,
			newluzhitu_XMLǃdayanlu101ǃLOADER,
			newluzhitu_XMLǃdayanlu102ǃLOADER,
			newluzhitu_XMLǃdayanlu103ǃLOADER,
			newluzhitu_XMLǃdayanlu104ǃLOADER,
			newluzhitu_XMLǃdayanlu105ǃLOADER,
			newluzhitu_XMLǃdayanlu106ǃLOADER,
			newluzhitu_XMLǃdayanlu107ǃLOADER,
			newluzhitu_XMLǃdayanlu108ǃLOADER,
			newluzhitu_XMLǃdayanlu109ǃLOADER,
			newluzhitu_XMLǃdayanlu110ǃLOADER,
			newluzhitu_XMLǃdayanlu111ǃLOADER,
			newluzhitu_XMLǃdayanlu112ǃLOADER,
			newluzhitu_XMLǃdayanlu113ǃLOADER,
			newluzhitu_XMLǃdayanlu114ǃLOADER,
			newluzhitu_XMLǃdayanlu115ǃLOADER,
			newluzhitu_XMLǃdayanlu116ǃLOADER,
			newluzhitu_XMLǃdayanlu117ǃLOADER,
			newluzhitu_XMLǃdayanlu118ǃLOADER,
			newluzhitu_XMLǃdayanlu119ǃLOADER,
			newluzhitu_XMLǃdayanlu120ǃLOADER,
			newluzhitu_XMLǃdayanlu121ǃLOADER,
			newluzhitu_XMLǃdayanlu122ǃLOADER,
			newluzhitu_XMLǃdayanlu123ǃLOADER,
			newluzhitu_XMLǃdayanlu124ǃLOADER,
			newluzhitu_XMLǃdayanlu125ǃLOADER,
			newluzhitu_XMLǃdayanlu126ǃLOADER,
			newluzhitu_XMLǃdayanlu127ǃLOADER,
			newluzhitu_XMLǃdayanlu128ǃLOADER,
			newluzhitu_XMLǃdayanlu129ǃLOADER,
			newluzhitu_XMLǃdayanlu130ǃLOADER,
			newluzhitu_XMLǃdayanlu131ǃLOADER,
			newluzhitu_XMLǃdayanlu132ǃLOADER,
			newluzhitu_XMLǃdayanlu133ǃLOADER,
			newluzhitu_XMLǃdayanlu134ǃLOADER,
			newluzhitu_XMLǃdayanlu135ǃLOADER,
			newluzhitu_XMLǃdayanlu136ǃLOADER,
			newluzhitu_XMLǃdayanlu137ǃLOADER,
			newluzhitu_XMLǃdayanlu138ǃLOADER,
			newluzhitu_XMLǃdayanlu139ǃLOADER,
			newluzhitu_XMLǃdayanlu140ǃLOADER,
			newluzhitu_XMLǃdayanlu141ǃLOADER,
			newluzhitu_XMLǃdayanlu142ǃLOADER,
			newluzhitu_XMLǃdayanlu143ǃLOADER,
			newluzhitu_XMLǃdayanlu144ǃLOADER,
			newluzhitu_XMLǃdayanlu145ǃLOADER,
			newluzhitu_XMLǃdayanlu146ǃLOADER,
			newluzhitu_XMLǃdayanlu147ǃLOADER,
			newluzhitu_XMLǃdayanlu148ǃLOADER,
			newluzhitu_XMLǃdayanlu149ǃLOADER,
			newluzhitu_XMLǃdayanlu150ǃLOADER,
			newluzhitu_XMLǃdayanlu151ǃLOADER,
			newluzhitu_XMLǃdayanlu152ǃLOADER,
			newluzhitu_XMLǃdayanlu153ǃLOADER,
			newluzhitu_XMLǃdayanlu154ǃLOADER,
			newluzhitu_XMLǃdayanlu155ǃLOADER,
			newluzhitu_XMLǃdayanlu156ǃLOADER,
			newluzhitu_XMLǃdayanlu157ǃLOADER,
			newluzhitu_XMLǃdayanlu158ǃLOADER,
			newluzhitu_XMLǃdayanlu159ǃLOADER,
			newluzhitu_XMLǃdayanlu160ǃLOADER,
			newluzhitu_XMLǃdayanlu161ǃLOADER,
			newluzhitu_XMLǃdayanlu162ǃLOADER,
			newluzhitu_XMLǃdayanlu163ǃLOADER,
			newluzhitu_XMLǃdayanlu164ǃLOADER,
			newluzhitu_XMLǃdayanlu165ǃLOADER,
			newluzhitu_XMLǃdayanlu166ǃLOADER,
			newluzhitu_XMLǃdayanlu167ǃLOADER,
			newluzhitu_XMLǃdayanlu168ǃLOADER,
			newluzhitu_XMLǃdayanlu169ǃLOADER,
			newluzhitu_XMLǃdayanlu170ǃLOADER,
			newluzhitu_XMLǃdayanlu171ǃLOADER,
			newluzhitu_XMLǃdayanlu172ǃLOADER,
			newluzhitu_XMLǃdayanlu173ǃLOADER,
			newluzhitu_XMLǃdayanlu174ǃLOADER,
			newluzhitu_XMLǃdayanlu175ǃLOADER,
			newluzhitu_XMLǃdayanlu176ǃLOADER,
			newluzhitu_XMLǃdayanlu177ǃLOADER,
			newluzhitu_XMLǃdayanlu178ǃLOADER,
			newluzhitu_XMLǃdayanlu179ǃLOADER,
			newluzhitu_XMLǃdayanlu180ǃLOADER,
			newluzhitu_XMLǃdayanlu181ǃLOADER,
			newluzhitu_XMLǃdayanlu182ǃLOADER,
			newluzhitu_XMLǃdayanlu183ǃLOADER,
			newluzhitu_XMLǃdayanlu184ǃLOADER,
			newluzhitu_XMLǃdayanlu185ǃLOADER,
			newluzhitu_XMLǃdayanlu186ǃLOADER,
			newluzhitu_XMLǃdayanlu187ǃLOADER,
			newluzhitu_XMLǃdayanlu188ǃLOADER,
			newluzhitu_XMLǃdayanlu189ǃLOADER,
			newluzhitu_XMLǃdayanlu190ǃLOADER,
			newluzhitu_XMLǃdayanlu191ǃLOADER,
			newluzhitu_XMLǃdayanlu192ǃLOADER,
			newluzhitu_XMLǃdayanlu193ǃLOADER,
			newluzhitu_XMLǃdayanlu194ǃLOADER,
			newluzhitu_XMLǃdayanlu195ǃLOADER,
			newluzhitu_XMLǃdayanlu196ǃLOADER,
			newluzhitu_XMLǃdayanlu197ǃLOADER,
			newluzhitu_XMLǃxiaolu0ǃLOADER,
			newluzhitu_XMLǃxiaolu1ǃLOADER,
			newluzhitu_XMLǃxiaolu2ǃLOADER,
			newluzhitu_XMLǃxiaolu3ǃLOADER,
			newluzhitu_XMLǃxiaolu4ǃLOADER,
			newluzhitu_XMLǃxiaolu5ǃLOADER,
			newluzhitu_XMLǃxiaolu6ǃLOADER,
			newluzhitu_XMLǃxiaolu7ǃLOADER,
			newluzhitu_XMLǃxiaolu8ǃLOADER,
			newluzhitu_XMLǃxiaolu9ǃLOADER,
			newluzhitu_XMLǃxiaolu10ǃLOADER,
			newluzhitu_XMLǃxiaolu11ǃLOADER,
			newluzhitu_XMLǃxiaolu12ǃLOADER,
			newluzhitu_XMLǃxiaolu13ǃLOADER,
			newluzhitu_XMLǃxiaolu14ǃLOADER,
			newluzhitu_XMLǃxiaolu15ǃLOADER,
			newluzhitu_XMLǃxiaolu16ǃLOADER,
			newluzhitu_XMLǃxiaolu17ǃLOADER,
			newluzhitu_XMLǃxiaolu18ǃLOADER,
			newluzhitu_XMLǃxiaolu19ǃLOADER,
			newluzhitu_XMLǃxiaolu20ǃLOADER,
			newluzhitu_XMLǃxiaolu21ǃLOADER,
			newluzhitu_XMLǃxiaolu22ǃLOADER,
			newluzhitu_XMLǃxiaolu23ǃLOADER,
			newluzhitu_XMLǃxiaolu24ǃLOADER,
			newluzhitu_XMLǃxiaolu25ǃLOADER,
			newluzhitu_XMLǃxiaolu26ǃLOADER,
			newluzhitu_XMLǃxiaolu27ǃLOADER,
			newluzhitu_XMLǃxiaolu28ǃLOADER,
			newluzhitu_XMLǃxiaolu29ǃLOADER,
			newluzhitu_XMLǃxiaolu30ǃLOADER,
			newluzhitu_XMLǃxiaolu31ǃLOADER,
			newluzhitu_XMLǃxiaolu32ǃLOADER,
			newluzhitu_XMLǃxiaolu33ǃLOADER,
			newluzhitu_XMLǃxiaolu34ǃLOADER,
			newluzhitu_XMLǃxiaolu35ǃLOADER,
			newluzhitu_XMLǃxiaolu36ǃLOADER,
			newluzhitu_XMLǃxiaolu37ǃLOADER,
			newluzhitu_XMLǃxiaolu38ǃLOADER,
			newluzhitu_XMLǃxiaolu39ǃLOADER,
			newluzhitu_XMLǃxiaolu40ǃLOADER,
			newluzhitu_XMLǃxiaolu41ǃLOADER,
			newluzhitu_XMLǃxiaolu42ǃLOADER,
			newluzhitu_XMLǃxiaolu43ǃLOADER,
			newluzhitu_XMLǃxiaolu44ǃLOADER,
			newluzhitu_XMLǃxiaolu45ǃLOADER,
			newluzhitu_XMLǃxiaolu46ǃLOADER,
			newluzhitu_XMLǃxiaolu47ǃLOADER,
			newluzhitu_XMLǃxiaolu48ǃLOADER,
			newluzhitu_XMLǃxiaolu49ǃLOADER,
			newluzhitu_XMLǃxiaolu50ǃLOADER,
			newluzhitu_XMLǃxiaolu51ǃLOADER,
			newluzhitu_XMLǃxiaolu52ǃLOADER,
			newluzhitu_XMLǃxiaolu53ǃLOADER,
			newluzhitu_XMLǃxiaolu54ǃLOADER,
			newluzhitu_XMLǃxiaolu55ǃLOADER,
			newluzhitu_XMLǃxiaolu56ǃLOADER,
			newluzhitu_XMLǃxiaolu57ǃLOADER,
			newluzhitu_XMLǃxiaolu58ǃLOADER,
			newluzhitu_XMLǃxiaolu59ǃLOADER,
			newluzhitu_XMLǃxiaolu60ǃLOADER,
			newluzhitu_XMLǃxiaolu61ǃLOADER,
			newluzhitu_XMLǃxiaolu62ǃLOADER,
			newluzhitu_XMLǃxiaolu63ǃLOADER,
			newluzhitu_XMLǃxiaolu64ǃLOADER,
			newluzhitu_XMLǃxiaolu65ǃLOADER,
			newluzhitu_XMLǃxiaolu66ǃLOADER,
			newluzhitu_XMLǃxiaolu67ǃLOADER,
			newluzhitu_XMLǃxiaolu68ǃLOADER,
			newluzhitu_XMLǃxiaolu69ǃLOADER,
			newluzhitu_XMLǃxiaolu70ǃLOADER,
			newluzhitu_XMLǃxiaolu71ǃLOADER,
			newluzhitu_XMLǃxiaolu72ǃLOADER,
			newluzhitu_XMLǃxiaolu73ǃLOADER,
			newluzhitu_XMLǃxiaolu74ǃLOADER,
			newluzhitu_XMLǃxiaolu75ǃLOADER,
			newluzhitu_XMLǃxiaolu76ǃLOADER,
			newluzhitu_XMLǃxiaolu77ǃLOADER,
			newluzhitu_XMLǃxiaolu78ǃLOADER,
			newluzhitu_XMLǃxiaolu79ǃLOADER,
			newluzhitu_XMLǃxiaolu80ǃLOADER,
			newluzhitu_XMLǃxiaolu81ǃLOADER,
			newluzhitu_XMLǃxiaolu82ǃLOADER,
			newluzhitu_XMLǃxiaolu83ǃLOADER,
			newluzhitu_XMLǃxiaolu84ǃLOADER,
			newluzhitu_XMLǃxiaolu85ǃLOADER,
			newluzhitu_XMLǃxiaolu86ǃLOADER,
			newluzhitu_XMLǃxiaolu87ǃLOADER,
			newluzhitu_XMLǃxiaolu88ǃLOADER,
			newluzhitu_XMLǃxiaolu89ǃLOADER,
			newluzhitu_XMLǃxiaolu90ǃLOADER,
			newluzhitu_XMLǃxiaolu91ǃLOADER,
			newluzhitu_XMLǃxiaolu92ǃLOADER,
			newluzhitu_XMLǃxiaolu93ǃLOADER,
			newluzhitu_XMLǃxiaolu94ǃLOADER,
			newluzhitu_XMLǃxiaolu95ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu0ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu1ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu2ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu3ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu4ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu5ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu6ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu7ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu8ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu9ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu10ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu11ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu12ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu13ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu14ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu15ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu16ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu17ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu18ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu19ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu20ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu21ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu22ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu23ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu24ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu25ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu26ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu27ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu28ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu29ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu30ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu31ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu32ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu33ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu34ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu35ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu36ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu37ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu38ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu39ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu40ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu41ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu42ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu43ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu44ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu45ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu46ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu47ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu48ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu49ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu50ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu51ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu52ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu53ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu54ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu55ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu56ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu57ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu58ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu59ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu60ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu61ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu62ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu63ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu64ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu65ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu66ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu67ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu68ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu69ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu70ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu71ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu72ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu73ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu74ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu75ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu76ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu77ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu78ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu79ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu80ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu81ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu82ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu83ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu84ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu85ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu86ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu87ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu88ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu89ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu90ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu91ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu92ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu93ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu94ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu95ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu96ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu97ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu98ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu99ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu100ǃLOADER,
			newluzhitu_XMLǃxiaoqianglu101ǃLOADER,
			newluzhitu_XMLǃzhuangǃTEXT,
			newluzhitu_XMLǃxianǃTEXT,
			newluzhitu_XMLǃheǃTEXT,
			newluzhitu_XMLǃzhuangduiǃTEXT,
			newluzhitu_XMLǃxianduiǃTEXT,
			newluzhitu_XMLǃtotalǃTEXT,
			newluzhitu_XMLǃzwl0ǃLOADER,
			newluzhitu_XMLǃzwl1ǃLOADER,
			newluzhitu_XMLǃzwl2ǃLOADER,
			newluzhitu_XMLǃxwl0ǃLOADER,
			newluzhitu_XMLǃxwl1ǃLOADER,
			newluzhitu_XMLǃxwl2ǃLOADER,
			newluzhitu_XMLǃn598_CN2ǃTEXT,
			newluzhitu_XMLǃn598_ENǃTEXT,
			newluzhitu_XMLǃn598_INǃTEXT,
			newluzhitu_XMLǃn598_JPǃTEXT,
			newluzhitu_XMLǃn598_KRǃTEXT,
			newluzhitu_XMLǃn598_THǃTEXT,
			newluzhitu_XMLǃn598_VNǃTEXT,
			newluzhitu_XMLǃn598ǃTEXT,
			newluzhitu_XMLǃn599_CN2ǃTEXT,
			newluzhitu_XMLǃn599_ENǃTEXT,
			newluzhitu_XMLǃn599_INǃTEXT,
			newluzhitu_XMLǃn599_JPǃTEXT,
			newluzhitu_XMLǃn599_KRǃTEXT,
			newluzhitu_XMLǃn599_THǃTEXT,
			newluzhitu_XMLǃn599_VNǃTEXT,
			newluzhitu_XMLǃn599ǃTEXT,
			newluzhitu_XMLǃn600_CN2ǃTEXT,
			newluzhitu_XMLǃn600_ENǃTEXT,
			newluzhitu_XMLǃn600_INǃTEXT,
			newluzhitu_XMLǃn600_JPǃTEXT,
			newluzhitu_XMLǃn600_KRǃTEXT,
			newluzhitu_XMLǃn600_THǃTEXT,
			newluzhitu_XMLǃn600_VNǃTEXT,
			newluzhitu_XMLǃn600ǃTEXT,
			newluzhitu_XMLǃn601_CN2ǃTEXT,
			newluzhitu_XMLǃn601_ENǃTEXT,
			newluzhitu_XMLǃn601_INǃTEXT,
			newluzhitu_XMLǃn601_JPǃTEXT,
			newluzhitu_XMLǃn601_KRǃTEXT,
			newluzhitu_XMLǃn601_THǃTEXT,
			newluzhitu_XMLǃn601_VNǃTEXT,
			newluzhitu_XMLǃn601ǃTEXT,
			newluzhitu_XMLǃn602_CN2ǃTEXT,
			newluzhitu_XMLǃn602_ENǃTEXT,
			newluzhitu_XMLǃn602_INǃTEXT,
			newluzhitu_XMLǃn602_JPǃTEXT,
			newluzhitu_XMLǃn602_KRǃTEXT,
			newluzhitu_XMLǃn602_THǃTEXT,
			newluzhitu_XMLǃn602_VNǃTEXT,
			newluzhitu_XMLǃn602ǃTEXT,
			newluzhitu_XMLǃn603_CN2ǃTEXT,
			newluzhitu_XMLǃn603_ENǃTEXT,
			newluzhitu_XMLǃn603_INǃTEXT,
			newluzhitu_XMLǃn603_JPǃTEXT,
			newluzhitu_XMLǃn603_KRǃTEXT,
			newluzhitu_XMLǃn603_THǃTEXT,
			newluzhitu_XMLǃn603_VNǃTEXT,
			newluzhitu_XMLǃn603ǃTEXT,
			newluzhitu_XMLǃn605_CN2ǃTEXT,
			newluzhitu_XMLǃn605_ENǃTEXT,
			newluzhitu_XMLǃn605_INǃTEXT,
			newluzhitu_XMLǃn605_JPǃTEXT,
			newluzhitu_XMLǃn605_KRǃTEXT,
			newluzhitu_XMLǃn605_THǃTEXT,
			newluzhitu_XMLǃn605_VNǃTEXT,
			newluzhitu_XMLǃn605ǃTEXT,
			newluzhitu_XMLǃn606_CN2ǃTEXT,
			newluzhitu_XMLǃn606_ENǃTEXT,
			newluzhitu_XMLǃn606_INǃTEXT,
			newluzhitu_XMLǃn606_JPǃTEXT,
			newluzhitu_XMLǃn606_KRǃTEXT,
			newluzhitu_XMLǃn606_THǃTEXT,
			newluzhitu_XMLǃn606_VNǃTEXT,
			newluzhitu_XMLǃn606ǃTEXT
		]
		getController(name: '__language'): newluzhitu_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): newluzhitu_XMLǃ__languageǃCONTROLLER
		_controllers: [
			newluzhitu_XMLǃ__languageǃCONTROLLER
		]
	}
	interface newluzhitu_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan0ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan1ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan2ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan3ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan4ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan5ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan6ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan7ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan8ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan9ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan10ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan11ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan12ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan13ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan14ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan15ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan16ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan17ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan18ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan19ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan20ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan21ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan22ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan23ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan24ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan25ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan26ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan27ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan28ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan29ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan30ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan31ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan32ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan33ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan34ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan35ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan36ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan37ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan38ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan39ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan40ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan41ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan42ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan43ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan44ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan45ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan46ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan47ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan48ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan49ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan50ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan51ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan52ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan53ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan54ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan55ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan56ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan57ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan58ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhupan59ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface componentǁDaXiaoinfo_XML extends fairygui.GComponent{
		getChild(name: 'n1'): componentǁDaXiaoinfo_XMLǃn1ǃGRAPH
		getChildAt(index: 0): componentǁDaXiaoinfo_XMLǃn1ǃGRAPH
		getChildById(id: 'n642_wl2k'): componentǁDaXiaoinfo_XMLǃn1ǃGRAPH
		getChild(name: 'num'): componentǁDaXiaoinfo_XMLǃnumǃTEXT
		getChildAt(index: 1): componentǁDaXiaoinfo_XMLǃnumǃTEXT
		getChildById(id: 'n632_wl2k'): componentǁDaXiaoinfo_XMLǃnumǃTEXT
		_children: [
			componentǁDaXiaoinfo_XMLǃn1ǃGRAPH,
			componentǁDaXiaoinfo_XMLǃnumǃTEXT
		]
		getController(name: '__language'): componentǁDaXiaoinfo_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): componentǁDaXiaoinfo_XMLǃ__languageǃCONTROLLER
		_controllers: [
			componentǁDaXiaoinfo_XMLǃ__languageǃCONTROLLER
		]
	}
	interface componentǁDaXiaoinfo_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: componentǁDaXiaoinfo_XML
	}
	interface componentǁDaXiaoinfo_XMLǃn1ǃGRAPH extends fairygui.GGraph{
		parent: componentǁDaXiaoinfo_XML
	}
	interface componentǁDaXiaoinfo_XMLǃnumǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁDaXiaoinfo_XML
	}
	interface componentǁDaXiaoinfo_XMLǃnum_0ǃCOMPONENT extends componentǁDaXiaoinfo_XML{
		parent: newluzhitu_XML
	}
	interface componentǁDaXiaoinfo_XMLǃnum_1ǃCOMPONENT extends componentǁDaXiaoinfo_XML{
		parent: newluzhitu_XML
	}
	interface componentǁDaXiaoinfo_XMLǃnum_2ǃCOMPONENT extends componentǁDaXiaoinfo_XML{
		parent: newluzhitu_XML
	}
	interface componentǁDaXiaoinfo_XMLǃnum_3ǃCOMPONENT extends componentǁDaXiaoinfo_XML{
		parent: newluzhitu_XML
	}
	interface componentǁDaXiaoinfo_XMLǃnum_4ǃCOMPONENT extends componentǁDaXiaoinfo_XML{
		parent: newluzhitu_XML
	}
	interface componentǁDaXiaoinfo_XMLǃnum_5ǃCOMPONENT extends componentǁDaXiaoinfo_XML{
		parent: newluzhitu_XML
	}
	interface componentǁDaXiaoinfo_XMLǃnum_6ǃCOMPONENT extends componentǁDaXiaoinfo_XML{
		parent: newluzhitu_XML
	}
	interface componentǁDaXiaoinfo_XMLǃnum_7ǃCOMPONENT extends componentǁDaXiaoinfo_XML{
		parent: newluzhitu_XML
	}
	interface componentǁDaXiaoinfo_XMLǃnum_8ǃCOMPONENT extends componentǁDaXiaoinfo_XML{
		parent: newluzhitu_XML
	}
	interface componentǁDaXiaoinfo_XMLǃnum_9ǃCOMPONENT extends componentǁDaXiaoinfo_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XML extends fairygui.GComponent{
		getChild(name: 'loader'): daluComponent_XMLǃloaderǃLOADER
		getChildAt(index: 0): daluComponent_XMLǃloaderǃLOADER
		getChildById(id: 'n64_awmy'): daluComponent_XMLǃloaderǃLOADER
		getChild(name: 'num'): daluComponent_XMLǃnumǃTEXT
		getChildAt(index: 1): daluComponent_XMLǃnumǃTEXT
		getChildById(id: 'n65_awmy'): daluComponent_XMLǃnumǃTEXT
		_children: [
			daluComponent_XMLǃloaderǃLOADER,
			daluComponent_XMLǃnumǃTEXT
		]
		getController(name: '__language'): daluComponent_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): daluComponent_XMLǃ__languageǃCONTROLLER
		_controllers: [
			daluComponent_XMLǃ__languageǃCONTROLLER
		]
	}
	interface daluComponent_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: daluComponent_XML
	}
	interface daluComponent_XMLǃloaderǃLOADER extends fairygui.GLoader{
		parent: daluComponent_XML
	}
	interface daluComponent_XMLǃnumǃTEXT extends fairygui.GBasicTextField{
		parent: daluComponent_XML
	}
	interface daluComponent_XMLǃdalu0ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu1ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu2ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu3ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu4ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu5ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu6ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu7ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu8ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu9ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu10ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu11ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu12ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu13ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu14ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu15ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu16ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu17ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu18ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu19ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu20ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu21ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu22ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu23ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu24ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu25ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu26ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu27ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu28ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu29ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu30ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu31ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu32ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu33ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu34ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu35ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu36ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu37ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu38ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu39ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu40ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu41ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu42ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu43ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu44ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu45ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu46ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu47ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu48ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu49ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu50ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu51ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu52ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu53ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu54ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu55ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu56ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu57ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu58ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu59ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu60ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu61ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu62ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu63ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu64ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu65ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu66ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu67ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu68ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu69ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu70ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu71ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu72ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu73ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu74ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu75ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu76ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu77ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu78ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu79ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu80ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu81ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu82ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu83ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu84ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu85ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu86ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu87ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu88ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu89ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu90ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu91ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu92ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu93ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu94ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu95ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu96ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu97ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu98ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu99ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu100ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu101ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu102ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu103ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu104ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu105ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu106ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu107ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu108ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu109ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu110ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu111ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu112ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface daluComponent_XMLǃdalu113ǃCOMPONENT extends daluComponent_XML{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu0ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu1ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu2ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu3ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu4ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu5ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu6ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu7ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu8ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu9ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu10ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu11ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu12ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu13ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu14ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu15ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu16ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu17ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu18ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu19ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu20ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu21ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu22ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu23ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu24ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu25ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu26ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu27ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu28ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu29ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu30ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu31ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu32ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu33ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu34ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu35ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu36ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu37ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu38ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu39ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu40ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu41ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu42ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu43ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu44ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu45ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu46ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu47ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu48ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu49ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu50ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu51ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu52ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu53ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu54ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu55ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu56ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu57ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu58ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu59ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu60ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu61ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu62ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu63ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu64ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu65ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu66ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu67ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu68ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu69ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu70ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu71ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu72ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu73ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu74ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu75ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu76ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu77ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu78ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu79ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu80ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu81ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu82ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu83ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu84ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu85ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu86ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu87ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu88ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu89ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu90ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu91ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu92ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu93ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu94ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu95ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu96ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu97ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu98ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu99ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu100ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu101ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu102ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu103ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu104ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu105ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu106ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu107ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu108ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu109ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu110ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu111ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu112ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu113ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu114ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu115ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu116ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu117ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu118ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu119ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu120ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu121ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu122ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu123ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu124ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu125ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu126ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu127ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu128ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu129ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu130ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu131ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu132ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu133ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu134ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu135ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu136ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu137ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu138ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu139ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu140ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu141ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu142ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu143ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu144ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu145ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu146ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu147ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu148ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu149ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu150ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu151ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu152ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu153ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu154ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu155ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu156ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu157ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu158ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu159ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu160ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu161ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu162ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu163ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu164ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu165ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu166ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu167ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu168ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu169ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu170ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu171ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu172ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu173ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu174ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu175ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu176ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu177ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu178ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu179ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu180ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu181ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu182ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu183ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu184ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu185ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu186ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu187ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu188ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu189ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu190ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu191ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu192ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu193ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu194ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu195ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu196ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃdayanlu197ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu0ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu1ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu2ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu3ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu4ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu5ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu6ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu7ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu8ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu9ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu10ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu11ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu12ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu13ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu14ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu15ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu16ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu17ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu18ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu19ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu20ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu21ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu22ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu23ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu24ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu25ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu26ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu27ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu28ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu29ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu30ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu31ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu32ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu33ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu34ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu35ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu36ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu37ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu38ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu39ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu40ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu41ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu42ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu43ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu44ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu45ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu46ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu47ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu48ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu49ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu50ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu51ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu52ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu53ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu54ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu55ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu56ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu57ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu58ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu59ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu60ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu61ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu62ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu63ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu64ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu65ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu66ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu67ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu68ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu69ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu70ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu71ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu72ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu73ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu74ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu75ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu76ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu77ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu78ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu79ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu80ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu81ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu82ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu83ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu84ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu85ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu86ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu87ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu88ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu89ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu90ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu91ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu92ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu93ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu94ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaolu95ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu0ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu1ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu2ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu3ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu4ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu5ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu6ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu7ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu8ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu9ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu10ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu11ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu12ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu13ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu14ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu15ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu16ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu17ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu18ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu19ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu20ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu21ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu22ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu23ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu24ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu25ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu26ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu27ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu28ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu29ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu30ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu31ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu32ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu33ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu34ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu35ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu36ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu37ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu38ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu39ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu40ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu41ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu42ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu43ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu44ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu45ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu46ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu47ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu48ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu49ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu50ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu51ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu52ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu53ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu54ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu55ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu56ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu57ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu58ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu59ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu60ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu61ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu62ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu63ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu64ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu65ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu66ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu67ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu68ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu69ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu70ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu71ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu72ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu73ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu74ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu75ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu76ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu77ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu78ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu79ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu80ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu81ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu82ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu83ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu84ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu85ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu86ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu87ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu88ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu89ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu90ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu91ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu92ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu93ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu94ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu95ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu96ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu97ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu98ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu99ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu100ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxiaoqianglu101ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhuangǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxianǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃheǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzhuangduiǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxianduiǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃtotalǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzwl0ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzwl1ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃzwl2ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxwl0ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxwl1ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃxwl2ǃLOADER extends fairygui.GLoader{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn598_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn598_ENǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn598_INǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn598_JPǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn598_KRǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn598_THǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn598_VNǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn598ǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn599_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn599_ENǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn599_INǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn599_JPǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn599_KRǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn599_THǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn599_VNǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn599ǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn600_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn600_ENǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn600_INǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn600_JPǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn600_KRǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn600_THǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn600_VNǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn600ǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn601_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn601_ENǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn601_INǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn601_JPǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn601_KRǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn601_THǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn601_VNǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn601ǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn602_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn602_ENǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn602_INǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn602_JPǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn602_KRǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn602_THǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn602_VNǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn602ǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn603_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn603_ENǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn603_INǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn603_JPǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn603_KRǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn603_THǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn603_VNǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn603ǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn605_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn605_ENǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn605_INǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn605_JPǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn605_KRǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn605_THǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn605_VNǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn605ǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn606_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn606_ENǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn606_INǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn606_JPǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn606_KRǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn606_THǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn606_VNǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃn606ǃTEXT extends fairygui.GBasicTextField{
		parent: newluzhitu_XML
	}
	interface newluzhitu_XMLǃluzhituǃCOMPONENT extends newluzhitu_XML{
		parent: Main_XML
	}
	interface otherǁinfo_zhupan_XML extends fairygui.GComponent{
		getChild(name: 'n0'): otherǁinfo_zhupan_XMLǃn0ǃIMAGE
		getChildAt(index: 0): otherǁinfo_zhupan_XMLǃn0ǃIMAGE
		getChildById(id: 'n0_ww6c'): otherǁinfo_zhupan_XMLǃn0ǃIMAGE
		getChild(name: 'n1'): otherǁinfo_zhupan_XMLǃn1ǃTEXT
		getChildAt(index: 1): otherǁinfo_zhupan_XMLǃn1ǃTEXT
		getChildById(id: 'n1_ww6c'): otherǁinfo_zhupan_XMLǃn1ǃTEXT
		_children: [
			otherǁinfo_zhupan_XMLǃn0ǃIMAGE,
			otherǁinfo_zhupan_XMLǃn1ǃTEXT
		]
		getController(name: '__language'): otherǁinfo_zhupan_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): otherǁinfo_zhupan_XMLǃ__languageǃCONTROLLER
		_controllers: [
			otherǁinfo_zhupan_XMLǃ__languageǃCONTROLLER
		]
	}
	interface otherǁinfo_zhupan_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: otherǁinfo_zhupan_XML
	}
	interface otherǁinfo_zhupan_XMLǃn0ǃIMAGE extends fairygui.GImage{
		parent: otherǁinfo_zhupan_XML
	}
	interface otherǁinfo_zhupan_XMLǃn1ǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁinfo_zhupan_XML
	}
	interface otherǁinfo_zhupan_XMLǃinfo_zhupanǃCOMPONENT extends otherǁinfo_zhupan_XML{
		parent: Main_XML
	}
	interface settings_XML extends fairygui.GComponent{
		getChild(name: 'n20'): mask_empty_XMLǃn20ǃCOMPONENT
		getChildAt(index: 0): mask_empty_XMLǃn20ǃCOMPONENT
		getChildById(id: 'n20_s20q'): mask_empty_XMLǃn20ǃCOMPONENT
		getChild(name: 'n28'): settings_XMLǃn28ǃIMAGE
		getChildAt(index: 1): settings_XMLǃn28ǃIMAGE
		getChildById(id: 'n28_fn0e'): settings_XMLǃn28ǃIMAGE
		getChild(name: 'n0'): settings_XMLǃn0ǃIMAGE
		getChildAt(index: 2): settings_XMLǃn0ǃIMAGE
		getChildById(id: 'n0_mw5e'): settings_XMLǃn0ǃIMAGE
		getChild(name: 'n14_EN'): settings_XMLǃn14_ENǃIMAGE
		getChildAt(index: 3): settings_XMLǃn14_ENǃIMAGE
		getChildById(id: 'n14_puwk_EN'): settings_XMLǃn14_ENǃIMAGE
		getChild(name: 'n14_IN'): settings_XMLǃn14_INǃIMAGE
		getChildAt(index: 4): settings_XMLǃn14_INǃIMAGE
		getChildById(id: 'n14_puwk_IN'): settings_XMLǃn14_INǃIMAGE
		getChild(name: 'n14_JP'): settings_XMLǃn14_JPǃIMAGE
		getChildAt(index: 5): settings_XMLǃn14_JPǃIMAGE
		getChildById(id: 'n14_puwk_JP'): settings_XMLǃn14_JPǃIMAGE
		getChild(name: 'n14_KR'): settings_XMLǃn14_KRǃIMAGE
		getChildAt(index: 6): settings_XMLǃn14_KRǃIMAGE
		getChildById(id: 'n14_puwk_KR'): settings_XMLǃn14_KRǃIMAGE
		getChild(name: 'n14_TH'): settings_XMLǃn14_THǃIMAGE
		getChildAt(index: 7): settings_XMLǃn14_THǃIMAGE
		getChildById(id: 'n14_puwk_TH'): settings_XMLǃn14_THǃIMAGE
		getChild(name: 'n14_VN'): settings_XMLǃn14_VNǃIMAGE
		getChildAt(index: 8): settings_XMLǃn14_VNǃIMAGE
		getChildById(id: 'n14_puwk_VN'): settings_XMLǃn14_VNǃIMAGE
		getChild(name: 'n14_CN2'): settings_XMLǃn14_CN2ǃIMAGE
		getChildAt(index: 9): settings_XMLǃn14_CN2ǃIMAGE
		getChildById(id: 'n14_puwk_CN2'): settings_XMLǃn14_CN2ǃIMAGE
		getChild(name: 'n14'): settings_XMLǃn14ǃIMAGE
		getChildAt(index: 10): settings_XMLǃn14ǃIMAGE
		getChildById(id: 'n14_puwk'): settings_XMLǃn14ǃIMAGE
		getChild(name: 'button_close'): buttonsǁbutton_guanbiluzhitu_XMLǃbutton_closeǃCOMPONENT
		getChildAt(index: 11): buttonsǁbutton_guanbiluzhitu_XMLǃbutton_closeǃCOMPONENT
		getChildById(id: 'n9_jwy6'): buttonsǁbutton_guanbiluzhitu_XMLǃbutton_closeǃCOMPONENT
		getChild(name: 'music_slider'): settingsǁsetting_slider_XMLǃmusic_sliderǃCOMPONENT
		getChildAt(index: 12): settingsǁsetting_slider_XMLǃmusic_sliderǃCOMPONENT
		getChildById(id: 'n11_mjdg'): settingsǁsetting_slider_XMLǃmusic_sliderǃCOMPONENT
		getChild(name: 'button_music'): settingsǁsetting_onoff_button_XMLǃbutton_musicǃCOMPONENT
		getChildAt(index: 13): settingsǁsetting_onoff_button_XMLǃbutton_musicǃCOMPONENT
		getChildById(id: 'n3_jwy6'): settingsǁsetting_onoff_button_XMLǃbutton_musicǃCOMPONENT
		getChild(name: 'button_sound'): settingsǁsetting_onoff_button_XMLǃbutton_soundǃCOMPONENT
		getChildAt(index: 14): settingsǁsetting_onoff_button_XMLǃbutton_soundǃCOMPONENT
		getChildById(id: 'n4_jwy6'): settingsǁsetting_onoff_button_XMLǃbutton_soundǃCOMPONENT
		getChild(name: 'sound_slider'): settingsǁsetting_slider_XMLǃsound_sliderǃCOMPONENT
		getChildAt(index: 15): settingsǁsetting_slider_XMLǃsound_sliderǃCOMPONENT
		getChildById(id: 'n12_mjdg'): settingsǁsetting_slider_XMLǃsound_sliderǃCOMPONENT
		getChild(name: 'n15_EN'): settings_XMLǃn15_ENǃIMAGE
		getChildAt(index: 16): settings_XMLǃn15_ENǃIMAGE
		getChildById(id: 'n15_puwk_EN'): settings_XMLǃn15_ENǃIMAGE
		getChild(name: 'n15_IN'): settings_XMLǃn15_INǃIMAGE
		getChildAt(index: 17): settings_XMLǃn15_INǃIMAGE
		getChildById(id: 'n15_puwk_IN'): settings_XMLǃn15_INǃIMAGE
		getChild(name: 'n15_JP'): settings_XMLǃn15_JPǃIMAGE
		getChildAt(index: 18): settings_XMLǃn15_JPǃIMAGE
		getChildById(id: 'n15_puwk_JP'): settings_XMLǃn15_JPǃIMAGE
		getChild(name: 'n15_KR'): settings_XMLǃn15_KRǃIMAGE
		getChildAt(index: 19): settings_XMLǃn15_KRǃIMAGE
		getChildById(id: 'n15_puwk_KR'): settings_XMLǃn15_KRǃIMAGE
		getChild(name: 'n15_TH'): settings_XMLǃn15_THǃIMAGE
		getChildAt(index: 20): settings_XMLǃn15_THǃIMAGE
		getChildById(id: 'n15_puwk_TH'): settings_XMLǃn15_THǃIMAGE
		getChild(name: 'n15_VN'): settings_XMLǃn15_VNǃIMAGE
		getChildAt(index: 21): settings_XMLǃn15_VNǃIMAGE
		getChildById(id: 'n15_puwk_VN'): settings_XMLǃn15_VNǃIMAGE
		getChild(name: 'n15_CN2'): settings_XMLǃn15_CN2ǃIMAGE
		getChildAt(index: 22): settings_XMLǃn15_CN2ǃIMAGE
		getChildById(id: 'n15_puwk_CN2'): settings_XMLǃn15_CN2ǃIMAGE
		getChild(name: 'n15'): settings_XMLǃn15ǃIMAGE
		getChildAt(index: 23): settings_XMLǃn15ǃIMAGE
		getChildById(id: 'n15_puwk'): settings_XMLǃn15ǃIMAGE
		getChild(name: 'n16'): settings_XMLǃn16ǃIMAGE
		getChildAt(index: 24): settings_XMLǃn16ǃIMAGE
		getChildById(id: 'n16_puwk'): settings_XMLǃn16ǃIMAGE
		getChild(name: 'n17_EN'): settings_XMLǃn17_ENǃIMAGE
		getChildAt(index: 25): settings_XMLǃn17_ENǃIMAGE
		getChildById(id: 'n17_puwk_EN'): settings_XMLǃn17_ENǃIMAGE
		getChild(name: 'n17_IN'): settings_XMLǃn17_INǃIMAGE
		getChildAt(index: 26): settings_XMLǃn17_INǃIMAGE
		getChildById(id: 'n17_puwk_IN'): settings_XMLǃn17_INǃIMAGE
		getChild(name: 'n17_JP'): settings_XMLǃn17_JPǃIMAGE
		getChildAt(index: 27): settings_XMLǃn17_JPǃIMAGE
		getChildById(id: 'n17_puwk_JP'): settings_XMLǃn17_JPǃIMAGE
		getChild(name: 'n17_KR'): settings_XMLǃn17_KRǃIMAGE
		getChildAt(index: 28): settings_XMLǃn17_KRǃIMAGE
		getChildById(id: 'n17_puwk_KR'): settings_XMLǃn17_KRǃIMAGE
		getChild(name: 'n17_TH'): settings_XMLǃn17_THǃIMAGE
		getChildAt(index: 29): settings_XMLǃn17_THǃIMAGE
		getChildById(id: 'n17_puwk_TH'): settings_XMLǃn17_THǃIMAGE
		getChild(name: 'n17_VN'): settings_XMLǃn17_VNǃIMAGE
		getChildAt(index: 30): settings_XMLǃn17_VNǃIMAGE
		getChildById(id: 'n17_puwk_VN'): settings_XMLǃn17_VNǃIMAGE
		getChild(name: 'n17_CN2'): settings_XMLǃn17_CN2ǃIMAGE
		getChildAt(index: 31): settings_XMLǃn17_CN2ǃIMAGE
		getChildById(id: 'n17_puwk_CN2'): settings_XMLǃn17_CN2ǃIMAGE
		getChild(name: 'n17'): settings_XMLǃn17ǃIMAGE
		getChildAt(index: 32): settings_XMLǃn17ǃIMAGE
		getChildById(id: 'n17_puwk'): settings_XMLǃn17ǃIMAGE
		getChild(name: 'n18'): settings_XMLǃn18ǃIMAGE
		getChildAt(index: 33): settings_XMLǃn18ǃIMAGE
		getChildById(id: 'n18_puwk'): settings_XMLǃn18ǃIMAGE
		getChild(name: 'n22_EN'): settings_XMLǃn22_ENǃIMAGE
		getChildAt(index: 34): settings_XMLǃn22_ENǃIMAGE
		getChildById(id: 'n22_fn0e_EN'): settings_XMLǃn22_ENǃIMAGE
		getChild(name: 'n22_IN'): settings_XMLǃn22_INǃIMAGE
		getChildAt(index: 35): settings_XMLǃn22_INǃIMAGE
		getChildById(id: 'n22_fn0e_IN'): settings_XMLǃn22_INǃIMAGE
		getChild(name: 'n22_JP'): settings_XMLǃn22_JPǃIMAGE
		getChildAt(index: 36): settings_XMLǃn22_JPǃIMAGE
		getChildById(id: 'n22_fn0e_JP'): settings_XMLǃn22_JPǃIMAGE
		getChild(name: 'n22_KR'): settings_XMLǃn22_KRǃIMAGE
		getChildAt(index: 37): settings_XMLǃn22_KRǃIMAGE
		getChildById(id: 'n22_fn0e_KR'): settings_XMLǃn22_KRǃIMAGE
		getChild(name: 'n22_TH'): settings_XMLǃn22_THǃIMAGE
		getChildAt(index: 38): settings_XMLǃn22_THǃIMAGE
		getChildById(id: 'n22_fn0e_TH'): settings_XMLǃn22_THǃIMAGE
		getChild(name: 'n22_VN'): settings_XMLǃn22_VNǃIMAGE
		getChildAt(index: 39): settings_XMLǃn22_VNǃIMAGE
		getChildById(id: 'n22_fn0e_VN'): settings_XMLǃn22_VNǃIMAGE
		getChild(name: 'n22_CN2'): settings_XMLǃn22_CN2ǃIMAGE
		getChildAt(index: 40): settings_XMLǃn22_CN2ǃIMAGE
		getChildById(id: 'n22_fn0e_CN2'): settings_XMLǃn22_CN2ǃIMAGE
		getChild(name: 'n22'): settings_XMLǃn22ǃIMAGE
		getChildAt(index: 41): settings_XMLǃn22ǃIMAGE
		getChildById(id: 'n22_fn0e'): settings_XMLǃn22ǃIMAGE
		getChild(name: 'n23'): settings_XMLǃn23ǃIMAGE
		getChildAt(index: 42): settings_XMLǃn23ǃIMAGE
		getChildById(id: 'n23_fn0e'): settings_XMLǃn23ǃIMAGE
		getChild(name: 'button_danmu'): settingsǁsetting_onoff_button_XMLǃbutton_danmuǃCOMPONENT
		getChildAt(index: 43): settingsǁsetting_onoff_button_XMLǃbutton_danmuǃCOMPONENT
		getChildById(id: 'n24_fn0e'): settingsǁsetting_onoff_button_XMLǃbutton_danmuǃCOMPONENT
		getChild(name: 'chouma'): settingsǁchoumaSettings_XMLǃchoumaǃCOMPONENT
		getChildAt(index: 44): settingsǁchoumaSettings_XMLǃchoumaǃCOMPONENT
		getChildById(id: 'n21_fn0e'): settingsǁchoumaSettings_XMLǃchoumaǃCOMPONENT
		getChild(name: 'btn_confirm'): buttonsǁbutton_queren_XMLǃbtn_confirmǃCOMPONENT
		getChildAt(index: 45): buttonsǁbutton_queren_XMLǃbtn_confirmǃCOMPONENT
		getChildById(id: 'n25_fn0e'): buttonsǁbutton_queren_XMLǃbtn_confirmǃCOMPONENT
		getChild(name: 'btn_cancel'): buttonsǁbutton_quxiao_XMLǃbtn_cancelǃCOMPONENT
		getChildAt(index: 46): buttonsǁbutton_quxiao_XMLǃbtn_cancelǃCOMPONENT
		getChildById(id: 'n26_fn0e'): buttonsǁbutton_quxiao_XMLǃbtn_cancelǃCOMPONENT
		getChild(name: 'n27_CN2'): settings_XMLǃn27_CN2ǃTEXT
		getChildAt(index: 47): settings_XMLǃn27_CN2ǃTEXT
		getChildById(id: 'n27_fn0e_CN2'): settings_XMLǃn27_CN2ǃTEXT
		getChild(name: 'n27_EN'): settings_XMLǃn27_ENǃTEXT
		getChildAt(index: 48): settings_XMLǃn27_ENǃTEXT
		getChildById(id: 'n27_fn0e_EN'): settings_XMLǃn27_ENǃTEXT
		getChild(name: 'n27_IN'): settings_XMLǃn27_INǃTEXT
		getChildAt(index: 49): settings_XMLǃn27_INǃTEXT
		getChildById(id: 'n27_fn0e_IN'): settings_XMLǃn27_INǃTEXT
		getChild(name: 'n27_JP'): settings_XMLǃn27_JPǃTEXT
		getChildAt(index: 50): settings_XMLǃn27_JPǃTEXT
		getChildById(id: 'n27_fn0e_JP'): settings_XMLǃn27_JPǃTEXT
		getChild(name: 'n27_KR'): settings_XMLǃn27_KRǃTEXT
		getChildAt(index: 51): settings_XMLǃn27_KRǃTEXT
		getChildById(id: 'n27_fn0e_KR'): settings_XMLǃn27_KRǃTEXT
		getChild(name: 'n27_TH'): settings_XMLǃn27_THǃTEXT
		getChildAt(index: 52): settings_XMLǃn27_THǃTEXT
		getChildById(id: 'n27_fn0e_TH'): settings_XMLǃn27_THǃTEXT
		getChild(name: 'n27_VN'): settings_XMLǃn27_VNǃTEXT
		getChildAt(index: 53): settings_XMLǃn27_VNǃTEXT
		getChildById(id: 'n27_fn0e_VN'): settings_XMLǃn27_VNǃTEXT
		getChild(name: 'n27'): settings_XMLǃn27ǃTEXT
		getChildAt(index: 54): settings_XMLǃn27ǃTEXT
		getChildById(id: 'n27_fn0e'): settings_XMLǃn27ǃTEXT
		getChild(name: 'language_panel'): languageǁselection_panel_XMLǃlanguage_panelǃCOMPONENT
		getChildAt(index: 55): languageǁselection_panel_XMLǃlanguage_panelǃCOMPONENT
		getChildById(id: 'n63_hnm1'): languageǁselection_panel_XMLǃlanguage_panelǃCOMPONENT
		_children: [
			mask_empty_XMLǃn20ǃCOMPONENT,
			settings_XMLǃn28ǃIMAGE,
			settings_XMLǃn0ǃIMAGE,
			settings_XMLǃn14_ENǃIMAGE,
			settings_XMLǃn14_INǃIMAGE,
			settings_XMLǃn14_JPǃIMAGE,
			settings_XMLǃn14_KRǃIMAGE,
			settings_XMLǃn14_THǃIMAGE,
			settings_XMLǃn14_VNǃIMAGE,
			settings_XMLǃn14_CN2ǃIMAGE,
			settings_XMLǃn14ǃIMAGE,
			buttonsǁbutton_guanbiluzhitu_XMLǃbutton_closeǃCOMPONENT,
			settingsǁsetting_slider_XMLǃmusic_sliderǃCOMPONENT,
			settingsǁsetting_onoff_button_XMLǃbutton_musicǃCOMPONENT,
			settingsǁsetting_onoff_button_XMLǃbutton_soundǃCOMPONENT,
			settingsǁsetting_slider_XMLǃsound_sliderǃCOMPONENT,
			settings_XMLǃn15_ENǃIMAGE,
			settings_XMLǃn15_INǃIMAGE,
			settings_XMLǃn15_JPǃIMAGE,
			settings_XMLǃn15_KRǃIMAGE,
			settings_XMLǃn15_THǃIMAGE,
			settings_XMLǃn15_VNǃIMAGE,
			settings_XMLǃn15_CN2ǃIMAGE,
			settings_XMLǃn15ǃIMAGE,
			settings_XMLǃn16ǃIMAGE,
			settings_XMLǃn17_ENǃIMAGE,
			settings_XMLǃn17_INǃIMAGE,
			settings_XMLǃn17_JPǃIMAGE,
			settings_XMLǃn17_KRǃIMAGE,
			settings_XMLǃn17_THǃIMAGE,
			settings_XMLǃn17_VNǃIMAGE,
			settings_XMLǃn17_CN2ǃIMAGE,
			settings_XMLǃn17ǃIMAGE,
			settings_XMLǃn18ǃIMAGE,
			settings_XMLǃn22_ENǃIMAGE,
			settings_XMLǃn22_INǃIMAGE,
			settings_XMLǃn22_JPǃIMAGE,
			settings_XMLǃn22_KRǃIMAGE,
			settings_XMLǃn22_THǃIMAGE,
			settings_XMLǃn22_VNǃIMAGE,
			settings_XMLǃn22_CN2ǃIMAGE,
			settings_XMLǃn22ǃIMAGE,
			settings_XMLǃn23ǃIMAGE,
			settingsǁsetting_onoff_button_XMLǃbutton_danmuǃCOMPONENT,
			settingsǁchoumaSettings_XMLǃchoumaǃCOMPONENT,
			buttonsǁbutton_queren_XMLǃbtn_confirmǃCOMPONENT,
			buttonsǁbutton_quxiao_XMLǃbtn_cancelǃCOMPONENT,
			settings_XMLǃn27_CN2ǃTEXT,
			settings_XMLǃn27_ENǃTEXT,
			settings_XMLǃn27_INǃTEXT,
			settings_XMLǃn27_JPǃTEXT,
			settings_XMLǃn27_KRǃTEXT,
			settings_XMLǃn27_THǃTEXT,
			settings_XMLǃn27_VNǃTEXT,
			settings_XMLǃn27ǃTEXT,
			languageǁselection_panel_XMLǃlanguage_panelǃCOMPONENT
		]
		getController(name: '__language'): settings_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): settings_XMLǃ__languageǃCONTROLLER
		_controllers: [
			settings_XMLǃ__languageǃCONTROLLER
		]
	}
	interface settings_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: settings_XML
	}
	interface mask_empty_XML extends fairygui.GComponent{
		getChild(name: 'n0'): mask_empty_XMLǃn0ǃIMAGE
		getChildAt(index: 0): mask_empty_XMLǃn0ǃIMAGE
		getChildById(id: 'n0_p861'): mask_empty_XMLǃn0ǃIMAGE
		_children: [
			mask_empty_XMLǃn0ǃIMAGE
		]
		getController(name: '__language'): mask_empty_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): mask_empty_XMLǃ__languageǃCONTROLLER
		_controllers: [
			mask_empty_XMLǃ__languageǃCONTROLLER
		]
	}
	interface mask_empty_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: mask_empty_XML
	}
	interface mask_empty_XMLǃn0ǃIMAGE extends fairygui.GImage{
		parent: mask_empty_XML
	}
	interface mask_empty_XMLǃn20ǃCOMPONENT extends mask_empty_XML{
		parent: settings_XML
	}
	interface settings_XMLǃn28ǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface settings_XMLǃn0ǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface settings_XMLǃn14_ENǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface settings_XMLǃn14_INǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface settings_XMLǃn14_JPǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface settings_XMLǃn14_KRǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface settings_XMLǃn14_THǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface settings_XMLǃn14_VNǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface settings_XMLǃn14_CN2ǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface settings_XMLǃn14ǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface buttonsǁbutton_guanbiluzhitu_XML extends fairygui.GButton{
		getChild(name: 'n1'): buttonsǁbutton_guanbiluzhitu_XMLǃn1ǃIMAGE
		getChildAt(index: 0): buttonsǁbutton_guanbiluzhitu_XMLǃn1ǃIMAGE
		getChildById(id: 'n1'): buttonsǁbutton_guanbiluzhitu_XMLǃn1ǃIMAGE
		_children: [
			buttonsǁbutton_guanbiluzhitu_XMLǃn1ǃIMAGE
		]
		getController(name: '__language'): buttonsǁbutton_guanbiluzhitu_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁbutton_guanbiluzhitu_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁbutton_guanbiluzhitu_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁbutton_guanbiluzhitu_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁbutton_guanbiluzhitu_XMLǃ__languageǃCONTROLLER,
			buttonsǁbutton_guanbiluzhitu_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁbutton_guanbiluzhitu_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁbutton_guanbiluzhitu_XML
	}
	interface buttonsǁbutton_guanbiluzhitu_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁbutton_guanbiluzhitu_XML
	}
	interface buttonsǁbutton_guanbiluzhitu_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁbutton_guanbiluzhitu_XML
	}
	interface buttonsǁbutton_guanbiluzhitu_XMLǃbutton_closeǃCOMPONENT extends buttonsǁbutton_guanbiluzhitu_XML{
		parent: settings_XML
	}
	interface settingsǁsetting_slider_XML extends fairygui.GSlider{
		getChild(name: 'n1'): settingsǁsetting_slider_XMLǃn1ǃIMAGE
		getChildAt(index: 0): settingsǁsetting_slider_XMLǃn1ǃIMAGE
		getChildById(id: 'n1'): settingsǁsetting_slider_XMLǃn1ǃIMAGE
		getChild(name: 'bar'): settingsǁsetting_slider_XMLǃbarǃIMAGE
		getChildAt(index: 1): settingsǁsetting_slider_XMLǃbarǃIMAGE
		getChildById(id: 'n2'): settingsǁsetting_slider_XMLǃbarǃIMAGE
		getChild(name: 'grip'): settingsǁsetting_slider_XMLǃgripǃIMAGE
		getChildAt(index: 2): settingsǁsetting_slider_XMLǃgripǃIMAGE
		getChildById(id: 'n3'): settingsǁsetting_slider_XMLǃgripǃIMAGE
		_children: [
			settingsǁsetting_slider_XMLǃn1ǃIMAGE,
			settingsǁsetting_slider_XMLǃbarǃIMAGE,
			settingsǁsetting_slider_XMLǃgripǃIMAGE
		]
		getController(name: '__language'): settingsǁsetting_slider_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): settingsǁsetting_slider_XMLǃ__languageǃCONTROLLER
		_controllers: [
			settingsǁsetting_slider_XMLǃ__languageǃCONTROLLER
		]
	}
	interface settingsǁsetting_slider_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: settingsǁsetting_slider_XML
	}
	interface settingsǁsetting_slider_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: settingsǁsetting_slider_XML
	}
	interface settingsǁsetting_slider_XMLǃbarǃIMAGE extends fairygui.GImage{
		parent: settingsǁsetting_slider_XML
	}
	interface settingsǁsetting_slider_XMLǃgripǃIMAGE extends fairygui.GImage{
		parent: settingsǁsetting_slider_XML
	}
	interface settingsǁsetting_slider_XMLǃmusic_sliderǃCOMPONENT extends settingsǁsetting_slider_XML{
		parent: settings_XML
	}
	interface settingsǁsetting_onoff_button_XML extends fairygui.GButton{
		getChild(name: 'n1'): settingsǁsetting_onoff_button_XMLǃn1ǃIMAGE
		getChildAt(index: 0): settingsǁsetting_onoff_button_XMLǃn1ǃIMAGE
		getChildById(id: 'n1'): settingsǁsetting_onoff_button_XMLǃn1ǃIMAGE
		getChild(name: 'n4'): settingsǁsetting_onoff_button_XMLǃn4ǃIMAGE
		getChildAt(index: 1): settingsǁsetting_onoff_button_XMLǃn4ǃIMAGE
		getChildById(id: 'n4_jwy6'): settingsǁsetting_onoff_button_XMLǃn4ǃIMAGE
		getChild(name: 'n5'): settingsǁsetting_onoff_button_XMLǃn5ǃTEXT
		getChildAt(index: 2): settingsǁsetting_onoff_button_XMLǃn5ǃTEXT
		getChildById(id: 'n5_sbyt'): settingsǁsetting_onoff_button_XMLǃn5ǃTEXT
		getChild(name: 'n6'): settingsǁsetting_onoff_button_XMLǃn6ǃTEXT
		getChildAt(index: 3): settingsǁsetting_onoff_button_XMLǃn6ǃTEXT
		getChildById(id: 'n6_sbyt'): settingsǁsetting_onoff_button_XMLǃn6ǃTEXT
		_children: [
			settingsǁsetting_onoff_button_XMLǃn1ǃIMAGE,
			settingsǁsetting_onoff_button_XMLǃn4ǃIMAGE,
			settingsǁsetting_onoff_button_XMLǃn5ǃTEXT,
			settingsǁsetting_onoff_button_XMLǃn6ǃTEXT
		]
		getController(name: '__language'): settingsǁsetting_onoff_button_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): settingsǁsetting_onoff_button_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): settingsǁsetting_onoff_button_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): settingsǁsetting_onoff_button_XMLǃbuttonǃCONTROLLER
		_controllers: [
			settingsǁsetting_onoff_button_XMLǃ__languageǃCONTROLLER,
			settingsǁsetting_onoff_button_XMLǃbuttonǃCONTROLLER
		]
	}
	interface settingsǁsetting_onoff_button_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: settingsǁsetting_onoff_button_XML
	}
	interface settingsǁsetting_onoff_button_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: settingsǁsetting_onoff_button_XML
	}
	interface settingsǁsetting_onoff_button_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: settingsǁsetting_onoff_button_XML
	}
	interface settingsǁsetting_onoff_button_XMLǃn4ǃIMAGE extends fairygui.GImage{
		parent: settingsǁsetting_onoff_button_XML
	}
	interface settingsǁsetting_onoff_button_XMLǃn5ǃTEXT extends fairygui.GBasicTextField{
		parent: settingsǁsetting_onoff_button_XML
	}
	interface settingsǁsetting_onoff_button_XMLǃn6ǃTEXT extends fairygui.GBasicTextField{
		parent: settingsǁsetting_onoff_button_XML
	}
	interface settingsǁsetting_onoff_button_XMLǃbutton_musicǃCOMPONENT extends settingsǁsetting_onoff_button_XML{
		parent: settings_XML
	}
	interface settingsǁsetting_onoff_button_XMLǃbutton_soundǃCOMPONENT extends settingsǁsetting_onoff_button_XML{
		parent: settings_XML
	}
	interface settingsǁsetting_slider_XMLǃsound_sliderǃCOMPONENT extends settingsǁsetting_slider_XML{
		parent: settings_XML
	}
	interface settings_XMLǃn15_ENǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface settings_XMLǃn15_INǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface settings_XMLǃn15_JPǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface settings_XMLǃn15_KRǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface settings_XMLǃn15_THǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface settings_XMLǃn15_VNǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface settings_XMLǃn15_CN2ǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface settings_XMLǃn15ǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface settings_XMLǃn16ǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface settings_XMLǃn17_ENǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface settings_XMLǃn17_INǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface settings_XMLǃn17_JPǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface settings_XMLǃn17_KRǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface settings_XMLǃn17_THǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface settings_XMLǃn17_VNǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface settings_XMLǃn17_CN2ǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface settings_XMLǃn17ǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface settings_XMLǃn18ǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface settings_XMLǃn22_ENǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface settings_XMLǃn22_INǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface settings_XMLǃn22_JPǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface settings_XMLǃn22_KRǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface settings_XMLǃn22_THǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface settings_XMLǃn22_VNǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface settings_XMLǃn22_CN2ǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface settings_XMLǃn22ǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface settings_XMLǃn23ǃIMAGE extends fairygui.GImage{
		parent: settings_XML
	}
	interface settingsǁsetting_onoff_button_XMLǃbutton_danmuǃCOMPONENT extends settingsǁsetting_onoff_button_XML{
		parent: settings_XML
	}
	interface settingsǁchoumaSettings_XML extends fairygui.GComponent{
		getChild(name: 'cm_0'): buttonsǁchoumaǁshezhi_1kuai_XMLǃcm_0ǃCOMPONENT
		getChildAt(index: 0): buttonsǁchoumaǁshezhi_1kuai_XMLǃcm_0ǃCOMPONENT
		getChildById(id: 'n37_fn0e'): buttonsǁchoumaǁshezhi_1kuai_XMLǃcm_0ǃCOMPONENT
		getChild(name: 'cm_1'): buttonsǁchoumaǁshezhi_2kuai_XMLǃcm_1ǃCOMPONENT
		getChildAt(index: 1): buttonsǁchoumaǁshezhi_2kuai_XMLǃcm_1ǃCOMPONENT
		getChildById(id: 'n27_cvjj'): buttonsǁchoumaǁshezhi_2kuai_XMLǃcm_1ǃCOMPONENT
		getChild(name: 'cm_2'): buttonsǁchoumaǁshezhi_5kuai_XMLǃcm_2ǃCOMPONENT
		getChildAt(index: 2): buttonsǁchoumaǁshezhi_5kuai_XMLǃcm_2ǃCOMPONENT
		getChildById(id: 'n28_cvjj'): buttonsǁchoumaǁshezhi_5kuai_XMLǃcm_2ǃCOMPONENT
		getChild(name: 'cm_3'): buttonsǁchoumaǁshezhi_10kuai_XMLǃcm_3ǃCOMPONENT
		getChildAt(index: 3): buttonsǁchoumaǁshezhi_10kuai_XMLǃcm_3ǃCOMPONENT
		getChildById(id: 'n29_cvjj'): buttonsǁchoumaǁshezhi_10kuai_XMLǃcm_3ǃCOMPONENT
		getChild(name: 'cm_4'): buttonsǁchoumaǁshezhi_20kuai_XMLǃcm_4ǃCOMPONENT
		getChildAt(index: 4): buttonsǁchoumaǁshezhi_20kuai_XMLǃcm_4ǃCOMPONENT
		getChildById(id: 'n30_cvjj'): buttonsǁchoumaǁshezhi_20kuai_XMLǃcm_4ǃCOMPONENT
		getChild(name: 'cm_5'): buttonsǁchoumaǁshezhi_50kuai_XMLǃcm_5ǃCOMPONENT
		getChildAt(index: 5): buttonsǁchoumaǁshezhi_50kuai_XMLǃcm_5ǃCOMPONENT
		getChildById(id: 'n31_cvjj'): buttonsǁchoumaǁshezhi_50kuai_XMLǃcm_5ǃCOMPONENT
		getChild(name: 'cm_6'): buttonsǁchoumaǁshezhi_100kuai_XMLǃcm_6ǃCOMPONENT
		getChildAt(index: 6): buttonsǁchoumaǁshezhi_100kuai_XMLǃcm_6ǃCOMPONENT
		getChildById(id: 'n32_cvjj'): buttonsǁchoumaǁshezhi_100kuai_XMLǃcm_6ǃCOMPONENT
		getChild(name: 'cm_7'): buttonsǁchoumaǁshezhi_200kuai_XMLǃcm_7ǃCOMPONENT
		getChildAt(index: 7): buttonsǁchoumaǁshezhi_200kuai_XMLǃcm_7ǃCOMPONENT
		getChildById(id: 'n33_cvjj'): buttonsǁchoumaǁshezhi_200kuai_XMLǃcm_7ǃCOMPONENT
		getChild(name: 'cm_8'): buttonsǁchoumaǁshezhi_500kuai_XMLǃcm_8ǃCOMPONENT
		getChildAt(index: 8): buttonsǁchoumaǁshezhi_500kuai_XMLǃcm_8ǃCOMPONENT
		getChildById(id: 'n34_cvjj'): buttonsǁchoumaǁshezhi_500kuai_XMLǃcm_8ǃCOMPONENT
		getChild(name: 'cm_9'): buttonsǁchoumaǁshezhi_1k_XMLǃcm_9ǃCOMPONENT
		getChildAt(index: 9): buttonsǁchoumaǁshezhi_1k_XMLǃcm_9ǃCOMPONENT
		getChildById(id: 'n36_ikxl'): buttonsǁchoumaǁshezhi_1k_XMLǃcm_9ǃCOMPONENT
		getChild(name: 'cm_10'): buttonsǁchoumaǁshezhi_2000kuai_XMLǃcm_10ǃCOMPONENT
		getChildAt(index: 10): buttonsǁchoumaǁshezhi_2000kuai_XMLǃcm_10ǃCOMPONENT
		getChildById(id: 'n38_fn0e'): buttonsǁchoumaǁshezhi_2000kuai_XMLǃcm_10ǃCOMPONENT
		getChild(name: 'cm_11'): buttonsǁchoumaǁshezhi_10k_XMLǃcm_11ǃCOMPONENT
		getChildAt(index: 11): buttonsǁchoumaǁshezhi_10k_XMLǃcm_11ǃCOMPONENT
		getChildById(id: 'n35_ikxl'): buttonsǁchoumaǁshezhi_10k_XMLǃcm_11ǃCOMPONENT
		_children: [
			buttonsǁchoumaǁshezhi_1kuai_XMLǃcm_0ǃCOMPONENT,
			buttonsǁchoumaǁshezhi_2kuai_XMLǃcm_1ǃCOMPONENT,
			buttonsǁchoumaǁshezhi_5kuai_XMLǃcm_2ǃCOMPONENT,
			buttonsǁchoumaǁshezhi_10kuai_XMLǃcm_3ǃCOMPONENT,
			buttonsǁchoumaǁshezhi_20kuai_XMLǃcm_4ǃCOMPONENT,
			buttonsǁchoumaǁshezhi_50kuai_XMLǃcm_5ǃCOMPONENT,
			buttonsǁchoumaǁshezhi_100kuai_XMLǃcm_6ǃCOMPONENT,
			buttonsǁchoumaǁshezhi_200kuai_XMLǃcm_7ǃCOMPONENT,
			buttonsǁchoumaǁshezhi_500kuai_XMLǃcm_8ǃCOMPONENT,
			buttonsǁchoumaǁshezhi_1k_XMLǃcm_9ǃCOMPONENT,
			buttonsǁchoumaǁshezhi_2000kuai_XMLǃcm_10ǃCOMPONENT,
			buttonsǁchoumaǁshezhi_10k_XMLǃcm_11ǃCOMPONENT
		]
		getController(name: '__language'): settingsǁchoumaSettings_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): settingsǁchoumaSettings_XMLǃ__languageǃCONTROLLER
		_controllers: [
			settingsǁchoumaSettings_XMLǃ__languageǃCONTROLLER
		]
	}
	interface settingsǁchoumaSettings_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: settingsǁchoumaSettings_XML
	}
	interface buttonsǁchoumaǁshezhi_1kuai_XML extends fairygui.GButton{
		getChild(name: 'n1'): buttonsǁchoumaǁshezhi_1kuai_XMLǃn1ǃIMAGE
		getChildAt(index: 0): buttonsǁchoumaǁshezhi_1kuai_XMLǃn1ǃIMAGE
		getChildById(id: 'n1'): buttonsǁchoumaǁshezhi_1kuai_XMLǃn1ǃIMAGE
		getChild(name: 'n2'): buttonsǁchoumaǁshezhi_1kuai_XMLǃn2ǃIMAGE
		getChildAt(index: 1): buttonsǁchoumaǁshezhi_1kuai_XMLǃn2ǃIMAGE
		getChildById(id: 'n2'): buttonsǁchoumaǁshezhi_1kuai_XMLǃn2ǃIMAGE
		_children: [
			buttonsǁchoumaǁshezhi_1kuai_XMLǃn1ǃIMAGE,
			buttonsǁchoumaǁshezhi_1kuai_XMLǃn2ǃIMAGE
		]
		getController(name: '__language'): buttonsǁchoumaǁshezhi_1kuai_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁchoumaǁshezhi_1kuai_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁchoumaǁshezhi_1kuai_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁchoumaǁshezhi_1kuai_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁchoumaǁshezhi_1kuai_XMLǃ__languageǃCONTROLLER,
			buttonsǁchoumaǁshezhi_1kuai_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁchoumaǁshezhi_1kuai_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁchoumaǁshezhi_1kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_1kuai_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁchoumaǁshezhi_1kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_1kuai_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁchoumaǁshezhi_1kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_1kuai_XMLǃn2ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁchoumaǁshezhi_1kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_1kuai_XMLǃcm_0ǃCOMPONENT extends buttonsǁchoumaǁshezhi_1kuai_XML{
		parent: settingsǁchoumaSettings_XML
	}
	interface buttonsǁchoumaǁshezhi_2kuai_XML extends fairygui.GButton{
		getChild(name: 'n1'): buttonsǁchoumaǁshezhi_2kuai_XMLǃn1ǃIMAGE
		getChildAt(index: 0): buttonsǁchoumaǁshezhi_2kuai_XMLǃn1ǃIMAGE
		getChildById(id: 'n1'): buttonsǁchoumaǁshezhi_2kuai_XMLǃn1ǃIMAGE
		getChild(name: 'n2'): buttonsǁchoumaǁshezhi_2kuai_XMLǃn2ǃIMAGE
		getChildAt(index: 1): buttonsǁchoumaǁshezhi_2kuai_XMLǃn2ǃIMAGE
		getChildById(id: 'n2'): buttonsǁchoumaǁshezhi_2kuai_XMLǃn2ǃIMAGE
		_children: [
			buttonsǁchoumaǁshezhi_2kuai_XMLǃn1ǃIMAGE,
			buttonsǁchoumaǁshezhi_2kuai_XMLǃn2ǃIMAGE
		]
		getController(name: '__language'): buttonsǁchoumaǁshezhi_2kuai_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁchoumaǁshezhi_2kuai_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁchoumaǁshezhi_2kuai_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁchoumaǁshezhi_2kuai_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁchoumaǁshezhi_2kuai_XMLǃ__languageǃCONTROLLER,
			buttonsǁchoumaǁshezhi_2kuai_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁchoumaǁshezhi_2kuai_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁchoumaǁshezhi_2kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_2kuai_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁchoumaǁshezhi_2kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_2kuai_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁchoumaǁshezhi_2kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_2kuai_XMLǃn2ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁchoumaǁshezhi_2kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_2kuai_XMLǃcm_1ǃCOMPONENT extends buttonsǁchoumaǁshezhi_2kuai_XML{
		parent: settingsǁchoumaSettings_XML
	}
	interface buttonsǁchoumaǁshezhi_5kuai_XML extends fairygui.GButton{
		getChild(name: 'n1'): buttonsǁchoumaǁshezhi_5kuai_XMLǃn1ǃIMAGE
		getChildAt(index: 0): buttonsǁchoumaǁshezhi_5kuai_XMLǃn1ǃIMAGE
		getChildById(id: 'n1'): buttonsǁchoumaǁshezhi_5kuai_XMLǃn1ǃIMAGE
		getChild(name: 'n2'): buttonsǁchoumaǁshezhi_5kuai_XMLǃn2ǃIMAGE
		getChildAt(index: 1): buttonsǁchoumaǁshezhi_5kuai_XMLǃn2ǃIMAGE
		getChildById(id: 'n2'): buttonsǁchoumaǁshezhi_5kuai_XMLǃn2ǃIMAGE
		_children: [
			buttonsǁchoumaǁshezhi_5kuai_XMLǃn1ǃIMAGE,
			buttonsǁchoumaǁshezhi_5kuai_XMLǃn2ǃIMAGE
		]
		getController(name: '__language'): buttonsǁchoumaǁshezhi_5kuai_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁchoumaǁshezhi_5kuai_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁchoumaǁshezhi_5kuai_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁchoumaǁshezhi_5kuai_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁchoumaǁshezhi_5kuai_XMLǃ__languageǃCONTROLLER,
			buttonsǁchoumaǁshezhi_5kuai_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁchoumaǁshezhi_5kuai_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁchoumaǁshezhi_5kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_5kuai_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁchoumaǁshezhi_5kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_5kuai_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁchoumaǁshezhi_5kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_5kuai_XMLǃn2ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁchoumaǁshezhi_5kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_5kuai_XMLǃcm_2ǃCOMPONENT extends buttonsǁchoumaǁshezhi_5kuai_XML{
		parent: settingsǁchoumaSettings_XML
	}
	interface buttonsǁchoumaǁshezhi_10kuai_XML extends fairygui.GButton{
		getChild(name: 'n1'): buttonsǁchoumaǁshezhi_10kuai_XMLǃn1ǃIMAGE
		getChildAt(index: 0): buttonsǁchoumaǁshezhi_10kuai_XMLǃn1ǃIMAGE
		getChildById(id: 'n1'): buttonsǁchoumaǁshezhi_10kuai_XMLǃn1ǃIMAGE
		getChild(name: 'n2'): buttonsǁchoumaǁshezhi_10kuai_XMLǃn2ǃIMAGE
		getChildAt(index: 1): buttonsǁchoumaǁshezhi_10kuai_XMLǃn2ǃIMAGE
		getChildById(id: 'n2'): buttonsǁchoumaǁshezhi_10kuai_XMLǃn2ǃIMAGE
		_children: [
			buttonsǁchoumaǁshezhi_10kuai_XMLǃn1ǃIMAGE,
			buttonsǁchoumaǁshezhi_10kuai_XMLǃn2ǃIMAGE
		]
		getController(name: '__language'): buttonsǁchoumaǁshezhi_10kuai_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁchoumaǁshezhi_10kuai_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁchoumaǁshezhi_10kuai_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁchoumaǁshezhi_10kuai_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁchoumaǁshezhi_10kuai_XMLǃ__languageǃCONTROLLER,
			buttonsǁchoumaǁshezhi_10kuai_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁchoumaǁshezhi_10kuai_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁchoumaǁshezhi_10kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_10kuai_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁchoumaǁshezhi_10kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_10kuai_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁchoumaǁshezhi_10kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_10kuai_XMLǃn2ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁchoumaǁshezhi_10kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_10kuai_XMLǃcm_3ǃCOMPONENT extends buttonsǁchoumaǁshezhi_10kuai_XML{
		parent: settingsǁchoumaSettings_XML
	}
	interface buttonsǁchoumaǁshezhi_20kuai_XML extends fairygui.GButton{
		getChild(name: 'n1'): buttonsǁchoumaǁshezhi_20kuai_XMLǃn1ǃIMAGE
		getChildAt(index: 0): buttonsǁchoumaǁshezhi_20kuai_XMLǃn1ǃIMAGE
		getChildById(id: 'n1'): buttonsǁchoumaǁshezhi_20kuai_XMLǃn1ǃIMAGE
		getChild(name: 'n2'): buttonsǁchoumaǁshezhi_20kuai_XMLǃn2ǃIMAGE
		getChildAt(index: 1): buttonsǁchoumaǁshezhi_20kuai_XMLǃn2ǃIMAGE
		getChildById(id: 'n2'): buttonsǁchoumaǁshezhi_20kuai_XMLǃn2ǃIMAGE
		_children: [
			buttonsǁchoumaǁshezhi_20kuai_XMLǃn1ǃIMAGE,
			buttonsǁchoumaǁshezhi_20kuai_XMLǃn2ǃIMAGE
		]
		getController(name: '__language'): buttonsǁchoumaǁshezhi_20kuai_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁchoumaǁshezhi_20kuai_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁchoumaǁshezhi_20kuai_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁchoumaǁshezhi_20kuai_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁchoumaǁshezhi_20kuai_XMLǃ__languageǃCONTROLLER,
			buttonsǁchoumaǁshezhi_20kuai_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁchoumaǁshezhi_20kuai_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁchoumaǁshezhi_20kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_20kuai_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁchoumaǁshezhi_20kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_20kuai_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁchoumaǁshezhi_20kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_20kuai_XMLǃn2ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁchoumaǁshezhi_20kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_20kuai_XMLǃcm_4ǃCOMPONENT extends buttonsǁchoumaǁshezhi_20kuai_XML{
		parent: settingsǁchoumaSettings_XML
	}
	interface buttonsǁchoumaǁshezhi_50kuai_XML extends fairygui.GButton{
		getChild(name: 'n1'): buttonsǁchoumaǁshezhi_50kuai_XMLǃn1ǃIMAGE
		getChildAt(index: 0): buttonsǁchoumaǁshezhi_50kuai_XMLǃn1ǃIMAGE
		getChildById(id: 'n1'): buttonsǁchoumaǁshezhi_50kuai_XMLǃn1ǃIMAGE
		getChild(name: 'n2'): buttonsǁchoumaǁshezhi_50kuai_XMLǃn2ǃIMAGE
		getChildAt(index: 1): buttonsǁchoumaǁshezhi_50kuai_XMLǃn2ǃIMAGE
		getChildById(id: 'n2'): buttonsǁchoumaǁshezhi_50kuai_XMLǃn2ǃIMAGE
		_children: [
			buttonsǁchoumaǁshezhi_50kuai_XMLǃn1ǃIMAGE,
			buttonsǁchoumaǁshezhi_50kuai_XMLǃn2ǃIMAGE
		]
		getController(name: '__language'): buttonsǁchoumaǁshezhi_50kuai_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁchoumaǁshezhi_50kuai_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁchoumaǁshezhi_50kuai_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁchoumaǁshezhi_50kuai_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁchoumaǁshezhi_50kuai_XMLǃ__languageǃCONTROLLER,
			buttonsǁchoumaǁshezhi_50kuai_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁchoumaǁshezhi_50kuai_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁchoumaǁshezhi_50kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_50kuai_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁchoumaǁshezhi_50kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_50kuai_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁchoumaǁshezhi_50kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_50kuai_XMLǃn2ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁchoumaǁshezhi_50kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_50kuai_XMLǃcm_5ǃCOMPONENT extends buttonsǁchoumaǁshezhi_50kuai_XML{
		parent: settingsǁchoumaSettings_XML
	}
	interface buttonsǁchoumaǁshezhi_100kuai_XML extends fairygui.GButton{
		getChild(name: 'n1'): buttonsǁchoumaǁshezhi_100kuai_XMLǃn1ǃIMAGE
		getChildAt(index: 0): buttonsǁchoumaǁshezhi_100kuai_XMLǃn1ǃIMAGE
		getChildById(id: 'n1'): buttonsǁchoumaǁshezhi_100kuai_XMLǃn1ǃIMAGE
		getChild(name: 'n2'): buttonsǁchoumaǁshezhi_100kuai_XMLǃn2ǃIMAGE
		getChildAt(index: 1): buttonsǁchoumaǁshezhi_100kuai_XMLǃn2ǃIMAGE
		getChildById(id: 'n2'): buttonsǁchoumaǁshezhi_100kuai_XMLǃn2ǃIMAGE
		_children: [
			buttonsǁchoumaǁshezhi_100kuai_XMLǃn1ǃIMAGE,
			buttonsǁchoumaǁshezhi_100kuai_XMLǃn2ǃIMAGE
		]
		getController(name: '__language'): buttonsǁchoumaǁshezhi_100kuai_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁchoumaǁshezhi_100kuai_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁchoumaǁshezhi_100kuai_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁchoumaǁshezhi_100kuai_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁchoumaǁshezhi_100kuai_XMLǃ__languageǃCONTROLLER,
			buttonsǁchoumaǁshezhi_100kuai_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁchoumaǁshezhi_100kuai_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁchoumaǁshezhi_100kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_100kuai_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁchoumaǁshezhi_100kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_100kuai_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁchoumaǁshezhi_100kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_100kuai_XMLǃn2ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁchoumaǁshezhi_100kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_100kuai_XMLǃcm_6ǃCOMPONENT extends buttonsǁchoumaǁshezhi_100kuai_XML{
		parent: settingsǁchoumaSettings_XML
	}
	interface buttonsǁchoumaǁshezhi_200kuai_XML extends fairygui.GButton{
		getChild(name: 'n1'): buttonsǁchoumaǁshezhi_200kuai_XMLǃn1ǃIMAGE
		getChildAt(index: 0): buttonsǁchoumaǁshezhi_200kuai_XMLǃn1ǃIMAGE
		getChildById(id: 'n1'): buttonsǁchoumaǁshezhi_200kuai_XMLǃn1ǃIMAGE
		getChild(name: 'n2'): buttonsǁchoumaǁshezhi_200kuai_XMLǃn2ǃIMAGE
		getChildAt(index: 1): buttonsǁchoumaǁshezhi_200kuai_XMLǃn2ǃIMAGE
		getChildById(id: 'n2'): buttonsǁchoumaǁshezhi_200kuai_XMLǃn2ǃIMAGE
		_children: [
			buttonsǁchoumaǁshezhi_200kuai_XMLǃn1ǃIMAGE,
			buttonsǁchoumaǁshezhi_200kuai_XMLǃn2ǃIMAGE
		]
		getController(name: '__language'): buttonsǁchoumaǁshezhi_200kuai_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁchoumaǁshezhi_200kuai_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁchoumaǁshezhi_200kuai_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁchoumaǁshezhi_200kuai_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁchoumaǁshezhi_200kuai_XMLǃ__languageǃCONTROLLER,
			buttonsǁchoumaǁshezhi_200kuai_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁchoumaǁshezhi_200kuai_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁchoumaǁshezhi_200kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_200kuai_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁchoumaǁshezhi_200kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_200kuai_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁchoumaǁshezhi_200kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_200kuai_XMLǃn2ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁchoumaǁshezhi_200kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_200kuai_XMLǃcm_7ǃCOMPONENT extends buttonsǁchoumaǁshezhi_200kuai_XML{
		parent: settingsǁchoumaSettings_XML
	}
	interface buttonsǁchoumaǁshezhi_500kuai_XML extends fairygui.GButton{
		getChild(name: 'n1'): buttonsǁchoumaǁshezhi_500kuai_XMLǃn1ǃIMAGE
		getChildAt(index: 0): buttonsǁchoumaǁshezhi_500kuai_XMLǃn1ǃIMAGE
		getChildById(id: 'n1'): buttonsǁchoumaǁshezhi_500kuai_XMLǃn1ǃIMAGE
		getChild(name: 'n2'): buttonsǁchoumaǁshezhi_500kuai_XMLǃn2ǃIMAGE
		getChildAt(index: 1): buttonsǁchoumaǁshezhi_500kuai_XMLǃn2ǃIMAGE
		getChildById(id: 'n2'): buttonsǁchoumaǁshezhi_500kuai_XMLǃn2ǃIMAGE
		_children: [
			buttonsǁchoumaǁshezhi_500kuai_XMLǃn1ǃIMAGE,
			buttonsǁchoumaǁshezhi_500kuai_XMLǃn2ǃIMAGE
		]
		getController(name: '__language'): buttonsǁchoumaǁshezhi_500kuai_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁchoumaǁshezhi_500kuai_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁchoumaǁshezhi_500kuai_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁchoumaǁshezhi_500kuai_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁchoumaǁshezhi_500kuai_XMLǃ__languageǃCONTROLLER,
			buttonsǁchoumaǁshezhi_500kuai_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁchoumaǁshezhi_500kuai_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁchoumaǁshezhi_500kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_500kuai_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁchoumaǁshezhi_500kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_500kuai_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁchoumaǁshezhi_500kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_500kuai_XMLǃn2ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁchoumaǁshezhi_500kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_500kuai_XMLǃcm_8ǃCOMPONENT extends buttonsǁchoumaǁshezhi_500kuai_XML{
		parent: settingsǁchoumaSettings_XML
	}
	interface buttonsǁchoumaǁshezhi_1k_XML extends fairygui.GButton{
		getChild(name: 'n4'): buttonsǁchoumaǁshezhi_1k_XMLǃn4ǃIMAGE
		getChildAt(index: 0): buttonsǁchoumaǁshezhi_1k_XMLǃn4ǃIMAGE
		getChildById(id: 'n4_r22b'): buttonsǁchoumaǁshezhi_1k_XMLǃn4ǃIMAGE
		getChild(name: 'n5'): buttonsǁchoumaǁshezhi_1k_XMLǃn5ǃIMAGE
		getChildAt(index: 1): buttonsǁchoumaǁshezhi_1k_XMLǃn5ǃIMAGE
		getChildById(id: 'n5_r22b'): buttonsǁchoumaǁshezhi_1k_XMLǃn5ǃIMAGE
		_children: [
			buttonsǁchoumaǁshezhi_1k_XMLǃn4ǃIMAGE,
			buttonsǁchoumaǁshezhi_1k_XMLǃn5ǃIMAGE
		]
		getController(name: '__language'): buttonsǁchoumaǁshezhi_1k_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁchoumaǁshezhi_1k_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁchoumaǁshezhi_1k_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁchoumaǁshezhi_1k_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁchoumaǁshezhi_1k_XMLǃ__languageǃCONTROLLER,
			buttonsǁchoumaǁshezhi_1k_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁchoumaǁshezhi_1k_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁchoumaǁshezhi_1k_XML
	}
	interface buttonsǁchoumaǁshezhi_1k_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁchoumaǁshezhi_1k_XML
	}
	interface buttonsǁchoumaǁshezhi_1k_XMLǃn4ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁchoumaǁshezhi_1k_XML
	}
	interface buttonsǁchoumaǁshezhi_1k_XMLǃn5ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁchoumaǁshezhi_1k_XML
	}
	interface buttonsǁchoumaǁshezhi_1k_XMLǃcm_9ǃCOMPONENT extends buttonsǁchoumaǁshezhi_1k_XML{
		parent: settingsǁchoumaSettings_XML
	}
	interface buttonsǁchoumaǁshezhi_2000kuai_XML extends fairygui.GButton{
		getChild(name: 'n1'): buttonsǁchoumaǁshezhi_2000kuai_XMLǃn1ǃIMAGE
		getChildAt(index: 0): buttonsǁchoumaǁshezhi_2000kuai_XMLǃn1ǃIMAGE
		getChildById(id: 'n1'): buttonsǁchoumaǁshezhi_2000kuai_XMLǃn1ǃIMAGE
		getChild(name: 'n2'): buttonsǁchoumaǁshezhi_2000kuai_XMLǃn2ǃIMAGE
		getChildAt(index: 1): buttonsǁchoumaǁshezhi_2000kuai_XMLǃn2ǃIMAGE
		getChildById(id: 'n2'): buttonsǁchoumaǁshezhi_2000kuai_XMLǃn2ǃIMAGE
		_children: [
			buttonsǁchoumaǁshezhi_2000kuai_XMLǃn1ǃIMAGE,
			buttonsǁchoumaǁshezhi_2000kuai_XMLǃn2ǃIMAGE
		]
		getController(name: '__language'): buttonsǁchoumaǁshezhi_2000kuai_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁchoumaǁshezhi_2000kuai_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁchoumaǁshezhi_2000kuai_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁchoumaǁshezhi_2000kuai_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁchoumaǁshezhi_2000kuai_XMLǃ__languageǃCONTROLLER,
			buttonsǁchoumaǁshezhi_2000kuai_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁchoumaǁshezhi_2000kuai_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁchoumaǁshezhi_2000kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_2000kuai_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁchoumaǁshezhi_2000kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_2000kuai_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁchoumaǁshezhi_2000kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_2000kuai_XMLǃn2ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁchoumaǁshezhi_2000kuai_XML
	}
	interface buttonsǁchoumaǁshezhi_2000kuai_XMLǃcm_10ǃCOMPONENT extends buttonsǁchoumaǁshezhi_2000kuai_XML{
		parent: settingsǁchoumaSettings_XML
	}
	interface buttonsǁchoumaǁshezhi_10k_XML extends fairygui.GButton{
		getChild(name: 'n4'): buttonsǁchoumaǁshezhi_10k_XMLǃn4ǃIMAGE
		getChildAt(index: 0): buttonsǁchoumaǁshezhi_10k_XMLǃn4ǃIMAGE
		getChildById(id: 'n4_r22b'): buttonsǁchoumaǁshezhi_10k_XMLǃn4ǃIMAGE
		getChild(name: 'n5'): buttonsǁchoumaǁshezhi_10k_XMLǃn5ǃIMAGE
		getChildAt(index: 1): buttonsǁchoumaǁshezhi_10k_XMLǃn5ǃIMAGE
		getChildById(id: 'n5_r22b'): buttonsǁchoumaǁshezhi_10k_XMLǃn5ǃIMAGE
		_children: [
			buttonsǁchoumaǁshezhi_10k_XMLǃn4ǃIMAGE,
			buttonsǁchoumaǁshezhi_10k_XMLǃn5ǃIMAGE
		]
		getController(name: '__language'): buttonsǁchoumaǁshezhi_10k_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁchoumaǁshezhi_10k_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁchoumaǁshezhi_10k_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁchoumaǁshezhi_10k_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁchoumaǁshezhi_10k_XMLǃ__languageǃCONTROLLER,
			buttonsǁchoumaǁshezhi_10k_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁchoumaǁshezhi_10k_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁchoumaǁshezhi_10k_XML
	}
	interface buttonsǁchoumaǁshezhi_10k_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁchoumaǁshezhi_10k_XML
	}
	interface buttonsǁchoumaǁshezhi_10k_XMLǃn4ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁchoumaǁshezhi_10k_XML
	}
	interface buttonsǁchoumaǁshezhi_10k_XMLǃn5ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁchoumaǁshezhi_10k_XML
	}
	interface buttonsǁchoumaǁshezhi_10k_XMLǃcm_11ǃCOMPONENT extends buttonsǁchoumaǁshezhi_10k_XML{
		parent: settingsǁchoumaSettings_XML
	}
	interface settingsǁchoumaSettings_XMLǃchoumaǃCOMPONENT extends settingsǁchoumaSettings_XML{
		parent: settings_XML
	}
	interface buttonsǁbutton_queren_XML extends fairygui.GButton{
		getChild(name: 'n2'): buttonsǁbutton_queren_XMLǃn2ǃIMAGE
		getChildAt(index: 0): buttonsǁbutton_queren_XMLǃn2ǃIMAGE
		getChildById(id: 'n2_o9yi'): buttonsǁbutton_queren_XMLǃn2ǃIMAGE
		getChild(name: 'n3_CN2'): buttonsǁbutton_queren_XMLǃn3_CN2ǃTEXT
		getChildAt(index: 1): buttonsǁbutton_queren_XMLǃn3_CN2ǃTEXT
		getChildById(id: 'n3_o9yi_CN2'): buttonsǁbutton_queren_XMLǃn3_CN2ǃTEXT
		getChild(name: 'n3_EN'): buttonsǁbutton_queren_XMLǃn3_ENǃTEXT
		getChildAt(index: 2): buttonsǁbutton_queren_XMLǃn3_ENǃTEXT
		getChildById(id: 'n3_o9yi_EN'): buttonsǁbutton_queren_XMLǃn3_ENǃTEXT
		getChild(name: 'n3_IN'): buttonsǁbutton_queren_XMLǃn3_INǃTEXT
		getChildAt(index: 3): buttonsǁbutton_queren_XMLǃn3_INǃTEXT
		getChildById(id: 'n3_o9yi_IN'): buttonsǁbutton_queren_XMLǃn3_INǃTEXT
		getChild(name: 'n3_JP'): buttonsǁbutton_queren_XMLǃn3_JPǃTEXT
		getChildAt(index: 4): buttonsǁbutton_queren_XMLǃn3_JPǃTEXT
		getChildById(id: 'n3_o9yi_JP'): buttonsǁbutton_queren_XMLǃn3_JPǃTEXT
		getChild(name: 'n3_KR'): buttonsǁbutton_queren_XMLǃn3_KRǃTEXT
		getChildAt(index: 5): buttonsǁbutton_queren_XMLǃn3_KRǃTEXT
		getChildById(id: 'n3_o9yi_KR'): buttonsǁbutton_queren_XMLǃn3_KRǃTEXT
		getChild(name: 'n3_TH'): buttonsǁbutton_queren_XMLǃn3_THǃTEXT
		getChildAt(index: 6): buttonsǁbutton_queren_XMLǃn3_THǃTEXT
		getChildById(id: 'n3_o9yi_TH'): buttonsǁbutton_queren_XMLǃn3_THǃTEXT
		getChild(name: 'n3_VN'): buttonsǁbutton_queren_XMLǃn3_VNǃTEXT
		getChildAt(index: 7): buttonsǁbutton_queren_XMLǃn3_VNǃTEXT
		getChildById(id: 'n3_o9yi_VN'): buttonsǁbutton_queren_XMLǃn3_VNǃTEXT
		getChild(name: 'n3'): buttonsǁbutton_queren_XMLǃn3ǃTEXT
		getChildAt(index: 8): buttonsǁbutton_queren_XMLǃn3ǃTEXT
		getChildById(id: 'n3_o9yi'): buttonsǁbutton_queren_XMLǃn3ǃTEXT
		_children: [
			buttonsǁbutton_queren_XMLǃn2ǃIMAGE,
			buttonsǁbutton_queren_XMLǃn3_CN2ǃTEXT,
			buttonsǁbutton_queren_XMLǃn3_ENǃTEXT,
			buttonsǁbutton_queren_XMLǃn3_INǃTEXT,
			buttonsǁbutton_queren_XMLǃn3_JPǃTEXT,
			buttonsǁbutton_queren_XMLǃn3_KRǃTEXT,
			buttonsǁbutton_queren_XMLǃn3_THǃTEXT,
			buttonsǁbutton_queren_XMLǃn3_VNǃTEXT,
			buttonsǁbutton_queren_XMLǃn3ǃTEXT
		]
		getController(name: '__language'): buttonsǁbutton_queren_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁbutton_queren_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁbutton_queren_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁbutton_queren_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁbutton_queren_XMLǃ__languageǃCONTROLLER,
			buttonsǁbutton_queren_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁbutton_queren_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁbutton_queren_XML
	}
	interface buttonsǁbutton_queren_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁbutton_queren_XML
	}
	interface buttonsǁbutton_queren_XMLǃn2ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁbutton_queren_XML
	}
	interface buttonsǁbutton_queren_XMLǃn3_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁbutton_queren_XML
	}
	interface buttonsǁbutton_queren_XMLǃn3_ENǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁbutton_queren_XML
	}
	interface buttonsǁbutton_queren_XMLǃn3_INǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁbutton_queren_XML
	}
	interface buttonsǁbutton_queren_XMLǃn3_JPǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁbutton_queren_XML
	}
	interface buttonsǁbutton_queren_XMLǃn3_KRǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁbutton_queren_XML
	}
	interface buttonsǁbutton_queren_XMLǃn3_THǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁbutton_queren_XML
	}
	interface buttonsǁbutton_queren_XMLǃn3_VNǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁbutton_queren_XML
	}
	interface buttonsǁbutton_queren_XMLǃn3ǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁbutton_queren_XML
	}
	interface buttonsǁbutton_queren_XMLǃbtn_confirmǃCOMPONENT extends buttonsǁbutton_queren_XML{
		parent: settings_XML
	}
	interface buttonsǁbutton_quxiao_XML extends fairygui.GButton{
		getChild(name: 'n2'): buttonsǁbutton_quxiao_XMLǃn2ǃIMAGE
		getChildAt(index: 0): buttonsǁbutton_quxiao_XMLǃn2ǃIMAGE
		getChildById(id: 'n2_o9yi'): buttonsǁbutton_quxiao_XMLǃn2ǃIMAGE
		getChild(name: 'n3_CN2'): buttonsǁbutton_quxiao_XMLǃn3_CN2ǃTEXT
		getChildAt(index: 1): buttonsǁbutton_quxiao_XMLǃn3_CN2ǃTEXT
		getChildById(id: 'n3_o9yi_CN2'): buttonsǁbutton_quxiao_XMLǃn3_CN2ǃTEXT
		getChild(name: 'n3_EN'): buttonsǁbutton_quxiao_XMLǃn3_ENǃTEXT
		getChildAt(index: 2): buttonsǁbutton_quxiao_XMLǃn3_ENǃTEXT
		getChildById(id: 'n3_o9yi_EN'): buttonsǁbutton_quxiao_XMLǃn3_ENǃTEXT
		getChild(name: 'n3_IN'): buttonsǁbutton_quxiao_XMLǃn3_INǃTEXT
		getChildAt(index: 3): buttonsǁbutton_quxiao_XMLǃn3_INǃTEXT
		getChildById(id: 'n3_o9yi_IN'): buttonsǁbutton_quxiao_XMLǃn3_INǃTEXT
		getChild(name: 'n3_JP'): buttonsǁbutton_quxiao_XMLǃn3_JPǃTEXT
		getChildAt(index: 4): buttonsǁbutton_quxiao_XMLǃn3_JPǃTEXT
		getChildById(id: 'n3_o9yi_JP'): buttonsǁbutton_quxiao_XMLǃn3_JPǃTEXT
		getChild(name: 'n3_KR'): buttonsǁbutton_quxiao_XMLǃn3_KRǃTEXT
		getChildAt(index: 5): buttonsǁbutton_quxiao_XMLǃn3_KRǃTEXT
		getChildById(id: 'n3_o9yi_KR'): buttonsǁbutton_quxiao_XMLǃn3_KRǃTEXT
		getChild(name: 'n3_TH'): buttonsǁbutton_quxiao_XMLǃn3_THǃTEXT
		getChildAt(index: 6): buttonsǁbutton_quxiao_XMLǃn3_THǃTEXT
		getChildById(id: 'n3_o9yi_TH'): buttonsǁbutton_quxiao_XMLǃn3_THǃTEXT
		getChild(name: 'n3_VN'): buttonsǁbutton_quxiao_XMLǃn3_VNǃTEXT
		getChildAt(index: 7): buttonsǁbutton_quxiao_XMLǃn3_VNǃTEXT
		getChildById(id: 'n3_o9yi_VN'): buttonsǁbutton_quxiao_XMLǃn3_VNǃTEXT
		getChild(name: 'n3'): buttonsǁbutton_quxiao_XMLǃn3ǃTEXT
		getChildAt(index: 8): buttonsǁbutton_quxiao_XMLǃn3ǃTEXT
		getChildById(id: 'n3_o9yi'): buttonsǁbutton_quxiao_XMLǃn3ǃTEXT
		_children: [
			buttonsǁbutton_quxiao_XMLǃn2ǃIMAGE,
			buttonsǁbutton_quxiao_XMLǃn3_CN2ǃTEXT,
			buttonsǁbutton_quxiao_XMLǃn3_ENǃTEXT,
			buttonsǁbutton_quxiao_XMLǃn3_INǃTEXT,
			buttonsǁbutton_quxiao_XMLǃn3_JPǃTEXT,
			buttonsǁbutton_quxiao_XMLǃn3_KRǃTEXT,
			buttonsǁbutton_quxiao_XMLǃn3_THǃTEXT,
			buttonsǁbutton_quxiao_XMLǃn3_VNǃTEXT,
			buttonsǁbutton_quxiao_XMLǃn3ǃTEXT
		]
		getController(name: '__language'): buttonsǁbutton_quxiao_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁbutton_quxiao_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁbutton_quxiao_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁbutton_quxiao_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁbutton_quxiao_XMLǃ__languageǃCONTROLLER,
			buttonsǁbutton_quxiao_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁbutton_quxiao_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁbutton_quxiao_XML
	}
	interface buttonsǁbutton_quxiao_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁbutton_quxiao_XML
	}
	interface buttonsǁbutton_quxiao_XMLǃn2ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁbutton_quxiao_XML
	}
	interface buttonsǁbutton_quxiao_XMLǃn3_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁbutton_quxiao_XML
	}
	interface buttonsǁbutton_quxiao_XMLǃn3_ENǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁbutton_quxiao_XML
	}
	interface buttonsǁbutton_quxiao_XMLǃn3_INǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁbutton_quxiao_XML
	}
	interface buttonsǁbutton_quxiao_XMLǃn3_JPǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁbutton_quxiao_XML
	}
	interface buttonsǁbutton_quxiao_XMLǃn3_KRǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁbutton_quxiao_XML
	}
	interface buttonsǁbutton_quxiao_XMLǃn3_THǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁbutton_quxiao_XML
	}
	interface buttonsǁbutton_quxiao_XMLǃn3_VNǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁbutton_quxiao_XML
	}
	interface buttonsǁbutton_quxiao_XMLǃn3ǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁbutton_quxiao_XML
	}
	interface buttonsǁbutton_quxiao_XMLǃbtn_cancelǃCOMPONENT extends buttonsǁbutton_quxiao_XML{
		parent: settings_XML
	}
	interface settings_XMLǃn27_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: settings_XML
	}
	interface settings_XMLǃn27_ENǃTEXT extends fairygui.GBasicTextField{
		parent: settings_XML
	}
	interface settings_XMLǃn27_INǃTEXT extends fairygui.GBasicTextField{
		parent: settings_XML
	}
	interface settings_XMLǃn27_JPǃTEXT extends fairygui.GBasicTextField{
		parent: settings_XML
	}
	interface settings_XMLǃn27_KRǃTEXT extends fairygui.GBasicTextField{
		parent: settings_XML
	}
	interface settings_XMLǃn27_THǃTEXT extends fairygui.GBasicTextField{
		parent: settings_XML
	}
	interface settings_XMLǃn27_VNǃTEXT extends fairygui.GBasicTextField{
		parent: settings_XML
	}
	interface settings_XMLǃn27ǃTEXT extends fairygui.GBasicTextField{
		parent: settings_XML
	}
	interface languageǁselection_panel_XML extends fairygui.GComponent{
		getChild(name: 'language_list'): languageǁselection_panel_XMLǃlanguage_listǃLIST
		getChildAt(index: 0): languageǁselection_panel_XMLǃlanguage_listǃLIST
		getChildById(id: 'n3_ggwy'): languageǁselection_panel_XMLǃlanguage_listǃLIST
		_children: [
			languageǁselection_panel_XMLǃlanguage_listǃLIST
		]
		getController(name: 'language_ctrl'): languageǁselection_panel_XMLǃlanguage_ctrlǃCONTROLLER
		getControllerAt(index: 0): languageǁselection_panel_XMLǃlanguage_ctrlǃCONTROLLER
		_controllers: [
			languageǁselection_panel_XMLǃlanguage_ctrlǃCONTROLLER
		]
	}
	interface languageǁselection_panel_XMLǃlanguage_ctrlǃCONTROLLER extends fairygui.Controller{
		_parent: languageǁselection_panel_XML
	}
	interface languageǁselection_panel_XMLǃlanguage_listǃLIST extends fairygui.GList{
		_children: [
			languageǁchinese_simplified_XMLǃChineseǃITEM,
			languageǁchinese_traditional_XMLǃTraditionalChineseǃITEM,
			languageǁenglish_XMLǃEnglishǃITEM,
			languageǁindonesian_XMLǃIndonesianǃITEM,
			languageǁjapanese_XMLǃJapaneseǃITEM,
			languageǁkorean_XMLǃKoreanǃITEM,
			languageǁthai_XMLǃThaiǃITEM,
			languageǁvietnamese_XMLǃVietnameseǃITEM
		]
		parent: languageǁselection_panel_XML
	}
	interface languageǁchinese_simplified_XML extends fairygui.GButton{
		getChild(name: 'background'): languageǁchinese_simplified_XMLǃbackgroundǃGRAPH
		getChildAt(index: 0): languageǁchinese_simplified_XMLǃbackgroundǃGRAPH
		getChildById(id: 'n5_ggwy'): languageǁchinese_simplified_XMLǃbackgroundǃGRAPH
		getChild(name: 'selected_background'): languageǁchinese_simplified_XMLǃselected_backgroundǃGRAPH
		getChildAt(index: 1): languageǁchinese_simplified_XMLǃselected_backgroundǃGRAPH
		getChildById(id: 'n6_11208'): languageǁchinese_simplified_XMLǃselected_backgroundǃGRAPH
		getChild(name: 'icon'): languageǁchinese_simplified_XMLǃiconǃIMAGE
		getChildAt(index: 2): languageǁchinese_simplified_XMLǃiconǃIMAGE
		getChildById(id: 'n3_t9vb'): languageǁchinese_simplified_XMLǃiconǃIMAGE
		getChild(name: 'text'): languageǁchinese_simplified_XMLǃtextǃTEXT
		getChildAt(index: 3): languageǁchinese_simplified_XMLǃtextǃTEXT
		getChildById(id: 'n4_t9vb'): languageǁchinese_simplified_XMLǃtextǃTEXT
		_children: [
			languageǁchinese_simplified_XMLǃbackgroundǃGRAPH,
			languageǁchinese_simplified_XMLǃselected_backgroundǃGRAPH,
			languageǁchinese_simplified_XMLǃiconǃIMAGE,
			languageǁchinese_simplified_XMLǃtextǃTEXT
		]
		getController(name: 'language_ctrl'): languageǁchinese_simplified_XMLǃlanguage_ctrlǃCONTROLLER
		getControllerAt(index: 0): languageǁchinese_simplified_XMLǃlanguage_ctrlǃCONTROLLER
		getController(name: 'button'): languageǁchinese_simplified_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): languageǁchinese_simplified_XMLǃbuttonǃCONTROLLER
		_controllers: [
			languageǁchinese_simplified_XMLǃlanguage_ctrlǃCONTROLLER,
			languageǁchinese_simplified_XMLǃbuttonǃCONTROLLER
		]
	}
	interface languageǁchinese_simplified_XMLǃlanguage_ctrlǃCONTROLLER extends fairygui.Controller{
		_parent: languageǁchinese_simplified_XML
	}
	interface languageǁchinese_simplified_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: languageǁchinese_simplified_XML
	}
	interface languageǁchinese_simplified_XMLǃbackgroundǃGRAPH extends fairygui.GGraph{
		parent: languageǁchinese_simplified_XML
	}
	interface languageǁchinese_simplified_XMLǃselected_backgroundǃGRAPH extends fairygui.GGraph{
		parent: languageǁchinese_simplified_XML
	}
	interface languageǁchinese_simplified_XMLǃiconǃIMAGE extends fairygui.GImage{
		parent: languageǁchinese_simplified_XML
	}
	interface languageǁchinese_simplified_XMLǃtextǃTEXT extends fairygui.GBasicTextField{
		parent: languageǁchinese_simplified_XML
	}
	interface languageǁchinese_simplified_XMLǃChineseǃITEM extends languageǁchinese_simplified_XML{
		parent: languageǁselection_panel_XMLǃlanguage_listǃLIST
	}
	interface languageǁchinese_traditional_XML extends fairygui.GButton{
		getChild(name: 'background'): languageǁchinese_traditional_XMLǃbackgroundǃGRAPH
		getChildAt(index: 0): languageǁchinese_traditional_XMLǃbackgroundǃGRAPH
		getChildById(id: 'n5_ggwy'): languageǁchinese_traditional_XMLǃbackgroundǃGRAPH
		getChild(name: 'selected_background'): languageǁchinese_traditional_XMLǃselected_backgroundǃGRAPH
		getChildAt(index: 1): languageǁchinese_traditional_XMLǃselected_backgroundǃGRAPH
		getChildById(id: 'n6_11208'): languageǁchinese_traditional_XMLǃselected_backgroundǃGRAPH
		getChild(name: 'icon'): languageǁchinese_traditional_XMLǃiconǃIMAGE
		getChildAt(index: 2): languageǁchinese_traditional_XMLǃiconǃIMAGE
		getChildById(id: 'n3_t9vb'): languageǁchinese_traditional_XMLǃiconǃIMAGE
		getChild(name: 'text'): languageǁchinese_traditional_XMLǃtextǃTEXT
		getChildAt(index: 3): languageǁchinese_traditional_XMLǃtextǃTEXT
		getChildById(id: 'n4_t9vb'): languageǁchinese_traditional_XMLǃtextǃTEXT
		_children: [
			languageǁchinese_traditional_XMLǃbackgroundǃGRAPH,
			languageǁchinese_traditional_XMLǃselected_backgroundǃGRAPH,
			languageǁchinese_traditional_XMLǃiconǃIMAGE,
			languageǁchinese_traditional_XMLǃtextǃTEXT
		]
		getController(name: 'language_ctrl'): languageǁchinese_traditional_XMLǃlanguage_ctrlǃCONTROLLER
		getControllerAt(index: 0): languageǁchinese_traditional_XMLǃlanguage_ctrlǃCONTROLLER
		getController(name: 'button'): languageǁchinese_traditional_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): languageǁchinese_traditional_XMLǃbuttonǃCONTROLLER
		_controllers: [
			languageǁchinese_traditional_XMLǃlanguage_ctrlǃCONTROLLER,
			languageǁchinese_traditional_XMLǃbuttonǃCONTROLLER
		]
	}
	interface languageǁchinese_traditional_XMLǃlanguage_ctrlǃCONTROLLER extends fairygui.Controller{
		_parent: languageǁchinese_traditional_XML
	}
	interface languageǁchinese_traditional_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: languageǁchinese_traditional_XML
	}
	interface languageǁchinese_traditional_XMLǃbackgroundǃGRAPH extends fairygui.GGraph{
		parent: languageǁchinese_traditional_XML
	}
	interface languageǁchinese_traditional_XMLǃselected_backgroundǃGRAPH extends fairygui.GGraph{
		parent: languageǁchinese_traditional_XML
	}
	interface languageǁchinese_traditional_XMLǃiconǃIMAGE extends fairygui.GImage{
		parent: languageǁchinese_traditional_XML
	}
	interface languageǁchinese_traditional_XMLǃtextǃTEXT extends fairygui.GBasicTextField{
		parent: languageǁchinese_traditional_XML
	}
	interface languageǁchinese_traditional_XMLǃTraditionalChineseǃITEM extends languageǁchinese_traditional_XML{
		parent: languageǁselection_panel_XMLǃlanguage_listǃLIST
	}
	interface languageǁenglish_XML extends fairygui.GButton{
		getChild(name: 'background'): languageǁenglish_XMLǃbackgroundǃGRAPH
		getChildAt(index: 0): languageǁenglish_XMLǃbackgroundǃGRAPH
		getChildById(id: 'n5_ggwy'): languageǁenglish_XMLǃbackgroundǃGRAPH
		getChild(name: 'selected_background'): languageǁenglish_XMLǃselected_backgroundǃGRAPH
		getChildAt(index: 1): languageǁenglish_XMLǃselected_backgroundǃGRAPH
		getChildById(id: 'n6_11208'): languageǁenglish_XMLǃselected_backgroundǃGRAPH
		getChild(name: 'icon'): languageǁenglish_XMLǃiconǃIMAGE
		getChildAt(index: 2): languageǁenglish_XMLǃiconǃIMAGE
		getChildById(id: 'n3_t9vb'): languageǁenglish_XMLǃiconǃIMAGE
		getChild(name: 'text'): languageǁenglish_XMLǃtextǃTEXT
		getChildAt(index: 3): languageǁenglish_XMLǃtextǃTEXT
		getChildById(id: 'n4_t9vb'): languageǁenglish_XMLǃtextǃTEXT
		_children: [
			languageǁenglish_XMLǃbackgroundǃGRAPH,
			languageǁenglish_XMLǃselected_backgroundǃGRAPH,
			languageǁenglish_XMLǃiconǃIMAGE,
			languageǁenglish_XMLǃtextǃTEXT
		]
		getController(name: 'language_ctrl'): languageǁenglish_XMLǃlanguage_ctrlǃCONTROLLER
		getControllerAt(index: 0): languageǁenglish_XMLǃlanguage_ctrlǃCONTROLLER
		getController(name: 'button'): languageǁenglish_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): languageǁenglish_XMLǃbuttonǃCONTROLLER
		_controllers: [
			languageǁenglish_XMLǃlanguage_ctrlǃCONTROLLER,
			languageǁenglish_XMLǃbuttonǃCONTROLLER
		]
	}
	interface languageǁenglish_XMLǃlanguage_ctrlǃCONTROLLER extends fairygui.Controller{
		_parent: languageǁenglish_XML
	}
	interface languageǁenglish_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: languageǁenglish_XML
	}
	interface languageǁenglish_XMLǃbackgroundǃGRAPH extends fairygui.GGraph{
		parent: languageǁenglish_XML
	}
	interface languageǁenglish_XMLǃselected_backgroundǃGRAPH extends fairygui.GGraph{
		parent: languageǁenglish_XML
	}
	interface languageǁenglish_XMLǃiconǃIMAGE extends fairygui.GImage{
		parent: languageǁenglish_XML
	}
	interface languageǁenglish_XMLǃtextǃTEXT extends fairygui.GBasicTextField{
		parent: languageǁenglish_XML
	}
	interface languageǁenglish_XMLǃEnglishǃITEM extends languageǁenglish_XML{
		parent: languageǁselection_panel_XMLǃlanguage_listǃLIST
	}
	interface languageǁindonesian_XML extends fairygui.GButton{
		getChild(name: 'background'): languageǁindonesian_XMLǃbackgroundǃGRAPH
		getChildAt(index: 0): languageǁindonesian_XMLǃbackgroundǃGRAPH
		getChildById(id: 'n5_ggwy'): languageǁindonesian_XMLǃbackgroundǃGRAPH
		getChild(name: 'selected_background'): languageǁindonesian_XMLǃselected_backgroundǃGRAPH
		getChildAt(index: 1): languageǁindonesian_XMLǃselected_backgroundǃGRAPH
		getChildById(id: 'n6_11208'): languageǁindonesian_XMLǃselected_backgroundǃGRAPH
		getChild(name: 'icon'): languageǁindonesian_XMLǃiconǃIMAGE
		getChildAt(index: 2): languageǁindonesian_XMLǃiconǃIMAGE
		getChildById(id: 'n3_t9vb'): languageǁindonesian_XMLǃiconǃIMAGE
		getChild(name: 'text'): languageǁindonesian_XMLǃtextǃTEXT
		getChildAt(index: 3): languageǁindonesian_XMLǃtextǃTEXT
		getChildById(id: 'n4_t9vb'): languageǁindonesian_XMLǃtextǃTEXT
		_children: [
			languageǁindonesian_XMLǃbackgroundǃGRAPH,
			languageǁindonesian_XMLǃselected_backgroundǃGRAPH,
			languageǁindonesian_XMLǃiconǃIMAGE,
			languageǁindonesian_XMLǃtextǃTEXT
		]
		getController(name: 'language_ctrl'): languageǁindonesian_XMLǃlanguage_ctrlǃCONTROLLER
		getControllerAt(index: 0): languageǁindonesian_XMLǃlanguage_ctrlǃCONTROLLER
		getController(name: 'button'): languageǁindonesian_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): languageǁindonesian_XMLǃbuttonǃCONTROLLER
		_controllers: [
			languageǁindonesian_XMLǃlanguage_ctrlǃCONTROLLER,
			languageǁindonesian_XMLǃbuttonǃCONTROLLER
		]
	}
	interface languageǁindonesian_XMLǃlanguage_ctrlǃCONTROLLER extends fairygui.Controller{
		_parent: languageǁindonesian_XML
	}
	interface languageǁindonesian_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: languageǁindonesian_XML
	}
	interface languageǁindonesian_XMLǃbackgroundǃGRAPH extends fairygui.GGraph{
		parent: languageǁindonesian_XML
	}
	interface languageǁindonesian_XMLǃselected_backgroundǃGRAPH extends fairygui.GGraph{
		parent: languageǁindonesian_XML
	}
	interface languageǁindonesian_XMLǃiconǃIMAGE extends fairygui.GImage{
		parent: languageǁindonesian_XML
	}
	interface languageǁindonesian_XMLǃtextǃTEXT extends fairygui.GBasicTextField{
		parent: languageǁindonesian_XML
	}
	interface languageǁindonesian_XMLǃIndonesianǃITEM extends languageǁindonesian_XML{
		parent: languageǁselection_panel_XMLǃlanguage_listǃLIST
	}
	interface languageǁjapanese_XML extends fairygui.GButton{
		getChild(name: 'background'): languageǁjapanese_XMLǃbackgroundǃGRAPH
		getChildAt(index: 0): languageǁjapanese_XMLǃbackgroundǃGRAPH
		getChildById(id: 'n5_ggwy'): languageǁjapanese_XMLǃbackgroundǃGRAPH
		getChild(name: 'selected_background'): languageǁjapanese_XMLǃselected_backgroundǃGRAPH
		getChildAt(index: 1): languageǁjapanese_XMLǃselected_backgroundǃGRAPH
		getChildById(id: 'n6_11208'): languageǁjapanese_XMLǃselected_backgroundǃGRAPH
		getChild(name: 'icon'): languageǁjapanese_XMLǃiconǃIMAGE
		getChildAt(index: 2): languageǁjapanese_XMLǃiconǃIMAGE
		getChildById(id: 'n3_t9vb'): languageǁjapanese_XMLǃiconǃIMAGE
		getChild(name: 'text'): languageǁjapanese_XMLǃtextǃTEXT
		getChildAt(index: 3): languageǁjapanese_XMLǃtextǃTEXT
		getChildById(id: 'n4_t9vb'): languageǁjapanese_XMLǃtextǃTEXT
		_children: [
			languageǁjapanese_XMLǃbackgroundǃGRAPH,
			languageǁjapanese_XMLǃselected_backgroundǃGRAPH,
			languageǁjapanese_XMLǃiconǃIMAGE,
			languageǁjapanese_XMLǃtextǃTEXT
		]
		getController(name: 'language_ctrl'): languageǁjapanese_XMLǃlanguage_ctrlǃCONTROLLER
		getControllerAt(index: 0): languageǁjapanese_XMLǃlanguage_ctrlǃCONTROLLER
		getController(name: 'button'): languageǁjapanese_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): languageǁjapanese_XMLǃbuttonǃCONTROLLER
		_controllers: [
			languageǁjapanese_XMLǃlanguage_ctrlǃCONTROLLER,
			languageǁjapanese_XMLǃbuttonǃCONTROLLER
		]
	}
	interface languageǁjapanese_XMLǃlanguage_ctrlǃCONTROLLER extends fairygui.Controller{
		_parent: languageǁjapanese_XML
	}
	interface languageǁjapanese_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: languageǁjapanese_XML
	}
	interface languageǁjapanese_XMLǃbackgroundǃGRAPH extends fairygui.GGraph{
		parent: languageǁjapanese_XML
	}
	interface languageǁjapanese_XMLǃselected_backgroundǃGRAPH extends fairygui.GGraph{
		parent: languageǁjapanese_XML
	}
	interface languageǁjapanese_XMLǃiconǃIMAGE extends fairygui.GImage{
		parent: languageǁjapanese_XML
	}
	interface languageǁjapanese_XMLǃtextǃTEXT extends fairygui.GBasicTextField{
		parent: languageǁjapanese_XML
	}
	interface languageǁjapanese_XMLǃJapaneseǃITEM extends languageǁjapanese_XML{
		parent: languageǁselection_panel_XMLǃlanguage_listǃLIST
	}
	interface languageǁkorean_XML extends fairygui.GButton{
		getChild(name: 'background'): languageǁkorean_XMLǃbackgroundǃGRAPH
		getChildAt(index: 0): languageǁkorean_XMLǃbackgroundǃGRAPH
		getChildById(id: 'n5_ggwy'): languageǁkorean_XMLǃbackgroundǃGRAPH
		getChild(name: 'selected_background'): languageǁkorean_XMLǃselected_backgroundǃGRAPH
		getChildAt(index: 1): languageǁkorean_XMLǃselected_backgroundǃGRAPH
		getChildById(id: 'n6_11208'): languageǁkorean_XMLǃselected_backgroundǃGRAPH
		getChild(name: 'icon'): languageǁkorean_XMLǃiconǃIMAGE
		getChildAt(index: 2): languageǁkorean_XMLǃiconǃIMAGE
		getChildById(id: 'n3_t9vb'): languageǁkorean_XMLǃiconǃIMAGE
		getChild(name: 'text'): languageǁkorean_XMLǃtextǃTEXT
		getChildAt(index: 3): languageǁkorean_XMLǃtextǃTEXT
		getChildById(id: 'n4_t9vb'): languageǁkorean_XMLǃtextǃTEXT
		_children: [
			languageǁkorean_XMLǃbackgroundǃGRAPH,
			languageǁkorean_XMLǃselected_backgroundǃGRAPH,
			languageǁkorean_XMLǃiconǃIMAGE,
			languageǁkorean_XMLǃtextǃTEXT
		]
		getController(name: 'language_ctrl'): languageǁkorean_XMLǃlanguage_ctrlǃCONTROLLER
		getControllerAt(index: 0): languageǁkorean_XMLǃlanguage_ctrlǃCONTROLLER
		getController(name: 'button'): languageǁkorean_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): languageǁkorean_XMLǃbuttonǃCONTROLLER
		_controllers: [
			languageǁkorean_XMLǃlanguage_ctrlǃCONTROLLER,
			languageǁkorean_XMLǃbuttonǃCONTROLLER
		]
	}
	interface languageǁkorean_XMLǃlanguage_ctrlǃCONTROLLER extends fairygui.Controller{
		_parent: languageǁkorean_XML
	}
	interface languageǁkorean_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: languageǁkorean_XML
	}
	interface languageǁkorean_XMLǃbackgroundǃGRAPH extends fairygui.GGraph{
		parent: languageǁkorean_XML
	}
	interface languageǁkorean_XMLǃselected_backgroundǃGRAPH extends fairygui.GGraph{
		parent: languageǁkorean_XML
	}
	interface languageǁkorean_XMLǃiconǃIMAGE extends fairygui.GImage{
		parent: languageǁkorean_XML
	}
	interface languageǁkorean_XMLǃtextǃTEXT extends fairygui.GBasicTextField{
		parent: languageǁkorean_XML
	}
	interface languageǁkorean_XMLǃKoreanǃITEM extends languageǁkorean_XML{
		parent: languageǁselection_panel_XMLǃlanguage_listǃLIST
	}
	interface languageǁthai_XML extends fairygui.GButton{
		getChild(name: 'background'): languageǁthai_XMLǃbackgroundǃGRAPH
		getChildAt(index: 0): languageǁthai_XMLǃbackgroundǃGRAPH
		getChildById(id: 'n5_ggwy'): languageǁthai_XMLǃbackgroundǃGRAPH
		getChild(name: 'selected_background'): languageǁthai_XMLǃselected_backgroundǃGRAPH
		getChildAt(index: 1): languageǁthai_XMLǃselected_backgroundǃGRAPH
		getChildById(id: 'n6_11208'): languageǁthai_XMLǃselected_backgroundǃGRAPH
		getChild(name: 'icon'): languageǁthai_XMLǃiconǃIMAGE
		getChildAt(index: 2): languageǁthai_XMLǃiconǃIMAGE
		getChildById(id: 'n3_t9vb'): languageǁthai_XMLǃiconǃIMAGE
		getChild(name: 'text'): languageǁthai_XMLǃtextǃTEXT
		getChildAt(index: 3): languageǁthai_XMLǃtextǃTEXT
		getChildById(id: 'n4_t9vb'): languageǁthai_XMLǃtextǃTEXT
		_children: [
			languageǁthai_XMLǃbackgroundǃGRAPH,
			languageǁthai_XMLǃselected_backgroundǃGRAPH,
			languageǁthai_XMLǃiconǃIMAGE,
			languageǁthai_XMLǃtextǃTEXT
		]
		getController(name: 'language_ctrl'): languageǁthai_XMLǃlanguage_ctrlǃCONTROLLER
		getControllerAt(index: 0): languageǁthai_XMLǃlanguage_ctrlǃCONTROLLER
		getController(name: 'button'): languageǁthai_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): languageǁthai_XMLǃbuttonǃCONTROLLER
		_controllers: [
			languageǁthai_XMLǃlanguage_ctrlǃCONTROLLER,
			languageǁthai_XMLǃbuttonǃCONTROLLER
		]
	}
	interface languageǁthai_XMLǃlanguage_ctrlǃCONTROLLER extends fairygui.Controller{
		_parent: languageǁthai_XML
	}
	interface languageǁthai_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: languageǁthai_XML
	}
	interface languageǁthai_XMLǃbackgroundǃGRAPH extends fairygui.GGraph{
		parent: languageǁthai_XML
	}
	interface languageǁthai_XMLǃselected_backgroundǃGRAPH extends fairygui.GGraph{
		parent: languageǁthai_XML
	}
	interface languageǁthai_XMLǃiconǃIMAGE extends fairygui.GImage{
		parent: languageǁthai_XML
	}
	interface languageǁthai_XMLǃtextǃTEXT extends fairygui.GBasicTextField{
		parent: languageǁthai_XML
	}
	interface languageǁthai_XMLǃThaiǃITEM extends languageǁthai_XML{
		parent: languageǁselection_panel_XMLǃlanguage_listǃLIST
	}
	interface languageǁvietnamese_XML extends fairygui.GButton{
		getChild(name: 'background'): languageǁvietnamese_XMLǃbackgroundǃGRAPH
		getChildAt(index: 0): languageǁvietnamese_XMLǃbackgroundǃGRAPH
		getChildById(id: 'n5_ggwy'): languageǁvietnamese_XMLǃbackgroundǃGRAPH
		getChild(name: 'selected_background'): languageǁvietnamese_XMLǃselected_backgroundǃGRAPH
		getChildAt(index: 1): languageǁvietnamese_XMLǃselected_backgroundǃGRAPH
		getChildById(id: 'n6_11208'): languageǁvietnamese_XMLǃselected_backgroundǃGRAPH
		getChild(name: 'text'): languageǁvietnamese_XMLǃtextǃTEXT
		getChildAt(index: 2): languageǁvietnamese_XMLǃtextǃTEXT
		getChildById(id: 'n4_t9vb'): languageǁvietnamese_XMLǃtextǃTEXT
		getChild(name: 'n7'): languageǁvietnamese_XMLǃn7ǃIMAGE
		getChildAt(index: 3): languageǁvietnamese_XMLǃn7ǃIMAGE
		getChildById(id: 'n7_zegs'): languageǁvietnamese_XMLǃn7ǃIMAGE
		_children: [
			languageǁvietnamese_XMLǃbackgroundǃGRAPH,
			languageǁvietnamese_XMLǃselected_backgroundǃGRAPH,
			languageǁvietnamese_XMLǃtextǃTEXT,
			languageǁvietnamese_XMLǃn7ǃIMAGE
		]
		getController(name: 'language_ctrl'): languageǁvietnamese_XMLǃlanguage_ctrlǃCONTROLLER
		getControllerAt(index: 0): languageǁvietnamese_XMLǃlanguage_ctrlǃCONTROLLER
		getController(name: 'button'): languageǁvietnamese_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): languageǁvietnamese_XMLǃbuttonǃCONTROLLER
		_controllers: [
			languageǁvietnamese_XMLǃlanguage_ctrlǃCONTROLLER,
			languageǁvietnamese_XMLǃbuttonǃCONTROLLER
		]
	}
	interface languageǁvietnamese_XMLǃlanguage_ctrlǃCONTROLLER extends fairygui.Controller{
		_parent: languageǁvietnamese_XML
	}
	interface languageǁvietnamese_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: languageǁvietnamese_XML
	}
	interface languageǁvietnamese_XMLǃbackgroundǃGRAPH extends fairygui.GGraph{
		parent: languageǁvietnamese_XML
	}
	interface languageǁvietnamese_XMLǃselected_backgroundǃGRAPH extends fairygui.GGraph{
		parent: languageǁvietnamese_XML
	}
	interface languageǁvietnamese_XMLǃtextǃTEXT extends fairygui.GBasicTextField{
		parent: languageǁvietnamese_XML
	}
	interface languageǁvietnamese_XMLǃn7ǃIMAGE extends fairygui.GImage{
		parent: languageǁvietnamese_XML
	}
	interface languageǁvietnamese_XMLǃVietnameseǃITEM extends languageǁvietnamese_XML{
		parent: languageǁselection_panel_XMLǃlanguage_listǃLIST
	}
	interface languageǁselection_panel_XMLǃlanguage_panelǃCOMPONENT extends languageǁselection_panel_XML{
		parent: settings_XML
	}
	interface settings_XMLǃsettingsǃCOMPONENT extends settings_XML{
		parent: Main_XML
	}
	interface help_XML extends fairygui.GComponent{
		getChild(name: 'n8'): mask_empty_XMLǃn8ǃCOMPONENT
		getChildAt(index: 0): mask_empty_XMLǃn8ǃCOMPONENT
		getChildById(id: 'n8_s20q'): mask_empty_XMLǃn8ǃCOMPONENT
		getChild(name: 'n0'): help_XMLǃn0ǃIMAGE
		getChildAt(index: 1): help_XMLǃn0ǃIMAGE
		getChildById(id: 'n0_pfou'): help_XMLǃn0ǃIMAGE
		getChild(name: 'n6_EN'): help_XMLǃn6_ENǃIMAGE
		getChildAt(index: 2): help_XMLǃn6_ENǃIMAGE
		getChildById(id: 'n6_xls7_EN'): help_XMLǃn6_ENǃIMAGE
		getChild(name: 'n6_IN'): help_XMLǃn6_INǃIMAGE
		getChildAt(index: 3): help_XMLǃn6_INǃIMAGE
		getChildById(id: 'n6_xls7_IN'): help_XMLǃn6_INǃIMAGE
		getChild(name: 'n6_JP'): help_XMLǃn6_JPǃIMAGE
		getChildAt(index: 4): help_XMLǃn6_JPǃIMAGE
		getChildById(id: 'n6_xls7_JP'): help_XMLǃn6_JPǃIMAGE
		getChild(name: 'n6_KR'): help_XMLǃn6_KRǃIMAGE
		getChildAt(index: 5): help_XMLǃn6_KRǃIMAGE
		getChildById(id: 'n6_xls7_KR'): help_XMLǃn6_KRǃIMAGE
		getChild(name: 'n6_TH'): help_XMLǃn6_THǃIMAGE
		getChildAt(index: 6): help_XMLǃn6_THǃIMAGE
		getChildById(id: 'n6_xls7_TH'): help_XMLǃn6_THǃIMAGE
		getChild(name: 'n6_VN'): help_XMLǃn6_VNǃIMAGE
		getChildAt(index: 7): help_XMLǃn6_VNǃIMAGE
		getChildById(id: 'n6_xls7_VN'): help_XMLǃn6_VNǃIMAGE
		getChild(name: 'n6_CN2'): help_XMLǃn6_CN2ǃIMAGE
		getChildAt(index: 8): help_XMLǃn6_CN2ǃIMAGE
		getChildById(id: 'n6_xls7_CN2'): help_XMLǃn6_CN2ǃIMAGE
		getChild(name: 'n6'): help_XMLǃn6ǃIMAGE
		getChildAt(index: 9): help_XMLǃn6ǃIMAGE
		getChildById(id: 'n6_xls7'): help_XMLǃn6ǃIMAGE
		getChild(name: 'content'): helpneirong_XMLǃcontentǃCOMPONENT
		getChildAt(index: 10): helpneirong_XMLǃcontentǃCOMPONENT
		getChildById(id: 'n1_pfou'): helpneirong_XMLǃcontentǃCOMPONENT
		getChild(name: 'n9'): componentǁcontent2_XMLǃn9ǃCOMPONENT
		getChildAt(index: 11): componentǁcontent2_XMLǃn9ǃCOMPONENT
		getChildById(id: 'n9_cq3n'): componentǁcontent2_XMLǃn9ǃCOMPONENT
		getChild(name: 'n10'): componentǁContent3_XMLǃn10ǃCOMPONENT
		getChildAt(index: 12): componentǁContent3_XMLǃn10ǃCOMPONENT
		getChildById(id: 'n10_cq3n'): componentǁContent3_XMLǃn10ǃCOMPONENT
		getChild(name: 'scroll_down'): buttonsǁhelpcontentscrollǁbutton_scroll_down_XMLǃscroll_downǃCOMPONENT
		getChildAt(index: 13): buttonsǁhelpcontentscrollǁbutton_scroll_down_XMLǃscroll_downǃCOMPONENT
		getChildById(id: 'n3_pfou'): buttonsǁhelpcontentscrollǁbutton_scroll_down_XMLǃscroll_downǃCOMPONENT
		getChild(name: 'scroll_up'): buttonsǁhelpcontentscrollǁbutton_scroll_up_XMLǃscroll_upǃCOMPONENT
		getChildAt(index: 14): buttonsǁhelpcontentscrollǁbutton_scroll_up_XMLǃscroll_upǃCOMPONENT
		getChildById(id: 'n5_ibn5'): buttonsǁhelpcontentscrollǁbutton_scroll_up_XMLǃscroll_upǃCOMPONENT
		getChild(name: 'close'): buttonsǁbutton_guanbiluzhitu_XMLǃcloseǃCOMPONENT
		getChildAt(index: 15): buttonsǁbutton_guanbiluzhitu_XMLǃcloseǃCOMPONENT
		getChildById(id: 'n4_ibn5'): buttonsǁbutton_guanbiluzhitu_XMLǃcloseǃCOMPONENT
		_children: [
			mask_empty_XMLǃn8ǃCOMPONENT,
			help_XMLǃn0ǃIMAGE,
			help_XMLǃn6_ENǃIMAGE,
			help_XMLǃn6_INǃIMAGE,
			help_XMLǃn6_JPǃIMAGE,
			help_XMLǃn6_KRǃIMAGE,
			help_XMLǃn6_THǃIMAGE,
			help_XMLǃn6_VNǃIMAGE,
			help_XMLǃn6_CN2ǃIMAGE,
			help_XMLǃn6ǃIMAGE,
			helpneirong_XMLǃcontentǃCOMPONENT,
			componentǁcontent2_XMLǃn9ǃCOMPONENT,
			componentǁContent3_XMLǃn10ǃCOMPONENT,
			buttonsǁhelpcontentscrollǁbutton_scroll_down_XMLǃscroll_downǃCOMPONENT,
			buttonsǁhelpcontentscrollǁbutton_scroll_up_XMLǃscroll_upǃCOMPONENT,
			buttonsǁbutton_guanbiluzhitu_XMLǃcloseǃCOMPONENT
		]
		getController(name: '__language'): help_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): help_XMLǃ__languageǃCONTROLLER
		getController(name: 'c1'): help_XMLǃc1ǃCONTROLLER
		getControllerAt(index: 1): help_XMLǃc1ǃCONTROLLER
		_controllers: [
			help_XMLǃ__languageǃCONTROLLER,
			help_XMLǃc1ǃCONTROLLER
		]
	}
	interface help_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: help_XML
	}
	interface help_XMLǃc1ǃCONTROLLER extends fairygui.Controller{
		_parent: help_XML
	}
	interface mask_empty_XMLǃn8ǃCOMPONENT extends mask_empty_XML{
		parent: help_XML
	}
	interface help_XMLǃn0ǃIMAGE extends fairygui.GImage{
		parent: help_XML
	}
	interface help_XMLǃn6_ENǃIMAGE extends fairygui.GImage{
		parent: help_XML
	}
	interface help_XMLǃn6_INǃIMAGE extends fairygui.GImage{
		parent: help_XML
	}
	interface help_XMLǃn6_JPǃIMAGE extends fairygui.GImage{
		parent: help_XML
	}
	interface help_XMLǃn6_KRǃIMAGE extends fairygui.GImage{
		parent: help_XML
	}
	interface help_XMLǃn6_THǃIMAGE extends fairygui.GImage{
		parent: help_XML
	}
	interface help_XMLǃn6_VNǃIMAGE extends fairygui.GImage{
		parent: help_XML
	}
	interface help_XMLǃn6_CN2ǃIMAGE extends fairygui.GImage{
		parent: help_XML
	}
	interface help_XMLǃn6ǃIMAGE extends fairygui.GImage{
		parent: help_XML
	}
	interface helpneirong_XML extends fairygui.GComponent{
		getChild(name: 'n2_CN2'): helpneirong_XMLǃn2_CN2ǃTEXT
		getChildAt(index: 0): helpneirong_XMLǃn2_CN2ǃTEXT
		getChildById(id: 'n2_nca5_CN2'): helpneirong_XMLǃn2_CN2ǃTEXT
		getChild(name: 'n2_EN'): helpneirong_XMLǃn2_ENǃTEXT
		getChildAt(index: 1): helpneirong_XMLǃn2_ENǃTEXT
		getChildById(id: 'n2_nca5_EN'): helpneirong_XMLǃn2_ENǃTEXT
		getChild(name: 'n2_IN'): helpneirong_XMLǃn2_INǃTEXT
		getChildAt(index: 2): helpneirong_XMLǃn2_INǃTEXT
		getChildById(id: 'n2_nca5_IN'): helpneirong_XMLǃn2_INǃTEXT
		getChild(name: 'n2_JP'): helpneirong_XMLǃn2_JPǃTEXT
		getChildAt(index: 3): helpneirong_XMLǃn2_JPǃTEXT
		getChildById(id: 'n2_nca5_JP'): helpneirong_XMLǃn2_JPǃTEXT
		getChild(name: 'n2_KR'): helpneirong_XMLǃn2_KRǃTEXT
		getChildAt(index: 4): helpneirong_XMLǃn2_KRǃTEXT
		getChildById(id: 'n2_nca5_KR'): helpneirong_XMLǃn2_KRǃTEXT
		getChild(name: 'n2_TH'): helpneirong_XMLǃn2_THǃTEXT
		getChildAt(index: 5): helpneirong_XMLǃn2_THǃTEXT
		getChildById(id: 'n2_nca5_TH'): helpneirong_XMLǃn2_THǃTEXT
		getChild(name: 'n2_VN'): helpneirong_XMLǃn2_VNǃTEXT
		getChildAt(index: 6): helpneirong_XMLǃn2_VNǃTEXT
		getChildById(id: 'n2_nca5_VN'): helpneirong_XMLǃn2_VNǃTEXT
		getChild(name: 'n2'): helpneirong_XMLǃn2ǃTEXT
		getChildAt(index: 7): helpneirong_XMLǃn2ǃTEXT
		getChildById(id: 'n2_nca5'): helpneirong_XMLǃn2ǃTEXT
		_children: [
			helpneirong_XMLǃn2_CN2ǃTEXT,
			helpneirong_XMLǃn2_ENǃTEXT,
			helpneirong_XMLǃn2_INǃTEXT,
			helpneirong_XMLǃn2_JPǃTEXT,
			helpneirong_XMLǃn2_KRǃTEXT,
			helpneirong_XMLǃn2_THǃTEXT,
			helpneirong_XMLǃn2_VNǃTEXT,
			helpneirong_XMLǃn2ǃTEXT
		]
		getController(name: '__language'): helpneirong_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): helpneirong_XMLǃ__languageǃCONTROLLER
		_controllers: [
			helpneirong_XMLǃ__languageǃCONTROLLER
		]
	}
	interface helpneirong_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: helpneirong_XML
	}
	interface helpneirong_XMLǃn2_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: helpneirong_XML
	}
	interface helpneirong_XMLǃn2_ENǃTEXT extends fairygui.GBasicTextField{
		parent: helpneirong_XML
	}
	interface helpneirong_XMLǃn2_INǃTEXT extends fairygui.GBasicTextField{
		parent: helpneirong_XML
	}
	interface helpneirong_XMLǃn2_JPǃTEXT extends fairygui.GBasicTextField{
		parent: helpneirong_XML
	}
	interface helpneirong_XMLǃn2_KRǃTEXT extends fairygui.GBasicTextField{
		parent: helpneirong_XML
	}
	interface helpneirong_XMLǃn2_THǃTEXT extends fairygui.GBasicTextField{
		parent: helpneirong_XML
	}
	interface helpneirong_XMLǃn2_VNǃTEXT extends fairygui.GBasicTextField{
		parent: helpneirong_XML
	}
	interface helpneirong_XMLǃn2ǃTEXT extends fairygui.GBasicTextField{
		parent: helpneirong_XML
	}
	interface helpneirong_XMLǃcontentǃCOMPONENT extends helpneirong_XML{
		parent: help_XML
	}
	interface componentǁcontent2_XML extends fairygui.GComponent{
		getChild(name: 'n3_CN2'): componentǁcontent2_XMLǃn3_CN2ǃTEXT
		getChildAt(index: 0): componentǁcontent2_XMLǃn3_CN2ǃTEXT
		getChildById(id: 'n3_j069_CN2'): componentǁcontent2_XMLǃn3_CN2ǃTEXT
		getChild(name: 'n3_EN'): componentǁcontent2_XMLǃn3_ENǃTEXT
		getChildAt(index: 1): componentǁcontent2_XMLǃn3_ENǃTEXT
		getChildById(id: 'n3_j069_EN'): componentǁcontent2_XMLǃn3_ENǃTEXT
		getChild(name: 'n3_IN'): componentǁcontent2_XMLǃn3_INǃTEXT
		getChildAt(index: 2): componentǁcontent2_XMLǃn3_INǃTEXT
		getChildById(id: 'n3_j069_IN'): componentǁcontent2_XMLǃn3_INǃTEXT
		getChild(name: 'n3_JP'): componentǁcontent2_XMLǃn3_JPǃTEXT
		getChildAt(index: 3): componentǁcontent2_XMLǃn3_JPǃTEXT
		getChildById(id: 'n3_j069_JP'): componentǁcontent2_XMLǃn3_JPǃTEXT
		getChild(name: 'n3_KR'): componentǁcontent2_XMLǃn3_KRǃTEXT
		getChildAt(index: 4): componentǁcontent2_XMLǃn3_KRǃTEXT
		getChildById(id: 'n3_j069_KR'): componentǁcontent2_XMLǃn3_KRǃTEXT
		getChild(name: 'n3_TH'): componentǁcontent2_XMLǃn3_THǃTEXT
		getChildAt(index: 5): componentǁcontent2_XMLǃn3_THǃTEXT
		getChildById(id: 'n3_j069_TH'): componentǁcontent2_XMLǃn3_THǃTEXT
		getChild(name: 'n3_VN'): componentǁcontent2_XMLǃn3_VNǃTEXT
		getChildAt(index: 6): componentǁcontent2_XMLǃn3_VNǃTEXT
		getChildById(id: 'n3_j069_VN'): componentǁcontent2_XMLǃn3_VNǃTEXT
		getChild(name: 'n3'): componentǁcontent2_XMLǃn3ǃTEXT
		getChildAt(index: 7): componentǁcontent2_XMLǃn3ǃTEXT
		getChildById(id: 'n3_j069'): componentǁcontent2_XMLǃn3ǃTEXT
		getChild(name: 'n5'): componentǁcontent2_XMLǃn5ǃGRAPH
		getChildAt(index: 8): componentǁcontent2_XMLǃn5ǃGRAPH
		getChildById(id: 'n5_j069'): componentǁcontent2_XMLǃn5ǃGRAPH
		getChild(name: 'n6'): componentǁcontent2_XMLǃn6ǃGRAPH
		getChildAt(index: 9): componentǁcontent2_XMLǃn6ǃGRAPH
		getChildById(id: 'n6_j069'): componentǁcontent2_XMLǃn6ǃGRAPH
		getChild(name: 'n7'): componentǁcontent2_XMLǃn7ǃGRAPH
		getChildAt(index: 10): componentǁcontent2_XMLǃn7ǃGRAPH
		getChildById(id: 'n7_j069'): componentǁcontent2_XMLǃn7ǃGRAPH
		getChild(name: 'n8'): componentǁcontent2_XMLǃn8ǃGRAPH
		getChildAt(index: 11): componentǁcontent2_XMLǃn8ǃGRAPH
		getChildById(id: 'n8_j069'): componentǁcontent2_XMLǃn8ǃGRAPH
		getChild(name: 'n9'): componentǁcontent2_XMLǃn9ǃGRAPH
		getChildAt(index: 12): componentǁcontent2_XMLǃn9ǃGRAPH
		getChildById(id: 'n9_j069'): componentǁcontent2_XMLǃn9ǃGRAPH
		getChild(name: 'n10'): componentǁcontent2_XMLǃn10ǃGRAPH
		getChildAt(index: 13): componentǁcontent2_XMLǃn10ǃGRAPH
		getChildById(id: 'n10_j069'): componentǁcontent2_XMLǃn10ǃGRAPH
		getChild(name: 'n11'): componentǁcontent2_XMLǃn11ǃGRAPH
		getChildAt(index: 14): componentǁcontent2_XMLǃn11ǃGRAPH
		getChildById(id: 'n11_j069'): componentǁcontent2_XMLǃn11ǃGRAPH
		getChild(name: 'n14_CN2'): componentǁcontent2_XMLǃn14_CN2ǃTEXT
		getChildAt(index: 15): componentǁcontent2_XMLǃn14_CN2ǃTEXT
		getChildById(id: 'n14_j069_CN2'): componentǁcontent2_XMLǃn14_CN2ǃTEXT
		getChild(name: 'n14_EN'): componentǁcontent2_XMLǃn14_ENǃTEXT
		getChildAt(index: 16): componentǁcontent2_XMLǃn14_ENǃTEXT
		getChildById(id: 'n14_j069_EN'): componentǁcontent2_XMLǃn14_ENǃTEXT
		getChild(name: 'n14_IN'): componentǁcontent2_XMLǃn14_INǃTEXT
		getChildAt(index: 17): componentǁcontent2_XMLǃn14_INǃTEXT
		getChildById(id: 'n14_j069_IN'): componentǁcontent2_XMLǃn14_INǃTEXT
		getChild(name: 'n14_JP'): componentǁcontent2_XMLǃn14_JPǃTEXT
		getChildAt(index: 18): componentǁcontent2_XMLǃn14_JPǃTEXT
		getChildById(id: 'n14_j069_JP'): componentǁcontent2_XMLǃn14_JPǃTEXT
		getChild(name: 'n14_KR'): componentǁcontent2_XMLǃn14_KRǃTEXT
		getChildAt(index: 19): componentǁcontent2_XMLǃn14_KRǃTEXT
		getChildById(id: 'n14_j069_KR'): componentǁcontent2_XMLǃn14_KRǃTEXT
		getChild(name: 'n14_TH'): componentǁcontent2_XMLǃn14_THǃTEXT
		getChildAt(index: 20): componentǁcontent2_XMLǃn14_THǃTEXT
		getChildById(id: 'n14_j069_TH'): componentǁcontent2_XMLǃn14_THǃTEXT
		getChild(name: 'n14_VN'): componentǁcontent2_XMLǃn14_VNǃTEXT
		getChildAt(index: 21): componentǁcontent2_XMLǃn14_VNǃTEXT
		getChildById(id: 'n14_j069_VN'): componentǁcontent2_XMLǃn14_VNǃTEXT
		getChild(name: 'n14'): componentǁcontent2_XMLǃn14ǃTEXT
		getChildAt(index: 22): componentǁcontent2_XMLǃn14ǃTEXT
		getChildById(id: 'n14_j069'): componentǁcontent2_XMLǃn14ǃTEXT
		getChild(name: 'n15_CN2'): componentǁcontent2_XMLǃn15_CN2ǃTEXT
		getChildAt(index: 23): componentǁcontent2_XMLǃn15_CN2ǃTEXT
		getChildById(id: 'n15_j069_CN2'): componentǁcontent2_XMLǃn15_CN2ǃTEXT
		getChild(name: 'n15_EN'): componentǁcontent2_XMLǃn15_ENǃTEXT
		getChildAt(index: 24): componentǁcontent2_XMLǃn15_ENǃTEXT
		getChildById(id: 'n15_j069_EN'): componentǁcontent2_XMLǃn15_ENǃTEXT
		getChild(name: 'n15_IN'): componentǁcontent2_XMLǃn15_INǃTEXT
		getChildAt(index: 25): componentǁcontent2_XMLǃn15_INǃTEXT
		getChildById(id: 'n15_j069_IN'): componentǁcontent2_XMLǃn15_INǃTEXT
		getChild(name: 'n15_JP'): componentǁcontent2_XMLǃn15_JPǃTEXT
		getChildAt(index: 26): componentǁcontent2_XMLǃn15_JPǃTEXT
		getChildById(id: 'n15_j069_JP'): componentǁcontent2_XMLǃn15_JPǃTEXT
		getChild(name: 'n15_KR'): componentǁcontent2_XMLǃn15_KRǃTEXT
		getChildAt(index: 27): componentǁcontent2_XMLǃn15_KRǃTEXT
		getChildById(id: 'n15_j069_KR'): componentǁcontent2_XMLǃn15_KRǃTEXT
		getChild(name: 'n15_TH'): componentǁcontent2_XMLǃn15_THǃTEXT
		getChildAt(index: 28): componentǁcontent2_XMLǃn15_THǃTEXT
		getChildById(id: 'n15_j069_TH'): componentǁcontent2_XMLǃn15_THǃTEXT
		getChild(name: 'n15_VN'): componentǁcontent2_XMLǃn15_VNǃTEXT
		getChildAt(index: 29): componentǁcontent2_XMLǃn15_VNǃTEXT
		getChildById(id: 'n15_j069_VN'): componentǁcontent2_XMLǃn15_VNǃTEXT
		getChild(name: 'n15'): componentǁcontent2_XMLǃn15ǃTEXT
		getChildAt(index: 30): componentǁcontent2_XMLǃn15ǃTEXT
		getChildById(id: 'n15_j069'): componentǁcontent2_XMLǃn15ǃTEXT
		getChild(name: 'n16_CN2'): componentǁcontent2_XMLǃn16_CN2ǃTEXT
		getChildAt(index: 31): componentǁcontent2_XMLǃn16_CN2ǃTEXT
		getChildById(id: 'n16_j069_CN2'): componentǁcontent2_XMLǃn16_CN2ǃTEXT
		getChild(name: 'n16_EN'): componentǁcontent2_XMLǃn16_ENǃTEXT
		getChildAt(index: 32): componentǁcontent2_XMLǃn16_ENǃTEXT
		getChildById(id: 'n16_j069_EN'): componentǁcontent2_XMLǃn16_ENǃTEXT
		getChild(name: 'n16_IN'): componentǁcontent2_XMLǃn16_INǃTEXT
		getChildAt(index: 33): componentǁcontent2_XMLǃn16_INǃTEXT
		getChildById(id: 'n16_j069_IN'): componentǁcontent2_XMLǃn16_INǃTEXT
		getChild(name: 'n16_JP'): componentǁcontent2_XMLǃn16_JPǃTEXT
		getChildAt(index: 34): componentǁcontent2_XMLǃn16_JPǃTEXT
		getChildById(id: 'n16_j069_JP'): componentǁcontent2_XMLǃn16_JPǃTEXT
		getChild(name: 'n16_KR'): componentǁcontent2_XMLǃn16_KRǃTEXT
		getChildAt(index: 35): componentǁcontent2_XMLǃn16_KRǃTEXT
		getChildById(id: 'n16_j069_KR'): componentǁcontent2_XMLǃn16_KRǃTEXT
		getChild(name: 'n16_TH'): componentǁcontent2_XMLǃn16_THǃTEXT
		getChildAt(index: 36): componentǁcontent2_XMLǃn16_THǃTEXT
		getChildById(id: 'n16_j069_TH'): componentǁcontent2_XMLǃn16_THǃTEXT
		getChild(name: 'n16_VN'): componentǁcontent2_XMLǃn16_VNǃTEXT
		getChildAt(index: 37): componentǁcontent2_XMLǃn16_VNǃTEXT
		getChildById(id: 'n16_j069_VN'): componentǁcontent2_XMLǃn16_VNǃTEXT
		getChild(name: 'n16'): componentǁcontent2_XMLǃn16ǃTEXT
		getChildAt(index: 38): componentǁcontent2_XMLǃn16ǃTEXT
		getChildById(id: 'n16_j069'): componentǁcontent2_XMLǃn16ǃTEXT
		getChild(name: 'n17'): componentǁcontent2_XMLǃn17ǃTEXT
		getChildAt(index: 39): componentǁcontent2_XMLǃn17ǃTEXT
		getChildById(id: 'n17_j069'): componentǁcontent2_XMLǃn17ǃTEXT
		getChild(name: 'n18'): componentǁcontent2_XMLǃn18ǃTEXT
		getChildAt(index: 40): componentǁcontent2_XMLǃn18ǃTEXT
		getChildById(id: 'n18_j069'): componentǁcontent2_XMLǃn18ǃTEXT
		getChild(name: 'n19'): componentǁcontent2_XMLǃn19ǃTEXT
		getChildAt(index: 41): componentǁcontent2_XMLǃn19ǃTEXT
		getChildById(id: 'n19_j069'): componentǁcontent2_XMLǃn19ǃTEXT
		getChild(name: 'n20'): componentǁcontent2_XMLǃn20ǃTEXT
		getChildAt(index: 42): componentǁcontent2_XMLǃn20ǃTEXT
		getChildById(id: 'n20_j069'): componentǁcontent2_XMLǃn20ǃTEXT
		getChild(name: 'n21'): componentǁcontent2_XMLǃn21ǃTEXT
		getChildAt(index: 43): componentǁcontent2_XMLǃn21ǃTEXT
		getChildById(id: 'n21_j069'): componentǁcontent2_XMLǃn21ǃTEXT
		getChild(name: 'n22'): componentǁcontent2_XMLǃn22ǃTEXT
		getChildAt(index: 44): componentǁcontent2_XMLǃn22ǃTEXT
		getChildById(id: 'n22_j069'): componentǁcontent2_XMLǃn22ǃTEXT
		getChild(name: 'n23'): componentǁcontent2_XMLǃn23ǃTEXT
		getChildAt(index: 45): componentǁcontent2_XMLǃn23ǃTEXT
		getChildById(id: 'n23_j069'): componentǁcontent2_XMLǃn23ǃTEXT
		getChild(name: 'n24'): componentǁcontent2_XMLǃn24ǃTEXT
		getChildAt(index: 46): componentǁcontent2_XMLǃn24ǃTEXT
		getChildById(id: 'n24_j069'): componentǁcontent2_XMLǃn24ǃTEXT
		getChild(name: 'n25'): componentǁcontent2_XMLǃn25ǃTEXT
		getChildAt(index: 47): componentǁcontent2_XMLǃn25ǃTEXT
		getChildById(id: 'n25_j069'): componentǁcontent2_XMLǃn25ǃTEXT
		getChild(name: 'n26'): componentǁcontent2_XMLǃn26ǃTEXT
		getChildAt(index: 48): componentǁcontent2_XMLǃn26ǃTEXT
		getChildById(id: 'n26_j069'): componentǁcontent2_XMLǃn26ǃTEXT
		getChild(name: 'n27_CN2'): componentǁcontent2_XMLǃn27_CN2ǃTEXT
		getChildAt(index: 49): componentǁcontent2_XMLǃn27_CN2ǃTEXT
		getChildById(id: 'n27_j069_CN2'): componentǁcontent2_XMLǃn27_CN2ǃTEXT
		getChild(name: 'n27_EN'): componentǁcontent2_XMLǃn27_ENǃTEXT
		getChildAt(index: 50): componentǁcontent2_XMLǃn27_ENǃTEXT
		getChildById(id: 'n27_j069_EN'): componentǁcontent2_XMLǃn27_ENǃTEXT
		getChild(name: 'n27_IN'): componentǁcontent2_XMLǃn27_INǃTEXT
		getChildAt(index: 51): componentǁcontent2_XMLǃn27_INǃTEXT
		getChildById(id: 'n27_j069_IN'): componentǁcontent2_XMLǃn27_INǃTEXT
		getChild(name: 'n27_JP'): componentǁcontent2_XMLǃn27_JPǃTEXT
		getChildAt(index: 52): componentǁcontent2_XMLǃn27_JPǃTEXT
		getChildById(id: 'n27_j069_JP'): componentǁcontent2_XMLǃn27_JPǃTEXT
		getChild(name: 'n27_KR'): componentǁcontent2_XMLǃn27_KRǃTEXT
		getChildAt(index: 53): componentǁcontent2_XMLǃn27_KRǃTEXT
		getChildById(id: 'n27_j069_KR'): componentǁcontent2_XMLǃn27_KRǃTEXT
		getChild(name: 'n27_TH'): componentǁcontent2_XMLǃn27_THǃTEXT
		getChildAt(index: 54): componentǁcontent2_XMLǃn27_THǃTEXT
		getChildById(id: 'n27_j069_TH'): componentǁcontent2_XMLǃn27_THǃTEXT
		getChild(name: 'n27_VN'): componentǁcontent2_XMLǃn27_VNǃTEXT
		getChildAt(index: 55): componentǁcontent2_XMLǃn27_VNǃTEXT
		getChildById(id: 'n27_j069_VN'): componentǁcontent2_XMLǃn27_VNǃTEXT
		getChild(name: 'n27'): componentǁcontent2_XMLǃn27ǃTEXT
		getChildAt(index: 56): componentǁcontent2_XMLǃn27ǃTEXT
		getChildById(id: 'n27_j069'): componentǁcontent2_XMLǃn27ǃTEXT
		getChild(name: 'n29_CN2'): componentǁcontent2_XMLǃn29_CN2ǃTEXT
		getChildAt(index: 57): componentǁcontent2_XMLǃn29_CN2ǃTEXT
		getChildById(id: 'n29_j069_CN2'): componentǁcontent2_XMLǃn29_CN2ǃTEXT
		getChild(name: 'n29_EN'): componentǁcontent2_XMLǃn29_ENǃTEXT
		getChildAt(index: 58): componentǁcontent2_XMLǃn29_ENǃTEXT
		getChildById(id: 'n29_j069_EN'): componentǁcontent2_XMLǃn29_ENǃTEXT
		getChild(name: 'n29_IN'): componentǁcontent2_XMLǃn29_INǃTEXT
		getChildAt(index: 59): componentǁcontent2_XMLǃn29_INǃTEXT
		getChildById(id: 'n29_j069_IN'): componentǁcontent2_XMLǃn29_INǃTEXT
		getChild(name: 'n29_JP'): componentǁcontent2_XMLǃn29_JPǃTEXT
		getChildAt(index: 60): componentǁcontent2_XMLǃn29_JPǃTEXT
		getChildById(id: 'n29_j069_JP'): componentǁcontent2_XMLǃn29_JPǃTEXT
		getChild(name: 'n29_KR'): componentǁcontent2_XMLǃn29_KRǃTEXT
		getChildAt(index: 61): componentǁcontent2_XMLǃn29_KRǃTEXT
		getChildById(id: 'n29_j069_KR'): componentǁcontent2_XMLǃn29_KRǃTEXT
		getChild(name: 'n29_TH'): componentǁcontent2_XMLǃn29_THǃTEXT
		getChildAt(index: 62): componentǁcontent2_XMLǃn29_THǃTEXT
		getChildById(id: 'n29_j069_TH'): componentǁcontent2_XMLǃn29_THǃTEXT
		getChild(name: 'n29_VN'): componentǁcontent2_XMLǃn29_VNǃTEXT
		getChildAt(index: 63): componentǁcontent2_XMLǃn29_VNǃTEXT
		getChildById(id: 'n29_j069_VN'): componentǁcontent2_XMLǃn29_VNǃTEXT
		getChild(name: 'n29'): componentǁcontent2_XMLǃn29ǃTEXT
		getChildAt(index: 64): componentǁcontent2_XMLǃn29ǃTEXT
		getChildById(id: 'n29_j069'): componentǁcontent2_XMLǃn29ǃTEXT
		getChild(name: 'n30_CN2'): componentǁcontent2_XMLǃn30_CN2ǃTEXT
		getChildAt(index: 65): componentǁcontent2_XMLǃn30_CN2ǃTEXT
		getChildById(id: 'n30_j069_CN2'): componentǁcontent2_XMLǃn30_CN2ǃTEXT
		getChild(name: 'n30_EN'): componentǁcontent2_XMLǃn30_ENǃTEXT
		getChildAt(index: 66): componentǁcontent2_XMLǃn30_ENǃTEXT
		getChildById(id: 'n30_j069_EN'): componentǁcontent2_XMLǃn30_ENǃTEXT
		getChild(name: 'n30_IN'): componentǁcontent2_XMLǃn30_INǃTEXT
		getChildAt(index: 67): componentǁcontent2_XMLǃn30_INǃTEXT
		getChildById(id: 'n30_j069_IN'): componentǁcontent2_XMLǃn30_INǃTEXT
		getChild(name: 'n30_JP'): componentǁcontent2_XMLǃn30_JPǃTEXT
		getChildAt(index: 68): componentǁcontent2_XMLǃn30_JPǃTEXT
		getChildById(id: 'n30_j069_JP'): componentǁcontent2_XMLǃn30_JPǃTEXT
		getChild(name: 'n30_KR'): componentǁcontent2_XMLǃn30_KRǃTEXT
		getChildAt(index: 69): componentǁcontent2_XMLǃn30_KRǃTEXT
		getChildById(id: 'n30_j069_KR'): componentǁcontent2_XMLǃn30_KRǃTEXT
		getChild(name: 'n30_TH'): componentǁcontent2_XMLǃn30_THǃTEXT
		getChildAt(index: 70): componentǁcontent2_XMLǃn30_THǃTEXT
		getChildById(id: 'n30_j069_TH'): componentǁcontent2_XMLǃn30_THǃTEXT
		getChild(name: 'n30_VN'): componentǁcontent2_XMLǃn30_VNǃTEXT
		getChildAt(index: 71): componentǁcontent2_XMLǃn30_VNǃTEXT
		getChildById(id: 'n30_j069_VN'): componentǁcontent2_XMLǃn30_VNǃTEXT
		getChild(name: 'n30'): componentǁcontent2_XMLǃn30ǃTEXT
		getChildAt(index: 72): componentǁcontent2_XMLǃn30ǃTEXT
		getChildById(id: 'n30_j069'): componentǁcontent2_XMLǃn30ǃTEXT
		getChild(name: 'n31_CN2'): componentǁcontent2_XMLǃn31_CN2ǃTEXT
		getChildAt(index: 73): componentǁcontent2_XMLǃn31_CN2ǃTEXT
		getChildById(id: 'n31_j069_CN2'): componentǁcontent2_XMLǃn31_CN2ǃTEXT
		getChild(name: 'n31_EN'): componentǁcontent2_XMLǃn31_ENǃTEXT
		getChildAt(index: 74): componentǁcontent2_XMLǃn31_ENǃTEXT
		getChildById(id: 'n31_j069_EN'): componentǁcontent2_XMLǃn31_ENǃTEXT
		getChild(name: 'n31_IN'): componentǁcontent2_XMLǃn31_INǃTEXT
		getChildAt(index: 75): componentǁcontent2_XMLǃn31_INǃTEXT
		getChildById(id: 'n31_j069_IN'): componentǁcontent2_XMLǃn31_INǃTEXT
		getChild(name: 'n31_JP'): componentǁcontent2_XMLǃn31_JPǃTEXT
		getChildAt(index: 76): componentǁcontent2_XMLǃn31_JPǃTEXT
		getChildById(id: 'n31_j069_JP'): componentǁcontent2_XMLǃn31_JPǃTEXT
		getChild(name: 'n31_KR'): componentǁcontent2_XMLǃn31_KRǃTEXT
		getChildAt(index: 77): componentǁcontent2_XMLǃn31_KRǃTEXT
		getChildById(id: 'n31_j069_KR'): componentǁcontent2_XMLǃn31_KRǃTEXT
		getChild(name: 'n31_TH'): componentǁcontent2_XMLǃn31_THǃTEXT
		getChildAt(index: 78): componentǁcontent2_XMLǃn31_THǃTEXT
		getChildById(id: 'n31_j069_TH'): componentǁcontent2_XMLǃn31_THǃTEXT
		getChild(name: 'n31_VN'): componentǁcontent2_XMLǃn31_VNǃTEXT
		getChildAt(index: 79): componentǁcontent2_XMLǃn31_VNǃTEXT
		getChildById(id: 'n31_j069_VN'): componentǁcontent2_XMLǃn31_VNǃTEXT
		getChild(name: 'n31'): componentǁcontent2_XMLǃn31ǃTEXT
		getChildAt(index: 80): componentǁcontent2_XMLǃn31ǃTEXT
		getChildById(id: 'n31_j069'): componentǁcontent2_XMLǃn31ǃTEXT
		getChild(name: 'n32_CN2'): componentǁcontent2_XMLǃn32_CN2ǃTEXT
		getChildAt(index: 81): componentǁcontent2_XMLǃn32_CN2ǃTEXT
		getChildById(id: 'n32_j069_CN2'): componentǁcontent2_XMLǃn32_CN2ǃTEXT
		getChild(name: 'n32_EN'): componentǁcontent2_XMLǃn32_ENǃTEXT
		getChildAt(index: 82): componentǁcontent2_XMLǃn32_ENǃTEXT
		getChildById(id: 'n32_j069_EN'): componentǁcontent2_XMLǃn32_ENǃTEXT
		getChild(name: 'n32_IN'): componentǁcontent2_XMLǃn32_INǃTEXT
		getChildAt(index: 83): componentǁcontent2_XMLǃn32_INǃTEXT
		getChildById(id: 'n32_j069_IN'): componentǁcontent2_XMLǃn32_INǃTEXT
		getChild(name: 'n32_JP'): componentǁcontent2_XMLǃn32_JPǃTEXT
		getChildAt(index: 84): componentǁcontent2_XMLǃn32_JPǃTEXT
		getChildById(id: 'n32_j069_JP'): componentǁcontent2_XMLǃn32_JPǃTEXT
		getChild(name: 'n32_KR'): componentǁcontent2_XMLǃn32_KRǃTEXT
		getChildAt(index: 85): componentǁcontent2_XMLǃn32_KRǃTEXT
		getChildById(id: 'n32_j069_KR'): componentǁcontent2_XMLǃn32_KRǃTEXT
		getChild(name: 'n32_TH'): componentǁcontent2_XMLǃn32_THǃTEXT
		getChildAt(index: 86): componentǁcontent2_XMLǃn32_THǃTEXT
		getChildById(id: 'n32_j069_TH'): componentǁcontent2_XMLǃn32_THǃTEXT
		getChild(name: 'n32_VN'): componentǁcontent2_XMLǃn32_VNǃTEXT
		getChildAt(index: 87): componentǁcontent2_XMLǃn32_VNǃTEXT
		getChildById(id: 'n32_j069_VN'): componentǁcontent2_XMLǃn32_VNǃTEXT
		getChild(name: 'n32'): componentǁcontent2_XMLǃn32ǃTEXT
		getChildAt(index: 88): componentǁcontent2_XMLǃn32ǃTEXT
		getChildById(id: 'n32_j069'): componentǁcontent2_XMLǃn32ǃTEXT
		getChild(name: 'n33_CN2'): componentǁcontent2_XMLǃn33_CN2ǃTEXT
		getChildAt(index: 89): componentǁcontent2_XMLǃn33_CN2ǃTEXT
		getChildById(id: 'n33_j069_CN2'): componentǁcontent2_XMLǃn33_CN2ǃTEXT
		getChild(name: 'n33_EN'): componentǁcontent2_XMLǃn33_ENǃTEXT
		getChildAt(index: 90): componentǁcontent2_XMLǃn33_ENǃTEXT
		getChildById(id: 'n33_j069_EN'): componentǁcontent2_XMLǃn33_ENǃTEXT
		getChild(name: 'n33_IN'): componentǁcontent2_XMLǃn33_INǃTEXT
		getChildAt(index: 91): componentǁcontent2_XMLǃn33_INǃTEXT
		getChildById(id: 'n33_j069_IN'): componentǁcontent2_XMLǃn33_INǃTEXT
		getChild(name: 'n33_JP'): componentǁcontent2_XMLǃn33_JPǃTEXT
		getChildAt(index: 92): componentǁcontent2_XMLǃn33_JPǃTEXT
		getChildById(id: 'n33_j069_JP'): componentǁcontent2_XMLǃn33_JPǃTEXT
		getChild(name: 'n33_KR'): componentǁcontent2_XMLǃn33_KRǃTEXT
		getChildAt(index: 93): componentǁcontent2_XMLǃn33_KRǃTEXT
		getChildById(id: 'n33_j069_KR'): componentǁcontent2_XMLǃn33_KRǃTEXT
		getChild(name: 'n33_TH'): componentǁcontent2_XMLǃn33_THǃTEXT
		getChildAt(index: 94): componentǁcontent2_XMLǃn33_THǃTEXT
		getChildById(id: 'n33_j069_TH'): componentǁcontent2_XMLǃn33_THǃTEXT
		getChild(name: 'n33_VN'): componentǁcontent2_XMLǃn33_VNǃTEXT
		getChildAt(index: 95): componentǁcontent2_XMLǃn33_VNǃTEXT
		getChildById(id: 'n33_j069_VN'): componentǁcontent2_XMLǃn33_VNǃTEXT
		getChild(name: 'n33'): componentǁcontent2_XMLǃn33ǃTEXT
		getChildAt(index: 96): componentǁcontent2_XMLǃn33ǃTEXT
		getChildById(id: 'n33_j069'): componentǁcontent2_XMLǃn33ǃTEXT
		getChild(name: 'n34_CN2'): componentǁcontent2_XMLǃn34_CN2ǃTEXT
		getChildAt(index: 97): componentǁcontent2_XMLǃn34_CN2ǃTEXT
		getChildById(id: 'n34_j069_CN2'): componentǁcontent2_XMLǃn34_CN2ǃTEXT
		getChild(name: 'n34_EN'): componentǁcontent2_XMLǃn34_ENǃTEXT
		getChildAt(index: 98): componentǁcontent2_XMLǃn34_ENǃTEXT
		getChildById(id: 'n34_j069_EN'): componentǁcontent2_XMLǃn34_ENǃTEXT
		getChild(name: 'n34_IN'): componentǁcontent2_XMLǃn34_INǃTEXT
		getChildAt(index: 99): componentǁcontent2_XMLǃn34_INǃTEXT
		getChildById(id: 'n34_j069_IN'): componentǁcontent2_XMLǃn34_INǃTEXT
		getChild(name: 'n34_JP'): componentǁcontent2_XMLǃn34_JPǃTEXT
		getChildAt(index: 100): componentǁcontent2_XMLǃn34_JPǃTEXT
		getChildById(id: 'n34_j069_JP'): componentǁcontent2_XMLǃn34_JPǃTEXT
		getChild(name: 'n34_KR'): componentǁcontent2_XMLǃn34_KRǃTEXT
		getChildAt(index: 101): componentǁcontent2_XMLǃn34_KRǃTEXT
		getChildById(id: 'n34_j069_KR'): componentǁcontent2_XMLǃn34_KRǃTEXT
		getChild(name: 'n34_TH'): componentǁcontent2_XMLǃn34_THǃTEXT
		getChildAt(index: 102): componentǁcontent2_XMLǃn34_THǃTEXT
		getChildById(id: 'n34_j069_TH'): componentǁcontent2_XMLǃn34_THǃTEXT
		getChild(name: 'n34_VN'): componentǁcontent2_XMLǃn34_VNǃTEXT
		getChildAt(index: 103): componentǁcontent2_XMLǃn34_VNǃTEXT
		getChildById(id: 'n34_j069_VN'): componentǁcontent2_XMLǃn34_VNǃTEXT
		getChild(name: 'n34'): componentǁcontent2_XMLǃn34ǃTEXT
		getChildAt(index: 104): componentǁcontent2_XMLǃn34ǃTEXT
		getChildById(id: 'n34_j069'): componentǁcontent2_XMLǃn34ǃTEXT
		getChild(name: 'n35_CN2'): componentǁcontent2_XMLǃn35_CN2ǃTEXT
		getChildAt(index: 105): componentǁcontent2_XMLǃn35_CN2ǃTEXT
		getChildById(id: 'n35_j069_CN2'): componentǁcontent2_XMLǃn35_CN2ǃTEXT
		getChild(name: 'n35_EN'): componentǁcontent2_XMLǃn35_ENǃTEXT
		getChildAt(index: 106): componentǁcontent2_XMLǃn35_ENǃTEXT
		getChildById(id: 'n35_j069_EN'): componentǁcontent2_XMLǃn35_ENǃTEXT
		getChild(name: 'n35_IN'): componentǁcontent2_XMLǃn35_INǃTEXT
		getChildAt(index: 107): componentǁcontent2_XMLǃn35_INǃTEXT
		getChildById(id: 'n35_j069_IN'): componentǁcontent2_XMLǃn35_INǃTEXT
		getChild(name: 'n35_JP'): componentǁcontent2_XMLǃn35_JPǃTEXT
		getChildAt(index: 108): componentǁcontent2_XMLǃn35_JPǃTEXT
		getChildById(id: 'n35_j069_JP'): componentǁcontent2_XMLǃn35_JPǃTEXT
		getChild(name: 'n35_KR'): componentǁcontent2_XMLǃn35_KRǃTEXT
		getChildAt(index: 109): componentǁcontent2_XMLǃn35_KRǃTEXT
		getChildById(id: 'n35_j069_KR'): componentǁcontent2_XMLǃn35_KRǃTEXT
		getChild(name: 'n35_TH'): componentǁcontent2_XMLǃn35_THǃTEXT
		getChildAt(index: 110): componentǁcontent2_XMLǃn35_THǃTEXT
		getChildById(id: 'n35_j069_TH'): componentǁcontent2_XMLǃn35_THǃTEXT
		getChild(name: 'n35_VN'): componentǁcontent2_XMLǃn35_VNǃTEXT
		getChildAt(index: 111): componentǁcontent2_XMLǃn35_VNǃTEXT
		getChildById(id: 'n35_j069_VN'): componentǁcontent2_XMLǃn35_VNǃTEXT
		getChild(name: 'n35'): componentǁcontent2_XMLǃn35ǃTEXT
		getChildAt(index: 112): componentǁcontent2_XMLǃn35ǃTEXT
		getChildById(id: 'n35_j069'): componentǁcontent2_XMLǃn35ǃTEXT
		getChild(name: 'n36_CN2'): componentǁcontent2_XMLǃn36_CN2ǃTEXT
		getChildAt(index: 113): componentǁcontent2_XMLǃn36_CN2ǃTEXT
		getChildById(id: 'n36_j069_CN2'): componentǁcontent2_XMLǃn36_CN2ǃTEXT
		getChild(name: 'n36_EN'): componentǁcontent2_XMLǃn36_ENǃTEXT
		getChildAt(index: 114): componentǁcontent2_XMLǃn36_ENǃTEXT
		getChildById(id: 'n36_j069_EN'): componentǁcontent2_XMLǃn36_ENǃTEXT
		getChild(name: 'n36_IN'): componentǁcontent2_XMLǃn36_INǃTEXT
		getChildAt(index: 115): componentǁcontent2_XMLǃn36_INǃTEXT
		getChildById(id: 'n36_j069_IN'): componentǁcontent2_XMLǃn36_INǃTEXT
		getChild(name: 'n36_JP'): componentǁcontent2_XMLǃn36_JPǃTEXT
		getChildAt(index: 116): componentǁcontent2_XMLǃn36_JPǃTEXT
		getChildById(id: 'n36_j069_JP'): componentǁcontent2_XMLǃn36_JPǃTEXT
		getChild(name: 'n36_KR'): componentǁcontent2_XMLǃn36_KRǃTEXT
		getChildAt(index: 117): componentǁcontent2_XMLǃn36_KRǃTEXT
		getChildById(id: 'n36_j069_KR'): componentǁcontent2_XMLǃn36_KRǃTEXT
		getChild(name: 'n36_TH'): componentǁcontent2_XMLǃn36_THǃTEXT
		getChildAt(index: 118): componentǁcontent2_XMLǃn36_THǃTEXT
		getChildById(id: 'n36_j069_TH'): componentǁcontent2_XMLǃn36_THǃTEXT
		getChild(name: 'n36_VN'): componentǁcontent2_XMLǃn36_VNǃTEXT
		getChildAt(index: 119): componentǁcontent2_XMLǃn36_VNǃTEXT
		getChildById(id: 'n36_j069_VN'): componentǁcontent2_XMLǃn36_VNǃTEXT
		getChild(name: 'n36'): componentǁcontent2_XMLǃn36ǃTEXT
		getChildAt(index: 120): componentǁcontent2_XMLǃn36ǃTEXT
		getChildById(id: 'n36_j069'): componentǁcontent2_XMLǃn36ǃTEXT
		getChild(name: 'n37_CN2'): componentǁcontent2_XMLǃn37_CN2ǃTEXT
		getChildAt(index: 121): componentǁcontent2_XMLǃn37_CN2ǃTEXT
		getChildById(id: 'n37_j069_CN2'): componentǁcontent2_XMLǃn37_CN2ǃTEXT
		getChild(name: 'n37_EN'): componentǁcontent2_XMLǃn37_ENǃTEXT
		getChildAt(index: 122): componentǁcontent2_XMLǃn37_ENǃTEXT
		getChildById(id: 'n37_j069_EN'): componentǁcontent2_XMLǃn37_ENǃTEXT
		getChild(name: 'n37_IN'): componentǁcontent2_XMLǃn37_INǃTEXT
		getChildAt(index: 123): componentǁcontent2_XMLǃn37_INǃTEXT
		getChildById(id: 'n37_j069_IN'): componentǁcontent2_XMLǃn37_INǃTEXT
		getChild(name: 'n37_JP'): componentǁcontent2_XMLǃn37_JPǃTEXT
		getChildAt(index: 124): componentǁcontent2_XMLǃn37_JPǃTEXT
		getChildById(id: 'n37_j069_JP'): componentǁcontent2_XMLǃn37_JPǃTEXT
		getChild(name: 'n37_KR'): componentǁcontent2_XMLǃn37_KRǃTEXT
		getChildAt(index: 125): componentǁcontent2_XMLǃn37_KRǃTEXT
		getChildById(id: 'n37_j069_KR'): componentǁcontent2_XMLǃn37_KRǃTEXT
		getChild(name: 'n37_TH'): componentǁcontent2_XMLǃn37_THǃTEXT
		getChildAt(index: 126): componentǁcontent2_XMLǃn37_THǃTEXT
		getChildById(id: 'n37_j069_TH'): componentǁcontent2_XMLǃn37_THǃTEXT
		getChild(name: 'n37_VN'): componentǁcontent2_XMLǃn37_VNǃTEXT
		getChildAt(index: 127): componentǁcontent2_XMLǃn37_VNǃTEXT
		getChildById(id: 'n37_j069_VN'): componentǁcontent2_XMLǃn37_VNǃTEXT
		getChild(name: 'n37'): componentǁcontent2_XMLǃn37ǃTEXT
		getChildAt(index: 128): componentǁcontent2_XMLǃn37ǃTEXT
		getChildById(id: 'n37_j069'): componentǁcontent2_XMLǃn37ǃTEXT
		getChild(name: 'n38_CN2'): componentǁcontent2_XMLǃn38_CN2ǃTEXT
		getChildAt(index: 129): componentǁcontent2_XMLǃn38_CN2ǃTEXT
		getChildById(id: 'n38_j069_CN2'): componentǁcontent2_XMLǃn38_CN2ǃTEXT
		getChild(name: 'n38_EN'): componentǁcontent2_XMLǃn38_ENǃTEXT
		getChildAt(index: 130): componentǁcontent2_XMLǃn38_ENǃTEXT
		getChildById(id: 'n38_j069_EN'): componentǁcontent2_XMLǃn38_ENǃTEXT
		getChild(name: 'n38_IN'): componentǁcontent2_XMLǃn38_INǃTEXT
		getChildAt(index: 131): componentǁcontent2_XMLǃn38_INǃTEXT
		getChildById(id: 'n38_j069_IN'): componentǁcontent2_XMLǃn38_INǃTEXT
		getChild(name: 'n38_JP'): componentǁcontent2_XMLǃn38_JPǃTEXT
		getChildAt(index: 132): componentǁcontent2_XMLǃn38_JPǃTEXT
		getChildById(id: 'n38_j069_JP'): componentǁcontent2_XMLǃn38_JPǃTEXT
		getChild(name: 'n38_KR'): componentǁcontent2_XMLǃn38_KRǃTEXT
		getChildAt(index: 133): componentǁcontent2_XMLǃn38_KRǃTEXT
		getChildById(id: 'n38_j069_KR'): componentǁcontent2_XMLǃn38_KRǃTEXT
		getChild(name: 'n38_TH'): componentǁcontent2_XMLǃn38_THǃTEXT
		getChildAt(index: 134): componentǁcontent2_XMLǃn38_THǃTEXT
		getChildById(id: 'n38_j069_TH'): componentǁcontent2_XMLǃn38_THǃTEXT
		getChild(name: 'n38_VN'): componentǁcontent2_XMLǃn38_VNǃTEXT
		getChildAt(index: 135): componentǁcontent2_XMLǃn38_VNǃTEXT
		getChildById(id: 'n38_j069_VN'): componentǁcontent2_XMLǃn38_VNǃTEXT
		getChild(name: 'n38'): componentǁcontent2_XMLǃn38ǃTEXT
		getChildAt(index: 136): componentǁcontent2_XMLǃn38ǃTEXT
		getChildById(id: 'n38_j069'): componentǁcontent2_XMLǃn38ǃTEXT
		getChild(name: 'n39_CN2'): componentǁcontent2_XMLǃn39_CN2ǃTEXT
		getChildAt(index: 137): componentǁcontent2_XMLǃn39_CN2ǃTEXT
		getChildById(id: 'n39_j069_CN2'): componentǁcontent2_XMLǃn39_CN2ǃTEXT
		getChild(name: 'n39_EN'): componentǁcontent2_XMLǃn39_ENǃTEXT
		getChildAt(index: 138): componentǁcontent2_XMLǃn39_ENǃTEXT
		getChildById(id: 'n39_j069_EN'): componentǁcontent2_XMLǃn39_ENǃTEXT
		getChild(name: 'n39_IN'): componentǁcontent2_XMLǃn39_INǃTEXT
		getChildAt(index: 139): componentǁcontent2_XMLǃn39_INǃTEXT
		getChildById(id: 'n39_j069_IN'): componentǁcontent2_XMLǃn39_INǃTEXT
		getChild(name: 'n39_JP'): componentǁcontent2_XMLǃn39_JPǃTEXT
		getChildAt(index: 140): componentǁcontent2_XMLǃn39_JPǃTEXT
		getChildById(id: 'n39_j069_JP'): componentǁcontent2_XMLǃn39_JPǃTEXT
		getChild(name: 'n39_KR'): componentǁcontent2_XMLǃn39_KRǃTEXT
		getChildAt(index: 141): componentǁcontent2_XMLǃn39_KRǃTEXT
		getChildById(id: 'n39_j069_KR'): componentǁcontent2_XMLǃn39_KRǃTEXT
		getChild(name: 'n39_TH'): componentǁcontent2_XMLǃn39_THǃTEXT
		getChildAt(index: 142): componentǁcontent2_XMLǃn39_THǃTEXT
		getChildById(id: 'n39_j069_TH'): componentǁcontent2_XMLǃn39_THǃTEXT
		getChild(name: 'n39_VN'): componentǁcontent2_XMLǃn39_VNǃTEXT
		getChildAt(index: 143): componentǁcontent2_XMLǃn39_VNǃTEXT
		getChildById(id: 'n39_j069_VN'): componentǁcontent2_XMLǃn39_VNǃTEXT
		getChild(name: 'n39'): componentǁcontent2_XMLǃn39ǃTEXT
		getChildAt(index: 144): componentǁcontent2_XMLǃn39ǃTEXT
		getChildById(id: 'n39_j069'): componentǁcontent2_XMLǃn39ǃTEXT
		getChild(name: 'n40_CN2'): componentǁcontent2_XMLǃn40_CN2ǃTEXT
		getChildAt(index: 145): componentǁcontent2_XMLǃn40_CN2ǃTEXT
		getChildById(id: 'n40_j069_CN2'): componentǁcontent2_XMLǃn40_CN2ǃTEXT
		getChild(name: 'n40_EN'): componentǁcontent2_XMLǃn40_ENǃTEXT
		getChildAt(index: 146): componentǁcontent2_XMLǃn40_ENǃTEXT
		getChildById(id: 'n40_j069_EN'): componentǁcontent2_XMLǃn40_ENǃTEXT
		getChild(name: 'n40_IN'): componentǁcontent2_XMLǃn40_INǃTEXT
		getChildAt(index: 147): componentǁcontent2_XMLǃn40_INǃTEXT
		getChildById(id: 'n40_j069_IN'): componentǁcontent2_XMLǃn40_INǃTEXT
		getChild(name: 'n40_JP'): componentǁcontent2_XMLǃn40_JPǃTEXT
		getChildAt(index: 148): componentǁcontent2_XMLǃn40_JPǃTEXT
		getChildById(id: 'n40_j069_JP'): componentǁcontent2_XMLǃn40_JPǃTEXT
		getChild(name: 'n40_KR'): componentǁcontent2_XMLǃn40_KRǃTEXT
		getChildAt(index: 149): componentǁcontent2_XMLǃn40_KRǃTEXT
		getChildById(id: 'n40_j069_KR'): componentǁcontent2_XMLǃn40_KRǃTEXT
		getChild(name: 'n40_TH'): componentǁcontent2_XMLǃn40_THǃTEXT
		getChildAt(index: 150): componentǁcontent2_XMLǃn40_THǃTEXT
		getChildById(id: 'n40_j069_TH'): componentǁcontent2_XMLǃn40_THǃTEXT
		getChild(name: 'n40_VN'): componentǁcontent2_XMLǃn40_VNǃTEXT
		getChildAt(index: 151): componentǁcontent2_XMLǃn40_VNǃTEXT
		getChildById(id: 'n40_j069_VN'): componentǁcontent2_XMLǃn40_VNǃTEXT
		getChild(name: 'n40'): componentǁcontent2_XMLǃn40ǃTEXT
		getChildAt(index: 152): componentǁcontent2_XMLǃn40ǃTEXT
		getChildById(id: 'n40_j069'): componentǁcontent2_XMLǃn40ǃTEXT
		getChild(name: 'n41_CN2'): componentǁcontent2_XMLǃn41_CN2ǃTEXT
		getChildAt(index: 153): componentǁcontent2_XMLǃn41_CN2ǃTEXT
		getChildById(id: 'n41_j069_CN2'): componentǁcontent2_XMLǃn41_CN2ǃTEXT
		getChild(name: 'n41_EN'): componentǁcontent2_XMLǃn41_ENǃTEXT
		getChildAt(index: 154): componentǁcontent2_XMLǃn41_ENǃTEXT
		getChildById(id: 'n41_j069_EN'): componentǁcontent2_XMLǃn41_ENǃTEXT
		getChild(name: 'n41_IN'): componentǁcontent2_XMLǃn41_INǃTEXT
		getChildAt(index: 155): componentǁcontent2_XMLǃn41_INǃTEXT
		getChildById(id: 'n41_j069_IN'): componentǁcontent2_XMLǃn41_INǃTEXT
		getChild(name: 'n41_JP'): componentǁcontent2_XMLǃn41_JPǃTEXT
		getChildAt(index: 156): componentǁcontent2_XMLǃn41_JPǃTEXT
		getChildById(id: 'n41_j069_JP'): componentǁcontent2_XMLǃn41_JPǃTEXT
		getChild(name: 'n41_KR'): componentǁcontent2_XMLǃn41_KRǃTEXT
		getChildAt(index: 157): componentǁcontent2_XMLǃn41_KRǃTEXT
		getChildById(id: 'n41_j069_KR'): componentǁcontent2_XMLǃn41_KRǃTEXT
		getChild(name: 'n41_TH'): componentǁcontent2_XMLǃn41_THǃTEXT
		getChildAt(index: 158): componentǁcontent2_XMLǃn41_THǃTEXT
		getChildById(id: 'n41_j069_TH'): componentǁcontent2_XMLǃn41_THǃTEXT
		getChild(name: 'n41_VN'): componentǁcontent2_XMLǃn41_VNǃTEXT
		getChildAt(index: 159): componentǁcontent2_XMLǃn41_VNǃTEXT
		getChildById(id: 'n41_j069_VN'): componentǁcontent2_XMLǃn41_VNǃTEXT
		getChild(name: 'n41'): componentǁcontent2_XMLǃn41ǃTEXT
		getChildAt(index: 160): componentǁcontent2_XMLǃn41ǃTEXT
		getChildById(id: 'n41_j069'): componentǁcontent2_XMLǃn41ǃTEXT
		getChild(name: 'n42_CN2'): componentǁcontent2_XMLǃn42_CN2ǃTEXT
		getChildAt(index: 161): componentǁcontent2_XMLǃn42_CN2ǃTEXT
		getChildById(id: 'n42_j069_CN2'): componentǁcontent2_XMLǃn42_CN2ǃTEXT
		getChild(name: 'n42_EN'): componentǁcontent2_XMLǃn42_ENǃTEXT
		getChildAt(index: 162): componentǁcontent2_XMLǃn42_ENǃTEXT
		getChildById(id: 'n42_j069_EN'): componentǁcontent2_XMLǃn42_ENǃTEXT
		getChild(name: 'n42_IN'): componentǁcontent2_XMLǃn42_INǃTEXT
		getChildAt(index: 163): componentǁcontent2_XMLǃn42_INǃTEXT
		getChildById(id: 'n42_j069_IN'): componentǁcontent2_XMLǃn42_INǃTEXT
		getChild(name: 'n42_JP'): componentǁcontent2_XMLǃn42_JPǃTEXT
		getChildAt(index: 164): componentǁcontent2_XMLǃn42_JPǃTEXT
		getChildById(id: 'n42_j069_JP'): componentǁcontent2_XMLǃn42_JPǃTEXT
		getChild(name: 'n42_KR'): componentǁcontent2_XMLǃn42_KRǃTEXT
		getChildAt(index: 165): componentǁcontent2_XMLǃn42_KRǃTEXT
		getChildById(id: 'n42_j069_KR'): componentǁcontent2_XMLǃn42_KRǃTEXT
		getChild(name: 'n42_TH'): componentǁcontent2_XMLǃn42_THǃTEXT
		getChildAt(index: 166): componentǁcontent2_XMLǃn42_THǃTEXT
		getChildById(id: 'n42_j069_TH'): componentǁcontent2_XMLǃn42_THǃTEXT
		getChild(name: 'n42_VN'): componentǁcontent2_XMLǃn42_VNǃTEXT
		getChildAt(index: 167): componentǁcontent2_XMLǃn42_VNǃTEXT
		getChildById(id: 'n42_j069_VN'): componentǁcontent2_XMLǃn42_VNǃTEXT
		getChild(name: 'n42'): componentǁcontent2_XMLǃn42ǃTEXT
		getChildAt(index: 168): componentǁcontent2_XMLǃn42ǃTEXT
		getChildById(id: 'n42_j069'): componentǁcontent2_XMLǃn42ǃTEXT
		getChild(name: 'n43_CN2'): componentǁcontent2_XMLǃn43_CN2ǃTEXT
		getChildAt(index: 169): componentǁcontent2_XMLǃn43_CN2ǃTEXT
		getChildById(id: 'n43_j069_CN2'): componentǁcontent2_XMLǃn43_CN2ǃTEXT
		getChild(name: 'n43_EN'): componentǁcontent2_XMLǃn43_ENǃTEXT
		getChildAt(index: 170): componentǁcontent2_XMLǃn43_ENǃTEXT
		getChildById(id: 'n43_j069_EN'): componentǁcontent2_XMLǃn43_ENǃTEXT
		getChild(name: 'n43_IN'): componentǁcontent2_XMLǃn43_INǃTEXT
		getChildAt(index: 171): componentǁcontent2_XMLǃn43_INǃTEXT
		getChildById(id: 'n43_j069_IN'): componentǁcontent2_XMLǃn43_INǃTEXT
		getChild(name: 'n43_JP'): componentǁcontent2_XMLǃn43_JPǃTEXT
		getChildAt(index: 172): componentǁcontent2_XMLǃn43_JPǃTEXT
		getChildById(id: 'n43_j069_JP'): componentǁcontent2_XMLǃn43_JPǃTEXT
		getChild(name: 'n43_KR'): componentǁcontent2_XMLǃn43_KRǃTEXT
		getChildAt(index: 173): componentǁcontent2_XMLǃn43_KRǃTEXT
		getChildById(id: 'n43_j069_KR'): componentǁcontent2_XMLǃn43_KRǃTEXT
		getChild(name: 'n43_TH'): componentǁcontent2_XMLǃn43_THǃTEXT
		getChildAt(index: 174): componentǁcontent2_XMLǃn43_THǃTEXT
		getChildById(id: 'n43_j069_TH'): componentǁcontent2_XMLǃn43_THǃTEXT
		getChild(name: 'n43_VN'): componentǁcontent2_XMLǃn43_VNǃTEXT
		getChildAt(index: 175): componentǁcontent2_XMLǃn43_VNǃTEXT
		getChildById(id: 'n43_j069_VN'): componentǁcontent2_XMLǃn43_VNǃTEXT
		getChild(name: 'n43'): componentǁcontent2_XMLǃn43ǃTEXT
		getChildAt(index: 176): componentǁcontent2_XMLǃn43ǃTEXT
		getChildById(id: 'n43_j069'): componentǁcontent2_XMLǃn43ǃTEXT
		getChild(name: 'n44_CN2'): componentǁcontent2_XMLǃn44_CN2ǃTEXT
		getChildAt(index: 177): componentǁcontent2_XMLǃn44_CN2ǃTEXT
		getChildById(id: 'n44_j069_CN2'): componentǁcontent2_XMLǃn44_CN2ǃTEXT
		getChild(name: 'n44_EN'): componentǁcontent2_XMLǃn44_ENǃTEXT
		getChildAt(index: 178): componentǁcontent2_XMLǃn44_ENǃTEXT
		getChildById(id: 'n44_j069_EN'): componentǁcontent2_XMLǃn44_ENǃTEXT
		getChild(name: 'n44_IN'): componentǁcontent2_XMLǃn44_INǃTEXT
		getChildAt(index: 179): componentǁcontent2_XMLǃn44_INǃTEXT
		getChildById(id: 'n44_j069_IN'): componentǁcontent2_XMLǃn44_INǃTEXT
		getChild(name: 'n44_JP'): componentǁcontent2_XMLǃn44_JPǃTEXT
		getChildAt(index: 180): componentǁcontent2_XMLǃn44_JPǃTEXT
		getChildById(id: 'n44_j069_JP'): componentǁcontent2_XMLǃn44_JPǃTEXT
		getChild(name: 'n44_KR'): componentǁcontent2_XMLǃn44_KRǃTEXT
		getChildAt(index: 181): componentǁcontent2_XMLǃn44_KRǃTEXT
		getChildById(id: 'n44_j069_KR'): componentǁcontent2_XMLǃn44_KRǃTEXT
		getChild(name: 'n44_TH'): componentǁcontent2_XMLǃn44_THǃTEXT
		getChildAt(index: 182): componentǁcontent2_XMLǃn44_THǃTEXT
		getChildById(id: 'n44_j069_TH'): componentǁcontent2_XMLǃn44_THǃTEXT
		getChild(name: 'n44_VN'): componentǁcontent2_XMLǃn44_VNǃTEXT
		getChildAt(index: 183): componentǁcontent2_XMLǃn44_VNǃTEXT
		getChildById(id: 'n44_j069_VN'): componentǁcontent2_XMLǃn44_VNǃTEXT
		getChild(name: 'n44'): componentǁcontent2_XMLǃn44ǃTEXT
		getChildAt(index: 184): componentǁcontent2_XMLǃn44ǃTEXT
		getChildById(id: 'n44_j069'): componentǁcontent2_XMLǃn44ǃTEXT
		getChild(name: 'n45_CN2'): componentǁcontent2_XMLǃn45_CN2ǃTEXT
		getChildAt(index: 185): componentǁcontent2_XMLǃn45_CN2ǃTEXT
		getChildById(id: 'n45_j069_CN2'): componentǁcontent2_XMLǃn45_CN2ǃTEXT
		getChild(name: 'n45_EN'): componentǁcontent2_XMLǃn45_ENǃTEXT
		getChildAt(index: 186): componentǁcontent2_XMLǃn45_ENǃTEXT
		getChildById(id: 'n45_j069_EN'): componentǁcontent2_XMLǃn45_ENǃTEXT
		getChild(name: 'n45_IN'): componentǁcontent2_XMLǃn45_INǃTEXT
		getChildAt(index: 187): componentǁcontent2_XMLǃn45_INǃTEXT
		getChildById(id: 'n45_j069_IN'): componentǁcontent2_XMLǃn45_INǃTEXT
		getChild(name: 'n45_JP'): componentǁcontent2_XMLǃn45_JPǃTEXT
		getChildAt(index: 188): componentǁcontent2_XMLǃn45_JPǃTEXT
		getChildById(id: 'n45_j069_JP'): componentǁcontent2_XMLǃn45_JPǃTEXT
		getChild(name: 'n45_KR'): componentǁcontent2_XMLǃn45_KRǃTEXT
		getChildAt(index: 189): componentǁcontent2_XMLǃn45_KRǃTEXT
		getChildById(id: 'n45_j069_KR'): componentǁcontent2_XMLǃn45_KRǃTEXT
		getChild(name: 'n45_TH'): componentǁcontent2_XMLǃn45_THǃTEXT
		getChildAt(index: 190): componentǁcontent2_XMLǃn45_THǃTEXT
		getChildById(id: 'n45_j069_TH'): componentǁcontent2_XMLǃn45_THǃTEXT
		getChild(name: 'n45_VN'): componentǁcontent2_XMLǃn45_VNǃTEXT
		getChildAt(index: 191): componentǁcontent2_XMLǃn45_VNǃTEXT
		getChildById(id: 'n45_j069_VN'): componentǁcontent2_XMLǃn45_VNǃTEXT
		getChild(name: 'n45'): componentǁcontent2_XMLǃn45ǃTEXT
		getChildAt(index: 192): componentǁcontent2_XMLǃn45ǃTEXT
		getChildById(id: 'n45_j069'): componentǁcontent2_XMLǃn45ǃTEXT
		getChild(name: 'n46_CN2'): componentǁcontent2_XMLǃn46_CN2ǃTEXT
		getChildAt(index: 193): componentǁcontent2_XMLǃn46_CN2ǃTEXT
		getChildById(id: 'n46_j069_CN2'): componentǁcontent2_XMLǃn46_CN2ǃTEXT
		getChild(name: 'n46_EN'): componentǁcontent2_XMLǃn46_ENǃTEXT
		getChildAt(index: 194): componentǁcontent2_XMLǃn46_ENǃTEXT
		getChildById(id: 'n46_j069_EN'): componentǁcontent2_XMLǃn46_ENǃTEXT
		getChild(name: 'n46_IN'): componentǁcontent2_XMLǃn46_INǃTEXT
		getChildAt(index: 195): componentǁcontent2_XMLǃn46_INǃTEXT
		getChildById(id: 'n46_j069_IN'): componentǁcontent2_XMLǃn46_INǃTEXT
		getChild(name: 'n46_JP'): componentǁcontent2_XMLǃn46_JPǃTEXT
		getChildAt(index: 196): componentǁcontent2_XMLǃn46_JPǃTEXT
		getChildById(id: 'n46_j069_JP'): componentǁcontent2_XMLǃn46_JPǃTEXT
		getChild(name: 'n46_KR'): componentǁcontent2_XMLǃn46_KRǃTEXT
		getChildAt(index: 197): componentǁcontent2_XMLǃn46_KRǃTEXT
		getChildById(id: 'n46_j069_KR'): componentǁcontent2_XMLǃn46_KRǃTEXT
		getChild(name: 'n46_TH'): componentǁcontent2_XMLǃn46_THǃTEXT
		getChildAt(index: 198): componentǁcontent2_XMLǃn46_THǃTEXT
		getChildById(id: 'n46_j069_TH'): componentǁcontent2_XMLǃn46_THǃTEXT
		getChild(name: 'n46_VN'): componentǁcontent2_XMLǃn46_VNǃTEXT
		getChildAt(index: 199): componentǁcontent2_XMLǃn46_VNǃTEXT
		getChildById(id: 'n46_j069_VN'): componentǁcontent2_XMLǃn46_VNǃTEXT
		getChild(name: 'n46'): componentǁcontent2_XMLǃn46ǃTEXT
		getChildAt(index: 200): componentǁcontent2_XMLǃn46ǃTEXT
		getChildById(id: 'n46_j069'): componentǁcontent2_XMLǃn46ǃTEXT
		getChild(name: 'n48_CN2'): componentǁcontent2_XMLǃn48_CN2ǃTEXT
		getChildAt(index: 201): componentǁcontent2_XMLǃn48_CN2ǃTEXT
		getChildById(id: 'n48_j069_CN2'): componentǁcontent2_XMLǃn48_CN2ǃTEXT
		getChild(name: 'n48_EN'): componentǁcontent2_XMLǃn48_ENǃTEXT
		getChildAt(index: 202): componentǁcontent2_XMLǃn48_ENǃTEXT
		getChildById(id: 'n48_j069_EN'): componentǁcontent2_XMLǃn48_ENǃTEXT
		getChild(name: 'n48_IN'): componentǁcontent2_XMLǃn48_INǃTEXT
		getChildAt(index: 203): componentǁcontent2_XMLǃn48_INǃTEXT
		getChildById(id: 'n48_j069_IN'): componentǁcontent2_XMLǃn48_INǃTEXT
		getChild(name: 'n48_JP'): componentǁcontent2_XMLǃn48_JPǃTEXT
		getChildAt(index: 204): componentǁcontent2_XMLǃn48_JPǃTEXT
		getChildById(id: 'n48_j069_JP'): componentǁcontent2_XMLǃn48_JPǃTEXT
		getChild(name: 'n48_KR'): componentǁcontent2_XMLǃn48_KRǃTEXT
		getChildAt(index: 205): componentǁcontent2_XMLǃn48_KRǃTEXT
		getChildById(id: 'n48_j069_KR'): componentǁcontent2_XMLǃn48_KRǃTEXT
		getChild(name: 'n48_TH'): componentǁcontent2_XMLǃn48_THǃTEXT
		getChildAt(index: 206): componentǁcontent2_XMLǃn48_THǃTEXT
		getChildById(id: 'n48_j069_TH'): componentǁcontent2_XMLǃn48_THǃTEXT
		getChild(name: 'n48_VN'): componentǁcontent2_XMLǃn48_VNǃTEXT
		getChildAt(index: 207): componentǁcontent2_XMLǃn48_VNǃTEXT
		getChildById(id: 'n48_j069_VN'): componentǁcontent2_XMLǃn48_VNǃTEXT
		getChild(name: 'n48'): componentǁcontent2_XMLǃn48ǃTEXT
		getChildAt(index: 208): componentǁcontent2_XMLǃn48ǃTEXT
		getChildById(id: 'n48_j069'): componentǁcontent2_XMLǃn48ǃTEXT
		_children: [
			componentǁcontent2_XMLǃn3_CN2ǃTEXT,
			componentǁcontent2_XMLǃn3_ENǃTEXT,
			componentǁcontent2_XMLǃn3_INǃTEXT,
			componentǁcontent2_XMLǃn3_JPǃTEXT,
			componentǁcontent2_XMLǃn3_KRǃTEXT,
			componentǁcontent2_XMLǃn3_THǃTEXT,
			componentǁcontent2_XMLǃn3_VNǃTEXT,
			componentǁcontent2_XMLǃn3ǃTEXT,
			componentǁcontent2_XMLǃn5ǃGRAPH,
			componentǁcontent2_XMLǃn6ǃGRAPH,
			componentǁcontent2_XMLǃn7ǃGRAPH,
			componentǁcontent2_XMLǃn8ǃGRAPH,
			componentǁcontent2_XMLǃn9ǃGRAPH,
			componentǁcontent2_XMLǃn10ǃGRAPH,
			componentǁcontent2_XMLǃn11ǃGRAPH,
			componentǁcontent2_XMLǃn14_CN2ǃTEXT,
			componentǁcontent2_XMLǃn14_ENǃTEXT,
			componentǁcontent2_XMLǃn14_INǃTEXT,
			componentǁcontent2_XMLǃn14_JPǃTEXT,
			componentǁcontent2_XMLǃn14_KRǃTEXT,
			componentǁcontent2_XMLǃn14_THǃTEXT,
			componentǁcontent2_XMLǃn14_VNǃTEXT,
			componentǁcontent2_XMLǃn14ǃTEXT,
			componentǁcontent2_XMLǃn15_CN2ǃTEXT,
			componentǁcontent2_XMLǃn15_ENǃTEXT,
			componentǁcontent2_XMLǃn15_INǃTEXT,
			componentǁcontent2_XMLǃn15_JPǃTEXT,
			componentǁcontent2_XMLǃn15_KRǃTEXT,
			componentǁcontent2_XMLǃn15_THǃTEXT,
			componentǁcontent2_XMLǃn15_VNǃTEXT,
			componentǁcontent2_XMLǃn15ǃTEXT,
			componentǁcontent2_XMLǃn16_CN2ǃTEXT,
			componentǁcontent2_XMLǃn16_ENǃTEXT,
			componentǁcontent2_XMLǃn16_INǃTEXT,
			componentǁcontent2_XMLǃn16_JPǃTEXT,
			componentǁcontent2_XMLǃn16_KRǃTEXT,
			componentǁcontent2_XMLǃn16_THǃTEXT,
			componentǁcontent2_XMLǃn16_VNǃTEXT,
			componentǁcontent2_XMLǃn16ǃTEXT,
			componentǁcontent2_XMLǃn17ǃTEXT,
			componentǁcontent2_XMLǃn18ǃTEXT,
			componentǁcontent2_XMLǃn19ǃTEXT,
			componentǁcontent2_XMLǃn20ǃTEXT,
			componentǁcontent2_XMLǃn21ǃTEXT,
			componentǁcontent2_XMLǃn22ǃTEXT,
			componentǁcontent2_XMLǃn23ǃTEXT,
			componentǁcontent2_XMLǃn24ǃTEXT,
			componentǁcontent2_XMLǃn25ǃTEXT,
			componentǁcontent2_XMLǃn26ǃTEXT,
			componentǁcontent2_XMLǃn27_CN2ǃTEXT,
			componentǁcontent2_XMLǃn27_ENǃTEXT,
			componentǁcontent2_XMLǃn27_INǃTEXT,
			componentǁcontent2_XMLǃn27_JPǃTEXT,
			componentǁcontent2_XMLǃn27_KRǃTEXT,
			componentǁcontent2_XMLǃn27_THǃTEXT,
			componentǁcontent2_XMLǃn27_VNǃTEXT,
			componentǁcontent2_XMLǃn27ǃTEXT,
			componentǁcontent2_XMLǃn29_CN2ǃTEXT,
			componentǁcontent2_XMLǃn29_ENǃTEXT,
			componentǁcontent2_XMLǃn29_INǃTEXT,
			componentǁcontent2_XMLǃn29_JPǃTEXT,
			componentǁcontent2_XMLǃn29_KRǃTEXT,
			componentǁcontent2_XMLǃn29_THǃTEXT,
			componentǁcontent2_XMLǃn29_VNǃTEXT,
			componentǁcontent2_XMLǃn29ǃTEXT,
			componentǁcontent2_XMLǃn30_CN2ǃTEXT,
			componentǁcontent2_XMLǃn30_ENǃTEXT,
			componentǁcontent2_XMLǃn30_INǃTEXT,
			componentǁcontent2_XMLǃn30_JPǃTEXT,
			componentǁcontent2_XMLǃn30_KRǃTEXT,
			componentǁcontent2_XMLǃn30_THǃTEXT,
			componentǁcontent2_XMLǃn30_VNǃTEXT,
			componentǁcontent2_XMLǃn30ǃTEXT,
			componentǁcontent2_XMLǃn31_CN2ǃTEXT,
			componentǁcontent2_XMLǃn31_ENǃTEXT,
			componentǁcontent2_XMLǃn31_INǃTEXT,
			componentǁcontent2_XMLǃn31_JPǃTEXT,
			componentǁcontent2_XMLǃn31_KRǃTEXT,
			componentǁcontent2_XMLǃn31_THǃTEXT,
			componentǁcontent2_XMLǃn31_VNǃTEXT,
			componentǁcontent2_XMLǃn31ǃTEXT,
			componentǁcontent2_XMLǃn32_CN2ǃTEXT,
			componentǁcontent2_XMLǃn32_ENǃTEXT,
			componentǁcontent2_XMLǃn32_INǃTEXT,
			componentǁcontent2_XMLǃn32_JPǃTEXT,
			componentǁcontent2_XMLǃn32_KRǃTEXT,
			componentǁcontent2_XMLǃn32_THǃTEXT,
			componentǁcontent2_XMLǃn32_VNǃTEXT,
			componentǁcontent2_XMLǃn32ǃTEXT,
			componentǁcontent2_XMLǃn33_CN2ǃTEXT,
			componentǁcontent2_XMLǃn33_ENǃTEXT,
			componentǁcontent2_XMLǃn33_INǃTEXT,
			componentǁcontent2_XMLǃn33_JPǃTEXT,
			componentǁcontent2_XMLǃn33_KRǃTEXT,
			componentǁcontent2_XMLǃn33_THǃTEXT,
			componentǁcontent2_XMLǃn33_VNǃTEXT,
			componentǁcontent2_XMLǃn33ǃTEXT,
			componentǁcontent2_XMLǃn34_CN2ǃTEXT,
			componentǁcontent2_XMLǃn34_ENǃTEXT,
			componentǁcontent2_XMLǃn34_INǃTEXT,
			componentǁcontent2_XMLǃn34_JPǃTEXT,
			componentǁcontent2_XMLǃn34_KRǃTEXT,
			componentǁcontent2_XMLǃn34_THǃTEXT,
			componentǁcontent2_XMLǃn34_VNǃTEXT,
			componentǁcontent2_XMLǃn34ǃTEXT,
			componentǁcontent2_XMLǃn35_CN2ǃTEXT,
			componentǁcontent2_XMLǃn35_ENǃTEXT,
			componentǁcontent2_XMLǃn35_INǃTEXT,
			componentǁcontent2_XMLǃn35_JPǃTEXT,
			componentǁcontent2_XMLǃn35_KRǃTEXT,
			componentǁcontent2_XMLǃn35_THǃTEXT,
			componentǁcontent2_XMLǃn35_VNǃTEXT,
			componentǁcontent2_XMLǃn35ǃTEXT,
			componentǁcontent2_XMLǃn36_CN2ǃTEXT,
			componentǁcontent2_XMLǃn36_ENǃTEXT,
			componentǁcontent2_XMLǃn36_INǃTEXT,
			componentǁcontent2_XMLǃn36_JPǃTEXT,
			componentǁcontent2_XMLǃn36_KRǃTEXT,
			componentǁcontent2_XMLǃn36_THǃTEXT,
			componentǁcontent2_XMLǃn36_VNǃTEXT,
			componentǁcontent2_XMLǃn36ǃTEXT,
			componentǁcontent2_XMLǃn37_CN2ǃTEXT,
			componentǁcontent2_XMLǃn37_ENǃTEXT,
			componentǁcontent2_XMLǃn37_INǃTEXT,
			componentǁcontent2_XMLǃn37_JPǃTEXT,
			componentǁcontent2_XMLǃn37_KRǃTEXT,
			componentǁcontent2_XMLǃn37_THǃTEXT,
			componentǁcontent2_XMLǃn37_VNǃTEXT,
			componentǁcontent2_XMLǃn37ǃTEXT,
			componentǁcontent2_XMLǃn38_CN2ǃTEXT,
			componentǁcontent2_XMLǃn38_ENǃTEXT,
			componentǁcontent2_XMLǃn38_INǃTEXT,
			componentǁcontent2_XMLǃn38_JPǃTEXT,
			componentǁcontent2_XMLǃn38_KRǃTEXT,
			componentǁcontent2_XMLǃn38_THǃTEXT,
			componentǁcontent2_XMLǃn38_VNǃTEXT,
			componentǁcontent2_XMLǃn38ǃTEXT,
			componentǁcontent2_XMLǃn39_CN2ǃTEXT,
			componentǁcontent2_XMLǃn39_ENǃTEXT,
			componentǁcontent2_XMLǃn39_INǃTEXT,
			componentǁcontent2_XMLǃn39_JPǃTEXT,
			componentǁcontent2_XMLǃn39_KRǃTEXT,
			componentǁcontent2_XMLǃn39_THǃTEXT,
			componentǁcontent2_XMLǃn39_VNǃTEXT,
			componentǁcontent2_XMLǃn39ǃTEXT,
			componentǁcontent2_XMLǃn40_CN2ǃTEXT,
			componentǁcontent2_XMLǃn40_ENǃTEXT,
			componentǁcontent2_XMLǃn40_INǃTEXT,
			componentǁcontent2_XMLǃn40_JPǃTEXT,
			componentǁcontent2_XMLǃn40_KRǃTEXT,
			componentǁcontent2_XMLǃn40_THǃTEXT,
			componentǁcontent2_XMLǃn40_VNǃTEXT,
			componentǁcontent2_XMLǃn40ǃTEXT,
			componentǁcontent2_XMLǃn41_CN2ǃTEXT,
			componentǁcontent2_XMLǃn41_ENǃTEXT,
			componentǁcontent2_XMLǃn41_INǃTEXT,
			componentǁcontent2_XMLǃn41_JPǃTEXT,
			componentǁcontent2_XMLǃn41_KRǃTEXT,
			componentǁcontent2_XMLǃn41_THǃTEXT,
			componentǁcontent2_XMLǃn41_VNǃTEXT,
			componentǁcontent2_XMLǃn41ǃTEXT,
			componentǁcontent2_XMLǃn42_CN2ǃTEXT,
			componentǁcontent2_XMLǃn42_ENǃTEXT,
			componentǁcontent2_XMLǃn42_INǃTEXT,
			componentǁcontent2_XMLǃn42_JPǃTEXT,
			componentǁcontent2_XMLǃn42_KRǃTEXT,
			componentǁcontent2_XMLǃn42_THǃTEXT,
			componentǁcontent2_XMLǃn42_VNǃTEXT,
			componentǁcontent2_XMLǃn42ǃTEXT,
			componentǁcontent2_XMLǃn43_CN2ǃTEXT,
			componentǁcontent2_XMLǃn43_ENǃTEXT,
			componentǁcontent2_XMLǃn43_INǃTEXT,
			componentǁcontent2_XMLǃn43_JPǃTEXT,
			componentǁcontent2_XMLǃn43_KRǃTEXT,
			componentǁcontent2_XMLǃn43_THǃTEXT,
			componentǁcontent2_XMLǃn43_VNǃTEXT,
			componentǁcontent2_XMLǃn43ǃTEXT,
			componentǁcontent2_XMLǃn44_CN2ǃTEXT,
			componentǁcontent2_XMLǃn44_ENǃTEXT,
			componentǁcontent2_XMLǃn44_INǃTEXT,
			componentǁcontent2_XMLǃn44_JPǃTEXT,
			componentǁcontent2_XMLǃn44_KRǃTEXT,
			componentǁcontent2_XMLǃn44_THǃTEXT,
			componentǁcontent2_XMLǃn44_VNǃTEXT,
			componentǁcontent2_XMLǃn44ǃTEXT,
			componentǁcontent2_XMLǃn45_CN2ǃTEXT,
			componentǁcontent2_XMLǃn45_ENǃTEXT,
			componentǁcontent2_XMLǃn45_INǃTEXT,
			componentǁcontent2_XMLǃn45_JPǃTEXT,
			componentǁcontent2_XMLǃn45_KRǃTEXT,
			componentǁcontent2_XMLǃn45_THǃTEXT,
			componentǁcontent2_XMLǃn45_VNǃTEXT,
			componentǁcontent2_XMLǃn45ǃTEXT,
			componentǁcontent2_XMLǃn46_CN2ǃTEXT,
			componentǁcontent2_XMLǃn46_ENǃTEXT,
			componentǁcontent2_XMLǃn46_INǃTEXT,
			componentǁcontent2_XMLǃn46_JPǃTEXT,
			componentǁcontent2_XMLǃn46_KRǃTEXT,
			componentǁcontent2_XMLǃn46_THǃTEXT,
			componentǁcontent2_XMLǃn46_VNǃTEXT,
			componentǁcontent2_XMLǃn46ǃTEXT,
			componentǁcontent2_XMLǃn48_CN2ǃTEXT,
			componentǁcontent2_XMLǃn48_ENǃTEXT,
			componentǁcontent2_XMLǃn48_INǃTEXT,
			componentǁcontent2_XMLǃn48_JPǃTEXT,
			componentǁcontent2_XMLǃn48_KRǃTEXT,
			componentǁcontent2_XMLǃn48_THǃTEXT,
			componentǁcontent2_XMLǃn48_VNǃTEXT,
			componentǁcontent2_XMLǃn48ǃTEXT
		]
		getController(name: '__language'): componentǁcontent2_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): componentǁcontent2_XMLǃ__languageǃCONTROLLER
		_controllers: [
			componentǁcontent2_XMLǃ__languageǃCONTROLLER
		]
	}
	interface componentǁcontent2_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn3_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn3_ENǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn3_INǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn3_JPǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn3_KRǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn3_THǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn3_VNǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn3ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn5ǃGRAPH extends fairygui.GGraph{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn6ǃGRAPH extends fairygui.GGraph{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn7ǃGRAPH extends fairygui.GGraph{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn8ǃGRAPH extends fairygui.GGraph{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn9ǃGRAPH extends fairygui.GGraph{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn10ǃGRAPH extends fairygui.GGraph{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn11ǃGRAPH extends fairygui.GGraph{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn14_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn14_ENǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn14_INǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn14_JPǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn14_KRǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn14_THǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn14_VNǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn14ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn15_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn15_ENǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn15_INǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn15_JPǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn15_KRǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn15_THǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn15_VNǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn15ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn16_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn16_ENǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn16_INǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn16_JPǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn16_KRǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn16_THǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn16_VNǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn16ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn17ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn18ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn19ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn20ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn21ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn22ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn23ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn24ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn25ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn26ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn27_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn27_ENǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn27_INǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn27_JPǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn27_KRǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn27_THǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn27_VNǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn27ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn29_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn29_ENǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn29_INǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn29_JPǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn29_KRǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn29_THǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn29_VNǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn29ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn30_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn30_ENǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn30_INǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn30_JPǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn30_KRǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn30_THǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn30_VNǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn30ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn31_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn31_ENǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn31_INǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn31_JPǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn31_KRǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn31_THǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn31_VNǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn31ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn32_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn32_ENǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn32_INǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn32_JPǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn32_KRǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn32_THǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn32_VNǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn32ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn33_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn33_ENǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn33_INǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn33_JPǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn33_KRǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn33_THǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn33_VNǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn33ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn34_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn34_ENǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn34_INǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn34_JPǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn34_KRǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn34_THǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn34_VNǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn34ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn35_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn35_ENǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn35_INǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn35_JPǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn35_KRǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn35_THǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn35_VNǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn35ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn36_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn36_ENǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn36_INǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn36_JPǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn36_KRǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn36_THǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn36_VNǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn36ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn37_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn37_ENǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn37_INǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn37_JPǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn37_KRǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn37_THǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn37_VNǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn37ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn38_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn38_ENǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn38_INǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn38_JPǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn38_KRǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn38_THǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn38_VNǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn38ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn39_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn39_ENǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn39_INǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn39_JPǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn39_KRǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn39_THǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn39_VNǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn39ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn40_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn40_ENǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn40_INǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn40_JPǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn40_KRǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn40_THǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn40_VNǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn40ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn41_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn41_ENǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn41_INǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn41_JPǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn41_KRǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn41_THǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn41_VNǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn41ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn42_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn42_ENǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn42_INǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn42_JPǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn42_KRǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn42_THǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn42_VNǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn42ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn43_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn43_ENǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn43_INǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn43_JPǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn43_KRǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn43_THǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn43_VNǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn43ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn44_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn44_ENǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn44_INǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn44_JPǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn44_KRǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn44_THǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn44_VNǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn44ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn45_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn45_ENǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn45_INǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn45_JPǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn45_KRǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn45_THǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn45_VNǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn45ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn46_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn46_ENǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn46_INǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn46_JPǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn46_KRǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn46_THǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn46_VNǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn46ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn48_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn48_ENǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn48_INǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn48_JPǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn48_KRǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn48_THǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn48_VNǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn48ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁcontent2_XML
	}
	interface componentǁcontent2_XMLǃn9ǃCOMPONENT extends componentǁcontent2_XML{
		parent: help_XML
	}
	interface componentǁContent3_XML extends fairygui.GComponent{
		getChild(name: 'n12_CN2'): componentǁContent3_XMLǃn12_CN2ǃTEXT
		getChildAt(index: 0): componentǁContent3_XMLǃn12_CN2ǃTEXT
		getChildById(id: 'n12_j069_CN2'): componentǁContent3_XMLǃn12_CN2ǃTEXT
		getChild(name: 'n12_EN'): componentǁContent3_XMLǃn12_ENǃTEXT
		getChildAt(index: 1): componentǁContent3_XMLǃn12_ENǃTEXT
		getChildById(id: 'n12_j069_EN'): componentǁContent3_XMLǃn12_ENǃTEXT
		getChild(name: 'n12_IN'): componentǁContent3_XMLǃn12_INǃTEXT
		getChildAt(index: 2): componentǁContent3_XMLǃn12_INǃTEXT
		getChildById(id: 'n12_j069_IN'): componentǁContent3_XMLǃn12_INǃTEXT
		getChild(name: 'n12_JP'): componentǁContent3_XMLǃn12_JPǃTEXT
		getChildAt(index: 3): componentǁContent3_XMLǃn12_JPǃTEXT
		getChildById(id: 'n12_j069_JP'): componentǁContent3_XMLǃn12_JPǃTEXT
		getChild(name: 'n12_KR'): componentǁContent3_XMLǃn12_KRǃTEXT
		getChildAt(index: 4): componentǁContent3_XMLǃn12_KRǃTEXT
		getChildById(id: 'n12_j069_KR'): componentǁContent3_XMLǃn12_KRǃTEXT
		getChild(name: 'n12_TH'): componentǁContent3_XMLǃn12_THǃTEXT
		getChildAt(index: 5): componentǁContent3_XMLǃn12_THǃTEXT
		getChildById(id: 'n12_j069_TH'): componentǁContent3_XMLǃn12_THǃTEXT
		getChild(name: 'n12_VN'): componentǁContent3_XMLǃn12_VNǃTEXT
		getChildAt(index: 6): componentǁContent3_XMLǃn12_VNǃTEXT
		getChildById(id: 'n12_j069_VN'): componentǁContent3_XMLǃn12_VNǃTEXT
		getChild(name: 'n12'): componentǁContent3_XMLǃn12ǃTEXT
		getChildAt(index: 7): componentǁContent3_XMLǃn12ǃTEXT
		getChildById(id: 'n12_j069'): componentǁContent3_XMLǃn12ǃTEXT
		getChild(name: 'n13_CN2'): componentǁContent3_XMLǃn13_CN2ǃTEXT
		getChildAt(index: 8): componentǁContent3_XMLǃn13_CN2ǃTEXT
		getChildById(id: 'n13_j069_CN2'): componentǁContent3_XMLǃn13_CN2ǃTEXT
		getChild(name: 'n13_EN'): componentǁContent3_XMLǃn13_ENǃTEXT
		getChildAt(index: 9): componentǁContent3_XMLǃn13_ENǃTEXT
		getChildById(id: 'n13_j069_EN'): componentǁContent3_XMLǃn13_ENǃTEXT
		getChild(name: 'n13_IN'): componentǁContent3_XMLǃn13_INǃTEXT
		getChildAt(index: 10): componentǁContent3_XMLǃn13_INǃTEXT
		getChildById(id: 'n13_j069_IN'): componentǁContent3_XMLǃn13_INǃTEXT
		getChild(name: 'n13_JP'): componentǁContent3_XMLǃn13_JPǃTEXT
		getChildAt(index: 11): componentǁContent3_XMLǃn13_JPǃTEXT
		getChildById(id: 'n13_j069_JP'): componentǁContent3_XMLǃn13_JPǃTEXT
		getChild(name: 'n13_KR'): componentǁContent3_XMLǃn13_KRǃTEXT
		getChildAt(index: 12): componentǁContent3_XMLǃn13_KRǃTEXT
		getChildById(id: 'n13_j069_KR'): componentǁContent3_XMLǃn13_KRǃTEXT
		getChild(name: 'n13_TH'): componentǁContent3_XMLǃn13_THǃTEXT
		getChildAt(index: 13): componentǁContent3_XMLǃn13_THǃTEXT
		getChildById(id: 'n13_j069_TH'): componentǁContent3_XMLǃn13_THǃTEXT
		getChild(name: 'n13_VN'): componentǁContent3_XMLǃn13_VNǃTEXT
		getChildAt(index: 14): componentǁContent3_XMLǃn13_VNǃTEXT
		getChildById(id: 'n13_j069_VN'): componentǁContent3_XMLǃn13_VNǃTEXT
		getChild(name: 'n13'): componentǁContent3_XMLǃn13ǃTEXT
		getChildAt(index: 15): componentǁContent3_XMLǃn13ǃTEXT
		getChildById(id: 'n13_j069'): componentǁContent3_XMLǃn13ǃTEXT
		getChild(name: 'n4_CN2'): componentǁContent3_XMLǃn4_CN2ǃTEXT
		getChildAt(index: 16): componentǁContent3_XMLǃn4_CN2ǃTEXT
		getChildById(id: 'n4_j069_CN2'): componentǁContent3_XMLǃn4_CN2ǃTEXT
		getChild(name: 'n4_EN'): componentǁContent3_XMLǃn4_ENǃTEXT
		getChildAt(index: 17): componentǁContent3_XMLǃn4_ENǃTEXT
		getChildById(id: 'n4_j069_EN'): componentǁContent3_XMLǃn4_ENǃTEXT
		getChild(name: 'n4_IN'): componentǁContent3_XMLǃn4_INǃTEXT
		getChildAt(index: 18): componentǁContent3_XMLǃn4_INǃTEXT
		getChildById(id: 'n4_j069_IN'): componentǁContent3_XMLǃn4_INǃTEXT
		getChild(name: 'n4_JP'): componentǁContent3_XMLǃn4_JPǃTEXT
		getChildAt(index: 19): componentǁContent3_XMLǃn4_JPǃTEXT
		getChildById(id: 'n4_j069_JP'): componentǁContent3_XMLǃn4_JPǃTEXT
		getChild(name: 'n4_KR'): componentǁContent3_XMLǃn4_KRǃTEXT
		getChildAt(index: 20): componentǁContent3_XMLǃn4_KRǃTEXT
		getChildById(id: 'n4_j069_KR'): componentǁContent3_XMLǃn4_KRǃTEXT
		getChild(name: 'n4_TH'): componentǁContent3_XMLǃn4_THǃTEXT
		getChildAt(index: 21): componentǁContent3_XMLǃn4_THǃTEXT
		getChildById(id: 'n4_j069_TH'): componentǁContent3_XMLǃn4_THǃTEXT
		getChild(name: 'n4_VN'): componentǁContent3_XMLǃn4_VNǃTEXT
		getChildAt(index: 22): componentǁContent3_XMLǃn4_VNǃTEXT
		getChildById(id: 'n4_j069_VN'): componentǁContent3_XMLǃn4_VNǃTEXT
		getChild(name: 'n4'): componentǁContent3_XMLǃn4ǃTEXT
		getChildAt(index: 23): componentǁContent3_XMLǃn4ǃTEXT
		getChildById(id: 'n4_j069'): componentǁContent3_XMLǃn4ǃTEXT
		_children: [
			componentǁContent3_XMLǃn12_CN2ǃTEXT,
			componentǁContent3_XMLǃn12_ENǃTEXT,
			componentǁContent3_XMLǃn12_INǃTEXT,
			componentǁContent3_XMLǃn12_JPǃTEXT,
			componentǁContent3_XMLǃn12_KRǃTEXT,
			componentǁContent3_XMLǃn12_THǃTEXT,
			componentǁContent3_XMLǃn12_VNǃTEXT,
			componentǁContent3_XMLǃn12ǃTEXT,
			componentǁContent3_XMLǃn13_CN2ǃTEXT,
			componentǁContent3_XMLǃn13_ENǃTEXT,
			componentǁContent3_XMLǃn13_INǃTEXT,
			componentǁContent3_XMLǃn13_JPǃTEXT,
			componentǁContent3_XMLǃn13_KRǃTEXT,
			componentǁContent3_XMLǃn13_THǃTEXT,
			componentǁContent3_XMLǃn13_VNǃTEXT,
			componentǁContent3_XMLǃn13ǃTEXT,
			componentǁContent3_XMLǃn4_CN2ǃTEXT,
			componentǁContent3_XMLǃn4_ENǃTEXT,
			componentǁContent3_XMLǃn4_INǃTEXT,
			componentǁContent3_XMLǃn4_JPǃTEXT,
			componentǁContent3_XMLǃn4_KRǃTEXT,
			componentǁContent3_XMLǃn4_THǃTEXT,
			componentǁContent3_XMLǃn4_VNǃTEXT,
			componentǁContent3_XMLǃn4ǃTEXT
		]
		getController(name: '__language'): componentǁContent3_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): componentǁContent3_XMLǃ__languageǃCONTROLLER
		_controllers: [
			componentǁContent3_XMLǃ__languageǃCONTROLLER
		]
	}
	interface componentǁContent3_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: componentǁContent3_XML
	}
	interface componentǁContent3_XMLǃn12_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁContent3_XML
	}
	interface componentǁContent3_XMLǃn12_ENǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁContent3_XML
	}
	interface componentǁContent3_XMLǃn12_INǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁContent3_XML
	}
	interface componentǁContent3_XMLǃn12_JPǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁContent3_XML
	}
	interface componentǁContent3_XMLǃn12_KRǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁContent3_XML
	}
	interface componentǁContent3_XMLǃn12_THǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁContent3_XML
	}
	interface componentǁContent3_XMLǃn12_VNǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁContent3_XML
	}
	interface componentǁContent3_XMLǃn12ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁContent3_XML
	}
	interface componentǁContent3_XMLǃn13_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁContent3_XML
	}
	interface componentǁContent3_XMLǃn13_ENǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁContent3_XML
	}
	interface componentǁContent3_XMLǃn13_INǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁContent3_XML
	}
	interface componentǁContent3_XMLǃn13_JPǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁContent3_XML
	}
	interface componentǁContent3_XMLǃn13_KRǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁContent3_XML
	}
	interface componentǁContent3_XMLǃn13_THǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁContent3_XML
	}
	interface componentǁContent3_XMLǃn13_VNǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁContent3_XML
	}
	interface componentǁContent3_XMLǃn13ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁContent3_XML
	}
	interface componentǁContent3_XMLǃn4_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁContent3_XML
	}
	interface componentǁContent3_XMLǃn4_ENǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁContent3_XML
	}
	interface componentǁContent3_XMLǃn4_INǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁContent3_XML
	}
	interface componentǁContent3_XMLǃn4_JPǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁContent3_XML
	}
	interface componentǁContent3_XMLǃn4_KRǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁContent3_XML
	}
	interface componentǁContent3_XMLǃn4_THǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁContent3_XML
	}
	interface componentǁContent3_XMLǃn4_VNǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁContent3_XML
	}
	interface componentǁContent3_XMLǃn4ǃTEXT extends fairygui.GBasicTextField{
		parent: componentǁContent3_XML
	}
	interface componentǁContent3_XMLǃn10ǃCOMPONENT extends componentǁContent3_XML{
		parent: help_XML
	}
	interface buttonsǁhelpcontentscrollǁbutton_scroll_down_XML extends fairygui.GButton{
		getChild(name: 'n1'): buttonsǁhelpcontentscrollǁbutton_scroll_down_XMLǃn1ǃIMAGE
		getChildAt(index: 0): buttonsǁhelpcontentscrollǁbutton_scroll_down_XMLǃn1ǃIMAGE
		getChildById(id: 'n1'): buttonsǁhelpcontentscrollǁbutton_scroll_down_XMLǃn1ǃIMAGE
		_children: [
			buttonsǁhelpcontentscrollǁbutton_scroll_down_XMLǃn1ǃIMAGE
		]
		getController(name: '__language'): buttonsǁhelpcontentscrollǁbutton_scroll_down_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁhelpcontentscrollǁbutton_scroll_down_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁhelpcontentscrollǁbutton_scroll_down_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁhelpcontentscrollǁbutton_scroll_down_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁhelpcontentscrollǁbutton_scroll_down_XMLǃ__languageǃCONTROLLER,
			buttonsǁhelpcontentscrollǁbutton_scroll_down_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁhelpcontentscrollǁbutton_scroll_down_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁhelpcontentscrollǁbutton_scroll_down_XML
	}
	interface buttonsǁhelpcontentscrollǁbutton_scroll_down_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁhelpcontentscrollǁbutton_scroll_down_XML
	}
	interface buttonsǁhelpcontentscrollǁbutton_scroll_down_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁhelpcontentscrollǁbutton_scroll_down_XML
	}
	interface buttonsǁhelpcontentscrollǁbutton_scroll_down_XMLǃscroll_downǃCOMPONENT extends buttonsǁhelpcontentscrollǁbutton_scroll_down_XML{
		parent: help_XML
	}
	interface buttonsǁhelpcontentscrollǁbutton_scroll_up_XML extends fairygui.GButton{
		getChild(name: 'n1'): buttonsǁhelpcontentscrollǁbutton_scroll_up_XMLǃn1ǃIMAGE
		getChildAt(index: 0): buttonsǁhelpcontentscrollǁbutton_scroll_up_XMLǃn1ǃIMAGE
		getChildById(id: 'n1'): buttonsǁhelpcontentscrollǁbutton_scroll_up_XMLǃn1ǃIMAGE
		_children: [
			buttonsǁhelpcontentscrollǁbutton_scroll_up_XMLǃn1ǃIMAGE
		]
		getController(name: '__language'): buttonsǁhelpcontentscrollǁbutton_scroll_up_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁhelpcontentscrollǁbutton_scroll_up_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁhelpcontentscrollǁbutton_scroll_up_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁhelpcontentscrollǁbutton_scroll_up_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁhelpcontentscrollǁbutton_scroll_up_XMLǃ__languageǃCONTROLLER,
			buttonsǁhelpcontentscrollǁbutton_scroll_up_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁhelpcontentscrollǁbutton_scroll_up_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁhelpcontentscrollǁbutton_scroll_up_XML
	}
	interface buttonsǁhelpcontentscrollǁbutton_scroll_up_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁhelpcontentscrollǁbutton_scroll_up_XML
	}
	interface buttonsǁhelpcontentscrollǁbutton_scroll_up_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁhelpcontentscrollǁbutton_scroll_up_XML
	}
	interface buttonsǁhelpcontentscrollǁbutton_scroll_up_XMLǃscroll_upǃCOMPONENT extends buttonsǁhelpcontentscrollǁbutton_scroll_up_XML{
		parent: help_XML
	}
	interface buttonsǁbutton_guanbiluzhitu_XMLǃcloseǃCOMPONENT extends buttonsǁbutton_guanbiluzhitu_XML{
		parent: help_XML
	}
	interface help_XMLǃhelpǃCOMPONENT extends help_XML{
		parent: Main_XML
	}
	interface otherǁLeaderboard_XML extends fairygui.GComponent{
		getChild(name: 'n19'): mask_empty_XMLǃn19ǃCOMPONENT
		getChildAt(index: 0): mask_empty_XMLǃn19ǃCOMPONENT
		getChildById(id: 'n19_s20q'): mask_empty_XMLǃn19ǃCOMPONENT
		getChild(name: 'n1'): otherǁLeaderboard_XMLǃn1ǃIMAGE
		getChildAt(index: 1): otherǁLeaderboard_XMLǃn1ǃIMAGE
		getChildById(id: 'n1_h3lk'): otherǁLeaderboard_XMLǃn1ǃIMAGE
		getChild(name: 'n8_EN'): otherǁLeaderboard_XMLǃn8_ENǃIMAGE
		getChildAt(index: 2): otherǁLeaderboard_XMLǃn8_ENǃIMAGE
		getChildById(id: 'n8_puwk_EN'): otherǁLeaderboard_XMLǃn8_ENǃIMAGE
		getChild(name: 'n8_IN'): otherǁLeaderboard_XMLǃn8_INǃIMAGE
		getChildAt(index: 3): otherǁLeaderboard_XMLǃn8_INǃIMAGE
		getChildById(id: 'n8_puwk_IN'): otherǁLeaderboard_XMLǃn8_INǃIMAGE
		getChild(name: 'n8_JP'): otherǁLeaderboard_XMLǃn8_JPǃIMAGE
		getChildAt(index: 4): otherǁLeaderboard_XMLǃn8_JPǃIMAGE
		getChildById(id: 'n8_puwk_JP'): otherǁLeaderboard_XMLǃn8_JPǃIMAGE
		getChild(name: 'n8_KR'): otherǁLeaderboard_XMLǃn8_KRǃIMAGE
		getChildAt(index: 5): otherǁLeaderboard_XMLǃn8_KRǃIMAGE
		getChildById(id: 'n8_puwk_KR'): otherǁLeaderboard_XMLǃn8_KRǃIMAGE
		getChild(name: 'n8_TH'): otherǁLeaderboard_XMLǃn8_THǃIMAGE
		getChildAt(index: 6): otherǁLeaderboard_XMLǃn8_THǃIMAGE
		getChildById(id: 'n8_puwk_TH'): otherǁLeaderboard_XMLǃn8_THǃIMAGE
		getChild(name: 'n8_VN'): otherǁLeaderboard_XMLǃn8_VNǃIMAGE
		getChildAt(index: 7): otherǁLeaderboard_XMLǃn8_VNǃIMAGE
		getChildById(id: 'n8_puwk_VN'): otherǁLeaderboard_XMLǃn8_VNǃIMAGE
		getChild(name: 'n8_CN2'): otherǁLeaderboard_XMLǃn8_CN2ǃIMAGE
		getChildAt(index: 8): otherǁLeaderboard_XMLǃn8_CN2ǃIMAGE
		getChildById(id: 'n8_puwk_CN2'): otherǁLeaderboard_XMLǃn8_CN2ǃIMAGE
		getChild(name: 'n8'): otherǁLeaderboard_XMLǃn8ǃIMAGE
		getChildAt(index: 9): otherǁLeaderboard_XMLǃn8ǃIMAGE
		getChildById(id: 'n8_puwk'): otherǁLeaderboard_XMLǃn8ǃIMAGE
		getChild(name: 'n13'): otherǁLeaderboard_XMLǃn13ǃIMAGE
		getChildAt(index: 10): otherǁLeaderboard_XMLǃn13ǃIMAGE
		getChildById(id: 'n13_nu7k'): otherǁLeaderboard_XMLǃn13ǃIMAGE
		getChild(name: 'button_down'): buttonsǁlbǁbutton_lb_down_XMLǃbutton_downǃCOMPONENT
		getChildAt(index: 11): buttonsǁlbǁbutton_lb_down_XMLǃbutton_downǃCOMPONENT
		getChildById(id: 'n3_h3lk'): buttonsǁlbǁbutton_lb_down_XMLǃbutton_downǃCOMPONENT
		getChild(name: 'button_up'): buttonsǁlbǁbutton_lb_up_XMLǃbutton_upǃCOMPONENT
		getChildAt(index: 12): buttonsǁlbǁbutton_lb_up_XMLǃbutton_upǃCOMPONENT
		getChildById(id: 'n4_h3lk'): buttonsǁlbǁbutton_lb_up_XMLǃbutton_upǃCOMPONENT
		getChild(name: 'list'): otherǁLeaderboard_XMLǃlistǃLIST
		getChildAt(index: 13): otherǁLeaderboard_XMLǃlistǃLIST
		getChildById(id: 'n5_h3lk'): otherǁLeaderboard_XMLǃlistǃLIST
		getChild(name: 'list_rate'): otherǁLeaderboard_XMLǃlist_rateǃLIST
		getChildAt(index: 14): otherǁLeaderboard_XMLǃlist_rateǃLIST
		getChildById(id: 'n15_nu7k'): otherǁLeaderboard_XMLǃlist_rateǃLIST
		getChild(name: 'wupaiming_CN2'): otherǁLeaderboard_XMLǃwupaiming_CN2ǃTEXT
		getChildAt(index: 15): otherǁLeaderboard_XMLǃwupaiming_CN2ǃTEXT
		getChildById(id: 'n6_ww6c_CN2'): otherǁLeaderboard_XMLǃwupaiming_CN2ǃTEXT
		getChild(name: 'wupaiming_EN'): otherǁLeaderboard_XMLǃwupaiming_ENǃTEXT
		getChildAt(index: 16): otherǁLeaderboard_XMLǃwupaiming_ENǃTEXT
		getChildById(id: 'n6_ww6c_EN'): otherǁLeaderboard_XMLǃwupaiming_ENǃTEXT
		getChild(name: 'wupaiming_IN'): otherǁLeaderboard_XMLǃwupaiming_INǃTEXT
		getChildAt(index: 17): otherǁLeaderboard_XMLǃwupaiming_INǃTEXT
		getChildById(id: 'n6_ww6c_IN'): otherǁLeaderboard_XMLǃwupaiming_INǃTEXT
		getChild(name: 'wupaiming_JP'): otherǁLeaderboard_XMLǃwupaiming_JPǃTEXT
		getChildAt(index: 18): otherǁLeaderboard_XMLǃwupaiming_JPǃTEXT
		getChildById(id: 'n6_ww6c_JP'): otherǁLeaderboard_XMLǃwupaiming_JPǃTEXT
		getChild(name: 'wupaiming_KR'): otherǁLeaderboard_XMLǃwupaiming_KRǃTEXT
		getChildAt(index: 19): otherǁLeaderboard_XMLǃwupaiming_KRǃTEXT
		getChildById(id: 'n6_ww6c_KR'): otherǁLeaderboard_XMLǃwupaiming_KRǃTEXT
		getChild(name: 'wupaiming_TH'): otherǁLeaderboard_XMLǃwupaiming_THǃTEXT
		getChildAt(index: 20): otherǁLeaderboard_XMLǃwupaiming_THǃTEXT
		getChildById(id: 'n6_ww6c_TH'): otherǁLeaderboard_XMLǃwupaiming_THǃTEXT
		getChild(name: 'wupaiming_VN'): otherǁLeaderboard_XMLǃwupaiming_VNǃTEXT
		getChildAt(index: 21): otherǁLeaderboard_XMLǃwupaiming_VNǃTEXT
		getChildById(id: 'n6_ww6c_VN'): otherǁLeaderboard_XMLǃwupaiming_VNǃTEXT
		getChild(name: 'wupaiming'): otherǁLeaderboard_XMLǃwupaimingǃTEXT
		getChildAt(index: 22): otherǁLeaderboard_XMLǃwupaimingǃTEXT
		getChildById(id: 'n6_ww6c'): otherǁLeaderboard_XMLǃwupaimingǃTEXT
		getChild(name: 'btnYingkui'): buttonsǁlbǁButtonRank1_XMLǃbtnYingkuiǃCOMPONENT
		getChildAt(index: 23): buttonsǁlbǁButtonRank1_XMLǃbtnYingkuiǃCOMPONENT
		getChildById(id: 'n9_n5lc'): buttonsǁlbǁButtonRank1_XMLǃbtnYingkuiǃCOMPONENT
		getChild(name: 'btnZhongtou'): buttonsǁlbǁButtonRank2_XMLǃbtnZhongtouǃCOMPONENT
		getChildAt(index: 24): buttonsǁlbǁButtonRank2_XMLǃbtnZhongtouǃCOMPONENT
		getChildById(id: 'n10_n5lc'): buttonsǁlbǁButtonRank2_XMLǃbtnZhongtouǃCOMPONENT
		getChild(name: 'n11_CN2'): otherǁLeaderboard_XMLǃn11_CN2ǃTEXT
		getChildAt(index: 25): otherǁLeaderboard_XMLǃn11_CN2ǃTEXT
		getChildById(id: 'n11_nu7k_CN2'): otherǁLeaderboard_XMLǃn11_CN2ǃTEXT
		getChild(name: 'n11_EN'): otherǁLeaderboard_XMLǃn11_ENǃTEXT
		getChildAt(index: 26): otherǁLeaderboard_XMLǃn11_ENǃTEXT
		getChildById(id: 'n11_nu7k_EN'): otherǁLeaderboard_XMLǃn11_ENǃTEXT
		getChild(name: 'n11_IN'): otherǁLeaderboard_XMLǃn11_INǃTEXT
		getChildAt(index: 27): otherǁLeaderboard_XMLǃn11_INǃTEXT
		getChildById(id: 'n11_nu7k_IN'): otherǁLeaderboard_XMLǃn11_INǃTEXT
		getChild(name: 'n11_JP'): otherǁLeaderboard_XMLǃn11_JPǃTEXT
		getChildAt(index: 28): otherǁLeaderboard_XMLǃn11_JPǃTEXT
		getChildById(id: 'n11_nu7k_JP'): otherǁLeaderboard_XMLǃn11_JPǃTEXT
		getChild(name: 'n11_KR'): otherǁLeaderboard_XMLǃn11_KRǃTEXT
		getChildAt(index: 29): otherǁLeaderboard_XMLǃn11_KRǃTEXT
		getChildById(id: 'n11_nu7k_KR'): otherǁLeaderboard_XMLǃn11_KRǃTEXT
		getChild(name: 'n11_TH'): otherǁLeaderboard_XMLǃn11_THǃTEXT
		getChildAt(index: 30): otherǁLeaderboard_XMLǃn11_THǃTEXT
		getChildById(id: 'n11_nu7k_TH'): otherǁLeaderboard_XMLǃn11_THǃTEXT
		getChild(name: 'n11_VN'): otherǁLeaderboard_XMLǃn11_VNǃTEXT
		getChildAt(index: 31): otherǁLeaderboard_XMLǃn11_VNǃTEXT
		getChildById(id: 'n11_nu7k_VN'): otherǁLeaderboard_XMLǃn11_VNǃTEXT
		getChild(name: 'n11'): otherǁLeaderboard_XMLǃn11ǃTEXT
		getChildAt(index: 32): otherǁLeaderboard_XMLǃn11ǃTEXT
		getChildById(id: 'n11_nu7k'): otherǁLeaderboard_XMLǃn11ǃTEXT
		getChild(name: 'n12_CN2'): otherǁLeaderboard_XMLǃn12_CN2ǃTEXT
		getChildAt(index: 33): otherǁLeaderboard_XMLǃn12_CN2ǃTEXT
		getChildById(id: 'n12_nu7k_CN2'): otherǁLeaderboard_XMLǃn12_CN2ǃTEXT
		getChild(name: 'n12_EN'): otherǁLeaderboard_XMLǃn12_ENǃTEXT
		getChildAt(index: 34): otherǁLeaderboard_XMLǃn12_ENǃTEXT
		getChildById(id: 'n12_nu7k_EN'): otherǁLeaderboard_XMLǃn12_ENǃTEXT
		getChild(name: 'n12_IN'): otherǁLeaderboard_XMLǃn12_INǃTEXT
		getChildAt(index: 35): otherǁLeaderboard_XMLǃn12_INǃTEXT
		getChildById(id: 'n12_nu7k_IN'): otherǁLeaderboard_XMLǃn12_INǃTEXT
		getChild(name: 'n12_JP'): otherǁLeaderboard_XMLǃn12_JPǃTEXT
		getChildAt(index: 36): otherǁLeaderboard_XMLǃn12_JPǃTEXT
		getChildById(id: 'n12_nu7k_JP'): otherǁLeaderboard_XMLǃn12_JPǃTEXT
		getChild(name: 'n12_KR'): otherǁLeaderboard_XMLǃn12_KRǃTEXT
		getChildAt(index: 37): otherǁLeaderboard_XMLǃn12_KRǃTEXT
		getChildById(id: 'n12_nu7k_KR'): otherǁLeaderboard_XMLǃn12_KRǃTEXT
		getChild(name: 'n12_TH'): otherǁLeaderboard_XMLǃn12_THǃTEXT
		getChildAt(index: 38): otherǁLeaderboard_XMLǃn12_THǃTEXT
		getChildById(id: 'n12_nu7k_TH'): otherǁLeaderboard_XMLǃn12_THǃTEXT
		getChild(name: 'n12_VN'): otherǁLeaderboard_XMLǃn12_VNǃTEXT
		getChildAt(index: 39): otherǁLeaderboard_XMLǃn12_VNǃTEXT
		getChildById(id: 'n12_nu7k_VN'): otherǁLeaderboard_XMLǃn12_VNǃTEXT
		getChild(name: 'n12'): otherǁLeaderboard_XMLǃn12ǃTEXT
		getChildAt(index: 40): otherǁLeaderboard_XMLǃn12ǃTEXT
		getChildById(id: 'n12_nu7k'): otherǁLeaderboard_XMLǃn12ǃTEXT
		getChild(name: 'n14_CN2'): otherǁLeaderboard_XMLǃn14_CN2ǃTEXT
		getChildAt(index: 41): otherǁLeaderboard_XMLǃn14_CN2ǃTEXT
		getChildById(id: 'n14_nu7k_CN2'): otherǁLeaderboard_XMLǃn14_CN2ǃTEXT
		getChild(name: 'n14_EN'): otherǁLeaderboard_XMLǃn14_ENǃTEXT
		getChildAt(index: 42): otherǁLeaderboard_XMLǃn14_ENǃTEXT
		getChildById(id: 'n14_nu7k_EN'): otherǁLeaderboard_XMLǃn14_ENǃTEXT
		getChild(name: 'n14_IN'): otherǁLeaderboard_XMLǃn14_INǃTEXT
		getChildAt(index: 43): otherǁLeaderboard_XMLǃn14_INǃTEXT
		getChildById(id: 'n14_nu7k_IN'): otherǁLeaderboard_XMLǃn14_INǃTEXT
		getChild(name: 'n14_JP'): otherǁLeaderboard_XMLǃn14_JPǃTEXT
		getChildAt(index: 44): otherǁLeaderboard_XMLǃn14_JPǃTEXT
		getChildById(id: 'n14_nu7k_JP'): otherǁLeaderboard_XMLǃn14_JPǃTEXT
		getChild(name: 'n14_KR'): otherǁLeaderboard_XMLǃn14_KRǃTEXT
		getChildAt(index: 45): otherǁLeaderboard_XMLǃn14_KRǃTEXT
		getChildById(id: 'n14_nu7k_KR'): otherǁLeaderboard_XMLǃn14_KRǃTEXT
		getChild(name: 'n14_TH'): otherǁLeaderboard_XMLǃn14_THǃTEXT
		getChildAt(index: 46): otherǁLeaderboard_XMLǃn14_THǃTEXT
		getChildById(id: 'n14_nu7k_TH'): otherǁLeaderboard_XMLǃn14_THǃTEXT
		getChild(name: 'n14_VN'): otherǁLeaderboard_XMLǃn14_VNǃTEXT
		getChildAt(index: 47): otherǁLeaderboard_XMLǃn14_VNǃTEXT
		getChildById(id: 'n14_nu7k_VN'): otherǁLeaderboard_XMLǃn14_VNǃTEXT
		getChild(name: 'n14'): otherǁLeaderboard_XMLǃn14ǃTEXT
		getChildAt(index: 48): otherǁLeaderboard_XMLǃn14ǃTEXT
		getChildById(id: 'n14_nu7k'): otherǁLeaderboard_XMLǃn14ǃTEXT
		getChild(name: 'n18_CN2'): otherǁLeaderboard_XMLǃn18_CN2ǃTEXT
		getChildAt(index: 49): otherǁLeaderboard_XMLǃn18_CN2ǃTEXT
		getChildById(id: 'n18_nu7k_CN2'): otherǁLeaderboard_XMLǃn18_CN2ǃTEXT
		getChild(name: 'n18_EN'): otherǁLeaderboard_XMLǃn18_ENǃTEXT
		getChildAt(index: 50): otherǁLeaderboard_XMLǃn18_ENǃTEXT
		getChildById(id: 'n18_nu7k_EN'): otherǁLeaderboard_XMLǃn18_ENǃTEXT
		getChild(name: 'n18_IN'): otherǁLeaderboard_XMLǃn18_INǃTEXT
		getChildAt(index: 51): otherǁLeaderboard_XMLǃn18_INǃTEXT
		getChildById(id: 'n18_nu7k_IN'): otherǁLeaderboard_XMLǃn18_INǃTEXT
		getChild(name: 'n18_JP'): otherǁLeaderboard_XMLǃn18_JPǃTEXT
		getChildAt(index: 52): otherǁLeaderboard_XMLǃn18_JPǃTEXT
		getChildById(id: 'n18_nu7k_JP'): otherǁLeaderboard_XMLǃn18_JPǃTEXT
		getChild(name: 'n18_KR'): otherǁLeaderboard_XMLǃn18_KRǃTEXT
		getChildAt(index: 53): otherǁLeaderboard_XMLǃn18_KRǃTEXT
		getChildById(id: 'n18_nu7k_KR'): otherǁLeaderboard_XMLǃn18_KRǃTEXT
		getChild(name: 'n18_TH'): otherǁLeaderboard_XMLǃn18_THǃTEXT
		getChildAt(index: 54): otherǁLeaderboard_XMLǃn18_THǃTEXT
		getChildById(id: 'n18_nu7k_TH'): otherǁLeaderboard_XMLǃn18_THǃTEXT
		getChild(name: 'n18_VN'): otherǁLeaderboard_XMLǃn18_VNǃTEXT
		getChildAt(index: 55): otherǁLeaderboard_XMLǃn18_VNǃTEXT
		getChildById(id: 'n18_nu7k_VN'): otherǁLeaderboard_XMLǃn18_VNǃTEXT
		getChild(name: 'n18'): otherǁLeaderboard_XMLǃn18ǃTEXT
		getChildAt(index: 56): otherǁLeaderboard_XMLǃn18ǃTEXT
		getChildById(id: 'n18_nu7k'): otherǁLeaderboard_XMLǃn18ǃTEXT
		_children: [
			mask_empty_XMLǃn19ǃCOMPONENT,
			otherǁLeaderboard_XMLǃn1ǃIMAGE,
			otherǁLeaderboard_XMLǃn8_ENǃIMAGE,
			otherǁLeaderboard_XMLǃn8_INǃIMAGE,
			otherǁLeaderboard_XMLǃn8_JPǃIMAGE,
			otherǁLeaderboard_XMLǃn8_KRǃIMAGE,
			otherǁLeaderboard_XMLǃn8_THǃIMAGE,
			otherǁLeaderboard_XMLǃn8_VNǃIMAGE,
			otherǁLeaderboard_XMLǃn8_CN2ǃIMAGE,
			otherǁLeaderboard_XMLǃn8ǃIMAGE,
			otherǁLeaderboard_XMLǃn13ǃIMAGE,
			buttonsǁlbǁbutton_lb_down_XMLǃbutton_downǃCOMPONENT,
			buttonsǁlbǁbutton_lb_up_XMLǃbutton_upǃCOMPONENT,
			otherǁLeaderboard_XMLǃlistǃLIST,
			otherǁLeaderboard_XMLǃlist_rateǃLIST,
			otherǁLeaderboard_XMLǃwupaiming_CN2ǃTEXT,
			otherǁLeaderboard_XMLǃwupaiming_ENǃTEXT,
			otherǁLeaderboard_XMLǃwupaiming_INǃTEXT,
			otherǁLeaderboard_XMLǃwupaiming_JPǃTEXT,
			otherǁLeaderboard_XMLǃwupaiming_KRǃTEXT,
			otherǁLeaderboard_XMLǃwupaiming_THǃTEXT,
			otherǁLeaderboard_XMLǃwupaiming_VNǃTEXT,
			otherǁLeaderboard_XMLǃwupaimingǃTEXT,
			buttonsǁlbǁButtonRank1_XMLǃbtnYingkuiǃCOMPONENT,
			buttonsǁlbǁButtonRank2_XMLǃbtnZhongtouǃCOMPONENT,
			otherǁLeaderboard_XMLǃn11_CN2ǃTEXT,
			otherǁLeaderboard_XMLǃn11_ENǃTEXT,
			otherǁLeaderboard_XMLǃn11_INǃTEXT,
			otherǁLeaderboard_XMLǃn11_JPǃTEXT,
			otherǁLeaderboard_XMLǃn11_KRǃTEXT,
			otherǁLeaderboard_XMLǃn11_THǃTEXT,
			otherǁLeaderboard_XMLǃn11_VNǃTEXT,
			otherǁLeaderboard_XMLǃn11ǃTEXT,
			otherǁLeaderboard_XMLǃn12_CN2ǃTEXT,
			otherǁLeaderboard_XMLǃn12_ENǃTEXT,
			otherǁLeaderboard_XMLǃn12_INǃTEXT,
			otherǁLeaderboard_XMLǃn12_JPǃTEXT,
			otherǁLeaderboard_XMLǃn12_KRǃTEXT,
			otherǁLeaderboard_XMLǃn12_THǃTEXT,
			otherǁLeaderboard_XMLǃn12_VNǃTEXT,
			otherǁLeaderboard_XMLǃn12ǃTEXT,
			otherǁLeaderboard_XMLǃn14_CN2ǃTEXT,
			otherǁLeaderboard_XMLǃn14_ENǃTEXT,
			otherǁLeaderboard_XMLǃn14_INǃTEXT,
			otherǁLeaderboard_XMLǃn14_JPǃTEXT,
			otherǁLeaderboard_XMLǃn14_KRǃTEXT,
			otherǁLeaderboard_XMLǃn14_THǃTEXT,
			otherǁLeaderboard_XMLǃn14_VNǃTEXT,
			otherǁLeaderboard_XMLǃn14ǃTEXT,
			otherǁLeaderboard_XMLǃn18_CN2ǃTEXT,
			otherǁLeaderboard_XMLǃn18_ENǃTEXT,
			otherǁLeaderboard_XMLǃn18_INǃTEXT,
			otherǁLeaderboard_XMLǃn18_JPǃTEXT,
			otherǁLeaderboard_XMLǃn18_KRǃTEXT,
			otherǁLeaderboard_XMLǃn18_THǃTEXT,
			otherǁLeaderboard_XMLǃn18_VNǃTEXT,
			otherǁLeaderboard_XMLǃn18ǃTEXT
		]
		getController(name: '__language'): otherǁLeaderboard_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): otherǁLeaderboard_XMLǃ__languageǃCONTROLLER
		getController(name: 'c1'): otherǁLeaderboard_XMLǃc1ǃCONTROLLER
		getControllerAt(index: 1): otherǁLeaderboard_XMLǃc1ǃCONTROLLER
		_controllers: [
			otherǁLeaderboard_XMLǃ__languageǃCONTROLLER,
			otherǁLeaderboard_XMLǃc1ǃCONTROLLER
		]
	}
	interface otherǁLeaderboard_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃc1ǃCONTROLLER extends fairygui.Controller{
		_parent: otherǁLeaderboard_XML
	}
	interface mask_empty_XMLǃn19ǃCOMPONENT extends mask_empty_XML{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn8_ENǃIMAGE extends fairygui.GImage{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn8_INǃIMAGE extends fairygui.GImage{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn8_JPǃIMAGE extends fairygui.GImage{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn8_KRǃIMAGE extends fairygui.GImage{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn8_THǃIMAGE extends fairygui.GImage{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn8_VNǃIMAGE extends fairygui.GImage{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn8_CN2ǃIMAGE extends fairygui.GImage{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn8ǃIMAGE extends fairygui.GImage{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn13ǃIMAGE extends fairygui.GImage{
		parent: otherǁLeaderboard_XML
	}
	interface buttonsǁlbǁbutton_lb_down_XML extends fairygui.GButton{
		getChild(name: 'n1'): buttonsǁlbǁbutton_lb_down_XMLǃn1ǃIMAGE
		getChildAt(index: 0): buttonsǁlbǁbutton_lb_down_XMLǃn1ǃIMAGE
		getChildById(id: 'n1'): buttonsǁlbǁbutton_lb_down_XMLǃn1ǃIMAGE
		_children: [
			buttonsǁlbǁbutton_lb_down_XMLǃn1ǃIMAGE
		]
		getController(name: '__language'): buttonsǁlbǁbutton_lb_down_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁlbǁbutton_lb_down_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁlbǁbutton_lb_down_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁlbǁbutton_lb_down_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁlbǁbutton_lb_down_XMLǃ__languageǃCONTROLLER,
			buttonsǁlbǁbutton_lb_down_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁlbǁbutton_lb_down_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁlbǁbutton_lb_down_XML
	}
	interface buttonsǁlbǁbutton_lb_down_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁlbǁbutton_lb_down_XML
	}
	interface buttonsǁlbǁbutton_lb_down_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁlbǁbutton_lb_down_XML
	}
	interface buttonsǁlbǁbutton_lb_down_XMLǃbutton_downǃCOMPONENT extends buttonsǁlbǁbutton_lb_down_XML{
		parent: otherǁLeaderboard_XML
	}
	interface buttonsǁlbǁbutton_lb_up_XML extends fairygui.GButton{
		getChild(name: 'n1'): buttonsǁlbǁbutton_lb_up_XMLǃn1ǃIMAGE
		getChildAt(index: 0): buttonsǁlbǁbutton_lb_up_XMLǃn1ǃIMAGE
		getChildById(id: 'n1'): buttonsǁlbǁbutton_lb_up_XMLǃn1ǃIMAGE
		_children: [
			buttonsǁlbǁbutton_lb_up_XMLǃn1ǃIMAGE
		]
		getController(name: '__language'): buttonsǁlbǁbutton_lb_up_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁlbǁbutton_lb_up_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁlbǁbutton_lb_up_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁlbǁbutton_lb_up_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁlbǁbutton_lb_up_XMLǃ__languageǃCONTROLLER,
			buttonsǁlbǁbutton_lb_up_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁlbǁbutton_lb_up_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁlbǁbutton_lb_up_XML
	}
	interface buttonsǁlbǁbutton_lb_up_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁlbǁbutton_lb_up_XML
	}
	interface buttonsǁlbǁbutton_lb_up_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁlbǁbutton_lb_up_XML
	}
	interface buttonsǁlbǁbutton_lb_up_XMLǃbutton_upǃCOMPONENT extends buttonsǁlbǁbutton_lb_up_XML{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃlistǃLIST extends fairygui.GList{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃlist_rateǃLIST extends fairygui.GList{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃwupaiming_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃwupaiming_ENǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃwupaiming_INǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃwupaiming_JPǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃwupaiming_KRǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃwupaiming_THǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃwupaiming_VNǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃwupaimingǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface buttonsǁlbǁButtonRank1_XML extends fairygui.GButton{
		getChild(name: 'n0'): buttonsǁlbǁButtonRank1_XMLǃn0ǃIMAGE
		getChildAt(index: 0): buttonsǁlbǁButtonRank1_XMLǃn0ǃIMAGE
		getChildById(id: 'n0_n5lc'): buttonsǁlbǁButtonRank1_XMLǃn0ǃIMAGE
		getChild(name: 'n1'): buttonsǁlbǁButtonRank1_XMLǃn1ǃIMAGE
		getChildAt(index: 1): buttonsǁlbǁButtonRank1_XMLǃn1ǃIMAGE
		getChildById(id: 'n1_n5lc'): buttonsǁlbǁButtonRank1_XMLǃn1ǃIMAGE
		getChild(name: 'n2_CN2'): buttonsǁlbǁButtonRank1_XMLǃn2_CN2ǃTEXT
		getChildAt(index: 2): buttonsǁlbǁButtonRank1_XMLǃn2_CN2ǃTEXT
		getChildById(id: 'n2_n5lc_CN2'): buttonsǁlbǁButtonRank1_XMLǃn2_CN2ǃTEXT
		getChild(name: 'n2_EN'): buttonsǁlbǁButtonRank1_XMLǃn2_ENǃTEXT
		getChildAt(index: 3): buttonsǁlbǁButtonRank1_XMLǃn2_ENǃTEXT
		getChildById(id: 'n2_n5lc_EN'): buttonsǁlbǁButtonRank1_XMLǃn2_ENǃTEXT
		getChild(name: 'n2_IN'): buttonsǁlbǁButtonRank1_XMLǃn2_INǃTEXT
		getChildAt(index: 4): buttonsǁlbǁButtonRank1_XMLǃn2_INǃTEXT
		getChildById(id: 'n2_n5lc_IN'): buttonsǁlbǁButtonRank1_XMLǃn2_INǃTEXT
		getChild(name: 'n2_JP'): buttonsǁlbǁButtonRank1_XMLǃn2_JPǃTEXT
		getChildAt(index: 5): buttonsǁlbǁButtonRank1_XMLǃn2_JPǃTEXT
		getChildById(id: 'n2_n5lc_JP'): buttonsǁlbǁButtonRank1_XMLǃn2_JPǃTEXT
		getChild(name: 'n2_KR'): buttonsǁlbǁButtonRank1_XMLǃn2_KRǃTEXT
		getChildAt(index: 6): buttonsǁlbǁButtonRank1_XMLǃn2_KRǃTEXT
		getChildById(id: 'n2_n5lc_KR'): buttonsǁlbǁButtonRank1_XMLǃn2_KRǃTEXT
		getChild(name: 'n2_TH'): buttonsǁlbǁButtonRank1_XMLǃn2_THǃTEXT
		getChildAt(index: 7): buttonsǁlbǁButtonRank1_XMLǃn2_THǃTEXT
		getChildById(id: 'n2_n5lc_TH'): buttonsǁlbǁButtonRank1_XMLǃn2_THǃTEXT
		getChild(name: 'n2_VN'): buttonsǁlbǁButtonRank1_XMLǃn2_VNǃTEXT
		getChildAt(index: 8): buttonsǁlbǁButtonRank1_XMLǃn2_VNǃTEXT
		getChildById(id: 'n2_n5lc_VN'): buttonsǁlbǁButtonRank1_XMLǃn2_VNǃTEXT
		getChild(name: 'n2'): buttonsǁlbǁButtonRank1_XMLǃn2ǃTEXT
		getChildAt(index: 9): buttonsǁlbǁButtonRank1_XMLǃn2ǃTEXT
		getChildById(id: 'n2_n5lc'): buttonsǁlbǁButtonRank1_XMLǃn2ǃTEXT
		_children: [
			buttonsǁlbǁButtonRank1_XMLǃn0ǃIMAGE,
			buttonsǁlbǁButtonRank1_XMLǃn1ǃIMAGE,
			buttonsǁlbǁButtonRank1_XMLǃn2_CN2ǃTEXT,
			buttonsǁlbǁButtonRank1_XMLǃn2_ENǃTEXT,
			buttonsǁlbǁButtonRank1_XMLǃn2_INǃTEXT,
			buttonsǁlbǁButtonRank1_XMLǃn2_JPǃTEXT,
			buttonsǁlbǁButtonRank1_XMLǃn2_KRǃTEXT,
			buttonsǁlbǁButtonRank1_XMLǃn2_THǃTEXT,
			buttonsǁlbǁButtonRank1_XMLǃn2_VNǃTEXT,
			buttonsǁlbǁButtonRank1_XMLǃn2ǃTEXT
		]
		getController(name: '__language'): buttonsǁlbǁButtonRank1_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁlbǁButtonRank1_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁlbǁButtonRank1_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁlbǁButtonRank1_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁlbǁButtonRank1_XMLǃ__languageǃCONTROLLER,
			buttonsǁlbǁButtonRank1_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁlbǁButtonRank1_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁlbǁButtonRank1_XML
	}
	interface buttonsǁlbǁButtonRank1_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁlbǁButtonRank1_XML
	}
	interface buttonsǁlbǁButtonRank1_XMLǃn0ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁlbǁButtonRank1_XML
	}
	interface buttonsǁlbǁButtonRank1_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁlbǁButtonRank1_XML
	}
	interface buttonsǁlbǁButtonRank1_XMLǃn2_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁlbǁButtonRank1_XML
	}
	interface buttonsǁlbǁButtonRank1_XMLǃn2_ENǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁlbǁButtonRank1_XML
	}
	interface buttonsǁlbǁButtonRank1_XMLǃn2_INǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁlbǁButtonRank1_XML
	}
	interface buttonsǁlbǁButtonRank1_XMLǃn2_JPǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁlbǁButtonRank1_XML
	}
	interface buttonsǁlbǁButtonRank1_XMLǃn2_KRǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁlbǁButtonRank1_XML
	}
	interface buttonsǁlbǁButtonRank1_XMLǃn2_THǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁlbǁButtonRank1_XML
	}
	interface buttonsǁlbǁButtonRank1_XMLǃn2_VNǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁlbǁButtonRank1_XML
	}
	interface buttonsǁlbǁButtonRank1_XMLǃn2ǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁlbǁButtonRank1_XML
	}
	interface buttonsǁlbǁButtonRank1_XMLǃbtnYingkuiǃCOMPONENT extends buttonsǁlbǁButtonRank1_XML{
		parent: otherǁLeaderboard_XML
	}
	interface buttonsǁlbǁButtonRank2_XML extends fairygui.GButton{
		getChild(name: 'n0'): buttonsǁlbǁButtonRank2_XMLǃn0ǃIMAGE
		getChildAt(index: 0): buttonsǁlbǁButtonRank2_XMLǃn0ǃIMAGE
		getChildById(id: 'n0_n5lc'): buttonsǁlbǁButtonRank2_XMLǃn0ǃIMAGE
		getChild(name: 'n1'): buttonsǁlbǁButtonRank2_XMLǃn1ǃIMAGE
		getChildAt(index: 1): buttonsǁlbǁButtonRank2_XMLǃn1ǃIMAGE
		getChildById(id: 'n1_n5lc'): buttonsǁlbǁButtonRank2_XMLǃn1ǃIMAGE
		getChild(name: 'n2_CN2'): buttonsǁlbǁButtonRank2_XMLǃn2_CN2ǃTEXT
		getChildAt(index: 2): buttonsǁlbǁButtonRank2_XMLǃn2_CN2ǃTEXT
		getChildById(id: 'n2_n5lc_CN2'): buttonsǁlbǁButtonRank2_XMLǃn2_CN2ǃTEXT
		getChild(name: 'n2_EN'): buttonsǁlbǁButtonRank2_XMLǃn2_ENǃTEXT
		getChildAt(index: 3): buttonsǁlbǁButtonRank2_XMLǃn2_ENǃTEXT
		getChildById(id: 'n2_n5lc_EN'): buttonsǁlbǁButtonRank2_XMLǃn2_ENǃTEXT
		getChild(name: 'n2_IN'): buttonsǁlbǁButtonRank2_XMLǃn2_INǃTEXT
		getChildAt(index: 4): buttonsǁlbǁButtonRank2_XMLǃn2_INǃTEXT
		getChildById(id: 'n2_n5lc_IN'): buttonsǁlbǁButtonRank2_XMLǃn2_INǃTEXT
		getChild(name: 'n2_JP'): buttonsǁlbǁButtonRank2_XMLǃn2_JPǃTEXT
		getChildAt(index: 5): buttonsǁlbǁButtonRank2_XMLǃn2_JPǃTEXT
		getChildById(id: 'n2_n5lc_JP'): buttonsǁlbǁButtonRank2_XMLǃn2_JPǃTEXT
		getChild(name: 'n2_KR'): buttonsǁlbǁButtonRank2_XMLǃn2_KRǃTEXT
		getChildAt(index: 6): buttonsǁlbǁButtonRank2_XMLǃn2_KRǃTEXT
		getChildById(id: 'n2_n5lc_KR'): buttonsǁlbǁButtonRank2_XMLǃn2_KRǃTEXT
		getChild(name: 'n2_TH'): buttonsǁlbǁButtonRank2_XMLǃn2_THǃTEXT
		getChildAt(index: 7): buttonsǁlbǁButtonRank2_XMLǃn2_THǃTEXT
		getChildById(id: 'n2_n5lc_TH'): buttonsǁlbǁButtonRank2_XMLǃn2_THǃTEXT
		getChild(name: 'n2_VN'): buttonsǁlbǁButtonRank2_XMLǃn2_VNǃTEXT
		getChildAt(index: 8): buttonsǁlbǁButtonRank2_XMLǃn2_VNǃTEXT
		getChildById(id: 'n2_n5lc_VN'): buttonsǁlbǁButtonRank2_XMLǃn2_VNǃTEXT
		getChild(name: 'n2'): buttonsǁlbǁButtonRank2_XMLǃn2ǃTEXT
		getChildAt(index: 9): buttonsǁlbǁButtonRank2_XMLǃn2ǃTEXT
		getChildById(id: 'n2_n5lc'): buttonsǁlbǁButtonRank2_XMLǃn2ǃTEXT
		_children: [
			buttonsǁlbǁButtonRank2_XMLǃn0ǃIMAGE,
			buttonsǁlbǁButtonRank2_XMLǃn1ǃIMAGE,
			buttonsǁlbǁButtonRank2_XMLǃn2_CN2ǃTEXT,
			buttonsǁlbǁButtonRank2_XMLǃn2_ENǃTEXT,
			buttonsǁlbǁButtonRank2_XMLǃn2_INǃTEXT,
			buttonsǁlbǁButtonRank2_XMLǃn2_JPǃTEXT,
			buttonsǁlbǁButtonRank2_XMLǃn2_KRǃTEXT,
			buttonsǁlbǁButtonRank2_XMLǃn2_THǃTEXT,
			buttonsǁlbǁButtonRank2_XMLǃn2_VNǃTEXT,
			buttonsǁlbǁButtonRank2_XMLǃn2ǃTEXT
		]
		getController(name: '__language'): buttonsǁlbǁButtonRank2_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁlbǁButtonRank2_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁlbǁButtonRank2_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁlbǁButtonRank2_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁlbǁButtonRank2_XMLǃ__languageǃCONTROLLER,
			buttonsǁlbǁButtonRank2_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁlbǁButtonRank2_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁlbǁButtonRank2_XML
	}
	interface buttonsǁlbǁButtonRank2_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁlbǁButtonRank2_XML
	}
	interface buttonsǁlbǁButtonRank2_XMLǃn0ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁlbǁButtonRank2_XML
	}
	interface buttonsǁlbǁButtonRank2_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁlbǁButtonRank2_XML
	}
	interface buttonsǁlbǁButtonRank2_XMLǃn2_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁlbǁButtonRank2_XML
	}
	interface buttonsǁlbǁButtonRank2_XMLǃn2_ENǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁlbǁButtonRank2_XML
	}
	interface buttonsǁlbǁButtonRank2_XMLǃn2_INǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁlbǁButtonRank2_XML
	}
	interface buttonsǁlbǁButtonRank2_XMLǃn2_JPǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁlbǁButtonRank2_XML
	}
	interface buttonsǁlbǁButtonRank2_XMLǃn2_KRǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁlbǁButtonRank2_XML
	}
	interface buttonsǁlbǁButtonRank2_XMLǃn2_THǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁlbǁButtonRank2_XML
	}
	interface buttonsǁlbǁButtonRank2_XMLǃn2_VNǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁlbǁButtonRank2_XML
	}
	interface buttonsǁlbǁButtonRank2_XMLǃn2ǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁlbǁButtonRank2_XML
	}
	interface buttonsǁlbǁButtonRank2_XMLǃbtnZhongtouǃCOMPONENT extends buttonsǁlbǁButtonRank2_XML{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn11_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn11_ENǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn11_INǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn11_JPǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn11_KRǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn11_THǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn11_VNǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn11ǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn12_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn12_ENǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn12_INǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn12_JPǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn12_KRǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn12_THǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn12_VNǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn12ǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn14_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn14_ENǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn14_INǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn14_JPǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn14_KRǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn14_THǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn14_VNǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn14ǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn18_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn18_ENǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn18_INǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn18_JPǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn18_KRǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn18_THǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn18_VNǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃn18ǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁLeaderboard_XML
	}
	interface otherǁLeaderboard_XMLǃleaderboardǃCOMPONENT extends otherǁLeaderboard_XML{
		parent: Main_XML
	}
	interface buttonsǁautobetDialog_XML extends fairygui.GButton{
		getChild(name: 'n4'): buttonsǁautobetDialog_XMLǃn4ǃIMAGE
		getChildAt(index: 0): buttonsǁautobetDialog_XMLǃn4ǃIMAGE
		getChildById(id: 'n4_pwo9'): buttonsǁautobetDialog_XMLǃn4ǃIMAGE
		getChild(name: 'n10_EN'): buttonsǁautobetDialog_XMLǃn10_ENǃIMAGE
		getChildAt(index: 1): buttonsǁautobetDialog_XMLǃn10_ENǃIMAGE
		getChildById(id: 'n10_n11q_EN'): buttonsǁautobetDialog_XMLǃn10_ENǃIMAGE
		getChild(name: 'n10_IN'): buttonsǁautobetDialog_XMLǃn10_INǃIMAGE
		getChildAt(index: 2): buttonsǁautobetDialog_XMLǃn10_INǃIMAGE
		getChildById(id: 'n10_n11q_IN'): buttonsǁautobetDialog_XMLǃn10_INǃIMAGE
		getChild(name: 'n10_JP'): buttonsǁautobetDialog_XMLǃn10_JPǃIMAGE
		getChildAt(index: 3): buttonsǁautobetDialog_XMLǃn10_JPǃIMAGE
		getChildById(id: 'n10_n11q_JP'): buttonsǁautobetDialog_XMLǃn10_JPǃIMAGE
		getChild(name: 'n10_KR'): buttonsǁautobetDialog_XMLǃn10_KRǃIMAGE
		getChildAt(index: 4): buttonsǁautobetDialog_XMLǃn10_KRǃIMAGE
		getChildById(id: 'n10_n11q_KR'): buttonsǁautobetDialog_XMLǃn10_KRǃIMAGE
		getChild(name: 'n10_TH'): buttonsǁautobetDialog_XMLǃn10_THǃIMAGE
		getChildAt(index: 5): buttonsǁautobetDialog_XMLǃn10_THǃIMAGE
		getChildById(id: 'n10_n11q_TH'): buttonsǁautobetDialog_XMLǃn10_THǃIMAGE
		getChild(name: 'n10_VN'): buttonsǁautobetDialog_XMLǃn10_VNǃIMAGE
		getChildAt(index: 6): buttonsǁautobetDialog_XMLǃn10_VNǃIMAGE
		getChildById(id: 'n10_n11q_VN'): buttonsǁautobetDialog_XMLǃn10_VNǃIMAGE
		getChild(name: 'n10_CN2'): buttonsǁautobetDialog_XMLǃn10_CN2ǃIMAGE
		getChildAt(index: 7): buttonsǁautobetDialog_XMLǃn10_CN2ǃIMAGE
		getChildById(id: 'n10_n11q_CN2'): buttonsǁautobetDialog_XMLǃn10_CN2ǃIMAGE
		getChild(name: 'n10'): buttonsǁautobetDialog_XMLǃn10ǃIMAGE
		getChildAt(index: 8): buttonsǁautobetDialog_XMLǃn10ǃIMAGE
		getChildById(id: 'n10_n11q'): buttonsǁautobetDialog_XMLǃn10ǃIMAGE
		getChild(name: 'content_CN2'): buttonsǁautobetDialog_XMLǃcontent_CN2ǃTEXT
		getChildAt(index: 9): buttonsǁautobetDialog_XMLǃcontent_CN2ǃTEXT
		getChildById(id: 'n5_pwo9_CN2'): buttonsǁautobetDialog_XMLǃcontent_CN2ǃTEXT
		getChild(name: 'content_EN'): buttonsǁautobetDialog_XMLǃcontent_ENǃTEXT
		getChildAt(index: 10): buttonsǁautobetDialog_XMLǃcontent_ENǃTEXT
		getChildById(id: 'n5_pwo9_EN'): buttonsǁautobetDialog_XMLǃcontent_ENǃTEXT
		getChild(name: 'content_IN'): buttonsǁautobetDialog_XMLǃcontent_INǃTEXT
		getChildAt(index: 11): buttonsǁautobetDialog_XMLǃcontent_INǃTEXT
		getChildById(id: 'n5_pwo9_IN'): buttonsǁautobetDialog_XMLǃcontent_INǃTEXT
		getChild(name: 'content_JP'): buttonsǁautobetDialog_XMLǃcontent_JPǃTEXT
		getChildAt(index: 12): buttonsǁautobetDialog_XMLǃcontent_JPǃTEXT
		getChildById(id: 'n5_pwo9_JP'): buttonsǁautobetDialog_XMLǃcontent_JPǃTEXT
		getChild(name: 'content_KR'): buttonsǁautobetDialog_XMLǃcontent_KRǃTEXT
		getChildAt(index: 13): buttonsǁautobetDialog_XMLǃcontent_KRǃTEXT
		getChildById(id: 'n5_pwo9_KR'): buttonsǁautobetDialog_XMLǃcontent_KRǃTEXT
		getChild(name: 'content_TH'): buttonsǁautobetDialog_XMLǃcontent_THǃTEXT
		getChildAt(index: 14): buttonsǁautobetDialog_XMLǃcontent_THǃTEXT
		getChildById(id: 'n5_pwo9_TH'): buttonsǁautobetDialog_XMLǃcontent_THǃTEXT
		getChild(name: 'content_VN'): buttonsǁautobetDialog_XMLǃcontent_VNǃTEXT
		getChildAt(index: 15): buttonsǁautobetDialog_XMLǃcontent_VNǃTEXT
		getChildById(id: 'n5_pwo9_VN'): buttonsǁautobetDialog_XMLǃcontent_VNǃTEXT
		getChild(name: 'content'): buttonsǁautobetDialog_XMLǃcontentǃTEXT
		getChildAt(index: 16): buttonsǁautobetDialog_XMLǃcontentǃTEXT
		getChildById(id: 'n5_pwo9'): buttonsǁautobetDialog_XMLǃcontentǃTEXT
		getChild(name: 'n1'): buttonsǁautobetDialog_XMLǃn1ǃIMAGE
		getChildAt(index: 17): buttonsǁautobetDialog_XMLǃn1ǃIMAGE
		getChildById(id: 'n6_pwo9'): buttonsǁautobetDialog_XMLǃn1ǃIMAGE
		getChild(name: 'n2'): buttonsǁautobetDialog_XMLǃn2ǃIMAGE
		getChildAt(index: 18): buttonsǁautobetDialog_XMLǃn2ǃIMAGE
		getChildById(id: 'n7_pwo9'): buttonsǁautobetDialog_XMLǃn2ǃIMAGE
		getChild(name: 'n8_CN2'): buttonsǁautobetDialog_XMLǃn8_CN2ǃTEXT
		getChildAt(index: 19): buttonsǁautobetDialog_XMLǃn8_CN2ǃTEXT
		getChildById(id: 'n8_pwo9_CN2'): buttonsǁautobetDialog_XMLǃn8_CN2ǃTEXT
		getChild(name: 'n8_EN'): buttonsǁautobetDialog_XMLǃn8_ENǃTEXT
		getChildAt(index: 20): buttonsǁautobetDialog_XMLǃn8_ENǃTEXT
		getChildById(id: 'n8_pwo9_EN'): buttonsǁautobetDialog_XMLǃn8_ENǃTEXT
		getChild(name: 'n8_IN'): buttonsǁautobetDialog_XMLǃn8_INǃTEXT
		getChildAt(index: 21): buttonsǁautobetDialog_XMLǃn8_INǃTEXT
		getChildById(id: 'n8_pwo9_IN'): buttonsǁautobetDialog_XMLǃn8_INǃTEXT
		getChild(name: 'n8_JP'): buttonsǁautobetDialog_XMLǃn8_JPǃTEXT
		getChildAt(index: 22): buttonsǁautobetDialog_XMLǃn8_JPǃTEXT
		getChildById(id: 'n8_pwo9_JP'): buttonsǁautobetDialog_XMLǃn8_JPǃTEXT
		getChild(name: 'n8_KR'): buttonsǁautobetDialog_XMLǃn8_KRǃTEXT
		getChildAt(index: 23): buttonsǁautobetDialog_XMLǃn8_KRǃTEXT
		getChildById(id: 'n8_pwo9_KR'): buttonsǁautobetDialog_XMLǃn8_KRǃTEXT
		getChild(name: 'n8_TH'): buttonsǁautobetDialog_XMLǃn8_THǃTEXT
		getChildAt(index: 24): buttonsǁautobetDialog_XMLǃn8_THǃTEXT
		getChildById(id: 'n8_pwo9_TH'): buttonsǁautobetDialog_XMLǃn8_THǃTEXT
		getChild(name: 'n8_VN'): buttonsǁautobetDialog_XMLǃn8_VNǃTEXT
		getChildAt(index: 25): buttonsǁautobetDialog_XMLǃn8_VNǃTEXT
		getChildById(id: 'n8_pwo9_VN'): buttonsǁautobetDialog_XMLǃn8_VNǃTEXT
		getChild(name: 'n8'): buttonsǁautobetDialog_XMLǃn8ǃTEXT
		getChildAt(index: 26): buttonsǁautobetDialog_XMLǃn8ǃTEXT
		getChildById(id: 'n8_pwo9'): buttonsǁautobetDialog_XMLǃn8ǃTEXT
		_children: [
			buttonsǁautobetDialog_XMLǃn4ǃIMAGE,
			buttonsǁautobetDialog_XMLǃn10_ENǃIMAGE,
			buttonsǁautobetDialog_XMLǃn10_INǃIMAGE,
			buttonsǁautobetDialog_XMLǃn10_JPǃIMAGE,
			buttonsǁautobetDialog_XMLǃn10_KRǃIMAGE,
			buttonsǁautobetDialog_XMLǃn10_THǃIMAGE,
			buttonsǁautobetDialog_XMLǃn10_VNǃIMAGE,
			buttonsǁautobetDialog_XMLǃn10_CN2ǃIMAGE,
			buttonsǁautobetDialog_XMLǃn10ǃIMAGE,
			buttonsǁautobetDialog_XMLǃcontent_CN2ǃTEXT,
			buttonsǁautobetDialog_XMLǃcontent_ENǃTEXT,
			buttonsǁautobetDialog_XMLǃcontent_INǃTEXT,
			buttonsǁautobetDialog_XMLǃcontent_JPǃTEXT,
			buttonsǁautobetDialog_XMLǃcontent_KRǃTEXT,
			buttonsǁautobetDialog_XMLǃcontent_THǃTEXT,
			buttonsǁautobetDialog_XMLǃcontent_VNǃTEXT,
			buttonsǁautobetDialog_XMLǃcontentǃTEXT,
			buttonsǁautobetDialog_XMLǃn1ǃIMAGE,
			buttonsǁautobetDialog_XMLǃn2ǃIMAGE,
			buttonsǁautobetDialog_XMLǃn8_CN2ǃTEXT,
			buttonsǁautobetDialog_XMLǃn8_ENǃTEXT,
			buttonsǁautobetDialog_XMLǃn8_INǃTEXT,
			buttonsǁautobetDialog_XMLǃn8_JPǃTEXT,
			buttonsǁautobetDialog_XMLǃn8_KRǃTEXT,
			buttonsǁautobetDialog_XMLǃn8_THǃTEXT,
			buttonsǁautobetDialog_XMLǃn8_VNǃTEXT,
			buttonsǁautobetDialog_XMLǃn8ǃTEXT
		]
		getController(name: '__language'): buttonsǁautobetDialog_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): buttonsǁautobetDialog_XMLǃ__languageǃCONTROLLER
		getController(name: 'button'): buttonsǁautobetDialog_XMLǃbuttonǃCONTROLLER
		getControllerAt(index: 1): buttonsǁautobetDialog_XMLǃbuttonǃCONTROLLER
		_controllers: [
			buttonsǁautobetDialog_XMLǃ__languageǃCONTROLLER,
			buttonsǁautobetDialog_XMLǃbuttonǃCONTROLLER
		]
	}
	interface buttonsǁautobetDialog_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁautobetDialog_XML
	}
	interface buttonsǁautobetDialog_XMLǃbuttonǃCONTROLLER extends fairygui.Controller{
		_parent: buttonsǁautobetDialog_XML
	}
	interface buttonsǁautobetDialog_XMLǃn4ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁautobetDialog_XML
	}
	interface buttonsǁautobetDialog_XMLǃn10_ENǃIMAGE extends fairygui.GImage{
		parent: buttonsǁautobetDialog_XML
	}
	interface buttonsǁautobetDialog_XMLǃn10_INǃIMAGE extends fairygui.GImage{
		parent: buttonsǁautobetDialog_XML
	}
	interface buttonsǁautobetDialog_XMLǃn10_JPǃIMAGE extends fairygui.GImage{
		parent: buttonsǁautobetDialog_XML
	}
	interface buttonsǁautobetDialog_XMLǃn10_KRǃIMAGE extends fairygui.GImage{
		parent: buttonsǁautobetDialog_XML
	}
	interface buttonsǁautobetDialog_XMLǃn10_THǃIMAGE extends fairygui.GImage{
		parent: buttonsǁautobetDialog_XML
	}
	interface buttonsǁautobetDialog_XMLǃn10_VNǃIMAGE extends fairygui.GImage{
		parent: buttonsǁautobetDialog_XML
	}
	interface buttonsǁautobetDialog_XMLǃn10_CN2ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁautobetDialog_XML
	}
	interface buttonsǁautobetDialog_XMLǃn10ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁautobetDialog_XML
	}
	interface buttonsǁautobetDialog_XMLǃcontent_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁautobetDialog_XML
	}
	interface buttonsǁautobetDialog_XMLǃcontent_ENǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁautobetDialog_XML
	}
	interface buttonsǁautobetDialog_XMLǃcontent_INǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁautobetDialog_XML
	}
	interface buttonsǁautobetDialog_XMLǃcontent_JPǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁautobetDialog_XML
	}
	interface buttonsǁautobetDialog_XMLǃcontent_KRǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁautobetDialog_XML
	}
	interface buttonsǁautobetDialog_XMLǃcontent_THǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁautobetDialog_XML
	}
	interface buttonsǁautobetDialog_XMLǃcontent_VNǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁautobetDialog_XML
	}
	interface buttonsǁautobetDialog_XMLǃcontentǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁautobetDialog_XML
	}
	interface buttonsǁautobetDialog_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁautobetDialog_XML
	}
	interface buttonsǁautobetDialog_XMLǃn2ǃIMAGE extends fairygui.GImage{
		parent: buttonsǁautobetDialog_XML
	}
	interface buttonsǁautobetDialog_XMLǃn8_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁautobetDialog_XML
	}
	interface buttonsǁautobetDialog_XMLǃn8_ENǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁautobetDialog_XML
	}
	interface buttonsǁautobetDialog_XMLǃn8_INǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁautobetDialog_XML
	}
	interface buttonsǁautobetDialog_XMLǃn8_JPǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁautobetDialog_XML
	}
	interface buttonsǁautobetDialog_XMLǃn8_KRǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁautobetDialog_XML
	}
	interface buttonsǁautobetDialog_XMLǃn8_THǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁautobetDialog_XML
	}
	interface buttonsǁautobetDialog_XMLǃn8_VNǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁautobetDialog_XML
	}
	interface buttonsǁautobetDialog_XMLǃn8ǃTEXT extends fairygui.GBasicTextField{
		parent: buttonsǁautobetDialog_XML
	}
	interface buttonsǁautobetDialog_XMLǃn247ǃCOMPONENT extends buttonsǁautobetDialog_XML{
		parent: Main_XML
	}
	interface yingqian_tip_XML extends fairygui.GComponent{
		getChild(name: 'n106'): mask_empty_XMLǃn106ǃCOMPONENT
		getChildAt(index: 0): mask_empty_XMLǃn106ǃCOMPONENT
		getChildById(id: 'n106_s20q'): mask_empty_XMLǃn106ǃCOMPONENT
		getChild(name: 'n3'): yingqian_tip_XMLǃn3ǃIMAGE
		getChildAt(index: 1): yingqian_tip_XMLǃn3ǃIMAGE
		getChildById(id: 'n3_mjdg'): yingqian_tip_XMLǃn3ǃIMAGE
		getChild(name: 'n100_EN'): yingqian_tip_XMLǃn100_ENǃIMAGE
		getChildAt(index: 2): yingqian_tip_XMLǃn100_ENǃIMAGE
		getChildById(id: 'n100_sjy9_EN'): yingqian_tip_XMLǃn100_ENǃIMAGE
		getChild(name: 'n100_IN'): yingqian_tip_XMLǃn100_INǃIMAGE
		getChildAt(index: 3): yingqian_tip_XMLǃn100_INǃIMAGE
		getChildById(id: 'n100_sjy9_IN'): yingqian_tip_XMLǃn100_INǃIMAGE
		getChild(name: 'n100_JP'): yingqian_tip_XMLǃn100_JPǃIMAGE
		getChildAt(index: 4): yingqian_tip_XMLǃn100_JPǃIMAGE
		getChildById(id: 'n100_sjy9_JP'): yingqian_tip_XMLǃn100_JPǃIMAGE
		getChild(name: 'n100_KR'): yingqian_tip_XMLǃn100_KRǃIMAGE
		getChildAt(index: 5): yingqian_tip_XMLǃn100_KRǃIMAGE
		getChildById(id: 'n100_sjy9_KR'): yingqian_tip_XMLǃn100_KRǃIMAGE
		getChild(name: 'n100_TH'): yingqian_tip_XMLǃn100_THǃIMAGE
		getChildAt(index: 6): yingqian_tip_XMLǃn100_THǃIMAGE
		getChildById(id: 'n100_sjy9_TH'): yingqian_tip_XMLǃn100_THǃIMAGE
		getChild(name: 'n100_VN'): yingqian_tip_XMLǃn100_VNǃIMAGE
		getChildAt(index: 7): yingqian_tip_XMLǃn100_VNǃIMAGE
		getChildById(id: 'n100_sjy9_VN'): yingqian_tip_XMLǃn100_VNǃIMAGE
		getChild(name: 'n100_CN2'): yingqian_tip_XMLǃn100_CN2ǃIMAGE
		getChildAt(index: 8): yingqian_tip_XMLǃn100_CN2ǃIMAGE
		getChildById(id: 'n100_sjy9_CN2'): yingqian_tip_XMLǃn100_CN2ǃIMAGE
		getChild(name: 'n100'): yingqian_tip_XMLǃn100ǃIMAGE
		getChildAt(index: 9): yingqian_tip_XMLǃn100ǃIMAGE
		getChildById(id: 'n100_sjy9'): yingqian_tip_XMLǃn100ǃIMAGE
		getChild(name: 'n101_EN'): yingqian_tip_XMLǃn101_ENǃIMAGE
		getChildAt(index: 10): yingqian_tip_XMLǃn101_ENǃIMAGE
		getChildById(id: 'n101_sjy9_EN'): yingqian_tip_XMLǃn101_ENǃIMAGE
		getChild(name: 'n101_IN'): yingqian_tip_XMLǃn101_INǃIMAGE
		getChildAt(index: 11): yingqian_tip_XMLǃn101_INǃIMAGE
		getChildById(id: 'n101_sjy9_IN'): yingqian_tip_XMLǃn101_INǃIMAGE
		getChild(name: 'n101_JP'): yingqian_tip_XMLǃn101_JPǃIMAGE
		getChildAt(index: 12): yingqian_tip_XMLǃn101_JPǃIMAGE
		getChildById(id: 'n101_sjy9_JP'): yingqian_tip_XMLǃn101_JPǃIMAGE
		getChild(name: 'n101_KR'): yingqian_tip_XMLǃn101_KRǃIMAGE
		getChildAt(index: 13): yingqian_tip_XMLǃn101_KRǃIMAGE
		getChildById(id: 'n101_sjy9_KR'): yingqian_tip_XMLǃn101_KRǃIMAGE
		getChild(name: 'n101_TH'): yingqian_tip_XMLǃn101_THǃIMAGE
		getChildAt(index: 14): yingqian_tip_XMLǃn101_THǃIMAGE
		getChildById(id: 'n101_sjy9_TH'): yingqian_tip_XMLǃn101_THǃIMAGE
		getChild(name: 'n101_VN'): yingqian_tip_XMLǃn101_VNǃIMAGE
		getChildAt(index: 15): yingqian_tip_XMLǃn101_VNǃIMAGE
		getChildById(id: 'n101_sjy9_VN'): yingqian_tip_XMLǃn101_VNǃIMAGE
		getChild(name: 'n101_CN2'): yingqian_tip_XMLǃn101_CN2ǃIMAGE
		getChildAt(index: 16): yingqian_tip_XMLǃn101_CN2ǃIMAGE
		getChildById(id: 'n101_sjy9_CN2'): yingqian_tip_XMLǃn101_CN2ǃIMAGE
		getChild(name: 'n101'): yingqian_tip_XMLǃn101ǃIMAGE
		getChildAt(index: 17): yingqian_tip_XMLǃn101ǃIMAGE
		getChildById(id: 'n101_sjy9'): yingqian_tip_XMLǃn101ǃIMAGE
		getChild(name: 'n114_EN'): yingqian_tip_XMLǃn114_ENǃIMAGE
		getChildAt(index: 18): yingqian_tip_XMLǃn114_ENǃIMAGE
		getChildById(id: 'n114_ia2b_EN'): yingqian_tip_XMLǃn114_ENǃIMAGE
		getChild(name: 'n114_IN'): yingqian_tip_XMLǃn114_INǃIMAGE
		getChildAt(index: 19): yingqian_tip_XMLǃn114_INǃIMAGE
		getChildById(id: 'n114_ia2b_IN'): yingqian_tip_XMLǃn114_INǃIMAGE
		getChild(name: 'n114_JP'): yingqian_tip_XMLǃn114_JPǃIMAGE
		getChildAt(index: 20): yingqian_tip_XMLǃn114_JPǃIMAGE
		getChildById(id: 'n114_ia2b_JP'): yingqian_tip_XMLǃn114_JPǃIMAGE
		getChild(name: 'n114_KR'): yingqian_tip_XMLǃn114_KRǃIMAGE
		getChildAt(index: 21): yingqian_tip_XMLǃn114_KRǃIMAGE
		getChildById(id: 'n114_ia2b_KR'): yingqian_tip_XMLǃn114_KRǃIMAGE
		getChild(name: 'n114_TH'): yingqian_tip_XMLǃn114_THǃIMAGE
		getChildAt(index: 22): yingqian_tip_XMLǃn114_THǃIMAGE
		getChildById(id: 'n114_ia2b_TH'): yingqian_tip_XMLǃn114_THǃIMAGE
		getChild(name: 'n114_VN'): yingqian_tip_XMLǃn114_VNǃIMAGE
		getChildAt(index: 23): yingqian_tip_XMLǃn114_VNǃIMAGE
		getChildById(id: 'n114_ia2b_VN'): yingqian_tip_XMLǃn114_VNǃIMAGE
		getChild(name: 'n114_CN2'): yingqian_tip_XMLǃn114_CN2ǃIMAGE
		getChildAt(index: 24): yingqian_tip_XMLǃn114_CN2ǃIMAGE
		getChildById(id: 'n114_ia2b_CN2'): yingqian_tip_XMLǃn114_CN2ǃIMAGE
		getChild(name: 'n114'): yingqian_tip_XMLǃn114ǃIMAGE
		getChildAt(index: 25): yingqian_tip_XMLǃn114ǃIMAGE
		getChildById(id: 'n114_ia2b'): yingqian_tip_XMLǃn114ǃIMAGE
		getChild(name: 'n107_EN'): yingqian_tip_XMLǃn107_ENǃIMAGE
		getChildAt(index: 26): yingqian_tip_XMLǃn107_ENǃIMAGE
		getChildById(id: 'n107_rddw_EN'): yingqian_tip_XMLǃn107_ENǃIMAGE
		getChild(name: 'n107_IN'): yingqian_tip_XMLǃn107_INǃIMAGE
		getChildAt(index: 27): yingqian_tip_XMLǃn107_INǃIMAGE
		getChildById(id: 'n107_rddw_IN'): yingqian_tip_XMLǃn107_INǃIMAGE
		getChild(name: 'n107_JP'): yingqian_tip_XMLǃn107_JPǃIMAGE
		getChildAt(index: 28): yingqian_tip_XMLǃn107_JPǃIMAGE
		getChildById(id: 'n107_rddw_JP'): yingqian_tip_XMLǃn107_JPǃIMAGE
		getChild(name: 'n107_KR'): yingqian_tip_XMLǃn107_KRǃIMAGE
		getChildAt(index: 29): yingqian_tip_XMLǃn107_KRǃIMAGE
		getChildById(id: 'n107_rddw_KR'): yingqian_tip_XMLǃn107_KRǃIMAGE
		getChild(name: 'n107_TH'): yingqian_tip_XMLǃn107_THǃIMAGE
		getChildAt(index: 30): yingqian_tip_XMLǃn107_THǃIMAGE
		getChildById(id: 'n107_rddw_TH'): yingqian_tip_XMLǃn107_THǃIMAGE
		getChild(name: 'n107_VN'): yingqian_tip_XMLǃn107_VNǃIMAGE
		getChildAt(index: 31): yingqian_tip_XMLǃn107_VNǃIMAGE
		getChildById(id: 'n107_rddw_VN'): yingqian_tip_XMLǃn107_VNǃIMAGE
		getChild(name: 'n107_CN2'): yingqian_tip_XMLǃn107_CN2ǃIMAGE
		getChildAt(index: 32): yingqian_tip_XMLǃn107_CN2ǃIMAGE
		getChildById(id: 'n107_rddw_CN2'): yingqian_tip_XMLǃn107_CN2ǃIMAGE
		getChild(name: 'n107'): yingqian_tip_XMLǃn107ǃIMAGE
		getChildAt(index: 33): yingqian_tip_XMLǃn107ǃIMAGE
		getChildById(id: 'n107_rddw'): yingqian_tip_XMLǃn107ǃIMAGE
		getChild(name: 'n108_EN'): yingqian_tip_XMLǃn108_ENǃIMAGE
		getChildAt(index: 34): yingqian_tip_XMLǃn108_ENǃIMAGE
		getChildById(id: 'n108_rddw_EN'): yingqian_tip_XMLǃn108_ENǃIMAGE
		getChild(name: 'n108_IN'): yingqian_tip_XMLǃn108_INǃIMAGE
		getChildAt(index: 35): yingqian_tip_XMLǃn108_INǃIMAGE
		getChildById(id: 'n108_rddw_IN'): yingqian_tip_XMLǃn108_INǃIMAGE
		getChild(name: 'n108_JP'): yingqian_tip_XMLǃn108_JPǃIMAGE
		getChildAt(index: 36): yingqian_tip_XMLǃn108_JPǃIMAGE
		getChildById(id: 'n108_rddw_JP'): yingqian_tip_XMLǃn108_JPǃIMAGE
		getChild(name: 'n108_KR'): yingqian_tip_XMLǃn108_KRǃIMAGE
		getChildAt(index: 37): yingqian_tip_XMLǃn108_KRǃIMAGE
		getChildById(id: 'n108_rddw_KR'): yingqian_tip_XMLǃn108_KRǃIMAGE
		getChild(name: 'n108_TH'): yingqian_tip_XMLǃn108_THǃIMAGE
		getChildAt(index: 38): yingqian_tip_XMLǃn108_THǃIMAGE
		getChildById(id: 'n108_rddw_TH'): yingqian_tip_XMLǃn108_THǃIMAGE
		getChild(name: 'n108_VN'): yingqian_tip_XMLǃn108_VNǃIMAGE
		getChildAt(index: 39): yingqian_tip_XMLǃn108_VNǃIMAGE
		getChildById(id: 'n108_rddw_VN'): yingqian_tip_XMLǃn108_VNǃIMAGE
		getChild(name: 'n108_CN2'): yingqian_tip_XMLǃn108_CN2ǃIMAGE
		getChildAt(index: 40): yingqian_tip_XMLǃn108_CN2ǃIMAGE
		getChildById(id: 'n108_rddw_CN2'): yingqian_tip_XMLǃn108_CN2ǃIMAGE
		getChild(name: 'n108'): yingqian_tip_XMLǃn108ǃIMAGE
		getChildAt(index: 41): yingqian_tip_XMLǃn108ǃIMAGE
		getChildById(id: 'n108_rddw'): yingqian_tip_XMLǃn108ǃIMAGE
		getChild(name: 'n109'): yingqian_tip_XMLǃn109ǃIMAGE
		getChildAt(index: 42): yingqian_tip_XMLǃn109ǃIMAGE
		getChildById(id: 'n109_rddw'): yingqian_tip_XMLǃn109ǃIMAGE
		getChild(name: 'n110'): yingqian_tip_XMLǃn110ǃIMAGE
		getChildAt(index: 43): yingqian_tip_XMLǃn110ǃIMAGE
		getChildById(id: 'n110_rddw'): yingqian_tip_XMLǃn110ǃIMAGE
		getChild(name: 'txt_bet'): yingqian_tip_XMLǃtxt_betǃTEXT
		getChildAt(index: 44): yingqian_tip_XMLǃtxt_betǃTEXT
		getChildById(id: 'n15_uc7a'): yingqian_tip_XMLǃtxt_betǃTEXT
		getChild(name: 'txt_rank_CN2'): yingqian_tip_XMLǃtxt_rank_CN2ǃTEXT
		getChildAt(index: 45): yingqian_tip_XMLǃtxt_rank_CN2ǃTEXT
		getChildById(id: 'n93_n5lc_CN2'): yingqian_tip_XMLǃtxt_rank_CN2ǃTEXT
		getChild(name: 'txt_rank_EN'): yingqian_tip_XMLǃtxt_rank_ENǃTEXT
		getChildAt(index: 46): yingqian_tip_XMLǃtxt_rank_ENǃTEXT
		getChildById(id: 'n93_n5lc_EN'): yingqian_tip_XMLǃtxt_rank_ENǃTEXT
		getChild(name: 'txt_rank_IN'): yingqian_tip_XMLǃtxt_rank_INǃTEXT
		getChildAt(index: 47): yingqian_tip_XMLǃtxt_rank_INǃTEXT
		getChildById(id: 'n93_n5lc_IN'): yingqian_tip_XMLǃtxt_rank_INǃTEXT
		getChild(name: 'txt_rank_JP'): yingqian_tip_XMLǃtxt_rank_JPǃTEXT
		getChildAt(index: 48): yingqian_tip_XMLǃtxt_rank_JPǃTEXT
		getChildById(id: 'n93_n5lc_JP'): yingqian_tip_XMLǃtxt_rank_JPǃTEXT
		getChild(name: 'txt_rank_KR'): yingqian_tip_XMLǃtxt_rank_KRǃTEXT
		getChildAt(index: 49): yingqian_tip_XMLǃtxt_rank_KRǃTEXT
		getChildById(id: 'n93_n5lc_KR'): yingqian_tip_XMLǃtxt_rank_KRǃTEXT
		getChild(name: 'txt_rank_TH'): yingqian_tip_XMLǃtxt_rank_THǃTEXT
		getChildAt(index: 50): yingqian_tip_XMLǃtxt_rank_THǃTEXT
		getChildById(id: 'n93_n5lc_TH'): yingqian_tip_XMLǃtxt_rank_THǃTEXT
		getChild(name: 'txt_rank_VN'): yingqian_tip_XMLǃtxt_rank_VNǃTEXT
		getChildAt(index: 51): yingqian_tip_XMLǃtxt_rank_VNǃTEXT
		getChildById(id: 'n93_n5lc_VN'): yingqian_tip_XMLǃtxt_rank_VNǃTEXT
		getChild(name: 'txt_rank'): yingqian_tip_XMLǃtxt_rankǃTEXT
		getChildAt(index: 52): yingqian_tip_XMLǃtxt_rankǃTEXT
		getChildById(id: 'n93_n5lc'): yingqian_tip_XMLǃtxt_rankǃTEXT
		getChild(name: 'txt_total'): yingqian_tip_XMLǃtxt_totalǃTEXT
		getChildAt(index: 53): yingqian_tip_XMLǃtxt_totalǃTEXT
		getChildById(id: 'n9_uc7a'): yingqian_tip_XMLǃtxt_totalǃTEXT
		getChild(name: 'btn_con'): buttonsǁbutton_queren_XMLǃbtn_conǃCOMPONENT
		getChildAt(index: 54): buttonsǁbutton_queren_XMLǃbtn_conǃCOMPONENT
		getChildById(id: 'n111_rddw'): buttonsǁbutton_queren_XMLǃbtn_conǃCOMPONENT
		getChild(name: 'list'): yingqian_tip_XMLǃlistǃLIST
		getChildAt(index: 55): yingqian_tip_XMLǃlistǃLIST
		getChildById(id: 'n112_cq3n'): yingqian_tip_XMLǃlistǃLIST
		getChild(name: 'countdown'): yingqian_tip_XMLǃcountdownǃTEXT
		getChildAt(index: 56): yingqian_tip_XMLǃcountdownǃTEXT
		getChildById(id: 'n113_cq3n'): yingqian_tip_XMLǃcountdownǃTEXT
		_children: [
			mask_empty_XMLǃn106ǃCOMPONENT,
			yingqian_tip_XMLǃn3ǃIMAGE,
			yingqian_tip_XMLǃn100_ENǃIMAGE,
			yingqian_tip_XMLǃn100_INǃIMAGE,
			yingqian_tip_XMLǃn100_JPǃIMAGE,
			yingqian_tip_XMLǃn100_KRǃIMAGE,
			yingqian_tip_XMLǃn100_THǃIMAGE,
			yingqian_tip_XMLǃn100_VNǃIMAGE,
			yingqian_tip_XMLǃn100_CN2ǃIMAGE,
			yingqian_tip_XMLǃn100ǃIMAGE,
			yingqian_tip_XMLǃn101_ENǃIMAGE,
			yingqian_tip_XMLǃn101_INǃIMAGE,
			yingqian_tip_XMLǃn101_JPǃIMAGE,
			yingqian_tip_XMLǃn101_KRǃIMAGE,
			yingqian_tip_XMLǃn101_THǃIMAGE,
			yingqian_tip_XMLǃn101_VNǃIMAGE,
			yingqian_tip_XMLǃn101_CN2ǃIMAGE,
			yingqian_tip_XMLǃn101ǃIMAGE,
			yingqian_tip_XMLǃn114_ENǃIMAGE,
			yingqian_tip_XMLǃn114_INǃIMAGE,
			yingqian_tip_XMLǃn114_JPǃIMAGE,
			yingqian_tip_XMLǃn114_KRǃIMAGE,
			yingqian_tip_XMLǃn114_THǃIMAGE,
			yingqian_tip_XMLǃn114_VNǃIMAGE,
			yingqian_tip_XMLǃn114_CN2ǃIMAGE,
			yingqian_tip_XMLǃn114ǃIMAGE,
			yingqian_tip_XMLǃn107_ENǃIMAGE,
			yingqian_tip_XMLǃn107_INǃIMAGE,
			yingqian_tip_XMLǃn107_JPǃIMAGE,
			yingqian_tip_XMLǃn107_KRǃIMAGE,
			yingqian_tip_XMLǃn107_THǃIMAGE,
			yingqian_tip_XMLǃn107_VNǃIMAGE,
			yingqian_tip_XMLǃn107_CN2ǃIMAGE,
			yingqian_tip_XMLǃn107ǃIMAGE,
			yingqian_tip_XMLǃn108_ENǃIMAGE,
			yingqian_tip_XMLǃn108_INǃIMAGE,
			yingqian_tip_XMLǃn108_JPǃIMAGE,
			yingqian_tip_XMLǃn108_KRǃIMAGE,
			yingqian_tip_XMLǃn108_THǃIMAGE,
			yingqian_tip_XMLǃn108_VNǃIMAGE,
			yingqian_tip_XMLǃn108_CN2ǃIMAGE,
			yingqian_tip_XMLǃn108ǃIMAGE,
			yingqian_tip_XMLǃn109ǃIMAGE,
			yingqian_tip_XMLǃn110ǃIMAGE,
			yingqian_tip_XMLǃtxt_betǃTEXT,
			yingqian_tip_XMLǃtxt_rank_CN2ǃTEXT,
			yingqian_tip_XMLǃtxt_rank_ENǃTEXT,
			yingqian_tip_XMLǃtxt_rank_INǃTEXT,
			yingqian_tip_XMLǃtxt_rank_JPǃTEXT,
			yingqian_tip_XMLǃtxt_rank_KRǃTEXT,
			yingqian_tip_XMLǃtxt_rank_THǃTEXT,
			yingqian_tip_XMLǃtxt_rank_VNǃTEXT,
			yingqian_tip_XMLǃtxt_rankǃTEXT,
			yingqian_tip_XMLǃtxt_totalǃTEXT,
			buttonsǁbutton_queren_XMLǃbtn_conǃCOMPONENT,
			yingqian_tip_XMLǃlistǃLIST,
			yingqian_tip_XMLǃcountdownǃTEXT
		]
		getTransition(name: 't0'): yingqian_tip_XMLǃt0ǃTRANSITION
		getTransitionAt(index: 0): yingqian_tip_XMLǃt0ǃTRANSITION
		getTransition(name: 't1'): yingqian_tip_XMLǃt1ǃTRANSITION
		getTransitionAt(index: 1): yingqian_tip_XMLǃt1ǃTRANSITION
		_transitions: [
			yingqian_tip_XMLǃt0ǃTRANSITION,
			yingqian_tip_XMLǃt1ǃTRANSITION
		]
		getController(name: '__language'): yingqian_tip_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): yingqian_tip_XMLǃ__languageǃCONTROLLER
		getController(name: 'c1'): yingqian_tip_XMLǃc1ǃCONTROLLER
		getControllerAt(index: 1): yingqian_tip_XMLǃc1ǃCONTROLLER
		_controllers: [
			yingqian_tip_XMLǃ__languageǃCONTROLLER,
			yingqian_tip_XMLǃc1ǃCONTROLLER
		]
	}
	interface yingqian_tip_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃc1ǃCONTROLLER extends fairygui.Controller{
		_parent: yingqian_tip_XML
	}
	interface mask_empty_XMLǃn106ǃCOMPONENT extends mask_empty_XML{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn3ǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn100_ENǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn100_INǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn100_JPǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn100_KRǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn100_THǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn100_VNǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn100_CN2ǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn100ǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn101_ENǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn101_INǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn101_JPǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn101_KRǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn101_THǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn101_VNǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn101_CN2ǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn101ǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn114_ENǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn114_INǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn114_JPǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn114_KRǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn114_THǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn114_VNǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn114_CN2ǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn114ǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn107_ENǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn107_INǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn107_JPǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn107_KRǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn107_THǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn107_VNǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn107_CN2ǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn107ǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn108_ENǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn108_INǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn108_JPǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn108_KRǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn108_THǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn108_VNǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn108_CN2ǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn108ǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn109ǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃn110ǃIMAGE extends fairygui.GImage{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃtxt_betǃTEXT extends fairygui.GBasicTextField{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃtxt_rank_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃtxt_rank_ENǃTEXT extends fairygui.GBasicTextField{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃtxt_rank_INǃTEXT extends fairygui.GBasicTextField{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃtxt_rank_JPǃTEXT extends fairygui.GBasicTextField{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃtxt_rank_KRǃTEXT extends fairygui.GBasicTextField{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃtxt_rank_THǃTEXT extends fairygui.GBasicTextField{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃtxt_rank_VNǃTEXT extends fairygui.GBasicTextField{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃtxt_rankǃTEXT extends fairygui.GBasicTextField{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃtxt_totalǃTEXT extends fairygui.GBasicTextField{
		parent: yingqian_tip_XML
	}
	interface buttonsǁbutton_queren_XMLǃbtn_conǃCOMPONENT extends buttonsǁbutton_queren_XML{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃlistǃLIST extends fairygui.GList{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃcountdownǃTEXT extends fairygui.GBasicTextField{
		parent: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃt0ǃTRANSITION extends fairygui.Transition{
		_owner: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃt1ǃTRANSITION extends fairygui.Transition{
		_owner: yingqian_tip_XML
	}
	interface yingqian_tip_XMLǃyingqian_tipsǃCOMPONENT extends yingqian_tip_XML{
		parent: Main_XML
	}
	interface kaishixiazhu_XML extends fairygui.GComponent{
		getChild(name: 'n1'): kaishixiazhu_XMLǃn1ǃIMAGE
		getChildAt(index: 0): kaishixiazhu_XMLǃn1ǃIMAGE
		getChildById(id: 'n1_sjy9'): kaishixiazhu_XMLǃn1ǃIMAGE
		getChild(name: 'n0_EN'): kaishixiazhu_XMLǃn0_ENǃIMAGE
		getChildAt(index: 1): kaishixiazhu_XMLǃn0_ENǃIMAGE
		getChildById(id: 'n0_x8xm_EN'): kaishixiazhu_XMLǃn0_ENǃIMAGE
		getChild(name: 'n0_IN'): kaishixiazhu_XMLǃn0_INǃIMAGE
		getChildAt(index: 2): kaishixiazhu_XMLǃn0_INǃIMAGE
		getChildById(id: 'n0_x8xm_IN'): kaishixiazhu_XMLǃn0_INǃIMAGE
		getChild(name: 'n0_JP'): kaishixiazhu_XMLǃn0_JPǃIMAGE
		getChildAt(index: 3): kaishixiazhu_XMLǃn0_JPǃIMAGE
		getChildById(id: 'n0_x8xm_JP'): kaishixiazhu_XMLǃn0_JPǃIMAGE
		getChild(name: 'n0_KR'): kaishixiazhu_XMLǃn0_KRǃIMAGE
		getChildAt(index: 4): kaishixiazhu_XMLǃn0_KRǃIMAGE
		getChildById(id: 'n0_x8xm_KR'): kaishixiazhu_XMLǃn0_KRǃIMAGE
		getChild(name: 'n0_TH'): kaishixiazhu_XMLǃn0_THǃIMAGE
		getChildAt(index: 5): kaishixiazhu_XMLǃn0_THǃIMAGE
		getChildById(id: 'n0_x8xm_TH'): kaishixiazhu_XMLǃn0_THǃIMAGE
		getChild(name: 'n0_VN'): kaishixiazhu_XMLǃn0_VNǃIMAGE
		getChildAt(index: 6): kaishixiazhu_XMLǃn0_VNǃIMAGE
		getChildById(id: 'n0_x8xm_VN'): kaishixiazhu_XMLǃn0_VNǃIMAGE
		getChild(name: 'n0_CN2'): kaishixiazhu_XMLǃn0_CN2ǃIMAGE
		getChildAt(index: 7): kaishixiazhu_XMLǃn0_CN2ǃIMAGE
		getChildById(id: 'n0_x8xm_CN2'): kaishixiazhu_XMLǃn0_CN2ǃIMAGE
		getChild(name: 'n0'): kaishixiazhu_XMLǃn0ǃIMAGE
		getChildAt(index: 8): kaishixiazhu_XMLǃn0ǃIMAGE
		getChildById(id: 'n0_x8xm'): kaishixiazhu_XMLǃn0ǃIMAGE
		_children: [
			kaishixiazhu_XMLǃn1ǃIMAGE,
			kaishixiazhu_XMLǃn0_ENǃIMAGE,
			kaishixiazhu_XMLǃn0_INǃIMAGE,
			kaishixiazhu_XMLǃn0_JPǃIMAGE,
			kaishixiazhu_XMLǃn0_KRǃIMAGE,
			kaishixiazhu_XMLǃn0_THǃIMAGE,
			kaishixiazhu_XMLǃn0_VNǃIMAGE,
			kaishixiazhu_XMLǃn0_CN2ǃIMAGE,
			kaishixiazhu_XMLǃn0ǃIMAGE
		]
		getController(name: '__language'): kaishixiazhu_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): kaishixiazhu_XMLǃ__languageǃCONTROLLER
		_controllers: [
			kaishixiazhu_XMLǃ__languageǃCONTROLLER
		]
	}
	interface kaishixiazhu_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: kaishixiazhu_XML
	}
	interface kaishixiazhu_XMLǃn1ǃIMAGE extends fairygui.GImage{
		parent: kaishixiazhu_XML
	}
	interface kaishixiazhu_XMLǃn0_ENǃIMAGE extends fairygui.GImage{
		parent: kaishixiazhu_XML
	}
	interface kaishixiazhu_XMLǃn0_INǃIMAGE extends fairygui.GImage{
		parent: kaishixiazhu_XML
	}
	interface kaishixiazhu_XMLǃn0_JPǃIMAGE extends fairygui.GImage{
		parent: kaishixiazhu_XML
	}
	interface kaishixiazhu_XMLǃn0_KRǃIMAGE extends fairygui.GImage{
		parent: kaishixiazhu_XML
	}
	interface kaishixiazhu_XMLǃn0_THǃIMAGE extends fairygui.GImage{
		parent: kaishixiazhu_XML
	}
	interface kaishixiazhu_XMLǃn0_VNǃIMAGE extends fairygui.GImage{
		parent: kaishixiazhu_XML
	}
	interface kaishixiazhu_XMLǃn0_CN2ǃIMAGE extends fairygui.GImage{
		parent: kaishixiazhu_XML
	}
	interface kaishixiazhu_XMLǃn0ǃIMAGE extends fairygui.GImage{
		parent: kaishixiazhu_XML
	}
	interface kaishixiazhu_XMLǃkaishixiazhuǃCOMPONENT extends kaishixiazhu_XML{
		parent: Main_XML
	}
	interface otherǁzuihoutouzhu_confirm_XML extends fairygui.GComponent{
		getChild(name: 'n4'): mask_empty_XMLǃn4ǃCOMPONENT
		getChildAt(index: 0): mask_empty_XMLǃn4ǃCOMPONENT
		getChildById(id: 'n4_s20q'): mask_empty_XMLǃn4ǃCOMPONENT
		getChild(name: 'n0'): otherǁzuihoutouzhu_confirm_XMLǃn0ǃIMAGE
		getChildAt(index: 1): otherǁzuihoutouzhu_confirm_XMLǃn0ǃIMAGE
		getChildById(id: 'n0_fjuj'): otherǁzuihoutouzhu_confirm_XMLǃn0ǃIMAGE
		getChild(name: 'content_CN2'): otherǁzuihoutouzhu_confirm_XMLǃcontent_CN2ǃTEXT
		getChildAt(index: 2): otherǁzuihoutouzhu_confirm_XMLǃcontent_CN2ǃTEXT
		getChildById(id: 'n1_fjuj_CN2'): otherǁzuihoutouzhu_confirm_XMLǃcontent_CN2ǃTEXT
		getChild(name: 'content_EN'): otherǁzuihoutouzhu_confirm_XMLǃcontent_ENǃTEXT
		getChildAt(index: 3): otherǁzuihoutouzhu_confirm_XMLǃcontent_ENǃTEXT
		getChildById(id: 'n1_fjuj_EN'): otherǁzuihoutouzhu_confirm_XMLǃcontent_ENǃTEXT
		getChild(name: 'content_IN'): otherǁzuihoutouzhu_confirm_XMLǃcontent_INǃTEXT
		getChildAt(index: 4): otherǁzuihoutouzhu_confirm_XMLǃcontent_INǃTEXT
		getChildById(id: 'n1_fjuj_IN'): otherǁzuihoutouzhu_confirm_XMLǃcontent_INǃTEXT
		getChild(name: 'content_JP'): otherǁzuihoutouzhu_confirm_XMLǃcontent_JPǃTEXT
		getChildAt(index: 5): otherǁzuihoutouzhu_confirm_XMLǃcontent_JPǃTEXT
		getChildById(id: 'n1_fjuj_JP'): otherǁzuihoutouzhu_confirm_XMLǃcontent_JPǃTEXT
		getChild(name: 'content_KR'): otherǁzuihoutouzhu_confirm_XMLǃcontent_KRǃTEXT
		getChildAt(index: 6): otherǁzuihoutouzhu_confirm_XMLǃcontent_KRǃTEXT
		getChildById(id: 'n1_fjuj_KR'): otherǁzuihoutouzhu_confirm_XMLǃcontent_KRǃTEXT
		getChild(name: 'content_TH'): otherǁzuihoutouzhu_confirm_XMLǃcontent_THǃTEXT
		getChildAt(index: 7): otherǁzuihoutouzhu_confirm_XMLǃcontent_THǃTEXT
		getChildById(id: 'n1_fjuj_TH'): otherǁzuihoutouzhu_confirm_XMLǃcontent_THǃTEXT
		getChild(name: 'content_VN'): otherǁzuihoutouzhu_confirm_XMLǃcontent_VNǃTEXT
		getChildAt(index: 8): otherǁzuihoutouzhu_confirm_XMLǃcontent_VNǃTEXT
		getChildById(id: 'n1_fjuj_VN'): otherǁzuihoutouzhu_confirm_XMLǃcontent_VNǃTEXT
		getChild(name: 'content'): otherǁzuihoutouzhu_confirm_XMLǃcontentǃTEXT
		getChildAt(index: 9): otherǁzuihoutouzhu_confirm_XMLǃcontentǃTEXT
		getChildById(id: 'n1_fjuj'): otherǁzuihoutouzhu_confirm_XMLǃcontentǃTEXT
		getChild(name: 'button_quxiao'): buttonsǁbutton_quxiao_XMLǃbutton_quxiaoǃCOMPONENT
		getChildAt(index: 10): buttonsǁbutton_quxiao_XMLǃbutton_quxiaoǃCOMPONENT
		getChildById(id: 'n2_fjuj'): buttonsǁbutton_quxiao_XMLǃbutton_quxiaoǃCOMPONENT
		getChild(name: 'button_queren'): buttonsǁbutton_queren_XMLǃbutton_querenǃCOMPONENT
		getChildAt(index: 11): buttonsǁbutton_queren_XMLǃbutton_querenǃCOMPONENT
		getChildById(id: 'n3_fjuj'): buttonsǁbutton_queren_XMLǃbutton_querenǃCOMPONENT
		getChild(name: 'n5_EN'): otherǁzuihoutouzhu_confirm_XMLǃn5_ENǃIMAGE
		getChildAt(index: 12): otherǁzuihoutouzhu_confirm_XMLǃn5_ENǃIMAGE
		getChildById(id: 'n5_swyz_EN'): otherǁzuihoutouzhu_confirm_XMLǃn5_ENǃIMAGE
		getChild(name: 'n5_IN'): otherǁzuihoutouzhu_confirm_XMLǃn5_INǃIMAGE
		getChildAt(index: 13): otherǁzuihoutouzhu_confirm_XMLǃn5_INǃIMAGE
		getChildById(id: 'n5_swyz_IN'): otherǁzuihoutouzhu_confirm_XMLǃn5_INǃIMAGE
		getChild(name: 'n5_JP'): otherǁzuihoutouzhu_confirm_XMLǃn5_JPǃIMAGE
		getChildAt(index: 14): otherǁzuihoutouzhu_confirm_XMLǃn5_JPǃIMAGE
		getChildById(id: 'n5_swyz_JP'): otherǁzuihoutouzhu_confirm_XMLǃn5_JPǃIMAGE
		getChild(name: 'n5_KR'): otherǁzuihoutouzhu_confirm_XMLǃn5_KRǃIMAGE
		getChildAt(index: 15): otherǁzuihoutouzhu_confirm_XMLǃn5_KRǃIMAGE
		getChildById(id: 'n5_swyz_KR'): otherǁzuihoutouzhu_confirm_XMLǃn5_KRǃIMAGE
		getChild(name: 'n5_TH'): otherǁzuihoutouzhu_confirm_XMLǃn5_THǃIMAGE
		getChildAt(index: 16): otherǁzuihoutouzhu_confirm_XMLǃn5_THǃIMAGE
		getChildById(id: 'n5_swyz_TH'): otherǁzuihoutouzhu_confirm_XMLǃn5_THǃIMAGE
		getChild(name: 'n5_VN'): otherǁzuihoutouzhu_confirm_XMLǃn5_VNǃIMAGE
		getChildAt(index: 17): otherǁzuihoutouzhu_confirm_XMLǃn5_VNǃIMAGE
		getChildById(id: 'n5_swyz_VN'): otherǁzuihoutouzhu_confirm_XMLǃn5_VNǃIMAGE
		getChild(name: 'n5_CN2'): otherǁzuihoutouzhu_confirm_XMLǃn5_CN2ǃIMAGE
		getChildAt(index: 18): otherǁzuihoutouzhu_confirm_XMLǃn5_CN2ǃIMAGE
		getChildById(id: 'n5_swyz_CN2'): otherǁzuihoutouzhu_confirm_XMLǃn5_CN2ǃIMAGE
		getChild(name: 'n5'): otherǁzuihoutouzhu_confirm_XMLǃn5ǃIMAGE
		getChildAt(index: 19): otherǁzuihoutouzhu_confirm_XMLǃn5ǃIMAGE
		getChildById(id: 'n5_swyz'): otherǁzuihoutouzhu_confirm_XMLǃn5ǃIMAGE
		_children: [
			mask_empty_XMLǃn4ǃCOMPONENT,
			otherǁzuihoutouzhu_confirm_XMLǃn0ǃIMAGE,
			otherǁzuihoutouzhu_confirm_XMLǃcontent_CN2ǃTEXT,
			otherǁzuihoutouzhu_confirm_XMLǃcontent_ENǃTEXT,
			otherǁzuihoutouzhu_confirm_XMLǃcontent_INǃTEXT,
			otherǁzuihoutouzhu_confirm_XMLǃcontent_JPǃTEXT,
			otherǁzuihoutouzhu_confirm_XMLǃcontent_KRǃTEXT,
			otherǁzuihoutouzhu_confirm_XMLǃcontent_THǃTEXT,
			otherǁzuihoutouzhu_confirm_XMLǃcontent_VNǃTEXT,
			otherǁzuihoutouzhu_confirm_XMLǃcontentǃTEXT,
			buttonsǁbutton_quxiao_XMLǃbutton_quxiaoǃCOMPONENT,
			buttonsǁbutton_queren_XMLǃbutton_querenǃCOMPONENT,
			otherǁzuihoutouzhu_confirm_XMLǃn5_ENǃIMAGE,
			otherǁzuihoutouzhu_confirm_XMLǃn5_INǃIMAGE,
			otherǁzuihoutouzhu_confirm_XMLǃn5_JPǃIMAGE,
			otherǁzuihoutouzhu_confirm_XMLǃn5_KRǃIMAGE,
			otherǁzuihoutouzhu_confirm_XMLǃn5_THǃIMAGE,
			otherǁzuihoutouzhu_confirm_XMLǃn5_VNǃIMAGE,
			otherǁzuihoutouzhu_confirm_XMLǃn5_CN2ǃIMAGE,
			otherǁzuihoutouzhu_confirm_XMLǃn5ǃIMAGE
		]
		getController(name: '__language'): otherǁzuihoutouzhu_confirm_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): otherǁzuihoutouzhu_confirm_XMLǃ__languageǃCONTROLLER
		_controllers: [
			otherǁzuihoutouzhu_confirm_XMLǃ__languageǃCONTROLLER
		]
	}
	interface otherǁzuihoutouzhu_confirm_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: otherǁzuihoutouzhu_confirm_XML
	}
	interface mask_empty_XMLǃn4ǃCOMPONENT extends mask_empty_XML{
		parent: otherǁzuihoutouzhu_confirm_XML
	}
	interface otherǁzuihoutouzhu_confirm_XMLǃn0ǃIMAGE extends fairygui.GImage{
		parent: otherǁzuihoutouzhu_confirm_XML
	}
	interface otherǁzuihoutouzhu_confirm_XMLǃcontent_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁzuihoutouzhu_confirm_XML
	}
	interface otherǁzuihoutouzhu_confirm_XMLǃcontent_ENǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁzuihoutouzhu_confirm_XML
	}
	interface otherǁzuihoutouzhu_confirm_XMLǃcontent_INǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁzuihoutouzhu_confirm_XML
	}
	interface otherǁzuihoutouzhu_confirm_XMLǃcontent_JPǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁzuihoutouzhu_confirm_XML
	}
	interface otherǁzuihoutouzhu_confirm_XMLǃcontent_KRǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁzuihoutouzhu_confirm_XML
	}
	interface otherǁzuihoutouzhu_confirm_XMLǃcontent_THǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁzuihoutouzhu_confirm_XML
	}
	interface otherǁzuihoutouzhu_confirm_XMLǃcontent_VNǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁzuihoutouzhu_confirm_XML
	}
	interface otherǁzuihoutouzhu_confirm_XMLǃcontentǃTEXT extends fairygui.GBasicTextField{
		parent: otherǁzuihoutouzhu_confirm_XML
	}
	interface buttonsǁbutton_quxiao_XMLǃbutton_quxiaoǃCOMPONENT extends buttonsǁbutton_quxiao_XML{
		parent: otherǁzuihoutouzhu_confirm_XML
	}
	interface buttonsǁbutton_queren_XMLǃbutton_querenǃCOMPONENT extends buttonsǁbutton_queren_XML{
		parent: otherǁzuihoutouzhu_confirm_XML
	}
	interface otherǁzuihoutouzhu_confirm_XMLǃn5_ENǃIMAGE extends fairygui.GImage{
		parent: otherǁzuihoutouzhu_confirm_XML
	}
	interface otherǁzuihoutouzhu_confirm_XMLǃn5_INǃIMAGE extends fairygui.GImage{
		parent: otherǁzuihoutouzhu_confirm_XML
	}
	interface otherǁzuihoutouzhu_confirm_XMLǃn5_JPǃIMAGE extends fairygui.GImage{
		parent: otherǁzuihoutouzhu_confirm_XML
	}
	interface otherǁzuihoutouzhu_confirm_XMLǃn5_KRǃIMAGE extends fairygui.GImage{
		parent: otherǁzuihoutouzhu_confirm_XML
	}
	interface otherǁzuihoutouzhu_confirm_XMLǃn5_THǃIMAGE extends fairygui.GImage{
		parent: otherǁzuihoutouzhu_confirm_XML
	}
	interface otherǁzuihoutouzhu_confirm_XMLǃn5_VNǃIMAGE extends fairygui.GImage{
		parent: otherǁzuihoutouzhu_confirm_XML
	}
	interface otherǁzuihoutouzhu_confirm_XMLǃn5_CN2ǃIMAGE extends fairygui.GImage{
		parent: otherǁzuihoutouzhu_confirm_XML
	}
	interface otherǁzuihoutouzhu_confirm_XMLǃn5ǃIMAGE extends fairygui.GImage{
		parent: otherǁzuihoutouzhu_confirm_XML
	}
	interface otherǁzuihoutouzhu_confirm_XMLǃzuihoutouzhu_confirmǃCOMPONENT extends otherǁzuihoutouzhu_confirm_XML{
		parent: Main_XML
	}
	interface MessageBox_XML extends fairygui.GComponent{
		getChild(name: 'n7'): mask_empty_XMLǃn7ǃCOMPONENT
		getChildAt(index: 0): mask_empty_XMLǃn7ǃCOMPONENT
		getChildById(id: 'n7_s20q'): mask_empty_XMLǃn7ǃCOMPONENT
		getChild(name: 'n0'): MessageBox_XMLǃn0ǃIMAGE
		getChildAt(index: 1): MessageBox_XMLǃn0ǃIMAGE
		getChildById(id: 'n0_e1vg'): MessageBox_XMLǃn0ǃIMAGE
		getChild(name: 'n5_EN'): MessageBox_XMLǃn5_ENǃIMAGE
		getChildAt(index: 2): MessageBox_XMLǃn5_ENǃIMAGE
		getChildById(id: 'n5_o9yi_EN'): MessageBox_XMLǃn5_ENǃIMAGE
		getChild(name: 'n5_IN'): MessageBox_XMLǃn5_INǃIMAGE
		getChildAt(index: 3): MessageBox_XMLǃn5_INǃIMAGE
		getChildById(id: 'n5_o9yi_IN'): MessageBox_XMLǃn5_INǃIMAGE
		getChild(name: 'n5_JP'): MessageBox_XMLǃn5_JPǃIMAGE
		getChildAt(index: 4): MessageBox_XMLǃn5_JPǃIMAGE
		getChildById(id: 'n5_o9yi_JP'): MessageBox_XMLǃn5_JPǃIMAGE
		getChild(name: 'n5_KR'): MessageBox_XMLǃn5_KRǃIMAGE
		getChildAt(index: 5): MessageBox_XMLǃn5_KRǃIMAGE
		getChildById(id: 'n5_o9yi_KR'): MessageBox_XMLǃn5_KRǃIMAGE
		getChild(name: 'n5_TH'): MessageBox_XMLǃn5_THǃIMAGE
		getChildAt(index: 6): MessageBox_XMLǃn5_THǃIMAGE
		getChildById(id: 'n5_o9yi_TH'): MessageBox_XMLǃn5_THǃIMAGE
		getChild(name: 'n5_VN'): MessageBox_XMLǃn5_VNǃIMAGE
		getChildAt(index: 7): MessageBox_XMLǃn5_VNǃIMAGE
		getChildById(id: 'n5_o9yi_VN'): MessageBox_XMLǃn5_VNǃIMAGE
		getChild(name: 'n5_CN2'): MessageBox_XMLǃn5_CN2ǃIMAGE
		getChildAt(index: 8): MessageBox_XMLǃn5_CN2ǃIMAGE
		getChildById(id: 'n5_o9yi_CN2'): MessageBox_XMLǃn5_CN2ǃIMAGE
		getChild(name: 'n5'): MessageBox_XMLǃn5ǃIMAGE
		getChildAt(index: 9): MessageBox_XMLǃn5ǃIMAGE
		getChildById(id: 'n5_o9yi'): MessageBox_XMLǃn5ǃIMAGE
		getChild(name: 'content_CN2'): MessageBox_XMLǃcontent_CN2ǃTEXT
		getChildAt(index: 10): MessageBox_XMLǃcontent_CN2ǃTEXT
		getChildById(id: 'n1_e1vg_CN2'): MessageBox_XMLǃcontent_CN2ǃTEXT
		getChild(name: 'content_EN'): MessageBox_XMLǃcontent_ENǃTEXT
		getChildAt(index: 11): MessageBox_XMLǃcontent_ENǃTEXT
		getChildById(id: 'n1_e1vg_EN'): MessageBox_XMLǃcontent_ENǃTEXT
		getChild(name: 'content_IN'): MessageBox_XMLǃcontent_INǃTEXT
		getChildAt(index: 12): MessageBox_XMLǃcontent_INǃTEXT
		getChildById(id: 'n1_e1vg_IN'): MessageBox_XMLǃcontent_INǃTEXT
		getChild(name: 'content_JP'): MessageBox_XMLǃcontent_JPǃTEXT
		getChildAt(index: 13): MessageBox_XMLǃcontent_JPǃTEXT
		getChildById(id: 'n1_e1vg_JP'): MessageBox_XMLǃcontent_JPǃTEXT
		getChild(name: 'content_KR'): MessageBox_XMLǃcontent_KRǃTEXT
		getChildAt(index: 14): MessageBox_XMLǃcontent_KRǃTEXT
		getChildById(id: 'n1_e1vg_KR'): MessageBox_XMLǃcontent_KRǃTEXT
		getChild(name: 'content_TH'): MessageBox_XMLǃcontent_THǃTEXT
		getChildAt(index: 15): MessageBox_XMLǃcontent_THǃTEXT
		getChildById(id: 'n1_e1vg_TH'): MessageBox_XMLǃcontent_THǃTEXT
		getChild(name: 'content_VN'): MessageBox_XMLǃcontent_VNǃTEXT
		getChildAt(index: 16): MessageBox_XMLǃcontent_VNǃTEXT
		getChildById(id: 'n1_e1vg_VN'): MessageBox_XMLǃcontent_VNǃTEXT
		getChild(name: 'content'): MessageBox_XMLǃcontentǃTEXT
		getChildAt(index: 17): MessageBox_XMLǃcontentǃTEXT
		getChildById(id: 'n1_e1vg'): MessageBox_XMLǃcontentǃTEXT
		getChild(name: 'btn_queren'): buttonsǁbutton_queren_XMLǃbtn_querenǃCOMPONENT
		getChildAt(index: 18): buttonsǁbutton_queren_XMLǃbtn_querenǃCOMPONENT
		getChildById(id: 'n3_e1vg'): buttonsǁbutton_queren_XMLǃbtn_querenǃCOMPONENT
		getChild(name: 'countdown'): MessageBox_XMLǃcountdownǃTEXT
		getChildAt(index: 19): MessageBox_XMLǃcountdownǃTEXT
		getChildById(id: 'n4_xojn'): MessageBox_XMLǃcountdownǃTEXT
		_children: [
			mask_empty_XMLǃn7ǃCOMPONENT,
			MessageBox_XMLǃn0ǃIMAGE,
			MessageBox_XMLǃn5_ENǃIMAGE,
			MessageBox_XMLǃn5_INǃIMAGE,
			MessageBox_XMLǃn5_JPǃIMAGE,
			MessageBox_XMLǃn5_KRǃIMAGE,
			MessageBox_XMLǃn5_THǃIMAGE,
			MessageBox_XMLǃn5_VNǃIMAGE,
			MessageBox_XMLǃn5_CN2ǃIMAGE,
			MessageBox_XMLǃn5ǃIMAGE,
			MessageBox_XMLǃcontent_CN2ǃTEXT,
			MessageBox_XMLǃcontent_ENǃTEXT,
			MessageBox_XMLǃcontent_INǃTEXT,
			MessageBox_XMLǃcontent_JPǃTEXT,
			MessageBox_XMLǃcontent_KRǃTEXT,
			MessageBox_XMLǃcontent_THǃTEXT,
			MessageBox_XMLǃcontent_VNǃTEXT,
			MessageBox_XMLǃcontentǃTEXT,
			buttonsǁbutton_queren_XMLǃbtn_querenǃCOMPONENT,
			MessageBox_XMLǃcountdownǃTEXT
		]
		getController(name: '__language'): MessageBox_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): MessageBox_XMLǃ__languageǃCONTROLLER
		_controllers: [
			MessageBox_XMLǃ__languageǃCONTROLLER
		]
	}
	interface MessageBox_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: MessageBox_XML
	}
	interface mask_empty_XMLǃn7ǃCOMPONENT extends mask_empty_XML{
		parent: MessageBox_XML
	}
	interface MessageBox_XMLǃn0ǃIMAGE extends fairygui.GImage{
		parent: MessageBox_XML
	}
	interface MessageBox_XMLǃn5_ENǃIMAGE extends fairygui.GImage{
		parent: MessageBox_XML
	}
	interface MessageBox_XMLǃn5_INǃIMAGE extends fairygui.GImage{
		parent: MessageBox_XML
	}
	interface MessageBox_XMLǃn5_JPǃIMAGE extends fairygui.GImage{
		parent: MessageBox_XML
	}
	interface MessageBox_XMLǃn5_KRǃIMAGE extends fairygui.GImage{
		parent: MessageBox_XML
	}
	interface MessageBox_XMLǃn5_THǃIMAGE extends fairygui.GImage{
		parent: MessageBox_XML
	}
	interface MessageBox_XMLǃn5_VNǃIMAGE extends fairygui.GImage{
		parent: MessageBox_XML
	}
	interface MessageBox_XMLǃn5_CN2ǃIMAGE extends fairygui.GImage{
		parent: MessageBox_XML
	}
	interface MessageBox_XMLǃn5ǃIMAGE extends fairygui.GImage{
		parent: MessageBox_XML
	}
	interface MessageBox_XMLǃcontent_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: MessageBox_XML
	}
	interface MessageBox_XMLǃcontent_ENǃTEXT extends fairygui.GBasicTextField{
		parent: MessageBox_XML
	}
	interface MessageBox_XMLǃcontent_INǃTEXT extends fairygui.GBasicTextField{
		parent: MessageBox_XML
	}
	interface MessageBox_XMLǃcontent_JPǃTEXT extends fairygui.GBasicTextField{
		parent: MessageBox_XML
	}
	interface MessageBox_XMLǃcontent_KRǃTEXT extends fairygui.GBasicTextField{
		parent: MessageBox_XML
	}
	interface MessageBox_XMLǃcontent_THǃTEXT extends fairygui.GBasicTextField{
		parent: MessageBox_XML
	}
	interface MessageBox_XMLǃcontent_VNǃTEXT extends fairygui.GBasicTextField{
		parent: MessageBox_XML
	}
	interface MessageBox_XMLǃcontentǃTEXT extends fairygui.GBasicTextField{
		parent: MessageBox_XML
	}
	interface buttonsǁbutton_queren_XMLǃbtn_querenǃCOMPONENT extends buttonsǁbutton_queren_XML{
		parent: MessageBox_XML
	}
	interface MessageBox_XMLǃcountdownǃTEXT extends fairygui.GBasicTextField{
		parent: MessageBox_XML
	}
	interface MessageBox_XMLǃmessage_boxǃCOMPONENT extends MessageBox_XML{
		parent: Main_XML
	}
	interface MessageBox_XMLǃreconnect_boxǃCOMPONENT extends MessageBox_XML{
		parent: Main_XML
	}
	interface message_confirm_XML extends fairygui.GComponent{
		getChild(name: 'n0'): message_confirm_XMLǃn0ǃIMAGE
		getChildAt(index: 0): message_confirm_XMLǃn0ǃIMAGE
		getChildById(id: 'n0_e1vg'): message_confirm_XMLǃn0ǃIMAGE
		getChild(name: 'n7_EN'): message_confirm_XMLǃn7_ENǃIMAGE
		getChildAt(index: 1): message_confirm_XMLǃn7_ENǃIMAGE
		getChildById(id: 'n7_o9yi_EN'): message_confirm_XMLǃn7_ENǃIMAGE
		getChild(name: 'n7_IN'): message_confirm_XMLǃn7_INǃIMAGE
		getChildAt(index: 2): message_confirm_XMLǃn7_INǃIMAGE
		getChildById(id: 'n7_o9yi_IN'): message_confirm_XMLǃn7_INǃIMAGE
		getChild(name: 'n7_JP'): message_confirm_XMLǃn7_JPǃIMAGE
		getChildAt(index: 3): message_confirm_XMLǃn7_JPǃIMAGE
		getChildById(id: 'n7_o9yi_JP'): message_confirm_XMLǃn7_JPǃIMAGE
		getChild(name: 'n7_KR'): message_confirm_XMLǃn7_KRǃIMAGE
		getChildAt(index: 4): message_confirm_XMLǃn7_KRǃIMAGE
		getChildById(id: 'n7_o9yi_KR'): message_confirm_XMLǃn7_KRǃIMAGE
		getChild(name: 'n7_TH'): message_confirm_XMLǃn7_THǃIMAGE
		getChildAt(index: 5): message_confirm_XMLǃn7_THǃIMAGE
		getChildById(id: 'n7_o9yi_TH'): message_confirm_XMLǃn7_THǃIMAGE
		getChild(name: 'n7_VN'): message_confirm_XMLǃn7_VNǃIMAGE
		getChildAt(index: 6): message_confirm_XMLǃn7_VNǃIMAGE
		getChildById(id: 'n7_o9yi_VN'): message_confirm_XMLǃn7_VNǃIMAGE
		getChild(name: 'n7_CN2'): message_confirm_XMLǃn7_CN2ǃIMAGE
		getChildAt(index: 7): message_confirm_XMLǃn7_CN2ǃIMAGE
		getChildById(id: 'n7_o9yi_CN2'): message_confirm_XMLǃn7_CN2ǃIMAGE
		getChild(name: 'n7'): message_confirm_XMLǃn7ǃIMAGE
		getChildAt(index: 8): message_confirm_XMLǃn7ǃIMAGE
		getChildById(id: 'n7_o9yi'): message_confirm_XMLǃn7ǃIMAGE
		getChild(name: 'content_CN2'): message_confirm_XMLǃcontent_CN2ǃTEXT
		getChildAt(index: 9): message_confirm_XMLǃcontent_CN2ǃTEXT
		getChildById(id: 'n1_e1vg_CN2'): message_confirm_XMLǃcontent_CN2ǃTEXT
		getChild(name: 'content_EN'): message_confirm_XMLǃcontent_ENǃTEXT
		getChildAt(index: 10): message_confirm_XMLǃcontent_ENǃTEXT
		getChildById(id: 'n1_e1vg_EN'): message_confirm_XMLǃcontent_ENǃTEXT
		getChild(name: 'content_IN'): message_confirm_XMLǃcontent_INǃTEXT
		getChildAt(index: 11): message_confirm_XMLǃcontent_INǃTEXT
		getChildById(id: 'n1_e1vg_IN'): message_confirm_XMLǃcontent_INǃTEXT
		getChild(name: 'content_JP'): message_confirm_XMLǃcontent_JPǃTEXT
		getChildAt(index: 12): message_confirm_XMLǃcontent_JPǃTEXT
		getChildById(id: 'n1_e1vg_JP'): message_confirm_XMLǃcontent_JPǃTEXT
		getChild(name: 'content_KR'): message_confirm_XMLǃcontent_KRǃTEXT
		getChildAt(index: 13): message_confirm_XMLǃcontent_KRǃTEXT
		getChildById(id: 'n1_e1vg_KR'): message_confirm_XMLǃcontent_KRǃTEXT
		getChild(name: 'content_TH'): message_confirm_XMLǃcontent_THǃTEXT
		getChildAt(index: 14): message_confirm_XMLǃcontent_THǃTEXT
		getChildById(id: 'n1_e1vg_TH'): message_confirm_XMLǃcontent_THǃTEXT
		getChild(name: 'content_VN'): message_confirm_XMLǃcontent_VNǃTEXT
		getChildAt(index: 15): message_confirm_XMLǃcontent_VNǃTEXT
		getChildById(id: 'n1_e1vg_VN'): message_confirm_XMLǃcontent_VNǃTEXT
		getChild(name: 'content'): message_confirm_XMLǃcontentǃTEXT
		getChildAt(index: 16): message_confirm_XMLǃcontentǃTEXT
		getChildById(id: 'n1_e1vg'): message_confirm_XMLǃcontentǃTEXT
		getChild(name: 'btn_quxiao'): buttonsǁbutton_quxiao_XMLǃbtn_quxiaoǃCOMPONENT
		getChildAt(index: 17): buttonsǁbutton_quxiao_XMLǃbtn_quxiaoǃCOMPONENT
		getChildById(id: 'n5_l9o9'): buttonsǁbutton_quxiao_XMLǃbtn_quxiaoǃCOMPONENT
		_children: [
			message_confirm_XMLǃn0ǃIMAGE,
			message_confirm_XMLǃn7_ENǃIMAGE,
			message_confirm_XMLǃn7_INǃIMAGE,
			message_confirm_XMLǃn7_JPǃIMAGE,
			message_confirm_XMLǃn7_KRǃIMAGE,
			message_confirm_XMLǃn7_THǃIMAGE,
			message_confirm_XMLǃn7_VNǃIMAGE,
			message_confirm_XMLǃn7_CN2ǃIMAGE,
			message_confirm_XMLǃn7ǃIMAGE,
			message_confirm_XMLǃcontent_CN2ǃTEXT,
			message_confirm_XMLǃcontent_ENǃTEXT,
			message_confirm_XMLǃcontent_INǃTEXT,
			message_confirm_XMLǃcontent_JPǃTEXT,
			message_confirm_XMLǃcontent_KRǃTEXT,
			message_confirm_XMLǃcontent_THǃTEXT,
			message_confirm_XMLǃcontent_VNǃTEXT,
			message_confirm_XMLǃcontentǃTEXT,
			buttonsǁbutton_quxiao_XMLǃbtn_quxiaoǃCOMPONENT
		]
		getController(name: '__language'): message_confirm_XMLǃ__languageǃCONTROLLER
		getControllerAt(index: 0): message_confirm_XMLǃ__languageǃCONTROLLER
		_controllers: [
			message_confirm_XMLǃ__languageǃCONTROLLER
		]
	}
	interface message_confirm_XMLǃ__languageǃCONTROLLER extends fairygui.Controller{
		_parent: message_confirm_XML
	}
	interface message_confirm_XMLǃn0ǃIMAGE extends fairygui.GImage{
		parent: message_confirm_XML
	}
	interface message_confirm_XMLǃn7_ENǃIMAGE extends fairygui.GImage{
		parent: message_confirm_XML
	}
	interface message_confirm_XMLǃn7_INǃIMAGE extends fairygui.GImage{
		parent: message_confirm_XML
	}
	interface message_confirm_XMLǃn7_JPǃIMAGE extends fairygui.GImage{
		parent: message_confirm_XML
	}
	interface message_confirm_XMLǃn7_KRǃIMAGE extends fairygui.GImage{
		parent: message_confirm_XML
	}
	interface message_confirm_XMLǃn7_THǃIMAGE extends fairygui.GImage{
		parent: message_confirm_XML
	}
	interface message_confirm_XMLǃn7_VNǃIMAGE extends fairygui.GImage{
		parent: message_confirm_XML
	}
	interface message_confirm_XMLǃn7_CN2ǃIMAGE extends fairygui.GImage{
		parent: message_confirm_XML
	}
	interface message_confirm_XMLǃn7ǃIMAGE extends fairygui.GImage{
		parent: message_confirm_XML
	}
	interface message_confirm_XMLǃcontent_CN2ǃTEXT extends fairygui.GBasicTextField{
		parent: message_confirm_XML
	}
	interface message_confirm_XMLǃcontent_ENǃTEXT extends fairygui.GBasicTextField{
		parent: message_confirm_XML
	}
	interface message_confirm_XMLǃcontent_INǃTEXT extends fairygui.GBasicTextField{
		parent: message_confirm_XML
	}
	interface message_confirm_XMLǃcontent_JPǃTEXT extends fairygui.GBasicTextField{
		parent: message_confirm_XML
	}
	interface message_confirm_XMLǃcontent_KRǃTEXT extends fairygui.GBasicTextField{
		parent: message_confirm_XML
	}
	interface message_confirm_XMLǃcontent_THǃTEXT extends fairygui.GBasicTextField{
		parent: message_confirm_XML
	}
	interface message_confirm_XMLǃcontent_VNǃTEXT extends fairygui.GBasicTextField{
		parent: message_confirm_XML
	}
	interface message_confirm_XMLǃcontentǃTEXT extends fairygui.GBasicTextField{
		parent: message_confirm_XML
	}
	interface buttonsǁbutton_quxiao_XMLǃbtn_quxiaoǃCOMPONENT extends buttonsǁbutton_quxiao_XML{
		parent: message_confirm_XML
	}
	interface message_confirm_XMLǃleave_confirmǃCOMPONENT extends message_confirm_XML{
		parent: Main_XML
	}
	interface Main_XMLǃn273ǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn273_CN2ǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn273_ENǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn273_INǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn273_JPǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn273_KRǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn273_THǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃn273_VNǃIMAGE extends fairygui.GImage{
		parent: Main_XML
	}
	interface Main_XMLǃt0ǃTRANSITION extends fairygui.Transition{
		_owner: Main_XML
	}
	interface Main_XMLǃt1ǃTRANSITION extends fairygui.Transition{
		_owner: Main_XML
	}
	interface Main_XMLǃt3ǃTRANSITION extends fairygui.Transition{
		_owner: Main_XML
	}
	interface Main_XMLǃt4ǃTRANSITION extends fairygui.Transition{
		_owner: Main_XML
	}
}