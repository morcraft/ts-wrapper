import { BetTypesIndex, BetTypes } from './BetTypes'
type Game = {
    _view: mainUI.Main_XML | mobileMainUI.Main_XML
    _betTypes: BetTypes[]
    _betTypesIndex: BetTypesIndex
    PlayGameSound: Function
    emit: Function
    onFaSongLiaoTian: Function
    timeleft: number
    ping: number
    filePreName: string
    initButtonMouseStyle: Function
    _leaderboardController: fairygui.Controller
    _danmuCtrl: fairygui.Controller
    onSetChouMa: Function
    showMessageBox: Function
}

export default Game