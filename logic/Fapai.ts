import Game from './Game'
import Handler = Laya.Handler
import SoundManager = laya.media.SoundManager

declare function MyLogger(...a)

export default class Fapai{
    _game: Game
    _view: mainUI.Main_XML | mobileMainUI.Main_XML
    _xianbu: fairygui.GObject
    _zhuangbu: fairygui.GObject
    _fapai_ani: fairygui.GMovieClip
    _fapai_controller: fairygui.Controller
    _fapai_cards: fairygui.GComponent[]
    counterdownTimerId: any
    _fapai_centerText: fairygui.GTextField
    _fapai_countdownText: fairygui.GTextField
    _fapai_selectIndex_arr: any[]
    _fapai_zhuang: any[]
    _fapai_xian: any[]
    _dianshu_zhuang: any[]
    _dianshu_xian: any[]
    _fapai_laststate: number
    _gameState: number
    _stateCounter: number
    _receivedCounterTime: number
    counter: number
    backupZhuang: any
    backupXian: any
    _totalCardNums: number
    _isRecovering: boolean
    _zhuangdianshu: fairygui.GTextField
    _xiandianshu: fairygui.GTextField
    _step03done: boolean
    _hasXianpai: boolean
    _step01done: boolean
    _step02done: boolean

    constructor(game: Game) {
        this._view = game._view
        this._game = game
        this.init()
    }

    init = () => {
        this._xianbu = this._view.getChild('xianbu') // 闲补牌的 补字
        this._zhuangbu = this._view.getChild('zhuangbu') // 庄补牌的 补字
        this._xianbu.visible = false
        this._zhuangbu.visible = false
        // 发牌洗牌
        this._fapai_ani = this._view.getChild('ani_xipai').asMovieClip
        this._fapai_controller = this._view.getController('controller_fapai')
        this._fapai_cards = []
        for (let i = 1; i <=8; ++i){
            //@ts-ignore
            this._fapai_cards[i] = this._view.getChild('fapai_0'+i).asCom
        }

        this.setControllerIndex(1, true)

        this.counterdownTimerId = null
        this._fapai_centerText = this._view.getChild('text_center').asTextField
        this._fapai_centerText.text = ''
        this._fapai_countdownText = this._view.getChild('text_countdown').asTextField
        this._fapai_countdownText.visible = false

        this._fapai_ani.visible = false
        this._fapai_selectIndex_arr = []
        this._fapai_zhuang = []
        this._fapai_xian = []
        this._dianshu_zhuang = []
        this._dianshu_xian = []
        this._fapai_laststate = 0

        this._gameState = 0
        this._stateCounter = 0
        this._receivedCounterTime = 0
		this.counter = 0
        this.backupZhuang = null
        this.backupXian = null

        this._zhuangdianshu = this._view.getChild('text_zhuangdianshu').asTextField
        this._xiandianshu = this._view.getChild('text_xiandianshu').asTextField
        this._zhuangdianshu.visible = false
        this._xiandianshu.visible = false
    }

    dianshu = (arr: number[]) => {
        const result = []
        for(let i = 0; i < arr.length; i++){
            let value = Math.floor(arr[i] % 13) + 1
            if(value >= 10){
                value = 0
            }
            result[i] = value
        }

        return result
    }

    cards(arr: number[]){
        const result = []
        for(let i = 0; i < arr.length; i++){
            result[i] = this.card(arr[i])
        }
        return result
    }

    card = (n: number) => {
        const kind = Math.floor(n / 13)
        const value = Math.floor(n % 13) + 1
        const kinds = ['heitao', 'hongxin', 'fangpian', 'caohua']
        return kinds[kind] + '_' + value
    }

    updateState = (state, counter: number) => {
       this._gameState = state
       this._stateCounter = counter
       this._receivedCounterTime = Date.now()
       this.updateFapaiCenterText(state, counter)
    }

    getStateCounter = () => this._stateCounter - Math.round((Date.now() - this._receivedCounterTime) / 1000)
    
    getTouzhuCounter = () => {
        if (this._gameState != 0)
            return -1

        return this._stateCounter - (Date.now() - this._receivedCounterTime) / 1000
    }

    updateFapaiCenterText = (state, counter) => {
        var newCounter = Math.round(counter - this._game.ping / 1000)
        if (Math.abs(this.counter - newCounter) < 3) 
            return
        
        this.counter = newCounter
        
        var self = this
        window.clearInterval(self.counterdownTimerId) 
        MyLogger('updateState counter: ' + counter)
        if (this.counter <= 0){
            this.counter = 1
        }
        if (state == 0){
            // 下注中
            const t1Text = self._view.getTransition('t1')
            self._fapai_centerText.text  = '投注中...'
            self._fapai_countdownText.text = self.counter + '秒' 
            self.counterdownTimerId =window.setInterval(function(){
                self.counter -= 1
                if(self.counter <= 28){
                    self._fapai_countdownText.visible = true
                }
                self._game.timeleft = self.counter
                if (self.counter >= 1){
                    var txtstr = self.counter+'秒'
                    self._fapai_countdownText.text  = txtstr

                    if (self.counter <= 5){
                        var col = self._fapai_countdownText.color
                        t1Text.play(Handler.create(self, self.onTextAniPlayend, [self._fapai_countdownText, col, txtstr]))
                        //SoundManager.playSound('res/sounds/countdown.mp3', 1)
                        self._game.PlayGameSound('countdown.mp3', 1)
                    }
                }
            },1000)
        }
        else if (state == 2){
            // 开奖中       
            self._fapai_countdownText.visible = false
            self._fapai_countdownText.text = ''
            self._fapai_centerText.text  = '发牌中'
        }
        else if (state == 3){
            // 开奖中       
            self._fapai_centerText.text  = '开奖中'
        }
    }

    onTextAniPlayend = (text, col, txtstr) => {
        text.text = null
        text.text = txtstr
        text.color = '#FFFF99'
    }

    firstEnterView = (result) => {
        //this.onVisibilityChange()
        if (result){
            this.backupZhuang = result.zhuang
            this.backupXian = result.xian
            //this.startFapai(result.zhuang,result.xian)
        }

        this.recover(false)
        Laya.stage.on('visibilitychange', this, this.onVisibilityChange)
    }

    onVisibilityChange = () => {
        if(Laya.stage.isVisibility){
            this.recover(true)
        } 
        else {
            Laya.timer.clear(this, this.onXianFanpaiEnd)
        }
    }
    
    onShouPaiEnd = () => {
        this._fapai_cards[this._totalCardNums].off(fairygui.Events.GEAR_STOP, this, this.onShouPaiEnd)
        for (let i = 1; i <=8 ; ++i){
            //@ts-ignore
            this._fapai_cards[i].gearXY.delay = i*0.1
        }
        this.setControllerIndex(1)
    }

    OnfapaiStep03 = (forceRecover?: boolean) => {

        // if(!Laya.stage.isVisibility) return

        MyLogger("OnfapaiStep03 this._fapai_laststate="+this._fapai_laststate + ", step03done: " + this._step03done)

        if(this._isRecovering && forceRecover != true) return

        if (this._fapai_zhuang.length < 3)
            return
        // 翻第6张牌
         this.Fanpai(this._fapai_cards[6], this._fapai_zhuang[2], this._step03done)
        //  SoundManager.playSound("res/sounds/fanpai.mp3", 1)


        this._fapai_cards[6].off(fairygui.Events.GEAR_STOP,this, this.OnfapaiStep03)

        Laya.timer.once(3200, this, function() {
            var ds = (this._dianshu_zhuang[0] + this._dianshu_zhuang[1] + this._dianshu_zhuang[2]) % 10
            this.setDianShuText(this._zhuangdianshu, ds)
        })

        this._step03done = true
    }

    OnfapaiStep02 = (forceRecover?: boolean) => {

        // if(!Laya.stage.isVisibility) return
        MyLogger("OnfapaiStep02 this._fapai_laststate="+this._fapai_laststate + ", step02done: " + this._step02done)

        if(this._isRecovering && forceRecover != true) return

        this.setFapaiSelectedIndexArr()
        this._fapai_selectIndex_arr.splice(0, 1)

        // 翻第5张牌
        if (this._fapai_laststate == 4){
            // 闲家
            if (this._fapai_xian.length < 3)
                return
            // SoundManager.playSound("res/sounds/fanpai.mp3", 1)
            this.Fanpai(this._fapai_cards[5], this._fapai_xian[2], this._step02done)
            
            Laya.timer.once(3200, this, function() {
                const ds = (this._dianshu_xian[0] + this._dianshu_xian[1] + this._dianshu_xian[2]) % 10
                this.setDianShuText(this._xiandianshu, ds)
            })
        } 
        else if (this._fapai_laststate == 5) {
            // 庄家
            if (this._fapai_zhuang.length < 3)
                return
            // SoundManager.playSound("res/sounds/fanpai.mp3", 1)
            this.Fanpai(this._fapai_cards[5], this._fapai_zhuang[2], this._step02done)

            Laya.timer.once(3200, this, function() {
                const ds = (this._dianshu_zhuang[0] + this._dianshu_zhuang[1] + this._dianshu_zhuang[2]) % 10
                this.setDianShuText(this._zhuangdianshu, ds)
            })
        }

        if (this._fapai_selectIndex_arr.length > 0){
            //SoundManager.playSound("res/sounds/fapai.mp3", 1)
            // 发第6张牌
            //@ts-ignore
            this._fapai_cards[6].gearXY.delay = 3.5
            const index = this._fapai_selectIndex_arr[0]
            this._fapai_laststate  = index

            this.setControllerIndex(this._fapai_laststate, this._step02done)
            this._fapai_selectIndex_arr.splice(0,1)

            this._fapai_cards[6].on(fairygui.Events.GEAR_STOP,this, this.OnfapaiStep03, [forceRecover])

            const self = this
            Laya.timer.once(3200, this, function() {
                self._zhuangbu.visible = true
            })
        }

        this._fapai_cards[5].off(fairygui.Events.GEAR_STOP,this, this.OnfapaiStep02)

        this._step02done = true
    }

    onFanpaiAniPlayend = (controller: fairygui.Controller, value: string) => {
        controller.selectedPage = value
        //SoundManager.playSound('res/sounds/fanpai.mp3', 1)
        this._game.PlayGameSound('fanpai.mp3', 1)
    }

    Fanpai = (obj, value, skipAni) => {
    
        var controller = obj.getController('c1')
        if (skipAni) {
            var playing = obj.getTransition('t2').playing
            
            if(playing) {
                obj.getTransition('t2').stop(true)
                obj.getChild('face').visible = false
                obj.getChild('finger').visible = false
            }

            obj.getChild('n2').visible = true
            controller.selectedPage = value
            return
        }
         var anim = obj.getTransition('t4')
         var mipaiTrans = obj.getTransition('t2')

         if(anim.playing) {
            anim.stop(true)
            obj.getChild('face1').visible = false
            obj.getChild('finger1').visible = false
         }
         if(mipaiTrans.playing) {
             mipaiTrans.stop(true)
             obj.getChild('face').visible = false
             obj.getChild('finger').visible = false
         }
         
         obj.getChild('face').url = 'ui://' + this._game.filePreName + 'MainUI/' + value
         obj.getChild('face1').url = 'ui://' + this._game.filePreName + 'MainUI/' + value
         var self= this
        // mipaiTrans.play(Handler.create(self, self.onFanpaiAniPlayend, [controller, value]))
         anim.play(Handler.create(this, function(controller, value,obj){
            obj.getChild('face1').visible = false
            obj.getChild('finger1').visible = false
            obj.getChild('n61').visible = false
            obj.getChild('mask').height = 200
            mipaiTrans.play(Handler.create(self, self.onFanpaiAniPlayend, [controller, value]))
        }, [controller, value,obj]))
    }

    onXianFanpaiEnd = (skip) => {
       
        var forceRecover = skip

        if(this._isRecovering && forceRecover != true) return

        this.setFapaiSelectedIndexArr()

        var ds = (this._dianshu_xian[0] + this._dianshu_xian[1]) % 10
        // if(this._fapai_xian.length < 3)
        this.setDianShuText(this._xiandianshu, ds)

        if(skip != true) {
            this.Fanpai(this._fapai_cards[2], this._fapai_zhuang[0], this._step01done)
            this.Fanpai(this._fapai_cards[4], this._fapai_zhuang[1], this._step01done)
            // SoundManager.playSound("res/sounds/fanpai.mp3", 1)
        }

        if (this._fapai_selectIndex_arr.length > 0){
            //SoundManager.playSound("res/sounds/fapai.mp3", 1)
            // 发第5张牌
            //@ts-ignore
            this._fapai_cards[5].gearXY.delay = 0.2//3.5
            var index = this._fapai_selectIndex_arr[0]

            this._fapai_laststate  = index
            this.setControllerIndex(this._fapai_laststate, this._step01done)
            this._fapai_selectIndex_arr.splice(0,1)
            this._fapai_cards[5].on(fairygui.Events.GEAR_STOP,this, this.OnfapaiStep02, [forceRecover])

            var self = this
            Laya.timer.once(200, this, function() {
                if(self._fapai_laststate == 4) {
                    self._xianbu.visible = true
                } else {
                    self._zhuangbu.visible = true
                }
            })
        }

        this._fapai_cards[4].off(fairygui.Events.GEAR_STOP,this, this.OnfapaiStep01)

       // Laya.timer.once(3200, this, function() {
            var ds = (this._dianshu_zhuang[0] + this._dianshu_zhuang[1]) % 10
            // if (this._fapai_zhuang.length < 3)
            this.setDianShuText(this._zhuangdianshu, ds)
       // })

        this._step01done = true
    }

    setDianShuText = (target, value) => {
        target.text = value.toString() + "点"
        target.visible = true
    }

    OnfapaiStep01 = () => {

        MyLogger("OnfapaiStep01 this._fapai_laststate="+this._fapai_laststate)
        if (this._fapai_zhuang.length < 2 || this._fapai_xian.length < 2)
            return
        // 翻前面4张牌
        this.Fanpai(this._fapai_cards[1], this._fapai_xian[0], this._step01done)
        this.Fanpai(this._fapai_cards[3], this._fapai_xian[1], this._step01done)
        this.Fanpai(this._fapai_cards[2], this._fapai_zhuang[0], this._step01done)
        this.Fanpai(this._fapai_cards[4], this._fapai_zhuang[1], this._step01done)
        // SoundManager.playSound("res/sounds/fanpai.mp3", 1)

        Laya.timer.once(3300, this, this.onXianFanpaiEnd, [true])
        
    }

    onPlayendXipai = () => {
        var self = this
        self._fapai_ani.visible = false
        // this._fapai_selectIndex_arr = []
        // if (this._fapai_xian.length == 3)
        // {
        //     this._fapai_selectIndex_arr[0] = 4 // 闲家博牌
        //     if (this._fapai_zhuang.length == 3)
        //     {
        //         this._fapai_selectIndex_arr[1] = 6 // 庄家博牌
        //     }
        // }
        // else
        // {
        //     if (this._fapai_zhuang.length == 3)
        //     {
        //         this._fapai_selectIndex_arr[0] = 5 // 庄家博牌
        //     }
        // }

        // 调整延迟时间
        for (var i = 1; i <=4 ; ++i){
            //@ts-ignore
            this._fapai_cards[i].gearXY.delay = (i-1)*0.3 // 2s
            Laya.timer.once(10 + (i-1)*300,this,this.playFapaiSound)
        }
        
        this._fapai_laststate  = 3     
        this.setControllerIndex(this._fapai_laststate)
        // this._fapai_cards[4].on(fairygui.Events.GEAR_STOP, this, this.OnfapaiStep01)
    }

    startFapaiFirst4 = () => {
        this.setControllerIndex(2, true)
        var self = this
        self._fapai_ani.visible = true
        self._fapai_ani.playing = true 
        self._fapai_ani.setPlaySettings(0, -1, 3, 1,  Handler.create(self, self.onPlayendXipai))
        //SoundManager.playSound("res/sounds/xipai.mp3", 2)
        this._game.PlayGameSound("xipai.mp3", 1)
    }

    startFapai = (zhuang,xian) => {
        this._totalCardNums = zhuang.length + xian.length

        this._fapai_zhuang = this.cards(zhuang)
        this._fapai_xian = this.cards(xian)
        this._dianshu_zhuang = this.dianshu(zhuang)
        this._dianshu_xian = this.dianshu(xian)
        this.backupZhuang = zhuang
        this.backupXian = xian

        this._fapai_selectIndex_arr = []
        if (this._fapai_xian.length == 3){
            this._fapai_selectIndex_arr[0] = 4 // 闲家博牌
            if (this._fapai_zhuang.length == 3){
                this._fapai_selectIndex_arr[1] = 6 // 庄家博牌
            }
        }
        else{
            if (this._fapai_zhuang.length == 3){
                this._fapai_selectIndex_arr[0] = 5 // 庄家博牌
            }
        }

        // 调整延迟时间
        for (var i = 1; i <=4 ; ++i){
            //@ts-ignore
            this._fapai_cards[i].gearXY.delay = (i-1)*0.5 // 2s
        }

        // if(!Laya.stage.isVisibility)
        //     return

        this.OnfapaiStep01()


        MyLogger("startFapai, zhuang:"+this._fapai_zhuang  +" xian:"+this._fapai_xian)
        
        // var self = this
        // self._fapai_ani.visible = true
        // self._fapai_ani.playing = true 
        // self._fapai_ani.setPlaySettings(0, -1, 3, 1,  Handler.create(self, self.onPlayendXipai))
        // SoundManager.playSound("res/sounds/xipai.mp3", 2)

        

    }

    setFapaiSelectedIndexArr = () => {
        this._fapai_selectIndex_arr = []
        if (this._fapai_xian.length == 3){
            this._fapai_selectIndex_arr[0] = 4 // 闲家博牌
            if (this._fapai_zhuang.length == 3){
                this._fapai_selectIndex_arr[1] = 6 // 庄家博牌
            }
        }
        else{
            if (this._fapai_zhuang.length == 3){
                this._fapai_selectIndex_arr[0] = 5 // 庄家博牌
            }
        }
    }

    playFapaiSound = (i) => {
        //this._fapai_cards[i].off(fairygui.Events.GEAR_STOP, this, this.playFapaiSound)
        SoundManager.playSound("res/sounds/fapai.mp3", 1)
    }

    clearAll = () => {
        for (let i = 1; i <= 8; ++i){
            this.Fanpai(this._fapai_cards[i], 'paidi', true)
            this._fapai_cards[i].getChild('n2').skewY = 0
        }
       
        for (let i = 1; i <=8 ; ++i){
            //@ts-ignore
            this._fapai_cards[i].gearXY.delay = i*0.1
        }

        for (let i = 4; i <= 6; ++i){
            this._fapai_cards[i].off(fairygui.Events.GEAR_STOP, this, this.onShouPaiEnd)
            this._fapai_cards[i].off(fairygui.Events.GEAR_STOP,this, this.OnfapaiStep01)
            this._fapai_cards[i].off(fairygui.Events.GEAR_STOP,this, this.OnfapaiStep02)
            this._fapai_cards[i].off(fairygui.Events.GEAR_STOP,this, this.OnfapaiStep03)
            this._fapai_cards[i].off(fairygui.Events.GEAR_STOP, this, this.playFapaiSound)
        }

        this._fapai_ani.visible = false
        this._fapai_ani.playing = false
        this._fapai_selectIndex_arr = []
        this._fapai_zhuang=[]
        this._fapai_xian = []
        this._fapai_laststate = 0

        this._xianbu.visible = false
        this._zhuangbu.visible = false
    }

    recover = (isRecovering: boolean) => {
        
        this._isRecovering = isRecovering

        this.clearAll()

        this._step01done = false
        this._step02done = false
        this._step03done = false

        this.setControllerIndex(3, true)
        
        if (this.backupZhuang != null && this.backupXian != null){
            const zhuang = this.backupZhuang
            const xian = this.backupXian
            this._fapai_zhuang = this.cards(zhuang)
            this._fapai_xian = this.cards(xian)
            this._dianshu_zhuang = this.dianshu(zhuang)
            this._dianshu_xian = this.dianshu(xian)
            this._totalCardNums = zhuang.length + xian.length


            this._fapai_selectIndex_arr = []
            if (this._fapai_xian.length == 3){
                this._fapai_selectIndex_arr[0] = 4 // 闲家博牌
                if (this._fapai_zhuang.length == 3){
                    this._fapai_selectIndex_arr[1] = 6 // 庄家博牌
                }
            }
            else{
                if (this._fapai_zhuang.length == 3){
                    this._fapai_selectIndex_arr[0] = 5 // 庄家博牌
                }
            }
        }
        
        // this.setControllerIndex(3, true)
        if(this._gameState == 0 || this._gameState == 1){ // 下注中
            for (let i = 1; i <= 8; ++i){
                this.Fanpai(this._fapai_cards[i], 'paidi', true)
                this._fapai_cards[i].getChild('n2').skewY = 0
            }
            this._isRecovering = false

        } else if (this._gameState == 2){ // 发牌中
            this.setControllerIndex(3, true)
            var timers = [5,0,0]
            //0: 前4张 1：第5张 2： 第6张
            if　(this._totalCardNums == 4){
               
            }else if　(this._totalCardNums == 5){
                 timers = [7,4,0]
            }else if　(this._totalCardNums == 6){
                timers = [9,5,0]
            }

            MyLogger("recover this.getStateCounter(): "+ this.getStateCounter())

            var flag = -1
            for (let i = 0; i < timers.length; ++i){
                if (this.getStateCounter() >= timers[i]){

                    flag = i
                    break
                }
            }
            MyLogger("recover: "+ flag)
            if (flag == 0){
                this.startFapai(this.backupZhuang, this.backupXian)
            } else if (flag == 1){
                //@ts-ignore
                fairygui.GearBase.disableAllTweenEffect = true
                // this.onPlayendXipai()
                //@ts-ignore
                fairygui.GearBase.disableAllTweenEffect = false
                this.Fanpai(this._fapai_cards[1], this._fapai_xian[0], true)
                this.Fanpai(this._fapai_cards[2], this._fapai_zhuang[0], true)
                this.Fanpai(this._fapai_cards[3], this._fapai_xian[1], true)
                this.Fanpai(this._fapai_cards[4], this._fapai_zhuang[1], true)
                this.onXianFanpaiEnd(true)
                if(this._fapai_laststate == 4){
                    this._xianbu.visible = true
                } else {
                    this._zhuangbu.visible = true
                }
                this.OnfapaiStep02()
            }
            else{
                this.toFinalFapai()
            }
        }
        else if(this._gameState == 3){// 开奖中       
        
            this.setControllerIndex(3, true)
            MyLogger("recover toFinalFapai")
            this.toFinalFapai()
        }
    }

    toFinalFapai = () => {
        //@ts-ignore
        fairygui.GearBase.disableAllTweenEffect = true
        this._fapai_ani.visible = false
        this._fapai_ani.playing = false

        const n = this._totalCardNums

        if(n <= 0)
            return

        this.Fanpai(this._fapai_cards[1], this._fapai_xian[0], true)
        this.Fanpai(this._fapai_cards[2], this._fapai_zhuang[0], true)
        this.Fanpai(this._fapai_cards[3], this._fapai_xian[1], true)
        this.Fanpai(this._fapai_cards[4], this._fapai_zhuang[1], true)

        if(n == 4){
            this.setControllerIndex(3)
        }
        else if(n == 5){
            if(this._fapai_xian.length == 3) {
                this.Fanpai(this._fapai_cards[5], this._fapai_xian[2], true)
                this.setControllerIndex(4)
                this._xianbu.visible = true
            }
            else {
                this.Fanpai(this._fapai_cards[5], this._fapai_zhuang[2], true)
                this.setControllerIndex(5)
                this._zhuangbu.visible = true
            }
        }
        else if(n == 6){
            this.Fanpai(this._fapai_cards[5], this._fapai_xian[2], true)
            this.Fanpai(this._fapai_cards[6], this._fapai_zhuang[2], true)
            this.setControllerIndex(6)
            this._xianbu.visible = true
            this._zhuangbu.visible = true
        }
        //@ts-ignore
        fairygui.GearBase.disableAllTweenEffect = false
    }

    setControllerIndex = (idx: number, force?: boolean) => {
        MyLogger("setControllerIndex:"+idx+ " force:"+force)
        if (!!force && force == true){
            //@ts-ignore
            fairygui.GearBase.disableAllTweenEffect = true
            this._fapai_controller.selectedIndex = idx
            //@ts-ignore
            fairygui.GearBase.disableAllTweenEffect = false

        }else{
            this._fapai_controller.selectedIndex = idx
        }
        
    }

    gotoNextGame = () => {
        this.clearAll()
        
        this._isRecovering = false

        this.backupZhuang = null
        this.backupXian = null
        this._hasXianpai = false
        this._step01done = false
        this._step02done = false
        this._step03done = false


        this.setControllerIndex(2)
        if (this._totalCardNums > 0){
            this._fapai_cards[this._totalCardNums].on(fairygui.Events.GEAR_STOP, this, this.onShouPaiEnd)
        } else{
            for (let i = 1; i <=8 ; ++i){
                //@ts-ignore
                this._fapai_cards[i].gearXY.delay = 0
            }

            this.setControllerIndex(1)
        }

        // this._fapai_laststate = 2
        Laya.timer.once(3000, this, this.startFapaiFirst4)

        this._zhuangdianshu.visible = false
        this._xiandianshu.visible = false
    }

}