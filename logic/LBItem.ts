export default class LBItem extends fairygui.GButton{
    _paimingText: fairygui.GTextField
    _nickNameText: fairygui.GTextField
    _bonusText: fairygui.GTextField
    _leaderController: fairygui.Controller
    _rateText: fairygui.GTextField

    constructor(){
        super()
    }

    constructFromXML(xml) {
        super.constructFromXML(xml)
        this._paimingText = this.getChild('paiming').asTextField
        this._nickNameText = this.getChild('nickname').asCom.getChild('nickname').asTextField
        this._bonusText = this.getChild('gain').asTextField
        this._leaderController = this.getController('leader')
        if(this.getChild('winrate')){
            this._rateText = this.getChild('winrate').asTextField
        }
    }

    setLead(value: number) {
        this._paimingText.text = value.toString()
        if (value >= 1 && value <= 3){
            this._leaderController.selectedIndex = value
        }
        else{
            this._leaderController.selectedIndex = 0
        }
    }

    setNickName = (value: string) => this._nickNameText.text = value

    setBonus(value: number) {
        var v = Math.round(value * 100) / 100 
        this._bonusText.text = v.toString()
    }

    setWinRate = (value: number) => this._rateText.text = value + '%'
}