import Game from './Game'

export type TextsType = {
    archoCom: fairygui.GComponent
    childrens:  fairygui.GObject[]
    tail: number
}

export default class Danmu{
    _game: Game
    _danmuRoot: fairygui.GComponent
    _tickTime: number
    _deltaTime: number
    _moveSpeed: number
    _moveStep: number
    _textCount: number
    _texts: TextsType[]
    leavePageTime: any

    constructor(game: Game, danmuCom: fairygui.GComponent) {
        this._game = game
        this._danmuRoot = danmuCom
        this._texts = []
        this._tickTime = Date.now()
        this._deltaTime = 10
        this._moveSpeed = 0.06
        this._moveStep = 3
        this._textCount = 0
        this.init()
    }

    init = () => {
        for (let i = 0; i < 3; ++i){
            this._texts[i] = {
                archoCom:  this._danmuRoot.getChild('text_0' + i).asCom,
                childrens:  [],
                tail:  0,
            }
        }

        this._danmuRoot.getChild('n0').visible = false
        window.setInterval(this.runTick, 20)
        //Laya.timer.frameLoop(3,this,this.runTick)

        this.leavePageTime = null
        Laya.stage.on('visibilitychange', this, this.onVisibilityChange)
    }

   onVisibilityChange = () => {
        if(!Laya.stage.isVisibility){
            this.leavePageTime = Date.now()
        } 
        else{
            if (this.leavePageTime){
                const diff = Date.now() - this.leavePageTime
                this._moveStep = (3 * diff) / 20 
                for (let i = 0; i < this._texts.length; ++i){
                    if (this._texts[i].childrens.length == 0)
                        continue
                    
                    this.updateTextItems(this._texts[i])
                }

                this._moveStep = 3
                this.leavePageTime = null
            }
        }
    }

   newTextItem = (txt,fontSize) => {
        const aRichTextField = new fairygui.GRichTextField()
        aRichTextField.setSize(2000, 25)
        aRichTextField.ubbEnabled = true
        aRichTextField.singleLine = true
        aRichTextField.bold = true
        aRichTextField.fontSize = 22
        aRichTextField.stroke = 3
        aRichTextField.strokeColor = '#000000'
        aRichTextField.text = txt
        aRichTextField.width = Math.round(aRichTextField.div.contextWidth + 10)
        return aRichTextField
    }

   getShortestRoot = () => {
        let shortest = this._texts[0].tail
        let shortestIdx = 0
        for (let i = 1; i < this._texts.length; ++i){
            if (this._texts[i].tail <shortest ){
                shortest = this._texts[i].tail
                shortestIdx = i
            }
             
        }

        return this._texts[shortestIdx]
    }

   addText = (txt, fontSize) => {
        if (!this._danmuRoot.visible )
            return

        if (this._textCount > 20)
            return

        const newItem = this.newTextItem(txt, fontSize)
        const croot = this.getShortestRoot()
        croot.archoCom.addChild(newItem)
        croot.childrens[croot.childrens.length] = newItem
        const randomOffset = Math.random()*100+ 20
        newItem.x = croot.tail+randomOffset
        croot.tail += (newItem.width+randomOffset)
        this._textCount ++
    }

    updateTextItems = (croot) => {
        croot.tail -= this._moveStep
        if (croot.tail < 0){
            croot.tail = 0
        }
        let delCount = 0
        for (let i = 0; i < croot.childrens.length; ++i){
            croot.childrens[i].x -= this._moveStep
            if (croot.childrens[i].x + croot.childrens[i].width + this._danmuRoot.width < 0){
                croot.archoCom.removeChild(croot.childrens[i])
                this._textCount--
                delCount++
            }
        }

        if (delCount > 0){
            croot.childrens.splice(0,delCount)
        }
    }

    runTick = () => {
        if (this._textCount == 0)
            return

        // var now = Date.now()
        // this._deltaTime = now - this._tickTime 
        // if (this._deltaTime>30)
            //    this._deltaTime = 30
        // this._tickTime = now
        // this._moveStep = this._deltaTime*this._moveSpeed

        for (let i = 0; i < this._texts.length; ++i){
            if (this._texts[i].childrens.length == 0){
                continue
            }
            
            this.updateTextItems(this._texts[i])
        }
    }
}