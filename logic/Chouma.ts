import Game from './Game'
import { BetTypesIndex, BetTypes } from './BetTypes'

export default class Chouma{
    _game: Game
    _choumaValues: number[]
    _betTypes: BetTypes[]
    _controllers: fairygui.Controller[]
    _betTypesIndex: BetTypesIndex
    _playerChoumas: any[]
    _tables: fairygui.GComponent[]
    playingMoveChoumaAniCounter: number
    waitPlayTableChoumaQue: any

    constructor(game: Game){
        this._game = game
        this._choumaValues = [1000, 500, 100, 50, 10, 5, 1]
        this._betTypes = game._betTypes
        this._betTypesIndex = game._betTypesIndex
        this.init()
        Laya.timer.frameLoop(30, this, this.checkLoop)
    }

    init = () => {
        this._controllers = []
        this._playerChoumas = []
        this._tables = []
        for (let m = 0; m < this._betTypes.length; ++m){
            const type = this._betTypes[m]

            //@ts-ignore
            this._tables[m] = this._game._view.getChild('table_'+type).asCom
            //@ts-ignore
            this._controllers[m] = this._game._view.getController('playercm_'+type)
            this._playerChoumas[m] = []
            for (let i = 1; i <= 6; ++i){
                //@ts-ignore
                this._playerChoumas[m][i] = this._game._view.getChild('playerchouma_'+type+'_'+i).asCom
                if (this._playerChoumas[m][i] )
                    this._playerChoumas[m][i].visible = false
            }
        }   

        this.playingMoveChoumaAniCounter = 0
        this.waitPlayTableChoumaQue = null

        this.gotoNextGame()
    }

    isTargetType = (type: string) => {
        for (let m = 0; m < this._betTypes.length; ++m){
            if (type == this._betTypes[m])
                return true
        }

        return false
    }

    calcChoumaNumber = (number: number) => {
        var value = number
        const result = []
        for (let i = 0; i < this._choumaValues.length; ++i){
            result[i] = Math.floor(value / this._choumaValues[i])
            if (result[i] > 0)
                value -= result[i] * this._choumaValues[i]
        }

        return result
    }

    gotoNextGame = () => {
        //@ts-ignore
        fairygui.GearBase.disableAllTweenEffect = true 
        for (let m = 0; m < this._betTypes.length; ++m){
            this._controllers[m].selectedIndex = 1
            this._tables[m].visible = false
        }
        //@ts-ignore
        fairygui.GearBase.disableAllTweenEffect = false

        this.playingMoveChoumaAniCounter = 0
        this.waitPlayTableChoumaQue = []
    }

    playBetChoumaAni = (type: string, number: number, total: number) => {
        this.playChoumaAni(type, number, false, total)
    }

    playRewardChoumaAni = (type: string, number: number) => {
        //SoundManager.playSound("res/sounds/chouma.mp3", 1)
        this._game.PlayGameSound('chouma.mp3', 1)
        this.playChoumaAni(type, number, true)
    }

    playChoumaAni = (type: string, number: number, isReward: boolean, total?: number) => {
        const choumas = this._playerChoumas[this._betTypesIndex[type]]
        if (choumas == undefined || choumas == null) return
        if (number <= 0) return

        const result = this.calcChoumaNumber(number)
        var k = 6
        for (let i = 0; i < result.length && k >= 1; ++i){
            if (result[i] <= 0)
                continue

            for (let j = 0; j < result[i] && k >= 1;++j){
                const target = choumas[k--]
                target.visible = true
                const controller = target.getController('c1')
                controller.selectedPage = this._choumaValues[i].toString()
            }
        }

        for (let i = 1; i <= k; ++i){
            choumas[i].visible = false
        }

        if (k < 6){
            for (let i = k+1; i <=6 ; ++i){
                choumas[i].gearXY.delay = (i-k-1)*0.1
            }

            const start = isReward ? 0 : 2
            const end = isReward ? 2 : 0
            
            const ctrl = this._controllers[this._betTypesIndex[type]]
            //@ts-ignore
            fairygui.GearBase.disableAllTweenEffect = true 
            ctrl.selectedIndex = start
            //@ts-ignore
            fairygui.GearBase.disableAllTweenEffect = false
            ctrl.selectedIndex = end

            this.playingMoveChoumaAniCounter++
            choumas[6].on(fairygui.Events.GEAR_STOP,this, this.onPlayChoumaAniEnd, [isReward, number, total,  ctrl, this._betTypesIndex[type]])
        }
    }

    onPlayChoumaAniEnd = (isReward: boolean, number: number, total: number, ctrl: fairygui.Controller, typeIndex: string) => {
        this.playingMoveChoumaAniCounter--
        //@ts-ignore
        fairygui.GearBase.disableAllTweenEffect = true 
        ctrl.selectedIndex = 1
        //@ts-ignore
        fairygui.GearBase.disableAllTweenEffect = false

        const choumas = this._playerChoumas[typeIndex]
        for (let i = 1; i <= 6; ++i){
            choumas[i].visible = false
        }

        if (isReward){
            this._game.emit('choumaAniPlayEnd')
        }
        else{
            this.setTableChouma(this._betTypes[typeIndex], total)
        }


        for (let i = 1; i <=6 ; ++i){
            choumas[i].gearXY.delay = (i-1)*0.1
        }
    }

    checkLoop = () => {
        //if ( this.playingMoveChoumaAniCounter > 0)
        //    return;

        while (this.waitPlayTableChoumaQue.length > 0){
            const type = this.waitPlayTableChoumaQue[0][0]
            const number = this.waitPlayTableChoumaQue[0][1]
            this.waitPlayTableChoumaQue.splice(0,1)

            this.func_setTableChouma(type, number)
        }
    }

    setTableChouma = (type: string, number: number) => {
        this.waitPlayTableChoumaQue[this.waitPlayTableChoumaQue.length] = [type, number]
    }

    func_setTableChouma = (type: string, number: number) => {    
        const typeIndex = this._betTypesIndex[type]
        const table = this._tables[typeIndex]
        if (table == undefined || table == null) return
        if (number <= 0){
            table.visible = false
            return
        }
        table.visible = true

        //SoundManager.playSound('res/sounds/chouma2.mp3', 1)
        this._game.PlayGameSound('chouma2.mp3', 1)
        var result = this.calcChoumaNumber(number)
        const tableChoumaArr = []

        for (var k = 1; k <= 6; ++k){
            const cm = table.getChild('chouma_'+k.toString())
            if (cm){
                tableChoumaArr[tableChoumaArr.length] = cm.asCom
            }

        }

        for (let i = 0 ; i < tableChoumaArr.length; ++i){
            tableChoumaArr[i].visible = false
        }
        
        var k = 0
        for (let i = 0; i < result.length; ++i){
            if (result[i] > 0){
                const target = tableChoumaArr[k++]
                target.visible = true
                const controller = target.getController('c1')
                controller.selectedPage = this._choumaValues[i].toString()
                
                if (typeIndex <=2 && result[i] > 1 && k <= 3){
                    const bottom = tableChoumaArr[k+2]
                    if (bottom){
                        bottom.visible = true
                        const bottomCtrl = bottom.getController('c1')
                        bottomCtrl.selectedPage = this._choumaValues[i].toString()
                    }
                } else if (typeIndex > 2 &&  result[i] > 1 ){
                    for (let mm = 0; mm < result[i]; ++mm){
                        const t2 = tableChoumaArr[k+mm]
                        if (t2){
                            t2.visible = true
                            const c2 = t2.getController('c1')
                            c2.selectedPage = this._choumaValues[i].toString()
                        }

                    }
                }

                if (k >= tableChoumaArr.length)
                    break
            }
        }

        
    }
}