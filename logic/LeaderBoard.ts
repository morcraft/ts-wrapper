import Game from './Game'
import LBItem from './LBItem'
import Browser = Laya.Browser
import Handler = Laya.Handler

export type Ratio = {
    nickName: string
    ratio: number
}

export default class Leaderboard{
    _game: Game
    _lb: fairygui.GComponent
    _list: fairygui.GList
    _list_rate: fairygui.GList
    _datas: any
    _zanwupaiming: fairygui.GTextField
    ratioData: Ratio[]

    constructor(game: Game, lbCompontent: fairygui.GComponent) {
        if (Browser.onMobile){
            fairygui.UIObjectFactory.setPackageItemExtension('ui://mobileMainUI/leaderboad_item', LBItem)
           // fairygui.UIObjectFactory.setPackageItemExtension('ui://mobileMainUI/leaderboad_item_rate', LBItem)
        } else{
            fairygui.UIObjectFactory.setPackageItemExtension('ui://MainUI/leaderboad_item', LBItem)
           // fairygui.UIObjectFactory.setPackageItemExtension('ui://MainUI/leaderboad_item_rate', LBItem)
        }
        
        this._game = game
        this._lb = lbCompontent
        this._list = lbCompontent.getChild('list').asList
        this._list.setVirtual()
        this._list.itemRenderer = Handler.create(this, this.renderListItem, null, false)
        this._list.on(fairygui.Events.SCROLL, this, this.onScroll)
        this._list_rate = lbCompontent.getChild('list_rate').asList
        this._list_rate.setVirtual()
        this._list_rate.itemRenderer = Handler.create(this, this.renderListItem_rate, null, false)
        this._list_rate.on(fairygui.Events.SCROLL, this, this.onScroll)
        this._datas = null
        this._zanwupaiming = lbCompontent.getChild('wupaiming').asTextField
        lbCompontent.getChild('button_down').visible = false
        lbCompontent.getChild('button_up').visible = false

        this.initButtons()
        
    }

    initButtons = () => {
         const lbCompontent= this._lb 
         let b1 = lbCompontent.getChild('button_down')
         this._game.initButtonMouseStyle(b1)
         b1 = lbCompontent.getChild('button_up')
         this._game.initButtonMouseStyle(b1)
         b1 = lbCompontent.getChild('close')
         this._game.initButtonMouseStyle(b1)

        lbCompontent.getChild('button_down').onClick(this, () => {
            var next = this._list.selectedIndex + 9
            if (next >  this._list.numItems -9){
                next = this._list.numItems -9
                if (next < 0){
                    next = 0
                }
            }
            if (next != this._list.selectedIndex){
                this._list.addSelection(next, true)
            }
        })

        lbCompontent.getChild('button_up').onClick(this, () => {
            let next = this._list.selectedIndex - 9
            if (next < 0){
                next = 0
            }
            if (next != this._list.selectedIndex){
                this._list.addSelection(next, true)
            }
        })

        lbCompontent.getChild('close').onClick(this, () => {
            //this._game.onClickLeaderboard()
            this._game._leaderboardController.selectedIndex = 0
        })
    }

    /*
    [AreaService] leaderboards:[{'account':'q1q1q1q1','nickName':'cccccccc','bonus':9250},
    {'account':'q1q1q1q3','nickName':'游客0003','bonus':8920},
    {'account':'q1q1q1q2','nickName':','bonus':2150}]
    */
    setLBDatas = (datas) => {
        this.ratioData = []
        if (datas.length > 0){
            this._zanwupaiming.visible = false 
            this._datas  = datas
            this._list.numItems = datas.length
            let ratio
            for (const i in datas) {
                if (datas[i].betcost > 0 && datas[i].bonus > 0) {
                    ratio = datas[i].bonus / datas[i].betcost
                } else {
                    ratio = 0
                }
                this.ratioData.push({
                    nickName: datas[i].nickName,
                    ratio: ratio
                })
            }

            this.ratioData.sort((a, b) => {
                return b.ratio - a.ratio
            })

            this._list_rate.numItems = datas.length
            /*if (this._list.numItems > 9){
                this._lb.getChild('button_down').visible = true
            }*/
        } else {
            this._list.numItems = 0
            this._list_rate.numItems = 0
            this._zanwupaiming.visible = true
        }
         
    }

    onScroll = () => {
        const f = this._list.getFirstChildInView()
        //console.log('getFirstChildInView:', f)
        this._lb.getChild('button_up').visible = f > 0
        this._lb.getChild('button_down').visible = f < (this._list.numItems - 10)
    }

    renderListItem = (index, obj) => {
        const item = obj
        item.setLead(index+1)
        let dId = index
        if (dId >= this._datas.length){
            dId = this._datas.length-1
        }

        item.setNickName(this._datas[dId].nickName)//this._game.formatName(
        item.setBonus(this._datas[dId].bonus)

        if (!item.isInitedButtonStyle){
            item.isInitedButtonStyle = true
            this._game.initButtonMouseStyle(item)
        }
    }

    renderListItem_rate = (index: number, obj) => {
        let item = obj
        item.setLead(index+1)
        let dId = index
        if (dId >= this.ratioData.length){
            dId = this.ratioData.length-1
        }

        item.setNickName(this.ratioData[dId].nickName)//this._game.formatName(
        // item.setBonus(this._datas[dId].bonus)
        const ratioText = this.ratioData[dId].ratio == 0 ? 0 : ((this.ratioData[dId].ratio * 100).toFixed(2) + '%')
        //var rate = (this._datas[dId].bonus/this._datas[dId].betcost)*100
        // item.setWinRate(rate.toFixed(2))
        item.getChild('gain').text = ratioText
        if (!item.isInitedButtonStyle){
            item.isInitedButtonStyle = true
            this._game.initButtonMouseStyle(item)
        }
    }
}