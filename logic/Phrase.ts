import Game from './Game'

export default class Phrase{
    _game: Game
    _view: mainUI.phrase_XMLǃphraseǃCOMPONENT | mobileMainUI.phrase_XMLǃphraseǃCOMPONENT
    _phrases = []
    _input: mainUI.liaotianshi_XMLǃinputǃTEXT | mobileMainUI.liaotianshi_XMLǃinputǃTEXT
    _list: mainUI.phrase_XMLǃlistǃLIST | mobileMainUI.phrase_XMLǃlistǃLIST
    _btn_hide: mainUI.buttonsǁbtn_hide_XMLǃbutton_hideǃCOMPONENT | mobileMainUI.buttonsǁbtn_hide_XMLǃbutton_hideǃCOMPONENT

    constructor(game: Game) {
        this._game = game
        this._view = game._view.getChild('liaotianshi').getChild('phrase')
        this.init()
    }

    init = () => {
        this._view.visible = false
        this._input = this._game._view.getChild('liaotianshi').getChild('input')
        this._input.on(laya.events.Event.FOCUS, this, this.onFocusIn)

        // this._input.on(laya.events.Event.BLUR, this, this.onFocusOut)

        this._list = this._view.getChild('list')

        this._btn_hide = this._game._view.getChild('liaotianshi').getChild('button_hide')
        this._btn_hide.onClick(this, this.onHide)

        this._list.itemRenderer = Laya.Handler.create(this, this.onRender, null, false)
        this._list.on(fairygui.Events.CLICK_ITEM, this, this.onClickItem)

        this._list.setVirtual()
        this._list.numItems = 0
    }

    onFocusIn = () => {
        if (this._phrases && this._phrases.length > 0) {
            this._view.visible = true
            this._btn_hide.visible = true
            this._list.scrollToView(0)
        }
    }

    onRender = (index: string, obj: fairygui.GComponent) => {
        obj.getChild('content').text = this._phrases[index]
    }

    onClickItem = (obj) => {
        var index = this._list.getChildIndex(obj)
        this._input.text = this._phrases[this._list.childIndexToItemIndex(index)]

        this._game.onFaSongLiaoTian() //!!WATCH OUT FOR REFERENCE
    }

    onHide = () => {
        this._view.visible = false
        this._btn_hide.visible = false
    }

    firstEnterScene = (data) => {
        if (data) {
            this._phrases = data
            this._list.numItems = this._phrases.length
        }
    }
}