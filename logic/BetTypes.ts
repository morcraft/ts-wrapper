const betTypes = {
    da: 5,
    he: 2,
    renyiduizi: 7,
    wanmeiduizi: 8,
    xian: 1,
    xiandui: 4,
    xiao: 6,
    zhuang: 0,
    zhuangdui: 3,
}

export default betTypes

export type BetTypesIndex = typeof betTypes
export type BetTypes = keyof BetTypesIndex