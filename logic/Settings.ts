import SoundManager = laya.media.SoundManager
import Game from './Game'

export type SettingsContent = {
    music: 'on' | 'off'
    musicVolume: number
    sound: 'on' | 'off'
    soundVolume: number
    chouma: number[]
    danmu: 'on' | 'off'
}

export default class Settings{
    _game: Game
    _view: mainUI.Main_XML | mobileMainUI.Main_XML
    SELECTED_CHOUMA_NEED: number
    _curSelectedChoumaCnt: number
    _itemKey: string
    _settings: mainUI.settings_XMLǃsettingsǃCOMPONENT | mobileMainUI.settings_XMLǃsettingsǃCOMPONENT
    _choumaBtns: any
    _choumaComBtns: fairygui.GButton[]
    _choumaPanelBtns: fairygui.GButton[]
    _choumaSettingCom: mainUI.settingsǁchoumaSettings_XMLǃchoumaǃCOMPONENT | mobileMainUI.settingsǁchoumaSettings_XMLǃchoumaǃCOMPONENT
    _danmuBtn: mainUI.settingsǁsetting_onoff_button_XMLǃbutton_danmuǃCOMPONENT | mobileMainUI.settingsǁsetting_onoff_button_XMLǃbutton_danmuǃCOMPONENT
    _musicSlider: mainUI.settingsǁsetting_slider_XMLǃmusic_sliderǃCOMPONENT | mobileMainUI.settingsǁsetting_slider_XMLǃmusic_sliderǃCOMPONENT
    _soundSlider: mainUI.settingsǁsetting_slider_XMLǃsound_sliderǃCOMPONENT | mobileMainUI.settingsǁsetting_slider_XMLǃsound_sliderǃCOMPONENT
    _musicBtn: mainUI.settingsǁsetting_onoff_button_XMLǃbutton_musicǃCOMPONENT | mobileMainUI.settingsǁsetting_onoff_button_XMLǃbutton_musicǃCOMPONENT
    _soundBtn: mainUI.settingsǁsetting_onoff_button_XMLǃbutton_soundǃCOMPONENT | mobileMainUI.settingsǁsetting_onoff_button_XMLǃbutton_soundǃCOMPONENT
    _isPanel: boolean
    _tmpSettingsContent: any
    _settingsContent: SettingsContent
    _defaultSettings: SettingsContent
    _tmpChouma: number[]

    constructor(game: Game) {
        this._game = game
        this._view = this._game._view
       // this._choumaSettingPanel = choumaPanel
        this.init()
    }

    init = () => {
        this.SELECTED_CHOUMA_NEED = 4
        this._curSelectedChoumaCnt = this.SELECTED_CHOUMA_NEED

        this._itemKey = ''

        this._settings = this._view.getChild('settings')
        this._settings.displayObject.cacheAsBitmap = true
        this._settings.visible = false

        this._choumaBtns = null
        this._choumaComBtns = []
        this._choumaPanelBtns = []
        this._choumaSettingCom = this._settings.getChild('chouma')
        this.initChoumaSetting(this._choumaSettingCom, false)
       // this.initChoumaSetting(this._choumaSettingPanel, true)
        let btn: fairygui.GButton = this._settings.getChild('btn_cancel')
        btn.onClick(this, this.onHide)
        this._game.initButtonMouseStyle(btn)

        btn = this._settings.getChild('btn_confirm')
        btn.onClick(this, this.onQueDing)
        this._game.initButtonMouseStyle(btn)

        btn = this._settings.getChild('button_close')
        btn.onClick(this, this.onHide)
        this._game.initButtonMouseStyle(btn)

       /* btn = this._choumaSettingPanel.getChild('btn_close').asButton
        btn.onClick(this, this.onHide)
        this._game.initButtonMouseStyle(btn)*/

        this._danmuBtn = this._settings.getChild('button_danmu')
        this._game.initButtonMouseStyle(this._danmuBtn)
        this._danmuBtn.on(fairygui.Events.STATE_CHANGED, this, function () {
            this._game._danmuCtrl.selectedIndex = this._danmuBtn.selected?1:0
            this._tmpSettingsContent.danmu = (this._danmuBtn.selected ? 'on' : 'off')
        })
        this._danmuBtn.selected = true
        this._game._danmuCtrl.selectedIndex = 1

       /* this._btnCheat = this._settings.getChild('button_cheat')
        this._game.initButtonMouseStyle(this._btnCheat)
        this._btnCheat.on(fairygui.Events.STATE_CHANGED, this, function () {
            pomelo.request('area.playerHandler.cheat', { result:this._btnCheat.selected}, function(responseData) {
                if (responseData.code == 200){
                    //self.showMessageBox('现在不允许发送聊天消息。')

                   // self._cheatPanel.getChild('content').asTextField.text = JSON.stringify(result)
                }
            })
        })*/

        this._musicSlider = this._settings.getChild('music_slider')
        this._musicSlider.on(fairygui.Events.STATE_CHANGED, this, function () {
            SoundManager.setMusicVolume(this._musicSlider.value / this._musicSlider.max)

            this._tmpSettingsContent.musicVolume = this._musicSlider.value
        })

        this._soundSlider = this._settings.getChild('sound_slider')
        this._soundSlider.on(fairygui.Events.STATE_CHANGED, this, function () {
            SoundManager.setSoundVolume(this._soundSlider.value / this._soundSlider.max)

            SoundManager.soundMuted = this._soundSlider.value === 0//兼容性问题

            this._tmpSettingsContent.soundVolume = this._soundSlider.value
        })

        this._musicBtn = this._settings.getChild('button_music')
        this._game.initButtonMouseStyle(this._musicBtn)
        this._musicBtn.on(fairygui.Events.STATE_CHANGED, this, function () {
            SoundManager.musicMuted = !this._musicBtn.selected
            this._musicSlider.enabled = this._musicBtn.selected
            if(!SoundManager.musicMuted)
            {
                SoundManager.stopMusic()
                SoundManager.playMusic(this._game.resourceSoundDir + 'bg.mp3', 0)
            }
            this._tmpSettingsContent.music = (this._musicBtn.selected ? 'on' : 'off')
        })

        this._soundBtn = this._settings.getChild('button_sound')
        this._game.initButtonMouseStyle(this._soundBtn)
        this._soundBtn.on(fairygui.Events.STATE_CHANGED, this, function () {
            SoundManager.soundMuted = !this._soundBtn.selected
            this._soundSlider.enabled = this._soundBtn.selected

            this._tmpSettingsContent.sound = (this._soundBtn.selected ? 'on' : 'off')
        })


        this._tmpSettingsContent = {}

        this._isPanel = false
    }

    initChoumaSetting = (choumaCom, isPanel) => {
        let btns
        if (isPanel){
            btns = this._choumaPanelBtns
        } 
        else{
            btns = this._choumaComBtns
        } 

        for (let i = 0; i < 12; i++) {
            const btn = choumaCom.getChild('cm_' + i).asButton
            this._game.initButtonMouseStyle(btn)
            btn.onClick(this, this.onClickChouMa)
            btn.enabled = false

            btns.push(btn)
        }
       /* if (isPanel){
            var btn = this._choumaSettingPanel.getChild('btn_quxiao').asButton
            btn.onClick(this, this.onHide)
            this._game.initButtonMouseStyle(btn)

            btn = this._choumaSettingPanel.getChild('btn_queding').asButton
            btn.onClick(this, this.onQueDing)
            this._game.initButtonMouseStyle(btn)
        }*/
    }

    firstEnterScene = (account) => {
        this._itemKey = account + 'baccarat_settings_20190304'
        this.loadSettings()
        this.initSettings()
        this._game.onSetChouMa(this._settingsContent['chouma'])
    }

    loadSettings = () => {
        this._defaultSettings = {
            music: 'on',
            musicVolume: 50,
            sound: 'on',
            soundVolume: 50,
            chouma: [ 1, 0, 0, 1, 0, 0, 1, 0,0,1,0,0],
            danmu: 'on',
        }

        if (window.localStorage.getItem(this._itemKey)) {
            this._settingsContent = JSON.parse(window.localStorage.getItem(this._itemKey))
            this.checkDefaultChoumaValid()
        }
        else {
            this._settingsContent = this._defaultSettings
            window.localStorage.setItem(this._itemKey, JSON.stringify(this._settingsContent))
        }
    }

    checkDefaultChoumaValid = () => {
        const chouma = this._settingsContent.chouma
        const selected = []
        const notSelected = []
        for (var i = 0; i < chouma.length; i++) {
            if (chouma[i] === 1) {
                selected.push(i)
            } else {
                notSelected.push(i)
            }
        }

        if (selected.length === this.SELECTED_CHOUMA_NEED) return

        if (selected.length > this.SELECTED_CHOUMA_NEED) {
            while (selected.length > this.SELECTED_CHOUMA_NEED) {
                const idx = selected.shift()
                chouma[idx] = 0
            }
        } else {
            while (selected.length < this.SELECTED_CHOUMA_NEED) {
                const idx = notSelected.shift()
                chouma[idx] = 1
            }
        }

        this._settingsContent.chouma = chouma

        window.localStorage.setItem(this._itemKey, JSON.stringify(this._settingsContent))
    }

    initSettings = () => {
        if (this._choumaBtns == null) this._choumaBtns = this._choumaComBtns

        this._musicBtn.selected = (this._settingsContent.music === 'on')
        this._musicSlider.enabled = this._musicBtn.selected
        this._musicSlider.value = this._settingsContent.musicVolume
        SoundManager.musicMuted = (this._settingsContent.music === 'off')
        SoundManager.setMusicVolume(this._musicSlider.value / this._musicSlider.max)

        this._soundBtn.selected = (this._settingsContent.sound === 'on')
        this._soundSlider.enabled = this._soundBtn.selected
        this._soundSlider.value = this._settingsContent.soundVolume
        SoundManager.soundMuted = (this._settingsContent.sound === 'off')
        SoundManager.setSoundVolume(this._soundSlider.value / this._soundSlider.max)
        this._danmuBtn.selected = (this._settingsContent.danmu === 'on')
        this._game._danmuCtrl.selectedIndex = (this._settingsContent.danmu === 'on')?1:0
        //兼容性问题
        if (this._settingsContent.sound === 'on') {
            SoundManager.soundMuted = SoundManager.soundVolume === 0
        }

        const choumaTypes = this._settingsContent.chouma.slice()
        for (let i = 0; i < choumaTypes.length; i++) {
            const selected = (choumaTypes[i] == 1)
            this._choumaBtns[i].enabled = selected
            this._choumaBtns[i].selected = selected
        }
    }

    onQueDing = () => {
        if (this._curSelectedChoumaCnt != this.SELECTED_CHOUMA_NEED) {
            this._game.showMessageBox('请选择4个筹码')
            return
        }
        this.saveSettings()
        this.onHide()
    }

    saveSettings = () => {
        for (const key in this._tmpSettingsContent) {
            this._settingsContent[key] = this._tmpSettingsContent[key]
        }

        this._settingsContent.chouma = this._tmpChouma.slice()

        window.localStorage.setItem(this._itemKey, JSON.stringify(this._settingsContent))

        this._game.onSetChouMa(this._tmpChouma)
    }

    onShow = (isPanel: boolean) => {
        if (isPanel) {
            this._choumaBtns = this._choumaPanelBtns
        } else {
            this._choumaBtns = this._choumaComBtns
        }

        this._isPanel = isPanel

        this.initSettings()
        this._curSelectedChoumaCnt = this.SELECTED_CHOUMA_NEED
        this._tmpSettingsContent = {}
        this._tmpChouma = this._settingsContent.chouma.slice()
    }

    onHide = () => {
        /*if (this._isPanel) {
            this._game.onClickChoumaSettings.call(this._game)
        } else {
            this._game.onClickSettings.call(this._game)
        }*/

        this.initSettings()
        this._settings.visible = false
    }

    onClickChouMa = (evt) => {
        const btn = fairygui.GObject.cast(evt.currentTarget) as fairygui.GButton
        const index = btn.name.substr(3)

        this._tmpChouma[index] = (btn.selected ? 1 : 0)

        if (btn.selected) {
            this._curSelectedChoumaCnt += 1

            if (this._curSelectedChoumaCnt >= this.SELECTED_CHOUMA_NEED) {
                this.setChouMaStatus(this._tmpChouma)
            }
        } else {
            this._curSelectedChoumaCnt -= 1
            this.enableAllChouma()
        }
    }

    enableAllChouma = () => {
        for (let i = 0; i < this._choumaBtns.length; i++) {
            this._choumaBtns[i].enabled = true
        }
    }

    setChouMaStatus = (arr) => {
        for (let i = 0; i < arr.length; i++) {
            this._choumaBtns[i].enabled = (arr[i] == 1)
        }
    }
}