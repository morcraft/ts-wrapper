import LanguageAssociator from 'language/associator'
import DictionaryCollection, { DictionaryName } from 'language/dictionary/collection'
import Definitions from 'language/dictionary/definitions'

export default class LanguagePanelHandler{
    currentLanguageButton: fairygui.GButton

    constructor(readonly view: mobileMainUI.Main_XML | mainUI.Main_XML, autoStart?: boolean){
        if(autoStart === true) this.autoStart()
    }

    private autoStart(): void{
        const languagePanel = this.view.getChild('settings').getChild('language_panel').getChild('language_list')

        this.setActiveButton(languagePanel._children[0])

        languagePanel._children.forEach(button => {
            const name = button.name
            const dictionary = DictionaryCollection[name]
            if(!dictionary){
                console.error(`Couldn't set up handlers for button with name ${name} in the language settings panel.`)
            }
            else{
                this.setUpLanguageButton(button, dictionary, name as DictionaryName)
            }
        })
    }

    setActiveButton = (button: fairygui.GButton) => {
        if(this.currentLanguageButton){
            this.currentLanguageButton.getChild('selected_background').alpha = 0
        }
        this.currentLanguageButton = button
        this.currentLanguageButton.getChild('selected_background').alpha = 0.5
    }

    onMouseOver = () => Laya.Mouse.cursor = 'pointer'
    onMouseOut = () => Laya.Mouse.cursor = 'default'

    onClick = (button: fairygui.GButton, dictionary: Definitions, language: DictionaryName) => {
        LanguageAssociator.associateView(this.view, dictionary, language)
        this.setActiveButton(button)
    }

    setUpLanguageButton(button: fairygui.GButton, dictionary: Definitions, language: DictionaryName): fairygui.GButton{
        button.on(Laya.Event.MOUSE_OVER, this, this.onMouseOver)
        button.on(Laya.Event.MOUSE_OUT, this, this.onMouseOut)
        button.on(Laya.Event.CLICK, this, () => this.onClick(button, dictionary, language))
        return button
    }
}