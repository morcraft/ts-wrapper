import LanguagePanelHandler from 'ui/languagePanel/handler'

export default class MainScreen {
    LanguagePanelHandler: LanguagePanelHandler

    constructor(readonly view: mainUI.Main_XML) {
        this.LanguagePanelHandler = new LanguagePanelHandler(this.view, true)
    }
}
