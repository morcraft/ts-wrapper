import MobileStageHandler from 'ui/stage/mobile/handler'
import LanguagePanelHandler from 'ui/languagePanel/handler'

export default class MainScreen {
    StageHandler: MobileStageHandler
    LanguagePanelHandler: LanguagePanelHandler

    constructor(readonly view: mobileMainUI.Main_XML) {
        this.LanguagePanelHandler = new LanguagePanelHandler(this.view, true)
        //TODO re-enable autostart once relations are set correctly
        this.StageHandler = new MobileStageHandler(this.view, false)
    }
}
