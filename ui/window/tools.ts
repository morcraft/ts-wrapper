export type WindowProperties = {
    width: number,
    height: number
}

export default class Tools{
    static getWindowWidth = () => window.outerWidth

    static getWindowHeight = () => window.outerHeight

    static getWindowProperties = (): WindowProperties => {
        return {
            width: Tools.getWindowWidth(),
            height: Tools.getWindowHeight(),
        }
    }
}