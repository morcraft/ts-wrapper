const properties = {
    scaleMode: laya.display.Stage.SCALE_SHOWALL,
    alignH: laya.display.Stage.ALIGN_CENTER,
    alignV: laya.display.Stage.ALIGN_TOP,
    screenAdaptationEnabled: !Laya.Browser.onMobile,
}

export default properties