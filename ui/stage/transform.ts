import defaultProperties from './defaultProperties'
import wTools from 'ui/window/tools'
import EventEmitter = require('event-emitter')
import eventNames from './eventNames'
import orientations from './orientations'

export interface Viewport{
    width: number
    height: number
    aspectRatio?: number
}

const originalProperties = [
    'height',
    'width',
    'designHeight',
    'designWidth',
]

export default class Transform{
    private currentViewport: Viewport
    private originalProperties: any = {}
    isMobile = Laya.Browser.onMobile
    EventEmitter = EventEmitter()
    
    constructor(private readonly stage: Laya.Stage){
        this.storeOriginalProperties()
        this.addListeners()
    }

    storeOriginalProperties(){
        originalProperties.forEach(e => {
            this.originalProperties[e] = this.stage[e]
        })

        this.originalProperties.aspectRatio = this.originalProperties.width / this.originalProperties.height
    }

    getOriginalProperties = () => originalProperties

    setStageProperties = () =>{
        for(const property in defaultProperties){
            this.stage[property] = defaultProperties[property]
        }
    }

    getOrientation(){
        return this.currentViewport.width / this.currentViewport.height < 1 ? orientations.PORTRAIT: orientations.LANDSCAPE
    }

    getAspectRatio = (width: number, height: number, orientation?: string) => {
        let _orientation
        if(typeof orientation != 'string'){
            _orientation = orientations.PORTRAIT
        }
        else{
            let found = false
            for(const prop in orientations){
                if(orientations[prop] === orientation){
                    found = true
                    break
                }
            }
            
            if(!found){
                console.error(`Invalid orientation: '${orientation}'. Allowed values are`, orientations)
                return
            }

            _orientation = orientation
        }

        return _orientation === orientations.PORTRAIT ? width / height : height / width
    }

    fitStageVertically = () => {
        const orientation = this.getOrientation()

        if(orientation === orientations.PORTRAIT){
            this.stage.height = this.currentViewport.height * this.stage.width / this.currentViewport.width
        }
        else{
            this.stage.height = this.currentViewport.width * this.stage.width / this.currentViewport.height
        }

        this.EventEmitter.emit(eventNames.MOBILE_VERTICAL_SCALE, {
            stage: this.stage,
            currentViewport: this.currentViewport,
            orientation: orientation,
            originalProperties: this.originalProperties,
        })
    }

    fitStageInArea = () =>{
        if(!this.isMobile) return

        //on-screen keyboard emits a resize event and changes the 
        //window height, causing an aspect ratio miscalculation
        if(Laya.stage.focus instanceof Laya.Input) return 

        this.currentViewport = {
            width: wTools.getWindowWidth(),
            height: wTools.getWindowHeight(),
        }

        const orientation = this.getOrientation()
        this.currentViewport.aspectRatio = this.getAspectRatio(this.currentViewport.width, this.currentViewport.height, orientation)

        if(this.currentViewport.aspectRatio < this.originalProperties.aspectRatio)
            this.fitStageVertically()
    }

    addListeners(){
        window.addEventListener(eventNames.RESIZE, this.setStageProperties)
        window.addEventListener(eventNames.RESIZE, this.fitStageInArea)
    }
}