import StageTransform from '../transform'
import eventNames from '../eventNames'
import screenfull = require('screenfull')

export default class StageHandler{
    private StageTransform: StageTransform
    private fullscreenButton: fairygui.GButton
    screenfull = screenfull

    constructor(readonly view: mobileMainUI.Main_XML, autoStart?: boolean){
        if(autoStart === true) this.autoStart()
    }

    private autoStart = () => {
        this.StageTransform = new StageTransform(Laya.stage)
        this.StageTransform.EventEmitter.on(eventNames.MOBILE_VERTICAL_SCALE, this.onMobileVerticalScale)
        this.StageTransform.fitStageInArea()
        
        if(Laya.Browser.onMobile)
            this.addFullscreenEvents()
    }

    private onMobileVerticalScale = (properties) => this.view.height = properties.stage.height

    private toggleFullscreen = () => {
        if (screenfull.enabled)
            screenfull.toggle()
    }

    private addFullscreenEvents = () => {
        /*const fullscreenButton = this.view.getChild('fullscreen_button')
        this.fullscreenButton = fullscreenButton.asButton
        if(this.isMobileSafari()){
            this.onMobileSafari()
        }
        else{
            this.fullscreenButton.onClick(this, this.toggleFullscreen)
        }*/
    }
}