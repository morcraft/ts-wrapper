const style = {
    'h1.scroll-down': {
        'color': 'rgba(255, 255, 255, 0.9)',
        'text-align': 'center',
        'font-family': 'Arial',
        'padding': '35px 0',
        'pointer-events': 'none'
    },
    '#layaContainer': {
        'top': '150px',
        'left': '0px',
        'width': '100%',
        'height': '100%',
        'position': 'absolute'
    }
}

export default style