const eventNames = {
    RESIZE: 'resize',
    MOBILE_VERTICAL_SCALE: 'mobileVerticalScale'
}

export default eventNames