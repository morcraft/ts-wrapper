export default class StyleManager{
    static toString = (style:any) => {
        let styleString = ''
        for(const selector in style){
            styleString += `${selector}{`
            for(const rule in style[selector]){
                styleString += `${rule}:${style[selector][rule]};` 
            }
            styleString += `}`
        }
        return styleString
    }

    static add = (style: any) => {
        const styleString = StyleManager.toString(style)
        const head = document.head || document.getElementsByTagName('head')[0]
        const styleNode = document.createElement('style')
        head.appendChild(styleNode)
        styleNode.type = 'text/css'

        //@ts-ignore
        if (styleNode.styleSheet){
            // This is required for IE8 and below.
            //@ts-ignore
            styleNode.styleSheet.cssText = styleString
        } else {
            styleNode.appendChild(document.createTextNode(styleString))
        }  
    }
}