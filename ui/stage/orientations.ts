const orientations = {
    PORTRAIT: 'portrait',
    LANDSCAPE: 'landscape'
}

export default orientations