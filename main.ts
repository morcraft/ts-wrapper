import MobileScreen from 'ui/screen/mobile/main'
import DesktopScreen from 'ui/screen/desktop/main'
import LanguageAssociator from 'language/associator'
import StageTransform from 'ui/stage/transform'
import { parseProperties } from 'debug/panel/childWindow'
import hotkeys from 'hotkeys-js'
import Chouma from 'logic/Chouma'
import Fapai from 'logic/Fapai'
import Danmu from 'logic/Danmu'
import Leaderboard from 'logic/LeaderBoard'
import Settings from 'logic/Settings'
import Phrase from 'logic/Phrase'
import LBItem from 'logic/LBItem'
import GameMain from 'logic/GameMain'
import EventEmitter = require('event-emitter')
//window['EventEmitter'] = EventEmitter
//window['__resources__'] = {}

// import PomeloProtocol = require('pomelo-protocol')
// import PomeloProtobuf = require('pomelo-protobuf')
// import PomeloWebsocket = require('pomelo-jsclient-websocket')


export default class TSWrapper {
    view:any
    languageAssociator = LanguageAssociator
    screenHandler: MobileScreen | DesktopScreen
    stageTransform = StageTransform
    
    constructor(){
        this.addDebuggerKeybindings()
    }
    
    load(view:any, mobile: boolean): void {//entry-point for non-TS environment
        this.view = view
        this.screenHandler = mobile ? new MobileScreen(this.view) : new DesktopScreen(this.view)
    }

    openDebugger(){
        window.open('./debug/debugger.html', 'Debugger', parseProperties())
    }

    addDebuggerKeybindings(){
        hotkeys('ctrl+shift+d, command+shift+d', (event, handler) => {
            this.openDebugger()
        })
    }
}


window['TSWrapper'] = new TSWrapper()//keep?
//The following are temporary global references to the classes themselves
// window['MyLogger'] = (...whatevs) => {}
// window['Chouma'] = Chouma
// window['Fapai'] = Fapai
// window['Danmu'] = Danmu
// window['Leaderboard'] = Leaderboard
// window['Settings'] = Settings
// window['Phrase'] = Phrase
// window['LBItem'] = LBItem

// window['Protocol'] = PomeloProtocol
// window['protobuf'] = PomeloProtobuf
// window['pomelo'] = PomeloWebsocket
//@ts-ignore
// console.log('pomelo is', pomelo)
// window['GameMain'] = GameMain

// window['pomelo'].game = new GameMain()