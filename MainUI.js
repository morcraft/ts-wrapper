var Handler = laya.utils.Handler;
var Loader = laya.net.Loader;
var EventEmitter = window.EventEmitter;
var pomelo = window.pomelo;
var Browser = laya.utils.Browser;
var Mouse = laya.utils.Mouse;
var SoundManager = laya.media.SoundManager;
var MAX_LOADING_SECOND = 70;

function MyLogger(v) {
    //console.log(Date.now()+"|" +v);
}

// 程序入口
var GameMain = (function () {
    //dictionary collection
    const __D = TSWrapper.languageAssociator.getTextFromKey

    function GameMain() {
        Browser.__init__();
        //Browser.onMobile = true;
        if (Browser.onMobile) {
            Laya.init(750, 1134, Laya.WebGL);
        } else {
            Laya.init(1391, 810, Laya.WebGL);
        }

        //laya.utils.Stat.show(0, 0);
        //Laya.DebugTool.init();
        Laya.WorkerLoader.workerPath = "libs/worker.js";
        Laya.WorkerLoader.enable = true;

        //设置适配模式
        if (Browser.onMobile) {
            Laya.stage.scaleMode = "showall";
            //设置横竖屏
            Laya.stage.screenMode = "vertical";

            this.resourceImageDir = "res/mobile/mobile";
            this.resourceSoundDir = "res/sounds/";
            this.resourceBufferDir = "res/mobile/mobile";
            this.filePreName = "mobile";
        } else {
            Laya.stage.screenMode = "horizontal";
            Laya.stage.scaleMode = "showall";

            this.resourceImageDir = "res/";
            this.resourceSoundDir = "res/sounds/";
            this.resourceBufferDir = "res/";
            this.filePreName = "";
        }

        Laya.stage.alignH = "middle";
        if (Browser.onMobile) {
            Laya.stage.alignV = "middle";
            Laya.stage.bgColor = "#000000";
        }
        else
        {
            Laya.stage.alignV = "top";
            Laya.stage.bgColor = "#ffffff";
        }
        document.body.style.backgroundColor = "black";


        this.eventEmitter = new EventEmitter();

        this.start_LoadingTime = 0;
        this.isTimeoutLoading = false;
        this.isTimeoutMain = false;
        this.errorResources = [];
        Laya.loader.on('error', this, function(param){
            this.errorResources.push(param);
        })
    }

    GameMain.prototype.isMobile = function() {
        return Browser.onMobile;
    }

    GameMain.prototype.onResize = function () {
        Laya.Utils.fitDOMElementInArea(this._helpdiv, this._helpBtn.displayObject, 0, 0, this._helpBtn.width, this._helpBtn.height);
    }

    GameMain.prototype.createHelpDiv = function () {
        this._helpdiv = Laya.Browser.document.createElement("div");
        this._helpdiv.setAttribute("id", "help");
        this._helpdiv.style.width = this._helpBtn.width;
        this._helpdiv.style.height = this._helpBtn.height;
        this._helpdiv.style.opacity = 0;
        Browser.document.body.appendChild(this._helpdiv);

        Laya.Utils.fitDOMElementInArea(this._helpdiv, this._helpBtn.displayObject, 0, 0, this._helpBtn.width, this._helpBtn.height);

        Browser.document.getElementById("help").onclick = function () {
            window.open("help/zh/b1.html");
        }
    }

    GameMain.prototype.emit = function (evt) {
        this.eventEmitter.emit(evt);
    }

    GameMain.prototype.returnHall = function () {
        if (Browser.onMobile) {
            window.location = this._domain + "/lotts/otherGames";
        } else {
            window.location = this._domain + "/qyy/bet/index";
        }
    }

    GameMain.prototype.startLoading = function () {
        if (Browser.onMobile) {
            if(pomelo.config.RESOURCE_ADDRESS_IMAGE_MOBILE != '')
                this.resourceImageDir = pomelo.config.RESOURCE_ADDRESS_IMAGE_MOBILE;
            if(pomelo.config.RESOURCE_ADDRESS_SOUND_MOBILE != '')
                this.resourceSoundDir = pomelo.config.RESOURCE_ADDRESS_SOUND_MOBILE;
            if(pomelo.config.RESOURCE_ADDRESS_BUFFER_MOBILE != '')
                this.resourceBufferDir = pomelo.config.RESOURCE_ADDRESS_BUFFER_MOBILE;            
        }else{
            if(pomelo.config.RESOURCE_ADDRESS_IMAGE != '')
                this.resourceImageDir = pomelo.config.RESOURCE_ADDRESS_IMAGE;
            if(pomelo.config.RESOURCE_ADDRESS_SOUND != '')
                this.resourceSoundDir = pomelo.config.RESOURCE_ADDRESS_SOUND;
            if(pomelo.config.RESOURCE_ADDRESS_BUFFER != '')
                this.resourceBufferDir = pomelo.config.RESOURCE_ADDRESS_BUFFER;
        }
        this.firstViewAssets = [
            { url: this.resourceImageDir + "Loading0401@atlas0.png", type: Loader.IMAGE },
            { url: this.resourceBufferDir + "Loading0401.fui", type: Loader.BUFFER }
        ];

        this.errorResources = [];
        this.loadTimerID = window.setInterval(function(){
            if(this.isTimeoutLoading){
                var resString = '';
                for(var i = 0; i < this.errorResources.length; i++)
                    resString += this.errorResources[i] + ' ';
                alert(__D('RESOURCE_MISSING_MESSAGE0') + resString + __D('RESOURCE_MISSING_MESSAGE1'));
                this.loadReqLog(this.errorResources,false,Date.now() - this.start_LoadingTime);
                window.clearInterval(this.loadTimerID);
                this.returnHall();    
            }else{
                this.isTimeoutLoading = true;
                this.loadReqLog(this.errorResources,true,Date.now() - this.start_LoadingTime);
                alert(__D('REQUIRED_GAME_ASSETS_CANNOT_BE_DOWNLOADED_PRESS_TRY_AGAIN'));
            }
        }.bind(this), MAX_LOADING_SECOND * 1000);
        Laya.loader.load(this.firstViewAssets, Handler.create(this, this.onFirstViewLoaded));
    }

    GameMain.prototype.onFirstViewLoaded = function () {
        Laya.timer.once(200, this, function(){
            try{
                if(this.errorResources.length > 0){
                    var resources = this.errorResources;
                    this.errorResources = [];
                    this.loadReqLog(resources,false,Date.now() - this.start_LoadingTime);
                    Laya.loader.load(resources, Handler.create(this, this.onFirstViewLoaded));
                    return
                }
                window.clearInterval(this.loadTimerID);
    
                Laya.stage.addChild(fairygui.GRoot.inst.displayObject);
                var fui = Laya.loader.getRes(this.resourceBufferDir + "Loading0401.fui");
                fairygui.UIPackage.addPackage(this.resourceImageDir + "Loading0401", fui);
                this._loadingView = fairygui.UIPackage.createObject(this.filePreName + "Loading", "Main").asCom;
                this._loadingView.setSize(fairygui.GRoot.inst.width, fairygui.GRoot.inst.height);
                this._loadingViewProgress = this._loadingView.getChild('progress').asProgress;
                this._loadingViewProgress.value = 0;
                //this._loadingView.getChild('n4').asTextField.text = '加载中...';
                fairygui.GRoot.inst.addChild(this._loadingView);
        
                this.errorResources = [];
                this.loadTimerID = window.setInterval(function(){
                    if(this.isTimeoutMain){
                        var resString = '';
                        for(var i = 0; i < this.errorResources.length; i++)
                            resString += this.errorResources[i] + ' ';
                        alert(__D('RESOURCE_MISSING_MESSAGE0') + resString + __D('RESOURCE_MISSING_MESSAGE1'));
                        this.loadReqLog(this.errorResources,false,Date.now() - this.start_LoadingTime);
                        window.clearInterval(this.loadTimerID);
                        this.returnHall();    
                    }else{
                        this.isTimeoutMain = true;
                        this.loadReqLog(this.errorResources,true,Date.now() - this.start_LoadingTime);
                        alert(__D('REQUIRED_GAME_ASSETS_CANNOT_BE_DOWNLOADED_PRESS_TRY_AGAIN'));
                    }
                }.bind(this), MAX_LOADING_SECOND * 1000);
                
                var allAssets = [
                    { url: this.resourceImageDir + "MainUI0401@atlas0.png", type: Loader.IMAGE },
                    { url: this.resourceImageDir + "MainUI0401@atlas0_1.png", type: Loader.IMAGE },
                    { url: this.resourceImageDir + "MainUI0401@atlas0_2.png", type: Loader.IMAGE },
                    { url: this.resourceBufferDir + "MainUI0401.fui", type: Loader.BUFFER },
                    { url: this.resourceSoundDir + "bg.mp3", type: Loader.SOUND },
                    { url: this.resourceSoundDir + "chouma.mp3", type: Loader.SOUND },
                    { url: this.resourceSoundDir + "chouma2.mp3", type: Loader.SOUND },
                    { url: this.resourceSoundDir + "click.mp3", type: Loader.SOUND },
                    { url: this.resourceSoundDir + "fapai.mp3", type: Loader.SOUND },
                    { url: this.resourceSoundDir + "xipai.mp3", type: Loader.SOUND },
                    { url: this.resourceSoundDir + "fanpai.mp3", type: Loader.SOUND },
                    { url: this.resourceSoundDir + "yingqian.mp3", type: Loader.SOUND },
                    { url: this.resourceSoundDir + "start.mp3", type: Loader.SOUND },
                    { url: this.resourceSoundDir + "countdown.mp3", type: Loader.SOUND },
                    { url: this.resourceSoundDir + "warning.mp3", type: Loader.SOUND }
                ]
                if(this.isMobile()){
                   // allAssets.push({ url: this.resourceImageDir + "MainUI0228@atlas0_1.png", type: Loader.IMAGE });
                   // allAssets.push({ url: this.resourceImageDir + "MainUI0228@atlas0_2.png", type: Loader.IMAGE });
                   // allAssets.push({ url: this.resourceImageDir + "MainUI0228@atlas0_3.png", type: Loader.IMAGE });
                }
                
                Laya.loader.load(allAssets, Handler.create(this, this.onLoaded), Laya.Handler.create(this, this.onLoading));
        
                Laya.timer.frameLoop(12, this, this.fakeLoading);
            }
            catch(error)
            {
                document.write(error)
            }
                
        }.bind(this));
    }

    GameMain.prototype.unloadFirstView = function () {

        fairygui.GRoot.inst.removeChild(this._loadingView);
        fairygui.UIPackage.removePackage(this.resourceImageDir + "Loading0401");

        Laya.loader.clearRes(this.resourceImageDir + "Loading0401@atlas0.png");
        Laya.loader.clearRes(this.resourceBufferDir + "Loading0401.fui");

        this._loadingView = null;
        this._loadingViewProgress = null;
    }

    GameMain.prototype.fakeLoading = function () {
        if (!!this._loadingViewProgress) {
            var val = this._loadingViewProgress.value;
            val++;
            if (val > 75) {
                Laya.timer.clear(this, this.fakeLoading);
                return;
            }
            MyLogger("loading  ", val, "%");
            this._loadingViewProgress.value = val;
        }

    }

    GameMain.prototype.onLoading = function (t) {
        var text = __D('INTERFACE_RESOURCES') + Math.floor(100 * t) + "%";
        if (t * 100 < this._loadingViewProgress.value)
            return;
        this._loadingViewProgress.value = t * 100;
        MyLogger(text);
    }

    GameMain.prototype.lastLoading = function () {
        var val = this._loadingViewProgress.value;
        if (val >= 99) {
            Laya.timer.clear(this, this.lastLoading);
            var fui = Laya.loader.getRes(this.resourceBufferDir + "MainUI0401.fui");
            fairygui.UIPackage.addPackage(this.resourceImageDir + "MainUI0401", fui);
            //fairygui.UIObjectFactory.setPackageItemExtension("ui://MainUI/mailItem", MailItem);
            // new MainPanel();
            this._view = fairygui.UIPackage.createObject(this.filePreName + "MainUI", "Main").asCom;
            this._view.setSize(fairygui.GRoot.inst.width, fairygui.GRoot.inst.height);
            fairygui.GRoot.inst.addChild(this._view);

            this._loadingView.visible = true;
            this._view.visible = false;
            var jiazaiAni = this._loadingView.getChild('ani_loading').asCom;
            jiazaiAni.getChild('n6').asTextField.text = '登';
            jiazaiAni.getChild('n7').asTextField.text = '录';

            this.init();
            this._choumaMgr = new Chouma(this);
            this._setting = new Settings(this);
            this._fapaiMgr = new Fapai(this);
            this._leaderboard = new Leaderboard(this, this._view.getChild('leaderboard').asCom);
            this._Danmu = new Danmu(this, this._view.getChild('danmu').asCom);
            this._phrase = new Phrase(this);
            //this._gain = new Gainboard(this,"BJL");
            var self = this;
            this.eventEmitter.on('logingsok', function () {
              //  self.initSound();

                self.unloadFirstView();
                //self._loadingView.visible = false;
                if (!Browser.onMobile) {
                    Laya.stage.alignH = "middle";
                    document.body.style.backgroundColor = "#202A33";
                }
                self.postClientLogs(pomelo.account,"loading",200,0,Date.now() - self.start_LoadingTime);
                self._view.visible = true;
                SoundManager.playMusic(self.resourceSoundDir + "bg.mp3", 0, new Handler(self, self.onPlayMusicComplete));
                MyLogger('logingsok');
                
            });

            this.emit('startlogings');
        } else {
            val += 5;
            if (val > 100)
                val = 100;
            this.onLoading(val / 100);
        }
    }

    GameMain.prototype.onLoaded = function () {
        Laya.timer.once(200, this, function(){
            if(this.errorResources.length > 0){
                var resources = this.errorResources;
                this.errorResources = [];
                this.loadReqLog(resources,false,Date.now() - this.start_LoadingTime);
                Laya.loader.load(resources, Handler.create(this, this.onFirstViewLoaded));
                return
            }
            window.clearInterval(this.loadTimerID);
            
            MyLogger("onLoaded ");
            Laya.timer.clear(this, this.fakeLoading);
            Laya.timer.frameLoop(3, this, this.lastLoading);
        }.bind(this));
    };

    GameMain.prototype.onPlayMusicComplete = function () {
    }

    GameMain.prototype.init = function () {
        this._currency = 0;
		this.ping = 0;
        this._playerFenXiangInfo = [];

        //TODO update during runtime
        this._luzi = ["庄", "闲", "和"];
        this._betList = [];
        this._previousBet = [0, 0, 0, 0, 0, 0, 0, 0, 0];
        this._currentBet = [0, 0, 0, 0, 0, 0, 0, 0, 0];
        this._currentGameTotalBet = [0, 0, 0, 0, 0, 0, 0, 0, 0];
        this._cardValues = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"];
        this._betTypes = ["zhuang", "xian", "he", "zhuangdui", "xiandui", "da", "xiao", "renyiduizi", "wanmeiduizi"];
        this._betTypesIndex = { "zhuang": 0, "xian": 1, "he": 2, "zhuangdui": 3, "xiandui": 4, "da": 5, "xiao": 6, "renyiduizi": 7, "wanmeiduizi": 8 };
        //TODO update during runtime
        this._betTypesChinese = ["庄", "闲", "和", "庄对", "闲对", "大", "小", "任意对子", "完美对子"];
        this._betChips = [1, 10, 100, 1000];

        this.myPlayerTotalBet = [0, 0, 0, 0, 0, 0, 0, 0, 0];
        this.allPlayerTotalBet = [0, 0, 0, 0, 0, 0, 0, 0, 0];

        this.timeleft = 0;

        // 点击记录
        this.resetTouZhuClickInfo();

        // 本局下注总金额
        this._benjuxiazhuAmount = 0;
        this._previousTotalBetAmount = 0;
        // 本次下注金额，一局可多次下注
        this._totalBetAmount = 0;
        this._reward = 0;
        this._totalBet = 0;
        this._otherResults = []; // 除去 庄，闲，和之外开出的其他奖项的索引

        // 下注上下限
        // this._lowerLimit = 2;
        // this._upperLimit = 20000;
        this._menu = this._view.getChild("menu").asCom;
        this._menu_ctrl = this._view.getController("menu_ctrl");
        this._menu_ctrl.selectedIndex = 0;

        this._btn_menu = this._view.getChild('btn_menu').asButton;
        this._btn_menu.onClick(this,function(){
            this._menu_ctrl.selectedIndex ^= 1;
        });
        this.initButtonMouseStyle(this._btn_menu);
        // 筹码
       /* this._chouma = this._view.getChild("chouma").asCom;
        for (var i = 0; i < this._betChips.length; i++) {
            var betValue = this._betChips[i];
            var button = this._chouma.getChild("button_" + betValue + "kuai").asButton;
            this.initButtonMouseStyle(button);
        }
        

        this._choumaController = this._chouma.getController("selectedChoumaCtrl");
        this._choumaController.on(fairygui.Events.STATE_CHANGED, this, this.onChoumaSelectChanged);*/
        this._chouma = this._view.getChild("chouma").asCom;
        this._chouma.displayObject.cacheAsBitmap = true;
        this._choumaController = this._chouma.getController("c1");
        this._choumaValues = [ 1,2, 5, 10, 20, 50, 100, 200,500,1000,2000,10000];
        this._choumaOnTable = [];
        for (var i = 0; i < 4; i++) {
            this.initButtonMouseStyle(this._chouma.getChild("chouma_" + i));
        }
        /*
        this._xiazhu = this._view.getChild("xiazhu").asCom;
        for(var i = 0; i < this._betTypes.length; i++)
        {
            var button = this._xiazhu.getChild("button_" + this._betTypes[i]).asButton;
            button.on(Laya.Event.ROLL_OVER, this, this.onXiaZhuMouseOver);
            this.initButtonMouseStyle(button);
        }
        
        this._xiazhuController = this._xiazhu.getController("controller");
        this._xiazhuController.on(fairygui.Events.STATE_CHANGED, this, this.onXiaZhuChanged);
        this._xiazhuController.selectedIndex = 0;
        */
        this._xinxiMousedownIndex = -1;
        this._xinxiMousedownTimestamp = 0;
        for (var i = 0; i < this._betTypes.length; i++) {
            var type = this._betTypes[i];
            var btn = this._view.getChild("xinxi_" + type);
            var myPreBetText = btn.getChild('myPreBet').asTextField;
            myPreBetText.text = '';

            btn.onClick(this, this.onXinXiClicked);

            this.initButtonMouseStyle(btn);
            btn.on(Laya.Event.MOUSE_DOWN, this, this.onXinxiMouseDown);
            btn.on(Laya.Event.ROLL_OVER, this, this.onXinxiMouseOver);
            btn.on(Laya.Event.ROLL_OUT, this, this.onXinxiMouseOut);

        }
        this._stateCounter = 0;
        this._receivedCounterTime = 0;
        this._betAddValueTimeStamp = [0, 0, 0, 0, 0, 0, 0, 0, 0];
        this._betAddValueQuickClick = [0, 0, 0, 0, 0, 0, 0, 0, 0];

        this._btnQueRenTouZhu = this._view.getChild("button_querentouzhu").asButton;
        this._btnQueRenTouZhu.onClick(this, this.onQueRenTouZhu);
        this.initButtonMouseStyle(this._btnQueRenTouZhu);
        this._autobetCountdownText = this._btnQueRenTouZhu.getChild("countdown").asTextField;
        this._autobetCountdownText.visible = false;

        
        this._btnQuanQing = this._view.getChild("button_quanqing").asButton;
        this._btnQuanQing.onClick(this, this.onQuanQing, [true]);
        this.initButtonMouseStyle(this._btnQuanQing);

        this._btnCheXiao = this._view.getChild("button_chexiao").asButton;
        this._btnCheXiao.onClick(this, this.onCheXiao);
        this.initButtonMouseStyle(this._btnCheXiao);

        this._kaishixiazhu = this._view.getChild("kaishixiazhu").asCom;
        this._kaishixiazhu.visible = false;

        this._zuihoutouzhu = this._view.getChild("zuihoutouzhu_confirm").asCom;
        this._zuihoutouzhu.visible = false;
        this._zuihoutouzhuController = this._view.getController("zuihoutouzhu");
        var btnZuihoutouzhuQueren = this._zuihoutouzhu.getChild("button_queren");
        var btnZuihoutouzhuQuxiao = this._zuihoutouzhu.getChild("button_quxiao");
        btnZuihoutouzhuQueren.onClick(this, this.onQuerenZuihoutouzhu);
        btnZuihoutouzhuQuxiao.onClick(this, this.onQuxiaoZuihoutouzhu);
        this.initButtonMouseStyle(btnZuihoutouzhuQueren);
        this.initButtonMouseStyle(btnZuihoutouzhuQuxiao);

        //help
        for (var i = 0; i < this._betTypes.length; i++) {
            var type = this._betTypes[i];
            var btn = this._view.getChild("btn_help_" + type);
            if (!!btn) {
                this.initButtonMouseStyle(btn);
            }
        }

        for (var i = 1; i <= 6; ++i) {
            var help_info = this._view.getChild("help_" + i);

            help_info.on(Laya.Event.ROLL_OVER, this, this.onHelpMouseOver);
            help_info.on(Laya.Event.ROLL_OUT, this, this.onHelpMouseOut);
        }

        this._rewardTextAni = this._view.getChild('reward_text_ani').asCom;
        this._rewardTextAni.visible = false;

        this._btnGain = this._view.getChild("btn_gain").asButton;
        this._btnGain.onClick(this, this.onClickGain);
        this.initButtonMouseStyle(this._btnGain);
        //mask
        this._maskZhongjiang = new Array();
        for (var i = 0; i < this._betTypes.length; i++) {
            var type = this._betTypes[i];
            this._maskZhongjiang[i] = this._view.getChild("mask_" + type).asCom;
            this._maskZhongjiang[i].visible = false;
        }

        this._buttonLeaderboard = this._menu.getChild('button_paihangbang').asButton;
        this._leaderboardController = this._view.getController('lb_ctrl');
        this.initButtonMouseStyle(this._buttonLeaderboard);
        this._buttonLeaderboard.onClick(this, this.onClickLeaderboard);

        this._luzhitu = this._view.getChild("luzhitu").asCom;
        this._luzhitu.displayObject.cacheAsBitmap = true;
        this._tfStatZhuang = this._luzhitu.getChild("zhuang").asTextField;
        this._tfStatXian = this._luzhitu.getChild("xian").asTextField;
        this._tfStatHe = this._luzhitu.getChild("he").asTextField;
        this._tfStatZhuangDui = this._luzhitu.getChild("zhuangdui").asTextField;
        this._tfStatXianDui = this._luzhitu.getChild("xiandui").asTextField;
        this._tfStatTotal = this._luzhitu.getChild("total").asTextField;

        this._raw_zhuang_values = [];
        this._raw_xian_values = [];
        this._result_zhuang = [];
        this._result_xian = [];
        this._card_zhuang = [];
        this._card_xian = [];
        this._gameState = 0;
        this._lastChatTime = 0;
        /*for(var i=0;i<3;i++)
        {
            this._view.getChild('yingqian_tips').getChild("zhuang_" + i).url = "";
            this._view.getChild('yingqian_tips').getChild("xian_" + i).url = "";
        }
        this._view.getChild('yingqian_tips').getChild("result_other").asRichTextField.text = "";*/
        this.gameNumberText = this._view.getChild("text_qishu").asRichTextField;
        this.gamePlayerNumberText = this._view.getChild("text_renshu").asTextField;

        this._btnTipclose = this._view.getChild('yingqian_tips').getChild('close').asButton;
            this.initButtonMouseStyle(this._btnTipclose);
            this._btnTipclose.onClick(this, function () {
                this._view.getChild('yingqian_tips').visible = false;
            });

        //this.firstEnterView();

        this.resetBet();
        this.resetMyBet();

        this._gameNumber = 0;

        //聊天室 全部，我的投注，中奖记录
        if (Browser.onMobile) {
            this._chat_ctrl = this._view.getController("chat_ctrl");
            this._chat_ctrl.selectedIndex = 0;

            this._btn_chat = this._view.getChild("btn_chat").asButton;
            var self = this;
            this._btn_chat.onClick(this, function(){
                self._chat_ctrl.selectedIndex = 1;
            });
            this.initButtonMouseStyle(this._btn_chat);

        }
        this._contents = ["", "", ""];
        this._liaotianshi = this._view.getChild("liaotianshi").asCom;
        this._liaotianneirong = this._liaotianshi.getChild("content").asCom;
        this._contentController = this._liaotianshi.getController("controller_content");
        this._contentController.on(fairygui.Events.STATE_CHANGED, this, this.onSelectContent);
        this._contentController.selectedIndex = 0;
        this._liaotianshi.opaque = true;
        if (Browser.onMobile) {
            this._liaotianshi.getController("controller_showhide").selectedIndex = 0;
            this._liaotianshi.opaque = false;
        }
        else {
            var ltsCtrl = this._liaotianshi.getController("controller_showhide");
            ltsCtrl.selectedIndex = 0;
            ltsCtrl.on(fairygui.Events.STATE_CHANGED, this, this.onLiaotianshiShoworHide);
        }


        var c = this._liaotianshi.getChild("content").asCom;
        c.on(laya.events.Event.LINK, this, this.onGenDan);
        this._content = c.getChild("text").asRichTextField;
        c.displayObject.cacheAsBitmap = true;

        


        if (Browser.onMobile) {
            /*var btnHide = this._liaotianshi.getChild("button_hide").asButton;
            btnHide.onClick(this, this.onShowHideLiaoTianShi);
            var btnShow = this._liaotianshi.getChild("button_show").asButton;
            btnShow.onClick(this, this.onShowHideLiaoTianShi);

            this.initButtonMouseStyle(btnHide);
            this.initButtonMouseStyle(btnShow);*/
            var btnClose = this._liaotianshi.getChild("close").asButton;
            var self = this;
            btnClose.onClick(this, function(){
                self._chat_ctrl.selectedIndex = 0;
            });
        }

        var tButton = this._liaotianshi.getChild("button_quanbu").asButton;
        this.initButtonMouseStyle(tButton);
        tButton = this._liaotianshi.getChild("button_wodetouzhu").asButton;
        this.initButtonMouseStyle(tButton);
        tButton = this._liaotianshi.getChild("button_zhongjiangjilu").asButton;
        this.initButtonMouseStyle(tButton);
        /*tButton = this._liaotianshi.getChild("btn_hide").asButton;
        this.initButtonMouseStyle(tButton);
        tButton = this._liaotianshi.getChild("button_show").asButton;
        this.initButtonMouseStyle(tButton);*/


        this._textInput = this._liaotianshi.getChild("input").asTextInput;
        this._textInput.input.on("enter", this, this.onFaSongLiaoTian);
        this._textInput.input.on("input", this, this.updateBtnFaSongStatus);
        if (Browser.onMobile) {
            this._zhuanghuyue = this._view.getChild("zhanghuyue").asTextField;
            this._benjuxiazhu = this._view.getChild("benjuxiazhu").asTextField;
            this._meiriyingkui = this._view.getChild("zongyingkui").asTextField;
        }
        else {
            this._zhuanghuyue = this._liaotianshi.getChild("zhanghuyue").asTextField;
            this._benjuxiazhu = this._liaotianshi.getChild("benjuxiazhu").asTextField;
            this._meiriyingkui = this._liaotianshi.getChild("zongyingkui").asTextField;
        }
        this._zhuanghuyue.text = "0";
        this._benjuxiazhu.text = "0";
        this._meiriyingkui.text = "";



        this._btnFaSong = this._liaotianshi.getChild("button_fasong").asButton;
        this._btnFaSong.onClick(this, this.onFaSongLiaoTian);
        this.initButtonMouseStyle(this._btnFaSong);

        this.setCanChat(false);

        this._totalCardNums = 0;

        this._canTouZhu = true; // 下注之后置为false, 下注返回结果后设置true


        var mbox = this._view.getChild('message_box').asCom;
        mbox._closeCbs = [];
        var msgOKBtn = mbox.getChild('btn_queren').asButton;
        msgOKBtn.onClick(this, this.hideMessageBox, [mbox]);
        this.initButtonMouseStyle(msgOKBtn);

        mbox = this._view.getChild("reconnect_box").asCom;
        mbox.getChild("countdown").visible = false;
        msgOKBtn = mbox.getChild("btn_queren").asButton;
        msgOKBtn.onClick(this, this.hideReconnectBox, [mbox]);
        this.initButtonMouseStyle(msgOKBtn);

        //var str = "pix:"+Browser.clientWidth+"-"+ Browser.clientHeight;
        //var str = "Browser.onMobile:"+Browser.onMobile;
        //this.showMessageBox(str);

        

        this._help = this._view.getChild("help").asCom;
        this._help.cacheAsBitmap = true;
        this._help.visible = false;

        this._help_ctrl = this._help.getController("c1");
        this._help_ctrl.selectedIndex = 0;
       // this._helpScrollPane = this._help.getChild("content").scrollPane;
        
        this._help.getChild("scroll_down").onClick(this, function () {
            //this._helpScrollPane.scrollDown(5, true);
            this._help_ctrl.selectedIndex = (this._help_ctrl.selectedIndex+1)%3;
        });
        this._help.getChild("scroll_up").onClick(this, function () {
            //this._helpScrollPane.scrollUp(5, true);
            this._help_ctrl.selectedIndex = Math.abs(this._help_ctrl.selectedIndex-1)%3;
        });

        this._help.getChild("close").onClick(this, function () {
            this._help.visible = false;
        });


        this._helpBtn = this._menu.getChild("button_help");
        this.initButtonMouseStyle(this._helpBtn);
        this._helpBtn.onClick(this, function () {
            this._help.visible = !this._help.visible;
            if (this._help.visible)
                this._help_ctrl.selectedIndex = 0;

            this._settings.visible = false;
            this._leaderboardController.selectedIndex = 0;
            this._menu_ctrl.selectedIndex = 0;
            return;
        });


        //this.createHelpDiv();
        //Laya.stage.on("resize", this, this.onResize);

        var chongfutouzhuBtn = this._view.getChild("button_chongfutouzhu");
        this.initButtonMouseStyle(chongfutouzhuBtn);
        chongfutouzhuBtn.onClick(this, this.onChongFuTouZhu);

       /* this._defaultSettings = {};
        this._defaultSettings["music"] = "on";
        this._defaultSettings["musicVolume"] = 50;
        this._defaultSettings["sound"] = "on";
        this._defaultSettings["soundVolume"] = 50;
        this._defaultSettings["chouma"] = [ 1, 0, 0, 0, 1, 1, 1, 0,0,0,0,0];
        this._defaultSettings["danmu"] = "on";

        if (!!window.localStorage.getItem("baccarat_settings")) {
            this._settingsContent = JSON.parse(window.localStorage.getItem("baccarat_settings"));
        }
        else {
            this._settingsContent = this._defaultSettings;
            window.localStorage.setItem("baccarat_settings", JSON.stringify(this._settingsContent));
        }
        this._choumaBtns = null;
        this._choumaComBtns = [];
        this._choumaPanelBtns = [];
        this._choumaSettingCom = this._settings.getChild("chouma").asCom;
        this.initChoumaSetting(this._choumaSettingCom, false);
        this._settings = this._view.getChild("settings").asCom;
        this._settings.visible = false;

        var settingsCloseBtn = this._settings.getChild("button_close").asButton;
        this.initButtonMouseStyle(settingsCloseBtn);
        settingsCloseBtn.onClick(this, function () {
            this._settings.visible = false;
            this.saveSettings();
        });

        this._musicSlider = this._settings.getChild("music_slider").asSlider;
        this._musicSlider.on(fairygui.Events.STATE_CHANGED, this, function () {
            var value = this._musicSlider.value / this._musicSlider.max;
            SoundManager.setMusicVolume(this._musicSlider.value / this._musicSlider.max);
            this._tmpSettingsContent["musicVolume"] = this._musicSlider.value;
        });
        this._soundSlider = this._settings.getChild("sound_slider").asSlider;
        this._soundSlider.on(fairygui.Events.STATE_CHANGED, this, function () {
            var value = this._soundSlider.value / this._soundSlider.max;
            SoundManager.setSoundVolume(this._soundSlider.value / this._soundSlider.max);
            this._tmpSettingsContent["soundVolume"] = this._soundSlider.value;
        });

        this._musicBtn = this._settings.getChild("button_music").asButton;
        this.initButtonMouseStyle(this._musicBtn);
        this._musicBtn.on(fairygui.Events.STATE_CHANGED, this, function () {
            SoundManager.musicMuted = !this._musicBtn.selected;
            this._musicSlider.enabled = this._musicBtn.selected;
            this._tmpSettingsContent["music"] = (this._musicBtn.selected ? "on" : "off");
        });
        this._soundBtn = this._settings.getChild("button_sound").asButton;
        this.initButtonMouseStyle(this._soundBtn);
        this._soundBtn.on(fairygui.Events.STATE_CHANGED, this, function () {
            SoundManager.soundMuted = !this._soundBtn.selected;
            this._soundSlider.enabled = this._soundBtn.selected;
            this._tmpSettingsContent["sound"] = (this._soundBtn.selected ? "on" : "off");
        });*/
        this._settings = this._view.getChild("settings").asCom;
        this._settings.visible = false;
        var settingsBtn = this._menu.getChild("button_settings");
        this.initButtonMouseStyle(settingsBtn);
        settingsBtn.onClick(this, function () {
            this._settings.visible = !this._settings.visible;

            /*if (!this._settings.visible)
                this.saveSettings();*/
                if(this._settings.visible)
                this._setting.onShow(false);
            this._help.visible = false;
            this._leaderboardController.selectedIndex = 0;
            this._menu_ctrl.selectedIndex = 0;
        });
        //弹幕
        this._danmuCtrl = this._view.getController('danmu_ctrl');
        //if(!Browser.onMobile)
        {
           /* this._btnDanmu = this._liaotianshi.getChild('button_danmu');
            this.initButtonMouseStyle(this._btnDanmu);
            this._btnDanmu.onClick(this, function () {
                this._danmuCtrl.selectedIndex = (this._danmuCtrl.selectedIndex + 1) % 2;
            });*/
            /*this._danmuBtn = this._settings.getChild("button_danmu").asButton;
            this.initButtonMouseStyle(this._danmuBtn);
            this._danmuBtn.on(fairygui.Events.STATE_CHANGED, this, function () {
                this._danmuCtrl.selectedIndex = this._danmuBtn.selected?1:0;
                this._tmpSettingsContent["danmu"] = (this._danmuBtn.selected ? "on" : "off");
            });
            this._danmuBtn.selected = true;
            this._danmuCtrl.selectedIndex = 1;*/
        }

        
        
        this._helpController = this._view.getController('help_ctrl');
        this._helpController.on(fairygui.Events.STATE_CHANGED, this, this.onHelpCtrlChanged);


        this._returnBtn = this._view.getChild('button_return').asButton;
        this.initButtonMouseStyle(this._returnBtn);
        this._returnBtn.onClick(this, this.onReturn);

        this._leave_confirm = this._view.getChild('leave_confirm').asCom;
        this._leave_confirm_ok = this._leave_confirm.getChild('btn_queren').asButton;
        this._leave_confirm_cancel = this._leave_confirm.getChild('btn_quxiao').asButton;

        this._infoZhupan = this._view.getChild("info_zhupan").asCom;
        this._infoZhupan.visible = false;

        if (Browser.onMobile) {
            this._daluTotalCols = 20;
            this._dayanluTotalCols = 34;
            this._xiaoluTotalCols = 17;
            this._xiaoqiangluTotalCols = 17;
            this._zhupanluTotalNum = 60;
            this._daxiaoluTotalCols = 10;
        }
        else {
            this._daluTotalCols = 19;
            this._dayanluTotalCols = 33;
            this._xiaoluTotalCols = 16;
            this._xiaoqiangluTotalCols = 17;
            this._zhupanluTotalNum = 60;
            this._daxiaoluTotalCols = 10;
        }
        
        this._resultInfo = this._view.getChild('yingqian_tips').asCom;
        this.result_cl = this._resultInfo.getController("c1");
        this.result_cl.selectedIndex = 0;
        this._list_result = this._resultInfo.getChild("list").asList;
        this._list_result.setVirtual();
        this._list_result.itemRenderer = Handler.create(this, this.renderListItem_result, null, false);
        this._list_result.numItems = 0;
        this._result_data = [];

        var msgOKBtn = this._resultInfo.getChild("btn_con").asButton;
        msgOKBtn.onClick(this, this.hideResultBox, [this._resultInfo]);
        this.initButtonMouseStyle(msgOKBtn);
        var closeBtn = this._resultInfo.getChild("close").asButton;
        closeBtn.onClick(this, this.hideResultBox, [this._resultInfo]);
        this.initButtonMouseStyle(closeBtn);

        this._redirectGame = this._view.getChild("redirectGame").asComboBox;
        this._redirectGame.on(fairygui.Events.STATE_CHANGED, this, this.onRedirectGame);

        this._pingInfo = this._view.getChild("ping").asCom;
        this._pingInfo.visible = false;
        this._ping_cl = this._pingInfo.getChild("green").getController("c1");
        this._ping_cl.selectedIndex = 0;
        
        this._autoBet_ctrl = this._view.getController("autobet_ctrl");
        this._autoBet_ctrl.selectedIndex = 0;
        
       

        Laya.stage.on(Laya.Event.CLICK,this,this.onMouseClick);

       // this.listenToBackKey();

        this._btnZhuGuLu = this._view.getChild("button_zhugulu").asButton;
        this._btnDaXiaoLu = this._view.getChild("button_daxiaolu").asButton;
        this._luzhiController = this._view.getController('luzhi_ctrl');
        this._luzhiController.selectedIndex = 0;
        this._luzhiController.on(fairygui.Events.STATE_CHANGED, this, this.onLuZhiCtrlChanged);

        /*var counterDownTxt = this._btnQueRenTouZhu.getChild('counterdown').asTextField;
        counterDownTxt.visible = false;*/
        //Entry point for TS environment
        TSWrapper.load(this._view, Laya.Browser.onMobile);
    }
    
    GameMain.prototype.onMouseClick = function () {
        this._autoBet_ctrl.selectedIndex = 1;
        if(this._gameState == 0)
            this.setPreTouzhuEnabled(true);
    }

    GameMain.prototype.onLuZhiCtrlChanged = function () {
        var index = this._luzhiController.selectedIndex;

        if (index == 0)
            this.showZhuPanLu();
        else
            this.showDaXiaoLu();
    }

    GameMain.prototype.listenToBackKey = function () {
        this._historyState = 'BACCARAT';
        var self = this;

        window.addEventListener('popstate', function (event) {
            if (event.state == self._historyState) {
                self.onReturn();
            }
        });

        //e.state refers to the second last state that was pushed. 
        //You need to have pushed state at least twice for e.state to not be null. 
        //This is because you should save the state when your site is loaded the first time and thereafter every time it changes state.
        window.history.pushState(this._historyState, null, window.location.href);
        window.history.pushState(this._historyState, null, window.location.href);
    }

    GameMain.prototype.onHelpCtrlChanged = function () {
        if (this._helpController.selectedIndex != 0) {
            Laya.stage.on("mousedown", this, this.OnClickStage);
        } else {
            Laya.stage.off("mousedown", this, this.OnClickStage);
        }
    }

    GameMain.prototype.setPreTouzhuEnabled = function (enabled) {
        for (var i = 0; i < this._betTypes.length; i++) {
            var type = this._betTypes[i];
            var btn = this._view.getChild("xinxi_" + type);
            btn.enabled = enabled;
        }
    }

    GameMain.prototype.onHelpMouseOver = function () {
        //this._helpController.selectedIndex = 0;
    }
    GameMain.prototype.onHelpMouseOut = function () {
        this._helpController.selectedIndex = 0;
    }

    GameMain.prototype.OnClickStage = function () {
        this._helpController.selectedIndex = 0;
    }

    GameMain.prototype.onSocketClose = function () {
        this.showReconnectBox(__D('DISCONNECTED_PLEASE_TRY_CONNECTING_AGAIN!'));
    }

    /*GameMain.prototype.saveSettings = function () {
        this._settingsContent["music"] = (this._musicBtn.selected) ? "on" : "off";
        this._settingsContent["sound"] = (this._soundBtn.selected) ? "on" : "off";
        this._settingsContent["musicVolume"] = this._musicSlider.value;
        this._settingsContent["soundVolume"] = this._soundSlider.value;

        window.localStorage.setItem("baccarat_settings", JSON.stringify(this._settingsContent));
    }*/

   /* GameMain.prototype.initSound = function () {
        this._musicBtn.selected = (this._settingsContent["music"] === "on");
        this._musicSlider.enabled = this._musicBtn.selected;
        this._musicSlider.value = this._settingsContent["musicVolume"];
        SoundManager.musicMuted = (this._settingsContent["music"] === "off");
        SoundManager.setMusicVolume(this._musicSlider.value / this._musicSlider.max);

        this._soundBtn.selected = (this._settingsContent["sound"] === "on");
        this._soundSlider.enabled = this._soundBtn.selected;
        this._soundSlider.value = this._settingsContent["soundVolume"];
        SoundManager.soundMuted = (this._settingsContent["sound"] === "off");
        SoundManager.setSoundVolume(this._soundSlider.value / this._soundSlider.max);
    }*/
    GameMain.prototype.onRedirectGame= function (){
        var self = this; 
        if(self._redirectGame.selectedIndex == 9)
        {
            self.onReturn();
        }
        else {
            self.enterOtherGame(self._redirectGame.selectedIndex,pomelo.account,self.getCurrency(),pomelo.playerNickName)
        }
    }

    GameMain.prototype.onReturn = function () {

        this._leave_confirm.getChild('content').asTextField.text = __D('LEAVE_GAME');

        if (this._leave_confirm.visible == false) {
            SoundManager.playSound(this.resourceSoundDir + "warning.mp3", 1);
        }

        this._leave_confirm.visible = true;

        var self = this;
        this._leave_confirm_ok.onClick(this, function () {
            self._leave_confirm.visible = false;
            if (Browser.onMobile) {
                window.location = self._domain + "/lotts/otherGames";
            }
            else {
                window.location = self._domain + "/qyy/bet/index";
            }
        });
        this._leave_confirm_cancel.onClick(this, function () {
            self._leave_confirm.visible = false;
            window.history.pushState(self._historyState, null, window.location.href);
        });
    }

    GameMain.prototype.onBtnMouseOverStyle = function (evt) {
        Mouse.cursor = 'pointer';
    }
    GameMain.prototype.onBtnMouseOutStyle = function (evt) {
        Mouse.cursor = 'default';
    }

    GameMain.prototype.onButtonClicked = function () {
        SoundManager.playSound(this.resourceSoundDir + "click.mp3", 1);
    }

    GameMain.prototype.initButtonMouseStyle = function (button) {

        button.onClick(this, this.onButtonClicked);

        if (Browser.onMobile) {
            return;
        }
        button.on(Laya.Event.MOUSE_OVER, this, this.onBtnMouseOverStyle);
        button.on(Laya.Event.MOUSE_OUT, this, this.onBtnMouseOutStyle);
    }

    GameMain.prototype.onShowHideLiaoTianShi = function (evt) {
        var name = fairygui.GObject.cast(evt.currentTarget).name;
        if (name === "button_show") {
            this._liaotianshi.opaque = true;
            this._btnZhuGuLu.visible = false;
            this._btnDaXiaoLu.visible = false;
        }
        else if (name === "button_hide") {
            this._liaotianshi.opaque = false;
            this._btnZhuGuLu.visible = true;
            this._btnDaXiaoLu.visible = true;
        }
    }


    GameMain.prototype.onXinxiMouseOut = function (evt) {
        var type = fairygui.GObject.cast(evt.currentTarget).name.substr(6);
        var index = this._betTypesIndex[type];

        this._maskZhongjiang[index].visible = evt.currentTarget._maskZhongjiangVisible;

        this._xinxiMousedownIndex = -1;
    }

    GameMain.prototype.setMaskShow = function (index) {
        var mask = this._maskZhongjiang[index];
        mask.visible = true;
        mask.getChild('tf_amount').visible = false;
        mask.getChild('n0').alpha = 0.3;
        var t = mask.getTransition('t0');
        t.stop();
    }

    GameMain.prototype.onXinxiMouseOver = function (evt) {
        var type = fairygui.GObject.cast(evt.currentTarget).name.substr(6);
        var index = this._betTypesIndex[type];

        evt.currentTarget._maskZhongjiangVisible = this._maskZhongjiang[index].visible;

        this.setMaskShow(index);
    }

    GameMain.prototype.onXinxiMouseDown = function (evt) {
        if (!this._canTouZhu) {
            return;
        }
        var type = fairygui.GObject.cast(evt.currentTarget).name.substr(6);
        var index = this._betTypesIndex[type];

        this._xinxiMousedownIndex = index;
        this._xinxiMousedownTimestamp = Date.now();

        var self = this;
        if (!!self.onXinxiMouseDownTimerId)
            window.clearInterval(self.onXinxiMouseDownTimerId);

        self.onXinxiMouseDownTimerId = window.setInterval(function () {
            if (Date.now() - self._xinxiMousedownTimestamp < 1000)
                return;
            if (self._xinxiMousedownIndex == -1) {
                window.clearInterval(self.onXinxiMouseDownTimerId);
                return;
            }
            var value = self.getSelectChoumaValue();
            (self.addBet(self._xinxiMousedownIndex, value))();
        }, 50);

    }

    GameMain.prototype.onXinXiClicked = function (evt) {
        if (!this._canTouZhu) {
            return;
        }

        var type = fairygui.GObject.cast(evt.currentTarget).name.substr(6);
        var index = this._betTypesIndex[type];
        var value = this.getSelectChoumaValue();
        (this.addBet(index, value))();

        this._touZhuQuClicked = true;
        this._touzhuQuClick[index]++;

        this._xinxiMousedownIndex = -1;
    }

    GameMain.prototype.onSetChouMa = function (arr) {

        this._choumaOnTable = [];
        var curIndex = 0;
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] == 0) continue;
            var value = this._choumaValues[i];
            this._choumaOnTable[curIndex] = value;

            var strValue = value.toString();
            /*if (value == "0.1") strValue = "01";
            else if (value == "0.5") strValue = "05";
            else if (value == "10000") strValue = "10k";*/

            if (!!this._chouma.getChild("chouma_" + curIndex)) {
                var chouma = this._chouma.getChild("chouma_" + curIndex).asButton;
                chouma.getChild("n1").url = "ui://" + this.filePreName + "MainUI/" + "cm_" + strValue + "g";
                chouma.getChild("n2").url = "ui://" + this.filePreName + "MainUI/" + "cm_" + strValue + "y";
            }

            curIndex += 1;
        }
    }

    GameMain.prototype.getSelectedChoumaValue = function () {
        return this._choumaOnTable[this._choumaController.selectedIndex];
    }

    GameMain.prototype.updateBtnFaSongStatus = function () {
        if (!this._canChat || this._textInput.text.trim().length == 0) {
            this._btnFaSong.enabled = false;
            return;
        }

        this._btnFaSong.enabled = true;
    }

    GameMain.prototype.onFaSongLiaoTian = function () {
        //if (this._textInput.text.length > 8 && this._textInput.text.substr(0,8) == 'setcards'){
        //    this._textInput.input.maxChars = 32;
        //    pomelo.request("area.playerHandler.chat", {text : this._textInput.text});
        //    this._textInput.text = "";
        //    return;
        // }

        if (!this._canChat || this._textInput.text === "" || this._textInput.text.trim().length == 0) {
            if(!this._canChat)
                this.showMessageBox(__D('DISABLED_CHAT_MESSAGES'));
            this._phrase.onHide();
            return;
        }

        if(this._lastChatTime > 0 && Date.now()- this._lastChatTime< 10*1000 )
        {
            var num = Math.floor((Date.now()- this._lastChatTime) / 1000);
            //TODO find definition for this
            this.showMessageBox("聊天间隔时间太短，请稍后发送。");
            this._phrase.onHide();
            return;
        }

        var text = this._textInput.text + "<br/>";

        var self = this;
        var start_time = Date.now();
        pomelo.request("area.playerHandler.chat", { text: this._textInput.text }, function (responseData) {
            self.addRecievedMsgCount();
            if (responseData.code != 200) {
                self.showMessageBox(__D('DISABLED_CHAT_MESSAGES'));
            }
            self.postClientLogs(pomelo.account,"requestChat",responseData.code,responseData.usedTime,Date.now() - start_time);
            self._lastChatTime = Date.now();
        });
        this._textInput.text = "";

        this._btnFaSong.enabled = false;
        this._phrase.onHide();
    }

    GameMain.prototype.onLiaotianshiShoworHide = function () {
        var ltsCtrl = this._liaotianshi.getController("controller_showhide");
        if (ltsCtrl.selectedIndex == 0) {
            // xianshi
            document.body.style.backgroundColor = "#202A33";
        } else {
            document.body.style.backgroundColor = "#130E22";
        }
    }

    GameMain.prototype.onSelectContent = function () {
        var index = this._contentController.selectedIndex;
        this._content.text = this._contents[index];
        this._content.height = this._content.div.contextHeight;
        this._liaotianneirong.scrollPane.scrollBottom();
    }

    GameMain.prototype.showReconnectBox = function (t) {
        var mbox = this._view.getChild('reconnect_box').asCom;
        mbox.getChild('content').asTextField.text = t;

        if (mbox.visible == false) {
            SoundManager.playSound(this.resourceSoundDir + "warning.mp3", 1);
        }

        mbox.visible = true;
    }

    GameMain.prototype.showMessageBox = function (t, seconds, closeCb) {
        var mbox = this._view.getChild('message_box').asCom;
        mbox.getChild('content').asTextField.text = t;

        if (mbox.visible == false) {
            SoundManager.playSound(this.resourceSoundDir + "warning.mp3", 1);
        }

        mbox.visible = true;
        if (!!closeCb)
            mbox._closeCbs[mbox._closeCbs.length] = closeCb;

        var counter = seconds || 3;
        var self = this;
        if (!!self.showMessageBoxTimerId)
            window.clearInterval(self.showMessageBoxTimerId);
        mbox.getChild('countdown').asTextField.text = '(3s)';
        self.showMessageBoxTimerId = window.setInterval(function () {
            counter--;
            mbox.getChild('countdown').asTextField.text = '(' + counter + 's)';
            if (counter <= 0) {
                window.clearInterval(self.showMessageBoxTimerId);
                self.showMessageBoxTimerId = null;
                self.hideMessageBox(mbox);
            }
        }, 1000);
    }
    GameMain.prototype.hideMessageBox = function (mbox) {
        for (var i = 0; i < mbox._closeCbs.length; ++i) {
            if (mbox._closeCbs[i]) {
                (mbox._closeCbs[i])();
            }
        }

        mbox.visible = false;
        mbox._closeCbs = [];
    }

    GameMain.prototype.hideReconnectBox = function (mbox) {
        mbox.visible = false;
        pomelo.emit("player-reconnect");
        //if (Browser.onMobile) {
        //    window.location = this._domain + "/lotts/otherGames";
        //}
        //else {
        //    window.location = this._domain + "/qyy/bet/index";
        //}
    }


    GameMain.prototype.setGamePlayerNumber = function (number) {
        this.gamePlayerNumberText.text = number.toString();
    }

    GameMain.prototype.setGameNumber = function (number) {
        this._gameNumber = number;
        this.gameNumberText.text = __D('STATUS_MESSAGE', {
            number: number
        })
    }



    GameMain.prototype.updateState = function (state, counter) {

        this._gameState = state;
        this._stateCounter = counter;
        this._fapaiMgr.updateState(state, counter);
        this._receivedCounterTime = Date.now();
        if (state == 0 && this._totalBetAmount > 0) {
            this.checkZiDongQueren();
        }
    }
    GameMain.prototype.onTextAniPlayend = function (text, col, txtstr) {

        text.text = null;
        text.text = txtstr;
        text.color = "#FFFF99";
    }

    GameMain.prototype.rawCards = function (arr) {
        var result = [];
        for (var i = 0; i < arr.length; i++) {
            result[i] = Math.floor(arr[i] % 13) + 1;
        }
        return result;
    }

    GameMain.prototype.gotoNextGame = function () {
        var self = this;
        var reqTime = Date.now();
        pomelo.request("area.playerHandler.evaluatePing", { reqTime: reqTime }, function (resData) {
            self.addRecievedMsgCount();
            self.ping = Date.now() - resData.reqTime ;
            self.updatePingInfo(self.ping);
           // self.postClientLogs(pomelo.account,"requestPing",resData.code,Date.now() - reqTime);
        });
        this.allPlayerTotalBet = [0, 0, 0, 0, 0, 0, 0, 0, 0];
        this.myPlayerTotalBet = [0, 0, 0, 0, 0, 0, 0, 0, 0];
        this._currentBet = [0, 0, 0, 0, 0, 0, 0, 0, 0];
        //this._view.getChild('yingqian_tips').visible = false;
        this.enableBetFunction(false);
        Laya.timer.once(10, this, this.gotoNextGame_func);
    }

    GameMain.prototype.enableBetFunction = function (enabled) {
        this._view.getChild('button_chexiao').enabled = enabled;
        this._view.getChild('button_quanqing').enabled = enabled;
        this._view.getChild('button_chongfutouzhu').enabled = enabled;
        this._view.getChild('button_querentouzhu').enabled = enabled;
        if(!enabled)
            this._autobetCountdownText.visible = false;
    }
    
    GameMain.prototype.updatePingInfo = function(value) {
        var COLORS = ["3CF33C", "F3940C", "ED300B"];//绿黄红
        var index = 0;
        if(value >1000)
            this._ping_cl.selectedIndex = 4;
        else if(value >800 && value<=1000)
            this._ping_cl.selectedIndex = 3;
        else if(value >400 && value<=800)
            this._ping_cl.selectedIndex = 2;
        else if(value >200 && value<=4000)
            this._ping_cl.selectedIndex = 1;
        else
            this._ping_cl.selectedIndex = 0;

        this._pingText = this._pingInfo.getChild("ping").asRichTextField;
        this._pingText.text =  "[color=#" + COLORS[index] + "]" +value+"ms[/color]<br/>";
        this._pingInfo.visible = true;
    }

    GameMain.prototype.gotoNextGame_func = function () {
        this._raw_zhuang_values = [];
        this._raw_xian_values = [];
        this._result_zhuang = [];
        this._result_xian = [];
        this._card_zhuang = [];
        this._card_xian = [];
        this._gameState = 0;
        /*for(var i=0;i<3;i++)
        {
            this._view.getChild('yingqian_tips').getChild("zhuang_" + i).url = "";
            this._view.getChild('yingqian_tips').getChild("xian_" + i).url = "";
        }
        this._view.getChild('yingqian_tips').getChild("result_other").asRichTextField.text = "";*/
        this.resetMyBet();

        if (this._fapaiMgr)
            this._fapaiMgr.gotoNextGame();

        if (this._choumaMgr)
            this._choumaMgr.gotoNextGame();
    }


    GameMain.prototype.startKaiJiang = function (data) {

        //this.startFapai(data.zhuang, data.xian);
        this._btnQueRenTouZhu.enabled = false;
        this._view.getChild('button_chongfutouzhu').enabled = false;
        this._view.getChild('button_chexiao').enabled = false;
        this._view.getChild('button_quanqing').enabled = false;
        this.setPreTouzhuEnabled(false);

        if (!this.autoBet.autoBet) {
            this.onQuanQing(false);
        }

        this._xinxiMousedownIndex = -1;
        this._fapaiMgr.startFapai(data.zhuang, data.xian);
        this._raw_zhuang_values = this.rawCards(data.zhuang);
        this._raw_xian_values = this.rawCards(data.xian);
        this._result_zhuang = data.zhuang;
        this._result_xian = data.xian;
        this._yingjia = data.yingjia;
        this._yingjiadianshu = data.dianshu;
        
        this._card_zhuang = this._fapaiMgr.cards(data.zhuang);
        this._card_xian = this._fapaiMgr.cards(data.xian);
        this._otherResults = [];
        if (!!data.leixing) {
            for (var i = 0; i < data.leixing.length; i++) {
                var value = data.leixing[i];
                if (value > 2) {
                    this._otherResults.push(value);
                }
            }
        }

        this.sendClickInfo();
    }

    GameMain.prototype.onChoumaSelectChanged = function () {
        //var index = this._choumaController.selectedIndex;
        // this._betChips[index];
    }

    GameMain.prototype.getSelectChoumaValue = function () {
        var index = this._choumaController.selectedIndex;
        return this._choumaOnTable[index];
    }

    GameMain.prototype.onQueRenTouZhu = function () {
        if (!this._canTouZhu) {
            return;
        }

        Laya.timer.clear(this, this._looping);
        this.isLooping = false;
        this.autoBet.detail = [];

        var fenxiang = this._btnFenXiang.selected;

        for (var i = 0; i < this._currentBet.length; i++) {
            if (this._currentBet[i] == 0)
                continue;

            var type = this._betTypesChinese[i];
            if (this._currentBet[i] < this._lowerLimit) {
                this.showMessageBox(type + __D('MINIMUM_BET_AMOUNT') + this._lowerLimit);
                return;
            }

            if (this._currentGameTotalBet[i] > this._upperLimit[i]) {
                //TODO find message for this
                this.showMessageBox(type + " 最高投注金额 " + this._upperLimit[i]);
                return;
            }
        }
        if (this._totalBetAmount <= 0)
            return;

        MyLogger(this._currentBet);


        this._canTouZhu = false;

        var self = this;
        /*self._view.getChild('button_chexiao').enabled = false;
        self._view.getChild('button_quanqing').enabled = false;
        self._view.getChild('button_chongfutouzhu').enabled = false;
        self._view.getChild('button_querentouzhu').enabled = false;*/
        self.enableBetFunction(false);
        var reqTime = Date.now();
        pomelo.request('area.playerHandler.bet', { detail: this._currentBet, fenxiang: fenxiang }, function (responseData) {
            MyLogger("responseData: " + responseData.code);
            self.addRecievedMsgCount();
            self.postClientLogs(pomelo.account,"requestBet",responseData.code,responseData.usedTime,Date.now() - reqTime);
            self._canTouZhu = true;

            var detail = responseData.detail;

            if (responseData.code == 200) {
                for (var i = 0; i < self._currentBet.length; i++)
                    self._previousBet[i] = self._currentBet[i];
                self._previousTotalBetAmount = self._totalBetAmount;
                self._view.getChild('button_chexiao').enabled = false;
                self._view.getChild('button_quanqing').enabled = false;
                self._view.getChild('button_chongfutouzhu').enabled = true;
                self._view.getChild('button_querentouzhu').enabled = false;
                self._autobetCountdownText.visible = false;
                self.setCanChat(true);
                self.updateCurrency(-self._totalBetAmount);
                self._benjuxiazhuAmount += self._totalBetAmount;
                self._benjuxiazhu.text = "" + self._benjuxiazhuAmount;//¥
                MyLogger("下注成功 当前余额 " + self._currency);

                // var text = "[color=#ff0000]第 " + self._gameNumber + " 期 下注信息[/color]<br/>";
                var text = "[color=#ff0000]" + self._gameNumber + " 期：[/color]<br/>";
                for (var i = 0; i < self._currentBet.length; i++) {
                    var type = self._betTypes[i];
                    var xinxi = self._view.getChild("xinxi_" + type).asCom;

                    var amount = detail[i];

                    if (amount > 0) {
                        text += "[color=#00ffff]" + self._betTypesChinese[i] + "\t" + amount + "[/color]" + "<br/>";


                        self.allPlayerTotalBet[i] += amount;
                        self.myPlayerTotalBet[i] += amount;
                    }
                }

                self.updateMyBet(self.myPlayerTotalBet);
                self.updateAllPlayerBet(self.allPlayerTotalBet);


                text += "<br/>";
                self._contents[1] += text;

                if (self._contentController.selectedIndex == 1) {
                    self.updateCurrentLiaoTianShiContent(text);
                }

                self.resetMyBet();
            } else {
               /* self._view.getChild('button_chexiao').enabled = true;
                self._view.getChild('button_quanqing').enabled = true;
                self._view.getChild('button_chongfutouzhu').enabled = true;
                self._view.getChild('button_querentouzhu').enabled = true; */   
                self.enableBetFunction(true);
                if (responseData.code == 3002) // TIMEOUT
                {
                    self.showMessageBox(__D('BETTING_FAILED_THE_REQUEST_TIMED_OUT'));
                } else if (responseData.code == 3003) { // SERVER_ERR
                    self.showMessageBox(__D('FAILED_BET_SERVER_ERROR'))
                } else if (responseData.code == 3004) { // NOT_ENOUGH_MONEY
                    self.showMessageBox(__D('FAILED_BET_INSUFFICIENT_BALANCE'));
                } else if (responseData.code == 3005) { // KAIJIANGZHONG
                    self.showMessageBox(__D('FAILED_BET_COUNCIL_WON'));
                } else if (responseData.code == 3006) { // KOUKUAN_TIMEOUT
                    self.showMessageBox(__D('FAILED_BET_COUNCIL_WON'));
                } else if (responseData.code == 3007) { // IN_BAN_LIST
                    self.showMessageBox(__D('FAILED_BET_BLACKLISTED_ACCOUNT'));
                } else if (responseData.code == 3008) { // NOT_IN_BET_LIMITS
                    // self.showMessageBox("投注失败，投注额应在 "+this._lowerLimit +"~"+ this._upperLimit+" 之间。");
                    self.showMessageBox(__D('FAILED_BET_OUT_OF_RANGE_AMOUNT'));
                }
            }
        });
    }

    /**
     * 自动下注结果
     */
    GameMain.prototype.onAutoBet = function (data) {
        var self = this;
        var code = data.code;

        var detail = data.detail.split(",");
        var total = 0;
        for (var i in detail) {

            total += parseFloat(detail[i]);
        }

        if (code == 200) {
            for (var i = 0; i < self._currentBet.length; i++)
                self._previousBet[i] = self._currentBet[i];
            self._previousTotalBetAmount = self._totalBetAmount;

            self.setCanChat(true);
            self.updateCurrency(-self._totalBetAmount);
            self._benjuxiazhuAmount += self._totalBetAmount;
            self._benjuxiazhu.text = "" + self._benjuxiazhuAmount;//¥
            MyLogger("下注成功 当前余额 " + self._currency);

            // var text = "[color=#ff0000]第 " + self._gameNumber + " 期 下注信息[/color]<br/>";
            var text = "[color=#ff0000]" + self._gameNumber + " 期：[/color]<br/>";
            for (var i = 0; i < self._currentBet.length; i++) {
                var type = self._betTypes[i];
                var xinxi = self._view.getChild("xinxi_" + type).asCom;

                var amount = parseFloat(detail[i]);

                if (amount > 0) {
                    text += "[color=#00ffff]" + self._betTypesChinese[i] + "\t" + amount + "[/color]" + "<br/>";


                    self.allPlayerTotalBet[i] += amount;
                    self.myPlayerTotalBet[i] += amount;
                }
            }

            self.updateMyBet(self.myPlayerTotalBet);
            self.updateAllPlayerBet(self.allPlayerTotalBet);


            text += "<br/>";
            self._contents[1] += text;

            if (self._contentController.selectedIndex == 1) {
                self.updateCurrentLiaoTianShiContent(text);
            }

            self.resetMyBet();
        } else {
            this.onQuanQing(false);
            if (total == 0) return;

            if (code == 3002) // TIMEOUT
            {
                self.showMessageBox(__D('FAILED_AUTO_BET_REQUEST_TIMEOUT'));
            } else if (code == 3003) { // SERVER_ERR
                self.showMessageBox(__D('FAILED_AUTO_BET_SERVER_ERROR'));
            } else if (code == 3004) { // NOT_ENOUGH_MONEY
                self.showMessageBox(__D('FAILED_AUTO_BET_INSUFFICIENT_BALANCE'));
            } else if (code == 3005) { // KAIJIANGZHONG
                self.showMessageBox(__D('FAILED_AUTO_BET_COUNCIL_WON'));
            } else if (code == 3006) { // KOUKUAN_TIMEOUT
                self.showMessageBox(__D('FAILED_AUTO_BET_COUNCIL_WON'));
            } else if (code == 3007) { // IN_BAN_LIST
                self.showMessageBox(__D('FAILED_AUTO_BET_BLACKLISTED_ACCOUNT'));
            } else if (code == 3008) { // NOT_IN_BET_LIMITS
                // self.showMessageBox("投注失败，投注额应在 "+this._lowerLimit +"~"+ this._upperLimit+" 之间。");
                self.showMessageBox(__D('FAILED_AUTO_BET_OUT_OF_RANGE_AMOUNT'));
            }
        }
    }


    GameMain.prototype.onBtnZidongquerenStateChange = function () {

        this.autoBet.autoBet = this._btnZidongqueren.selected;
        if(this._btnZidongqueren.selected)
        {
            var self = this;
            window.clearInterval(self.autoBetCountdownTimerId); 
            self.autoBetCountdownTimerId =window.setInterval(function(){
                self.checkAutoBet();
            },200);
        }
        else
        {
            window.clearInterval(this.autoBetCountdownTimerId); 
            this._autobetCountdownText.visible = false;
        }
        this.updateAutoBet();



        // if (this._btnZidongqueren.selected) {
        //     //Laya.timer.clear(this,this.checkZiDongQueren);
        //     //Laya.timer.frameLoop(10,this,this.checkZiDongQueren);
        //     var self = this;
        //     window.clearInterval(self.zidongTouzhuCounterdownTimerId);
        //     self.zidongTouzhuCounterdownTimerId = window.setInterval(function () {
        //         self.checkZiDongQueren();
        //     }, 200);
        // } else {
        //     //Laya.timer.clear(this,this.checkZiDongQueren);
        //     window.clearInterval(this.zidongTouzhuCounterdownTimerId);
        //     var counterDownTxt = this._btnQueRenTouZhu.getChild('counterdown').asTextField;
        //     counterDownTxt.visible = false;
        // }
    }

    GameMain.prototype.checkAutoBet = function() {
        this._autobetCountdownText.visible = false;
        if (!this._btnZidongqueren.selected){
         //Laya.timer.clear(this,this.checkZiDongQueren);
         window.clearInterval(this.autoBetCountdownTimerId); 
         return;
        }
        if(!this._btnQueRenTouZhu.enabled) return;
 
        var timeleft = this.getTouzhuLeftTime();
        if(timeleft < 0)
         return;
 
         if(timeleft > 0) {
             var txtstr = "("+Math.round(timeleft)+"s)";
             this._autobetCountdownText.text  = txtstr;
             this._autobetCountdownText.visible = true;
 
         }
         if(timeleft < 1)
         {
             this._autobetCountdownText.visible = false;
         }
     }
     GameMain.prototype.getTouzhuLeftTime = function() {
        if(this._gameState != 0)
         return -1;
 
        return this._stateCounter - (Date.now() - this._receivedCounterTime) / 1000;
     }

    GameMain.prototype.onBtnFenxiangStateChange = function () {
        this.autoBet.fenxiang = this._btnFenXiang.selected
        if (this.autoBet.autoBet)
            this.updateAutoBet();
    }

    GameMain.prototype.checkZiDongQueren = function () {

        // var counterDownTxt = this._btnQueRenTouZhu.getChild('counterdown').asTextField;
        // counterDownTxt.visible = false;
        // if (!this._btnZidongqueren.selected) {
        //     //Laya.timer.clear(this,this.checkZiDongQueren);
        //     window.clearInterval(this.zidongTouzhuCounterdownTimerId);
        //     return;
        // }

        // if (!this._btnQueRenTouZhu.enabled)
        //     return;

        // if (this._totalBetAmount <= 0)
        //     return;

        // var fapaiCounter = this._fapaiMgr.getTouzhuCounter();
        // if (fapaiCounter < 0)
        //     return;

        // if (fapaiCounter > 0) {
        //     var txtstr = "(" + Math.round(fapaiCounter) + "s)";
        //     counterDownTxt.text = txtstr;
        //     counterDownTxt.visible = true;
        // }

        // if (fapaiCounter < 1) {
        //     this._btnQueRenTouZhu.enabled = false;
        //     this._xinxiMousedownIndex = -1;
        //     this.setPreTouzhuEnabled(false);
        //     counterDownTxt.visible = false;
        //     this.onQueRenTouZhu();
        // }
    }

    GameMain.prototype.onChongFuTouZhu = function () {
        MyLogger("重复投注 prevbetamount: " + this._previousTotalBetAmount);
        if (this._previousTotalBetAmount <= 0)
            return;

        if (this._gameState !=0) {
            //TODO find text for this
            this.showMessageBox("开牌期间，无法投注。");
            return;
        }
        /*this._view.getChild('button_chexiao').enabled = true;
        this._view.getChild('button_quanqing').enabled = true;
        this._view.getChild('button_chongfutouzhu').enabled = true;
        this._view.getChild('button_querentouzhu').enabled = true;*/
        this.enableBetFunction(true);
        this.betWithReference(this._previousBet);
    }

    GameMain.prototype.onGenDan = function (evt) {
        var id = parseInt(evt);
        var data = this._playerFenXiangInfo[id];

        if (/*!this._btnQueRenTouZhu.enabled && */this._gameState !=0) {
            //TODO find text for this
            this.showMessageBox("开牌期间，无法投注。");
            return;
        }
        /*this._view.getChild('button_chexiao').enabled = true;
        this._view.getChild('button_quanqing').enabled = true;
        this._view.getChild('button_chongfutouzhu').enabled = true;
        this._view.getChild('button_querentouzhu').enabled = true;*/
        this.enableBetFunction(true);
        this.betWithReference(this._playerFenXiangInfo[id]);
    }

    GameMain.prototype.betWithReference = function (refData) {
        if (this._currency <= this._totalBetAmount) {
            this.showMessageBox(__D('INSUFFICIENT_BALANCE_PLEASE_CHARGE'));
            return;
        }

        var betValues = [0, 0, 0, 0, 0, 0, 0, 0, 0];
        var valid = false;
        for (var i = 0; i < refData.length; i++) {
            var value = refData[i];
            if (this._currentGameTotalBet[i] + value > this._upperLimit[i]) {
                value = this._upperLimit[i] - this._currentGameTotalBet[i];
            }

            betValues[i] = value;

            if (value > 0)
                valid = true;
        }

        if (!valid)
            return;

        var currencyLeft = this._currency - this._totalBetAmount;
        var betList = [];
        var currencyNotEnough = false;
        for (var i = 0; i < betValues.length; i++) {
            var value = betValues[i];

            if (value <= 0)
                continue;

            if (currencyLeft <= 0) {
                currencyNotEnough = true;
                break;
            }

            if (currencyLeft <= value) {
                value = currencyLeft;
            }
            currencyLeft -= value;

            this._currentBet[i] += value;
            this._currentGameTotalBet[i] += value;

            this.addMyPreBetValue(i, value);

            betList.push({ index: i, value: value });

            this._totalBetAmount += value;
        }

        this.checkZiDongQueren();

        this._betList.push({ listcontent: betList });

        if (currencyNotEnough) {
            this.showMessageBox(__D('INSUFFICIENT_BALANCE_PLEASE_CHARGE'));
        }
    }

    GameMain.prototype.setAllMyPreBetValue = function () {
        for (var i = 0; i < this._betTypes.length; i++) {
            this.setMyPreBetValue(i);
        }
    }

    GameMain.prototype.reduceMyPreBetValue = function (typeIndex, reduceValue) {
        this.setMyPreBetText(typeIndex);
        var type = this._betTypes[typeIndex];
        this._choumaMgr.playRewardChoumaAni(type, reduceValue);

    }

    GameMain.prototype.addMyPreBetValue = function (typeIndex, addValue) {
        this.setMyPreBetText(typeIndex);
        var type = this._betTypes[typeIndex];

        var value = addValue;
        var now = Date.now();
        if (now - this._betAddValueTimeStamp[typeIndex] < 200) {
            this._betAddValueQuickClick[typeIndex] += addValue;
            if (this._betAddValueQuickClick[typeIndex] / addValue > 8)
                this._betAddValueQuickClick[typeIndex] = addValue * 8;
            value = this._betAddValueQuickClick[typeIndex];

        } else {
            this._betAddValueQuickClick[typeIndex] = 0;
        }
        this._betAddValueTimeStamp[typeIndex] = now;

        var total = this._currentBet[typeIndex] + this.allPlayerTotalBet[typeIndex];

        this._choumaMgr.playBetChoumaAni(type, value, total);

        //更新自动投注
        this.updateAutoBetLooping(true);

    }

    GameMain.prototype.setMyPreBetValue = function (typeIndex) {
        this.setMyPreBetText(typeIndex);
        var type = this._betTypes[typeIndex];
        var value = this._currentBet[typeIndex];
        var total = this._currentBet[typeIndex] + this.allPlayerTotalBet[typeIndex];
        if (value > 0)
            this._choumaMgr.playBetChoumaAni(type, value, total);
    }

    GameMain.prototype.setMyPreBetText = function (typeIndex) {
        var value = this._currentBet[typeIndex];
        var type = this._betTypes[typeIndex];
        var xinxi = this._view.getChild("xinxi_" + type);
        var myPreBetText = xinxi.getChild('myPreBet').asTextField;
        if (value > 0)
            myPreBetText.text = '¥' + value;
        else
            myPreBetText.text = '';

    }

    GameMain.prototype.clearMyPreBetValue = function (typeIndex) {
        for (var i = 0; i < this._betTypes.length; i++) {
            var type = this._betTypes[i];
            var btn = this._view.getChild("xinxi_" + type);
            var myPreBetText = btn.getChild('myPreBet').asTextField;
            myPreBetText.text = '';
        }
    }

    GameMain.prototype.onClickLeaderboard = function () {
        //var last = this._leaderboardController.selectedIndex;
        //this._leaderboardController.selectedIndex = (last + 1) % 2;
        if (this._leaderboardController.selectedIndex == 1) 
            return;
        this._leaderboardController.selectedIndex = 1;
        if (this._leaderboardController.selectedIndex == 1) {
            var self = this;
            var reqTime = Date.now();
           // var game = this.getGameType();
           // var date = getNowFormatDate();
            pomelo.request("area.playerHandler.getLeaderboard", {}, function (data) {
            //   this.postLeadBoardRequest(game,date,function(data){
                self.addRecievedMsgCount();
                if (!!data)
                    self._leaderboard.setLBDatas(data.datas);
                self.postClientLogs(pomelo.account,"requestLeaderboard",200,data.usedTime,Date.now() - reqTime);
            });

        }

        this._help.visible = false;
        this._settings.visible = false;
        this._menu_ctrl.selectedIndex = 0;
    }

    GameMain.prototype.onClickZhuPanLuItem = function (evt) {
        var itemIdx = fairygui.GObject.cast(evt.currentTarget).name.substr(6);

        if (itemIdx >= this._zhupanContent.length)
            return;

        var num = this._zhupanContent[itemIdx][1];
        var info = this._zhupanContent[itemIdx][2];

        //TODO find out what this string maps to
        var text = "期数：" + num + "\n";
        text += __D('STATS_PANEL_VILLAGE');
        for (var i = 0; i < info.zhuang.length; i++) {
            var idx = info.zhuang[i] - 1;
            text += this._cardValues[idx] + " ";
        }
        text += ";" + __D('IDLE');
        for (var i = 0; i < info.xian.length; i++) {
            var idx = info.xian[i] - 1;
            text += this._cardValues[idx] + " ";
        }
        text += "\n";

        //TODO language
        var tmp = this._betTypesChinese[info.yingjia] + "【" + info.dianshu + "】点";
        if (info.yingjia == 0 || info.yingjia == 1) {
            tmp += __D('VICTORY_STRING');
        }
        else if (info.yingjia == 2) {
            tmp += __D('STATS_PANEL_WITH');
        }

        for (var i = 0; i < info.leixing.length; i++) {
            tmp += "; ";
            tmp += this._betTypesChinese[info.leixing[i]] + " ";
        }

        text += tmp;

        this._infoZhupan.visible = true;
        this._infoZhupan.getChild("n1").text = text;

        Laya.stage.on("mousedown", this, this.hideInfoZhuPan);
    }

    GameMain.prototype.hideInfoZhuPan = function () {
        this._infoZhupan.visible = false;
        Laya.stage.off("mousedown", this, this.hideInfoZhuPan);
    }

    GameMain.prototype.initLuZhiTu = function (data) {
        // var zhupanluIdx = 0;
        // while(true)
        // {
        //     var item = this._luzhitu.getChild("zhupan" + zhupanluIdx);
        //     if(item == null)
        //         break;

        //     item.onClick(this, this.onClickZhuPanLuItem);

        //     zhupanluIdx++;
        // }

        this._luzhitu.getChild("zwl0").url = "";
        this._luzhitu.getChild("zwl1").url = "";
        this._luzhitu.getChild("zwl2").url = "";
        this._luzhitu.getChild("xwl0").url = "";
        this._luzhitu.getChild("xwl1").url = "";
        this._luzhitu.getChild("xwl2").url = "";

        this._statsYingjia = [0, 0, 0]; // 庄，闲，和
        this._statsDuizi = [0, 0, 0, 0]; // 没有对子，庄对，闲对，庄对+闲对 
        this._zhupanContent = [];
        this._daluContent = [];
        this._daxiaoluContent = [];
        var tmpZhuPan = [];
        var daxiaolu = [];

        for (var i = 0; i < data.length; i++) {
            var content = data[i];
            var dalu = []; // 每个元素为数组: 0 赢家（庄/闲）, 1 和的个数 
            for (var j = 0; j < content.length; j++) {
                var yingjia = content[j][0];
                var duizi = content[j][1];
                var num = content[j][2];
                var jieguo = content[j][3]; // zhuang, xian, yingjia, dianshu, leixing
                var winner = jieguo.yingjia;
                if(yingjia != winner)
                {
                    yingjia = winner;
                }
                jieguo.zhuang = this.rawCards(jieguo.zhuang);
                jieguo.xian = this.rawCards(jieguo.xian);

                var tmpResult = []; // 除去庄闲之外的其他结果
                for (var tmpIdx = 0; tmpIdx < jieguo.leixing.length; tmpIdx++) {
                    var value = jieguo.leixing[tmpIdx];
                    if (value > 2)
                        tmpResult.push(value);

                    if (value == 5 || value == 6) // 大小
                    {
                        daxiaolu.push(value);
                    }
                }
                data.leixing = tmpResult;

                // 珠盘路
                var name = this._luzi[yingjia].toString() + duizi.toString();
                tmpZhuPan.push([name, num, jieguo]);

                //大路
                if (yingjia == 2) // 和
                {
                    if (dalu.length == 0) {
                        dalu.push([2, 1]);
                    } else {
                        var last = dalu[dalu.length - 1];
                        last[1]++;
                        dalu[dalu.length - 1] = last;
                    }
                }
                else // 庄/闲
                {
                    if (dalu.length == 0) {
                        dalu.push([yingjia, 0]);
                    } else {
                        var last = dalu[dalu.length - 1];
                        if (last[0] == 2) {
                            dalu.push([yingjia, last[1]]);
                            dalu.shift();
                        } else {
                            dalu.push([yingjia, 0]);
                        }

                    }
                }

                // 赢家，对子统计
                this._statsYingjia[yingjia]++;
                this._statsDuizi[duizi]++;
            }

            this._daluContent.push(dalu);
        }

        this._zhupanContent = tmpZhuPan.slice(0, this._zhupanluTotalNum);

        this.updateStats();

        this.showZhuPanLu();
        this.showDaLu();
        this.initDaXiaoLu(daxiaolu);

        this._dayanluContent = [];
        this._xiaoluContent = [];
        this._xiaoqiangluContent = [];
        //TODO language
        this.initYanShenLuZhi(this._dayanluContent, "红圈", "蓝圈", 2, 2, 1, this._dayanluTotalCols);
        this.initYanShenLuZhi(this._xiaoluContent, "红色小", "蓝色小", 2, 3, 2, this._xiaoluTotalCols);
        this.initYanShenLuZhi(this._xiaoqiangluContent, "红色斜", "蓝色斜", 2, 4, 3, this._xiaoqiangluTotalCols);

        this.showYanShenLuZhi("dayanlu", this._dayanluContent);
        this.showYanShenLuZhi("xiaolu", this._xiaoluContent);
        this.showYanShenLuZhi("xiaoqianglu", this._xiaoqiangluContent);

        this.showWenLu(); // 庄问路/闲问路
    }

    GameMain.prototype.showWenLu = function () {
        var daluCopy = []
        for (var i = 0; i < this._daluContent.length; i++) {
            daluCopy[i] = this._daluContent[i].slice(0);
        }

        this.updateDaLu(daluCopy, 0, true);
    }

    // 初始化从大路延伸出来的其他路子
    // 大眼路 从大路的第2列第2行开始分析，若那个位置没有结果，则从大路的第3列第1行开始分析
    // 小路   从大路的第3列第2行开始分析，若那个位置没有结果，则从大路的第4列第1行开始分析
    // 小强路 从大路的第4列第2行开始分析，若那个位置没有结果，则从大路的第5列第1行开始分析
    GameMain.prototype.initYanShenLuZhi = function (target, type1, type2, startRow, startCol, colGap, maxCol) {
        // if(this._daluContent.length < startCol || this._daluContent[startCol-1].length < startRow)
        //     return;

        for (var i = startCol - 1; i < this._daluContent.length; i++) {
            for (var j = 0; j < this._daluContent[i].length; j++) {
                if (i == startCol - 1 && j == 0)
                    continue;

                var type;
                if (j > 0) {
                    var refContent = this._daluContent[i - colGap];
                    if (!!(refContent[j])) // 相隔gap列对应的位置为 庄/闲
                        type = type1; // 红圈
                    else { // 相隔gap列对应位置为空
                        type = type2; // 蓝圈
                        if (j > 1 && (refContent[j - 1] == null || refContent[j - 1] == undefined))
                            type = type1;
                    }
                } else {
                    if (this._daluContent[i - 1].length == this._daluContent[i - 1 - colGap].length)
                        type = type1; // 红圈 
                    else
                        type = type2; // 蓝圈
                }

                var len = target.length;
                if (len == 0 || target[len - 1][0] != type) {
                    target.push([type, 1]);
                } else {
                    target[len - 1][1]++;
                }
            }
        }

        this.recalculateDaLuColumns(target, maxCol);
    }

    GameMain.prototype.updateYanShenLuZhi = function (refData, target, type1, type2, startRow, startCol, colGap, maxCol, i, j, isWenLu) {
        var res_types = [null, null];
        if ((i + 1 < startCol) || ((i + 1 == startCol) && (j + 1 < startRow)))
            return res_types;

        var type;
        if (j > 0) {
            var refContent = refData[i - colGap];
            if (!!(refContent[j])) // 相隔gap列对应的位置为 庄/闲
                type = type1; // 红圈
            else { // 相隔gap列对应位置为空
                type = type2; // 蓝圈
                if (j > 1 && (refContent[j - 1] == null || refContent[j - 1] == undefined))
                    type = type1;
            }
        } else {
            if (refData[i - 1].length == refData[i - 1 - colGap].length)
                type = type1; // 红圈 
            else
                type = type2; // 蓝圈
        }

        if (!isWenLu) {
            var len = target.length;
            if (len == 0 || target[len - 1][0] != type) {
                target.push([type, 1]);
            } else {
                target[len - 1][1]++;
            }

            this.recalculateYanShenLuZhiCol(target, maxCol,false);
        }

        if (type === type1) res_types = [type1, type2];
        else res_types = [type2, type1];
        return res_types;

    }

    GameMain.prototype.recalculateYanShenLuZhiCol = function (target, maxColumns,isDaXiaoLu) {
        var used = {};
        var totalColumns = 0;
        for (var j = 0; j < target.length; j++) {
            var currentColumns = j + 1;
            var canGoDown = true;
            var nextSlot = 0;
            for (var i = 0; i < target[j][1]; i++) {

                if (canGoDown) {
                    nextSlot = j * 6 + i;
                    if (i >= 6 || used[nextSlot]) {
                        if(isDaXiaoLu)
                        {
                            break;
                        }
                        else
                        {
                            canGoDown = false;
                            nextSlot = j * 6 + i + 5;
                            currentColumns++;
                        }
                    }
                } else {
                    nextSlot += 6;
                    currentColumns++;
                }

                used[nextSlot] = true;
            }

            if (currentColumns > totalColumns) {
                totalColumns = currentColumns;
            }
        }

        if (target.length > maxColumns || totalColumns > maxColumns)
            target.shift();
    }

    GameMain.prototype.showYanShenLuZhi = function (type, contents) {
        var n = 0;
        while (true) {
            var loader = this._luzhitu.getChild(type + n);
            if (loader == null)
                break;

            loader.url = "";
            n++;
        }

        var used = {};
        for (var j = 0; j < contents.length; j++) {
            var canGoDown = true;
            var nextSlot = 0;
            for (var i = 0; i < contents[j][1]; i++) {
                if (canGoDown) {
                    nextSlot = j * 6 + i;
                    if (i >= 6 || used[nextSlot]) {
                        canGoDown = false;
                        nextSlot = j * 6 + i + 5;
                    }
                } else {
                    nextSlot += 6;
                }

                var loader = this._luzhitu.getChild(type + nextSlot);
                if (loader == null)
                    break;
                loader.url = "ui://" + this.filePreName + "MainUI/" + contents[j][0];
                loader.visible = true;
                used[nextSlot] = true;
            }
        }
    }

    GameMain.prototype.showZhuPanLu = function () {
        for (var i = 0; i < this._zhupanluTotalNum; i++) {
            var loader = this._luzhitu.getChild("zhupan" + i);
            loader.url = "";

            loader.onClick(this, this.onClickZhuPanLuItem);
        }
        for(var i=0;i< 10;i++)
        {
            var display_num = this._luzhitu.getChild("num_" + i).asCom;
            display_num.visible=false;
            display_num.getChild("num").text = "";
        }
        var data = this._zhupanContent;
        for (var i = 0; i < data.length; i++) {
            var url = "ui://" + this.filePreName + "MainUI/" + data[i][0];
            var loader = this._luzhitu.getChild("zhupan" + i);
            loader.url = url;
            loader.visible = true;
        }
    }

    GameMain.prototype.initDaXiaoLu = function (datas) {
        if (datas.length == 0)
            return;

        this._daxiaoluContent = [];
        this._daxiaoluContent.push([datas[0], 1]);

        for (var i = 1; i < datas.length; i++) {
            var data = datas[i];

            if (this._daxiaoluContent.length == 0) {
                this._daxiaoluContent.push([data, 1]);
                continue;
            }

            var last = this._daxiaoluContent[this._daxiaoluContent.length - 1];
            if (last[0] == data) {
                last = [last[0], last[1] + 1];

               // if (last[1] >= this._daxiaoluTotalCols + 5) 
               //     last[1] = this._daxiaoluTotalCols + 5;

                this._daxiaoluContent[this._daxiaoluContent.length - 1] = last;
            } else {
                this._daxiaoluContent.push([data, 1]);
            }

            this.recalculateYanShenLuZhiCol(this._daxiaoluContent, this._daxiaoluTotalCols,true);
        }
    }

    GameMain.prototype.showDaXiaoLu = function () {
        for (var i = 0; i < this._zhupanluTotalNum; i++) {
            var loader = this._luzhitu.getChild("zhupan" + i);
            loader.url = "";
            loader.offClick(this, this.onClickZhuPanLuItem);
        }
        for(var i=0;i< 10;i++)
        {
            var display_num = this._luzhitu.getChild("num_" + i).asCom;
            display_num.visible=false;
            display_num.getChild("num").text = "";
        }
        var contents = this._daxiaoluContent;
        var used = {};
        for (var j = 0; j < contents.length; j++) {
            var canGoDown = true;
            var nextSlot = 0;

            var name = contents[j][0] == 5 ? "dalu" : "xiaolu";

            for (var i = 0; i < contents[j][1]; i++) {
                if (canGoDown) {
                    nextSlot = j * 6 + i;
                    if (i >= 6 || used[nextSlot]) {
                        var display_num = this._luzhitu.getChild("num_" + j).asCom;
                        display_num.visible=true;
                        display_num.getChild("num").text = ""+(contents[j][1]);
                        break;
                       // canGoDown = false;
                       // nextSlot = j * 6 + i + 5;
                    }
                } else {
                    nextSlot += 6;
                }

                used[nextSlot] = true;

                var loader = this._luzhitu.getChild("zhupan" + nextSlot);
                if (loader == null)
                    break;
                loader.url = "ui://" + this.filePreName + "MainUI/" + name;
            }
        }
    }

    GameMain.prototype.updateDaXiaoLu = function (data) {

        if (this._daxiaoluContent.length == 0) {
            this._daxiaoluContent.push([data, 1]);
        } else {
            var last = this._daxiaoluContent[this._daxiaoluContent.length - 1];
            if (last[0] == data) {
                last = [last[0], last[1] + 1];

               // if (last[1] >= this._daxiaoluTotalCols + 5) 
               //     last[1] = this._daxiaoluTotalCols + 5;

                this._daxiaoluContent[this._daxiaoluContent.length - 1] = last;
            } else {
                this._daxiaoluContent.push([data, 1]);
            }

            this.recalculateYanShenLuZhiCol(this._daxiaoluContent, this._daxiaoluTotalCols,true);
        }

        if (this._luzhiController.selectedIndex == 1) {
            this.showDaXiaoLu();
        }

    }

    GameMain.prototype.recalculateDaXiaoLuColumns = function () {

    }

    // 根据开奖结果更新 data格式: 庄0
    GameMain.prototype.updateZhuPanLu = function (data) {
        jieguo = {};
        jieguo["zhuang"] = this._raw_zhuang_values
        jieguo["xian"] = this._raw_xian_values
        jieguo["yingjia"] = this._yingjia;
        jieguo["dianshu"] = this._yingjiadianshu;
        jieguo["leixing"] = this._otherResults;

        this._zhupanContent.push([data, this._gameNumber, jieguo]);
        var len = this._zhupanContent.length;
        if (len > this._zhupanluTotalNum)
            this._zhupanContent.shift();

        if (this._luzhiController.selectedIndex != 0)
            return;

        if (len <= this._zhupanluTotalNum) {
            var url = "ui://" + this.filePreName + "MainUI/" + data;
            var index = len - 1;
            var loader = this._luzhitu.getChild("zhupan" + index);
            loader.url = url;
            loader.visible = true;
        } else {
            this.showZhuPanLu();
        }
    }

    GameMain.prototype.updateDaLu = function (target, yingjia, isWenLu) {
        var len = target.length;
        var result = [yingjia, 0];
        if (len == 0) {
            target.push([result]);
        }
        else {
            var last = target[len - 1];
            if (yingjia == 2) // 和
            {
                last[last.length - 1][1]++;
                target[len - 1] = last;
            }
            else // 庄/闲
            {
                var allHe = true;
                for (var i = 0; i < last.length; i++) {
                    if (last[i][0] == yingjia)
                        break;

                    if (last[i][0] != 2)
                        allHe = false;
                }

                if (i < last.length || allHe) {
                    last.push(result);
                    target[len - 1] = last;
                }
                else {
                    target.push([result]);
                }
            }
        }

        this.recalculateDaLuColumns(target, this._daluTotalCols);

        // if(wenLu === "zwl" || wenLu === "xwl") {
        if (isWenLu) {
            var i = target.length - 1;
            var j = target[target.length - 1].length - 1;
            var type;
            //TODO language
            type = this.updateYanShenLuZhi(target, this._dayanluContent, "红圈", "蓝圈", 2, 2, 1, this._dayanluTotalCols, i, j, true);
            this._luzhitu.getChild("zwl0").url = "ui://" + this.filePreName + "MainUI/" + type[0];
            this._luzhitu.getChild("xwl0").url = "ui://" + this.filePreName + "MainUI/" + type[1];
            type = this.updateYanShenLuZhi(target, this._xiaoluContent, "红色小", "蓝色小", 2, 3, 2, this._xiaoluTotalCols, i, j, true);
            this._luzhitu.getChild("zwl1").url = "ui://" + this.filePreName + "MainUI/" + type[0];
            this._luzhitu.getChild("xwl1").url = "ui://" + this.filePreName + "MainUI/" + type[1];
            type = this.updateYanShenLuZhi(target, this._xiaoqiangluContent, "红色斜", "蓝色斜", 2, 4, 3, this._xiaoqiangluTotalCols, i, j, true);
            this._luzhitu.getChild("zwl2").url = "ui://" + this.filePreName + "MainUI/" + type[0];
            this._luzhitu.getChild("xwl2").url = "ui://" + this.filePreName + "MainUI/" + type[1];
        }
        else {

            this.showDaLu();

            // 开奖结果不为和，更新从大路延伸出来的路子
            if (yingjia != 2) {
                var i = this._daluContent.length - 1;
                var j = this._daluContent[this._daluContent.length - 1].length - 1;
                //TODO language
                this.updateYanShenLuZhi(target, this._dayanluContent, "红圈", "蓝圈", 2, 2, 1, this._dayanluTotalCols, i, j, false);
                this.updateYanShenLuZhi(target, this._xiaoluContent, "红色小", "蓝色小", 2, 3, 2, this._xiaoluTotalCols, i, j, false);
                this.updateYanShenLuZhi(target, this._xiaoqiangluContent, "红色斜", "蓝色斜", 2, 4, 3, this._xiaoqiangluTotalCols, i, j, false);

                this.showYanShenLuZhi("dayanlu", this._dayanluContent);
                this.showYanShenLuZhi("xiaolu", this._xiaoluContent);
                this.showYanShenLuZhi("xiaoqianglu", this._xiaoqiangluContent);
            }
        }
    }

    GameMain.prototype.recalculateDaLuColumns = function (target, maxColumns) {
        // 重新计算列数
        var used = {};
        var totalColumns = 0;
        for (var j = 0; j < target.length; j++) {
            var currentColumns = j + 1;
            var canGoDown = true;
            var nextSlot = 0;
            var content = target[j];
            for (var i = 0; i < content.length; i++) {
                // if(content[k][0] == this.consts.BetTypeIndex.HE)
                //     continue;

                if (canGoDown) {
                    nextSlot = j * 6 + i;
                    if (i >= 6 || used[nextSlot]) {
                        canGoDown = false;
                        nextSlot = j * 6 + i + 5;
                        currentColumns++;
                    }
                } else {
                    nextSlot += 6;
                    currentColumns++;
                }

                used[nextSlot] = true;
            }

            if (currentColumns > totalColumns) {
                totalColumns = currentColumns;
            }
        }

        if (target.length > maxColumns || totalColumns > maxColumns)
            target.shift();
    }

    // 每一列 [[庄, 和的个数], [庄, 和的个数], [庄，和的个数], ...]
    GameMain.prototype.showDaLu = function () {
        var n = 0;
        while (true) {

            var c = this._luzhitu.getChild("dalu" + n);
            if (c == null)
                break;

            c.getChild("loader").url = "";
            c.getChild("num").visible = false;

            n++;
        }

        var contents = this._daluContent;
        var used = {};
        for (var j = 0; j < contents.length; j++) {
            var canGoDown = true;
            var nextSlot = 0;
            var i = 0;

            for (var k = 0; k < contents[j].length; k++) {
                if (canGoDown) {
                    nextSlot = j * 6 + i;
                    if (i >= 6 || used[nextSlot]) {
                        canGoDown = false;
                        nextSlot = j * 6 + i + 5;
                    }
                } else {
                    nextSlot += 6;
                }

                used[nextSlot] = true;

                i++;

                var yingjia = contents[j][k][0];
                var heNum = contents[j][k][1];

                var name = "";
                if (yingjia == 2) {
                    //TODO language
                    name = "大路和";
                } else {
                    //TODO language
                    if (heNum > 0)
                        name = (yingjia == 0) ? "大路庄和" : "大路闲和";
                    else
                        name = (yingjia == 0) ? "大路庄" : "大路闲";
                }

                var c = this._luzhitu.getChild("dalu" + nextSlot);
                if (c == null)
                    break;
                c.getChild("loader").url = "ui://" + this.filePreName + "MainUI/" + name;
                var tf = c.getChild("num").asTextField;
                tf.text = heNum.toString();
                tf.visible = (heNum > 1);
            }
        }
    }

    GameMain.prototype.updateStats = function () {
        this._tfStatZhuang.text = this._statsYingjia[0].toString(); // 庄
        this._tfStatXian.text = this._statsYingjia[1].toString(); // 闲
        this._tfStatHe.text = this._statsYingjia[2].toString(); // 和

        this._tfStatZhuangDui.text = this._statsDuizi[1] + this._statsDuizi[3]; // 庄对
        this._tfStatXianDui.text = this._statsDuizi[2] + this._statsDuizi[3]; // 闲对
        this._tfStatTotal.text = this._statsYingjia[0] + this._statsYingjia[1] + this._statsYingjia[2];
    }

    // TODO: 按下全清时判断状态
    GameMain.prototype.onQuanQing = function (doUpdating) {
        if(this._gameState != 0)
            return;

        for (var i = 0; i < this._betTypes.length; i++) {
            var type = this._betTypes[i];

            if (this._currentBet[i] > 0) {
                var value = this._currentBet[i];
                this._currentBet[i] = 0;
                this._currentGameTotalBet[i] -= value;
                this.reduceMyPreBetValue(i, value);
            }
        }

        this.resetMyBet();

        this.updateTableChouma();

        MyLogger(this._betList);

        //更新自动投注
        this.updateAutoBetLooping(doUpdating);
        this._view.getChild('button_chexiao').enabled = false;
        this._view.getChild('button_quanqing').enabled = false;
        this._view.getChild('button_chongfutouzhu').enabled = true;
        this._view.getChild('button_querentouzhu').enabled = false;
        this._autobetCountdownText.visible = false;
    }
    GameMain.prototype.formatName = function (name) {
        if (name.length <=10) {
            if(name.length > 3)
                name = "***" + name.slice(3);
            else
            {
                if(name.length == 1)
                    name = "***" + name;
                else
                    name = "***" + name.slice(1);
            }
        }
        else
        {
            if(name.length >10){
                name = name.slice(0,10);
                name = "***" + name.slice(3)+"...";
            }
        }
        return name;
    }
    GameMain.prototype.onCheXiao = function () {
        if (this._betList.length == 0) {
            return;
        }

        var lastBet = this._betList.pop();
        MyLogger(this._betList);
        MyLogger(lastBet);

        var lastbetlist = [];
        if ("listcontent" in lastBet) {
            lastbetlist = lastBet["listcontent"];
        }
        else
            lastbetlist.push(lastBet);

        for (var i = 0; i < lastbetlist.length; i++) {
            var item = lastbetlist[i];
            var index = item.index;
            var value = item.value;

            this._currentBet[index] -= value;
            this._currentGameTotalBet[index] -= value;
            this._totalBetAmount -= value;

            var type = this._betTypes[index];


            this.reduceMyPreBetValue(index, value);
        }
        if(this._betList.length == 0){
            this.enableBetFunction(false);
        }
        this.updateTableChouma();

        //更新自动投注
        this.updateAutoBetLooping(true);
    }

    GameMain.prototype.onQuerenZuihoutouzhu = function () {
        (this.addBet(this._zuihoutouzhu.betTypeIndex, 0))();
        this._zuihoutouzhu.visible = false;
        this._zuihoutouzhuController.selectedIndex = 0;
    }
    GameMain.prototype.onQuxiaoZuihoutouzhu = function () {
        this._zuihoutouzhu.visible = false;
        this._zuihoutouzhuController.selectedIndex = 0;
    }

    GameMain.prototype.startGame = function (gamenumber) {

        this.setCanChat(false);
        this._btnQueRenTouZhu.enabled = false;
        this.setPreTouzhuEnabled(false);
        this.resetBet();
        MyLogger("startGame 当前余额: " + this._currency);

        SoundManager.playSound(this.resourceSoundDir + "start.mp3", 1);
        //var content = this._kaishixiazhu.getChild("n1").asTextField;
        //content.text = "新局已开\n局号："+gamenumber;
        //content.text = "投注开始。";
        var self = this;
        setTimeout(function () {
            //self._kaishixiazhu.visible = false;
            var t = self._view.getTransition('t0');
            t.play(Handler.create(self, self.onKaishiyouxiAniPlayend));

        }, 2000);

        this._benjuxiazhu.text = "0";//¥
        this._zhuanghuyue.text = "" + this._currency;
    }

    GameMain.prototype.onKaishiyouxiAniPlayend = function () {
        var t = this._view.getTransition('t3');
        t.play();
    }

    GameMain.prototype.startBet = function (gamenumber) {
        this.setCanChat(false);
        //this._btnQueRenTouZhu.enabled = true;
        
        this.resetBet();
        MyLogger("startBet 当前余额: " + this._currency);

        //this._kaishixiazhu.visible = true;
        SoundManager.playSound(this.resourceSoundDir + "start.mp3", 1);

        var self = this;
        setTimeout(function () {
            self._kaishixiazhu.visible = true;
            self.setPreTouzhuEnabled(true);
        }, 2000);
        setTimeout(function () {
            self._kaishixiazhu.visible = false;
            var t = self._view.getTransition('t0');
            t.play(Handler.create(self, self.onKaishixiazhuAniPlayend));

        }, 2500);

        this._benjuxiazhu.text = "0";
        this._zhuanghuyue.text = "" + this._currency;
    }
    GameMain.prototype.onKaishixiazhuAniPlayend = function () {
        var t = this._view.getTransition('t3');
        t.play();
    }

    GameMain.prototype.addBet = function (typeIndex, argValue) {
        var thisObj = this;
        return function () {

            var value = argValue;

            var type = thisObj._betTypes[typeIndex];

            if (value == 0) {
                // 梭哈
                value = thisObj._currency - thisObj._totalBetAmount;
                if (thisObj._currentGameTotalBet[typeIndex] + value > thisObj._upperLimit[typeIndex]) {
                    value = thisObj._upperLimit[typeIndex] - thisObj._currentGameTotalBet[typeIndex];

                    if (value == 0) {
                        //TODO find text for this
                        thisObj.showMessageBox(thisObj._betTypesChinese[typeIndex] + " 最高投注金额 " + thisObj._upperLimit[typeIndex]);
                        return;
                    }
                }

                value = Math.floor(value * 10000) / 10000;
            }

            if (value <= 0) {
                MyLogger(__D('INSUFFICIENT_BALANCE'));
                thisObj.showMessageBox(__D('INSUFFICIENT_BALANCE_PLEASE_CHARGE'));
                return;
            }

            if (thisObj._totalBetAmount + value > thisObj._currency) {
                var leftmoney = thisObj._currency - thisObj._totalBetAmount;
                leftmoney = Math.floor(leftmoney * 10000) / 10000;
                if (leftmoney > 0) {
                    var content = thisObj._zuihoutouzhu.getChild("content").asTextField;
                    content.text = __D('ASK_FINAL_BET_AMOUNT', {
                        moneyLeft: leftmoney
                    })
                    thisObj._zuihoutouzhu.betTypeIndex = typeIndex;
                    thisObj._zuihoutouzhu.visible = true;
                    thisObj._zuihoutouzhuController.selectedIndex = 1;
                }
                else {
                    thisObj.showMessageBox(__D('INSUFFICIENT_BALANCE_PLEASE_CHARGE'));
                }

                return;
            }

            var i = typeIndex;
            {

                var bt = thisObj._betTypesChinese[i];

                if (thisObj._currentGameTotalBet[i] + value > thisObj._upperLimit[i]) {
                    var valueToLimit = thisObj._upperLimit[i] - thisObj._currentGameTotalBet[i];
                    //TODO get text for this
                    thisObj.showMessageBox(bt + " 最高投注金额 " + thisObj._upperLimit[i]);
                    if (valueToLimit == 0) {
                        return;
                    } else {
                        value = valueToLimit;
                    }
                }
            }


            // if(thisObj._gameState != 0)
            // {
            //     MyLogger("当前状态不能下注。");
            //     thisObj.showMessageBox("发牌开奖中，无法投注。");
            //     return;
            // }

            thisObj._currentBet[typeIndex] += value;
            thisObj._currentGameTotalBet[typeIndex] += value;

            thisObj._totalBetAmount += value;

            thisObj._betList.push({ index: typeIndex, value: value });

            thisObj.addMyPreBetValue(typeIndex, value);

            thisObj.checkZiDongQueren();
            /*thisObj._view.getChild('button_chexiao').enabled = true;
            thisObj._view.getChild('button_quanqing').enabled = true;
            thisObj._view.getChild('button_chongfutouzhu').enabled = true;
            thisObj._view.getChild('button_querentouzhu').enabled = true;*/
            thisObj.enableBetFunction(true);
        }
    }

    GameMain.prototype.resetBet = function () {
        //this.resetMyBet();
        this.resetAllPlayerBet();
        this._benjuxiazhuAmount = 0;
        for (var i = 0; i < this._currentGameTotalBet.length; i++) {
            this._currentGameTotalBet[i] = this._currentBet[i];
        }
    }

    GameMain.prototype.resetMyBet = function () {

        this._totalBetAmount = 0;
        this._totalBet = 0;
        this._reward = 0;
        for (var i = 0; i < this._betTypes.length; i++) {
            var type = this._betTypes[i];

            this._currentBet[i] = 0;
        }

        this.clearMyPreBetValue();

        this._betList = [];
    }

    GameMain.prototype.updateMyBet = function (betinfo) {
        this.myPlayerTotalBet = betinfo;
        var totalAmount = 0;
        for (var i = 0; i < betinfo.length; i++) {
            var type = this._betTypes[i];
            var xinxi = this._view.getChild("xinxi_" + type).asCom;

            if (betinfo[i] > 0) {
                totalAmount += betinfo[i];

                this.setMaskShow(i);

                if (xinxi.getChild("myBet").text == betinfo[i].toString())
                    continue;
                //xinxi.getChild("myBet").text = betinfo[i].toString();
                var t = xinxi.getTransition('t1');
                t.play(Handler.create(this, this.onBetTextPlayend, [xinxi.getChild("myBet"), betinfo[i]]));
            }
        }
        if (totalAmount > 0) {
            this._benjuxiazhu.text = "" + totalAmount;//¥
            this._benjuxiazhuAmount = totalAmount;

            this.setCanChat(true);
        }
    }

    GameMain.prototype.resetAllPlayerBet = function () {
        for (var i = 0; i < this._betTypes.length; i++) {
            var type = this._betTypes[i];

            var xinxi = this._view.getChild("xinxi_" + type).asCom;
            xinxi.getChild("totalBet").text = "0";
            xinxi.getChild("myBet").text = "";
        }
    }

    GameMain.prototype.updateAllPlayerBet = function (betInfo) {
        this.allPlayerTotalBet = betInfo;
        for (var i = 0; i < betInfo.length; i++) {
            var type = this._betTypes[i];
            var xinxi = this._view.getChild("xinxi_" + type).asCom;
            //xinxi.getChild("totalBet").text = betInfo[i];

            if (xinxi.getChild("totalBet").text != betInfo[i].toString()) {
                var t = xinxi.getTransition('t0');
                t.play(Handler.create(this, this.onBetTextPlayend, [xinxi.getChild("totalBet"), betInfo[i]]));

                //if (i >= 0 && i <= 2) 
                {// all             
                    this._choumaMgr.setTableChouma(type, betInfo[i]);
                }

            }
        }
    }

    GameMain.prototype.updateTableChouma = function () {
        var betInfo = this.allPlayerTotalBet;
        var curBetInfo = this._currentBet;
        for (var i = 0; i < betInfo.length; i++) {
            var type = this._betTypes[i];
            this._choumaMgr.setTableChouma(type, betInfo[i] + curBetInfo[i]);
        }
    }

    GameMain.prototype.onBetTextPlayend = function (obj, number) {
        number = Math.floor(number * 10) / 10;
        obj.text = number.toString();
    }

    GameMain.prototype.setCurrency = function (value) {

        this._currency = value;
        this._currency = Math.floor(this._currency * 10000) / 10000;
        MyLogger("setCurrency " + this._currency);
        this._zhuanghuyue.text = "" + this._currency;
    }

    GameMain.prototype.updateCurrency = function (value) {
        this._currency += value;
        this._currency = Math.floor(this._currency * 10000) / 10000;
        this._zhuanghuyue.text = "" + this._currency;
    }

    GameMain.prototype.getCurrency = function () {

       return this._currency;
       
    }
    GameMain.prototype.onGainDataUpdate = function (value,date) {
        if(isNaN(value))
        {
            value = 0;
        }
        if(date == getNowFormatDate())
        {
            var v = Math.round(value * 100) / 100;
            if (v < 0) {
                this._meiriyingkui.text = "-" + (-v).toString();
            } else {
                this._meiriyingkui.text = "" + v.toString();
            }
        }
    }
    GameMain.prototype.onPlayerGainUpdate = function (value) {
        if(isNaN(value))
        {
            value = 0;
        }
        var Host = window.location.hostname;
        if(Host == "127.0.0.1")
        {
            //for local
            var date = getNowFormatDate();
            this.onGainDataUpdate(value,date);
        }
        else
        {
            var date = getNowFormatDate();
            var self = this;
            var gamename = ["BJL","bjl_exchange","TEXAS","SGJ","EXTEXAS","SG","QQC","BULL","FQZS","SLWH"];
        //    var gamename_cn = ["荣一百家乐","走地百家乐","百人德州","水果拉霸","走地德州","百人三公","荣一彩球","百人牛牛","飞禽走兽","森林舞会"];
            this.postGainRequest(1,date,0,0,function(res){
        //  pomelo.request("area.playerHandler.getAllGameGainData", {type:1,date:date}, function (res) {
                if (!!res)
                {
                    var resData = res.data;
                    var total = 0;
                    var curDate = '';
                    for(var i=0;i<resData.length;i++)
                    {
                        var data = resData[i];
                        if(data.gameType == self.getGameType()){
                            //var gainData = {date:gamename_cn[gamename.indexOf(data.gameType)],rebate:data.rebateAmount,gain:data.profitAmount,total:data.countAmount,time: data.createDate,gameType:data.gameType };
                            var v = Math.round(data.countAmount * 100) / 100;
                            if (v < 0) {
                                self._meiriyingkui.text = "-" + (-v).toString();
                            } else {
                                self._meiriyingkui.text = "" + v.toString();
                            }
                        } 
                    }
                }
                else
                {
                    self._meiriyingkui.text = "0";
                }
                //self.isGainReqing = false;
            });
        }
    }
    GameMain.prototype.onPlayerReward = function (value, detail, currency,totalBet,rank) {
        //this.updateCurrency(value);
        this.setCurrency(currency);
        MyLogger('onPlayerReward', value)

        // var text = "[color=#ff0000]第 " + this._gameNumber + " 期 中奖信息" + "[/color]<br/>";
        //TODO language
        var text = "[color=#ff0000]派奖：[/color]<br/>";
        text += "[color=#ff00ff]" + value.toFixed(2) + "[/color]" + "<br/>";
        text += "<br/>";

        this._contents[1] += text;

        if (this._contentController.selectedIndex == 1) {
            this.updateCurrentLiaoTianShiContent(text);
        }
        if(value - totalBet >0)
        {
            this.result_cl.selectedIndex = 1;
        }
        else if(value - totalBet ==0)
            this.result_cl.selectedIndex = 2;
        else
            this.result_cl.selectedIndex = 0;
        
        this._totalBet = totalBet;
        this._reward = value;
       // Laya.timer.once(1500, this,this.playMyRewardAni,[value,totalBet,rank,4]);

        for (var i = 0; i < this._betTypes.length; i++) {
            var type = this._betTypes[i];
            if (!!detail[type]) {
                var num = detail[type];

                if (this._choumaMgr.isTargetType(type))
                    this._choumaMgr.playRewardChoumaAni(type, num);
            }
        }

        var self = this;
        this.eventEmitter.on('choumaAniPlayEnd', function () {
            self._rewardTextAni.visible = true;
            self._rewardTextAni.getChild('tf_reward').asTextField.text = '+' + value.toString();
            var t = self._rewardTextAni.getTransition('t0');
            t.play();
            self.eventEmitter.off('choumaAniPlayEnd');
        });
    }


    GameMain.prototype.setOdds = function (odds) {
        for (var i = 0; i < odds.length; i++) {
            var type = this._betTypes[i];
            var xinxi = this._view.getChild("xinxi_" + type).asCom;
            xinxi.getChild("odds").text = "1:" + odds[i];
        }
    }

    GameMain.prototype.setLimits = function (data) {
        this._lowerLimit = data.lower;
        this._upperLimit = data.upper;
    }

    GameMain.prototype.onEnterScene = function (data) {

        this.updateState(data.area.state, data.area.counter);
        //this.setCurrency(data.currency);
        this._setting.firstEnterScene(pomelo.account);
        this.setOdds(data.odds);
        this.setLimits(data.limits);
        this.updateAllPlayerBet(data.totalBetCurrency);
        this.updateMyBet(data.playerBetCurrency);
        //this.onPlayerGainUpdate(data.gainToday);
        this.initLuZhiTu(data.luzhitu);
        this._phrase.firstEnterScene(data.phrase);
        if (!!data.kaijiangjieguo) {
            result = data.kaijiangjieguo;
            this._raw_zhuang_values = this.rawCards(result.zhuang);
            this._raw_xian_values = this.rawCards(result.xian);
            this._result_zhuang = result.zhuang;
            this._result_xian = result.xian;
            this._yingjia = result.yingjia;
            this._yingjiadianshu = result.dianshu;
            this._card_zhuang = this._fapaiMgr.cards(result.zhuang);
            this._card_xian = this._fapaiMgr.cards(result.xian);
            this._otherResults = [];
            if (!!result.leixing) {
                for (var i = 0; i < result.leixing.length; i++) {
                    var value = result.leixing[i];
                    if (value > 2) {
                        this._otherResults.push(value);
                    }
                }
            }
        }

        this.enableBetFunction(false);
       // this._btnQueRenTouZhu.enabled = (data.area.state == 0);
        this.setPreTouzhuEnabled(data.area.state == 0 && this._autoBet_ctrl.selectedIndex == 1);

        this._fapaiMgr.firstEnterView(data.kaijiangjieguo);

        this.autoBet = data.autoBet;
        this._btnZidongqueren = this._view.getChild('button_zidongtouzhu').asButton;
        this._btnZidongqueren.selected = data.autoBet.autoBet;
        this._btnZidongqueren.on(fairygui.Events.STATE_CHANGED, this, this.onBtnZidongquerenStateChange);
        this.onBtnZidongquerenStateChange();
        
        

        this._btnFenXiang = this._liaotianshi.getChild("button_fenxiang").asButton;
        this._btnFenXiang.selected = data.autoBet.fenxiang;
        this._btnFenXiang.on(fairygui.Events.STATE_CHANGED, this, this.onBtnFenxiangStateChange);
        this.initButtonMouseStyle(this._btnFenXiang);

        //如果已有预下注，恢复之前的状态
        if (!data.autoBet.autoBet) {
            this._btnZidongqueren.selected = true;
            this.autoBet.autoBet = true;
            this.autoBet.detail = [];
            this.updateAutoBet();
        } else if (this.autoBet.totalAmount > 0) {
            //每一项的预投注当作一次操作，用于撤销
            var value;
            for (var index in this.autoBet.detail) {
                value = this.autoBet.detail[index];
                if (value > 0) {
                    this._betList.push({ index: index, value: value });
                }
            }
            this._currentBet = this.autoBet.detail;
            this.updateTableChouma(false);
            this._btnQueRenTouZhu.enabled = true;
        }

        this.receivedMsgCount = 0;
        this.timeoutCount = 0;
        this.checkServerTimeID = window.setInterval(function(){
            if(this.receivedMsgCount == 0){
                this.timeoutCount++;
                if(this.timeoutCount > 1){
                    window.clearInterval(this.checkServerTimeID);
                    this.showReconnectBox(__D('DISCONNECTED_PLEASE_TRY_CONNECTING_AGAIN!'));
                    return;
                }
            }else{
                this.receivedMsgCount = 0;
                this.timeoutCount = 0;
            }
        }.bind(this), 30 * 1000);

    }

    GameMain.prototype.addRecievedMsgCount = function(){
        this.receivedMsgCount++;
    }

    GameMain.prototype.PlayGameSound = function (sund, times) {
         SoundManager.playSound(this.resourceSoundDir + sund, times);
    }

    GameMain.prototype.playMyRewardAni = function (number,totalBet,rank,seconds) {
        /*var txt = this._view.getChild('yingqian_tips').getChild('content');
        txt.text = number.toString();
        var t = this._view.getTransition('t4');
        t.play();*/
        var panel = this._view.getChild('yingqian_tips');
       // var winner_ctrl = panel.getController("c1");
            
        /*else if(this._yingjia == 1)
        {
            winner_ctrl.selectedIndex = 1;
        }
        else
            winner_ctrl.selectedIndex = 2;
        for(var i=0;i<this._card_zhuang.length;i++)
        {
            panel.getChild("zhuang_" + i).url = "ui://" + this.filePreName + "MainUI/" + this._card_zhuang[i];
        }
        for(var i=0;i<this._card_xian.length;i++)
        {
            panel.getChild("xian_" + i).url = "ui://" + this.filePreName + "MainUI/" + this._card_xian[i];
        }
        var tmp = "[color=#9a9a9a]" + this._betTypesChinese[this._yingjia] + "【" + this._yingjiadianshu + "】点";
        if (this._yingjia == 0 || this._yingjia == 1) {
            tmp += "赢;[/color]<br/>";
        }
        else if (this._yingjia == 2) {
            tmp += "和;[/color]<br/>";
        }
        tmp +=  "[color=#9a9a9a]";
        if (this._otherResults.length > 0) {
            for (var i = 0; i < this._otherResults.length; i++) { 
                tmp += this._betTypesChinese[this._otherResults[i]] + "";
                tmp += "; ";
            }
        }

        tmp +=  "[/color]<br/>";
        panel.getChild("result_other").asRichTextField.text = tmp;*/
        /*var anim = panel.getChild('anim');
        if(!!anim)
            anim.visible = false;*/
        panel.visible = true;
        totalBet = Math.round(totalBet * 100) / 100;
        panel.getChild('txt_bet').asTextField.text = totalBet;

        number = Math.round((number - totalBet)* 100) / 100;
        if(number >= 0){
         //   winner_ctrl.selectedIndex = 0;
            panel.getChild('txt_total').asTextField.text = '+' + number;
            panel.getChild('txt_total').asTextField.color = '#00FF00';
        }else{
         //   winner_ctrl.selectedIndex = 1;
            panel.getChild('txt_total').asTextField.text = number;
            panel.getChild('txt_total').asTextField.color = '#FF0000';
        }
        panel.getChild('countdown').visible = true;

       // if (!!closeCb)
       //     mbox._closeCbs[mbox._closeCbs.length] = closeCb;

        var counter = seconds || 3;
        var self = this;
        if (!!self.showResultBoxTimerId)
            window.clearInterval(self.showResultBoxTimerId);
        // mbox.getChild('countdown').asTextField.text = '(3s)';
        panel.getChild('countdown').asTextField.text = '(' + counter + 's)';

        self.showResultBoxTimerId = window.setInterval(function () {
            counter--;
            panel.getChild('countdown').asTextField.text = '(' + counter + 's)';
            if (counter <= 0) {
                window.clearInterval(self.showResultBoxTimerId);
                self.showResultBoxTimerId = null;
                self.hideResultBox(panel);
            }
        }, 1000);
        /*if(number > 0){
            var anim = panel.getChild('anim');
            if(!!anim){
                anim.visible = true;
                panel.getTransition('t1').play();
            }
        }*/
        
        /*var rankinfo = panel.getChild('txt_rank');
        if(!rankinfo)
            return;
        rankinfo.asTextField.text = rank ==0 ?"无":'' + (rank);*/
        SoundManager.playSound(this.resourceSoundDir + "yingqian.mp3", 1);
    }

    GameMain.prototype.hideResultBox = function (mbox) {
        /*for (var i = 0; i < mbox._closeCbs.length; ++i) {
            if (mbox._closeCbs[i]) {
                (mbox._closeCbs[i])();
            }
        }*/
       // this._result_ctrl.selectedIndex = 0;
        this.result_cl.selectedIndex = 0;
       // mbox.getChild("tran").visible = false;
        mbox.visible = false;
       // mbox._closeCbs = [];
    }
    GameMain.prototype.onResult = function (result, rewards,betPlayer) {
        this._result_data = [];
        this.myRank = -1;
        if(!!betPlayer)
        {
            for(var i=0;i<betPlayer.length;i++)
            {
                if(pomelo.playerNickName == betPlayer[i].name)
                {
                    this.myRank = i;
                    break;
                }
            }
            if(this.myRank > -1 && this.myRank<5){
                this._result_data = betPlayer.length <6?betPlayer:betPlayer.slice(0, 5);
                this._list_result.numItems = betPlayer.length <6?betPlayer.length:5;
            }
            else
            {
                if(this.myRank > -1){
                    var name = betPlayer[this.myRank].name;
                    var total = betPlayer[this.myRank].total;
                    var data = betPlayer.slice(0, 5);
                    data.push({ name: name, total:total });
                    this._result_data = data;
                    this._list_result.numItems = 6;
                }
            }
        }
        if(this._totalBet > 0)
        {
            this.playMyRewardAni(this._reward,this._totalBet,0,4);
        }
        // 显示本局结果
        this.showResult();

        // 显示前三名中奖用户
        //TODO figure out this whole paragraph
        if (!!rewards && rewards.length > 0) {
            //TODO language
            var rewardText = "[color=#ff0000]第 " + this._gameNumber + " 期中奖用户:[/color]<br/>";
            //TODO language
            var danmutext = "[color=#ffffff]第[/color][color=#5ee621] " + this._gameNumber + " [/color][color=#ffffff]期中奖用户:[/color]<br/>";

            for (var i = 0; i < rewards.length; i++) {
                var reward = rewards[i];
                //TODO language
                var str = "[color=#ffff00] 第 " + (i + 1) + " 名： " + reward.name + " [/color][color=#ffffff] " +" " +reward.total.toFixed(2);
                rewardText += (str + "[/color]<br/>");
                //TODO language
                danmutext += "[color=#5ee621] 第 " + (i + 1) + " 名： " + reward.name + " " + reward.total.toFixed(2);
                danmutext += ";&nbsp;[/color]";
            }

            this._Danmu.addText(danmutext, Browser.onMobile ? 30 : 24);

            rewardText += "<br/>";
            this._contents[2] += rewardText;

            //rewardText += "[color=#ff00ff]--------------------------------[/color]<br/><br/>";
            if (Browser.onMobile) {
                rewardText +=  "[color=#333333]" +"────────────────────"+"[/color]<br/><br/>";
            }
            else
            {
                rewardText +=  "[color=#333333]" +"──────────────"+"[/color]<br/><br/>";
            }
            this._contents[0] += rewardText;

            if (this._contentController.selectedIndex == 0 ||
                this._contentController.selectedIndex == 2) {
                this.updateCurrentLiaoTianShiContent(rewardText);
            }
        }

        if (!!result) {
            var self = this;
            for (var i = 0; i < this._betTypes.length; i++) {
                var type = this._betTypes[i];
                var number = result[type];
                if (number != undefined) {
                    number = Math.floor(number * 10000) / 10000;
                    this._maskZhongjiang[i].visible = true;
                    var txt = this._maskZhongjiang[i].getChild('tf_amount').asTextField;
                    txt.visible = true;
                    txt.text = "+" + number;

                    var t = this._maskZhongjiang[i].getTransition('t0');
                    t.play(Handler.create(self, self.onMaskPlayEnd, [self._maskZhongjiang[i]]));
                } else {
                    this._maskZhongjiang[i].visible = false;
                }
            }
        }

        //if (this.//.selectedIndex == 0 ){
        //    this.hi//deLuZhiTu();
        //    this.show//LuZhiTu();
        //} // sh//ow


    }

    GameMain.prototype.onMaskPlayEnd = function (obj) {
        obj.visible = false;
    }
    GameMain.prototype.postClientLogs = function (account,reqType,errorCode,processTime,usedTime) {
        var httpHost = window.location.href.replace(location.hash, '');
        var num = httpHost.indexOf("?");
        var host = httpHost.substr(0,num);
        if(host.substr(num-4,num) == "bin/"){
            host = httpHost.substr(0,num-4);
        }
        var url= host + "clientreqlog";///http://127.0.0.1:3001/
        var domain = window.location.host;
        $.post(url,
        {
            gameType:"BJL",
            account:account,
            reqType:reqType,
            error:errorCode,
            remoteIP:domain,
            processTime:processTime,
            usedTime:usedTime
        },
        function(){});
    }
    GameMain.prototype.enterOtherGame = function (gameIndex,account,balance,nickName) {
        var httpHost = window.location.href.replace(location.hash, '');
        var num = httpHost.indexOf("?");
        var host = httpHost.substr(0,num);
        if(host.substr(num-4,num) == "bin/"){
            host = httpHost.substr(0,num-4);
        }
        var url= host + "redirect";///http://127.0.0.1:3001/
        var domain = window.location.host;
        $.post(url,
        {
            gameType:"BJL",
            gameIndex:gameIndex,
            account:account,
            balance:balance,
            nickName:nickName,
            domain:this._domain
        },
        function(data){
            if(data.code == 200)
            {
                window.location = data.url;
            }
        });
    }
    GameMain.prototype.showResult = function () {
        //TODO language
        var cardType = [ '[color=#555555]♠[/color]', '[color=#ff0000]♥[/color]','[color=#ff0000]♦[/color]','[color=#555555]♣[/color]'];
        var text = "[color=#ff0000]本局【[/color][color=#ffffff]" + this._gameNumber + "[/color][color=#ff0000]】结果：[/color] <br/>";
        text += "[color=#58f5ff]庄： [/color]";
        for (var i = 0; i < this._raw_zhuang_values.length; i++) {
            var idx = this._raw_zhuang_values[i] - 1;
            var type = Math.floor((this._result_zhuang[i])/13);
            text += cardType[type] +"[color=#58f5ff]"+ this._cardValues[idx] + "[/color] ";
        }
        text += ";<br/>";
        text +=  "[color=#58f5ff]" + __D('IDLE') + "： [/color]";
        for (var i = 0; i < this._raw_xian_values.length; i++) {
            var idx = this._raw_xian_values[i] - 1;
            var type = Math.floor((this._result_xian[i])/13);
            text += cardType[type] + "[color=#58f5ff]"+ this._cardValues[idx] + "[/color] ";
        }
        text += "<br/>";

        //TODO language
        var tmp = "[color=#58f5ff]" + this._betTypesChinese[this._yingjia] + "【" + this._yingjiadianshu + "】点";
        if (this._yingjia == 0 || this._yingjia == 1) {
            tmp += __D('VICTORY_STRING');
        }
        else if (this._yingjia == 2) {
            tmp += __D('STATS_PANEL_WITH');
        }

        var duizileixing = 0;
        var daxiao = 0;
        if (this._otherResults.length > 0) {
            for (var i = 0; i < this._otherResults.length; i++) {
                tmp += "; ";
                tmp += this._betTypesChinese[this._otherResults[i]] + " ";

                var value = this._otherResults[i];
                if (value == 3) // 庄对
                    duizileixing += 1;
                else if (value == 4) // 闲对
                    duizileixing += 2;
                else if (value == 5 || value == 6) // 大小
                {
                    daxiao = value;
                }
            }
        }

        text += tmp + "[/color]<br/>";

        if (Browser.onMobile) {
            text +=  "[color=#333333]" +"────────────────────"+"[/color]<br/>";
        }
        else
        {
            text +=  "[color=#333333]" +"──────────────"+"[/color]<br/>";
        }
        this._contents[0] += text;
        this._contents[2] += text;
        if (this._contentController.selectedIndex == 0 ||
            this._contentController.selectedIndex == 2) {
            this.updateCurrentLiaoTianShiContent(text);
        }

        this._statsYingjia[this._yingjia]++;
        this._statsDuizi[duizileixing]++;
        this.updateStats();

        var zhupanjieguo = this._betTypesChinese[this._yingjia] + duizileixing.toString();
        this.updateZhuPanLu(zhupanjieguo);

        this.updateDaLu(this._daluContent, this._yingjia, false);

        this.showWenLu();

        this.updateDaXiaoLu(daxiao);
    }

    GameMain.prototype.onChat = function (data) {
        var text = "[color=#fed751]";
        var danmutext = "[color=#ffffff]";
        if (data.id == pomelo.playerId) {
            text += __IDLE('I') + ":";
            danmutext += __IDLE('I') + ":";
        } else {
            text += data.name + "：";
            danmutext += data.name + "：";
        }
        text += "[/color][color=#ffffff]" + data.msg + "[/color]<br/>";
        danmutext += "[/color][color=#5ee621]" + data.msg + "[/color]<br/>";

        this._Danmu.addText(danmutext, Browser.onMobile ? 30 : 24);

        this._contents[0] += text;
        if (this._contentController.selectedIndex == 0) {
            this.updateCurrentLiaoTianShiContent(text);
        }
    }

    GameMain.prototype.onPlayerEnterRoom = function (names) {
        if (!names || names.length == 0)
            return;

        var text = '';
        for (var i = 0; i < names.length; ++i) {
            //TODO language
            text += "[color=#ff2c2c]" + names[i] + "[/color] &nbsp; [color=#ffffff]进入房间[/color]<br/>";
        }


        this._contents[0] += text;
        if (this._contentController.selectedIndex == 0) {
            this.updateCurrentLiaoTianShiContent(text);
        }
    }

    GameMain.prototype.renderListItem_result = function (index, obj) {
        
        var item = obj;
        
        var dId = index;
        if (dId >= this._result_data.length)
            dId = this._result_data.length-1;
        var cl = item.getController("c1");
        cl.selectedIndex = 0;
        if(this._result_data[dId].name == pomelo.playerNickName)
        {
            cl.selectedIndex = this.result_cl.selectedIndex == 1?2:1;
            if(this.myRank != -1)
            {
                item.getChild("rank").text = '' + (this.myRank+1);
            }
            item.getChild("self").visible = true;
        }
        else
        {
            item.getChild("rank").text = '' + (index+1);
            item.getChild("self").visible = false;
        }
        if(this.result_cl.selectedIndex == 0)
        {
            item.getChild("gain").color = "#ff5656";
        }
        else
        {
            item.getChild("gain").color = "#ffbd2f";
        }
        item.getChild("name").text = this._result_data[dId].name;
        
        item.getChild("gain").text = (this._result_data[dId].total).toFixed(2);
    }

    GameMain.prototype.onPlayerFenXiang = function (fenxiang) {
        if (!fenxiang || fenxiang.length == 0)
            return;

        var length = this._playerFenXiangInfo.length;
        var tt = "";
        for (var k = fenxiang.length - 1; k >= 0; --k) {
            var data = fenxiang[k];
            var text = "";
            var danmutext = "";
            text += "[color=#ff0000]" + __D('PLAYER_STRING') + "【[/color][color=#ff00ff]" + data.name + "[/color][color=#ff0000]】" + __D('SHARE_BET_INFOMATION') + "[/color]<br/>";
            danmutext += "[color=#ffffff]" + __D('PLAYER_STRING') + "【[/color][color=#5ee621]" + data.name + "[/color][color=#ffffff]】" + __D('SHARE_BET_INFOMATION') + "：[/color]<br/>";

            for (var i = 0; i < data.detail.length; i++) {
                if (data.detail[i] > 0) {
                    var str = "[color=#ff0000] " + this._betTypesChinese[i] + "：[/color]&nbsp; [color=#ff00ff]" + data.detail[i];
                    text += (str + "[/color]<br/>");
                    danmutext += "[color=#5ee621] " + this._betTypesChinese[i] + "：[/color]&nbsp; [color=#5ee621]" + data.detail[i];
                    danmutext += (";&nbsp;[/color]");
                }
            }

            if (!!data.gentou) {
                //TODO language
                text += "[color=#ffff00]<a href='" + length + "'>点击跟单</a>[/color]<br/>";
                this._playerFenXiangInfo[length++] = data.detail;
            }

            tt += text;
            this._Danmu.addText(danmutext, Browser.onMobile ? 30 : 24);
        }


        this._contents[0] += tt;
        if (this._contentController.selectedIndex == 0) {
            this.updateCurrentLiaoTianShiContent(tt);
        }
    }

    GameMain.prototype.updateCurrentLiaoTianShiContent = function (addedText) {
        var tmp = this._content.text + addedText;

        // 最大长度 
        var maxTextLen = 5000;
        if (tmp.length > maxTextLen) {
            tmp = tmp.substr(tmp.length - maxTextLen);
            var inx = tmp.indexOf("<br/>", 0);
            tmp = tmp.substr(inx);
        }
        this._content.text = tmp;
        this._content.height = this._content.div.contextHeight;
        this._liaotianneirong.scrollPane.scrollBottom();
        //console.log("updateCurrentLiaoTianShiContent:", this._content.text.length, "  ", this._content.height);
    }

    GameMain.prototype.kickLoginDup = function () {
        var self = this;
        this.showMessageBox(__D('ALREADY_LOGGED_IN_ERROR'), 8, function () {
            if (Browser.onMobile) {
                window.location = self._domain + "/lotts/otherGames";
            }
            else {
                window.location = self._domain + "/qyy/bet/index";
            }
        });
    }

    GameMain.prototype.resetTouZhuClickInfo = function () {
        this._touZhuQuClicked = false;
        this._touzhuButtonClicked = false;
        this._touzhuQuClick = [0, 0, 0, 0, 0, 0, 0, 0, 0];
        this._touzhuButtonClick = [0, 0, 0, 0, 0, 0, 0, 0, 0];
    }

    // 发送点击信息
    GameMain.prototype.sendClickInfo = function () {
        var data = {};
        if (this._touZhuQuClicked)
            data["touzhuqu"] = this._touzhuQuClick;
        if (this._touzhuButtonClicked)
            data["touzhubutton"] = this._touzhuButtonClick;

        if (this._touZhuQuClicked || this._touzhuButtonClicked) {
            //    pomelo.request('area.playerHandler.updateClickInfo', data);
        }

        this.resetTouZhuClickInfo();
    }

    GameMain.prototype.setCanChat = function (canChat) {
        this._canChat = canChat;
        this.updateBtnFaSongStatus();
    }

    GameMain.prototype.setDomain = function (domain) {
        this._domain = domain;
    }

    /**
    * 发送修改自动下注的请求
    * 
    */
    GameMain.prototype.updateAutoBet = function () {
        this.isLooping = false;

        if (!this._canTouZhu) {
            return;
        }

        pomelo.notify('area.playerHandler.updatePlayerAutoBet', this.autoBet);
    }

    GameMain.prototype.updateAutoBetLooping = function (doUpdating) {
        if (doUpdating) {
            this.autoBet.detail = this._currentBet;
            if (this.autoBet.autoBet) {
                if (this.timeleft < 3) {
                    Laya.timer.clear(this, this._looping);
                    this.updateAutoBet();
                } else {
                    this.loopEndTime = Date.now() + 500;//停止操作后0.5秒发送更新

                    if (!this.isLooping) {
                        this.isLooping = true;
                        Laya.timer.loop(100, this, this._looping);
                    }
                }


            }
        }
    }

    GameMain.prototype._looping = function () {
        if (Date.now() - this.loopEndTime >= 0) {
            Laya.timer.clear(this, this._looping);
            this.updateAutoBet();
        }
    }

    GameMain.prototype.loadReqLog = function (res,isLoadFail,time) {
        if(res.length <=0){
            var type = isLoadFail?"loadingResfail":"reloading";
            this.postClientLogs(pomelo.loginaccount,type,"TimeOut",0,time);
            return;
        }
        var resname = "";
        var type = isLoadFail?"loadingResfail":"reloadingResfail";
        for(var i=0;i<res.length;i++){
            if(i>0){
                resname += " + ";
            }
            resname +='['+ res[i]+']';
        }
        this.postClientLogs(pomelo.loginaccount,type,resname,0,time);
    }

    GameMain.prototype.postLeadBoardRequest = function (date,game,callback) {
        var httpHost = window.location.origin;

        var url= httpHost + "/queryLBdata";///http://127.0.0.1:3001/
        var self = this;
        $.post(url,
            {
                account:pomelo.account,
                date:date,
                game:game
            },
            function(res){
                if(!!res)
                {
                    //var data = JSON.parse(res);
                    callback(res);
                }
            });
        
        
    }
    GameMain.prototype.getGameType = function () {
       return "BJL";
    }
    GameMain.prototype.postGainRequest = function (reqType,date,game,seasonId,callback) {
        //var httpHost = window.location.href.replace(location.hash, '');
        var httpHost = window.location.origin;
        /*var num = httpHost.indexOf("?");
        var host = httpHost.substr(0,num);
        if(host.substr(num-4,num) == "bin/"){
            host = httpHost.substr(0,num-4);
        }*/
        var url= httpHost + "/queryGaindata";///http://127.0.0.1:3001/
        //var domain = window.location.host;
        var self = this;
        $.post(url,
      //  window.bdgame_utils.ajax("post",url,
            {
                reqType:reqType,
                account:pomelo.account,
                date:date,
                game:game,
                seasonId:seasonId
            },
            function(res){
                if(!!res)
                {
                    //var data = JSON.parse(res);
                    callback(res);
                }
            });
        
        
    }
    GameMain.prototype.onClickGain = function () {
        this.iFrame = createIframe();
    }
    GameMain.prototype.closeiFrame = function () {
        destroyIframe(this.iFrame);
    }
    function createIframe(dom, src, onload){
        var Browser=laya.utils.Browser;
        var iframe = Browser.document.createElement("iframe");
        iframe.style.position ="absolute";//设置布局定位。这个不能少。
        iframe.style.zIndex = 100;//设置层级
        iframe.style.left ="0px";
        iframe.style.top ="0px";
        iframe.style.width ="100%";
        iframe.style.height ="100%";
        iframe.style.border = "none";
       // iframe.align = "middle";
        iframe.style.overflow ="hidden";
        iframe.marginwidth=0;
        iframe.marginheight=0;
        //iframe.border =0;
        iframe.scrolling="no";
        if(window.location.pathname =="/bin/")
            iframe.src = "../bin/gain/gain.html";
        else
            iframe.src = "../gain/gain.html";
        Browser.document.body.appendChild(iframe);
        return iframe;
    }

    function destroyIframe(iframe){
        //把iframe指向空白页面，这样能够释放大部分内存。
        iframe.src = 'about:blank';
        try{
            iframe.contentWindow.document.write('');
            iframe.contentWindow.document.clear();
        }catch(e){}
        //把iframe从页面移除
        iframe.parentNode.removeChild(iframe);
    }

    //获取当前时间，格式YYYY-MM-DD
    function getNowFormatDate() {
        var date = new Date();
        var seperator1 = "-";
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var strDate = date.getDate();
        if (month >= 1 && month <= 9) {
            month = "0" + month;
        }
        if (strDate >= 0 && strDate <= 9) {
            strDate = "0" + strDate;
        }
        var currentdate = year + seperator1 + month + seperator1 + strDate;
        return currentdate;
    }
    return GameMain;
}());

pomelo.game = new GameMain();
//# sourceMappingURL=GameMain.js.map