import Chinese from './chinese'
import English from './english'
import Indonesian from './indonesian'
import Japanese from './japanese'
import Korean from './korean'
import Thai from './thai'
import TraditionalChinese from './traditionalChinese'
import Vietnamese from './vietnamese'

const DictionaryCollection = {
    Chinese,
    English,
    Indonesian,
    Japanese,
    Korean,
    Thai,
    TraditionalChinese,
    Vietnamese,
}

export default DictionaryCollection
export type DictionaryName = keyof typeof DictionaryCollection