import Definitions from './dictionary/definitions'
import DictionaryCollection, { DictionaryName } from 'language/dictionary/collection'
import Utils from 'debug/utils'

interface LanguageInfo{
    language: string
    dictionary: Definitions
}

export interface InterpolationCollection{
    [s: string]: any
}

const languageIndexMap = {
    Chinese: 0,
    TraditionalChinese: 1,
    English: 2,
    Indonesian: 3,
    Japanese: 4,
    Korean: 5,
    Thai: 6,
    Vietnamese: 7,
} 

export default class Associator{
    static controllerName = '__language'
    static currentLanguage: DictionaryName = 'Chinese'
    static currentDictionary: Definitions = DictionaryCollection.Chinese
    static Collection = DictionaryCollection

    static interpolateString = (string: string, args: Object) => 
        string.replace(/\${(\w+)}/g, (_, v) => args[v])

    static associateView = (view: fairygui.GComponent, dictionary: Definitions, language: DictionaryName) => {
        const components = [view]
        Associator.switchLanguage(dictionary, language)
        const children = Utils.getChildrenByType(view, 'GComponent', true) as fairygui.GComponent[]
        children.forEach(child => components.push(child))
        components.forEach(component => {
            const controller = component.getController(Associator.controllerName)
            if(controller instanceof fairygui.Controller){
                controller.selectedIndex = languageIndexMap[language]
            }
        })
    }

    static switchLanguage = (dictionary: Definitions, language: DictionaryName) => {
        Associator.currentLanguage = language
        Associator.currentDictionary = dictionary
    }

    static getTextFromKey = (key: string, args: InterpolationCollection) => {
        if(!Associator.currentDictionary.hasOwnProperty(key))
            throw new Error(`Current dictionary ${Associator.currentLanguage} doesn't contain a value for given key ${key}`)

        const value = Associator.currentDictionary[key]
        if(args == Object(args))
            return Associator.interpolateString(value, args)

        return value
    }
}