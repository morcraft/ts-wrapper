export interface Item{
    id: string
    parent: string
    text: string
}

export default class StringManager{
    static ids: Item[]
    static isChineseString = (text: string) => {
        return typeof text == 'string' && text.match(/[\u2E80-\u2FD5\u3190-\u319f\u3400-\u4DBF\u4E00-\u9FCC对]+/gu)
    }

    static extractStrings = (item: fairygui.GComponent, parent: string) => {
        try{
            if (StringManager.isChineseString(item.text)){
                StringManager.ids.push({
                    id: item._id, 
                    parent: parent, 
                    text: item.text 
                })
            }

            parent += `.${item._id}`

            if (item._children)
                item._children.forEach(child => StringManager.extractStrings(child, parent))
        }
        catch(error){
            console.error('Error at', item)
            throw error
        }
    }
}

