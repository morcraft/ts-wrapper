
# H5 Game Development - Hiteky

The stack being used to develop H5 (HTML5) games at Hiteky Co. primarily consists of the following technologies:

- [NodeJS](https://nodejs.org/) (for back-end operations and real-time capabilities)
- [Pomelo](https://github.com/NetEase/pomelo) (as the main game server framework)
- [LayaAir](https://github.com/layabox/layaair) (as the main game engine)
- [FairyGUI Editor](https://github.com/fairygui/FairyGUI-Editor) (as the UI editor, compatible with LayaBox)
- [FairyGUI Layabox Runtime Library](https://github.com/fairygui/FairyGUI-layabox) (to access and operate during runtime the exported Buffer)

To understand each of the above, you should become familiar with their functionality and API. Each one of them provides extensive API documentation and examples, so their use cases, features and importance are easier to grasp.

While it’s not necessary to memorize and understand all these technologies’ features and APIs, it’s very convenient to have a general understanding of the purpose of them and their implementation details, so the learning curve to successfully develop an H5 game using these is far gentler.

Since JavaScript is the main language being used throughout the development process, it is critical to know the language, its features, rules, syntax, structure, etc. 

To overcome some of the flaws and common issues in a development environment supported in JavaScript (loose types, type coercion, silent fails, type checking unavailable during compile time, etc.), we’re using [TypeScript](https://www.typescriptlang.org/). It allows us to have a much richer set of development tools, so less time is spent in debugging and much of it is done during compile time.

The existing JavaScript codebase should rarely be modified, since it already encloses most of the required logic and doesn’t provide documentation. To extend the game’s functionality, we’re using a separate TypeScript environment that is later transformed via [webpack](https://webpack.js.org) and loaded in the browser.

## Project structure

The current folder project structure allows the creation and consumption of individual, reusable components throughout the game implementation. Given that the existing game scripts are usually already in a mature state, we dont't modify them. This is mostly to avoid costly refactoring and to prevent unexpected additional bugs. Instead, we add a single entry point that's assigned in a global "TSWrapper". These are the main files that shouldn't change, as the rest of this code depends on it:

```
|-- LayaAir.d.ts
|-- fairygui.d.ts
|-- main.ts
|-- package.json
|-- tsconfig.json
`-- webpack.config.js
```

## Game Flow

The following diagram illustrates the game consumption phases:

![Game development flowchart](https://drive.google.com/uc?export=download&id=1ibH7Xe8TmCyYcYY8ztuhr3aG_WF113q5)

As it's illustrated above, FairyGUI editor publishes a buffer file, along with a few atlas files, that are loaded through the methods provided by LayaAir.  FairyGUI takes advantage of LayaAir's API to provide multiple methods to operate on each element that makes a part of the .fairy project file. This gives the developer a high level of abstraction to focus on the core logic of the game, while leaving the rendering and UI refreshing process to the engine.