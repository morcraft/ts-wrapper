## Responsive feature

The responsive feature for the games consists of two phases:

- The first phase corresponds to the automatic resizing of the Laya stage instance according to the window dimensions while in mobile mode (although this could be implemented in desktop mode as well. This is already implemented in the game scripts and the handler should be enabled by default in main.ts. The handler will listen to window resize events and adjust the dimensions of the stage accordingly.

Since Laya renders everything in the stage per frame, the "relations" feature of FairyGUI will take care of adjusting the positions, dimensions, and other attributes controllable via relations. Refer to the [docs](http://fairygui.com/guide/editor/relation.html) for a deep dive.

- The second phase consists of setting the relations of the components that will be affected by the resizing of the stage according to how they should behave. The most common ones will be width -> width and height -> height, though this needs to be checked for each element, since some of them might behave differently. 

An easy way to set relations for multiple elements is to modify the XML component files directly instead of using the FairyGUI editor, because it doesn't offer a feature to set relations for more than one element at a time. 

In most games, the relations that will have to be set belong to the Main component, since the additional components (Settings, LeaderBoard) can simply be centered as a whole regardless of the dimensions of the parent container. 

Relations alone and the screen handler should suffice the requirement for responsiveness.
