## Translation process for HTML5 FairyGUI-Laya-based games

The translation process consists of a few linear steps and processes.

All the game UI projects are created on the FairyGUI editor, which runs on top of Laya during runtime to provide access to members of the project and common methods and abstractions.

Among its features, there exist controllers, which essentially manipulate the state of UI elements. 

Limited as they are, they provide an easy mechanism to control attributes such as visibility, color, position, scale, etc. These are listed and explained in the [FairyGUI Docs](http://www.fairygui.com/guide/editor/controller.html).

Due to the limitations in the styling and control of effects of text elements in FairyGUI, the designers often use images containing text instead of text elements and input them in the editor. This works fairly well in most cases, but possesses plenty of drawbacks, one of them concerning the translation process:

- All the text these pictures contain needs to be stored in plain text, listed and later added into the Excel translation spreadsheets. This text (usually) can be obtained from the original design files (Illustrator, Photoshop, etc.)

To find the pictures used in the project, a simple file lookup in the FairyGUI project path is enough. Then the pictures containing Chinese text can be listed, and their text can be extracted.

The above could be considered the first step in the translation process.

- The second step consists of extracting all the strings used in the FairyGUI projects and the game script files. This is a time-consuming process that can take from days to weeks if it's not automated and it's proven to be extremely error-prone.

FairyGUI has a built-in command for extracting all the strings used in the project and generating an XML file containing the path of IDs (every element in FairyGUI has an automatically-generated ID). This feature comes useful for translation purposes. However, it needs to be re-run every time there's a change in any text element of the project.
During the early stages of the implementation of multiple-languages for the games, we took advantage of these paths, so once the translation rounds were finished, we could replace the text of all the text nodes containing a translation during runtime, and it worked well.

The biggest issue with the above method is that every text element can have different attributes for each language, involving not just the text but any random attribute (font size, coordinates, rotation etc.), so that means we not only need the store the translation text, but all the attributes for all the text elements in the project!

Since the designer often needs to modify the project, all of this becomes cumbersome and quite hard to maintain.

The solution we came up is the following:

- Instead of storing attributes in our code base for text elements to modify their appearance and content during runtime, we can simply store all of that in the FairyGUI project itself by modifying its configuration files (XML).

Every language can be thought of as a mode of display for the game, so all what's needed are a language controller for all the components of the game, and links between the elements that may change (text nodes or any others) and the controller pages. Each controller page represents a language, as follows: 


    Page 0: CN2: 'Traditional Chinese',
    Page 1: EN: 'English',
    Page 2: IN: 'Indonesian',
    Page 3: JP: 'Japanese',
    Page 4: KR: 'Korean',
    Page 5: TH: 'Thai',
    Page 6: VN: 'Vietnamese',


The creation of those controllers and linking of text nodes to them has already been automated by the task [synchronizeText](https://gitlab.com/morcraft/fairygui-project-tasks/tree/master/synchronizeText), so the Excel translation spreadsheets become the center of the translation process, and are all what's needed to include the translation text in the FairyGUI project and allow the designer to change attributes arbitrarily without needing an additional effort from the developer, so the game buffer can just be re-exported and added into the game.

Going back to the drawbacks of having image elements containing Chinese text instead of using text elements in the FairyGUI project, given that there are no strings to match against when the script reads the translation spreadsheets and analyzes the project, these pictures must be treated as a special case. 

The designer must export the corresponding translated images, so these can be integrated in the project. This step, however, can become very tedious and time-consuming as well, because either the designer or the developer have to:

- a. Place the translated pictures in the project folder (keeping in mind the original developers maintained two separate projects for each game, one for each platform: desktop and mobile)
- b. Add the pictures in the project exactly on the same components wherever they appear (this can be hundreds of occurrences)
- c. Link each picture to a page in the language controller

All of the above steps have been automated as well in the [synchronizePictures task](https://gitlab.com/morcraft/fairygui-project-tasks/tree/master/synchronizePictures), so all the translated pictures the designer produces can be located in one folder, and the script will take care of placing them correctly in the project according to their occurrences.

Note: The matching of pictures is done based on the file name and the prefixes of the pictures, e.g.
    
    庄.png
    庄_CN2.png
    庄_EN.png
    庄_IN.png
    庄_JP.png
    庄_KR.png
    庄_TH.png
    庄_VN.png

The language suffixes are obtained from the configuration the developer provides, so these can be altered freely.

The last step consists of updating the Chinese text that is used on the game scripts. The original developers used static text for almost every message used in the game (modals, chat texts, popups, etc.), so it must be replaced accordingly for each language, so that the user will see translated messages every time he switches to another language. This step could be automated as well, but preferably should be done manually, examining every instance of occurrence withing the game scripts, since this text often contains run-time variables and changes according to game state.

All those Chinese strings in the scripts can be easily obtained with a text crawler or [any text search tool](https://helpdeskgeek.com/how-to/search-inside-multiple-text-files-at-once/)
