## Orientation Feature

This feature refers to the ability of having a second layout for mobile devices, with similar characteristics as the layour for desktops, but with a readability-oriented and minimalistic design, so it's easier to interact with on tablets and mobile phones while these are held in a horizontal position.

To implement this feature, we can also make use of the __Controllers__ feature of FairyGUI. Refer to the [docs](http://fairygui.com/guide/editor/controller.html) for a deep dive. This feature is heavily related to the multiple-languages feature described in multiLanguageFeature.md, as each language might have a different configuration for the attributes of all the elements in the stage. 

However, certain elements will not need to be behave differently according to each language. These include backgrounds, buttons with no text, etc. Also not all components will behave differently. Because of that, the scripts in charge of integrating translated text and pictures in the FairyGUI project don't take the multiple-orientation feature into account. 

A simple way to do this is to create more pages in the components that will behave differently in each language, and add a 'LM' suffix or another identifier to let the designers and developers know it refers to the landscape mode. For most games, the components that will have significant changes are the Main component and the Stats component. Most of the other components will have the same appearance, or they might just fit the screen accordingly. This can be achieved easily with [__Relations__](http://fairygui.com/guide/editor/relation.html).

These docs server as a general guideline, but each game might pose different challenges, so additional considerations should be taken in each case.
