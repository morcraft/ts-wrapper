## TSWrapper

This repository can be thought of as the additions to the existing game features and logic in the games developed on Laya and FairyGUI.

Some of the files in these folders are the output of scripts, such as [fairygui-typescript-tree-generator](https://gitlab.com/morcraft/fairygui-typescript-tree-generator), [fairygui-project-tasks](https://gitlab.com/morcraft/fairygui-project-tasks), so you need to check the docs of these in order to see where they were used.

A debugger/inspector has been included and should be opened after the shortcut ctrl+shift+d

How to build:

	npm install
	npm run build

For live reloading, you may execute

	npm run hot-build

The TypeScript definitions file will be automatically generated before each build. 

The output file needs to be loaded, and a TSWrapper global variable will be available for use.

The intention behind of it is to be easily extensible through "handlers", so you may add your own and rely on the definition files created by [fairygui-typescript-tree-generator](https://gitlab.com/morcraft/fairygui-typescript-tree-generator) for member access type safety.

![Load in your HTML file](https://drive.google.com/uc?export=download&id=1rm0EiH8YRNtQhpMQegNZRDhLxx8LLWMH)

![Send a view and mobile boolean](https://drive.google.com/uc?export=download&id=1mTgGcBPfYuAobfU1Uo_x6ieYofYFighv)


